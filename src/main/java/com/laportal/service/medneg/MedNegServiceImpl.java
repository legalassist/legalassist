package com.laportal.service.medneg;

import com.laportal.Repo.*;
import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class MedNegServiceImpl extends AbstractApi implements MedNegService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;

    @Autowired
    private TblMnclaimRepo tblMnclaimRepo;

    @Autowired
    private TblMnlogRepo tblMnlogRepo;

    @Autowired
    private TblMnmessageRepo tblMnmessageRepo;

    @Autowired
    private TblMnnoteRepo tblMnnoteRepo;

    @Autowired
    private TblMnsolicitorRepo tblMnsolicitorRepo;

    @Autowired
    private TblTaskRepo tblTaskRepo;

    @Autowired
    private TblUsersRepo tblUsersRepo;

    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;

    @Autowired
    private TblEmailRepo tblEmailRepo;

    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;

    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;

    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;

    @Autowired
    private TblMndocumentRepo tblMndocumentRepo;

    @Autowired
    private TblMntaskRepo tblMntaskRepo;

    @Autowired
    private TblEmailTemplateRepo tblEmailTemplateRepo;

    @Autowired
    private TblSolicitorInvoiceDetailRepo tblSolicitorInvoiceDetailRepo;

    @Autowired
    private TblEsignStatusRepo tblEsignStatusRepo;

    @Autowired
    private ViewMnclaimRepo viewMnclaimRepo;

    @Autowired
    private ViewStatuscountRepo viewStatuscountRepo;

    @Autowired
    private TblRtaflowRepo tblRtaflowRepo;


    @Override
    public TblMnclaim findMnCaseById(long MnCaseId, TblUser tblUser) {
        TblMnclaim tblMnclaim = tblMnclaimRepo.findById(MnCaseId).orElse(null);

        TblRtastatus tblRtastatus = tblRtastatusRepo.findById(tblMnclaim.getStatus().longValue()).orElse(null);
        if (tblMnclaim != null) {

            List<TblMndocument> tblMndocuments = tblMndocumentRepo.findByTblMnclaimMnclaimcode(MnCaseId);
            if (tblMndocuments != null && tblMndocuments.size() > 0) {
                tblMnclaim.setTblMndocuments(tblMndocuments);
            }

            List<RtaActionButton> rtaActionButtons = new ArrayList<>();
            List<RtaActionButton> rtaActionButtonsForLA = new ArrayList<>();
            RtaActionButton rtaActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    tblMnclaim.getStatus().longValue(), "11",
                    tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetailForLA = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    tblMnclaim.getStatus().longValue(), "11",
                    tblCompanyprofile.getTblUsercategory().getCategorycode());

            TblRtaflow tblRtaflow = tblRtaflowRepo.findByTblCompaignCompaigncodeAndTblRtastatusStatuscode("11", tblMnclaim.getStatus().longValue());
            if (tblRtaflow != null) {
                tblMnclaim.setEditFlag(tblRtaflow.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode()) ? tblRtaflow.getEditflag() : "N");
            }

            /// for introducer and solicitor
            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    rtaActionButton = new RtaActionButton();
                    rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                    rtaActionButtons.add(rtaActionButton);
                }

                // for LEGAL INTERNAL action buttons in dropdown

                if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                        rtaActionButton = new RtaActionButton();
                        rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                        rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                        rtaActionButtonsForLA.add(rtaActionButton);
                    }
                    tblMnclaim.setMnActionButtonForLA(rtaActionButtonsForLA);

                    if (tblRtaflowdetailForLA.get(0).getTblRtaflow().getTaskflag().equalsIgnoreCase("Y")) {
                        List<TblMntask> tblMntasks = tblMntaskRepo.findByTblMnclaimMnclaimcode(tblMnclaim.getMnclaimcode());
                        tblMnclaim.setTblMntasks(tblMntasks);
                    }
                }


                tblMnclaim.setMnActionButtons(rtaActionButtons);
                tblMnclaim.setStatusDescr(tblRtastatus.getDescr());

                if (tblMnclaim.getIntroducer() != null && tblMnclaim.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblMnclaim.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo
                            .findById(String.valueOf(tblMnclaim.getIntroducer())).orElse(null);

                    TblMnsolicitor tblMnsolicitor = tblMnsolicitorRepo
                            .findByTblMnclaimMnclaimcodeAndStatus(tblMnclaim.getMnclaimcode(), "Y");
                    if (tblMnsolicitor != null) {
                        tblMnclaim.setSolicitorcompany(tblCompanyprofileRepo
                                .findById(String.valueOf(tblMnsolicitor.getCompanycode())).orElse(null).getName());
                        tblMnclaim.setSolicitorusername(tblUsersRepo
                                .findByUsercode(String.valueOf(tblMnsolicitor.getUsercode())).getLoginid());
                    }
                    tblMnclaim.setAdvisorname(user == null ? null : user.getLoginid());
                    tblMnclaim.setIntroducername(companyprofile == null ? null : companyprofile.getName());
                }

                List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("11", new BigDecimal(tblMnclaim.getMnclaimcode()));
                if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                    for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                        TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                        if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                            tblMnclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblMnclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                            tblMnclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblMnclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        }

                    }
                }


                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("11", new BigDecimal(tblMnclaim.getMnclaimcode()));
                tblMnclaim.setTblEsignStatus(tblEsignStatus);

                return tblMnclaim;

            } else {

                if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                        rtaActionButton = new RtaActionButton();
                        rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                        rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                        rtaActionButtonsForLA.add(rtaActionButton);
                    }
                    tblMnclaim.setMnActionButtons(rtaActionButtonsForLA);

                    if (tblRtaflowdetailForLA.get(0).getTblRtaflow().getTaskflag().equalsIgnoreCase("Y")) {
                        List<TblMntask> tblMntasks = tblMntaskRepo.findByTblMnclaimMnclaimcode(tblMnclaim.getMnclaimcode());
                        tblMnclaim.setTblMntasks(tblMntasks);
                    }
                }

                if (tblMnclaim.getIntroducer() != null && tblMnclaim.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblMnclaim.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo
                            .findById(String.valueOf(tblMnclaim.getIntroducer())).orElse(null);

                    TblMnsolicitor tblMnsolicitor = tblMnsolicitorRepo
                            .findByTblMnclaimMnclaimcodeAndStatus(tblMnclaim.getMnclaimcode(), "Y");
                    if (tblMnsolicitor != null) {
                        tblMnclaim.setSolicitorcompany(tblCompanyprofileRepo
                                .findById(String.valueOf(tblMnsolicitor.getCompanycode())).orElse(null).getName());
                        tblMnclaim.setSolicitorusername(tblUsersRepo
                                .findByUsercode(String.valueOf(tblMnsolicitor.getUsercode())).getLoginid());
                    }
                    tblMnclaim.setAdvisorname(user.getLoginid());
                    tblMnclaim.setIntroducername(companyprofile.getName());
                }


                List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("11", new BigDecimal(tblMnclaim.getMnclaimcode()));
                if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                    for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                        TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                        if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                            tblMnclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblMnclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                            tblMnclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblMnclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        }

                    }
                }
                tblMnclaim.setStatusDescr(tblRtastatus.getDescr());

                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("11", new BigDecimal(tblMnclaim.getMnclaimcode()));
                tblMnclaim.setTblEsignStatus(tblEsignStatus);


                return tblMnclaim;
            }
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public List<MnStatusCountList> getAllMnStatusCounts(String companycode, String campaignCode) {
        List<MnStatusCountList> mnStatusCountLists = new ArrayList<>();
        List<ViewStatuscount> OlStatusCountListObject = viewStatuscountRepo.getStatusCount(Long.valueOf(companycode), Long.valueOf(campaignCode));
        MnStatusCountList mnStatusCountList;
        if (OlStatusCountListObject != null && OlStatusCountListObject.size() > 0) {
            for (ViewStatuscount viewStatuscount : OlStatusCountListObject) {
                mnStatusCountList = new MnStatusCountList();


                mnStatusCountList.setStatusCount(viewStatuscount.getStatuscount());
                mnStatusCountList.setStatusName(viewStatuscount.getDescr());
                mnStatusCountList.setStatusCode(viewStatuscount.getStatuscode());

                mnStatusCountLists.add(mnStatusCountList);
            }
        }
        if (mnStatusCountLists != null && mnStatusCountLists.size() > 0) {
            return mnStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<MNCaseList> getMnCasesByStatus(String usercode, String status, String categorycode) {
        List<MNCaseList> MNCaseLists = new ArrayList<>();
        List<ViewMnclaim> MnCasesListForIntroducers = viewMnclaimRepo.getAuthMnCasesUserAndCategoryWiseAndStatusWise(new BigDecimal(usercode), new BigDecimal(categorycode), new BigDecimal(status));
        MNCaseList MNCaseList;
        if (MnCasesListForIntroducers != null && MnCasesListForIntroducers.size() > 0) {
            for (ViewMnclaim record : MnCasesListForIntroducers) {
                MNCaseList = new MNCaseList();


                MNCaseList.setMnClaimCode(record.getMnclaimcode());
                MNCaseList.setCreated(record.getCreatedate());
                MNCaseList.setCode(record.getMncode());
                MNCaseList.setClient(record.getClientname());
//                MNCaseList.setTaskDue(record.get);
                MNCaseList.setTaskName(record.getMntaskname());
                MNCaseList.setStatus(record.getStatus());
                MNCaseList.setEmail(record.getClientemail());
                MNCaseList.setAddress(record.getClientaddress());
                MNCaseList.setContactNo(record.getClientcontactno());
                MNCaseList.setLastUpdated(record.getUpdatedate());
                MNCaseList.setIntroducer(record.getIntroducerUser());
//                MNCaseList.setLastNote((Date) row[12]);
                MNCaseLists.add(MNCaseList);
            }
        }
        if (MNCaseLists != null && MNCaseLists.size() > 0) {
            return MNCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblMnlog> getMnCaseLogs(String Mncode) {
        List<TblMnlog> TblMnlogs = tblMnlogRepo.findByTblMnclaimMnclaimcode(Long.valueOf(Mncode));
        if (TblMnlogs != null && TblMnlogs.size() > 0) {
            for (TblMnlog TblMnlog : TblMnlogs) {
                TblUser tblUser = tblUsersRepo.findById(TblMnlog.getUsercode()).orElse(null);
                TblMnlog.setUserName(tblUser.getUsername());
            }
            return TblMnlogs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblMnnote> getMnCaseNotes(String Mncode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblMnnote> TblMnnotes = tblMnnoteRepo.findNotesOnMnbyUserAndCategory(
                Long.valueOf(Mncode), tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (TblMnnotes != null && TblMnnotes.size() > 0) {
            for (TblMnnote TblMnnote : TblMnnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(TblMnnote.getUsercode()).orElse(null);
                TblMnnote.setUserName(tblUser1.getUsername());
                TblMnnote.setSelf(TblMnnote.getUsercode() == tblUser.getUsercode() ? true : false);
            }
            return TblMnnotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblMnmessage> getMnCaseMessages(String Mncode) {
        List<TblMnmessage> TblMnmessageList = tblMnmessageRepo.findByTblMnclaimMnclaimcode(Long.valueOf(Mncode));
        if (TblMnmessageList != null && TblMnmessageList.size() > 0) {
            for (TblMnmessage TblMnmessage : TblMnmessageList) {
                TblUser tblUser = tblUsersRepo.findById(TblMnmessage.getUsercode()).orElse(null);
                TblMnmessage.setUserName(tblUser.getUsername());
            }
            return TblMnmessageList;
        } else {
            return null;
        }
    }

    @Override
    public boolean isMnCaseAllowed(String companycode, String status) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companycode,
                status);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TblMnclaim saveMnRequest(TblMnclaim TblMnclaim) {
        TblMnclaim = tblMnclaimRepo.save(TblMnclaim);
        TblMnlog tblMnlog = new TblMnlog();

        TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(559))
                .orElse(null);


        tblMnlog.setCreatedon(new Date());
        tblMnlog.setDescr(tblStatus.getDescr());
        tblMnlog.setUsercode(TblMnclaim.getCreateuser().toString());
        tblMnlog.setTblMnclaim(TblMnclaim);
        tblMnlog = tblMnlogRepo.save(tblMnlog);
        return TblMnclaim;
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public TblMnnote addTblMnNote(TblMnnote tblMnnote) {
        tblMnnote = tblMnnoteRepo.save(tblMnnote);
        TblMnclaim tblMnclaim = tblMnclaimRepo.findById(tblMnnote.getTblMnclaim().getMnclaimcode()).orElse(null);
        TblUser legalUser = tblUsersRepo.findByUsercode("2");
        // for legal internal

        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("note");
        String legalbody = tblEmailTemplate.getEmailtemplate();

        legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
        legalbody = legalbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
        legalbody = legalbody.replace("[CLIENT_NAME]",
                tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
        legalbody = legalbody.replace("[NOTE]", tblMnnote.getNote());
        legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

        saveEmail(legalUser.getUsername(), legalbody, tblMnclaim.getMncode() + " | Processing Note",
                tblMnclaim, legalUser);

        if (!tblMnnote.getUsercategorycode().equals("4")) {
            // for introducer
            if (tblMnnote.getUsercategorycode().equals("1")) {
                TblUser introducerUser = tblUsersRepo
                        .findByUsercode(String.valueOf(tblMnclaim.getAdvisor()));
                String introducerBody = tblEmailTemplate.getEmailtemplate();

                introducerBody = introducerBody.replace("[USER_NAME]", introducerUser.getLoginid());
                introducerBody = introducerBody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
                introducerBody = introducerBody.replace("[CLIENT_NAME]",
                        tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
                introducerBody = introducerBody.replace("[NOTE]", tblMnnote.getNote());
                introducerBody = introducerBody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

                saveEmail(introducerUser.getUsername(), introducerBody,
                        tblMnclaim.getMncode() + " | Processing Note", tblMnclaim,
                        introducerUser);
            } else if (tblMnnote.getUsercategorycode().equals("2")) {

                TblMnsolicitor tblMnsolicitor = tblMnsolicitorRepo
                        .findByTblMnclaimMnclaimcodeAndStatus(tblMnclaim.getMnclaimcode(), "Y");

                if (tblMnsolicitor != null) {
                    TblUser solicitorUser = tblUsersRepo.findById(tblMnsolicitor.getUsercode()).orElse(null);

                    String solicitorBody = tblEmailTemplate.getEmailtemplate();

                    solicitorBody = solicitorBody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorBody = solicitorBody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
                    solicitorBody = solicitorBody.replace("[CLIENT_NAME]",
                            tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
                    solicitorBody = solicitorBody.replace("[NOTE]", tblMnnote.getNote());
                    solicitorBody = solicitorBody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

                    saveEmail(solicitorUser.getUsername(), solicitorBody,
                            tblMnclaim.getMncode() + " | Processing Note", tblMnclaim,
                            solicitorUser);

                }
            }

        }

        return tblMnnote;
    }


    @Override
    public TblMnmessage getTblMnMessageById(String Mnmessagecode) {
        return tblMnmessageRepo.findById(Long.valueOf(Mnmessagecode)).orElse(null);
    }

    @Override
    public TblEmail saveTblEmail(TblEmail tblEmail) {
        return tblEmailRepo.save(tblEmail);
    }

   /* @Override
    public TblMnclaim assignCaseToSolicitor(AssignMnCasetoSolicitor assignMnCasetoSolicitor, TblUser tblUser) {
        int update = tblMnclaimRepo.performAction(new BigDecimal("41"), Long.valueOf(assignMnCasetoSolicitor.getMnClaimCode()));
        if (update > 0) {

            TblMnclaim TblMnclaim1 = new TblMnclaim();
            TblMnlog TblMnlog = new TblMnlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(41))
                    .orElse(null);
            TblMnclaim1.setMnclaimcode(Long.valueOf(assignMnCasetoSolicitor.getMnClaimCode()));

            TblMnlog.setCreatedon(new Date());
            TblMnlog.setDescr(tblStatus.getDescr());
            TblMnlog.setUsercode(tblUser.getUsercode());
            TblMnlog.setTblMnclaim(TblMnclaim1);

            int updateSolicitor = tblMnsolicitorRepo.updateSolicitor("N", Long.valueOf(assignMnCasetoSolicitor.getMnClaimCode()));

            TblMnsolicitor TblMnsolicitor = new TblMnsolicitor();
            TblMnsolicitor.setCompanycode(assignMnCasetoSolicitor.getSolicitorUserCode());
            TblMnsolicitor.setCreatedon(new Date());
            TblMnsolicitor.setTblMnclaim(TblMnclaim1);
            TblMnsolicitor.setStatus("Y");
            TblMnsolicitor.setUsercode(assignMnCasetoSolicitor.getSolicitorCode());

            TblMnsolicitor = tblMnsolicitorRepo.save(TblMnsolicitor);
            TblMnlog = tblMnlogRepo.save(TblMnlog);

            TblMnclaim TblMnclaim = tblMnclaimRepo.findById(TblMnclaim1.getMnclaimcode()).orElse(null);

            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + TblMnclaim.getMncode());
            tblEmail.setEmailsubject("HDR CASE" + TblMnclaim.getMncode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            TblMnmessage TblMnmessage = new TblMnmessage();

            TblMnmessage.setTblMnclaim(TblMnclaim1);
            TblMnmessage.setUserName(tblUser.getUsername());
            TblMnmessage.setCreatedon(new Date());
            TblMnmessage.setMessage(tblEmail.getEmailbody());
            TblMnmessage.setSentto(tblEmail.getEmailaddress());
            TblMnmessage.setUsercode(tblUser.getUsercode());

            TblMnmessage = tblMnmessageRepo.save(TblMnmessage);
            return tblMnclaimRepo.findById(Long.valueOf(assignMnCasetoSolicitor.getMnClaimCode())).orElse(null);

        } else {
            return null;
        }
    }*/

    @Override
    public TblMnclaim performRejectCancelActionOnMn(String MnClaimCode, String toStatus, String reason, TblUser tblUser) {
        TblMnclaim tblMnclaim = tblMnclaimRepo.findById(Long.valueOf(MnClaimCode)).orElse(null);
        TblMnsolicitor tblMnsolicitor = tblMnsolicitorRepo.findByTblMnclaimMnclaimcodeAndStatus(Long.valueOf(MnClaimCode), "Y");
        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status-Reject");
        TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

        if (tblMnsolicitor != null) {
            int update = tblMnsolicitorRepo.updateRemarksReason(reason, "N", Long.parseLong(MnClaimCode));


            TblUser SolicitorUser = tblUsersRepo.findById(tblMnsolicitor.getUsercode()).orElse(null);
            TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblMnsolicitor.getCompanycode())
                    .orElse(null);

            TblMnnote tblMnnote = new TblMnnote();
            // NOte for Legal internal user and NOTE for introducer
            tblMnnote.setTblMnclaim(tblMnclaim);
            tblMnnote.setNote(SolicitorCompany.getName() + " has rejected the case. Reason :" + reason);
            tblMnnote.setUsercategorycode("1");
            tblMnnote.setCreatedon(new Date());
            tblMnnote.setUsercode("1");

            addTblMnNote(tblMnnote);

            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            // Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(tblMnclaim.getAdvisor())).orElse(null);

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
            introducerbody = introducerbody.replace("[SOLICITOR_NAME]", SolicitorCompany.getName());
            introducerbody = introducerbody.replace("[REASON]", reason);
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

            saveEmail(introducerUser.getUsername(), introducerbody, tblMnclaim.getMnclaimcode() + " | " + tblStatus.getDescr(),
                    tblMnclaim, introducerUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
            legalbody = legalbody.replace("[SOLICITOR_NAME]", SolicitorCompany.getName());
            legalbody = legalbody.replace("[REASON]", reason);
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

            saveEmail(legalUser.getUsername(), legalbody, tblMnclaim.getMnclaimcode() + " | " + tblStatus.getDescr(),
                    tblMnclaim, introducerUser);

        } else {

            TblMnnote tblMnnote = new TblMnnote();
            // NOte for Legal internal user and NOTE for introducer
            tblMnnote.setTblMnclaim(tblMnclaim);
            tblMnnote.setNote(tblUser.getLoginid() + " has rejected the case. Reason :" + reason);
            tblMnnote.setUsercategorycode("1");
            tblMnnote.setCreatedon(new Date());
            tblMnnote.setUsercode("1");

            addTblMnNote(tblMnnote);

            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(tblMnclaim.getAdvisor())).orElse(null);

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
            introducerbody = introducerbody.replace("[SOLICITOR_NAME]", tblUser.getLoginid());
            introducerbody = introducerbody.replace("[REASON]", reason);
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

            saveEmail(introducerUser.getUsername(), introducerbody, tblMnclaim.getMnclaimcode() + " | " + tblStatus.getDescr(),
                    tblMnclaim, introducerUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
            legalbody = legalbody.replace("[SOLICITOR_NAME]", tblUser.getLoginid());
            legalbody = legalbody.replace("[REASON]", reason);
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

            saveEmail(legalUser.getUsername(), legalbody, tblMnclaim.getMnclaimcode() + " | " + tblStatus.getDescr(),
                    tblMnclaim, introducerUser);
        }

        TblMnlog tblMnlog = new TblMnlog();


        tblMnlog.setCreatedon(new Date());
        tblMnlog.setDescr(tblStatus.getDescr());
        tblMnlog.setUsercode(tblUser.getUsercode());
        tblMnlog.setTblMnclaim(tblMnclaim);
        tblMnlog.setOldstatus(tblMnclaim.getStatus().longValue());
        tblMnlog.setNewstatus(Long.valueOf(toStatus));

        tblMnlog = tblMnlogRepo.save(tblMnlog);
        int update = tblMnclaimRepo.performRejectCancelAction(reason, toStatus, tblMnclaim.getMnclaimcode(), tblUser.getUsercode());

        return tblMnclaim;
    }


    @Override
    public TblMnclaim performActionOnMn(String mnClaimCode, String toStatus, TblUser tblUser) {
        int update = tblMnclaimRepo.performAction(new BigDecimal(toStatus), Long.valueOf(mnClaimCode), tblUser.getUsercode());
        if (update > 0) {

            TblMnclaim tblMnclaim = tblMnclaimRepo.findById(Long.valueOf(mnClaimCode)).orElse(null);
            TblMnlog tblMnlog = new TblMnlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblMnlog.setCreatedon(new Date());
            tblMnlog.setDescr(tblStatus.getDescr());
            tblMnlog.setUsercode(tblUser.getUsercode());
            tblMnlog.setTblMnclaim(tblMnclaim);
            tblMnlog.setOldstatus(tblMnclaim.getStatus().longValue());
            tblMnlog.setNewstatus(Long.valueOf(toStatus));

            tblMnlog = tblMnlogRepo.save(tblMnlog);
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status");

            // for introducer
            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblMnclaim.getAdvisor()));
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, introducerUser);

            // for legal internal
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
            legalbody = legalbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, legalUser);

            if (toStatus.equals(559)) {
                // when reverting a case takes to new status remove all solcitors from case if
                // any
                int updateSolicitor = tblMnsolicitorRepo.updateSolicitor("N", Long.parseLong(mnClaimCode));
                TblMnsolicitor tblMnsolicitor = tblMnsolicitorRepo
                        .findByTblMnclaimMnclaimcodeAndStatus(tblMnclaim.getMnclaimcode(), "Y");
                if (tblMnsolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblMnsolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            } else {
                // for normal emails
                TblMnsolicitor tblRtasolicitor = tblMnsolicitorRepo
                        .findByTblMnclaimMnclaimcodeAndStatus(tblMnclaim.getMnclaimcode(), "Y");
                if (tblRtasolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblRtasolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            }

            // if submmitting a case add clawback date current date + 90 days
            TblMnclaim tblMnclaimForClawBack = tblMnclaimRepo.findById(tblMnclaim.getMnclaimcode()).orElse(null);
            if (toStatus.equals("565")) {
                Date clawbackDate = addDaysToDate(90);
                tblMnclaimRepo.updateClawbackdate(clawbackDate, tblMnclaim.getMnclaimcode());
                tblMnclaimRepo.updateSubmitDate(new Date(), tblMnclaim.getMnclaimcode());
            } else if (toStatus.equals("576")) {
                Date clawbackDate = addDaysToSpecificDate(tblMnclaimForClawBack.getClawbackDate(), 30);
                tblMnclaimRepo.updateClawbackdate(clawbackDate, tblMnclaim.getMnclaimcode());
            } else if (toStatus.equals("578")) {
                Date clawbackDate = addDaysToSpecificDate(tblMnclaimForClawBack.getClawbackDate(), 60);
                tblMnclaimRepo.updateClawbackdate(clawbackDate, tblMnclaim.getMnclaimcode());
            } else if (toStatus.equals("579")) {
                Date clawbackDate = addDaysToSpecificDate(tblMnclaimForClawBack.getClawbackDate(), 90);
                tblMnclaimRepo.updateClawbackdate(clawbackDate, tblMnclaim.getMnclaimcode());
            } else if (toStatus.equals("563")) {
                TblMnnote tblMnnote = new TblMnnote();
                TblMnsolicitor tblMnsolicitor = tblMnsolicitorRepo
                        .findByTblMnclaimMnclaimcodeAndStatus(tblMnclaim.getMnclaimcode(), "Y");
                TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblMnsolicitor.getCompanycode())
                        .orElse(null);
                // NOte for Legal internal user and NOTE for introducer
                tblMnnote.setTblMnclaim(tblMnclaim);
                tblMnnote.setNote("Case Accepted By " + SolicitorCompany.getName());
                tblMnnote.setUsercategorycode("1");
                tblMnnote.setCreatedon(new Date());
                tblMnnote.setUsercode("1");

                addTblMnNote(tblMnnote);
            }
            return tblMnclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblMnclaim findMnCaseByIdWithoutUser(long rtaCode) {
        return tblMnclaimRepo.findById(rtaCode).orElse(null);
    }

    @Override
    public TblCompanyprofile getCompanyProfileAgainstMnCode(long Mnclaimcode) {
        return tblCompanyprofileRepo.getCompanyProfileAgainstMnCode(Mnclaimcode);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndTblCompaignCompaigncode(companycode, "11");
    }

    @Override
    public String getMnDbColumnValue(String key, long Mnclaimcode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from TBL_mnCLAIM  WHERE mnclaimcode = " + Mnclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT FIRSTNAME || ' ' || NVL(MIDDLENAME,'') || ' ' || NVL(LASTNAME, '') from TBL_mnCLAIM  WHERE mnclaimcode = " + Mnclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT POSTALCODE || ' ' || ADDRESS1 || ' ' || ADDRESS2  || ' ' || ADDRESS3 || ' ' || CITY || ' ' || REGION from TBL_mnCLAIM  WHERE mnclaimcode = " + Mnclaimcode;
            query = em.createNativeQuery(sql);
        } else {
            String sql = "SELECT " + key + " from TBL_mnCLAIM  WHERE mnclaimcode = " + Mnclaimcode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")
        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    @Override
    public TblMndocument saveTblMndocument(TblMndocument TblMndocument) {
        return tblMndocumentRepo.save(TblMndocument);
    }

    @Override
    public TblMntask getMnTaskByMnCodeAndTaskCode(long Mnclaimcode, long taskCode) {
        return tblMntaskRepo.findByTblMnclaimMnclaimcodeAndTaskcode(Mnclaimcode, new BigDecimal(taskCode));
    }

    @Override
    public int updateTblMntask(TblMntask TblMntask) {
        return tblMntaskRepo.updatetask(TblMntask.getStatus(), TblMntask.getRemarks(), TblMntask.getTaskcode().longValue(), TblMntask.getTblMnclaim().getMnclaimcode());
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public List<TblMndocument> saveTblMndocuments(List<TblMndocument> TblMndocuments) {
        return tblMndocumentRepo.saveAll(TblMndocuments);
    }

    @Override
    public List<TblMntask> findTblMntaskByMnClaimCode(long Mnclaimcode) {
        return tblMntaskRepo.findByTblMnclaimMnclaimcode(Mnclaimcode);
    }


    @Override
    public TblMnclaim updateMnCase(TblMnclaim tblMnclaim) {
        return tblMnclaimRepo.saveAndFlush(tblMnclaim);
    }

    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblMnclaim tblMnclaim,
                             TblUser tblUser) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblMnmessage tblMnmessage = new TblMnmessage();

        tblMnmessage.setTblMnclaim(tblMnclaim);
        tblMnmessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblMnmessage.setCreatedon(new Date());
        tblMnmessage.setMessage(tblEmail.getEmailbody());
        tblMnmessage.setSentto(tblEmail.getEmailaddress());
        tblMnmessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblMnmessage.setEmailcode(tblEmail);

        tblMnmessage = tblMnmessageRepo.save(tblMnmessage);

        return true;
    }


    @Override
    public TblEmail resendEmail(String mnMessagecode) {
        TblMnmessage tblMnmessage = tblMnmessageRepo.findById(Long.valueOf(mnMessagecode)).orElse(null);

        TblEmail tblEmail = tblEmailRepo.findById(tblMnmessage.getEmailcode().getEmailcode()).orElse(null);

        tblEmail.setSenflag(new BigDecimal(0));

        return tblEmailRepo.saveAndFlush(tblEmail);
    }

    @Override
    public TblMnclaim performActionOnMnFromDirectIntro(PerformHotKeyOnRtaRequest performHotKeyOnMnRequest, TblUser tblUser) {
        TblMnclaim tblMnclaim = tblMnclaimRepo.findById(Long.valueOf(performHotKeyOnMnRequest.getMncode()))
                .orElse(null);

        int update = tblMnclaimRepo.performAction(new BigDecimal(performHotKeyOnMnRequest.getToStatus()),
                Long.valueOf(performHotKeyOnMnRequest.getMncode()), tblUser.getUsercode());
        if (update > 0) {

            TblMnlog tblMnlog = new TblMnlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(performHotKeyOnMnRequest.getToStatus()))
                    .orElse(null);

            tblMnlog.setCreatedon(new Date());
            tblMnlog.setDescr(tblStatus.getDescr());
            tblMnlog.setUsercode(tblUser.getUsercode());
            tblMnlog.setTblMnclaim(tblMnclaim);
            tblMnlog.setOldstatus(tblMnclaim.getStatus().longValue());
            tblMnlog.setNewstatus(Long.valueOf(performHotKeyOnMnRequest.getToStatus()));

            int updateSolicitor = tblMnsolicitorRepo.updateSolicitor("N", tblMnclaim.getMnclaimcode());

            TblMnsolicitor tblMnsolicitor = new TblMnsolicitor();
            tblMnsolicitor.setCompanycode(performHotKeyOnMnRequest.getSolicitorCode());
            tblMnsolicitor.setCreatedon(new Date());
            tblMnsolicitor.setTblMnclaim(tblMnclaim);
            tblMnsolicitor.setStatus("Y");
            tblMnsolicitor.setUsercode(performHotKeyOnMnRequest.getSolicitorUserCode());

            tblMnsolicitor = tblMnsolicitorRepo.save(tblMnsolicitor);
            tblMnlog = tblMnlogRepo.save(tblMnlog);

            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblMnclaim.getAdvisor()));
            TblUser solicitorUser = tblUsersRepo
                    .findByUsercode(String.valueOf(performHotKeyOnMnRequest.getSolicitorUserCode()));
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status-hotkey");

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
//                    tblMnclaim.getFirstname() + " "
//                            + (tblMnclaim.getMiddlename() == null ? "" : tblMnclaim.getMiddlename() + " ")
//                            + tblMnclaim.getLastname());
//            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    tblMnclaim.getRtanumber() + " | HotKey", tblMnclaim, introducerUser);

            // for solicitor
            String solicitorbody = tblEmailTemplate.getEmailtemplate();

            solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
            solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
            solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
//                    tblMnclaim.getFirstname() + " "
//                            + (tblMnclaim.getMiddlename() == null ? "" : tblMnclaim.getMiddlename() + " ")
//                            + tblMnclaim.getLastname());
//            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

            saveEmail(solicitorUser.getUsername(), solicitorbody, tblMnclaim.getMncode() + " | Processing Note",
                    tblMnclaim, solicitorUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() + (tblMnclaim.getMiddlename() != null ? tblMnclaim.getMiddlename() : "") + " " + tblMnclaim.getLastname());
//                    tblMnclaim.getFirstname() + " "
//                            + (tblMnclaim.getMiddlename() == null ? "" : tblMnclaim.getMiddlename() + " ")
//                            + tblMnclaim.getLastname());

            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblMnclaim.getMnclaimcode());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    tblMnclaim.getRtanumber() + " | HotKey", tblMnclaim, legalUser);


            return tblMnclaim;

        } else {
            return null;
        }
    }

    @Override
    public List<TblMntask> getMnTaskAgainstELCodeAndStatus(String mncode, String status) {
        return tblMntaskRepo.findByTblMnclaimMnclaimcodeAndStatus(Long.parseLong(mncode), status);
    }

    @Override
    public TblMnclaim performRevertActionOnMn(String mncode, String toStatus, String reason, TblUser tblUser) {
        return null;
    }

    @Override
    public List<TblMndocument> getAuthMnCasedocuments(long mnclaimcode) {
        return tblMndocumentRepo.findByTblMnclaimMnclaimcode(mnclaimcode);
    }

    @Override
    public TblMndocument addMnSingleDocumentSingle(TblMndocument tblMndocument) {
        return tblMndocumentRepo.save(tblMndocument);
    }

    @Override
    public TblUser findByUserId(String userCode) {
        return tblUsersRepo.findByUsercode(userCode);
    }

    @Override
    public TblEmailTemplate findByEmailType(String emailType) {
        return tblEmailTemplateRepo.findByEmailtype(emailType);
    }

    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblMnclaim tblMnclaim, TblUser tblUser, String attachment) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setEmailattachment(attachment);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblMnmessage tblMnmessage = new TblMnmessage();

        tblMnmessage.setTblMnclaim(tblMnclaim);
        tblMnmessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblMnmessage.setCreatedon(new Date());
        tblMnmessage.setMessage(tblEmail.getEmailbody());
        tblMnmessage.setSentto(tblEmail.getEmailaddress());
        tblMnmessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblMnmessage.setEmailcode(tblEmail);

        tblMnmessage = tblMnmessageRepo.save(tblMnmessage);

        return true;
    }

    @Override
    public TblMnsolicitor getMnSolicitorsOfMnClaim(long mnClaimCode) {
        return tblMnsolicitorRepo.findByTblMnclaimMnclaimcodeAndStatus(mnClaimCode, "Y");
    }

    @Override
    public int updateCurrentTask(long taskCode, long mnCode, String current) {
        tblMntaskRepo.setAllTask("N", mnCode);
        return tblMntaskRepo.setCurrentTask(current, taskCode, mnCode);
    }

    @Override
    public void deleteMnDocument(long doccode) {
        tblMndocumentRepo.deleteById(doccode);
    }

    @Override
    public List<MNCaseList> getAuthMnCasesUserAndCategoryWise(String usercode, String categorycode) {
        List<MNCaseList> MNCaseLists = new ArrayList<>();
        List<ViewMnclaim> MnCasesListForIntroducers = viewMnclaimRepo.getAuthMnCasesUserAndCategoryWise(new BigDecimal(usercode), new BigDecimal(categorycode));
        MNCaseList MNCaseList;
        if (MnCasesListForIntroducers != null && MnCasesListForIntroducers.size() > 0) {
            for (ViewMnclaim record : MnCasesListForIntroducers) {
                MNCaseList = new MNCaseList();


                MNCaseList.setMnClaimCode(record.getMnclaimcode());
                MNCaseList.setCreated(record.getCreatedate());
                MNCaseList.setCode(record.getMncode());
                MNCaseList.setClient(record.getClientname());
//                MNCaseList.setTaskDue(record.get);
                MNCaseList.setTaskName(record.getMntaskname());
                MNCaseList.setStatus(record.getStatus());
                MNCaseList.setEmail(record.getClientemail());
                MNCaseList.setAddress(record.getClientaddress());
                MNCaseList.setContactNo(record.getClientcontactno());
                MNCaseList.setLastUpdated(record.getUpdatedate());
                MNCaseList.setIntroducer(record.getIntroducerUser());
//                MNCaseList.setLastNote((Date) row[12]);
                MNCaseLists.add(MNCaseList);
            }
        }
        if (MNCaseLists != null && MNCaseLists.size() > 0) {
            return MNCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblMnnote> getAuthMnCaseNotesOfLegalInternal(String mNcode, TblUser tblUser) {
        List<TblMnnote> tblMnnotes = tblMnnoteRepo.getAuthMnCaseNotesOfLegalInternal(Long.valueOf(mNcode),
                tblUser.getUsercode());
        if (tblMnnotes != null && tblMnnotes.size() > 0) {
            for (TblMnnote tblMnnote : tblMnnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblMnnote.getUsercode()).orElse(null);
                tblMnnote.setUserName(tblUser1.getLoginid());
                tblMnnote.setSelf(tblMnnote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
            }

            return tblMnnotes;
        } else {
            return null;
        }
    }

    @Override
    public List<RtaAuditLogResponse> getMnAuditLogs(Long mnCode) {
        List<RtaAuditLogResponse> rtaAuditLogResponses = new ArrayList<>();
        RtaAuditLogResponse rtaAuditLogResponse = null;
        List<Object> auditlogsObject = tblMnclaimRepo.getMnAuditLogs(mnCode);
        RtaStatusCountList PlStatusCountList;
        if (auditlogsObject != null && auditlogsObject.size() > 0) {
            for (Object record : auditlogsObject) {
                rtaAuditLogResponse = new RtaAuditLogResponse();
                Object[] row = (Object[]) record;

                rtaAuditLogResponse.setFieldName((String) row[0]);
                rtaAuditLogResponse.setOldValue((String) row[1]);
                rtaAuditLogResponse.setNewValue((String) row[2]);
                rtaAuditLogResponse.setAuditDate((Date) row[3]);
                rtaAuditLogResponse.setLoggedUser((String) row[4]);

                rtaAuditLogResponses.add(rtaAuditLogResponse);
            }
            return rtaAuditLogResponses;
        } else {
            return null;
        }
    }
}
