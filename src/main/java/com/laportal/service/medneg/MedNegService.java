package com.laportal.service.medneg;

import com.laportal.dto.MNCaseList;
import com.laportal.dto.MnStatusCountList;
import com.laportal.dto.PerformHotKeyOnRtaRequest;
import com.laportal.dto.RtaAuditLogResponse;
import com.laportal.model.*;

import java.util.List;

public interface MedNegService {

    TblMnclaim findMnCaseById(long MnCaseId, TblUser tblUser);

    TblCompanyprofile getCompanyProfile(String companycode);

    List<MnStatusCountList> getAllMnStatusCounts(String companycode, String campaignCode);

    List<MNCaseList> getMnCasesByStatus(String usercode, String status, String categorycode);

    List<TblMnlog> getMnCaseLogs(String Mncode);

    List<TblMnnote> getMnCaseNotes(String Mncode, TblUser tblUser);

    List<TblMnmessage> getMnCaseMessages(String Mncode);

    boolean isMnCaseAllowed(String companycode, String s);

    TblMnclaim saveMnRequest(TblMnclaim TblMnclaim);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    TblMnnote addTblMnNote(TblMnnote tblMnnote);

    TblMnmessage getTblMnMessageById(String Mnmessagecode);

    TblEmail saveTblEmail(TblEmail tblEmail);

//    TblMnclaim assignCaseToSolicitor(AssignMnCasetoSolicitor assignMnCasetoSolicitor, TblUser tblUser);

    TblMnclaim performRejectCancelActionOnMn(String MnClaimCode, String toStatus, String reason, TblUser tblUser);

    TblMnclaim performActionOnMn(String MnClaimCode, String toStatus, TblUser tblUser);

    TblMnclaim findMnCaseByIdWithoutUser(long rtaCode);

    TblCompanyprofile getCompanyProfileAgainstMnCode(long Mnclaimcode);

    TblCompanydoc getCompanyDocs(String companycode);

    String getMnDbColumnValue(String key, long Mnclaimcode);

    TblMndocument saveTblMndocument(TblMndocument TblMndocument);

    TblMntask getMnTaskByMnCodeAndTaskCode(long Mnclaimcode, long taskCode);

    int updateTblMntask(TblMntask TblMntask);

    TblTask getTaskAgainstCode(long taskCode);

    List<TblMndocument> saveTblMndocuments(List<TblMndocument> TblMndocuments);

    List<TblMntask> findTblMntaskByMnClaimCode(long Mnclaimcode);


    TblMnclaim updateMnCase(TblMnclaim tblMnclaim);

    boolean saveEmail(String username, String legalbody, String s, TblMnclaim tblMnclaim, TblUser legalUser);

    TblEmail resendEmail(String mnMessagecode);

    TblMnclaim performActionOnMnFromDirectIntro(PerformHotKeyOnRtaRequest performHotKeyOnElRequest, TblUser tblUser);

    List<TblMntask> getMnTaskAgainstELCodeAndStatus(String mncode, String status);

    TblMnclaim performRevertActionOnMn(String mncode, String toStatus, String reason, TblUser tblUser);

    List<TblMndocument> getAuthMnCasedocuments(long mnclaimcode);

    TblMndocument addMnSingleDocumentSingle(TblMndocument tblMndocument);

    TblUser findByUserId(String userCode);

    TblEmailTemplate findByEmailType(String emailType);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblMnclaim tblMnclaim,
                   TblUser tblUser, String attachment);

    TblMnsolicitor getMnSolicitorsOfMnClaim(long mnClaimCode);

    int updateCurrentTask(long taskCode, long mnCode, String current);

    void deleteMnDocument(long doccode);

    List<MNCaseList> getAuthMnCasesUserAndCategoryWise(String usercode, String categorycode);

    List<TblMnnote> getAuthMnCaseNotesOfLegalInternal(String mNcode, TblUser tblUser);

    List<RtaAuditLogResponse> getMnAuditLogs(Long mnCode);
}
