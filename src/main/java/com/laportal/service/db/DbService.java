package com.laportal.service.db;

import com.laportal.dto.AssignDbCasetoSolicitor;
import com.laportal.dto.DbCaseList;
import com.laportal.dto.DbStatusCountList;
import com.laportal.model.*;

import java.util.List;

public interface DbService {

    TblDbclaim findDbCaseById(long DbCaseId, TblUser tblUser);

    TblCompanyprofile getCompanyProfile(String companycode);

    List<DbStatusCountList> getAllDbStatusCounts();

    List<DbCaseList> getDbCasesByStatus(long statusId);

    List<TblDblog> getDbCaseLogs(String Dbcode);

    List<TblDbnote> getDbCaseNotes(String Dbcode, TblUser tblUser);

    List<TblDbmessage> getDbCaseMessages(String Dbcode);

    boolean isHdrCaseAllowed(String companycode, String s);

    TblDbclaim saveDbRequest(TblDbclaim tblDbclaim);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    TblDbnote addTblDbNote(TblDbnote tblDbnote);

    TblDbmessage getTblDbMessageById(String Dbmessagecode);

    TblEmail saveTblEmail(TblEmail tblEmail);

    TblDbclaim assignCaseToSolicitor(AssignDbCasetoSolicitor assignDbCasetoSolicitor, TblUser tblUser);

    TblDbclaim performRejectCancelActionOnDb(String DbClaimCode, String toStatus, String reason, TblUser tblUser);

    TblDbclaim performActionOnDb(String DbClaimCode, String toStatus, TblUser tblUser);

    TblDbclaim findDbCaseByIdWithoutUser(long rtaCode);

    TblCompanyprofile getCompanyProfileAgainstDbCode(long Dbclaimcode);

    TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType);

    String getDbDbColumnValue(String key, long Dbclaimcode);

    TblDbdocument saveTblDbDocument(TblDbdocument tblDbdocument);

    TblDbtask getDbTaskByDbCodeAndTaskCode(long Dbclaimcode, long taskCode);

    TblDbtask updateTblDbTask(TblDbtask tblDbtask);

    TblTask getTaskAgainstCode(long taskCode);

    List<TblDbdocument> saveTblDbDocuments(List<TblDbdocument> tblDbdocuments);

    List<TblDbtask> findTblDbTaskByDbClaimCode(long Dbclaimcode);

    List<DbCaseList> getAuthDbCasesIntroducers(String usercode);

    List<DbCaseList> getAuthDbCasesSolicitors(String usercode);

    List<DbCaseList> getAuthDbCasesLegalAssist();

    List<DbStatusCountList> getAllDbStatusCountsForIntroducers();

    List<DbStatusCountList> getAllDbStatusCountsForSolicitor();
}
