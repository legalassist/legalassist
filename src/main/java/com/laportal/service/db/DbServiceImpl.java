package com.laportal.service.db;

import com.laportal.Repo.*;
import com.laportal.dto.AssignDbCasetoSolicitor;
import com.laportal.dto.DbCaseList;
import com.laportal.dto.DbStatusCountList;
import com.laportal.dto.HdrActionButton;
import com.laportal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class DbServiceImpl implements DbService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;

    @Autowired
    private TblDbclaimRepo tblDbclaimRepo;

    @Autowired
    private TblDblogRepo tblDblogRepo;

    @Autowired
    private TblDbmessageRepo tblDbmessageRepo;

    @Autowired
    private TblDbnoteRepo tblDbnoteRepo;

    @Autowired
    private TblDbsolicitorRepo tblDbsolicitorRepo;

    @Autowired
    private TblTaskRepo tblTaskRepo;

    @Autowired
    private TblUsersRepo tblUsersRepo;

    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;

    @Autowired
    private TblEmailRepo tblEmailRepo;

    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;

    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;

    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;

    @Autowired
    private TblDbdocumentRepo tblDbdocumentRepo;

    @Autowired
    private TblDbtaskRepo tblDbtaskRepo;

    @Override
    public TblDbclaim findDbCaseById(long DbCaseId, TblUser tblUser) {
        TblDbclaim tblDbclaim = tblDbclaimRepo.findById(DbCaseId).orElse(null);
        if (tblDbclaim != null) {
            TblRtastatus tblRtastatus = tblRtastatusRepo.findById(Long.valueOf(tblDbclaim.getStatus().toString())).orElse(null);
            tblDbclaim.setStatusDescr(tblRtastatus.getDescr());
            List<HdrActionButton> rtaActionButtons = new ArrayList<>();
            List<HdrActionButton> rtaActionButtonsForLA = new ArrayList<>();
            HdrActionButton hdrActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);

            List<TblRtaflowdetail> tblRtaflowdetailForLA = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    Long.valueOf(tblDbclaim.getStatus().toString()), "3", tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    Long.valueOf(tblDbclaim.getStatus().toString()), "3", tblCompanyprofile.getTblUsercategory().getCategorycode());
            TblDbsolicitor tblTenancysolicitor = tblDbsolicitorRepo.findByTblDbclaimDbclaimcodeAndStatus(DbCaseId, "Y");
            if (tblTenancysolicitor != null) {
                List<TblDbsolicitor> tblTenancysolicitors = new ArrayList<>();
                tblTenancysolicitors.add(tblTenancysolicitor);
                tblDbclaim.setTblDbsolicitors(tblTenancysolicitors);
            } else {
                tblDbclaim.setTblDbsolicitors(null);
            }
            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    hdrActionButton = new HdrActionButton();
                    hdrActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hdrActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hdrActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hdrActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    hdrActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());
                    hdrActionButton.setAcceptDialog(tblRtaflowdetail.getCaseacceptdialog());

                    rtaActionButtons.add(hdrActionButton);
                }
                tblDbclaim.setHdrActionButton(rtaActionButtons);
            }
            if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                    hdrActionButton = new HdrActionButton();
                    hdrActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hdrActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hdrActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hdrActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    hdrActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());
                    hdrActionButton.setAcceptDialog(tblRtaflowdetail.getCaseacceptdialog());

                    rtaActionButtonsForLA.add(hdrActionButton);
                }
                tblDbclaim.setHdrActionButtonForLA(rtaActionButtonsForLA);
            }
            return tblDbclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public List<DbStatusCountList> getAllDbStatusCounts() {
        List<DbStatusCountList> DbStatusCountLists = new ArrayList<>();
        List<Object> DbStatusCountListObject = tblDbclaimRepo.getAllDbStatusCounts();
        DbStatusCountList DbStatusCountList;
        if (DbStatusCountListObject != null && DbStatusCountListObject.size() > 0) {
            for (Object record : DbStatusCountListObject) {
                DbStatusCountList = new DbStatusCountList();
                Object[] row = (Object[]) record;

                DbStatusCountList.setStatusCount((BigDecimal) row[0]);
                DbStatusCountList.setStatusName((String) row[1]);
                DbStatusCountList.setStatusCode((BigDecimal) row[2]);

                DbStatusCountLists.add(DbStatusCountList);
            }
        }
        if (DbStatusCountLists != null && DbStatusCountLists.size() > 0) {
            return DbStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<DbCaseList> getDbCasesByStatus(long statusId) {
        List<DbCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListObject = tblDbclaimRepo.getDbCasesListStatusWise(statusId);
        DbCaseList DbCaseList;
        if (DbCasesListObject != null && DbCasesListObject.size() > 0) {
            for (Object record : DbCasesListObject) {
                DbCaseList = new DbCaseList();
                Object[] row = (Object[]) record;

                DbCaseList.setDbClaimCode((BigDecimal) row[0]);
                DbCaseList.setCreated((String) row[1]);
                DbCaseList.setCode((String) row[2]);
                DbCaseList.setClient((String) row[3]);
//                DbCaseList.setTaskDue((String) row[4]);
//                DbCaseList.setTaskName((String) row[5]);
                DbCaseList.setStatus((String) row[4]);
//                DbCaseList.setEmail((String) row[7]);
//                DbCaseList.setAddress((String) row[8]);
//                DbCaseList.setContactNo((String) row[9]);
//                DbCaseList.setLastUpdated((Date) row[10]);
//                DbCaseList.setIntroducer((String) row[11]);
//                DbCaseList.setLastNote((Date) row[12]);

                DbCaseLists.add(DbCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblDblog> getDbCaseLogs(String Dbcode) {
        List<TblDblog> tblDblogs = tblDblogRepo.findByTblDbclaimDbclaimcode(Long.valueOf(Dbcode));
        if (tblDblogs != null && tblDblogs.size() > 0) {
            for (TblDblog tblDblog : tblDblogs) {
                TblUser tblUser = tblUsersRepo.findById(tblDblog.getUsercode()).orElse(null);
                tblDblog.setUserName(tblUser.getUsername());
            }
            return tblDblogs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblDbnote> getDbCaseNotes(String Dbcode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblDbnote> tblDbnotes = tblDbnoteRepo.findNotesOnDbbyUserAndCategory(
                Long.valueOf(Dbcode), tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (tblDbnotes != null && tblDbnotes.size() > 0) {
            for (TblDbnote tblDbnote : tblDbnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblDbnote.getUsercode()).orElse(null);
                tblDbnote.setUserName(tblUser1.getUsername());
                tblDbnote.setSelf(tblDbnote.getUsercode() == tblUser.getUsercode() ? true : false);
            }
            return tblDbnotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblDbmessage> getDbCaseMessages(String Dbcode) {
        List<TblDbmessage> tblDbmessageList = tblDbmessageRepo.findByTblDbclaimDbclaimcode(Long.valueOf(Dbcode));
        if (tblDbmessageList != null && tblDbmessageList.size() > 0) {
            for (TblDbmessage tblDbmessage : tblDbmessageList) {
                TblUser tblUser = tblUsersRepo.findById(tblDbmessage.getUsercode()).orElse(null);
                tblDbmessage.setUserName(tblUser.getUsername());
            }
            return tblDbmessageList;
        } else {
            return null;
        }
    }

    @Override
    public boolean isHdrCaseAllowed(String companycode, String s) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companycode,
                s);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TblDbclaim saveDbRequest(TblDbclaim tblDbclaim) {
        return tblDbclaimRepo.save(tblDbclaim);
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public TblDbnote addTblDbNote(TblDbnote tblDbnote) {
        return tblDbnoteRepo.save(tblDbnote);
    }

    @Override
    public TblDbmessage getTblDbMessageById(String Dbmessagecode) {
        return tblDbmessageRepo.findById(Long.valueOf(Dbmessagecode)).orElse(null);
    }

    @Override
    public TblEmail saveTblEmail(TblEmail tblEmail) {
        return tblEmailRepo.save(tblEmail);
    }

    @Override
    public TblDbclaim assignCaseToSolicitor(AssignDbCasetoSolicitor assignDbCasetoSolicitor, TblUser tblUser) {
        int update = tblDbclaimRepo.performAction(new BigDecimal("41"), Long.valueOf(assignDbCasetoSolicitor.getDbClaimCode()));
        if (update > 0) {

            TblDbclaim tblDbclaim1 = new TblDbclaim();
            TblDblog tblDblog = new TblDblog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(41))
                    .orElse(null);
            tblDbclaim1.setDbclaimcode(Long.valueOf(assignDbCasetoSolicitor.getDbClaimCode()));

            tblDblog.setCreatedon(new Date());
            tblDblog.setDescr(tblStatus.getDescr());
            tblDblog.setUsercode(tblUser.getUsercode());
            tblDblog.setTblDbclaim(tblDbclaim1);

            int updateSolicitor = tblDbsolicitorRepo.updateSolicitor("N", Long.valueOf(assignDbCasetoSolicitor.getDbClaimCode()));

            TblDbsolicitor tblDbsolicitor = new TblDbsolicitor();
            tblDbsolicitor.setCompanycode(assignDbCasetoSolicitor.getSolicitorUserCode());
            tblDbsolicitor.setCreatedon(new Date());
            tblDbsolicitor.setTblDbclaim(tblDbclaim1);
            tblDbsolicitor.setStatus("Y");
            tblDbsolicitor.setUsercode(assignDbCasetoSolicitor.getSolicitorCode());

            tblDbsolicitor = tblDbsolicitorRepo.save(tblDbsolicitor);
            tblDblog = tblDblogRepo.save(tblDblog);

            TblDbclaim tblDbclaim = tblDbclaimRepo.findById(tblDbclaim1.getDbclaimcode()).orElse(null);

            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + tblDbclaim.getDbcode());
            tblEmail.setEmailsubject("HDR CASE" + tblDbclaim.getDbcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            TblDbmessage tblDbmessage = new TblDbmessage();

            tblDbmessage.setTblDbclaim(tblDbclaim1);
            tblDbmessage.setUserName(tblUser.getUsername());
            tblDbmessage.setCreatedon(new Date());
            tblDbmessage.setMessage(tblEmail.getEmailbody());
            tblDbmessage.setSentto(tblEmail.getEmailaddress());
            tblDbmessage.setUsercode(tblUser.getUsercode());

            tblDbmessage = tblDbmessageRepo.save(tblDbmessage);
            return tblDbclaimRepo.findById(Long.valueOf(assignDbCasetoSolicitor.getDbClaimCode())).orElse(null);

        } else {
            return null;
        }
    }

    @Override
    public TblDbclaim performRejectCancelActionOnDb(String DbClaimCode, String toStatus, String reason, TblUser tblUser) {
        TblDbsolicitor tblDbsolicitor = tblDbsolicitorRepo.findByTblDbclaimDbclaimcodeAndStatus(Long.valueOf(DbClaimCode), "Y");

        if (tblDbsolicitor != null) {
            int update = tblDbsolicitorRepo.updateRemarksReason(reason, "N", Long.valueOf(DbClaimCode));

            TblDbclaim tblDbclaim = tblDbclaimRepo.findById(Long.valueOf(DbClaimCode)).orElse(null);
            TblDblog tblDblog = new TblDblog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblDblog.setCreatedon(new Date());
            tblDblog.setDescr(tblStatus.getDescr());
            tblDblog.setUsercode(tblUser.getUsercode());
            tblDblog.setTblDbclaim(tblDbclaim);

            tblDblog = tblDblogRepo.save(tblDblog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblDbclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("you have been hotkey the case number is " + tblDbclaim.getDbcode());
            tblEmailIntroducer.setEmailsubject("HDR CASE" + tblDbclaim.getDbcode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Solicitor

            if (tblDbsolicitor != null) {
                TblUser solicitorUser = tblUsersRepo.findById(tblDbsolicitor.getUsercode()).orElse(null);
                TblEmail tblEmailSolicitor = new TblEmail();
                tblEmailSolicitor.setSenflag(new BigDecimal(0));
                tblEmailSolicitor.setEmailaddress(solicitorUser.getUsername());
                tblEmailSolicitor.setEmailbody("you have been hotkey the case number is " + tblDbclaim.getDbcode());
                tblEmailSolicitor.setEmailsubject("HDR CASE" + tblDbclaim.getDbcode());
                tblEmailSolicitor.setCreatedon(new Date());

                tblEmailSolicitor = tblEmailRepo.save(tblEmailSolicitor);
            }

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + tblDbclaim.getDbcode());
            tblEmail.setEmailsubject("HDR CASE" + tblDbclaim.getDbcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return tblDbclaim;

        } else {
            int update = tblDbclaimRepo.performRejectCancelAction(reason, toStatus, Long.valueOf(DbClaimCode));

            TblDbclaim tblDbclaim = tblDbclaimRepo.findById(Long.valueOf(DbClaimCode)).orElse(null);
            TblDblog tblDblog = new TblDblog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblDblog.setCreatedon(new Date());
            tblDblog.setDescr(tblStatus.getDescr());
            tblDblog.setUsercode(tblUser.getUsercode());
            tblDblog.setTblDbclaim(tblDbclaim);

            tblDblog = tblDblogRepo.save(tblDblog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblDbclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("you have been hotkey the case number is " + tblDbclaim.getDbcode());
            tblEmailIntroducer.setEmailsubject("RTA CASE" + tblDbclaim.getDbcode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + tblDbclaim.getDbcode());
            tblEmail.setEmailsubject("RTA CASE" + tblDbclaim.getDbcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return tblDbclaim;

        }
    }

    @Override
    public TblDbclaim performActionOnDb(String DbClaimCode, String toStatus, TblUser tblUser) {
        int update = tblDbclaimRepo.performAction(new BigDecimal(toStatus), Long.valueOf(DbClaimCode));
        if (update > 0) {

            TblDbclaim tblDbclaim = tblDbclaimRepo.findById(Long.valueOf(DbClaimCode)).orElse(null);
            TblDbclaim tblDbclaim1 = new TblDbclaim();
            TblDblog tblDblog = new TblDblog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);
            tblDbclaim1.setDbclaimcode(Long.valueOf(DbClaimCode));

            tblDblog.setCreatedon(new Date());
            tblDblog.setDescr(tblStatus.getDescr());
            tblDblog.setUsercode(tblUser.getUsercode());
            tblDblog.setTblDbclaim(tblDbclaim1);

            tblDblog = tblDblogRepo.save(tblDblog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblDbclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("Action taken on  case number :: " + tblDbclaim.getDbcode());
            tblEmailIntroducer.setEmailsubject("HDR CASE" + tblDbclaim.getDbcode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Solicitor
            TblDbsolicitor tblHdrsolicitor = tblDbsolicitorRepo.findByTblDbclaimDbclaimcodeAndStatus(tblDbclaim.getDbclaimcode(), "Y");
            if (tblHdrsolicitor != null) {
                TblUser solicitorUser = tblUsersRepo.findById(tblHdrsolicitor.getUsercode()).orElse(null);
                TblEmail tblEmailSolicitor = new TblEmail();
                tblEmailSolicitor.setSenflag(new BigDecimal(0));
                tblEmailSolicitor.setEmailaddress(solicitorUser.getUsername());
                tblEmailSolicitor.setEmailbody("Action taken on  case number :: " + tblDbclaim.getDbcode());
                tblEmailSolicitor.setEmailsubject("HDR CASE" + tblDbclaim.getDbcode());
                tblEmailSolicitor.setCreatedon(new Date());

                tblEmailSolicitor = tblEmailRepo.save(tblEmailSolicitor);
            }

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("Action taken on  case number :: " + tblDbclaim.getDbcode());
            tblEmail.setEmailsubject("HDR CASE" + tblDbclaim.getDbcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return tblDbclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblDbclaim findDbCaseByIdWithoutUser(long rtaCode) {
        return tblDbclaimRepo.findById(rtaCode).orElse(null);
    }

    @Override
    public TblCompanyprofile getCompanyProfileAgainstDbCode(long Dbclaimcode) {
        return tblCompanyprofileRepo.getCompanyProfileAgainstDbCode(Dbclaimcode);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndAgenatureAndCountrytypeAndTblCompaignCompaigncode(companycode, ageNature, countryType, null);
    }

    @Override
    public String getDbDbColumnValue(String key, long Dbclaimcode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + Dbclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT FIRSTNAME || ' ' || NVL(MIDDLENAME,'') || ' ' || NVL(LASTNAME, '') from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + Dbclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT POSTALCODE || ' ' || ADDRESS1 || ' ' || ADDRESS2  || ' ' || ADDRESS3 || ' ' || CITY || ' ' || REGION from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + Dbclaimcode;
            query = em.createNativeQuery(sql);
        } else {
            String sql = "SELECT " + key + " from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + Dbclaimcode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")
        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    @Override
    public TblDbdocument saveTblDbDocument(TblDbdocument tblDbdocument) {
        return tblDbdocumentRepo.save(tblDbdocument);
    }

    @Override
    public TblDbtask getDbTaskByDbCodeAndTaskCode(long Dbclaimcode, long taskCode) {
        return tblDbtaskRepo.findByTblDbclaimDbclaimcodeAndTaskcode(Dbclaimcode, new BigDecimal(taskCode));
    }

    @Override
    public TblDbtask updateTblDbTask(TblDbtask tblDbtask) {
        return tblDbtaskRepo.saveAndFlush(tblDbtask);
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public List<TblDbdocument> saveTblDbDocuments(List<TblDbdocument> tblDbdocuments) {
        return tblDbdocumentRepo.saveAll(tblDbdocuments);
    }

    @Override
    public List<TblDbtask> findTblDbTaskByDbClaimCode(long Dbclaimcode) {
        return tblDbtaskRepo.findByTblDbclaimDbclaimcode(Dbclaimcode);
    }

    @Override
    public List<DbCaseList> getAuthDbCasesIntroducers(String usercode) {
        List<DbCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListForIntroducers = tblDbclaimRepo.getDbCasesListForIntroducers(usercode);
        DbCaseList DbCaseList;
        if (DbCasesListForIntroducers != null && DbCasesListForIntroducers.size() > 0) {
            for (Object record : DbCasesListForIntroducers) {
                DbCaseList = new DbCaseList();
                Object[] row = (Object[]) record;

                DbCaseList.setDbClaimCode((BigDecimal) row[0]);
                DbCaseList.setCreated((String) row[1]);
                DbCaseList.setCode((String) row[2]);
                DbCaseList.setClient((String) row[3]);
                DbCaseList.setTaskDue((String) row[4]);
                DbCaseList.setTaskName((String) row[5]);
                DbCaseList.setStatus((String) row[6]);
                DbCaseList.setEmail((String) row[7]);
                DbCaseList.setAddress((String) row[8]);
                DbCaseList.setContactNo((String) row[9]);
                DbCaseList.setLastUpdated((Date) row[10]);
                DbCaseList.setIntroducer((String) row[11]);
                DbCaseList.setLastNote((Date) row[12]);
                DbCaseLists.add(DbCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<DbCaseList> getAuthDbCasesSolicitors(String usercode) {
        List<DbCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListForSolicitor = tblDbclaimRepo.getDbCasesListForSolicitor(usercode);
        DbCaseList DbCaseList;
        if (DbCasesListForSolicitor != null && DbCasesListForSolicitor.size() > 0) {
            for (Object record : DbCasesListForSolicitor) {
                DbCaseList = new DbCaseList();
                Object[] row = (Object[]) record;

                DbCaseList.setDbClaimCode((BigDecimal) row[0]);
                DbCaseList.setCreated((String) row[1]);
                DbCaseList.setCode((String) row[2]);
                DbCaseList.setClient((String) row[3]);
                DbCaseList.setTaskDue((String) row[4]);
                DbCaseList.setTaskName((String) row[5]);
                DbCaseList.setStatus((String) row[6]);
                DbCaseList.setEmail((String) row[7]);
                DbCaseList.setAddress((String) row[8]);
                DbCaseList.setContactNo((String) row[9]);
                DbCaseList.setLastUpdated((Date) row[10]);
                DbCaseList.setIntroducer((String) row[11]);
                DbCaseList.setLastNote((Date) row[12]);
                DbCaseLists.add(DbCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<DbCaseList> getAuthDbCasesLegalAssist() {
        List<DbCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesList = tblDbclaimRepo.getDbCasesList();
        DbCaseList DbCaseList;
        if (DbCasesList != null && DbCasesList.size() > 0) {
            for (Object record : DbCasesList) {
                DbCaseList = new DbCaseList();
                Object[] row = (Object[]) record;

                DbCaseList.setDbClaimCode((BigDecimal) row[0]);
                DbCaseList.setCreated((String) row[1]);
                DbCaseList.setCode((String) row[2]);
                DbCaseList.setClient((String) row[3]);
                DbCaseList.setTaskDue((String) row[4]);
                DbCaseList.setTaskName((String) row[5]);
                DbCaseList.setStatus((String) row[6]);
                DbCaseList.setEmail((String) row[7]);
                DbCaseList.setAddress((String) row[8]);
                DbCaseList.setContactNo((String) row[9]);
                DbCaseList.setLastUpdated((Date) row[10]);
                DbCaseList.setIntroducer((String) row[11]);
                DbCaseList.setLastNote((Date) row[12]);
                DbCaseLists.add(DbCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<DbStatusCountList> getAllDbStatusCountsForIntroducers() {
        List<DbStatusCountList> DbStatusCountLists = new ArrayList<>();
        List<Object> DbStatusCountListObject = tblDbclaimRepo.getAllDbStatusCountsForIntroducers();
        DbStatusCountList DbStatusCountList;
        if (DbStatusCountListObject != null && DbStatusCountListObject.size() > 0) {
            for (Object record : DbStatusCountListObject) {
                DbStatusCountList = new DbStatusCountList();
                Object[] row = (Object[]) record;

                DbStatusCountList.setStatusCount((BigDecimal) row[0]);
                DbStatusCountList.setStatusName((String) row[1]);
                DbStatusCountList.setStatusCode((BigDecimal) row[2]);

                DbStatusCountLists.add(DbStatusCountList);
            }
        }
        if (DbStatusCountLists != null && DbStatusCountLists.size() > 0) {
            return DbStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<DbStatusCountList> getAllDbStatusCountsForSolicitor() {
        List<DbStatusCountList> DbStatusCountLists = new ArrayList<>();
        List<Object> DbStatusCountListObject = tblDbclaimRepo.getAllDbStatusCountsForSolicitors();
        DbStatusCountList DbStatusCountList;
        if (DbStatusCountListObject != null && DbStatusCountListObject.size() > 0) {
            for (Object record : DbStatusCountListObject) {
                DbStatusCountList = new DbStatusCountList();
                Object[] row = (Object[]) record;

                DbStatusCountList.setStatusCount((BigDecimal) row[0]);
                DbStatusCountList.setStatusName((String) row[1]);
                DbStatusCountList.setStatusCode((BigDecimal) row[2]);

                DbStatusCountLists.add(DbStatusCountList);
            }
        }
        if (DbStatusCountLists != null && DbStatusCountLists.size() > 0) {
            return DbStatusCountLists;
        } else {
            return null;
        }
    }
}
