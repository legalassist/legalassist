package com.laportal.service.lov;

import com.laportal.Repo.*;
import com.laportal.dto.LovResponse;
import com.laportal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class LovServiceImpl implements LovService {

    @PersistenceContext
    @Autowired
    EntityManager em;
    @Autowired
    private TblUsercategoryRepo tblUsercategoryRepo;
    @Autowired
    private TblCompaignsRepo tblCompaignsRepo;
    @Autowired
    private TblRolesRepo tblRolesRepo;
    @Autowired
    private TblModulesRepo tblModulesRepo;
    @Autowired
    private TblCircumstanceRepo tblCircumstanceRepo;
    @Autowired
    private TblInjclassRepo tblInjclassRepo;
    @SuppressWarnings("unused")
    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;
    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;
    @Autowired
    private TblUsersRepo tblUsersRepo;
    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;
    @Autowired
    private TblHirebusinessRepo tblHirebusinessRepo;
    @Autowired
    private TblTaskRepo tblTaskRepo;
    @Autowired
    private TblHdrclaimRepo tblHdrclaimRepo;
    @Autowired
    private TblRtaclaimRepo tblRtaclaimRepo;
    @Autowired
    private TblHireclaimRepo tblHireclaimRepo;

    @Override
    public List<LovResponse> getlovUserCategory() {
        List<TblUsercategory> tblUsercategories = tblUsercategoryRepo.findByStatus("Y");
        if (tblUsercategories != null && tblUsercategories.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblUsercategory result : tblUsercategories) {
                lovResponse = new LovResponse();
                lovResponse.setCode(result.getCategorycode());
                lovResponse.setName(result.getCategoryname());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> getlovCompaign() {
        List<TblCompaign> tblCompaigns = tblCompaignsRepo.findAll();
        if (tblCompaigns != null && tblCompaigns.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompaign result : tblCompaigns) {
                lovResponse = new LovResponse();
                lovResponse.setCode(result.getCompaigncode());
                lovResponse.setName(result.getCompaignname());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> getlovRole() {
        List<TblRole> entityResult = tblRolesRepo.findByRolestatus("Y");
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblRole result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(result.getRolecode());
                lovResponse.setName(result.getRolename());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> getlovModule() {
        List<TblModule> entityResult = tblModulesRepo.findByModulestatus("Y");
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblModule result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(result.getModulecode());
                lovResponse.setName(result.getDescr());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovCircumstances() {
        List<TblCircumstance> entityResult = tblCircumstanceRepo.findByStatus("Y");
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCircumstance result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCircumcode()));
                lovResponse.setName(result.getDescr());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> getlovInjuryClaims(String airBag) {
        List<TblInjclass> entityResult = null;
        if (airBag.equals("Y")) {
            entityResult = tblInjclassRepo.findByStatus("Y");
        } else {
            entityResult = tblInjclassRepo.findByStatusAndAirbag("Y", "N");
        }
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblInjclass result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getInjclasscode()));
                lovResponse.setType(result.getInjlevel());
                lovResponse.setName(result.getDescr());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> getSolicitorsForRta(String jurisdiction, String airbagCase) {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllRtaCompanyProfileByJurisdictionAndAirBagCase(jurisdiction);
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovCompanyWiseUSer(String companyCode) {
        List<TblUser> entityResult = tblUsersRepo.findByCompanycode(companyCode);
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblUser result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getUsercode()));
                lovResponse.setName(result.getLoginid());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> getlovCompany() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.findAllByOrderByNameAsc();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> getlovStatus() {
        List<TblRtastatus> entityResult = tblRtastatusRepo.findByStatus("Y");
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblRtastatus result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getStatuscode())+" --- "+result.getCompaigncode());
                lovResponse.setName(result.getDescr());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> getlovHireBusiness(long hireCode) {
        List<TblHirebusiness> entityResult = tblHirebusinessRepo.findByTblHireclaimHirecode(hireCode);
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblHirebusiness result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getTblCompanyprofile().getCompanycode()));
                lovResponse.setName(result.getTblCompanyprofile().getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> getLovHireCompanies() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllHireCompanyProfile();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> getLovHireBusiness(Long hireCode) {
        List<TblHirebusiness> entityResult = tblHirebusinessRepo.findByStatusAndTblHireclaimHirecode("P", hireCode);
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblHirebusiness result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getTblCompanyprofile().getCompanycode()));
                lovResponse.setName(result.getTblCompanyprofile().getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovTask() {
        List<TblTask> entityResult = tblTaskRepo.findAll();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblTask result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getTaskcode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovIntroducer() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllIntroducerCompanyProfile();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovSolicitorsForHdr() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllHdrCompanyProfile();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovSolicitorsForPcp() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllPcpCompanyProfile();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovSolicitorsForTenancy() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllTenancyCompanyProfile();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovSolicitorsForDb() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllDbCompanyProfile();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovSolicitorsForOl() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllOlCompanyProfile();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovSolicitorsForPl() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllPlCompanyProfile();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovSolicitorsForEl() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllElCompanyProfile();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovCaseForInvoicing(String solicitorId, String compaingId) {
        if (compaingId.equals("1")) {
            List<TblRtaclaim> entityResult = tblRtaclaimRepo.getGetCaseForInvoicing(solicitorId);
            if (entityResult != null && entityResult.size() > 0) {
                List<LovResponse> lovResponses = new ArrayList<>();
                LovResponse lovResponse = null;
                for (TblRtaclaim result : entityResult) {
                    lovResponse = new LovResponse();
                    lovResponse.setCode(String.valueOf(result.getRtacode()));
                    lovResponse.setName(result.getRtanumber());
                    lovResponses.add(lovResponse);
                }
                return lovResponses;
            } else {
                return null;
            }
        } else if (compaingId.equals("2")) {
            return null;
        } else if (compaingId.equals("3")) {
            List<TblHdrclaim> entityResult = tblHdrclaimRepo.getGetCaseForInvoicing(solicitorId);
            if (entityResult != null && entityResult.size() > 0) {
                List<LovResponse> lovResponses = new ArrayList<>();
                LovResponse lovResponse = null;
                for (TblHdrclaim result : entityResult) {
                    lovResponse = new LovResponse();
                    lovResponse.setCode(String.valueOf(result.getHdrclaimcode()));
                    lovResponse.setName(result.getClaimcode());
                    lovResponses.add(lovResponse);
                }
                return lovResponses;
            } else {
                return null;
            }
        } else {
            return null;
        }

    }


    @Override
    public List<LovResponse> lovInvoicingStatus() {
        List<TblRtastatus> entityResult = tblRtastatusRepo.findByCompaigncodeIsNull();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblRtastatus result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getStatuscode()));
                lovResponse.setName(result.getDescr());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovSolicitorsForHire() {
        List<TblCompanyprofile> entityResult = tblCompanyprofileRepo.getAllHireCompanyProfile();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblCompanyprofile result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getCompanycode()));
                lovResponse.setName(result.getName());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyById(String companyCode) {
        return tblCompanyprofileRepo.findById(companyCode).orElse(null);
    }

    @Override
    public List<LovResponse> getlovHireBusinessForHireCompanies(Long hireCode, String companycode) {
        TblHirebusiness entityResult = tblHirebusinessRepo.findByTblHireclaimHirecodeAndTblCompanyprofileCompanycode(hireCode, companycode);
        if (entityResult != null) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            lovResponse = new LovResponse();
            lovResponse.setCode(String.valueOf(entityResult.getTblCompanyprofile().getCompanycode()));
            lovResponse.setName(entityResult.getTblCompanyprofile().getName());
            lovResponses.add(lovResponse);

            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovInvoiceStatus() {
        List<TblRtastatus> entityResult = tblRtastatusRepo.findByCompaigncodeIsNull();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblRtastatus result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getStatuscode()));
                lovResponse.setName(result.getDescr());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovIntroducerUsers() {
        List<TblUser> entityResult = tblUsersRepo.lovintorducerUser();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblUser result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getUsercode()));
                lovResponse.setName(result.getUsername());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovSolicitorUsers() {
        List<TblUser> entityResult = tblUsersRepo.lovSolicitorUser();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblUser result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getUsercode()));
                lovResponse.setName(result.getUsername());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<LovResponse> lovLegalInternalsUsers() {
        List<TblUser> entityResult = tblUsersRepo.lovInternalUser();
        if (entityResult != null && entityResult.size() > 0) {
            List<LovResponse> lovResponses = new ArrayList<>();
            LovResponse lovResponse = null;
            for (TblUser result : entityResult) {
                lovResponse = new LovResponse();
                lovResponse.setCode(String.valueOf(result.getUsercode()));
                lovResponse.setName(result.getUsername());
                lovResponses.add(lovResponse);
            }
            return lovResponses;
        } else {
            return null;
        }
    }
}
