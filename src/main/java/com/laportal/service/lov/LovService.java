package com.laportal.service.lov;

import com.laportal.dto.LovResponse;
import com.laportal.model.TblCompanyprofile;

import java.util.List;

public interface LovService {

    List<LovResponse> getlovUserCategory();

    List<LovResponse> getlovCompaign();

    List<LovResponse> getlovRole();

    List<LovResponse> getlovModule();

    List<LovResponse> lovCircumstances();

    List<LovResponse> getlovInjuryClaims(String airBag);

    List<LovResponse> getSolicitorsForRta(String jurisdiction, String airbagCase);

    List<LovResponse> lovCompanyWiseUSer(String companyCode);

    List<LovResponse> getlovCompany();

    List<LovResponse> getlovStatus();

    List<LovResponse> getlovHireBusiness(long hireCode);

    List<LovResponse> getLovHireCompanies();

    List<LovResponse> getLovHireBusiness(Long hireCode);

    List<LovResponse> lovTask();

    List<LovResponse> lovIntroducer();

    List<LovResponse> lovSolicitorsForHdr();

    List<LovResponse> lovSolicitorsForPcp();

    List<LovResponse> lovSolicitorsForTenancy();

    List<LovResponse> lovSolicitorsForDb();

    List<LovResponse> lovSolicitorsForOl();

    List<LovResponse> lovSolicitorsForPl();

    List<LovResponse> lovSolicitorsForEl();

    List<LovResponse> lovCaseForInvoicing(String solicitorId, String compaingId);

    List<LovResponse> lovInvoicingStatus();

    List<LovResponse> lovSolicitorsForHire();

    TblCompanyprofile getCompanyById(String companyCode);

    List<LovResponse> getlovHireBusinessForHireCompanies(Long hireCode, String companycode);

    List<LovResponse> lovInvoiceStatus();

    List<LovResponse> lovSolicitorUsers();

    List<LovResponse> lovLegalInternalsUsers();

    List<LovResponse> lovIntroducerUsers();
}
