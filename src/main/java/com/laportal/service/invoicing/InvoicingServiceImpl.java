package com.laportal.service.invoicing;

import com.laportal.Repo.*;
import com.laportal.dto.InvoiceSearch;
import com.laportal.dto.InvoicingStatusCountList;
import com.laportal.dto.RemoveInvoiceRequest;
import com.laportal.model.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class InvoicingServiceImpl implements InvoicingService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblSolicitorInvoiceHeadRepo tblSolicitorInvoiceHeadRepo;

    @Autowired
    private TblSolicitorInvoiceDetailRepo tblSolicitorInvoiceDetailRepo;

    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;

    @Autowired
    private TblUsersRepo tblUsersRepo;

    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;

    @Autowired
    private ViewInvoiceRepo viewInvoiceRepo;

    @Autowired
    private ViewInvoiceLineRepo viewInvoiceLineRepo;
    @Autowired
    private TblManualinvoicingRepo tblManualinvoicingRepo;
    @Autowired
    private TblManualinvoicingdetailRepo tblManualinvoicingdetailRepo;
    @Autowired
    private TblInvoiceDetailRepo tblInvoiceDetailRepo;
    @Autowired
    private OnelinkRepo onelinkRepo;
    @Autowired
    private TblRtaclaimRepo tblRtaclaimRepo;
    @Autowired
    private TblHdrclaimRepo tblHdrclaimRepo;
    @Autowired
    private TblHireclaimRepo tblHireclaimRepo;
    @Autowired
    private TblEmailRepo tblEmailRepo;
    @Autowired
    private TblInvoicenoteRepo tblInvoicenoteRepo;

    @Override
    public List<TblSolicitorInvoiceHead> getInvoices() {
        return tblSolicitorInvoiceHeadRepo.findAllByOrderByCreatedonDesc();
    }

    @Override
    public List<InvoicingStatusCountList> getAllInvoicesStatusCount() {
        List<InvoicingStatusCountList> HdrStatusCountListS = new ArrayList<>();
        List<Object> invoiceStatusCountListObject = tblSolicitorInvoiceHeadRepo.getAllInvoicesStatusCount();
        InvoicingStatusCountList invoicingStatusCountList;
        if (invoiceStatusCountListObject != null && invoiceStatusCountListObject.size() > 0) {
            for (Object record : invoiceStatusCountListObject) {
                invoicingStatusCountList = new InvoicingStatusCountList();
                Object[] row = (Object[]) record;

                invoicingStatusCountList.setStatusCount((BigDecimal) row[0]);
                invoicingStatusCountList.setStatusName((String) row[1]);
                invoicingStatusCountList.setStatusCode((BigDecimal) row[2]);

                HdrStatusCountListS.add(invoicingStatusCountList);
            }
        }
        if (HdrStatusCountListS != null && HdrStatusCountListS.size() > 0) {
            return HdrStatusCountListS;
        } else {
            return null;
        }
    }

    @Override
    public List<TblSolicitorInvoiceHead> getInvoicesByStatus(long statusId) {
        return tblSolicitorInvoiceHeadRepo.findByStatus(new BigDecimal(statusId));
    }

    @Override
    public List<ViewInvoiceDetail> getInvoiceDetailById(long invoiceId) {


        List<Object> invoiceDetails = tblSolicitorInvoiceHeadRepo.getByInvoiceheadid(new BigDecimal(invoiceId));
        ViewInvoiceDetail viewInvoiceDetail;
        if (invoiceDetails != null && invoiceDetails.size() > 0) {
            List<ViewInvoiceDetail> viewInvoiceDetails = new ArrayList<>();
            for (Object record : invoiceDetails) {
                viewInvoiceDetail = new ViewInvoiceDetail();
                Object[] row = (Object[]) record;

                viewInvoiceDetail.setInvoiceheadid((BigDecimal) row[0]);
                viewInvoiceDetail.setDescription((String) row[1]);
                viewInvoiceDetail.setCharge((BigDecimal) row[2]);
                viewInvoiceDetail.setAdjust((BigDecimal) row[3]);
                viewInvoiceDetail.setInvType(String.valueOf((char) row[4]));
                viewInvoiceDetail.setCasecode((BigDecimal) row[5]);
                viewInvoiceDetail.setInvoicedetailid(((BigDecimal) row[6]).longValue());
                viewInvoiceDetail.setInvoicegeneration(String.valueOf((char) row[7]));
                viewInvoiceDetail.setCampaigncode((String)row[8]);


                viewInvoiceDetails.add(viewInvoiceDetail);

            }

            return viewInvoiceDetails;
        }
        return null;

    }

    @Override
    public TblSolicitorInvoiceHead getInvoiceById(long invoiceId) {
        return tblSolicitorInvoiceHeadRepo.findById(invoiceId).orElse(null);
    }

    @Override
    public TblCompanyprofile getCompanyProfileById(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public TblUser getUserById(String createuser) {
        return tblUsersRepo.findById(createuser).orElse(null);
    }

    @Override
    public TblSolicitorInvoiceDetail getInvoiceDetailByDetailId(Long invoiceDetailId) {
        return tblSolicitorInvoiceDetailRepo.findById(invoiceDetailId).orElse(null);
    }

    @Override
    public void deleteInvoiceDetail(TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail) {
        tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().setAmount(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getAmount().subtract(tblSolicitorInvoiceDetail.getAmount()));
        tblSolicitorInvoiceHeadRepo.saveAndFlush(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead());
        tblSolicitorInvoiceDetailRepo.delete(tblSolicitorInvoiceDetail);
    }

    @Override
    public TblSolicitorInvoiceHead performActionOnInvoicing(String invoiceheadid, String toStatus, TblUser tblUser, long emailcode) {
        TblRtastatus tblRtastatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);
        int update = 0;
        if (emailcode > 0) {
            update = tblSolicitorInvoiceHeadRepo.performAction(new BigDecimal(toStatus), Long.valueOf(invoiceheadid), tblRtastatus.getDescr(), emailcode);
        } else {
            update = tblSolicitorInvoiceHeadRepo.performAction(new BigDecimal(toStatus), Long.valueOf(invoiceheadid), tblRtastatus.getDescr());

        }
        if (update > 0) {
            TblSolicitorInvoiceHead tblSolicitorInvoiceHead = tblSolicitorInvoiceHeadRepo.findById(Long.valueOf(invoiceheadid)).orElse(null);
            return tblSolicitorInvoiceHead;
        } else {
            return null;
        }
    }

    @Override
    public List<TblRtastatus> getRtaStatusForInvoices() {
        return tblRtastatusRepo.findStatusforInvoices();
    }

    @Override
    public List<TblSolicitorInvoiceHead> invoiceSearch(InvoiceSearch invoiceSearch) throws ParseException {


        return tblSolicitorInvoiceHeadRepo.invoiceSearch(invoiceSearch.getStatus(),
                invoiceSearch.getCompanycode(), invoiceSearch.getDatefrom(), invoiceSearch.getDateto());
    }

    @Override
    public List<ViewInvoice> getInvoiceListCsv() {
        return viewInvoiceRepo.findAll();
    }

    @Override
    public List<ViewInvoiceLine> getInvoiceLinesCsv() {
        return viewInvoiceLineRepo.findAll();
    }

    @Override
    public TblSolicitorInvoiceHead addSingleInvoice(TblSolicitorInvoiceHead tblSolicitorInvoiceHead, TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail) {

        tblSolicitorInvoiceHead = tblSolicitorInvoiceHeadRepo.save(tblSolicitorInvoiceHead);
        tblSolicitorInvoiceDetail = tblSolicitorInvoiceDetailRepo.save(tblSolicitorInvoiceDetail);
        if (tblSolicitorInvoiceDetail.getInvoicedetailid() > 0) {
            return tblSolicitorInvoiceHead;
        } else {
            return null;
        }
    }

    @Override
    public String runManualJobForInvoicing(String invoicedate, String companyCode, String compaingCode, String usercode) throws ParseException {

        Session session = em.unwrap(Session.class);
        final int[] status = new int[1];
        final String[] statusdescr = new String[1];

/*         PROC_GENINVOICE
(
  P_DATE IN DATE,
  P_CREATEUSER IN NUMBER,
  P_COMPANYCODE IN VARCHAR2,
  P_CAMPAIGN IN VARCHAR2,  -- R=RTA/H=HDR/I=HIRE
  P_STATUS OUT NUMBER,
  P_STATUSDESCR OUT VARCHAR2
)*/


        session.doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                CallableStatement call = connection.prepareCall("{call PROC_GENINVOICE(?,?,?,?,?,?) }");
                call.setString(1, invoicedate);
                call.setString(2, usercode);
                call.setString(3, companyCode);
                call.setString(4, compaingCode);
                call.registerOutParameter(5, Types.INTEGER);
                call.registerOutParameter(6, Types.VARCHAR);
                call.execute();
                status[0] = call.getInt(5);
                statusdescr[0] = call.getString(6);
            }
        });

        return statusdescr[0];
    }


    @Override
    public TblManualinvoicing saveManualInvoice(TblManualinvoicing tblManualinvoicing, List<TblManualinvoicingdetail> tblManualinvoicingdetails) {

        tblManualinvoicing = tblManualinvoicingRepo.saveAndFlush(tblManualinvoicing);
        if (tblManualinvoicing != null) {
            for (TblManualinvoicingdetail tblManualinvoicingdetail : tblManualinvoicingdetails) {
                tblManualinvoicingdetail.setTblManualinvoicing(tblManualinvoicing);
            }
            tblManualinvoicingdetails = tblManualinvoicingdetailRepo.saveAll(tblManualinvoicingdetails);

        } else {
            return null;
        }
        tblManualinvoicing.setTblManualinvoicingdetails(tblManualinvoicingdetails);
        return tblManualinvoicing;
    }

    @Override
    public List<TblManualinvoicing> getAllManualInvoices() {
        return tblManualinvoicingRepo.findAll();
    }

    @Override
    public TblManualinvoicing getManualInvoiceById(long manualInvId) {
        return tblManualinvoicingRepo.findById(manualInvId).orElse(null);
    }

    @Override
    public String getmanualInvoiceNumber() {
        return StringUtils.leftPad(String.valueOf(tblManualinvoicingRepo.getmanualInvoiceNumber()), 8, "0");
    }

    @Override
    public int updateInvoiceDetail(TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail) {
        BigDecimal totalamount = tblSolicitorInvoiceDetail.getAmount().subtract(tblSolicitorInvoiceDetail.getAdjust());
        BigDecimal vatRate = new BigDecimal("0.20");

        tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().setAmount(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getAmount().add(totalamount));
        tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().setVatAmount(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getAmount().multiply(vatRate).doubleValue());
        tblSolicitorInvoiceHeadRepo.saveAndFlush(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead());
        tblSolicitorInvoiceDetailRepo.saveAndFlush(tblSolicitorInvoiceDetail);

        return 0;
    }

    @Override
    public TblInvoiceDetail addManualInvoiceLine(TblInvoiceDetail tblInvoiceDetail) {
        tblInvoiceDetail = tblInvoiceDetailRepo.saveAndFlush(tblInvoiceDetail);

        TblSolicitorInvoiceHead tblSolicitorInvoiceHead = tblSolicitorInvoiceHeadRepo.findById(tblInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()).orElse(null);
        BigDecimal vatRate = new BigDecimal("0.20");

        tblSolicitorInvoiceHead.setAmount(tblSolicitorInvoiceHead.getAmount().add(BigDecimal.valueOf(tblInvoiceDetail.getCharge() - tblInvoiceDetail.getAdjust())));
        tblSolicitorInvoiceHead.setVatAmount(tblSolicitorInvoiceHead.getAmount().multiply(vatRate).doubleValue());
        tblSolicitorInvoiceHeadRepo.saveAndFlush(tblSolicitorInvoiceHead);


        return null;
    }

    @Override
    public void deleteInvoiceDetailManual(RemoveInvoiceRequest removeInvoiceRequest) {
        tblInvoiceDetailRepo.deleteById(Long.valueOf(removeInvoiceRequest.getInvoiceDetailId()));
    }

    @Override
    public TblInvoiceDetail getManualInvoiceDetailById(String invoiceDetailId) {
        return tblInvoiceDetailRepo.findById(Long.valueOf(invoiceDetailId)).orElse(null);
    }

    @Override
    public void updateManualInvoiceDetail(TblInvoiceDetail tblInvoiceDetail) {
        tblInvoiceDetail = tblInvoiceDetailRepo.saveAndFlush(tblInvoiceDetail);
        TblSolicitorInvoiceHead tblSolicitorInvoiceHead = tblSolicitorInvoiceHeadRepo.findById(tblInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()).orElse(null);
        long totalamount = tblInvoiceDetail.getCharge() - tblInvoiceDetail.getAdjust();
        BigDecimal vatRate = new BigDecimal("0.20");

        tblSolicitorInvoiceHead.setAmount(tblSolicitorInvoiceHead.getAmount().add(BigDecimal.valueOf(totalamount)));
        tblSolicitorInvoiceHead.setVatAmount(tblSolicitorInvoiceHead.getAmount().multiply(vatRate).doubleValue());
        tblSolicitorInvoiceHeadRepo.saveAndFlush(tblSolicitorInvoiceHead);


    }

    @Override
    public List<Onelink> getAllOneLinkUnUsedTransactions() {
        return onelinkRepo.findByStatusIsNull();
    }

    @Override
    public void updateOneLink(Onelink updateOneLink) {
        onelinkRepo.saveAndFlush(updateOneLink);
    }

    @Override
    public TblSolicitorInvoiceHead performPaidActionOnInvoicing(String invoiceheadid, String toStatus, TblUser tblUser) {
        TblSolicitorInvoiceHead tblSolicitorInvoiceHead = tblSolicitorInvoiceHeadRepo.findById(Long.valueOf(invoiceheadid)).orElse(null);
        if (tblSolicitorInvoiceHead.getTblSolicitorInvoiceDetails() != null) {
            for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceHead.getTblSolicitorInvoiceDetails()) {
                if (tblSolicitorInvoiceDetail.getTblCompaign().getCompaignname().equals("RTA")) {

                    TblRtaclaim tblRtaclaim = tblRtaclaimRepo.findById(tblSolicitorInvoiceDetail.getCasecode().longValue()).orElse(null);
                    List<String> PCB_STATUS = List.of("16", "254", "253", "249", "252", "250", "251", "248", "246", "247");
                    List<String> INVOICED_STATUS = List.of("13");

//                    if (INVOICED_STATUS.contains(tblRtaclaim.getStatuscode())) {
//                        tblRtaclaimRepo.performAction("332", tblRtaclaim.getRtacode(),tblUser.getUsercode());
//                    } else if (PCB_STATUS.contains(tblRtaclaim.getStatuscode())) {
//                        tblRtaclaimRepo.performAction("333", tblRtaclaim.getRtacode(),tblUser.getUsercode());
//                    }
                } else if (tblSolicitorInvoiceDetail.getTblCompaign().getCompaignname().equals("HDR")) {
                    TblHdrclaim tblHdrclaim = tblHdrclaimRepo.findById(tblSolicitorInvoiceDetail.getCasecode().longValue()).orElse(null);
                    List<String> PCB_STATUS = List.of("53", "263", "262", "258", "261", "259", "260", "257", "255", "256");
                    List<String> INVOICED_STATUS = List.of("51");

//                    if (INVOICED_STATUS.contains(tblHdrclaim.getStatuscode())) {
//                        tblHdrclaimRepo.performAction(new BigDecimal(332), tblHdrclaim.getHdrclaimcode());
//                    } else if (PCB_STATUS.contains(tblHdrclaim.getStatuscode())) {
//                        tblHdrclaimRepo.performAction(new BigDecimal(333), tblHdrclaim.getHdrclaimcode());
//                    }


                } else if (tblSolicitorInvoiceDetail.getTblCompaign().getCompaignname().equals("HIRE")) {
                    TblHireclaim tblHireclaim = tblHireclaimRepo.findById(tblSolicitorInvoiceDetail.getCasecode().longValue()).orElse(null);
                    List<String> PCB_STATUS = List.of("323", "322", "324", "325", "326", "327", "328", "329", "330", "331");
                    List<String> INVOICED_STATUS = List.of("34");

//                    if (INVOICED_STATUS.contains(tblHireclaim.getTblRtastatus().getStatuscode())) {
//                        tblHireclaimRepo.performAction("332",tblUser.getUsercode(), tblHireclaim.getHirecode());
//                    } else if (PCB_STATUS.contains(tblHireclaim.getTblRtastatus().getStatuscode())) {
//                        tblHireclaimRepo.performAction("332",tblUser.getUsercode(), tblHireclaim.getHirecode());
//                    }

                }
            }
            return performActionOnInvoicing(invoiceheadid, toStatus, tblUser, 0);
        } else {
            return null;
        }
    }

    @Override
    public TblSolicitorInvoiceHead performVoidedActionOnInvoicing(String invoiceheadid, String toStatus, TblUser tblUser) {
        TblSolicitorInvoiceHead tblSolicitorInvoiceHead = tblSolicitorInvoiceHeadRepo.findById(Long.valueOf(invoiceheadid)).orElse(null);
        if (tblSolicitorInvoiceHead.getTblSolicitorInvoiceDetails() != null) {
            for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceHead.getTblSolicitorInvoiceDetails()) {
                if (tblSolicitorInvoiceDetail.getTblCompaign().getCompaignname().equals("RTA")) {

                    TblRtaclaim tblRtaclaim = tblRtaclaimRepo.findById(tblSolicitorInvoiceDetail.getCasecode().longValue()).orElse(null);
                    List<String> PCB_STATUS = List.of("16", "254", "253", "249", "252", "250", "251", "248", "246", "247");
                    List<String> INVOICED_STATUS = List.of("13");

                    if (INVOICED_STATUS.contains(tblRtaclaim.getStatuscode())) {
                        tblRtaclaimRepo.performAction("7", tblRtaclaim.getRtacode(),tblUser.getUsercode());
                    } else if (PCB_STATUS.contains(tblRtaclaim.getStatuscode())) {
                        tblRtaclaimRepo.performAction("15", tblRtaclaim.getRtacode(),tblUser.getUsercode());
                    }
                } else if (tblSolicitorInvoiceDetail.getTblCompaign().getCompaignname().equals("HDR")) {
                    TblHdrclaim tblHdrclaim = tblHdrclaimRepo.findById(tblSolicitorInvoiceDetail.getCasecode().longValue()).orElse(null);
                    List<String> PCB_STATUS = List.of("53", "263", "262", "258", "261", "259", "260", "257", "255", "256");
                    List<String> INVOICED_STATUS = List.of("51");

                    if (INVOICED_STATUS.contains(tblHdrclaim.getStatuscode())) {
                        tblHdrclaimRepo.performAction(new BigDecimal(50), tblHdrclaim.getHdrclaimcode());
                    } else if (PCB_STATUS.contains(tblHdrclaim.getStatuscode())) {
                        tblHdrclaimRepo.performAction(new BigDecimal(52), tblHdrclaim.getHdrclaimcode());
                    }


                } else if (tblSolicitorInvoiceDetail.getTblCompaign().getCompaignname().equals("HIRE")) {
                    TblHireclaim tblHireclaim = tblHireclaimRepo.findById(tblSolicitorInvoiceDetail.getCasecode().longValue()).orElse(null);
                    List<String> PCB_STATUS = List.of("323", "322", "324", "325", "326", "327", "328", "329", "330", "331");
                    List<String> INVOICED_STATUS = List.of("34");

                    if (INVOICED_STATUS.contains(tblHireclaim.getTblRtastatus().getStatuscode())) {
                        tblHireclaimRepo.performAction("28",tblUser.getUsercode(), tblHireclaim.getHirecode());
                    } else if (PCB_STATUS.contains(tblHireclaim.getTblRtastatus().getStatuscode())) {
                        tblHireclaimRepo.performAction("312",tblUser.getUsercode(), tblHireclaim.getHirecode());
                    }

                }
            }
            return performActionOnInvoicing(invoiceheadid, toStatus, tblUser, 0);
        } else {
            return null;
        }
    }

    @Override
    public TblSolicitorInvoiceHead performOverDueActionOnInvoicing(String invoiceheadid, String toStatus, TblUser tblUser) {
        TblSolicitorInvoiceHead tblSolicitorInvoiceHead = tblSolicitorInvoiceHeadRepo.findById(Long.valueOf(invoiceheadid)).orElse(null);
        if (tblSolicitorInvoiceHead.getTblSolicitorInvoiceDetails() != null) {
            TblEmail tblEmail = tblEmailRepo.findById(tblSolicitorInvoiceHead.getTblEmail().getEmailcode()).orElse(null);

            tblEmail.setSenflag(new BigDecimal(0));
            tblEmailRepo.saveAndFlush(tblEmail);
            return performActionOnInvoicing(invoiceheadid, toStatus, tblUser, 0);
        } else {
            return null;
        }
    }

    @Override
    public TblInvoicenote addNoteToInvoice(TblInvoicenote tblInvoicenote) {
        return tblInvoicenoteRepo.saveAndFlush(tblInvoicenote);
    }

    @Override
    public List<TblInvoicenote> getInvoiceCaseNotes(String invoiceHeadId, TblUser tblUser) {
        List<TblInvoicenote> tblInvoicenotes = tblInvoicenoteRepo.findByTblSolicitorInvoiceHeadInvoiceheadid(Long.valueOf(invoiceHeadId));
        if (tblInvoicenotes != null && tblInvoicenotes.size() > 0) {
            for (TblInvoicenote tblRttblInvoicenotenote : tblInvoicenotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblRttblInvoicenotenote.getUsercode()).orElse(null);
                tblRttblInvoicenotenote.setUserName(tblUser1.getLoginid());
                tblRttblInvoicenotenote.setSelf(tblRttblInvoicenotenote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
            }

            return tblInvoicenotes;
        } else {
            return null;
        }
    }
}
