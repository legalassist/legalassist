package com.laportal.service.invoicing;

import com.laportal.dto.InvoiceSearch;
import com.laportal.dto.InvoicingStatusCountList;
import com.laportal.dto.RemoveInvoiceRequest;
import com.laportal.model.*;

import java.text.ParseException;
import java.util.List;

public interface InvoicingService {

    List<TblSolicitorInvoiceHead> getInvoices();

    List<InvoicingStatusCountList> getAllInvoicesStatusCount();

    List<TblSolicitorInvoiceHead> getInvoicesByStatus(long statusId);

    List<ViewInvoiceDetail> getInvoiceDetailById(long invoiceId);

    TblSolicitorInvoiceHead getInvoiceById(long invoiceId);

    TblCompanyprofile getCompanyProfileById(String companycode);

    TblUser getUserById(String createuser);

    TblSolicitorInvoiceDetail getInvoiceDetailByDetailId(Long invoiceDetailId);

    void deleteInvoiceDetail(TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail);

    TblSolicitorInvoiceHead performActionOnInvoicing(String hdrClaimCode, String toStatus, TblUser tblUser, long emailcode);

    List<TblRtastatus> getRtaStatusForInvoices();

    List<TblSolicitorInvoiceHead> invoiceSearch(InvoiceSearch invoiceSearch) throws ParseException;

    List<ViewInvoice> getInvoiceListCsv();

    List<ViewInvoiceLine> getInvoiceLinesCsv();

    TblSolicitorInvoiceHead addSingleInvoice(TblSolicitorInvoiceHead tblSolicitorInvoiceHead, TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail);

    String runManualJobForInvoicing(String invoicedate, String companyCode, String compaingCode, String usercode) throws ParseException;


    TblManualinvoicing saveManualInvoice(TblManualinvoicing tblManualinvoicing, List<TblManualinvoicingdetail> tblManualinvoicingdetails);

    List<TblManualinvoicing> getAllManualInvoices();

    TblManualinvoicing getManualInvoiceById(long manualInvId);

    String getmanualInvoiceNumber();

    int updateInvoiceDetail(TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail);

    TblInvoiceDetail addManualInvoiceLine(TblInvoiceDetail tblInvoiceDetail);

    void deleteInvoiceDetailManual(RemoveInvoiceRequest removeInvoiceRequest);

    TblInvoiceDetail getManualInvoiceDetailById(String invoiceDetailId);

    void updateManualInvoiceDetail(TblInvoiceDetail tblInvoiceDetail);

    List<Onelink> getAllOneLinkUnUsedTransactions();


    void updateOneLink(Onelink updateOneLink);

    TblSolicitorInvoiceHead performPaidActionOnInvoicing(String caseId, String toStatus, TblUser tblUser);

    TblSolicitorInvoiceHead performVoidedActionOnInvoicing(String caseId, String toStatus, TblUser tblUser);

    TblSolicitorInvoiceHead performOverDueActionOnInvoicing(String caseId, String toStatus, TblUser tblUser);

    TblInvoicenote addNoteToInvoice(TblInvoicenote tblInvoicenote);

    List<TblInvoicenote> getInvoiceCaseNotes(String invoiceHeadId, TblUser tblUser);
}
