package com.laportal.service.pcp;

import com.laportal.dto.AssignPcpCasetoSolicitor;
import com.laportal.dto.PcpCaseList;
import com.laportal.dto.PcpStatusCountList;
import com.laportal.model.*;

import java.util.List;

public interface PcpService {

    List<PcpCaseList> getPcpCases(TblUser tblUser);

    TblPcpclaim findPcpCaseById(long pcpCaseId, TblUser tblUser);

    TblCompanyprofile getCompanyProfile(String companycode);

    List<PcpStatusCountList> getAllPcpStatusCounts();

    List<PcpCaseList> getPcpCasesByStatus(long statusId);

    List<TblPcplog> getPcpCaseLogs(String pcpcode);

    List<TblPcpnote> getPcpCaseNotes(String pcpcode, TblUser tblUser);

    List<TblPcpmessage> getPcpCaseMessages(String pcpcode);

    boolean isHdrCaseAllowed(String companycode, String s);

    TblPcpclaim savePcpRequest(TblPcpclaim tblPcpclaim);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    TblPcpnote addTblPcpNote(TblPcpnote tblPcpnote);

    TblPcpmessage getTblPcpMessageById(String pcpmessagecode);

    TblEmail saveTblEmail(TblEmail tblEmail);

    TblPcpclaim assignCaseToSolicitor(AssignPcpCasetoSolicitor assignPcpCasetoSolicitor, TblUser tblUser);

    TblPcpclaim performRejectCancelActionOnPcp(String pcpClaimCode, String toStatus, String reason, TblUser tblUser);

    TblPcpclaim performActionOnPcp(String pcpClaimCode, String toStatus, TblUser tblUser);

    TblPcpclaim findPcpCaseByIdWithoutUser(long rtaCode);

    TblCompanyprofile getCompanyProfileAgainstPcpCode(long pcpclaimcode);

    TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType);

    String getPcpDbColumnValue(String key, long pcpclaimcode);

    TblPcpdocument saveTblPcpDocument(TblPcpdocument tblPcpdocument);

    TblPcptask getPcpTaskByPcpCodeAndTaskCode(long pcpclaimcode, long taskCode);

    TblPcptask updateTblPcpTask(TblPcptask tblPcptask);

    TblTask getTaskAgainstCode(long taskCode);

    List<TblPcpdocument> saveTblPcpDocuments(List<TblPcpdocument> tblPcpdocuments);

    List<TblPcptask> findTblPcpTaskByPcpClaimCode(long pcpclaimcode);

    List<PcpCaseList> getAuthPcpCasesIntroducers(String usercode);

    List<PcpCaseList> getAuthPcpCasesSolicitors(String usercode);

    List<PcpCaseList> getAuthPcpCasesLegalAssist();

    List<PcpStatusCountList> getAllPcpStatusCountsForIntroducers();

    List<PcpStatusCountList> getAllPcpStatusCountsForSolicitor();
}
