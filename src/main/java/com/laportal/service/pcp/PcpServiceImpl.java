package com.laportal.service.pcp;

import com.laportal.Repo.*;
import com.laportal.dto.AssignPcpCasetoSolicitor;
import com.laportal.dto.HdrActionButton;
import com.laportal.dto.PcpCaseList;
import com.laportal.dto.PcpStatusCountList;
import com.laportal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class PcpServiceImpl implements PcpService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;

    @Autowired
    private TblPcpclaimRepo tblPcpclaimRepo;

    @Autowired
    private TblPcplogRepo tblPcplogRepo;

    @Autowired
    private TblPcpmessageRepo tblPcpmessageRepo;

    @Autowired
    private TblPcpnoteRepo tblPcpnoteRepo;

    @Autowired
    private TblPcpsolicitorRepo tblPcpsolicitorRepo;

    @Autowired
    private TblTaskRepo tblTaskRepo;

    @Autowired
    private TblUsersRepo tblUsersRepo;

    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;

    @Autowired
    private TblEmailRepo tblEmailRepo;

    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;

    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;

    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;

    @Autowired
    private TblPcpdocumentRepo tblPcpdocumentRepo;

    @Autowired
    private TblPcptaskRepo tblPcptaskRepo;

    @Override
    public List<PcpCaseList> getPcpCases(TblUser tblUser) {
        List<PcpCaseList> pcpCaseLists = new ArrayList<>();
        List<Object> pcpCasesListObject = tblPcpclaimRepo.getPcpCasesListUserWise();
        PcpCaseList pcpCaseList;
        if (pcpCasesListObject != null && pcpCasesListObject.size() > 0) {
            for (Object record : pcpCasesListObject) {
                pcpCaseList = new PcpCaseList();
                Object[] row = (Object[]) record;

                pcpCaseList.setPcpClaimCode((BigDecimal) row[0]);
                pcpCaseList.setCreated((String) row[1]);
                pcpCaseList.setCode((String) row[2]);
                pcpCaseList.setClient((String) row[3]);
//                pcpCaseList.setTaskDue((String) row[4]);
//                pcpCaseList.setTaskName((String) row[5]);
                pcpCaseList.setStatus((String) row[4]);
//                pcpCaseList.setEmail((String) row[7]);
//                pcpCaseList.setAddress((String) row[8]);
//                pcpCaseList.setContactNo((String) row[9]);
//                pcpCaseList.setLastUpdated((Date) row[10]);
//                pcpCaseList.setIntroducer((String) row[11]);
//                pcpCaseList.setLastNote((Date) row[12]);

                pcpCaseLists.add(pcpCaseList);
            }
        }
        if (pcpCaseLists != null && pcpCaseLists.size() > 0) {
            return pcpCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public TblPcpclaim findPcpCaseById(long pcpCaseId, TblUser tblUser) {
        TblPcpclaim tblPcpclaim = tblPcpclaimRepo.findById(pcpCaseId).orElse(null);
        if (tblPcpclaim != null) {
            TblRtastatus tblRtastatus = tblRtastatusRepo.findById(Long.valueOf(tblPcpclaim.getStatus().toString())).orElse(null);
            tblPcpclaim.setStatusDescr(tblRtastatus.getDescr());
            List<HdrActionButton> rtaActionButtons = new ArrayList<>();
            List<HdrActionButton> rtaActionButtonsForLA = new ArrayList<>();
            HdrActionButton hdrActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    Long.valueOf(tblPcpclaim.getStatus().toString()), "3", tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetailForLA = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    Long.valueOf(tblPcpclaim.getStatus().toString()), "3", tblCompanyprofile.getTblUsercategory().getCategorycode());

            TblPcpsolicitor tblTenancysolicitor = tblPcpsolicitorRepo.findByTblPcpclaimPcpclaimcodeAndStatus(pcpCaseId, "Y");
            if (tblTenancysolicitor != null) {
                List<TblPcpsolicitor> tblTenancysolicitors = new ArrayList<>();
                tblTenancysolicitors.add(tblTenancysolicitor);
                tblPcpclaim.setTblPcpsolicitors(tblTenancysolicitors);
            } else {
                tblPcpclaim.setTblPcpsolicitors(null);
            }
            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    hdrActionButton = new HdrActionButton();
                    hdrActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hdrActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hdrActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hdrActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    hdrActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());
                    hdrActionButton.setAcceptDialog(tblRtaflowdetail.getCaseacceptdialog());

                    rtaActionButtons.add(hdrActionButton);
                }
                tblPcpclaim.setHdrActionButton(rtaActionButtons);
            }
            if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                    hdrActionButton = new HdrActionButton();
                    hdrActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hdrActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hdrActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hdrActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    hdrActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());
                    hdrActionButton.setAcceptDialog(tblRtaflowdetail.getCaseacceptdialog());

                    rtaActionButtonsForLA.add(hdrActionButton);
                }
                tblPcpclaim.setHdrActionButtonForLA(rtaActionButtonsForLA);
            }
            return tblPcpclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public List<PcpStatusCountList> getAllPcpStatusCounts() {
        List<PcpStatusCountList> pcpStatusCountLists = new ArrayList<>();
        List<Object> pcpStatusCountListObject = tblPcpclaimRepo.getAllPcpStatusCounts();
        PcpStatusCountList pcpStatusCountList;
        if (pcpStatusCountListObject != null && pcpStatusCountListObject.size() > 0) {
            for (Object record : pcpStatusCountListObject) {
                pcpStatusCountList = new PcpStatusCountList();
                Object[] row = (Object[]) record;

                pcpStatusCountList.setStatusCount((BigDecimal) row[0]);
                pcpStatusCountList.setStatusName((String) row[1]);
                pcpStatusCountList.setStatusCode((BigDecimal) row[2]);

                pcpStatusCountLists.add(pcpStatusCountList);
            }
        }
        if (pcpStatusCountLists != null && pcpStatusCountLists.size() > 0) {
            return pcpStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<PcpCaseList> getPcpCasesByStatus(long statusId) {
        List<PcpCaseList> pcpCaseLists = new ArrayList<>();
        List<Object> pcpCasesListObject = tblPcpclaimRepo.getPcpCasesListStatusWise(statusId);
        PcpCaseList pcpCaseList;
        if (pcpCasesListObject != null && pcpCasesListObject.size() > 0) {
            for (Object record : pcpCasesListObject) {
                pcpCaseList = new PcpCaseList();
                Object[] row = (Object[]) record;

                pcpCaseList.setPcpClaimCode((BigDecimal) row[0]);
                pcpCaseList.setCreated((String) row[1]);
                pcpCaseList.setCode((String) row[2]);
                pcpCaseList.setClient((String) row[3]);
//                pcpCaseList.setTaskDue((String) row[4]);
//                pcpCaseList.setTaskName((String) row[5]);
                pcpCaseList.setStatus((String) row[4]);
//                pcpCaseList.setEmail((String) row[7]);
//                pcpCaseList.setAddress((String) row[8]);
//                pcpCaseList.setContactNo((String) row[9]);
//                pcpCaseList.setLastUpdated((Date) row[10]);
//                pcpCaseList.setIntroducer((String) row[11]);
//                pcpCaseList.setLastNote((Date) row[12]);

                pcpCaseLists.add(pcpCaseList);
            }
        }
        if (pcpCaseLists != null && pcpCaseLists.size() > 0) {
            return pcpCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblPcplog> getPcpCaseLogs(String pcpcode) {
        List<TblPcplog> tblPcplogs = tblPcplogRepo.findByTblPcpclaimPcpclaimcode(Long.valueOf(pcpcode));
        if (tblPcplogs != null && tblPcplogs.size() > 0) {
            for (TblPcplog tblPcplog : tblPcplogs) {
                TblUser tblUser = tblUsersRepo.findById(tblPcplog.getUsercode()).orElse(null);
                tblPcplog.setUserName(tblUser.getUsername());
            }
            return tblPcplogs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblPcpnote> getPcpCaseNotes(String pcpcode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblPcpnote> tblPcpnotes = tblPcpnoteRepo.findNotesOnPcpbyUserAndCategory(
                Long.valueOf(pcpcode), tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (tblPcpnotes != null && tblPcpnotes.size() > 0) {
            for (TblPcpnote tblPcpnote : tblPcpnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblPcpnote.getUsercode()).orElse(null);
                tblPcpnote.setUserName(tblUser1.getUsername());
                tblPcpnote.setSelf(tblPcpnote.getUsercode() == tblUser.getUsercode() ? true : false);
            }
            return tblPcpnotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblPcpmessage> getPcpCaseMessages(String pcpcode) {
        List<TblPcpmessage> tblPcpmessageList = tblPcpmessageRepo.findByTblPcpclaimPcpclaimcode(Long.valueOf(pcpcode));
        if (tblPcpmessageList != null && tblPcpmessageList.size() > 0) {
            for (TblPcpmessage tblPcpmessage : tblPcpmessageList) {
                TblUser tblUser = tblUsersRepo.findById(tblPcpmessage.getUsercode()).orElse(null);
                tblPcpmessage.setUserName(tblUser.getUsername());
            }
            return tblPcpmessageList;
        } else {
            return null;
        }
    }

    @Override
    public boolean isHdrCaseAllowed(String companycode, String s) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companycode,
                s);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TblPcpclaim savePcpRequest(TblPcpclaim tblPcpclaim) {
        return tblPcpclaimRepo.save(tblPcpclaim);
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public TblPcpnote addTblPcpNote(TblPcpnote tblPcpnote) {
        return tblPcpnoteRepo.save(tblPcpnote);
    }

    @Override
    public TblPcpmessage getTblPcpMessageById(String pcpmessagecode) {
        return tblPcpmessageRepo.findById(Long.valueOf(pcpmessagecode)).orElse(null);
    }

    @Override
    public TblEmail saveTblEmail(TblEmail tblEmail) {
        return tblEmailRepo.save(tblEmail);
    }

    @Override
    public TblPcpclaim assignCaseToSolicitor(AssignPcpCasetoSolicitor assignPcpCasetoSolicitor, TblUser tblUser) {
        int update = tblPcpclaimRepo.performAction(new BigDecimal("41"), Long.valueOf(assignPcpCasetoSolicitor.getPcpClaimCode()));
        if (update > 0) {

            TblPcpclaim tblPcpclaim1 = new TblPcpclaim();
            TblPcplog tblPcplog = new TblPcplog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(41))
                    .orElse(null);
            tblPcpclaim1.setPcpclaimcode(Long.valueOf(assignPcpCasetoSolicitor.getPcpClaimCode()));

            tblPcplog.setCreatedon(new java.util.Date());
            tblPcplog.setDescr(tblStatus.getDescr());
            tblPcplog.setUsercode(tblUser.getUsercode());
            tblPcplog.setTblPcpclaim(tblPcpclaim1);

            int updateSolicitor = tblPcpsolicitorRepo.updateSolicitor("N", Long.valueOf(assignPcpCasetoSolicitor.getPcpClaimCode()));

            TblPcpsolicitor tblPcpsolicitor = new TblPcpsolicitor();
            tblPcpsolicitor.setCompanycode(assignPcpCasetoSolicitor.getSolicitorUserCode());
            tblPcpsolicitor.setCreatedon(new java.util.Date());
            tblPcpsolicitor.setTblPcpclaim(tblPcpclaim1);
            tblPcpsolicitor.setStatus("Y");
            tblPcpsolicitor.setUsercode(assignPcpCasetoSolicitor.getSolicitorCode());

            tblPcpsolicitor = tblPcpsolicitorRepo.save(tblPcpsolicitor);
            tblPcplog = tblPcplogRepo.save(tblPcplog);

            TblPcpclaim tblPcpclaim = tblPcpclaimRepo.findById(tblPcpclaim1.getPcpclaimcode()).orElse(null);

            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + tblPcpclaim.getPcpcode());
            tblEmail.setEmailsubject("HDR CASE" + tblPcpclaim.getPcpcode());
            tblEmail.setCreatedon(new java.util.Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            TblPcpmessage tblPcpmessage = new TblPcpmessage();

            tblPcpmessage.setTblPcpclaim(tblPcpclaim1);
            tblPcpmessage.setUserName(tblUser.getUsername());
            tblPcpmessage.setCreatedon(new Date());
            tblPcpmessage.setMessage(tblEmail.getEmailbody());
            tblPcpmessage.setSentto(tblEmail.getEmailaddress());
            tblPcpmessage.setUsercode(tblUser.getUsercode());

            tblPcpmessage = tblPcpmessageRepo.save(tblPcpmessage);
            return tblPcpclaimRepo.findById(Long.valueOf(assignPcpCasetoSolicitor.getPcpClaimCode())).orElse(null);

        } else {
            return null;
        }
    }

    @Override
    public TblPcpclaim performRejectCancelActionOnPcp(String pcpClaimCode, String toStatus, String reason, TblUser tblUser) {
        TblPcpsolicitor tblPcpsolicitor = tblPcpsolicitorRepo.findByTblPcpclaimPcpclaimcodeAndStatus(Long.valueOf(pcpClaimCode), "Y");

        if (tblPcpsolicitor != null) {
            int update = tblPcpsolicitorRepo.updateRemarksReason(reason, "N", Long.valueOf(pcpClaimCode));

            TblPcpclaim tblPcpclaim = tblPcpclaimRepo.findById(Long.valueOf(pcpClaimCode)).orElse(null);
            TblPcplog tblPcplog = new TblPcplog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblPcplog.setCreatedon(new Date());
            tblPcplog.setDescr(tblStatus.getDescr());
            tblPcplog.setUsercode(tblUser.getUsercode());
            tblPcplog.setTblPcpclaim(tblPcpclaim);

            tblPcplog = tblPcplogRepo.save(tblPcplog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblPcpclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("you have been hotkey the case number is " + tblPcpclaim.getPcpcode());
            tblEmailIntroducer.setEmailsubject("HDR CASE" + tblPcpclaim.getPcpcode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Solicitor

            if (tblPcpsolicitor != null) {
                TblUser solicitorUser = tblUsersRepo.findById(tblPcpsolicitor.getUsercode()).orElse(null);
                TblEmail tblEmailSolicitor = new TblEmail();
                tblEmailSolicitor.setSenflag(new BigDecimal(0));
                tblEmailSolicitor.setEmailaddress(solicitorUser.getUsername());
                tblEmailSolicitor.setEmailbody("you have been hotkey the case number is " + tblPcpclaim.getPcpcode());
                tblEmailSolicitor.setEmailsubject("HDR CASE" + tblPcpclaim.getPcpcode());
                tblEmailSolicitor.setCreatedon(new Date());

                tblEmailSolicitor = tblEmailRepo.save(tblEmailSolicitor);
            }

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + tblPcpclaim.getPcpcode());
            tblEmail.setEmailsubject("HDR CASE" + tblPcpclaim.getPcpcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return tblPcpclaim;

        } else {
            int update = tblPcpclaimRepo.performRejectCancelAction(reason, toStatus, Long.valueOf(pcpClaimCode));

            TblPcpclaim tblPcpclaim = tblPcpclaimRepo.findById(Long.valueOf(pcpClaimCode)).orElse(null);
            TblPcplog tblPcplog = new TblPcplog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblPcplog.setCreatedon(new Date());
            tblPcplog.setDescr(tblStatus.getDescr());
            tblPcplog.setUsercode(tblUser.getUsercode());
            tblPcplog.setTblPcpclaim(tblPcpclaim);

            tblPcplog = tblPcplogRepo.save(tblPcplog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblPcpclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("you have been hotkey the case number is " + tblPcpclaim.getPcpcode());
            tblEmailIntroducer.setEmailsubject("RTA CASE" + tblPcpclaim.getPcpcode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + tblPcpclaim.getPcpcode());
            tblEmail.setEmailsubject("RTA CASE" + tblPcpclaim.getPcpcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return tblPcpclaim;

        }
    }

    @Override
    public TblPcpclaim performActionOnPcp(String pcpClaimCode, String toStatus, TblUser tblUser) {
        int update = tblPcpclaimRepo.performAction(new BigDecimal(toStatus), Long.valueOf(pcpClaimCode));
        if (update > 0) {

            TblPcpclaim tblPcpclaim = tblPcpclaimRepo.findById(Long.valueOf(pcpClaimCode)).orElse(null);
            TblPcpclaim tblPcpclaim1 = new TblPcpclaim();
            TblPcplog tblPcplog = new TblPcplog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);
            tblPcpclaim1.setPcpclaimcode(Long.valueOf(pcpClaimCode));

            tblPcplog.setCreatedon(new Date());
            tblPcplog.setDescr(tblStatus.getDescr());
            tblPcplog.setUsercode(tblUser.getUsercode());
            tblPcplog.setTblPcpclaim(tblPcpclaim1);

            tblPcplog = tblPcplogRepo.save(tblPcplog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblPcpclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("Action taken on  case number :: " + tblPcpclaim.getPcpcode());
            tblEmailIntroducer.setEmailsubject("HDR CASE" + tblPcpclaim.getPcpcode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Solicitor
            TblPcpsolicitor tblHdrsolicitor = tblPcpsolicitorRepo.findByTblPcpclaimPcpclaimcodeAndStatus(tblPcpclaim.getPcpclaimcode(), "Y");
            if (tblHdrsolicitor != null) {
                TblUser solicitorUser = tblUsersRepo.findById(tblHdrsolicitor.getUsercode()).orElse(null);
                TblEmail tblEmailSolicitor = new TblEmail();
                tblEmailSolicitor.setSenflag(new BigDecimal(0));
                tblEmailSolicitor.setEmailaddress(solicitorUser.getUsername());
                tblEmailSolicitor.setEmailbody("Action taken on  case number :: " + tblPcpclaim.getPcpcode());
                tblEmailSolicitor.setEmailsubject("HDR CASE" + tblPcpclaim.getPcpcode());
                tblEmailSolicitor.setCreatedon(new Date());

                tblEmailSolicitor = tblEmailRepo.save(tblEmailSolicitor);
            }

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("Action taken on  case number :: " + tblPcpclaim.getPcpcode());
            tblEmail.setEmailsubject("HDR CASE" + tblPcpclaim.getPcpcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return tblPcpclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblPcpclaim findPcpCaseByIdWithoutUser(long rtaCode) {
        return tblPcpclaimRepo.findById(rtaCode).orElse(null);
    }

    @Override
    public TblCompanyprofile getCompanyProfileAgainstPcpCode(long pcpclaimcode) {
        return tblCompanyprofileRepo.getCompanyProfileAgainstPcpCode(pcpclaimcode);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndAgenatureAndCountrytypeAndTblCompaignCompaigncode(companycode, ageNature, countryType, null);
    }

    @Override
    public String getPcpDbColumnValue(String key, long pcpclaimcode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + pcpclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT FIRSTNAME || ' ' || NVL(MIDDLENAME,'') || ' ' || NVL(LASTNAME, '') from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + pcpclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT POSTALCODE || ' ' || ADDRESS1 || ' ' || ADDRESS2  || ' ' || ADDRESS3 || ' ' || CITY || ' ' || REGION from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + pcpclaimcode;
            query = em.createNativeQuery(sql);
        } else {
            String sql = "SELECT " + key + " from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + pcpclaimcode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")
        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    @Override
    public TblPcpdocument saveTblPcpDocument(TblPcpdocument tblPcpdocument) {
        return tblPcpdocumentRepo.save(tblPcpdocument);
    }

    @Override
    public TblPcptask getPcpTaskByPcpCodeAndTaskCode(long pcpclaimcode, long taskCode) {
        return tblPcptaskRepo.findByTblPcpclaimPcpclaimcodeAndTaskcode(pcpclaimcode, new BigDecimal(taskCode));
    }

    @Override
    public TblPcptask updateTblPcpTask(TblPcptask tblPcptask) {
        return tblPcptaskRepo.saveAndFlush(tblPcptask);
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public List<TblPcpdocument> saveTblPcpDocuments(List<TblPcpdocument> tblPcpdocuments) {
        return tblPcpdocumentRepo.saveAll(tblPcpdocuments);
    }

    @Override
    public List<TblPcptask> findTblPcpTaskByPcpClaimCode(long pcpclaimcode) {
        return tblPcptaskRepo.findByTblPcpclaimPcpclaimcode(pcpclaimcode);
    }

    @Override
    public List<PcpCaseList> getAuthPcpCasesIntroducers(String usercode) {
        List<PcpCaseList> pcpCaseLists = new ArrayList<>();
        List<Object> pcpCasesListForIntroducers = tblPcpclaimRepo.getPcpCasesListForIntroducers(usercode);
        PcpCaseList pcpCaseList;
        if (pcpCasesListForIntroducers != null && pcpCasesListForIntroducers.size() > 0) {
            for (Object record : pcpCasesListForIntroducers) {
                pcpCaseList = new PcpCaseList();
                Object[] row = (Object[]) record;

                pcpCaseList.setPcpClaimCode((BigDecimal) row[0]);
                pcpCaseList.setCreated((String) row[1]);
                pcpCaseList.setCode((String) row[2]);
                pcpCaseList.setClient((String) row[3]);
                pcpCaseList.setTaskDue((String) row[4]);
                pcpCaseList.setTaskName((String) row[5]);
                pcpCaseList.setStatus((String) row[6]);
                pcpCaseList.setEmail((String) row[7]);
                pcpCaseList.setAddress((String) row[8]);
                pcpCaseList.setContactNo((String) row[9]);
                pcpCaseList.setLastUpdated((Date) row[10]);
                pcpCaseList.setIntroducer((String) row[11]);
                pcpCaseList.setLastNote((Date) row[12]);
                pcpCaseLists.add(pcpCaseList);
            }
        }
        if (pcpCaseLists != null && pcpCaseLists.size() > 0) {
            return pcpCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<PcpCaseList> getAuthPcpCasesSolicitors(String usercode) {
        List<PcpCaseList> pcpCaseLists = new ArrayList<>();
        List<Object> pcpCasesListForSolicitor = tblPcpclaimRepo.getPcpCasesListForSolicitor(usercode);
        PcpCaseList pcpCaseList;
        if (pcpCasesListForSolicitor != null && pcpCasesListForSolicitor.size() > 0) {
            for (Object record : pcpCasesListForSolicitor) {
                pcpCaseList = new PcpCaseList();
                Object[] row = (Object[]) record;

                pcpCaseList.setPcpClaimCode((BigDecimal) row[0]);
                pcpCaseList.setCreated((String) row[1]);
                pcpCaseList.setCode((String) row[2]);
                pcpCaseList.setClient((String) row[3]);
                pcpCaseList.setTaskDue((String) row[4]);
                pcpCaseList.setTaskName((String) row[5]);
                pcpCaseList.setStatus((String) row[6]);
                pcpCaseList.setEmail((String) row[7]);
                pcpCaseList.setAddress((String) row[8]);
                pcpCaseList.setContactNo((String) row[9]);
                pcpCaseList.setLastUpdated((Date) row[10]);
                pcpCaseList.setIntroducer((String) row[11]);
                pcpCaseList.setLastNote((Date) row[12]);
                pcpCaseLists.add(pcpCaseList);
            }
        }
        if (pcpCaseLists != null && pcpCaseLists.size() > 0) {
            return pcpCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<PcpCaseList> getAuthPcpCasesLegalAssist() {
        List<PcpCaseList> pcpCaseLists = new ArrayList<>();
        List<Object> pcpCasesList = tblPcpclaimRepo.getPcpCasesList();
        PcpCaseList pcpCaseList;
        if (pcpCasesList != null && pcpCasesList.size() > 0) {
            for (Object record : pcpCasesList) {
                pcpCaseList = new PcpCaseList();
                Object[] row = (Object[]) record;

                pcpCaseList.setPcpClaimCode((BigDecimal) row[0]);
                pcpCaseList.setCreated((String) row[1]);
                pcpCaseList.setCode((String) row[2]);
                pcpCaseList.setClient((String) row[3]);
                pcpCaseList.setTaskDue((String) row[4]);
                pcpCaseList.setTaskName((String) row[5]);
                pcpCaseList.setStatus((String) row[6]);
                pcpCaseList.setEmail((String) row[7]);
                pcpCaseList.setAddress((String) row[8]);
                pcpCaseList.setContactNo((String) row[9]);
                pcpCaseList.setLastUpdated((Date) row[10]);
                pcpCaseList.setIntroducer((String) row[11]);
                pcpCaseList.setLastNote((Date) row[12]);
                pcpCaseLists.add(pcpCaseList);
            }
        }
        if (pcpCaseLists != null && pcpCaseLists.size() > 0) {
            return pcpCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<PcpStatusCountList> getAllPcpStatusCountsForIntroducers() {
        List<PcpStatusCountList> pcpStatusCountLists = new ArrayList<>();
        List<Object> pcpStatusCountListObject = tblPcpclaimRepo.getAllPcpStatusCountsForIntroducers();
        PcpStatusCountList pcpStatusCountList;
        if (pcpStatusCountListObject != null && pcpStatusCountListObject.size() > 0) {
            for (Object record : pcpStatusCountListObject) {
                pcpStatusCountList = new PcpStatusCountList();
                Object[] row = (Object[]) record;

                pcpStatusCountList.setStatusCount((BigDecimal) row[0]);
                pcpStatusCountList.setStatusName((String) row[1]);
                pcpStatusCountList.setStatusCode((BigDecimal) row[2]);

                pcpStatusCountLists.add(pcpStatusCountList);
            }
        }
        if (pcpStatusCountLists != null && pcpStatusCountLists.size() > 0) {
            return pcpStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<PcpStatusCountList> getAllPcpStatusCountsForSolicitor() {
        List<PcpStatusCountList> pcpStatusCountLists = new ArrayList<>();
        List<Object> pcpStatusCountListObject = tblPcpclaimRepo.getAllPcpStatusCountsForSolicitors();
        PcpStatusCountList pcpStatusCountList;
        if (pcpStatusCountListObject != null && pcpStatusCountListObject.size() > 0) {
            for (Object record : pcpStatusCountListObject) {
                pcpStatusCountList = new PcpStatusCountList();
                Object[] row = (Object[]) record;

                pcpStatusCountList.setStatusCount((BigDecimal) row[0]);
                pcpStatusCountList.setStatusName((String) row[1]);
                pcpStatusCountList.setStatusCode((BigDecimal) row[2]);

                pcpStatusCountLists.add(pcpStatusCountList);
            }
        }
        if (pcpStatusCountLists != null && pcpStatusCountLists.size() > 0) {
            return pcpStatusCountLists;
        } else {
            return null;
        }
    }
}
