package com.laportal.service.ol;

import com.laportal.dto.OlCaseList;
import com.laportal.dto.OlStatusCountList;
import com.laportal.dto.PerformHotKeyOnRtaRequest;
import com.laportal.dto.RtaAuditLogResponse;
import com.laportal.model.*;

import java.util.List;

public interface OlService {


    TblOlclaim findOlCaseById(long OlCaseId, TblUser tblUser);

    TblCompanyprofile getCompanyProfile(String companycode);

    List<OlStatusCountList> getAllOlStatusCounts(String companycode, String campaignCode);

    List<OlCaseList> getOlCasesByStatus(String usercode, String status, String categorycode);

    List<TblOllog> getOlCaseLogs(String Olcode);

    List<TblOlnote> getOlCaseNotes(String Olcode, TblUser tblUser);

    List<TblOlmessage> getOlCaseMessages(String Olcode);

    boolean isOlCaseAllowed(String companycode, String s);

    TblOlclaim saveOlRequest(TblOlclaim tblOlclaim);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    TblOlnote addTblOlNote(TblOlnote tblOlnote);

    TblOlmessage getTblOlMessageById(String Olmessagecode);

    TblEmail saveTblEmail(TblEmail tblEmail);

    TblOlclaim performRejectCancelActionOnOl(String OlClaimCode, String toStatus, String reason, TblUser tblUser);

    TblOlclaim performActionOnOl(String OlClaimCode, String toStatus, TblUser tblUser);

    TblOlclaim findOlCaseByIdWithoutUser(long rtaCode);

    TblCompanyprofile getCompanyProfileAgainstOlCode(long Olclaimcode);

    TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType);

    String getOlDbColumnValue(String key, long Olclaimcode);

    TblOltask getOlTaskByOlCodeAndTaskCode(long Olclaimcode, long taskCode);

    int updateTblOlTask(TblOltask tblOltask);

    TblTask getTaskAgainstCode(long taskCode);

    List<TblOltask> findTblOlTaskByOlClaimCode(long Olclaimcode);

    List<OlStatusCountList> getAllOlStatusCountsForIntroducers();

    List<OlStatusCountList> getAllOlStatusCountsForSolicitor();

    List<TblOldocument> saveTblOlDocuments(List<TblOldocument> tblOldocuments);

    TblOldocument saveTblOlDocument(TblOldocument tblOldocument);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblOlclaim tblOlclaim,
                      TblUser tblUser);

    TblEmail resendEmail(String olmessagecode);

    TblOlclaim performActionOnOlFromDirectIntro(PerformHotKeyOnRtaRequest performHotKeyOnElRequest, TblUser tblUser);

    List<TblOltask> getOlTaskAgainstOLCodeAndStatus(String olCode, String status);

    TblOlclaim performRevertActionOnOl(String olCode, String toStatus, String reason, TblUser tblUser);

    List<TblOldocument> getAuthOlCasedocuments(long olclaimcode);

    TblCompanydoc getCompanyDocs(String companycode);

    TblOlclaim updateOlCase(TblOlclaim tblOlclaim);

    TblOldocument addOlSingleDocumentSingle(TblOldocument tblOldocument);

    TblUser findByUserId(String userCode);

    TblEmailTemplate findByEmailType(String emailType);

    void saveEmail(String email, String clientbody, String no_win_no_fee_agreement_, TblOlclaim tblOlclaim, TblUser legalUser, String path);

    TblOlsolicitor getOLSolicitorsOfOlclaim(long olClaimCode);

    List<TblOldocument> addOlDocument(List<TblOldocument> tblEldocuments);

    int updateCurrentTask(long taskCode, long olCode, String current);

    void deleteOlDocument(long doccode);

    List<TblOlnote> getAuthOlCaseNotesOfLegalInternal(String olcode, TblUser tblUser);

    List<RtaAuditLogResponse> getOlAuditLogs(Long olCode);

    TblOlclaim updateOlRequest(TblOlclaim tblOlclaim);

    List<OlCaseList> getAuthOlCasesUserAndCategoryWise(String usercode, String categorycode);
}
