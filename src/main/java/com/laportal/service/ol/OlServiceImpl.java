package com.laportal.service.ol;

import com.laportal.Repo.*;
import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class OlServiceImpl extends AbstractApi implements OlService {

    @PersistenceContext
    @Autowired
    EntityManager em;
    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;
    @Autowired
    private TblOlclaimRepo tblOlclaimRepo;
    @Autowired
    private TblOllogRepo tblOllogRepo;
    @Autowired
    private TblOlmessageRepo tblOlmessageRepo;
    @Autowired
    private TblOlnoteRepo tblOlnoteRepo;
    @Autowired
    private TblOlsolicitorRepo tblOlsolicitorRepo;
    @Autowired
    private TblTaskRepo tblTaskRepo;
    @Autowired
    private TblUsersRepo tblUsersRepo;
    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;
    @Autowired
    private TblEmailRepo tblEmailRepo;
    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;
    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;
    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;
    @Autowired
    private TblOldocumentRepo tblOldocumentRepo;
    @Autowired
    private TblOltaskRepo tblOltaskRepo;
    @Autowired
    private TblEmailTemplateRepo tblEmailTemplateRepo;
    @Autowired
    private TblSolicitorInvoiceDetailRepo tblSolicitorInvoiceDetailRepo;
    @Autowired
    private TblEsignStatusRepo tblEsignStatusRepo;
    @Autowired
    private ViewOlclaimRepo viewOlclaimRepo;
    @Autowired
    private ViewStatuscountRepo viewStatuscountRepo;
    @Autowired
    private TblRtaflowRepo tblRtaflowRepo;


    @Override
    public TblOlclaim findOlCaseById(long OlCaseId, TblUser tblUser) {
        TblOlclaim tblOlclaim = tblOlclaimRepo.findById(OlCaseId).orElse(null);
        TblRtastatus tblRtastatus = tblRtastatusRepo.findById(tblOlclaim.getStatus().longValue()).orElse(null);
        if (tblOlclaim != null) {

            List<TblOldocument> tblOldocuments = tblOldocumentRepo.findByTblOlclaimOlclaimcode(OlCaseId);
            if (tblOldocuments != null && tblOldocuments.size() > 0) {
                tblOlclaim.setTblOldocuments(tblOldocuments);
            }

            List<RtaActionButton> rtaActionButtons = new ArrayList<>();
            List<RtaActionButton> rtaActionButtonsForLA = new ArrayList<>();
            RtaActionButton rtaActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    tblOlclaim.getStatus().longValue(), "7",
                    tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetailForLA = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    tblOlclaim.getStatus().longValue(), "7",
                    tblCompanyprofile.getTblUsercategory().getCategorycode());

            TblRtaflow tblRtaflow = tblRtaflowRepo.findByTblCompaignCompaigncodeAndTblRtastatusStatuscode("7", tblOlclaim.getStatus().longValue());
            if (tblRtaflow != null) {
                tblOlclaim.setEditFlag(tblRtaflow.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode()) ? tblRtaflow.getEditflag() : "N");
            }

            /// for introducer and solicitor
            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    rtaActionButton = new RtaActionButton();
                    rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                    rtaActionButtons.add(rtaActionButton);
                }

                // for LEGAL INTERNAL action buttons in dropdown

                if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                        rtaActionButton = new RtaActionButton();
                        rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                        rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                        rtaActionButtonsForLA.add(rtaActionButton);
                    }
                    tblOlclaim.setOlActionButtonForLA(rtaActionButtonsForLA);

                    if (tblRtaflowdetailForLA.get(0).getTblRtaflow().getTaskflag().equalsIgnoreCase("Y")) {
                        List<TblOltask> tblOltasks = tblOltaskRepo.findByTblOlclaimOlclaimcode(OlCaseId);
                        tblOlclaim.setTblOltasks(tblOltasks);
                    }
                }


                tblOlclaim.setOlActionButtons(rtaActionButtons);
                tblOlclaim.setStatusDescr(tblRtastatus.getDescr());

                if (tblOlclaim.getIntroducer() != null && tblOlclaim.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblOlclaim.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo
                            .findById(String.valueOf(tblOlclaim.getIntroducer())).orElse(null);

                    TblOlsolicitor tblOlsolicitor = tblOlsolicitorRepo
                            .findByTblOlclaimOlclaimcodeAndStatus(OlCaseId, "Y");
                    if (tblOlsolicitor != null) {
                        tblOlclaim.setSolicitorcompany(tblCompanyprofileRepo
                                .findById(String.valueOf(tblOlsolicitor.getCompanycode())).orElse(null).getName());
                        tblOlclaim.setSolicitorusername(tblUsersRepo
                                .findByUsercode(String.valueOf(tblOlsolicitor.getUsercode())).getLoginid());
                    }
                    tblOlclaim.setAdvisorname(user == null ? null : user.getLoginid());
                    tblOlclaim.setIntroducername(companyprofile == null ? null : companyprofile.getName());
                }

                List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("9", new BigDecimal(tblOlclaim.getOlclaimcode()));
                if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                    for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                        TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                        if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                            tblOlclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblOlclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                            tblOlclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblOlclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        }

                    }
                }

                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("7", new BigDecimal(tblOlclaim.getOlclaimcode()));
                tblOlclaim.setTblEsignStatus(tblEsignStatus);

                return tblOlclaim;

            } else {

                if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                        rtaActionButton = new RtaActionButton();
                        rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                        rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                        rtaActionButtonsForLA.add(rtaActionButton);
                    }
                    tblOlclaim.setOlActionButtons(rtaActionButtonsForLA);

                    if (tblRtaflowdetailForLA.get(0).getTblRtaflow().getTaskflag().equalsIgnoreCase("Y")) {
                        List<TblOltask> tblEltasks = tblOltaskRepo.findByTblOlclaimOlclaimcode(OlCaseId);
                        tblOlclaim.setTblOltasks(tblEltasks);
                    }
                }

                if (tblOlclaim.getIntroducer() != null && tblOlclaim.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblOlclaim.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo
                            .findById(String.valueOf(tblOlclaim.getIntroducer())).orElse(null);

                    TblOlsolicitor tblElsolicitor = tblOlsolicitorRepo
                            .findByTblOlclaimOlclaimcodeAndStatus(OlCaseId, "Y");
                    if (tblElsolicitor != null) {
                        tblOlclaim.setSolicitorcompany(tblCompanyprofileRepo
                                .findById(String.valueOf(tblElsolicitor.getCompanycode())).orElse(null).getName());
                        tblOlclaim.setSolicitorusername(tblUsersRepo
                                .findByUsercode(String.valueOf(tblElsolicitor.getUsercode())).getLoginid());
                    }
                    tblOlclaim.setAdvisorname(user.getLoginid());
                    tblOlclaim.setIntroducername(companyprofile.getName());
                }


                List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("7", new BigDecimal(tblOlclaim.getOlclaimcode()));
                if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                    for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                        TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                        if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                            tblOlclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblOlclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                            tblOlclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblOlclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        }

                    }
                }
                tblOlclaim.setStatusDescr(tblRtastatus.getDescr());

                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("7", new BigDecimal(tblOlclaim.getOlclaimcode()));
                tblOlclaim.setTblEsignStatus(tblEsignStatus);


                return tblOlclaim;
            }
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public List<OlStatusCountList> getAllOlStatusCounts(String companycode, String campaignCode) {
        List<OlStatusCountList> OlStatusCountLists = new ArrayList<>();
        List<ViewStatuscount> OlStatusCountListObject = viewStatuscountRepo.getStatusCount(Long.valueOf(companycode), Long.valueOf(campaignCode));
        OlStatusCountList OlStatusCountList;
        if (OlStatusCountListObject != null && OlStatusCountListObject.size() > 0) {
            for (ViewStatuscount viewStatuscount : OlStatusCountListObject) {
                OlStatusCountList = new OlStatusCountList();


                OlStatusCountList.setStatusCount(viewStatuscount.getStatuscount());
                OlStatusCountList.setStatusName(viewStatuscount.getDescr());
                OlStatusCountList.setStatusCode(viewStatuscount.getStatuscode());

                OlStatusCountLists.add(OlStatusCountList);
            }
        }
        if (OlStatusCountLists != null && OlStatusCountLists.size() > 0) {
            return OlStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<OlCaseList> getOlCasesByStatus(String usercode, String status, String categorycode) {
        List<OlCaseList> OlCaseLists = new ArrayList<>();
        List<ViewOlclaim> OlCasesListForIntroducers = viewOlclaimRepo.getAuthOlCasesUserAndCategoryWiseAndStatusWise(new BigDecimal(usercode), new BigDecimal(categorycode), new BigDecimal(status));
        OlCaseList OlCaseList;
        if (OlCasesListForIntroducers != null && OlCasesListForIntroducers.size() > 0) {
            for (ViewOlclaim viewOlclaim : OlCasesListForIntroducers) {
                OlCaseList = new OlCaseList();


                OlCaseList.setOlClaimCode(viewOlclaim.getOlclaimcode());
                OlCaseList.setCreated(viewOlclaim.getCreatedate());
                OlCaseList.setCode(viewOlclaim.getOlcode());
                OlCaseList.setClient(viewOlclaim.getFirstname() + (viewOlclaim.getMiddlename() != null ? viewOlclaim.getMiddlename() : "") + viewOlclaim.getLastname());
                OlCaseList.setTaskName(viewOlclaim.getOltaskname());
                OlCaseList.setStatus(viewOlclaim.getStatus());
//                OlCaseList.setEmail(viewOlclaim.get);
                OlCaseList.setAddress(viewOlclaim.getPostalcode() + " " + viewOlclaim.getAddress1() + " " + viewOlclaim.getAddress2() + " " + viewOlclaim.getAddress3());
                OlCaseList.setContactNo(viewOlclaim.getMobile());
                OlCaseList.setLastUpdated(viewOlclaim.getUpdatedate());
                OlCaseList.setIntroducer(viewOlclaim.getIntroducerUser());
//                OlCaseList.setLastNote((Date) row[12]);
                OlCaseLists.add(OlCaseList);
            }
        }
        if (OlCaseLists != null && OlCaseLists.size() > 0) {
            return OlCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblOllog> getOlCaseLogs(String Olcode) {
        List<TblOllog> tblOllogs = tblOllogRepo.findByTblOlclaimOlclaimcode(Long.valueOf(Olcode));
        if (tblOllogs != null && tblOllogs.size() > 0) {
            for (TblOllog tblOllog : tblOllogs) {
                TblUser tblUser = tblUsersRepo.findById(tblOllog.getUsercode()).orElse(null);
                tblOllog.setUserName(tblUser.getLoginid());
            }
            return tblOllogs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblOlnote> getOlCaseNotes(String Olcode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblOlnote> tblOlnotes = tblOlnoteRepo.findNotesOnOlbyUserAndCategory(Long.valueOf(Olcode),
                tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (tblOlnotes != null && tblOlnotes.size() > 0) {
            for (TblOlnote tblElnote : tblOlnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblElnote.getUsercode()).orElse(null);
                tblElnote.setUserName(tblUser1.getLoginid());
                tblElnote.setSelf(tblElnote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
                if (!tblElnote.getUsercode().equalsIgnoreCase(tblUser.getUsercode())) {
                    TblCompanyprofile recevingTblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
                    tblElnote.setUsercategorycode(recevingTblCompanyprofile.getTblUsercategory().getCategorycode());
                }
            }

            return tblOlnotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblOlmessage> getOlCaseMessages(String Olcode) {
        List<TblOlmessage> tblOlmessageList = tblOlmessageRepo.findByTblOlclaimOlclaimcode(Long.valueOf(Olcode));
        if (tblOlmessageList != null && tblOlmessageList.size() > 0) {
            for (TblOlmessage tblOlmessage : tblOlmessageList) {
                TblUser tblUser = tblUsersRepo.findById(tblOlmessage.getUsercode()).orElse(null);
                tblOlmessage.setUserName(tblUser.getUsername());
            }
            return tblOlmessageList;
        } else {
            return null;
        }
    }

    @Override
    public boolean isOlCaseAllowed(String companycode, String campaignCode) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companycode,
                campaignCode);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TblOlclaim saveOlRequest(TblOlclaim tblOlclaim) {
        tblOlclaim = tblOlclaimRepo.save(tblOlclaim);
        TblOllog tblOllog = new TblOllog();

        TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(559))
                .orElse(null);


        tblOllog.setCreatedon(new Date());
        tblOllog.setDescr(tblStatus.getDescr());
        tblOllog.setUsercode(tblOlclaim.getCreateuser().toString());
        tblOllog.setTblOlclaim(tblOlclaim);
        tblOllog = tblOllogRepo.save(tblOllog);
        return tblOlclaim;
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public TblOlnote addTblOlNote(TblOlnote tblOlnote) {
        tblOlnote = tblOlnoteRepo.save(tblOlnote);
        TblOlclaim tblOlclaim = tblOlclaimRepo.findById(tblOlnote.getTblOlclaim().getOlclaimcode()).orElse(null);
        TblUser legalUser = tblUsersRepo.findByUsercode("2");
        // for legal internal

        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("note");
        String legalbody = tblEmailTemplate.getEmailtemplate();

        legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
        legalbody = legalbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
        legalbody = legalbody.replace("[CLIENT_NAME]",
                tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
        legalbody = legalbody.replace("[NOTE]", tblOlnote.getNote());
        legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

        saveEmail(legalUser.getUsername(), legalbody, tblOlclaim.getOlcode() + " | Processing Note",
                tblOlclaim, legalUser);

        if (!tblOlnote.getUsercategorycode().equals("4")) {
            // for introducer
            if (tblOlnote.getUsercategorycode().equals("1")) {
                TblUser introducerUser = tblUsersRepo
                        .findByUsercode(String.valueOf(tblOlclaim.getAdvisor()));
                String introducerBody = tblEmailTemplate.getEmailtemplate();

                introducerBody = introducerBody.replace("[USER_NAME]", introducerUser.getLoginid());
                introducerBody = introducerBody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
                introducerBody = introducerBody.replace("[CLIENT_NAME]",
                        tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
                introducerBody = introducerBody.replace("[NOTE]", tblOlnote.getNote());
                introducerBody = introducerBody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

                saveEmail(introducerUser.getUsername(), introducerBody,
                        tblOlclaim.getOlcode() + " | Processing Note", tblOlclaim,
                        introducerUser);
            } else if (tblOlnote.getUsercategorycode().equals("2")) {

                TblOlsolicitor tblOlsolicitor = tblOlsolicitorRepo
                        .findByTblOlclaimOlclaimcodeAndStatus(tblOlclaim.getOlclaimcode(), "Y");

                if (tblOlsolicitor != null) {
                    TblUser solicitorUser = tblUsersRepo.findById(tblOlsolicitor.getUsercode()).orElse(null);

                    String solicitorBody = tblEmailTemplate.getEmailtemplate();

                    solicitorBody = solicitorBody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorBody = solicitorBody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
                    solicitorBody = solicitorBody.replace("[CLIENT_NAME]",
                            tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
                    solicitorBody = solicitorBody.replace("[NOTE]", tblOlnote.getNote());
                    solicitorBody = solicitorBody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

                    saveEmail(solicitorUser.getUsername(), solicitorBody,
                            tblOlclaim.getOlcode() + " | Processing Note", tblOlclaim,
                            solicitorUser);

                }
            }

        }

        return tblOlnote;
    }

    @Override
    public TblOlmessage getTblOlMessageById(String Olmessagecode) {
        return tblOlmessageRepo.findById(Long.valueOf(Olmessagecode)).orElse(null);
    }

    @Override
    public TblEmail saveTblEmail(TblEmail tblEmail) {
        return tblEmailRepo.save(tblEmail);
    }


    @Override
    public TblOlclaim performRejectCancelActionOnOl(String OlClaimCode, String toStatus, String reason, TblUser tblUser) {

        TblOlclaim tblOlclaim = tblOlclaimRepo.findById(Long.valueOf(OlClaimCode)).orElse(null);
        TblOlsolicitor tblOlsolicitor = tblOlsolicitorRepo.findByTblOlclaimOlclaimcodeAndStatus(Long.valueOf(OlClaimCode), "Y");
        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status-Reject");
        TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

        if (tblOlsolicitor != null) {
            int update = tblOlsolicitorRepo.updateRemarksReason(reason, "N", Long.parseLong(OlClaimCode));


            TblUser SolicitorUser = tblUsersRepo.findById(tblOlsolicitor.getUsercode()).orElse(null);
            TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblOlsolicitor.getCompanycode())
                    .orElse(null);

            TblOlnote tblOlnote = new TblOlnote();
            // NOte for Legal internal user and NOTE for introducer
            tblOlnote.setTblOlclaim(tblOlclaim);
            tblOlnote.setNote(SolicitorCompany.getName() + " has rejected the case. Reason :" + reason);
            tblOlnote.setUsercategorycode("1");
            tblOlnote.setCreatedon(new Date());
            tblOlnote.setUsercode("1");

            addTblOlNote(tblOlnote);

            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            // Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(tblOlclaim.getAdvisor())).orElse(null);

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
            introducerbody = introducerbody.replace("[SOLICITOR_NAME]", SolicitorCompany.getName());
            introducerbody = introducerbody.replace("[REASON]", reason);
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

            saveEmail(introducerUser.getUsername(), introducerbody, tblOlclaim.getOlcode() + " | " + tblStatus.getDescr(),
                    tblOlclaim, introducerUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
            legalbody = legalbody.replace("[SOLICITOR_NAME]", SolicitorCompany.getName());
            legalbody = legalbody.replace("[REASON]", reason);
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

            saveEmail(legalUser.getUsername(), legalbody, tblOlclaim.getOlcode() + " | " + tblStatus.getDescr(),
                    tblOlclaim, introducerUser);

        } else {

            TblOlnote tblOlnote = new TblOlnote();
            // NOte for Legal internal user and NOTE for introducer
            tblOlnote.setTblOlclaim(tblOlclaim);
            tblOlnote.setNote(tblUser.getLoginid() + " has rejected the case. Reason :" + reason);
            tblOlnote.setUsercategorycode("1");
            tblOlnote.setCreatedon(new Date());
            tblOlnote.setUsercode("1");

            addTblOlNote(tblOlnote);

            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(tblOlclaim.getAdvisor())).orElse(null);

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
            introducerbody = introducerbody.replace("[SOLICITOR_NAME]", tblUser.getLoginid());
            introducerbody = introducerbody.replace("[REASON]", reason);
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

            saveEmail(introducerUser.getUsername(), introducerbody, tblOlclaim.getOlcode() + " | " + tblStatus.getDescr(),
                    tblOlclaim, introducerUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
            legalbody = legalbody.replace("[SOLICITOR_NAME]", tblUser.getLoginid());
            legalbody = legalbody.replace("[REASON]", reason);
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

            saveEmail(legalUser.getUsername(), legalbody, tblOlclaim.getOlcode() + " | " + tblStatus.getDescr(),
                    tblOlclaim, introducerUser);
        }

        TblOllog tblOllog = new TblOllog();


        tblOllog.setCreatedon(new Date());
        tblOllog.setDescr(tblStatus.getDescr());
        tblOllog.setUsercode(tblUser.getUsercode());
        tblOllog.setTblOlclaim(tblOlclaim);
        tblOllog.setOldstatus(tblOlclaim.getStatus().longValue());
        tblOllog.setNewstatus(Long.valueOf(toStatus));

        tblOllog = tblOllogRepo.save(tblOllog);
        int update = tblOlclaimRepo.performRejectCancelAction(reason, toStatus, tblOlclaim.getOlclaimcode(), tblUser.getUsercode());

        return tblOlclaim;
    }

    @Override
    public TblOlclaim performActionOnOl(String OlClaimCode, String toStatus, TblUser tblUser) {
        int update = tblOlclaimRepo.performAction(new BigDecimal(toStatus), Long.valueOf(OlClaimCode), tblUser.getUsercode());
        if (update > 0) {

            TblOlclaim tblOlclaim = tblOlclaimRepo.findById(Long.valueOf(OlClaimCode)).orElse(null);
            TblOllog tblOllog = new TblOllog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblOllog.setCreatedon(new Date());
            tblOllog.setDescr(tblStatus.getDescr());
            tblOllog.setUsercode(tblUser.getUsercode());
            tblOllog.setTblOlclaim(tblOlclaim);
            tblOllog.setOldstatus(tblOlclaim.getStatus().longValue());
            tblOllog.setNewstatus(Long.valueOf(toStatus));

            tblOllog = tblOllogRepo.save(tblOllog);
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status");

            // for introducer
            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblOlclaim.getAdvisor()));
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, introducerUser);

            // for legal internal
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
            legalbody = legalbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, legalUser);

            if (toStatus.equals(559)) {
                // when reverting a case takes to new status remove all solcitors from case if
                // any
                int updateSolicitor = tblOlsolicitorRepo.updateSolicitor("N", Long.parseLong(OlClaimCode));
                TblOlsolicitor tblOlsolicitor = tblOlsolicitorRepo
                        .findByTblOlclaimOlclaimcodeAndStatus(tblOlclaim.getOlclaimcode(), "Y");
                if (tblOlsolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblOlsolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            } else {
                // for normal emails
                TblOlsolicitor tblOlsolicitor = tblOlsolicitorRepo
                        .findByTblOlclaimOlclaimcodeAndStatus(tblOlclaim.getOlclaimcode(), "Y");
                if (tblOlsolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblOlsolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            }

            // if submmitting a case add clawback date current date + 90 days
            TblOlclaim tblElclaimForClawBack = tblOlclaimRepo.findById(tblOlclaim.getOlclaimcode()).orElse(null);
            if (toStatus.equals("565")) {
                Date clawbackDate = addDaysToDate(90);
                tblOlclaimRepo.updateClawbackdate(clawbackDate, tblOlclaim.getOlclaimcode());
                tblOlclaimRepo.updateSubmitDate(new Date(), tblOlclaim.getOlclaimcode());
            } else if (toStatus.equals("576")) {
                Date clawbackDate = addDaysToSpecificDate(tblElclaimForClawBack.getClawbackDate(), 30);
                tblOlclaimRepo.updateClawbackdate(clawbackDate, tblOlclaim.getOlclaimcode());
            } else if (toStatus.equals("578")) {
                Date clawbackDate = addDaysToSpecificDate(tblElclaimForClawBack.getClawbackDate(), 60);
                tblOlclaimRepo.updateClawbackdate(clawbackDate, tblOlclaim.getOlclaimcode());
            } else if (toStatus.equals("579")) {
                Date clawbackDate = addDaysToSpecificDate(tblElclaimForClawBack.getClawbackDate(), 90);
                tblOlclaimRepo.updateClawbackdate(clawbackDate, tblOlclaim.getOlclaimcode());
            } else if (toStatus.equals("563")) {
                TblOlnote tblOlnote = new TblOlnote();
                TblOlsolicitor tblOlsolicitor = tblOlsolicitorRepo
                        .findByTblOlclaimOlclaimcodeAndStatus(tblOlclaim.getOlclaimcode(), "Y");
                TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblOlsolicitor.getCompanycode())
                        .orElse(null);
                // NOte for Legal internal user and NOTE for introducer
                tblOlnote.setTblOlclaim(tblOlclaim);
                tblOlnote.setNote("Case Accepted By " + SolicitorCompany.getName());
                tblOlnote.setUsercategorycode("1");
                tblOlnote.setCreatedon(new Date());
                tblOlnote.setUsercode("1");

                addTblOlNote(tblOlnote);
            }
            return tblOlclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblOlclaim findOlCaseByIdWithoutUser(long rtaCode) {
        return tblOlclaimRepo.findById(rtaCode).orElse(null);
    }

    @Override
    public TblCompanyprofile getCompanyProfileAgainstOlCode(long Olclaimcode) {
        return tblCompanyprofileRepo.getCompanyProfileAgainstOlCode(Olclaimcode);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndAgenatureAndCountrytypeAndTblCompaignCompaigncode(companycode, ageNature, countryType, null);
    }

    @Override
    public String getOlDbColumnValue(String key, long Olclaimcode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from Tbl_tenancyclaim  WHERE TBL_OLCLAIM = " + Olclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT FIRSTNAME || ' ' || NVL(MIDDLENAME,'') || ' ' || NVL(LASTNAME, '') from Tbl_tenancyclaim  WHERE TBL_OLCLAIM = " + Olclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT POSTALCODE || ' ' || ADDRESS1 || ' ' || ADDRESS2  || ' ' || ADDRESS3 || ' ' || CITY || ' ' || REGION from Tbl_tenancyclaim  WHERE TBL_OLCLAIM = " + Olclaimcode;
            query = em.createNativeQuery(sql);
        } else {
            String sql = "SELECT " + key + " from Tbl_tenancyclaim  WHERE TBL_OLCLAIM = " + Olclaimcode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")
        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    @Override
    public TblOldocument saveTblOlDocument(TblOldocument tblOldocument) {
        return tblOldocumentRepo.save(tblOldocument);
    }

    @Override
    public TblOltask getOlTaskByOlCodeAndTaskCode(long Olclaimcode, long taskCode) {
        return tblOltaskRepo.findByTblOlclaimOlclaimcodeAndTblTaskTaskcode(Olclaimcode, new BigDecimal(taskCode));
    }

    @Override
    public int updateTblOlTask(TblOltask tblOltask) {
        return tblOltaskRepo.updatetask(tblOltask.getStatus(), tblOltask.getRemarks(), tblOltask.getOltaskcode(), tblOltask.getTblOlclaim().getOlclaimcode());
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public List<TblOldocument> saveTblOlDocuments(List<TblOldocument> tblOldocuments) {
        return tblOldocumentRepo.saveAll(tblOldocuments);
    }

    @Override
    public List<TblOltask> findTblOlTaskByOlClaimCode(long Olclaimcode) {
        return tblOltaskRepo.findByTblOlclaimOlclaimcode(Olclaimcode);
    }

    @Override
    public List<OlStatusCountList> getAllOlStatusCountsForIntroducers() {
        List<OlStatusCountList> OlStatusCountLists = new ArrayList<>();
        List<Object> OlStatusCountListObject = tblOlclaimRepo.getAllOlStatusCountsForIntroducers();
        OlStatusCountList OlStatusCountList;
        if (OlStatusCountListObject != null && OlStatusCountListObject.size() > 0) {
            for (Object record : OlStatusCountListObject) {
                OlStatusCountList = new OlStatusCountList();
                Object[] row = (Object[]) record;

                OlStatusCountList.setStatusCount((BigDecimal) row[0]);
                OlStatusCountList.setStatusName((String) row[1]);
                OlStatusCountList.setStatusCode((BigDecimal) row[2]);

                OlStatusCountLists.add(OlStatusCountList);
            }
        }
        if (OlStatusCountLists != null && OlStatusCountLists.size() > 0) {
            return OlStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<OlStatusCountList> getAllOlStatusCountsForSolicitor() {
        List<OlStatusCountList> OlStatusCountLists = new ArrayList<>();
        List<Object> OlStatusCountListObject = tblOlclaimRepo.getAllOlStatusCountsForSolicitors();
        OlStatusCountList OlStatusCountList;
        if (OlStatusCountListObject != null && OlStatusCountListObject.size() > 0) {
            for (Object record : OlStatusCountListObject) {
                OlStatusCountList = new OlStatusCountList();
                Object[] row = (Object[]) record;

                OlStatusCountList.setStatusCount((BigDecimal) row[0]);
                OlStatusCountList.setStatusName((String) row[1]);
                OlStatusCountList.setStatusCode((BigDecimal) row[2]);

                OlStatusCountLists.add(OlStatusCountList);
            }
        }
        if (OlStatusCountLists != null && OlStatusCountLists.size() > 0) {
            return OlStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblOlclaim tblOlclaim,
                             TblUser tblUser) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblOlmessage tblOlmessage = new TblOlmessage();

        tblOlmessage.setTblOlclaim(tblOlclaim);
        tblOlmessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblOlmessage.setCreatedon(new Date());
        tblOlmessage.setMessage(tblEmail.getEmailbody());
        tblOlmessage.setSentto(tblEmail.getEmailaddress());
        tblOlmessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblOlmessage.setEmailcode(tblEmail);

        tblOlmessage = tblOlmessageRepo.save(tblOlmessage);

        return true;
    }

    @Override
    public TblEmail resendEmail(String olmessagecode) {
        TblOlmessage tblElmessage = tblOlmessageRepo.findById(Long.valueOf(olmessagecode)).orElse(null);

        TblEmail tblEmail = tblEmailRepo.findById(tblElmessage.getEmailcode().getEmailcode()).orElse(null);

        tblEmail.setSenflag(new BigDecimal(0));

        return tblEmailRepo.saveAndFlush(tblEmail);
    }

    @Override
    public TblOlclaim performActionOnOlFromDirectIntro(PerformHotKeyOnRtaRequest performHotKeyOnElRequest, TblUser tblUser) {
        TblOlclaim tblOlclaim = tblOlclaimRepo.findById(Long.valueOf(performHotKeyOnElRequest.getOlcode()))
                .orElse(null);

        int update = tblOlclaimRepo.performAction(new BigDecimal(performHotKeyOnElRequest.getToStatus()),
                Long.valueOf(performHotKeyOnElRequest.getOlcode()), tblUser.getUsercode());
        if (update > 0) {

            TblOllog tblOllog = new TblOllog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(performHotKeyOnElRequest.getToStatus()))
                    .orElse(null);

            tblOllog.setCreatedon(new Date());
            tblOllog.setDescr(tblStatus.getDescr());
            tblOllog.setUsercode(tblUser.getUsercode());
            tblOllog.setTblOlclaim(tblOlclaim);
            tblOllog.setOldstatus(tblOlclaim.getStatus().longValue());
            tblOllog.setNewstatus(Long.valueOf(performHotKeyOnElRequest.getToStatus()));

            int updateSolicitor = tblOlsolicitorRepo.updateSolicitor("N", tblOlclaim.getOlclaimcode());

            TblOlsolicitor tblOlsolicitor = new TblOlsolicitor();
            tblOlsolicitor.setCompanycode(performHotKeyOnElRequest.getSolicitorCode());
            tblOlsolicitor.setCreatedon(new Date());
            tblOlsolicitor.setTblOlclaim(tblOlclaim);
            tblOlsolicitor.setStatus("Y");
            tblOlsolicitor.setUsercode(performHotKeyOnElRequest.getSolicitorUserCode());

            tblOlsolicitor = tblOlsolicitorRepo.save(tblOlsolicitor);
            tblOllog = tblOllogRepo.save(tblOllog);

            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblOlclaim.getAdvisor()));
            TblUser solicitorUser = tblUsersRepo
                    .findByUsercode(String.valueOf(performHotKeyOnElRequest.getSolicitorUserCode()));
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status-hotkey");

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
//                    tblElclaim.getFirstname() + " "
//                            + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                            + tblElclaim.getLastname());
//            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    tblElclaim.getRtanumber() + " | HotKey", tblElclaim, introducerUser);

            // for solicitor
            String solicitorbody = tblEmailTemplate.getEmailtemplate();

            solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
            solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
            solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
//                    tblElclaim.getFirstname() + " "
//                            + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                            + tblElclaim.getLastname());
//            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

            saveEmail(solicitorUser.getUsername(), solicitorbody, tblOlclaim.getOlcode() + " | Processing Note",
                    tblOlclaim, solicitorUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() + (tblOlclaim.getMiddlename() != null ? tblOlclaim.getMiddlename() : "") + " " + tblOlclaim.getLastname());
//                    tblElclaim.getFirstname() + " "
//                            + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                            + tblElclaim.getLastname());

            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblOlclaim.getOlclaimcode());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    tblElclaim.getRtanumber() + " | HotKey", tblElclaim, legalUser);


            return tblOlclaim;

        } else {
            return null;
        }
    }

    @Override
    public List<TblOltask> getOlTaskAgainstOLCodeAndStatus(String olCode, String status) {
        return tblOltaskRepo.findByTblOlclaimOlclaimcodeAndStatus(Long.parseLong(olCode), status);
    }

    @Override
    public TblOlclaim performRevertActionOnOl(String olCode, String toStatus, String reason, TblUser tblUser) {
        return null;
    }

    @Override
    public List<TblOldocument> getAuthOlCasedocuments(long olclaimcode) {
        return tblOldocumentRepo.findByTblOlclaimOlclaimcode(olclaimcode);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndTblCompaignCompaigncode(companycode, "7");
    }

    @Override
    public TblOlclaim updateOlCase(TblOlclaim tblOlclaim) {
        return tblOlclaimRepo.saveAndFlush(tblOlclaim);
    }

    @Override
    public TblOldocument addOlSingleDocumentSingle(TblOldocument tblOldocument) {
        return tblOldocumentRepo.saveAndFlush(tblOldocument);
    }

    @Override
    public TblUser findByUserId(String userCode) {
        return tblUsersRepo.findByUsercode(userCode);
    }

    @Override
    public TblEmailTemplate findByEmailType(String emailType) {
        return tblEmailTemplateRepo.findByEmailtype(emailType);
    }

    @Override
    public void saveEmail(String emailAddress, String emailBody, String emailSubject, TblOlclaim tblOlclaim, TblUser tblUser, String attachment) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setEmailattachment(attachment);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblOlmessage tblOlmessage = new TblOlmessage();

        tblOlmessage.setTblOlclaim(tblOlclaim);
        tblOlmessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblOlmessage.setCreatedon(new Date());
        tblOlmessage.setMessage(tblEmail.getEmailbody());
        tblOlmessage.setSentto(tblEmail.getEmailaddress());
        tblOlmessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblOlmessage.setEmailcode(tblEmail);

        tblOlmessage = tblOlmessageRepo.save(tblOlmessage);

    }

    @Override
    public TblOlsolicitor getOLSolicitorsOfOlclaim(long olClaimCode) {
        return tblOlsolicitorRepo.findByTblOlclaimOlclaimcodeAndStatus(olClaimCode, "Y");
    }

    @Override
    public List<TblOldocument> addOlDocument(List<TblOldocument> tblEldocuments) {

        return tblOldocumentRepo.saveAll(tblEldocuments);
    }

    @Override
    public int updateCurrentTask(long taskCode, long olCode, String current) {
        tblOltaskRepo.setAllTask("N", olCode);
        return tblOltaskRepo.setCurrentTask(current, taskCode, olCode);
    }

    @Override
    public void deleteOlDocument(long doccode) {
        tblOldocumentRepo.deleteById(doccode);
    }

    @Override
    public List<TblOlnote> getAuthOlCaseNotesOfLegalInternal(String olcode, TblUser tblUser) {
        List<TblOlnote> tblOlnotes = tblOlnoteRepo.getAuthOlCaseNotesOfLegalInternal(Long.valueOf(olcode),
                tblUser.getUsercode());
        if (tblOlnotes != null && tblOlnotes.size() > 0) {
            for (TblOlnote tblElnote : tblOlnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblElnote.getUsercode()).orElse(null);
                tblElnote.setUserName(tblUser1.getLoginid());
                tblElnote.setSelf(tblElnote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
            }

            return tblOlnotes;
        } else {
            return null;
        }
    }

    @Override
    public List<RtaAuditLogResponse> getOlAuditLogs(Long olCode) {
        List<RtaAuditLogResponse> rtaAuditLogResponses = new ArrayList<>();
        RtaAuditLogResponse rtaAuditLogResponse = null;
        List<Object> auditlogsObject = tblOlclaimRepo.getOlAuditLogs(olCode);
        RtaStatusCountList PlStatusCountList;
        if (auditlogsObject != null && auditlogsObject.size() > 0) {
            for (Object record : auditlogsObject) {
                rtaAuditLogResponse = new RtaAuditLogResponse();
                Object[] row = (Object[]) record;

                rtaAuditLogResponse.setFieldName((String) row[0]);
                rtaAuditLogResponse.setOldValue((String) row[1]);
                rtaAuditLogResponse.setNewValue((String) row[2]);
                rtaAuditLogResponse.setAuditDate((Date) row[3]);
                rtaAuditLogResponse.setLoggedUser((String) row[4]);

                rtaAuditLogResponses.add(rtaAuditLogResponse);
            }
            return rtaAuditLogResponses;
        } else {
            return null;
        }
    }

    @Override
    public TblOlclaim updateOlRequest(TblOlclaim tblOlclaim) {
        return tblOlclaimRepo.saveAndFlush(tblOlclaim);
    }

    @Override
    public List<OlCaseList> getAuthOlCasesUserAndCategoryWise(String usercode, String categorycode) {
        List<OlCaseList> OlCaseLists = new ArrayList<>();
        List<ViewOlclaim> OlCasesListForIntroducers = viewOlclaimRepo.getAuthOlCasesUserAndCategoryWise(Long.valueOf(usercode), Long.valueOf(categorycode));
        OlCaseList OlCaseList;
        if (OlCasesListForIntroducers != null && OlCasesListForIntroducers.size() > 0) {
            for (ViewOlclaim viewOlclaim : OlCasesListForIntroducers) {
                OlCaseList = new OlCaseList();


                OlCaseList.setOlClaimCode(viewOlclaim.getOlclaimcode());
                OlCaseList.setCreated(viewOlclaim.getCreatedate());
                OlCaseList.setCode(viewOlclaim.getOlcode());
                OlCaseList.setClient(viewOlclaim.getFirstname() + (viewOlclaim.getMiddlename() != null ? viewOlclaim.getMiddlename() : "") + viewOlclaim.getLastname());
                OlCaseList.setTaskName(viewOlclaim.getOltaskname());
                OlCaseList.setStatus(viewOlclaim.getStatus());
//                OlCaseList.setEmail(viewOlclaim.get);
                OlCaseList.setAddress(viewOlclaim.getPostalcode() + " " + viewOlclaim.getAddress1() + " " + viewOlclaim.getAddress2() + " " + viewOlclaim.getAddress3());
                OlCaseList.setContactNo(viewOlclaim.getMobile());
                OlCaseList.setLastUpdated(viewOlclaim.getUpdatedate());
                OlCaseList.setIntroducer(viewOlclaim.getIntroducerUser());
//                OlCaseList.setLastNote((Date) row[12]);
                OlCaseLists.add(OlCaseList);
            }
        }
        if (OlCaseLists != null && OlCaseLists.size() > 0) {
            return OlCaseLists;
        } else {
            return null;
        }
    }
}
