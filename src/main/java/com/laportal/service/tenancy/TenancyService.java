package com.laportal.service.tenancy;

import com.laportal.dto.AssignTenancyCasetoSolicitor;
import com.laportal.dto.TenancyCaseList;
import com.laportal.dto.TenancyStatusCountList;
import com.laportal.model.*;

import java.util.List;

public interface TenancyService {

    List<TenancyCaseList> getTenancyCases(TblUser tblUser);

    TblTenancyclaim findTenancyCaseById(long tenancyCaseId, TblUser tblUser);

    TblCompanyprofile getCompanyProfile(String companycode);

    TblTask findTaskById(Long taskCode);

    List<TenancyStatusCountList> getAllTenancyStatusCounts();

    List<TenancyCaseList> getTenancyCasesByStatus(long statusId);

    List<TblTenancylog> getTenancyCaseLogs(String tenancycode);

    List<TblTenancynote> getTenancyCaseNotes(String tenancycode, TblUser tblUser);

    List<TblTenancymessage> getTenancyCaseMessages(String tenancycode);

    boolean isHdrCaseAllowed(String companycode, String s);

    TblTenancyclaim saveTenancyRequest(TblTenancyclaim tblTenancyclaim);

    TblTenancynote addTblTenancyNote(TblTenancynote tblTenancynote);

    TblTenancymessage getTblTenancyMessageById(String tenancymessagecode);

    TblEmail saveTblEmail(TblEmail tblEmail);

    TblTenancyclaim assignCaseToSolicitor(AssignTenancyCasetoSolicitor assignTenancyCasetoSolicitor, TblUser tblUser);


    TblTenancyclaim performRejectCancelActionOnTenancy(String tenancyClaimCode, String toStatus, String reason, TblUser tblUser);

    TblTenancyclaim performActionOnTenancy(String tenancyClaimCode, String toStatus, TblUser tblUser);

    TblTenancyclaim findTenancyCaseByIdWithoutUser(long rtaCode);

    TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryTypee);

    TblCompanyprofile getCompanyProfileAgainstTenancyCode(long tenancyclaimcode);

    String gettenancyDbColumnValue(String key, long tenancyclaimcode);

    TblTenancydocument saveTblTenancyDocument(TblTenancydocument tblTenancydocument);

    TblTenancytask getTenancyTaskByTenancyCodeAndTaskCode(long tenancyclaimcode, long taskcode);

    TblTenancytask updateTblTenancyTask(TblTenancytask tblTenancytask);

    TblTask getTaskAgainstCode(long taskCode);

    List<TblTenancydocument> saveTblTenancyDocuments(List<TblTenancydocument> tblTenancydocuments);

    List<TblTenancytask> findTblTenancyTaskByTenancyClaimCode(long tenancyclaimcode);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    List<TenancyCaseList> getAuthTenancyCasesIntroducers(String usercode);

    List<TenancyCaseList> getAuthTenancyCasesSolicitors(String usercode);

    List<TenancyCaseList> getAuthTenancyCasesLegalAssist();

    List<TenancyStatusCountList> getAllTenancyStatusCountsForIntroducers();

    List<TenancyStatusCountList> getAllTenancyStatusCountsForSolicitor();
}
