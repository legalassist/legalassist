package com.laportal.service.tenancy;

import com.laportal.Repo.*;
import com.laportal.dto.AssignTenancyCasetoSolicitor;
import com.laportal.dto.HdrActionButton;
import com.laportal.dto.TenancyCaseList;
import com.laportal.dto.TenancyStatusCountList;
import com.laportal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class TenancyServiceImpl implements TenancyService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;

    @Autowired
    private TblTenancyclaimRepo tblTenancyclaimRepo;

    @Autowired
    private TblTenancylogRepo tblTenancylogRepo;

    @Autowired
    private TblTenancymessageRepo tblTenancymessageRepo;

    @Autowired
    private TblTenancynoteRepo tblTenancynoteRepo;

    @Autowired
    private TblTenancysolicitorRepo tblTenancysolicitorRepo;

    @Autowired
    private TblTaskRepo tblTaskRepo;

    @Autowired
    private TblUsersRepo tblUsersRepo;

    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;

    @Autowired
    private TblEmailRepo tblEmailRepo;

    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;

    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;

    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;

    @Autowired
    private TblTenancydocumentRepo tblTenancydocumentRepo;

    @Autowired
    private TblTenancytaskRepo tblTenancytaskRepo;

    @Override
    public List<TenancyCaseList> getTenancyCases(TblUser tblUser) {
        List<TenancyCaseList> tenancyCaseLists = new ArrayList<>();
        List<Object> tenancyCasesListObject = tblTenancyclaimRepo.getHdrCasesListUserWise();
        TenancyCaseList tenancyCaseList;
        if (tenancyCasesListObject != null && tenancyCasesListObject.size() > 0) {
            for (Object record : tenancyCasesListObject) {
                tenancyCaseList = new TenancyCaseList();
                Object[] row = (Object[]) record;

                tenancyCaseList.setTenancyClaimCode((BigDecimal) row[0]);
                tenancyCaseList.setCreated((String) row[1]);
                tenancyCaseList.setCode((String) row[2]);
                tenancyCaseList.setClient((String) row[3]);
                tenancyCaseList.setTaskDue((String) row[4]);
                tenancyCaseList.setTaskName((String) row[5]);
                tenancyCaseList.setStatus((String) row[6]);
                tenancyCaseList.setEmail((String) row[7]);
                tenancyCaseList.setAddress((String) row[8]);
                tenancyCaseList.setContactNo((String) row[9]);
                tenancyCaseList.setLastUpdated((Date) row[10]);
                tenancyCaseList.setIntroducer((String) row[11]);
                tenancyCaseList.setLastNote((Date) row[12]);

                tenancyCaseLists.add(tenancyCaseList);
            }
        }
        if (tenancyCaseLists != null && tenancyCaseLists.size() > 0) {
            return tenancyCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public TblTenancyclaim findTenancyCaseById(long tenancyCaseId, TblUser tblUser) {
        TblTenancyclaim tblTenancyclaim = tblTenancyclaimRepo.findById(tenancyCaseId).orElse(null);
        if (tblTenancyclaim != null) {
            TblRtastatus tblRtastatus = tblRtastatusRepo.findById(Long.valueOf(tblTenancyclaim.getStatus())).orElse(null);
            tblTenancyclaim.setStatusDescr(tblRtastatus.getDescr());
            List<HdrActionButton> rtaActionButtons = new ArrayList<>();
            List<HdrActionButton> rtaActionButtonsForLA = new ArrayList<>();
            HdrActionButton hdrActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    Long.valueOf(tblTenancyclaim.getStatus()), "3", tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetailForLA = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    Long.valueOf(tblTenancyclaim.getStatus()), "3", tblCompanyprofile.getTblUsercategory().getCategorycode());

            TblTenancysolicitor tblTenancysolicitor = tblTenancysolicitorRepo.findByTblTenancyclaimTenancyclaimcodeAndStatus(tenancyCaseId, "Y");
            if (tblTenancysolicitor != null) {
                List<TblTenancysolicitor> tblTenancysolicitors = new ArrayList<>();
                tblTenancysolicitors.add(tblTenancysolicitor);
                tblTenancyclaim.setTblTenancysolicitors(tblTenancysolicitors);
            } else {
                tblTenancyclaim.setTblTenancysolicitors(null);
            }
            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    hdrActionButton = new HdrActionButton();
                    hdrActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hdrActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hdrActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hdrActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    hdrActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());
                    hdrActionButton.setAcceptDialog(tblRtaflowdetail.getCaseacceptdialog());

                    rtaActionButtons.add(hdrActionButton);
                }
                tblTenancyclaim.setHdrActionButton(rtaActionButtons);
            }
            if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                    hdrActionButton = new HdrActionButton();
                    hdrActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hdrActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hdrActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hdrActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    hdrActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());
                    hdrActionButton.setAcceptDialog(tblRtaflowdetail.getCaseacceptdialog());

                    rtaActionButtonsForLA.add(hdrActionButton);
                }
                tblTenancyclaim.setHdrActionButtonForLA(rtaActionButtonsForLA);
            }
            return tblTenancyclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public TblTask findTaskById(Long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public List<TenancyStatusCountList> getAllTenancyStatusCounts() {
        List<TenancyStatusCountList> tenancyStatusCountLists = new ArrayList<>();
        List<Object> tenancyStatusCountListObject = tblTenancyclaimRepo.getAllTenancyStatusCounts();
        TenancyStatusCountList tenancyStatusCountList;
        if (tenancyStatusCountListObject != null && tenancyStatusCountListObject.size() > 0) {
            for (Object record : tenancyStatusCountListObject) {
                tenancyStatusCountList = new TenancyStatusCountList();
                Object[] row = (Object[]) record;

                tenancyStatusCountList.setStatusCount((BigDecimal) row[0]);
                tenancyStatusCountList.setStatusName((String) row[1]);
                tenancyStatusCountList.setStatusCode((BigDecimal) row[2]);

                tenancyStatusCountLists.add(tenancyStatusCountList);
            }
        }
        if (tenancyStatusCountLists != null && tenancyStatusCountLists.size() > 0) {
            return tenancyStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TenancyCaseList> getTenancyCasesByStatus(long statusId) {
        List<TenancyCaseList> tenancyCaseLists = new ArrayList<>();
        List<Object> tenancyCasesListObject = tblTenancyclaimRepo.getTenancyCasesListStatusWise(statusId);
        TenancyCaseList tenancyCaseList;
        if (tenancyCasesListObject != null && tenancyCasesListObject.size() > 0) {
            for (Object record : tenancyCasesListObject) {
                tenancyCaseList = new TenancyCaseList();
                Object[] row = (Object[]) record;

                tenancyCaseList.setTenancyClaimCode((BigDecimal) row[0]);
                tenancyCaseList.setCreated((String) row[1]);
                tenancyCaseList.setCode((String) row[2]);
                tenancyCaseList.setClient((String) row[3]);
//                tenancyCaseList.setTaskDue((String) row[4]);
//                tenancyCaseList.setTaskName((String) row[5]);
                tenancyCaseList.setStatus((String) row[4]);

                tenancyCaseLists.add(tenancyCaseList);
            }
        }
        if (tenancyCaseLists != null && tenancyCaseLists.size() > 0) {
            return tenancyCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblTenancylog> getTenancyCaseLogs(String tenancycode) {
        List<TblTenancylog> tblTenancylogs = tblTenancylogRepo.findByTblTenancyclaimTenancyclaimcode(Long.valueOf(tenancycode));
        if (tblTenancylogs != null && tblTenancylogs.size() > 0) {
            for (TblTenancylog tblTenancylog : tblTenancylogs) {
                TblUser tblUser = tblUsersRepo.findById(tblTenancylog.getUsercode()).orElse(null);
                tblTenancylog.setUserName(tblUser.getUsername());
            }
            return tblTenancylogs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblTenancynote> getTenancyCaseNotes(String tenancycode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblTenancynote> tblTenancynotes = tblTenancynoteRepo.findNotesOnTenancybyUserAndCategory(
                Long.valueOf(tenancycode), tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (tblTenancynotes != null && tblTenancynotes.size() > 0) {
            for (TblTenancynote tblTenancynote : tblTenancynotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblTenancynote.getUsercode()).orElse(null);
                tblTenancynote.setUserName(tblUser1.getUsername());
                tblTenancynote.setSelf(tblTenancynote.getUsercode() == tblUser.getUsercode() ? true : false);
            }
            return tblTenancynotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblTenancymessage> getTenancyCaseMessages(String tenancycode) {
        List<TblTenancymessage> tblTenancymessages = tblTenancymessageRepo.findByTblTenancyclaimTenancyclaimcode(Long.valueOf(tenancycode));
        if (tblTenancymessages != null && tblTenancymessages.size() > 0) {
            for (TblTenancymessage tblTenancymessage : tblTenancymessages) {
                TblUser tblUser = tblUsersRepo.findById(tblTenancymessage.getUsercode()).orElse(null);
                tblTenancymessage.setUserName(tblUser.getUsername());
            }
            return tblTenancymessages;
        } else {
            return null;
        }
    }

    @Override
    public boolean isHdrCaseAllowed(String companycode, String s) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companycode,
                s);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TblTenancyclaim saveTenancyRequest(TblTenancyclaim tblTenancyclaim) {
        return tblTenancyclaimRepo.save(tblTenancyclaim);
    }

    @Override
    public TblTenancynote addTblTenancyNote(TblTenancynote tblTenancynote) {
        return tblTenancynoteRepo.save(tblTenancynote);
    }

    @Override
    public TblTenancymessage getTblTenancyMessageById(String tenancymessagecode) {
        return tblTenancymessageRepo.findById(Long.valueOf(tenancymessagecode)).orElse(null);
    }

    @Override
    public TblEmail saveTblEmail(TblEmail tblEmail) {
        return tblEmailRepo.save(tblEmail);
    }

    @Override
    public TblTenancyclaim assignCaseToSolicitor(AssignTenancyCasetoSolicitor assignTenancyCasetoSolicitor, TblUser tblUser) {
        int update = tblTenancyclaimRepo.performAction(Long.valueOf(41), Long.valueOf(assignTenancyCasetoSolicitor.getTenancyClaimCode()));
        if (update > 0) {

            TblTenancyclaim tblTenancyclaim1 = new TblTenancyclaim();
            TblTenancylog tblTenancylog = new TblTenancylog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(41))
                    .orElse(null);
            tblTenancyclaim1.setTenancyclaimcode(Long.valueOf(assignTenancyCasetoSolicitor.getTenancyClaimCode()));

            tblTenancylog.setCreatedon(new java.util.Date());
            tblTenancylog.setDescr(tblStatus.getDescr());
            tblTenancylog.setUsercode(tblUser.getUsercode());
            tblTenancylog.setTblTenancyclaim(tblTenancyclaim1);

            int updateSolicitor = tblTenancysolicitorRepo.updateSolicitor("N", Long.valueOf(assignTenancyCasetoSolicitor.getTenancyClaimCode()));

            TblTenancysolicitor tblTenancysolicitor = new TblTenancysolicitor();
            tblTenancysolicitor.setCompanycode(assignTenancyCasetoSolicitor.getSolicitorUserCode());
            tblTenancysolicitor.setCreatedon(new java.util.Date());
            tblTenancysolicitor.setTblTenancyclaim(tblTenancyclaim1);
            tblTenancysolicitor.setStatus("Y");
            tblTenancysolicitor.setUsercode(assignTenancyCasetoSolicitor.getSolicitorCode());

            tblTenancysolicitor = tblTenancysolicitorRepo.save(tblTenancysolicitor);
            tblTenancylog = tblTenancylogRepo.save(tblTenancylog);

            TblTenancyclaim tblTenancyclaim = tblTenancyclaimRepo.findById(tblTenancyclaim1.getTenancyclaimcode()).orElse(null);

            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + tblTenancyclaim.getTenancyCode());
            tblEmail.setEmailsubject("HDR CASE" + tblTenancyclaim.getTenancyCode());
            tblEmail.setCreatedon(new java.util.Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            TblTenancymessage tblTenancymessage = new TblTenancymessage();

            tblTenancymessage.setTblTenancyclaim(tblTenancyclaim1);
            tblTenancymessage.setUserName(tblUser.getUsername());
            tblTenancymessage.setCreatedon(new Date());
            tblTenancymessage.setMessage(tblEmail.getEmailbody());
            tblTenancymessage.setSentto(tblEmail.getEmailaddress());
            tblTenancymessage.setUsercode(tblUser.getUsercode());

            tblTenancymessage = tblTenancymessageRepo.save(tblTenancymessage);
            return tblTenancyclaimRepo.findById(Long.valueOf(assignTenancyCasetoSolicitor.getTenancyClaimCode())).orElse(null);

        } else {
            return null;
        }
    }

    @Override
    public TblTenancyclaim performRejectCancelActionOnTenancy(String tenancyClaimCode, String toStatus, String reason, TblUser tblUser) {
        TblTenancysolicitor tblHdrsolicitor = tblTenancysolicitorRepo.findByTblTenancyclaimTenancyclaimcodeAndStatus(Long.valueOf(tenancyClaimCode), "Y");

        if (tblHdrsolicitor != null) {
            int update = tblTenancysolicitorRepo.updateRemarksReason(reason, "N", Long.valueOf(tenancyClaimCode));

            TblTenancyclaim tblTenancyclaim = tblTenancyclaimRepo.findById(Long.valueOf(tenancyClaimCode)).orElse(null);
            TblTenancylog tblTenancylog = new TblTenancylog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblTenancylog.setCreatedon(new Date());
            tblTenancylog.setDescr(tblStatus.getDescr());
            tblTenancylog.setUsercode(tblUser.getUsercode());
            tblTenancylog.setTblTenancyclaim(tblTenancyclaim);

            tblTenancylog = tblTenancylogRepo.save(tblTenancylog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblTenancyclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("you have been hotkey the case number is " + tblTenancyclaim.getTenancyCode());
            tblEmailIntroducer.setEmailsubject("HDR CASE" + tblTenancyclaim.getTenancyCode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Solicitor

            if (tblHdrsolicitor != null) {
                TblUser solicitorUser = tblUsersRepo.findById(tblHdrsolicitor.getUsercode()).orElse(null);
                TblEmail tblEmailSolicitor = new TblEmail();
                tblEmailSolicitor.setSenflag(new BigDecimal(0));
                tblEmailSolicitor.setEmailaddress(solicitorUser.getUsername());
                tblEmailSolicitor.setEmailbody("you have been hotkey the case number is " + tblTenancyclaim.getTenancyCode());
                tblEmailSolicitor.setEmailsubject("HDR CASE" + tblTenancyclaim.getTenancyCode());
                tblEmailSolicitor.setCreatedon(new Date());

                tblEmailSolicitor = tblEmailRepo.save(tblEmailSolicitor);
            }

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + tblTenancyclaim.getTenancyCode());
            tblEmail.setEmailsubject("HDR CASE" + tblTenancyclaim.getTenancyCode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return tblTenancyclaim;

        } else {
            int update = tblTenancyclaimRepo.performRejectCancelAction(reason, toStatus, Long.valueOf(tenancyClaimCode));

            TblTenancyclaim tblTenancyclaim = tblTenancyclaimRepo.findById(Long.valueOf(tenancyClaimCode)).orElse(null);
            TblTenancylog tblTenancylog = new TblTenancylog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblTenancylog.setCreatedon(new Date());
            tblTenancylog.setDescr(tblStatus.getDescr());
            tblTenancylog.setUsercode(tblUser.getUsercode());
            tblTenancylog.setTblTenancyclaim(tblTenancyclaim);

            tblTenancylog = tblTenancylogRepo.save(tblTenancylog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblTenancyclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("you have been hotkey the case number is " + tblTenancyclaim.getTenancyCode());
            tblEmailIntroducer.setEmailsubject("RTA CASE" + tblTenancyclaim.getTenancyCode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + tblTenancyclaim.getTenancyCode());
            tblEmail.setEmailsubject("RTA CASE" + tblTenancyclaim.getTenancyCode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return tblTenancyclaim;

        }
    }

    @Override
    public TblTenancyclaim performActionOnTenancy(String tenancyClaimCode, String toStatus, TblUser tblUser) {
        int update = tblTenancyclaimRepo.performAction(Long.valueOf(toStatus), Long.valueOf(tenancyClaimCode));
        if (update > 0) {

            TblTenancyclaim tblTenancyclaim = tblTenancyclaimRepo.findById(Long.valueOf(tenancyClaimCode)).orElse(null);
            TblTenancyclaim tblTenancyclaim1 = new TblTenancyclaim();
            TblTenancylog tblTenancylog = new TblTenancylog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);
            tblTenancyclaim1.setTenancyclaimcode(Long.valueOf(tenancyClaimCode));

            tblTenancylog.setCreatedon(new Date());
            tblTenancylog.setDescr(tblStatus.getDescr());
            tblTenancylog.setUsercode(tblUser.getUsercode());
            tblTenancylog.setTblTenancyclaim(tblTenancyclaim1);

            tblTenancylog = tblTenancylogRepo.save(tblTenancylog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblTenancyclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("Action taken on  case number :: " + tblTenancyclaim.getTenancyCode());
            tblEmailIntroducer.setEmailsubject("HDR CASE" + tblTenancyclaim.getTenancyCode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Solicitor
            TblTenancysolicitor tblHdrsolicitor = tblTenancysolicitorRepo.findByTblTenancyclaimTenancyclaimcodeAndStatus(tblTenancyclaim.getTenancyclaimcode(), "Y");
            if (tblHdrsolicitor != null) {
                TblUser solicitorUser = tblUsersRepo.findById(tblHdrsolicitor.getUsercode()).orElse(null);
                TblEmail tblEmailSolicitor = new TblEmail();
                tblEmailSolicitor.setSenflag(new BigDecimal(0));
                tblEmailSolicitor.setEmailaddress(solicitorUser.getUsername());
                tblEmailSolicitor.setEmailbody("Action taken on  case number :: " + tblTenancyclaim.getTenancyCode());
                tblEmailSolicitor.setEmailsubject("HDR CASE" + tblTenancyclaim.getTenancyCode());
                tblEmailSolicitor.setCreatedon(new Date());

                tblEmailSolicitor = tblEmailRepo.save(tblEmailSolicitor);
            }

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("Action taken on  case number :: " + tblTenancyclaim.getTenancyCode());
            tblEmail.setEmailsubject("HDR CASE" + tblTenancyclaim.getTenancyCode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return tblTenancyclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblTenancyclaim findTenancyCaseByIdWithoutUser(long rtaCode) {
        return tblTenancyclaimRepo.findById(rtaCode).orElse(null);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndAgenatureAndCountrytypeAndTblCompaignCompaigncode(companycode, ageNature, countryType, null);
    }

    @Override
    public TblCompanyprofile getCompanyProfileAgainstTenancyCode(long tenancyclaimcode) {
        return tblCompanyprofileRepo.getCompanyProfileAgainstTenancyCode(tenancyclaimcode);
    }

    @Override
    public String gettenancyDbColumnValue(String key, long tenancyclaimcode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + tenancyclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT FIRSTNAME || ' ' || NVL(MIDDLENAME,'') || ' ' || NVL(LASTNAME, '') from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + tenancyclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT POSTALCODE || ' ' || ADDRESS1 || ' ' || ADDRESS2  || ' ' || ADDRESS3 || ' ' || CITY || ' ' || REGION from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + tenancyclaimcode;
            query = em.createNativeQuery(sql);
        } else {
            String sql = "SELECT " + key + " from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + tenancyclaimcode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")
        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    @Override
    public TblTenancydocument saveTblTenancyDocument(TblTenancydocument tblTenancydocument) {
        return tblTenancydocumentRepo.save(tblTenancydocument);
    }

    @Override
    public TblTenancytask getTenancyTaskByTenancyCodeAndTaskCode(long tenancyclaimcode, long taskcode) {
        return tblTenancytaskRepo.findByTblTenancyclaimTenancyclaimcodeAndTaskcode(tenancyclaimcode, new BigDecimal(taskcode));
    }

    @Override
    public TblTenancytask updateTblTenancyTask(TblTenancytask tblTenancytask) {
        return tblTenancytaskRepo.saveAndFlush(tblTenancytask);
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public List<TblTenancydocument> saveTblTenancyDocuments(List<TblTenancydocument> tblTenancydocuments) {
        return tblTenancydocumentRepo.saveAll(tblTenancydocuments);
    }

    @Override
    public List<TblTenancytask> findTblTenancyTaskByTenancyClaimCode(long tenancyclaimcode) {
        return tblTenancytaskRepo.findByTblTenancyclaimTenancyclaimcode(tenancyclaimcode);
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public List<TenancyCaseList> getAuthTenancyCasesIntroducers(String usercode) {
        List<TenancyCaseList> tenancyCaseLists = new ArrayList<>();
        List<Object> tenancyCasesListForIntroducers = tblTenancyclaimRepo.getTenancyCasesListForIntroducers(usercode);
        TenancyCaseList tenancyCaseList;
        if (tenancyCasesListForIntroducers != null && tenancyCasesListForIntroducers.size() > 0) {
            for (Object record : tenancyCasesListForIntroducers) {
                tenancyCaseList = new TenancyCaseList();
                Object[] row = (Object[]) record;

                tenancyCaseList.setTenancyClaimCode((BigDecimal) row[0]);
                tenancyCaseList.setCreated((String) row[1]);
                tenancyCaseList.setCode((String) row[2]);
                tenancyCaseList.setClient((String) row[3]);
                tenancyCaseList.setTaskDue((String) row[4]);
                tenancyCaseList.setTaskName((String) row[5]);
                tenancyCaseList.setStatus((String) row[6]);
                tenancyCaseList.setEmail((String) row[7]);
                tenancyCaseList.setAddress((String) row[8]);
                tenancyCaseList.setContactNo((String) row[9]);
                tenancyCaseList.setLastUpdated((Date) row[10]);
                tenancyCaseList.setIntroducer((String) row[11]);
                tenancyCaseList.setLastNote((Date) row[12]);
                tenancyCaseLists.add(tenancyCaseList);
            }
        }
        if (tenancyCaseLists != null && tenancyCaseLists.size() > 0) {
            return tenancyCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<TenancyCaseList> getAuthTenancyCasesSolicitors(String usercode) {
        List<TenancyCaseList> tenancyCaseLists = new ArrayList<>();
        List<Object> tenancyCasesListForSolicitor = tblTenancyclaimRepo.getTenancyCasesListForSolicitor(usercode);
        TenancyCaseList tenancyCaseList;
        if (tenancyCasesListForSolicitor != null && tenancyCasesListForSolicitor.size() > 0) {
            for (Object record : tenancyCasesListForSolicitor) {
                tenancyCaseList = new TenancyCaseList();
                Object[] row = (Object[]) record;

                tenancyCaseList.setTenancyClaimCode((BigDecimal) row[0]);
                tenancyCaseList.setCreated((String) row[1]);
                tenancyCaseList.setCode((String) row[2]);
                tenancyCaseList.setClient((String) row[3]);
                tenancyCaseList.setTaskDue((String) row[4]);
                tenancyCaseList.setTaskName((String) row[5]);
                tenancyCaseList.setStatus((String) row[6]);
                tenancyCaseList.setEmail((String) row[7]);
                tenancyCaseList.setAddress((String) row[8]);
                tenancyCaseList.setContactNo((String) row[9]);
                tenancyCaseList.setLastUpdated((Date) row[10]);
                tenancyCaseList.setIntroducer((String) row[11]);
                tenancyCaseList.setLastNote((Date) row[12]);
                tenancyCaseLists.add(tenancyCaseList);
            }
        }
        if (tenancyCaseLists != null && tenancyCaseLists.size() > 0) {
            return tenancyCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<TenancyCaseList> getAuthTenancyCasesLegalAssist() {
        List<TenancyCaseList> tenancyCaseLists = new ArrayList<>();
        List<Object> tenancyCasesList = tblTenancyclaimRepo.getTenancyCasesList();
        TenancyCaseList tenancyCaseList;
        if (tenancyCasesList != null && tenancyCasesList.size() > 0) {
            for (Object record : tenancyCasesList) {
                tenancyCaseList = new TenancyCaseList();
                Object[] row = (Object[]) record;

                tenancyCaseList.setTenancyClaimCode((BigDecimal) row[0]);
                tenancyCaseList.setCreated((String) row[1]);
                tenancyCaseList.setCode((String) row[2]);
                tenancyCaseList.setClient((String) row[3]);
                tenancyCaseList.setTaskDue((String) row[4]);
                tenancyCaseList.setTaskName((String) row[5]);
                tenancyCaseList.setStatus((String) row[6]);
                tenancyCaseList.setEmail((String) row[7]);
                tenancyCaseList.setAddress((String) row[8]);
                tenancyCaseList.setContactNo((String) row[9]);
                tenancyCaseList.setLastUpdated((Date) row[10]);
                tenancyCaseList.setIntroducer((String) row[11]);
                tenancyCaseList.setLastNote((Date) row[12]);
                tenancyCaseLists.add(tenancyCaseList);
            }
        }
        if (tenancyCaseLists != null && tenancyCaseLists.size() > 0) {
            return tenancyCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TenancyStatusCountList> getAllTenancyStatusCountsForIntroducers() {
        List<TenancyStatusCountList> tenancyStatusCountLists = new ArrayList<>();
        List<Object> tenancyStatusCountListObject = tblTenancyclaimRepo.getAllTenancyStatusCountsForIntroducers();
        TenancyStatusCountList tenancyStatusCountList;
        if (tenancyStatusCountListObject != null && tenancyStatusCountListObject.size() > 0) {
            for (Object record : tenancyStatusCountListObject) {
                tenancyStatusCountList = new TenancyStatusCountList();
                Object[] row = (Object[]) record;

                tenancyStatusCountList.setStatusCount((BigDecimal) row[0]);
                tenancyStatusCountList.setStatusName((String) row[1]);
                tenancyStatusCountList.setStatusCode((BigDecimal) row[2]);

                tenancyStatusCountLists.add(tenancyStatusCountList);
            }
        }
        if (tenancyStatusCountLists != null && tenancyStatusCountLists.size() > 0) {
            return tenancyStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TenancyStatusCountList> getAllTenancyStatusCountsForSolicitor() {
        List<TenancyStatusCountList> tenancyStatusCountLists = new ArrayList<>();
        List<Object> tenancyStatusCountListObject = tblTenancyclaimRepo.getAllTenancyStatusCountsForSolicitors();
        TenancyStatusCountList tenancyStatusCountList;
        if (tenancyStatusCountListObject != null && tenancyStatusCountListObject.size() > 0) {
            for (Object record : tenancyStatusCountListObject) {
                tenancyStatusCountList = new TenancyStatusCountList();
                Object[] row = (Object[]) record;

                tenancyStatusCountList.setStatusCount((BigDecimal) row[0]);
                tenancyStatusCountList.setStatusName((String) row[1]);
                tenancyStatusCountList.setStatusCode((BigDecimal) row[2]);

                tenancyStatusCountLists.add(tenancyStatusCountList);
            }
        }
        if (tenancyStatusCountLists != null && tenancyStatusCountLists.size() > 0) {
            return tenancyStatusCountLists;
        } else {
            return null;
        }
    }
}
