package com.laportal.service.immigration;

import com.laportal.dto.AssignImmigrationCasetoSolicitor;
import com.laportal.dto.ImigrationStatusCountList;
import com.laportal.dto.ImmigrationCaseList;
import com.laportal.model.*;

import java.util.List;

public interface ImmigrationService {

    TblImmigrationclaim findImmigrationCaseById(long DbCaseId, TblUser tblUser);

    TblCompanyprofile getCompanyProfile(String companycode);

    List<ImigrationStatusCountList> getAllDbStatusCounts();

    List<ImmigrationCaseList> getDbCasesByStatus(long statusId);

    List<TblImmigrationlog> getDbCaseLogs(String Dbcode);

    List<TblImmigrationnote> getDbCaseNotes(String Dbcode, TblUser tblUser);

    List<TblImmigrationmessage> getDbCaseMessages(String Dbcode);

    boolean isHdrCaseAllowed(String companycode, String s);

    TblImmigrationclaim saveDbRequest(TblImmigrationclaim tblDbclaim);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    TblImmigrationnote addTblDbNote(TblImmigrationnote tblDbnote);

    TblImmigrationmessage getTblDbMessageById(String Dbmessagecode);

    TblEmail saveTblEmail(TblEmail tblEmail);

    TblImmigrationclaim assignCaseToSolicitor(AssignImmigrationCasetoSolicitor assignDbCasetoSolicitor, TblUser tblUser);

    TblImmigrationclaim performRejectCancelActionOnDb(String DbClaimCode, String toStatus, String reason, TblUser tblUser);

    TblImmigrationclaim performActionOnDb(String DbClaimCode, String toStatus, TblUser tblUser);

    TblImmigrationclaim findDbCaseByIdWithoutUser(long rtaCode);

    TblCompanyprofile getCompanyProfileAgainstDbCode(long Dbclaimcode);

    TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType);

    String getDbDbColumnValue(String key, long Dbclaimcode);

    TblImmigrationdocument saveTblDbDocument(TblImmigrationdocument tblDbdocument);

    TblImmigrationtask getDbTaskByDbCodeAndTaskCode(long Dbclaimcode, long taskCode);

    TblImmigrationtask updateTblDbTask(TblImmigrationtask tblDbtask);

    TblTask getTaskAgainstCode(long taskCode);

    List<TblImmigrationdocument> saveTblDbDocuments(List<TblImmigrationdocument> tblDbdocuments);

    List<TblImmigrationtask> findTblDbTaskByDbClaimCode(long Dbclaimcode);

    List<ImmigrationCaseList> getAuthDbCasesIntroducers(String usercode);

    List<ImmigrationCaseList> getAuthDbCasesSolicitors(String usercode);

    List<ImmigrationCaseList> getAuthDbCasesLegalAssist();

    List<ImigrationStatusCountList> getAllDbStatusCountsForIntroducers();

    List<ImigrationStatusCountList> getAllDbStatusCountsForSolicitor();
}
