package com.laportal.service.immigration;

import com.laportal.Repo.*;
import com.laportal.dto.AssignImmigrationCasetoSolicitor;
import com.laportal.dto.HdrActionButton;
import com.laportal.dto.ImigrationStatusCountList;
import com.laportal.dto.ImmigrationCaseList;
import com.laportal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class ImmigrationServiceImpl implements ImmigrationService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;

    @Autowired
    private TblImmigrationclaimRepo tblImmigrationclaimRepo;

    @Autowired
    private TblImmigrationlogRepo tblImmigrationlogRepo;

    @Autowired
    private TblImmigrationmessageRepo tblImmigrationmessageRepo;

    @Autowired
    private TblImmigrationnoteRepo tblImmigrationnoteRepo;

    @Autowired
    private TblImmigrationsolicitorRepo tblImmigrationsolicitorRepo;

    @Autowired
    private TblTaskRepo tblTaskRepo;

    @Autowired
    private TblUsersRepo tblUsersRepo;

    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;

    @Autowired
    private TblEmailRepo tblEmailRepo;

    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;

    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;

    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;

    @Autowired
    private TblImmigrationdocumentRepo tblImmigrationdocumentRepo;

    @Autowired
    private TblImmigrationtaskRepo tblImmigrationtaskRepo;

    @Override
    public TblImmigrationclaim findImmigrationCaseById(long DbCaseId, TblUser tblUser) {
        TblImmigrationclaim tblImmigrationclaim = tblImmigrationclaimRepo.findById(DbCaseId).orElse(null);
        if (tblImmigrationclaim != null) {
            TblRtastatus tblRtastatus = tblRtastatusRepo.findById(Long.valueOf(tblImmigrationclaim.getStatus().toString())).orElse(null);
            tblImmigrationclaim.setStatusDescr(tblRtastatus.getDescr());
            List<HdrActionButton> rtaActionButtons = new ArrayList<>();
            List<HdrActionButton> rtaActionButtonsForLA = new ArrayList<>();
            HdrActionButton hdrActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);

            List<TblRtaflowdetail> tblRtaflowdetailForLA = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    Long.valueOf(tblImmigrationclaim.getStatus().toString()), "3", tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    Long.valueOf(tblImmigrationclaim.getStatus().toString()), "3", tblCompanyprofile.getTblUsercategory().getCategorycode());
            TblImmigrationsolicitor tblTenancysolicitor = tblImmigrationsolicitorRepo.findByTblImmigrationclaimImmigrationclaimcodeAndStatus(DbCaseId, "Y");
            if (tblTenancysolicitor != null) {
                List<TblImmigrationsolicitor> tblTenancysolicitors = new ArrayList<>();
                tblTenancysolicitors.add(tblTenancysolicitor);
                tblImmigrationclaim.setTblImmigrationsolicitors(tblTenancysolicitors);
            } else {
                tblImmigrationclaim.setTblImmigrationsolicitors(null);
            }
            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    hdrActionButton = new HdrActionButton();
                    hdrActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hdrActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hdrActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hdrActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    hdrActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());
                    hdrActionButton.setAcceptDialog(tblRtaflowdetail.getCaseacceptdialog());

                    rtaActionButtons.add(hdrActionButton);
                }
                tblImmigrationclaim.setHdrActionButton(rtaActionButtons);
            }
            if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                    hdrActionButton = new HdrActionButton();
                    hdrActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hdrActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hdrActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hdrActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    hdrActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());
                    hdrActionButton.setAcceptDialog(tblRtaflowdetail.getCaseacceptdialog());

                    rtaActionButtonsForLA.add(hdrActionButton);
                }
                tblImmigrationclaim.setHdrActionButtonForLA(rtaActionButtonsForLA);
            }
            return tblImmigrationclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public List<ImigrationStatusCountList> getAllDbStatusCounts() {
        List<ImigrationStatusCountList> DbStatusCountLists = new ArrayList<>();
        List<Object> DbStatusCountListObject = tblImmigrationclaimRepo.getAllDbStatusCounts();
        ImigrationStatusCountList DbStatusCountList;
        if (DbStatusCountListObject != null && DbStatusCountListObject.size() > 0) {
            for (Object record : DbStatusCountListObject) {
                DbStatusCountList = new ImigrationStatusCountList();
                Object[] row = (Object[]) record;

                DbStatusCountList.setStatusCount((BigDecimal) row[0]);
                DbStatusCountList.setStatusName((String) row[1]);
                DbStatusCountList.setStatusCode((BigDecimal) row[2]);

                DbStatusCountLists.add(DbStatusCountList);
            }
        }
        if (DbStatusCountLists != null && DbStatusCountLists.size() > 0) {
            return DbStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<ImmigrationCaseList> getDbCasesByStatus(long statusId) {
        List<ImmigrationCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListObject = tblImmigrationclaimRepo.getDbCasesListStatusWise(statusId);
        ImmigrationCaseList DbCaseList;
        if (DbCasesListObject != null && DbCasesListObject.size() > 0) {
            for (Object record : DbCasesListObject) {
                DbCaseList = new ImmigrationCaseList();
                Object[] row = (Object[]) record;

                DbCaseList.setImmigrationClaimCode((BigDecimal) row[0]);
                DbCaseList.setCreated((String) row[1]);
                DbCaseList.setCode((String) row[2]);
                DbCaseList.setClient((String) row[3]);
//                DbCaseList.setTaskDue((String) row[4]);
//                DbCaseList.setTaskName((String) row[5]);
                DbCaseList.setStatus((String) row[4]);
//                DbCaseList.setEmail((String) row[7]);
//                DbCaseList.setAddress((String) row[8]);
//                DbCaseList.setContactNo((String) row[9]);
//                DbCaseList.setLastUpdated((Date) row[10]);
//                DbCaseList.setIntroducer((String) row[11]);
//                DbCaseList.setLastNote((Date) row[12]);

                DbCaseLists.add(DbCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblImmigrationlog> getDbCaseLogs(String Dbcode) {
        List<TblImmigrationlog> TblImmigrationlogs = tblImmigrationlogRepo.findByTblImmigrationclaimImmigrationclaimcode(Long.valueOf(Dbcode));
        if (TblImmigrationlogs != null && TblImmigrationlogs.size() > 0) {
            for (TblImmigrationlog TblImmigrationlog : TblImmigrationlogs) {
                TblUser tblUser = tblUsersRepo.findById(TblImmigrationlog.getUsercode()).orElse(null);
                TblImmigrationlog.setUserName(tblUser.getUsername());
            }
            return TblImmigrationlogs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblImmigrationnote> getDbCaseNotes(String Dbcode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblImmigrationnote> tblDbnotes = tblImmigrationnoteRepo.findNotesOnImmigrationbyUserAndCategory(
                Long.valueOf(Dbcode), tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (tblDbnotes != null && tblDbnotes.size() > 0) {
            for (TblImmigrationnote tblDbnote : tblDbnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblDbnote.getUsercode()).orElse(null);
                tblDbnote.setUserName(tblUser1.getUsername());
                tblDbnote.setSelf(tblDbnote.getUsercode() == tblUser.getUsercode() ? true : false);
            }
            return tblDbnotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblImmigrationmessage> getDbCaseMessages(String Dbcode) {
        List<TblImmigrationmessage> TblImmigrationmessageList = tblImmigrationmessageRepo.findByTblImmigrationclaimImmigrationclaimcode(Long.valueOf(Dbcode));
        if (TblImmigrationmessageList != null && TblImmigrationmessageList.size() > 0) {
            for (TblImmigrationmessage TblImmigrationmessage : TblImmigrationmessageList) {
                TblUser tblUser = tblUsersRepo.findById(TblImmigrationmessage.getUsercode()).orElse(null);
                TblImmigrationmessage.setUserName(tblUser.getUsername());
            }
            return TblImmigrationmessageList;
        } else {
            return null;
        }
    }

    @Override
    public boolean isHdrCaseAllowed(String companycode, String s) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companycode,
                s);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TblImmigrationclaim saveDbRequest(TblImmigrationclaim TblImmigrationclaim) {
        return tblImmigrationclaimRepo.save(TblImmigrationclaim);
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public TblImmigrationnote addTblDbNote(TblImmigrationnote tblDbnote) {
        return tblImmigrationnoteRepo.save(tblDbnote);
    }

    @Override
    public TblImmigrationmessage getTblDbMessageById(String Dbmessagecode) {
        return tblImmigrationmessageRepo.findById(Long.valueOf(Dbmessagecode)).orElse(null);
    }

    @Override
    public TblEmail saveTblEmail(TblEmail tblEmail) {
        return tblEmailRepo.save(tblEmail);
    }

    @Override
    public TblImmigrationclaim assignCaseToSolicitor(AssignImmigrationCasetoSolicitor assignDbCasetoSolicitor, TblUser tblUser) {
        int update = tblImmigrationclaimRepo.performAction(new BigDecimal("41"), Long.valueOf(assignDbCasetoSolicitor.getImmigrationClaimCode()));
        if (update > 0) {

            TblImmigrationclaim TblImmigrationclaim1 = new TblImmigrationclaim();
            TblImmigrationlog TblImmigrationlog = new TblImmigrationlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(41))
                    .orElse(null);
            TblImmigrationclaim1.setImmigrationclaimcode(Long.valueOf(assignDbCasetoSolicitor.getImmigrationClaimCode()));

            TblImmigrationlog.setCreatedon(new Date());
            TblImmigrationlog.setDescr(tblStatus.getDescr());
            TblImmigrationlog.setUsercode(tblUser.getUsercode());
            TblImmigrationlog.setTblImmigrationclaim(TblImmigrationclaim1);

            int updateSolicitor = tblImmigrationsolicitorRepo.updateSolicitor("N", Long.valueOf(assignDbCasetoSolicitor.getImmigrationClaimCode()));

            TblImmigrationsolicitor TblImmigrationsolicitor = new TblImmigrationsolicitor();
            TblImmigrationsolicitor.setCompanycode(assignDbCasetoSolicitor.getSolicitorUserCode());
            TblImmigrationsolicitor.setCreatedon(new Date());
            TblImmigrationsolicitor.setTblImmigrationclaim(TblImmigrationclaim1);
            TblImmigrationsolicitor.setStatus("Y");
            TblImmigrationsolicitor.setUsercode(assignDbCasetoSolicitor.getSolicitorCode());

            TblImmigrationsolicitor = tblImmigrationsolicitorRepo.save(TblImmigrationsolicitor);
            TblImmigrationlog = tblImmigrationlogRepo.save(TblImmigrationlog);

            TblImmigrationclaim TblImmigrationclaim = tblImmigrationclaimRepo.findById(TblImmigrationclaim1.getImmigrationclaimcode()).orElse(null);

            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + TblImmigrationclaim.getImmigrationcode());
            tblEmail.setEmailsubject("HDR CASE" + TblImmigrationclaim.getImmigrationcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            TblImmigrationmessage TblImmigrationmessage = new TblImmigrationmessage();

            TblImmigrationmessage.setTblImmigrationclaim(TblImmigrationclaim1);
            TblImmigrationmessage.setUserName(tblUser.getUsername());
            TblImmigrationmessage.setCreatedon(new Date());
            TblImmigrationmessage.setMessage(tblEmail.getEmailbody());
            TblImmigrationmessage.setSentto(tblEmail.getEmailaddress());
            TblImmigrationmessage.setUsercode(tblUser.getUsercode());

            TblImmigrationmessage = tblImmigrationmessageRepo.save(TblImmigrationmessage);
            return tblImmigrationclaimRepo.findById(Long.valueOf(assignDbCasetoSolicitor.getImmigrationClaimCode())).orElse(null);

        } else {
            return null;
        }
    }

    @Override
    public TblImmigrationclaim performRejectCancelActionOnDb(String DbClaimCode, String toStatus, String reason, TblUser tblUser) {
        TblImmigrationsolicitor TblImmigrationsolicitor = tblImmigrationsolicitorRepo.findByTblImmigrationclaimImmigrationclaimcodeAndStatus(Long.valueOf(DbClaimCode), "Y");

        if (TblImmigrationsolicitor != null) {
            int update = tblImmigrationsolicitorRepo.updateRemarksReason(reason, "N", Long.valueOf(DbClaimCode));

            TblImmigrationclaim TblImmigrationclaim = tblImmigrationclaimRepo.findById(Long.valueOf(DbClaimCode)).orElse(null);
            TblImmigrationlog TblImmigrationlog = new TblImmigrationlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            TblImmigrationlog.setCreatedon(new Date());
            TblImmigrationlog.setDescr(tblStatus.getDescr());
            TblImmigrationlog.setUsercode(tblUser.getUsercode());
            TblImmigrationlog.setTblImmigrationclaim(TblImmigrationclaim);

            TblImmigrationlog = tblImmigrationlogRepo.save(TblImmigrationlog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(TblImmigrationclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("you have been hotkey the case number is " + TblImmigrationclaim.getImmigrationcode());
            tblEmailIntroducer.setEmailsubject("HDR CASE" + TblImmigrationclaim.getImmigrationcode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Solicitor

            if (TblImmigrationsolicitor != null) {
                TblUser solicitorUser = tblUsersRepo.findById(TblImmigrationsolicitor.getUsercode()).orElse(null);
                TblEmail tblEmailSolicitor = new TblEmail();
                tblEmailSolicitor.setSenflag(new BigDecimal(0));
                tblEmailSolicitor.setEmailaddress(solicitorUser.getUsername());
                tblEmailSolicitor.setEmailbody("you have been hotkey the case number is " + TblImmigrationclaim.getImmigrationcode());
                tblEmailSolicitor.setEmailsubject("HDR CASE" + TblImmigrationclaim.getImmigrationcode());
                tblEmailSolicitor.setCreatedon(new Date());

                tblEmailSolicitor = tblEmailRepo.save(tblEmailSolicitor);
            }

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + TblImmigrationclaim.getImmigrationcode());
            tblEmail.setEmailsubject("HDR CASE" + TblImmigrationclaim.getImmigrationcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return TblImmigrationclaim;

        } else {
            int update = tblImmigrationclaimRepo.performRejectCancelAction(reason, toStatus, Long.valueOf(DbClaimCode));

            TblImmigrationclaim TblImmigrationclaim = tblImmigrationclaimRepo.findById(Long.valueOf(DbClaimCode)).orElse(null);
            TblImmigrationlog TblImmigrationlog = new TblImmigrationlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            TblImmigrationlog.setCreatedon(new Date());
            TblImmigrationlog.setDescr(tblStatus.getDescr());
            TblImmigrationlog.setUsercode(tblUser.getUsercode());
            TblImmigrationlog.setTblImmigrationclaim(TblImmigrationclaim);

            TblImmigrationlog = tblImmigrationlogRepo.save(TblImmigrationlog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(TblImmigrationclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("you have been hotkey the case number is " + TblImmigrationclaim.getImmigrationcode());
            tblEmailIntroducer.setEmailsubject("RTA CASE" + TblImmigrationclaim.getImmigrationcode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + TblImmigrationclaim.getImmigrationcode());
            tblEmail.setEmailsubject("RTA CASE" + TblImmigrationclaim.getImmigrationcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return TblImmigrationclaim;

        }
    }

    @Override
    public TblImmigrationclaim performActionOnDb(String DbClaimCode, String toStatus, TblUser tblUser) {
        int update = tblImmigrationclaimRepo.performAction(new BigDecimal(toStatus), Long.valueOf(DbClaimCode));
        if (update > 0) {

            TblImmigrationclaim TblImmigrationclaim = tblImmigrationclaimRepo.findById(Long.valueOf(DbClaimCode)).orElse(null);
            TblImmigrationclaim TblImmigrationclaim1 = new TblImmigrationclaim();
            TblImmigrationlog TblImmigrationlog = new TblImmigrationlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);
            TblImmigrationclaim1.setImmigrationclaimcode(Long.valueOf(DbClaimCode));

            TblImmigrationlog.setCreatedon(new Date());
            TblImmigrationlog.setDescr(tblStatus.getDescr());
            TblImmigrationlog.setUsercode(tblUser.getUsercode());
            TblImmigrationlog.setTblImmigrationclaim(TblImmigrationclaim1);

            TblImmigrationlog = tblImmigrationlogRepo.save(TblImmigrationlog);

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(TblImmigrationclaim.getCreateuser().toString()).orElse(null);
            TblEmail tblEmailIntroducer = new TblEmail();
            tblEmailIntroducer.setSenflag(new BigDecimal(0));
            tblEmailIntroducer.setEmailaddress(introducerUser.getUsername());
            tblEmailIntroducer.setEmailbody("Action taken on  case number :: " + TblImmigrationclaim.getImmigrationcode());
            tblEmailIntroducer.setEmailsubject("HDR CASE" + TblImmigrationclaim.getImmigrationcode());
            tblEmailIntroducer.setCreatedon(new Date());

            tblEmailIntroducer = tblEmailRepo.save(tblEmailIntroducer);

            //Send Email to Solicitor
            TblImmigrationsolicitor tblHdrsolicitor = tblImmigrationsolicitorRepo.findByTblImmigrationclaimImmigrationclaimcodeAndStatus(TblImmigrationclaim.getImmigrationclaimcode(), "Y");
            if (tblHdrsolicitor != null) {
                TblUser solicitorUser = tblUsersRepo.findById(tblHdrsolicitor.getUsercode()).orElse(null);
                TblEmail tblEmailSolicitor = new TblEmail();
                tblEmailSolicitor.setSenflag(new BigDecimal(0));
                tblEmailSolicitor.setEmailaddress(solicitorUser.getUsername());
                tblEmailSolicitor.setEmailbody("Action taken on  case number :: " + TblImmigrationclaim.getImmigrationcode());
                tblEmailSolicitor.setEmailsubject("HDR CASE" + TblImmigrationclaim.getImmigrationcode());
                tblEmailSolicitor.setCreatedon(new Date());

                tblEmailSolicitor = tblEmailRepo.save(tblEmailSolicitor);
            }

            //Send Email to Legal Assist
            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("Action taken on  case number :: " + TblImmigrationclaim.getImmigrationcode());
            tblEmail.setEmailsubject("HDR CASE" + TblImmigrationclaim.getImmigrationcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            return TblImmigrationclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblImmigrationclaim findDbCaseByIdWithoutUser(long rtaCode) {
        return tblImmigrationclaimRepo.findById(rtaCode).orElse(null);
    }

    @Override
    public TblCompanyprofile getCompanyProfileAgainstDbCode(long Dbclaimcode) {
        return tblCompanyprofileRepo.getCompanyProfileAgainstDbCode(Dbclaimcode);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndAgenatureAndCountrytypeAndTblCompaignCompaigncode(companycode, ageNature, countryType, null);
    }

    @Override
    public String getDbDbColumnValue(String key, long Dbclaimcode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + Dbclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT FIRSTNAME || ' ' || NVL(MIDDLENAME,'') || ' ' || NVL(LASTNAME, '') from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + Dbclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT POSTALCODE || ' ' || ADDRESS1 || ' ' || ADDRESS2  || ' ' || ADDRESS3 || ' ' || CITY || ' ' || REGION from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + Dbclaimcode;
            query = em.createNativeQuery(sql);
        } else {
            String sql = "SELECT " + key + " from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + Dbclaimcode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")
        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    @Override
    public TblImmigrationdocument saveTblDbDocument(TblImmigrationdocument TblImmigrationdocument) {
        return tblImmigrationdocumentRepo.save(TblImmigrationdocument);
    }

    @Override
    public TblImmigrationtask getDbTaskByDbCodeAndTaskCode(long Dbclaimcode, long taskCode) {
        return tblImmigrationtaskRepo.findByTblImmigrationclaimImmigrationclaimcodeAndTaskcode(Dbclaimcode, new BigDecimal(taskCode));
    }

    @Override
    public TblImmigrationtask updateTblDbTask(TblImmigrationtask TblImmigrationtask) {
        return tblImmigrationtaskRepo.saveAndFlush(TblImmigrationtask);
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public List<TblImmigrationdocument> saveTblDbDocuments(List<TblImmigrationdocument> TblImmigrationdocuments) {
        return tblImmigrationdocumentRepo.saveAll(TblImmigrationdocuments);
    }

    @Override
    public List<TblImmigrationtask> findTblDbTaskByDbClaimCode(long Dbclaimcode) {
        return tblImmigrationtaskRepo.findByTblImmigrationclaimImmigrationclaimcode(Dbclaimcode);
    }

    @Override
    public List<ImmigrationCaseList> getAuthDbCasesIntroducers(String usercode) {
        List<ImmigrationCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListForIntroducers = tblImmigrationclaimRepo.getDbCasesListForIntroducers(usercode);
        ImmigrationCaseList immigrationCaseList;
        if (DbCasesListForIntroducers != null && DbCasesListForIntroducers.size() > 0) {
            for (Object record : DbCasesListForIntroducers) {
                immigrationCaseList = new ImmigrationCaseList();
                Object[] row = (Object[]) record;

                immigrationCaseList.setImmigrationClaimCode((BigDecimal) row[0]);
                immigrationCaseList.setCreated((String) row[1]);
                immigrationCaseList.setCode((String) row[2]);
                immigrationCaseList.setClient((String) row[3]);
                immigrationCaseList.setTaskDue((String) row[4]);
                immigrationCaseList.setTaskName((String) row[5]);
                immigrationCaseList.setStatus((String) row[6]);
                immigrationCaseList.setEmail((String) row[7]);
                immigrationCaseList.setAddress((String) row[8]);
                immigrationCaseList.setContactNo((String) row[9]);
                immigrationCaseList.setLastUpdated((Date) row[10]);
                immigrationCaseList.setIntroducer((String) row[11]);
                immigrationCaseList.setLastNote((Date) row[12]);
                DbCaseLists.add(immigrationCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<ImmigrationCaseList> getAuthDbCasesSolicitors(String usercode) {
        List<ImmigrationCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListForSolicitor = tblImmigrationclaimRepo.getDbCasesListForSolicitor(usercode);
        ImmigrationCaseList immigrationCaseList;
        if (DbCasesListForSolicitor != null && DbCasesListForSolicitor.size() > 0) {
            for (Object record : DbCasesListForSolicitor) {
                immigrationCaseList = new ImmigrationCaseList();
                Object[] row = (Object[]) record;

                immigrationCaseList.setImmigrationClaimCode((BigDecimal) row[0]);
                immigrationCaseList.setCreated((String) row[1]);
                immigrationCaseList.setCode((String) row[2]);
                immigrationCaseList.setClient((String) row[3]);
                immigrationCaseList.setTaskDue((String) row[4]);
                immigrationCaseList.setTaskName((String) row[5]);
                immigrationCaseList.setStatus((String) row[6]);
                immigrationCaseList.setEmail((String) row[7]);
                immigrationCaseList.setAddress((String) row[8]);
                immigrationCaseList.setContactNo((String) row[9]);
                immigrationCaseList.setLastUpdated((Date) row[10]);
                immigrationCaseList.setIntroducer((String) row[11]);
                immigrationCaseList.setLastNote((Date) row[12]);
                DbCaseLists.add(immigrationCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<ImmigrationCaseList> getAuthDbCasesLegalAssist() {
        List<ImmigrationCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesList = tblImmigrationclaimRepo.getDbCasesList();
        ImmigrationCaseList immigrationCaseList;
        if (DbCasesList != null && DbCasesList.size() > 0) {
            for (Object record : DbCasesList) {
                immigrationCaseList = new ImmigrationCaseList();
                Object[] row = (Object[]) record;

                immigrationCaseList.setImmigrationClaimCode((BigDecimal) row[0]);
                immigrationCaseList.setCreated((String) row[1]);
                immigrationCaseList.setCode((String) row[2]);
                immigrationCaseList.setClient((String) row[3]);
                immigrationCaseList.setTaskDue((String) row[4]);
                immigrationCaseList.setTaskName((String) row[5]);
                immigrationCaseList.setStatus((String) row[6]);
                immigrationCaseList.setEmail((String) row[7]);
                immigrationCaseList.setAddress((String) row[8]);
                immigrationCaseList.setContactNo((String) row[9]);
                immigrationCaseList.setLastUpdated((Date) row[10]);
                immigrationCaseList.setIntroducer((String) row[11]);
                immigrationCaseList.setLastNote((Date) row[12]);
                DbCaseLists.add(immigrationCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<ImigrationStatusCountList> getAllDbStatusCountsForIntroducers() {
        List<ImigrationStatusCountList> DbStatusCountLists = new ArrayList<>();
        List<Object> DbStatusCountListObject = tblImmigrationclaimRepo.getAllDbStatusCountsForIntroducers();
        ImigrationStatusCountList DbStatusCountList;
        if (DbStatusCountListObject != null && DbStatusCountListObject.size() > 0) {
            for (Object record : DbStatusCountListObject) {
                DbStatusCountList = new ImigrationStatusCountList();
                Object[] row = (Object[]) record;

                DbStatusCountList.setStatusCount((BigDecimal) row[0]);
                DbStatusCountList.setStatusName((String) row[1]);
                DbStatusCountList.setStatusCode((BigDecimal) row[2]);

                DbStatusCountLists.add(DbStatusCountList);
            }
        }
        if (DbStatusCountLists != null && DbStatusCountLists.size() > 0) {
            return DbStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<ImigrationStatusCountList> getAllDbStatusCountsForSolicitor() {
        List<ImigrationStatusCountList> DbStatusCountLists = new ArrayList<>();
        List<Object> DbStatusCountListObject = tblImmigrationclaimRepo.getAllDbStatusCountsForSolicitors();
        ImigrationStatusCountList DbStatusCountList;
        if (DbStatusCountListObject != null && DbStatusCountListObject.size() > 0) {
            for (Object record : DbStatusCountListObject) {
                DbStatusCountList = new ImigrationStatusCountList();
                Object[] row = (Object[]) record;

                DbStatusCountList.setStatusCount((BigDecimal) row[0]);
                DbStatusCountList.setStatusName((String) row[1]);
                DbStatusCountList.setStatusCode((BigDecimal) row[2]);

                DbStatusCountLists.add(DbStatusCountList);
            }
        }
        if (DbStatusCountLists != null && DbStatusCountLists.size() > 0) {
            return DbStatusCountLists;
        } else {
            return null;
        }
    }
}
