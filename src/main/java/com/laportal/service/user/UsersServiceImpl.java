package com.laportal.service.user;

import com.laportal.Repo.*;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.util.JWTSecurity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.*;

@Service
@Transactional(rollbackFor = Exception.class)
public class UsersServiceImpl implements UsersService {

    @PersistenceContext
    @Autowired
    EntityManager em;
    @Autowired
    private Environment env;
    @Autowired
    private TblModulesRepo tblModulesRepo;
    @Autowired
    private TblPagesRepo tblPagesRepo;
    @Autowired
    private TblRolesRepo tblRolesRepo;
    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;
    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;
    @Autowired
    private TblUsersRepo tblUsersRepo;
    @Autowired
    private TblRolepageRepo tblRolepageRepo;
    @Autowired
    private TblUserrolesRepo tblUserrolesRepo;
    @SuppressWarnings("unused")
    @Autowired
    private TblCompaignsRepo tblCompaignsRepo;
    @Autowired
    private TblLogintokenRepo tblLogintokenRepo;
    @Autowired
    private TblEmailRepo tblEmailRepo;
    @Autowired
    private TblBroadcastRepo tblBroadcastRepo;
    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;

    @Override
    public LoginResponse userLogin(LoginRequest loginRequest) {
        LoginResponse loginResponse = new LoginResponse();
        DashboardResponse dashboardResponse = new DashboardResponse();
        List<Nav> navs = new ArrayList<>();
        TblUser tblUser = tblUsersRepo.getUserbyLoginIdAndPassword(loginRequest.getUserName().toUpperCase(),loginRequest.getPassword());
        if (tblUser != null && tblUser.getStatus().equals("Y")) {
            if (tblUser != null && tblUser.getPassword().toUpperCase().equals(loginRequest.getPassword().toUpperCase())) {
                TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
                List<TblUserrole> tblUserroles = tblUserrolesRepo.findByTblUserUsercode(tblUser.getUsercode());
                if (tblUserroles != null && tblUserroles.size() > 0) {
                    List<String> roleCodes = new ArrayList<>();
                    for (TblUserrole tblUserrole : tblUserroles) {
                        roleCodes.add(tblUserrole.getRolecode());
                    }
                    List<Menu> menus = new ArrayList<>();
                    Menu menu = null;
                    List<TblModule> tblModules = tblModulesRepo.getRoleWiseModule(roleCodes);
                    if (tblModules != null && tblModules.size() > 0) {
                        for (TblModule tblModule : tblModules) {
                            List<Menu> childMenus = new ArrayList<>();
                            Menu childMenu = null;
                            List<TblPage> tblPages = tblPagesRepo.getRoleAndModuelWisePages(tblModule.getModulecode(), roleCodes);
                            if (tblPages != null && tblPages.size() > 0) {

                                Nav nav = new Nav();
                                nav.setLabel(tblModule.getModulename());

                                for (TblPage tblPage : tblPages) {
                                    if (tblPage.getPagestatus().equals("Y")) {
                                        childMenu = new Menu();
                                        childMenu.setLabel(tblPage.getPagename());
                                        childMenu.setTo(tblPage.getPagepath());
                                        childMenu.setIcon("pi-ellipsis-h");
                                        childMenus.add(childMenu);
                                        if (tblPage.getIsnewcase().equals("Y") || tblPage.getIslist().equals("Y")) {
                                            if (tblPage.getIsnewcase().equals("Y")) {
                                                nav.setIsAddnewcase("true");
                                                nav.setAddNewCaseLabel(tblPage.getPagename());
                                                nav.setAddNewCaseLink(tblPage.getPagepath());
                                            }
                                            if (tblPage.getIslist().equals("Y")) {
                                                nav.setIsList("true");
                                                nav.setListLabel(tblPage.getPagename());
                                                nav.setListLink(tblPage.getPagepath());
                                                nav.setBottomAreaShowLabel(tblPage.getPagename());
                                                nav.setBottomAreaShowLink(tblPage.getPagepath());
                                            }

                                        }

                                    }
                                }
                                navs.add(nav);
                            }
                            menu = new Menu();
                            menu.setLabel(tblModule.getModulename());
                            menu.setItems(childMenus);
                            menu.setIcon("pi-angle-double-right");
                            menus.add(menu);
                        }
                        TblLogintoken tblLogintoken = new TblLogintoken();
                        Map<String, Object> claims = new HashMap<>();
                        claims.put("userName", tblUser.getLoginid());
                        claims.put("companyCode", tblUser.getCompanycode());
                        claims.put("userCode", tblUser.getUsercode());

                        JWTSecurity jwtSecurity = new JWTSecurity();
                        String token = "Bearer " + jwtSecurity.createJWTWithClaims(tblUser.getUsercode(), claims,env);
                        Calendar calendar = Calendar.getInstance();
                        tblLogintoken.setEffectivefrom(calendar.getTime());
                        calendar.add(Calendar.MINUTE, Integer.parseInt(env.getProperty("loginTokenTime")));
                        tblLogintoken.setEffectiveto(calendar.getTime());


                        tblLogintoken.setToken(token);
                        tblLogintoken.setUsercode(tblUser.getUsercode());
                        tblLogintoken.setLogindate(new Date());
                        tblLogintoken = tblLogintokenRepo.save(tblLogintoken);
                        if (tblCompanyprofile != null) {
                            loginResponse.setTblUser(tblUser);
                            loginResponse.setTblCompanyprofile(tblCompanyprofile);
                            loginResponse.setLogin(true);
                            loginResponse.setDirectIntroducer(tblCompanyprofile.getDirectintroducer().equals("Y"));
                        }
                        dashboardResponse.setNav(navs);
                        loginResponse.setDashboardResponse(dashboardResponse);
                        loginResponse.setToken(tblLogintoken.getToken());
                        loginResponse.set_nav(menus);
                        loginResponse.setLogin(true);
                        loginResponse.setPwdUpdateFlag(tblUser.getPwdupdateflag());
                        loginResponse.setIdleLogoutTimeMM(String.valueOf(((Integer.parseInt(env.getProperty("loginTokenTime"))) * 60 * 1000))); //15 minutes
                        loginResponse.setMessage("Login Successful....!!!");
                        return loginResponse;
                    } else {
                        loginResponse.setLogin(false);
                        loginResponse.setMessage("No Menu Attach To The Role Of The User...");
                        return loginResponse;
                    }
                } else {
                    loginResponse.setLogin(false);
                    loginResponse.setMessage("No Role Attached With The User...");
                    return loginResponse;
                }
            } else {
                loginResponse.setLogin(false);
                loginResponse.setMessage("Invalid Username/Password...!!");
                return loginResponse;
            }
        } else {
            loginResponse.setLogin(false);
            loginResponse.setMessage("Invalid Username/Password...!!");
            return loginResponse;
        }
    }

    @Override
    public List<TblModule> getAllModules() {
        return tblModulesRepo.findAll();
    }

    @Override
    public List<TblPage> getMenuPages(String moduleCode, String companyCode, String compaignCode) {
        return tblPagesRepo.findByTblModuleModulecode(moduleCode);
//        return tblPagesRepo.getMenuPages(moduleCode, companyCode, compaignCode);
    }

    @Override
    public List<TblRole> getRoles(String categoryCode, String compaignCode) {
        return tblRolesRepo.findAll();
//        return tblRolesRepo.getRoles(categoryCode, compaignCode);
    }

    @Override
    public List<TblCompanyprofile> getAllCompaniesProfile() {
        List<TblCompanyprofile> tblCompanyprofiles = tblCompanyprofileRepo.findAllByOrderByNameAsc();
        List<TblCompanyprofile> Companyprofiles = new ArrayList<>();
        if (tblCompanyprofiles != null && tblCompanyprofiles.size() > 0) {
            for (TblCompanyprofile tblCompanyprofile : tblCompanyprofiles) {
                if (tblCompanyprofile.getCompanycode() != null) {
                    List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycode(tblCompanyprofile.getCompanycode());
                    List<TblUser> tblUsers = tblUsersRepo.findByCompanycode(tblCompanyprofile.getCompanycode());
                    List<LovResponse> lovResponses = new ArrayList<>();
                    LovResponse lovResponse = null;
                    for (TblUser tblUser : tblUsers) {
                        List<TblRole> tblRoles = tblRolesRepo.getAllRoleFromUserRoles(tblUser.getUsercode());
                        for (TblRole tblRole : tblRoles) {
                            lovResponse = new LovResponse();
                            lovResponse.setCode(tblRole.getRolecode());
                            lovResponse.setName(tblRole.getRolename());
                            lovResponses.add(lovResponse);
                        }
                        tblUser.setLovResponse(lovResponses);
                    }
                    tblCompanyprofile.setTblCompanyjobs(tblCompanyjobs);
                    tblCompanyprofile.setTblUsers(tblUsers);
                }
                Companyprofiles.add(tblCompanyprofile);
            }
            return Companyprofiles;
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        tblCompanyprofile.setCompanycode(String.valueOf(tblCompanyprofileRepo.getSeq()));
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public TblCompanyjob saveCompanyCompaign(TblCompanyjob tblCompanyjobs) {
        tblCompanyjobs.setCompanyjobcode(String.valueOf(tblCompanyjobsRepo.getSeq()));
        return tblCompanyjobsRepo.save(tblCompanyjobs);
    }

    @Override
    public TblUser saveCompanyUser(TblUser tblUser, List<String> rolecodes) {
        tblUser.setUsercode(String.valueOf(tblUsersRepo.getSeq()));
        TblUser user = tblUsersRepo.save(tblUser);
        TblUserrole userrole = saveCompanyUserRole(tblUser,
                rolecodes);
        if(userrole != null && user!= null){
            return user;
        }else{
            return null;
        }
    }

    @Override
    public TblUserrole saveCompanyUserRole(TblUser tblUser, List<String> rolecodes) {
        TblUserrole tblUserrole = null;
        for (String rolecode : rolecodes) {
            tblUserrole = new TblUserrole();
            tblUserrole.setUserrolecode(String.valueOf(tblUserrolesRepo.getSeq()));
            tblUserrole.setTblUser(tblUser);
            tblUserrole.setRolecode(rolecode);
            tblUserrole.setStatus("Y");
            tblUserrole = tblUserrolesRepo.save(tblUserrole);
        }
        return tblUserrole;
    }

    @Override
    public TblModule saveModule(TblModule tblModule) {
        tblModule.setModulecode(String.valueOf(tblModulesRepo.getSeq()));
        tblModule.setSystemcode("1");
        return tblModulesRepo.save(tblModule);
    }

    @Override
    public TblPage saveModulePage(TblPage tblPage) {
        tblPage.setPagecode(String.valueOf(tblPagesRepo.getSeq()));
        return tblPagesRepo.save(tblPage);
    }

    @Override
    public TblRole saveRole(TblRole tblRole) {
        tblRole.setRolecode(String.valueOf(tblRolesRepo.getSeq()));
        return tblRolesRepo.save(tblRole);
    }

    @Override
    public List<TblRolepage> saveRoleRights(List<TblRolepage> tblRolepages) {
        return tblRolepageRepo.saveAll(tblRolepages);
    }

    @Override
    public List<TblCompanyjob> getcompanyJobs(String companycode) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycode(companycode);
        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
//            for (TblCompanyjob tblCompanyjob : tblCompanyjobs) {
//                TblCompaign tblCompaign = tblCompaignsRepo.findById(tblCompanyjob.getCompaigncode()).orElse(null);
//                tblCompanyjob.setCompaigndescr(tblCompaign.getCompaignname());
//            }
            return tblCompanyjobs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblUser> getUsers(String companycode) {
        List<TblUser> tblUserList = tblUsersRepo.findByCompanycode(companycode);
        List<TblUser> tblUsers = new ArrayList<TblUser>();
        List<LovResponse> lovResponses = new ArrayList<>();
        LovResponse lovResponse = null;
        for (TblUser tblUser : tblUserList) {
            List<TblRole> tblRoles = tblRolesRepo.getAllRoleFromUserRoles(tblUser.getUsercode());
            lovResponses = new ArrayList<>();
            for (TblRole tblRole : tblRoles) {
                lovResponse = new LovResponse();
                lovResponse.setCode(tblRole.getRolecode());
                lovResponse.setName(tblRole.getRolename());
                lovResponses.add(lovResponse);
            }
            tblUser.setLovResponse(lovResponses);
            tblUsers.add(tblUser);
        }
        return tblUsers;
    }

    @Override
    public TblCompanyprofile getCompaniesProfile(String companyCode) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(companyCode).orElse(null);
        if (tblCompanyprofile.getCompanycode() != null) {
            List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycode(tblCompanyprofile.getCompanycode());
            List<TblUser> tblUsers = tblUsersRepo.findByCompanycode(tblCompanyprofile.getCompanycode());
            tblCompanyprofile.setTblCompanyjobs(tblCompanyjobs);
            tblCompanyprofile.setTblUsers(tblUsers);
            return tblCompanyprofile;
        } else {
            return null;
        }
    }

    @Override
    public int updateCompanyProfile(UpdateCompanyProfileRequest updateCompanyProfileRequest) {
        return tblCompanyprofileRepo.updateCompanyProfile(updateCompanyProfileRequest.getAccountno(),
                updateCompanyProfileRequest.getAddressline1(),
                updateCompanyProfileRequest.getAddressline2(),
                updateCompanyProfileRequest.getCity(),
                updateCompanyProfileRequest.getCompanyregno(),
                updateCompanyProfileRequest.getCompanystatus().getCode(),
                updateCompanyProfileRequest.getContactperson(),
                updateCompanyProfileRequest.getEmail(),
                updateCompanyProfileRequest.getName(),
                updateCompanyProfileRequest.getPhone(),
                updateCompanyProfileRequest.getPhone2(),
                updateCompanyProfileRequest.getPostcode(),
                updateCompanyProfileRequest.getRegion(),
                updateCompanyProfileRequest.getRemarks(),
                updateCompanyProfileRequest.getVaregno(),
                updateCompanyProfileRequest.getWebsite(),
                updateCompanyProfileRequest.getDirectIntroducer(),
                updateCompanyProfileRequest.getBilltoemail(),
                updateCompanyProfileRequest.getBilltoname(),
                updateCompanyProfileRequest.getAccountemail(),
                updateCompanyProfileRequest.getSecondaryaccountemail(),
                updateCompanyProfileRequest.getVat(),
                updateCompanyProfileRequest.getJurisdiction(),
                updateCompanyProfileRequest.getCompanycode());
    }

    @Override
    public TblCompanyjob updateCompanyJobs(UpdateCompanyCompaignRequest updateCompanyCompaignRequest) {

        TblCompanyjob tblCompanyjob = tblCompanyjobsRepo.findById(updateCompanyCompaignRequest.getCompanyjobcode()).orElse(null);
        if (tblCompanyjob != null) {

            tblCompanyjob.setStandardroadworthy(updateCompanyCompaignRequest.getStandardroadworthy() == null || updateCompanyCompaignRequest.getStandardroadworthy().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getStandardroadworthy()));
            tblCompanyjob.setStandardunroadworthy(updateCompanyCompaignRequest.getStandardunroadworthy() == null || updateCompanyCompaignRequest.getStandardunroadworthy().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getStandardunroadworthy()));
            tblCompanyjob.setPrestigeroadworthy(updateCompanyCompaignRequest.getPrestigeroadworthy() == null || updateCompanyCompaignRequest.getPrestigeroadworthy().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getPrestigeroadworthy()));
            tblCompanyjob.setPrestigeunroadworthy(updateCompanyCompaignRequest.getPrestigeunroadworthy() == null || updateCompanyCompaignRequest.getPrestigeunroadworthy().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getPrestigeunroadworthy()));
            tblCompanyjob.setRecovery(updateCompanyCompaignRequest.getRecovery() == null || updateCompanyCompaignRequest.getRecovery().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getRecovery()));
            tblCompanyjob.setSalvage(updateCompanyCompaignRequest.getSalvage() == null || updateCompanyCompaignRequest.getSalvage().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getSalvage()));
            tblCompanyjob.setRepairs(updateCompanyCompaignRequest.getRepairs() == null || updateCompanyCompaignRequest.getRepairs().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getRepairs()));
            tblCompanyjob.setFaultrepairs(updateCompanyCompaignRequest.getFaultrepairs() == null || updateCompanyCompaignRequest.getFaultrepairs().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getFaultrepairs()));
            tblCompanyjob.setSolicitorsfees(updateCompanyCompaignRequest.getSolicitorsfees() == null || updateCompanyCompaignRequest.getSolicitorsfees().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getSolicitorsfees()));
            tblCompanyjob.setHousingFee(updateCompanyCompaignRequest.getHousingFee() == null || updateCompanyCompaignRequest.getHousingFee().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getHousingFee()));
            tblCompanyjob.setBike(updateCompanyCompaignRequest.getBike() == null || updateCompanyCompaignRequest.getBike().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getBike()));
            tblCompanyjob.setStorage(updateCompanyCompaignRequest.getStorage() == null || updateCompanyCompaignRequest.getStorage().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getStorage()));
            tblCompanyjob.setWiplash(updateCompanyCompaignRequest.getWiplash() == null || updateCompanyCompaignRequest.getWiplash().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getWiplash()));
            tblCompanyjob.setHybrid(updateCompanyCompaignRequest.getHybrid() == null || updateCompanyCompaignRequest.getHybrid().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getHybrid()));
            tblCompanyjob.setMinor(updateCompanyCompaignRequest.getMinor() == null || updateCompanyCompaignRequest.getMinor().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getMinor()));
            tblCompanyjob.setScotishRta(updateCompanyCompaignRequest.getScotishRta() == null || updateCompanyCompaignRequest.getScotishRta().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getScotishRta()));
            tblCompanyjob.setPedestrian(updateCompanyCompaignRequest.getPedestrian() == null || updateCompanyCompaignRequest.getPedestrian().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getPedestrian()));
            tblCompanyjob.setRecovery(updateCompanyCompaignRequest.getRecovery() == null || updateCompanyCompaignRequest.getRecovery().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getRecovery()));
            tblCompanyjob.setSerious(updateCompanyCompaignRequest.getSerious() == null || updateCompanyCompaignRequest.getSerious().isEmpty() ? new BigDecimal(0) : new BigDecimal(updateCompanyCompaignRequest.getSerious()));
            tblCompanyjob.setStatus(updateCompanyCompaignRequest.getStatus());

            return tblCompanyjobsRepo.saveAndFlush(tblCompanyjob);
        } else {
            return null;
        }
    }

    @Override
    public int updateCompanyUser(UpdateCompanyUserRequest updateCompanyUserRequest) {

        TblUser tblUser = tblUsersRepo.findById(updateCompanyUserRequest.getUsercode()).orElse(null);
        if (tblUser != null) {

            tblUser.setUsername(updateCompanyUserRequest.getUsername());
            tblUser.setLoginid(updateCompanyUserRequest.getLoginid());
            tblUser.setStatus(updateCompanyUserRequest.getStatus());
            tblUsersRepo.saveAndFlush(tblUser);

            int delete = tblUserrolesRepo.deletAllUserRole(updateCompanyUserRequest.getUsercode());
            if (delete >= 0) {
                TblUserrole tblUserrole = null;
                for (String rolecode : updateCompanyUserRequest.getRolecodes()) {
                    tblUserrole = new TblUserrole();
                    tblUserrole.setUserrolecode(String.valueOf(tblUserrolesRepo.getSeq()));
                    tblUserrole.setRolecode(rolecode);
                    tblUserrole.setStatus("Y");
                    TblUser tblUser1 = new TblUser();
                    tblUser1.setUsercode(updateCompanyUserRequest.getUsercode());
                    tblUserrole.setTblUser(tblUser);
                    tblUserrole = tblUserrolesRepo.save(tblUserrole);
                }
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    @Override
    public List<TblRolepage> getRolesRights(String roleCode) {
        return tblRolepageRepo.findByTblRoleRolecode(roleCode);
    }

    @Override
    public int updatepassword(ChangePasswordRequest changePasswordRequest) {
        TblUser tblUser = tblUsersRepo.findByUsercode(changePasswordRequest.getUserId());
        if (tblUser != null && tblUser.getStatus().equals("Y")) {
            tblUser.setPassword(changePasswordRequest.getPassword());
            tblUser.setPwdupdateflag("Y");
            tblUser.setPwdlastupdated(new Date());
            tblUser = tblUsersRepo.saveAndFlush(tblUser);
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public List<TblUser> getAllUsers() {
        return tblUsersRepo.findAll();
    }

    @Override
    public TblLogintoken saveToken(TblLogintoken tblLogintoken) {
        return tblLogintokenRepo.save(tblLogintoken);
    }

    @Override
    public String changepwd(ChangePwdRequest changePwdRequest) {
        TblUser tblUser = tblUsersRepo.getUserbyLoginId(changePwdRequest.getUserId());
        if (tblUser != null && tblUser.getStatus().equals("Y")) {
            if (tblUser != null && tblUser.getPassword().toUpperCase().equals(changePwdRequest.getOldPassword().toUpperCase())) {
                tblUser.setPassword(changePwdRequest.getPassword());
                tblUser.setPwdupdateflag("Y");
                tblUser.setPwdlastupdated(new Date());
                tblUser = tblUsersRepo.save(tblUser);
                return "Password Changed Successfully";
            } else {
                return "Invalid Username/Password...!!";
            }
        } else {
            return "User Is Not Active";
        }
    }

    @Override
    public TblEmail saveEmail(String emailAddress, String emailBody, String emailSubject) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setCreatedon(new Date());

        return tblEmailRepo.save(tblEmail);
    }

    @Override
    public TblEmail saveEmail(TblEmail tblEmail) {
        return tblEmailRepo.saveAndFlush(tblEmail);
    }

    @Override
    public TblCompaign getCompaingByName(String compaigntype) {
        return tblCompaignsRepo.findByCompaignname(compaigntype);
    }

    @Override
    public List<TblCompanyprofile> getAllIntroducersCompaniesProfile() {
        return tblCompanyprofileRepo.getAllIntroducerCompanyProfile();
    }

    @Override
    public TblUser getUserById(String usercode) {
        return tblUsersRepo.findById(usercode).orElse(null);
    }

    @Override
    public TblBroadcast createBroadCastMessage(TblBroadcast tblBroadcast) {
        return tblBroadcastRepo.saveAndFlush(tblBroadcast);
    }

    @Override
    public List<TblBroadcast> getAllBroadCastMessages() {
        return tblBroadcastRepo.findAll();
    }

    @Override
    public TblBroadcast getBroadCastMessage(Long broadCastId) {
        return tblBroadcastRepo.findById(broadCastId).orElse(null);
    }

    @Override
    public TblCompaign getcompaingById(String campaignCode) {
        return tblCompaignsRepo.findById(campaignCode).orElse(null);
    }

    @Override
    public List<TblCompanydoc> getCompanyDocs(String companyCode) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycode(companyCode);
    }

    @Override
    public TblCompanydoc addcompanyCfa(AddcompanyCfaRequest addcompanyCfaRequest) {

        TblCompanydoc tblCompanydoc = new TblCompanydoc();
        tblCompanydoc.setAgenature(addcompanyCfaRequest.getAgeNature());
        tblCompanydoc.setBike(addcompanyCfaRequest.getBike());
        tblCompanydoc.setCountrytype(addcompanyCfaRequest.getCountryType());
        tblCompanydoc.setJointtenency(addcompanyCfaRequest.getJointTenency());
        tblCompanydoc.setPath("C:\\Esigns\\"+addcompanyCfaRequest.getFileName());
        tblCompanydoc.setCreatedon(new Date());

        TblCompanyprofile companyprofile = new TblCompanyprofile();
        companyprofile.setCompanycode(addcompanyCfaRequest.getCompanyCode());

        TblCompaign tblCompaign = new TblCompaign();
        tblCompaign.setCompaigncode(addcompanyCfaRequest.getCompaignCode());

        TblUser user = new TblUser();
        user.setUsercode("1");

        tblCompanydoc.setTblCompanyprofile(companyprofile);
        tblCompanydoc.setTblCompaign(tblCompaign);
        tblCompanydoc.setTblUser(user);

        return  tblCompanydocRepo.save(tblCompanydoc);

    }
}