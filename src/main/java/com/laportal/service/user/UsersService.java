package com.laportal.service.user;


import com.laportal.dto.*;
import com.laportal.model.*;

import java.util.List;

public interface UsersService {


    List<TblModule> getAllModules();

    List<TblPage> getMenuPages(String moduleCode, String companyCode, String compaignCode);

    List<TblRole> getRoles(String categoryCode, String compaignCode);

    List<TblCompanyprofile> getAllCompaniesProfile();

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    TblCompanyjob saveCompanyCompaign(TblCompanyjob tblCompanyjob);

    TblUser saveCompanyUser(TblUser tblUser, List<String> rolecodes);

    TblModule saveModule(TblModule tblModule);

    TblPage saveModulePage(TblPage tblPage);

    TblRole saveRole(TblRole tblRole);

    List<TblRolepage> saveRoleRights(List<TblRolepage> tblRolepages);

    TblUserrole saveCompanyUserRole(TblUser tblUser, List<String> rolecodes);

    List<TblCompanyjob> getcompanyJobs(String companycode);

    List<TblUser> getUsers(String companycode);

    TblCompanyprofile getCompaniesProfile(String companyCode);

    LoginResponse userLogin(LoginRequest loginRequest);

    int updateCompanyProfile(UpdateCompanyProfileRequest updateCompanyProfileRequest);

    TblCompanyjob updateCompanyJobs(UpdateCompanyCompaignRequest updateCompanyCompaignRequest);

    int updateCompanyUser(UpdateCompanyUserRequest updateCompanyUserRequest);

    List<TblRolepage> getRolesRights(String roleCode);


    int updatepassword(ChangePasswordRequest changePasswordRequest);

    List<TblUser> getAllUsers();

    TblLogintoken saveToken(TblLogintoken tblLogintoken);

    String changepwd(ChangePwdRequest changePwdRequest);

    TblEmail saveEmail(String emailAddress, String emailBody, String emailSubject);

    TblCompaign getCompaingByName(String compaigntype);

    List<TblCompanyprofile> getAllIntroducersCompaniesProfile();

    TblUser getUserById(String usercode);

    TblBroadcast createBroadCastMessage(TblBroadcast tblBroadcast);

    List<TblBroadcast> getAllBroadCastMessages();

    TblBroadcast getBroadCastMessage(Long broadCastId);

    TblEmail saveEmail(TblEmail tblEmail);

    TblCompaign getcompaingById(String campaignCode);

    List<TblCompanydoc> getCompanyDocs(String companyCode);

    TblCompanydoc addcompanyCfa(AddcompanyCfaRequest addcompanyCfaRequest);
}
