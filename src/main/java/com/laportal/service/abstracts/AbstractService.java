package com.laportal.service.abstracts;


import com.laportal.model.*;

import java.util.List;

public interface AbstractService {

    TblUser getUserById(String userCode);

    List<TblEmail> getPendingEmails();

    TblEmail saveTblEmail(TblEmail tblEmail);

    TblEmailTemplate getEmailTemplate(String userCatCode);

    List<TblBroadcast> getPendingBroadCasts();

    TblEmailTemplate getEmailTemplateByType(String emailType);

    TblEsignStatus saveEsignStatus(TblEsignStatus tblEsignStatus);

    TblEsignStatus getEsignStatus(String claimCode, String compaignCode);

    TblLogintoken verifyToken(String token);
}
