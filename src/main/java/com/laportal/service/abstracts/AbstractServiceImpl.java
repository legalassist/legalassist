package com.laportal.service.abstracts;

import com.laportal.Repo.*;
import com.laportal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class AbstractServiceImpl implements AbstractService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblUsersRepo tblUsersRepo;

    @Autowired
    private TblEmailRepo tblEmailRepo;

    @Autowired
    private TblEmailTemplateRepo tblEmailTemplateRepo;
    @Autowired
    private TblBroadcastRepo tblBroadcastRepo;
    @Autowired
    private TblEsignStatusRepo tblEsignStatusRepo;
    @Autowired
    private TblLogintokenRepo tblLogintokenRepo;

    @Override
    public TblUser getUserById(String userCode) {
        return tblUsersRepo.findById(userCode).orElse(null);
    }

    @Override
    public List<TblEmail> getPendingEmails() {
        return tblEmailRepo.findBySenflag(new BigDecimal(0));
    }

    @Override
    public TblEmail saveTblEmail(TblEmail tblEmail) {
        return tblEmailRepo.save(tblEmail);
    }

    @Override
    public TblEmailTemplate getEmailTemplate(String userCatCode) {
        return tblEmailTemplateRepo.findByUserccategorycode(new BigDecimal(userCatCode));
    }

    @Override
    public List<TblBroadcast> getPendingBroadCasts() {
        return tblBroadcastRepo.findByStatus("N");
    }

    @Override
    public TblEmailTemplate getEmailTemplateByType(String emailType) {
        return tblEmailTemplateRepo.findByEmailtype(emailType);
    }

    @Override
    public TblEsignStatus saveEsignStatus(TblEsignStatus tblEsignStatus) {
        return tblEsignStatusRepo.saveAndFlush(tblEsignStatus);
    }

    @Override
    public TblEsignStatus getEsignStatus(String claimCode, String compaignCode) {
        return tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode(compaignCode,new BigDecimal(claimCode));
    }

    @Override
    public TblLogintoken verifyToken(String token) {
        return tblLogintokenRepo.findByTokenBetweenEffectiveFrom(token,new Date());
    }
}
