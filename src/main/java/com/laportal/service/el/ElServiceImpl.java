package com.laportal.service.el;

import com.laportal.Repo.*;
import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class ElServiceImpl extends AbstractApi implements ElService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;

    @Autowired
    private TblElclaimRepo tblElclaimRepo;

    @Autowired
    private TblEllogRepo tblEllogRepo;

    @Autowired
    private TblElmessageRepo tblElmessageRepo;

    @Autowired
    private TblElnoteRepo tblElnoteRepo;

    @Autowired
    private TblElsolicitorRepo tblElsolicitorRepo;

    @Autowired
    private TblTaskRepo tblTaskRepo;

    @Autowired
    private TblUsersRepo tblUsersRepo;

    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;

    @Autowired
    private TblEmailRepo tblEmailRepo;

    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;

    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;

    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;

    @Autowired
    private TblEldocumentRepo tblEldocumentRepo;

    @Autowired
    private TblEltaskRepo tblEltaskRepo;

    @Autowired
    private TblEmailTemplateRepo tblEmailTemplateRepo;

    @Autowired
    private TblSolicitorInvoiceDetailRepo tblSolicitorInvoiceDetailRepo;

    @Autowired
    private TblEsignStatusRepo tblEsignStatusRepo;
    @Autowired
    private ViewElclaimRepo viewElclaimRepo;
    @Autowired
    private TblRtaflowRepo tblRtaflowRepo;


    @Override
    public List<ElCaseList> getElCases(TblUser tblUser) {
        List<ElCaseList> ElCaseLists = new ArrayList<>();
        List<Object> ElCasesListObject = tblElclaimRepo.getElCasesListUserWise();
        ElCaseList ElCaseList;
        if (ElCasesListObject != null && ElCasesListObject.size() > 0) {
            for (Object record : ElCasesListObject) {
                ElCaseList = new ElCaseList();
                Object[] row = (Object[]) record;

                ElCaseList.setElClaimCode((BigDecimal) row[0]);
                ElCaseList.setCreated((Date) row[1]);
                ElCaseList.setCode((String) row[2]);
                ElCaseList.setClient((String) row[3]);
//                ElCaseList.setTaskDue((String) row[4]);
//                ElCaseList.setTaskName((String) row[5]);
                ElCaseList.setStatus((String) row[4]);
//                ElCaseList.setEmail((String) row[7]);
//                ElCaseList.setAddress((String) row[8]);
//                ElCaseList.setContactNo((String) row[9]);
//                ElCaseList.setLastUpdated((Date) row[10]);
//                ElCaseList.setIntroducer((String) row[11]);
//                ElCaseList.setLastNote((Date) row[12]);

                ElCaseLists.add(ElCaseList);
            }
        }
        if (ElCaseLists != null && ElCaseLists.size() > 0) {
            return ElCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public TblElclaim findElCaseByIdForView(long elCaseId, TblUser tblUser) {
        TblElclaim tblElclaim = tblElclaimRepo.findById(elCaseId).orElse(null);
        tblElclaim.setIsEmployementHistory(tblElclaim.getJobtitle() != null ?"Y":"N");
        TblRtastatus tblRtastatus = tblRtastatusRepo.findById(tblElclaim.getStatus().longValue()).orElse(null);
        if (tblElclaim != null) {

            List<TblEldocument> tblEldocuments = tblEldocumentRepo.findByTblElclaimElclaimcode(elCaseId);
            if (tblEldocuments != null && tblEldocuments.size() > 0) {
                tblElclaim.setTblEldocuments(tblEldocuments);
            }

            List<RtaActionButton> rtaActionButtons = new ArrayList<>();
            List<RtaActionButton> rtaActionButtonsForLA = new ArrayList<>();
            RtaActionButton rtaActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    tblElclaim.getStatus().longValue(), "9",
                    tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetailForLA = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    tblElclaim.getStatus().longValue(), "9",
                    tblCompanyprofile.getTblUsercategory().getCategorycode());

            TblRtaflow tblRtaflow = tblRtaflowRepo.findByTblCompaignCompaigncodeAndTblRtastatusStatuscode("9",tblElclaim.getStatus().longValue());
            if(tblRtaflow != null) {
                tblElclaim.setEditFlag(tblRtaflow.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode()) ? tblRtaflow.getEditflag() : "N");
            }

            /// for introducer and solicitor
            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    rtaActionButton = new RtaActionButton();
                    rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                    rtaActionButtons.add(rtaActionButton);
                }

                // for LEGAL INTERNAL action buttons in dropdown

                if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                        rtaActionButton = new RtaActionButton();
                        rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                        rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                        rtaActionButtonsForLA.add(rtaActionButton);
                    }
                    tblElclaim.setElActionButtonForLA(rtaActionButtonsForLA);
                    tblElclaim.setEditFlag(tblRtaflowdetails.get(0).getTblRtaflow().getEditflag());
                    if(tblRtaflowdetailForLA.get(0).getTblRtaflow().getTaskflag().equalsIgnoreCase("Y")) {
                        List<TblEltask> tblEltasks = tblEltaskRepo.findByTblElclaimElclaimcode(tblElclaim.getElclaimcode());
                        tblElclaim.setTblEltasks(tblEltasks);
                    }
                }


                tblElclaim.setElActionButtons(rtaActionButtons);
                tblElclaim.setStatusDescr(tblRtastatus.getDescr());

                if (tblElclaim.getIntroducer() != null && tblElclaim.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblElclaim.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo
                            .findById(String.valueOf(tblElclaim.getIntroducer())).orElse(null);

                    TblElsolicitor tblElsolicitor = tblElsolicitorRepo
                            .findByTblElclaimElclaimcodeAndStatus(tblElclaim.getElclaimcode(), "Y");
                    if (tblElsolicitor != null) {
                        tblElclaim.setSolicitorcompany(tblCompanyprofileRepo
                                .findById(String.valueOf(tblElsolicitor.getCompanycode())).orElse(null).getName());
                        tblElclaim.setSolicitorusername(tblUsersRepo
                                .findByUsercode(String.valueOf(tblElsolicitor.getUsercode())).getLoginid());
                    }
                    tblElclaim.setAdvisorname(user == null ? null : user.getLoginid());
                    tblElclaim.setIntroducername(companyprofile == null ? null : companyprofile.getName());
                }

                List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("9", new BigDecimal(tblElclaim.getElclaimcode()));
                if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                    for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                        TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                        if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                            tblElclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblElclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                            tblElclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblElclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        }

                    }
                }
                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("9", new BigDecimal(tblElclaim.getElclaimcode()));
                tblElclaim.setTblEsignStatus(tblEsignStatus);


                return tblElclaim;

            } else {

                if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                        rtaActionButton = new RtaActionButton();
                        rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                        rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                        rtaActionButtonsForLA.add(rtaActionButton);
                    }
                    tblElclaim.setElActionButtons(rtaActionButtonsForLA);
                    if(tblRtaflowdetailForLA.get(0).getTblRtaflow().getTaskflag().equalsIgnoreCase("Y")) {
                        List<TblEltask> tblEltasks = tblEltaskRepo.findByTblElclaimElclaimcode(tblElclaim.getElclaimcode());
                        tblElclaim.setTblEltasks(tblEltasks);
                    }
                }

                if (tblElclaim.getIntroducer() != null && tblElclaim.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblElclaim.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo
                            .findById(String.valueOf(tblElclaim.getIntroducer())).orElse(null);

                    TblElsolicitor tblElsolicitor = tblElsolicitorRepo
                            .findByTblElclaimElclaimcodeAndStatus(tblElclaim.getElclaimcode(), "Y");
                    if (tblElsolicitor != null) {
                        tblElclaim.setSolicitorcompany(tblCompanyprofileRepo
                                .findById(String.valueOf(tblElsolicitor.getCompanycode())).orElse(null).getName());
                        tblElclaim.setSolicitorusername(tblUsersRepo
                                .findByUsercode(String.valueOf(tblElsolicitor.getUsercode())).getLoginid());
                    }
                    tblElclaim.setAdvisorname(user.getLoginid());
                    tblElclaim.setIntroducername(companyprofile.getName());
                }


                List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("9", new BigDecimal(tblElclaim.getElclaimcode()));
                if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                    for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                        TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                        if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                            tblElclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblElclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                            tblElclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblElclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        }

                    }
                }
                tblElclaim.setStatusDescr(tblRtastatus.getDescr());

                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("9", new BigDecimal(tblElclaim.getElclaimcode()));
                tblElclaim.setTblEsignStatus(tblEsignStatus);




                return tblElclaim;
            }
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public List<ElStatusCountList> getAllElStatusCounts() {
        List<ElStatusCountList> ElStatusCountLists = new ArrayList<>();
        List<Object> ElStatusCountListObject = tblElclaimRepo.getAllElStatusCounts();
        ElStatusCountList ElStatusCountList;
        if (ElStatusCountListObject != null && ElStatusCountListObject.size() > 0) {
            for (Object record : ElStatusCountListObject) {
                ElStatusCountList = new ElStatusCountList();
                Object[] row = (Object[]) record;

                ElStatusCountList.setStatusCount((BigDecimal) row[0]);
                ElStatusCountList.setStatusName((String) row[1]);
                ElStatusCountList.setStatusCode((BigDecimal) row[2]);

                ElStatusCountLists.add(ElStatusCountList);
            }
        }
        if (ElStatusCountLists != null && ElStatusCountLists.size() > 0) {
            return ElStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<ElCaseList> getElCasesByStatus(long statusId) {
        List<ElCaseList> ElCaseLists = new ArrayList<>();
        List<Object> ElCasesListObject = tblElclaimRepo.getElCasesListStatusWise(statusId);
        ElCaseList ElCaseList;
        if (ElCasesListObject != null && ElCasesListObject.size() > 0) {
            for (Object record : ElCasesListObject) {
                ElCaseList = new ElCaseList();
                Object[] row = (Object[]) record;

                ElCaseList.setElClaimCode((BigDecimal) row[0]);
                ElCaseList.setCreated((Date) row[1]);
                ElCaseList.setCode((String) row[2]);
                ElCaseList.setClient((String) row[3]);
//                ElCaseList.setTaskDue((String) row[4]);
//                ElCaseList.setTaskName((String) row[5]);
                ElCaseList.setStatus((String) row[4]);
//                ElCaseList.setEmail((String) row[7]);
//                ElCaseList.setAddress((String) row[8]);
//                ElCaseList.setContactNo((String) row[9]);
//                ElCaseList.setLastUpdated((Date) row[10]);
//                ElCaseList.setIntroducer((String) row[11]);
//                ElCaseList.setLastNote((Date) row[12]);

                ElCaseLists.add(ElCaseList);
            }
        }
        if (ElCaseLists != null && ElCaseLists.size() > 0) {
            return ElCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblEllog> getElCaseLogs(String Elcode) {
        List<TblEllog> tblEllogs = tblEllogRepo.findByTblElclaimElclaimcode(Long.valueOf(Elcode));
        if (tblEllogs != null && tblEllogs.size() > 0) {
            for (TblEllog tblEllog : tblEllogs) {
                TblUser tblUser = tblUsersRepo.findById(tblEllog.getUsercode()).orElse(null);
                tblEllog.setUserName(tblUser.getLoginid());
            }
            return tblEllogs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblElnote> getElCaseNotes(String Elcode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblElnote> tblElnotes = tblElnoteRepo.findNotesOnElbyUserAndCategory(Long.valueOf(Elcode),
                tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (tblElnotes != null && tblElnotes.size() > 0) {
            for (TblElnote tblElnote : tblElnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblElnote.getUsercode()).orElse(null);
                tblElnote.setUserName(tblUser1.getLoginid());
                tblElnote.setSelf(tblElnote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
                if (!tblElnote.getUsercode().equalsIgnoreCase(tblUser.getUsercode())) {
                    TblCompanyprofile recevingTblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
                    tblElnote.setUsercategorycode(recevingTblCompanyprofile.getTblUsercategory().getCategorycode());
                }
            }

            return tblElnotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblElmessage> getElCaseMessages(String Elcode) {
        List<TblElmessage> tblElmessageList = tblElmessageRepo.findByTblElclaimElclaimcodeOrderByCreatedon(Long.valueOf(Elcode));
        if (tblElmessageList != null && tblElmessageList.size() > 0) {
            for (TblElmessage tblElmessage : tblElmessageList) {
                TblUser tblUser = tblUsersRepo.findById(tblElmessage.getUsercode()).orElse(null);
                tblElmessage.setUserName(tblUser.getUsername());
            }
            return tblElmessageList;
        } else {
            return null;
        }
    }

    @Override
    public boolean isELCaseAllowed(String companycode, String campaignCode) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companycode,
                campaignCode);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TblElclaim saveElRequest(TblElclaim tblElclaim) {
        TblElclaim elclaim = tblElclaimRepo.save(tblElclaim);
        TblEllog tblEllog = new TblEllog();

        TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(559))
                .orElse(null);


        tblEllog.setCreatedon(new Date());
        tblEllog.setDescr(tblStatus.getDescr());
        tblEllog.setUsercode(tblElclaim.getCreateuser().toString());
        tblEllog.setTblElclaim(elclaim);
        tblEllog = tblEllogRepo.save(tblEllog);
        return elclaim;
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public TblElnote addTblElNote(TblElnote tblElnote) {
        tblElnote = tblElnoteRepo.save(tblElnote);
        TblElclaim tblElclaim = tblElclaimRepo.findById(tblElnote.getTblElclaim().getElclaimcode()).orElse(null);
        TblUser legalUser = tblUsersRepo.findByUsercode("2");
        // for legal internal

        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("note");
        String legalbody = tblEmailTemplate.getEmailtemplate();

        legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
        legalbody = legalbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
        legalbody = legalbody.replace("[CLIENT_NAME]",
                tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
        legalbody = legalbody.replace("[NOTE]", tblElnote.getNote());
        legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElcode());

        saveEmail(legalUser.getUsername(), legalbody, tblElclaim.getElcode() + " | Processing Note",
                tblElclaim, legalUser);

        if (!tblElnote.getUsercategorycode().equals("4")) {
            // for introducer
            if (tblElnote.getUsercategorycode().equals("1")) {
                TblUser introducerUser = tblUsersRepo
                        .findByUsercode(String.valueOf(tblElclaim.getAdvisor()));
                String introducerBody = tblEmailTemplate.getEmailtemplate();

                introducerBody = introducerBody.replace("[USER_NAME]", introducerUser.getLoginid());
                introducerBody = introducerBody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
                introducerBody = introducerBody.replace("[CLIENT_NAME]",
                        tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
                introducerBody = introducerBody.replace("[NOTE]", tblElnote.getNote());
                introducerBody = introducerBody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElcode());

                saveEmail(introducerUser.getUsername(), introducerBody,
                        tblElclaim.getElcode() + " | Processing Note", tblElclaim,
                        introducerUser);
            } else if (tblElnote.getUsercategorycode().equals("2")) {

                TblElsolicitor tblElsolicitor = tblElsolicitorRepo
                        .findByTblElclaimElclaimcodeAndStatus(tblElclaim.getElclaimcode(), "Y");

                if (tblElsolicitor != null) {
                    TblUser solicitorUser = tblUsersRepo.findById(tblElsolicitor.getUsercode()).orElse(null);

                    String solicitorBody = tblEmailTemplate.getEmailtemplate();

                    solicitorBody = solicitorBody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorBody = solicitorBody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
                    solicitorBody = solicitorBody.replace("[CLIENT_NAME]",
                            tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
                    solicitorBody = solicitorBody.replace("[NOTE]", tblElnote.getNote());
                    solicitorBody = solicitorBody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElcode());

                    saveEmail(solicitorUser.getUsername(), solicitorBody,
                            tblElclaim.getElcode() + " | Processing Note", tblElclaim,
                            solicitorUser);

                }
            }

        }

        return tblElnote;
    }

    @Override
    public TblElmessage getTblElMessageById(String Elmessagecode) {
        return tblElmessageRepo.findById(Long.valueOf(Elmessagecode)).orElse(null);
    }

    @Override
    public TblEmail saveTblEmail(TblEmail tblEmail) {
        return tblEmailRepo.save(tblEmail);
    }

    @Override
    public TblElclaim assignCaseToSolicitor(AssignElCasetoSolicitor assignElCasetoSolicitor, TblUser tblUser) {
        int update = tblElclaimRepo.performAction(new BigDecimal("41"), Long.valueOf(assignElCasetoSolicitor.getElClaimCode()), tblUser.getUsercode());
        if (update > 0) {

            TblElclaim tblElclaim1 = new TblElclaim();
            TblEllog tblEllog = new TblEllog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(41))
                    .orElse(null);
            tblElclaim1.setElclaimcode(Long.valueOf(assignElCasetoSolicitor.getElClaimCode()));

            tblEllog.setCreatedon(new Date());
            tblEllog.setDescr(tblStatus.getDescr());
            tblEllog.setUsercode(tblUser.getUsercode());
            tblEllog.setTblElclaim(tblElclaim1);

            int updateSolicitor = tblElsolicitorRepo.updateSolicitor("N", Long.valueOf(assignElCasetoSolicitor.getElClaimCode()));

            TblElsolicitor tblElsolicitor = new TblElsolicitor();
            tblElsolicitor.setCompanycode(assignElCasetoSolicitor.getSolicitorUserCode());
            tblElsolicitor.setCreatedon(new Date());
            tblElsolicitor.setTblElclaim(tblElclaim1);
            tblElsolicitor.setStatus("Y");
            tblElsolicitor.setUsercode(assignElCasetoSolicitor.getSolicitorCode());

            tblElsolicitor = tblElsolicitorRepo.save(tblElsolicitor);
            tblEllog = tblEllogRepo.save(tblEllog);

            TblElclaim tblElclaim = tblElclaimRepo.findById(tblElclaim1.getElclaimcode()).orElse(null);

            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setEmailaddress("lee.collier@legalassistltd.co.uk");
            tblEmail.setEmailbody("you have been hotkey the case number is " + tblElclaim.getElcode());
            tblEmail.setEmailsubject("HDR CASE" + tblElclaim.getElcode());
            tblEmail.setCreatedon(new Date());

            tblEmail = tblEmailRepo.save(tblEmail);

            TblElmessage tblElmessage = new TblElmessage();

            tblElmessage.setTblElclaim(tblElclaim1);
            tblElmessage.setUserName(tblUser.getUsername());
            tblElmessage.setCreatedon(new Date());
            tblElmessage.setMessage(tblEmail.getEmailbody());
            tblElmessage.setSentto(tblEmail.getEmailaddress());
            tblElmessage.setUsercode(tblUser.getUsercode());

            tblElmessage = tblElmessageRepo.save(tblElmessage);
            return tblElclaimRepo.findById(Long.valueOf(assignElCasetoSolicitor.getElClaimCode())).orElse(null);

        } else {
            return null;
        }
    }

    @Override
    public TblElclaim performRejectCancelActionOnEl(String elClaimCode, String toStatus, String reason, TblUser tblUser) {

        TblElclaim tblElclaim = tblElclaimRepo.findById(Long.valueOf(elClaimCode)).orElse(null);
        TblElsolicitor tblElsolicitor = tblElsolicitorRepo.findByTblElclaimElclaimcodeAndStatus(Long.valueOf(elClaimCode), "Y");
        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status-Reject");
        TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

        if (tblElsolicitor != null) {
            int update = tblElsolicitorRepo.updateRemarksReason(reason, "N", Long.parseLong(elClaimCode));


            TblUser SolicitorUser = tblUsersRepo.findById(tblElsolicitor.getUsercode()).orElse(null);
            TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblElsolicitor.getCompanycode())
                    .orElse(null);

            TblElnote tblElnote = new TblElnote();
            // NOte for Legal internal user and NOTE for introducer
            tblElnote.setTblElclaim(tblElclaim);
            tblElnote.setNote(SolicitorCompany.getName() + " has rejected the case. Reason :" + reason);
            tblElnote.setUsercategorycode("1");
            tblElnote.setCreatedon(new Date());
            tblElnote.setUsercode("1");

            addTblElNote(tblElnote);

            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            // Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(tblElclaim.getAdvisor())).orElse(null);

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
            introducerbody = introducerbody.replace("[SOLICITOR_NAME]", SolicitorCompany.getName());
            introducerbody = introducerbody.replace("[REASON]", reason);
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

            saveEmail(introducerUser.getUsername(), introducerbody, tblElclaim.getElclaimcode() + " | " + tblStatus.getDescr(),
                    tblElclaim, introducerUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
            legalbody = legalbody.replace("[SOLICITOR_NAME]", SolicitorCompany.getName());
            legalbody = legalbody.replace("[REASON]", reason);
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

            saveEmail(legalUser.getUsername(), legalbody, tblElclaim.getElclaimcode() + " | " + tblStatus.getDescr(),
                    tblElclaim, introducerUser);

        } else {

            TblElnote tblElnote = new TblElnote();
            // NOte for Legal internal user and NOTE for introducer
            tblElnote.setTblElclaim(tblElclaim);
            tblElnote.setNote(tblUser.getLoginid() + " has rejected the case. Reason :" + reason);
            tblElnote.setUsercategorycode("1");
            tblElnote.setCreatedon(new Date());
            tblElnote.setUsercode("1");

            addTblElNote(tblElnote);

            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(tblElclaim.getAdvisor())).orElse(null);

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
            introducerbody = introducerbody.replace("[SOLICITOR_NAME]", tblUser.getLoginid());
            introducerbody = introducerbody.replace("[REASON]", reason);
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

            saveEmail(introducerUser.getUsername(), introducerbody, tblElclaim.getElclaimcode() + " | " + tblStatus.getDescr(),
                    tblElclaim, introducerUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
            legalbody = legalbody.replace("[SOLICITOR_NAME]", tblUser.getLoginid());
            legalbody = legalbody.replace("[REASON]", reason);
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

            saveEmail(legalUser.getUsername(), legalbody, tblElclaim.getElclaimcode() + " | " + tblStatus.getDescr(),
                    tblElclaim, introducerUser);
        }

        TblEllog tblEllog = new TblEllog();


        tblEllog.setCreatedon(new Date());
        tblEllog.setDescr(tblStatus.getDescr());
        tblEllog.setUsercode(tblUser.getUsercode());
        tblEllog.setTblElclaim(tblElclaim);
        tblEllog.setOldstatus(tblElclaim.getStatus().longValue());
        tblEllog.setNewstatus(Long.valueOf(toStatus));

        tblEllog = tblEllogRepo.save(tblEllog);
        int update = tblElclaimRepo.performRejectCancelAction(reason, toStatus, tblElclaim.getElclaimcode(), tblUser.getUsercode());

        return tblElclaim;
    }

    @Override
    public TblElclaim performActionOnEl(String elClaimCode, String toStatus, TblUser tblUser) {
        int update = tblElclaimRepo.performAction(new BigDecimal(toStatus), Long.valueOf(elClaimCode), tblUser.getUsercode());
        if (update > 0) {

            TblElclaim tblElclaim = tblElclaimRepo.findById(Long.valueOf(elClaimCode)).orElse(null);
            TblEllog tblEllog = new TblEllog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblEllog.setCreatedon(new Date());
            tblEllog.setDescr(tblStatus.getDescr());
            tblEllog.setUsercode(tblUser.getUsercode());
            tblEllog.setTblElclaim(tblElclaim);
            tblEllog.setOldstatus(tblElclaim.getStatus().longValue());
            tblEllog.setNewstatus(Long.valueOf(toStatus));

            tblEllog = tblEllogRepo.save(tblEllog);
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status");

            // for introducer
            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblElclaim.getAdvisor()));
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, introducerUser);

            // for legal internal
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
            legalbody = legalbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, legalUser);

            if (toStatus.equals(559)) {
                // when reverting a case takes to new status remove all solcitors from case if
                // any
                int updateSolicitor = tblElsolicitorRepo.updateSolicitor("N", Long.parseLong(elClaimCode));
                TblElsolicitor tblElsolicitor = tblElsolicitorRepo
                        .findByTblElclaimElclaimcodeAndStatus(tblElclaim.getElclaimcode(), "Y");
                if (tblElsolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblElsolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            } else {
                // for normal emails
                TblElsolicitor tblRtasolicitor = tblElsolicitorRepo
                        .findByTblElclaimElclaimcodeAndStatus(tblElclaim.getElclaimcode(), "Y");
                if (tblRtasolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblRtasolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            }

            // if submmitting a case add clawback date current date + 90 days
            TblElclaim tblElclaimForClawBack = tblElclaimRepo.findById(tblElclaim.getElclaimcode()).orElse(null);
            if (toStatus.equals("565")) {
                Date clawbackDate = addDaysToDate(90);
                tblElclaimRepo.updateClawbackdate(clawbackDate, tblElclaim.getElclaimcode());
                tblElclaimRepo.updateSubmitDate(new Date(), tblElclaim.getElclaimcode());
            } else if (toStatus.equals("576")) {
                Date clawbackDate = addDaysToSpecificDate(tblElclaimForClawBack.getClawbackDate(), 30);
                tblElclaimRepo.updateClawbackdate(clawbackDate, tblElclaim.getElclaimcode());
            } else if (toStatus.equals("578")) {
                Date clawbackDate = addDaysToSpecificDate(tblElclaimForClawBack.getClawbackDate(), 60);
                tblElclaimRepo.updateClawbackdate(clawbackDate, tblElclaim.getElclaimcode());
            } else if (toStatus.equals("579")) {
                Date clawbackDate = addDaysToSpecificDate(tblElclaimForClawBack.getClawbackDate(), 90);
                tblElclaimRepo.updateClawbackdate(clawbackDate, tblElclaim.getElclaimcode());
            } else if (toStatus.equals("563")) {
                TblElnote tblElnote = new TblElnote();
                TblElsolicitor tblElsolicitor = tblElsolicitorRepo
                        .findByTblElclaimElclaimcodeAndStatus(tblElclaim.getElclaimcode(), "Y");
                TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblElsolicitor.getCompanycode())
                        .orElse(null);
                // NOte for Legal internal user and NOTE for introducer
                tblElnote.setTblElclaim(tblElclaim);
                tblElnote.setNote("Case Accepted By " + SolicitorCompany.getName());
                tblElnote.setUsercategorycode("1");
                tblElnote.setCreatedon(new Date());
                tblElnote.setUsercode("1");

                addTblElNote(tblElnote);
            }
            return tblElclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblElclaim findElCaseByIdWithoutUser(long elCode) {
        return tblElclaimRepo.findById(elCode).orElse(null);
    }

    @Override
    public TblCompanyprofile getCompanyProfileAgainstElCode(long Elclaimcode) {
        return tblCompanyprofileRepo.getCompanyProfileAgainstElCode(Elclaimcode);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndAgenatureAndCountrytypeAndTblCompaignCompaigncode(companycode, ageNature, countryType, null);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndTblCompaignCompaigncode(companycode, "9");
    }

    @Override
    public TblElclaim updateElCase(TblElclaim tblElclaim) {
        return tblElclaimRepo.saveAndFlush(tblElclaim);
    }

    @Override
    public String getElDbColumnValue(String key, long Elclaimcode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from TBL_ELCLAIM  WHERE ELCLAIMCODE = " + Elclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT FIRSTNAME || ' ' || NVL(MIDDLENAME,'') || ' ' || NVL(LASTNAME, '') from TBL_ELCLAIM  WHERE ELCLAIMCODE = " + Elclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT POSTALCODE || ' ' || ADDRESS1 || ' ' || ADDRESS2  || ' ' || ADDRESS3 || ' ' || CITY || ' ' || REGION from TBL_ELCLAIM  WHERE ELCLAIMCODE = " + Elclaimcode;
            query = em.createNativeQuery(sql);
        } else {
            String sql = "SELECT " + key + " from Tbl_tenancyclaim  WHERE tenancyclaimcode = " + Elclaimcode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")
        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    @Override
    public TblEldocument saveTblElDocument(TblEldocument tblEldocument) {
        return tblEldocumentRepo.save(tblEldocument);
    }

    @Override
    public TblEltask getElTaskByElCodeAndTaskCode(long Elclaimcode, long taskCode) {
        return tblEltaskRepo.findByTblElclaimElclaimcodeAndTblTaskTaskcode(Elclaimcode, new BigDecimal(taskCode));
    }

    @Override
    public int updateTblElTask(TblEltask tblEltask) {
        return tblEltaskRepo.updatetask(tblEltask.getStatus(), tblEltask.getRemarks(), tblEltask.getEltaskcode(), tblEltask.getTblElclaim().getElclaimcode());
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public List<TblEltask> findTblElTaskByElClaimCode(long Elclaimcode) {
        return tblEltaskRepo.findByTblElclaimElclaimcode(Elclaimcode);
    }


    @Override
    public List<ElStatusCountList> getAllElStatusCountsForIntroducers(String companycode) {
        List<ElStatusCountList> ElStatusCountLists = new ArrayList<>();
        List<Object> ElStatusCountListObject = tblElclaimRepo.getAllElStatusCountsForIntroducers(companycode);
        ElStatusCountList ElStatusCountList;
        if (ElStatusCountListObject != null && ElStatusCountListObject.size() > 0) {
            for (Object record : ElStatusCountListObject) {
                ElStatusCountList = new ElStatusCountList();
                Object[] row = (Object[]) record;

                ElStatusCountList.setStatusCount((BigDecimal) row[0]);
                ElStatusCountList.setStatusName((String) row[1]);
                ElStatusCountList.setStatusCode((BigDecimal) row[2]);

                ElStatusCountLists.add(ElStatusCountList);
            }
        }
        if (ElStatusCountLists != null && ElStatusCountLists.size() > 0) {
            return ElStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<ElStatusCountList> getAllElStatusCountsForSolicitor(String companycode) {
        List<ElStatusCountList> ElStatusCountLists = new ArrayList<>();
        List<Object> ElStatusCountListObject = tblElclaimRepo.getAllElStatusCountsForSolicitors(companycode);
        ElStatusCountList ElStatusCountList;
        if (ElStatusCountListObject != null && ElStatusCountListObject.size() > 0) {
            for (Object record : ElStatusCountListObject) {
                ElStatusCountList = new ElStatusCountList();
                Object[] row = (Object[]) record;

                ElStatusCountList.setStatusCount((BigDecimal) row[0]);
                ElStatusCountList.setStatusName((String) row[1]);
                ElStatusCountList.setStatusCode((BigDecimal) row[2]);

                ElStatusCountLists.add(ElStatusCountList);
            }
        }
        if (ElStatusCountLists != null && ElStatusCountLists.size() > 0) {
            return ElStatusCountLists;
        } else {
            return null;
        }
    }


    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblElclaim tblElclaim,
                             TblUser tblUser) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblElmessage tblElmessage = new TblElmessage();

        tblElmessage.setTblElclaim(tblElclaim);
        tblElmessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblElmessage.setCreatedon(new Date());
        tblElmessage.setMessage(tblEmail.getEmailbody());
        tblElmessage.setSentto(tblEmail.getEmailaddress());
        tblElmessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblElmessage.setEmailcode(tblEmail);

        tblElmessage = tblElmessageRepo.save(tblElmessage);

        return true;
    }

    @Override
    public TblEmail resendEmail(String elmessagecode) {
        TblElmessage tblElmessage = tblElmessageRepo.findById(Long.valueOf(elmessagecode)).orElse(null);

        TblEmail tblEmail = tblEmailRepo.findById(tblElmessage.getEmailcode().getEmailcode()).orElse(null);

        tblEmail.setSenflag(new BigDecimal(0));

        return tblEmailRepo.saveAndFlush(tblEmail);
    }

    @Override
    public List<TblEltask> getELTaskAgainstELCodeAndStatus(String elClaimCode, String status) {
        return tblEltaskRepo.findByTblElclaimElclaimcodeAndStatus(Long.parseLong(elClaimCode), status);
    }

    @Override
    public TblElclaim performRevertActionOnEL(String elClaimCode, String toStatus, String reason, TblUser tblUser) {
        return null;
    }

    @Override
    public TblElclaim performActionOnElFromDirectIntro(PerformHotKeyOnRtaRequest performHotKeyOnElRequest, TblUser tblUser) {
        TblElclaim tblElclaim = tblElclaimRepo.findById(Long.valueOf(performHotKeyOnElRequest.getElcode()))
                .orElse(null);

        int update = tblElclaimRepo.performAction(new BigDecimal(performHotKeyOnElRequest.getToStatus()),
                Long.valueOf(performHotKeyOnElRequest.getElcode()), tblUser.getUsercode());
        if (update > 0) {

            TblEllog tblEllog = new TblEllog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(performHotKeyOnElRequest.getToStatus()))
                    .orElse(null);

            tblEllog.setCreatedon(new Date());
            tblEllog.setDescr(tblStatus.getDescr());
            tblEllog.setUsercode(tblUser.getUsercode());
            tblEllog.setTblElclaim(tblElclaim);
            tblEllog.setOldstatus(tblElclaim.getStatus().longValue());
            tblEllog.setNewstatus(Long.valueOf(performHotKeyOnElRequest.getToStatus()));

            int updateSolicitor = tblElsolicitorRepo.updateSolicitor("N", tblElclaim.getElclaimcode());

            TblElsolicitor tblElsolicitor = new TblElsolicitor();
            tblElsolicitor.setCompanycode(performHotKeyOnElRequest.getSolicitorCode());
            tblElsolicitor.setCreatedon(new Date());
            tblElsolicitor.setTblElclaim(tblElclaim);
            tblElsolicitor.setStatus("Y");
            tblElsolicitor.setUsercode(performHotKeyOnElRequest.getSolicitorUserCode());

            tblElsolicitor = tblElsolicitorRepo.save(tblElsolicitor);
            tblEllog = tblEllogRepo.save(tblEllog);

            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblElclaim.getAdvisor()));
            TblUser solicitorUser = tblUsersRepo
                    .findByUsercode(String.valueOf(performHotKeyOnElRequest.getSolicitorUserCode()));
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status-hotkey");

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                    tblElclaim.getFirstname() + " "
//                            + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                            + tblElclaim.getLastname());
//            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    tblElclaim.getRtanumber() + " | HotKey", tblElclaim, introducerUser);

            // for solicitor
            String solicitorbody = tblEmailTemplate.getEmailtemplate();

            solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
            solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
            solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                    tblElclaim.getFirstname() + " "
//                            + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                            + tblElclaim.getLastname());
//            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

            saveEmail(solicitorUser.getUsername(), solicitorbody, tblElclaim.getElcode() + " | Processing Note",
                    tblElclaim, solicitorUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                    tblElclaim.getFirstname() + " "
//                            + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                            + tblElclaim.getLastname());

            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblElclaim.getElclaimcode());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    tblElclaim.getRtanumber() + " | HotKey", tblElclaim, legalUser);


            return tblElclaim;

        } else {
            return null;
        }
    }

    @Override
    public List<TblEldocument> addElDocument(List<TblEldocument> tblEldocuments) {
        return tblEldocumentRepo.saveAll(tblEldocuments);
    }

    @Override
    public TblEmailTemplate findByEmailType(String emailType) {
        return tblEmailTemplateRepo.findByEmailtype(emailType);
    }

    @Override
    public TblUser findByUserId(String userCode) {
        return tblUsersRepo.findByUsercode(userCode);
    }

    @Override
    public TblElsolicitor getELSolicitorsOfElClaim(long elClaimCode) {
        return tblElsolicitorRepo.findByTblElclaimElclaimcodeAndStatus(elClaimCode, "Y");
    }

    @Override
    public int updateCurrentTask(long taskCode, long elCode, String current) {
        tblEltaskRepo.setAllTask("N", elCode);
        return tblEltaskRepo.setCurrentTask(current, taskCode, elCode);
    }

    @Override
    public List<TblEltask> getAuthElCaseTasks(String elcode) {
        return tblEltaskRepo.findByTblElclaimElclaimcode(Long.valueOf(elcode));
    }

    @Override
    public void deleteElDocument(long eldoccode) {
        tblEldocumentRepo.deleteById(eldoccode);
    }

    @Override
    public List<TblEldocument> getAuthElCasedocuments(long elclaimcode) {
        return tblEldocumentRepo.findByTblElclaimElclaimcode(elclaimcode);
    }

    @Override
    public TblEldocument addElSingleDocumentSingle(TblEldocument tblEldocument) {
        return tblEldocumentRepo.saveAndFlush(tblEldocument);
    }

    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblElclaim tblElclaim, TblUser tblUser, String attachment) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setEmailattachment(attachment);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblElmessage tblElmessage = new TblElmessage();

        tblElmessage.setTblElclaim(tblElclaim);
        tblElmessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblElmessage.setCreatedon(new Date());
        tblElmessage.setMessage(tblEmail.getEmailbody());
        tblElmessage.setSentto(tblEmail.getEmailaddress());
        tblElmessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblElmessage.setEmailcode(tblEmail);

        tblElmessage = tblElmessageRepo.save(tblElmessage);

        return true;
    }

    @Override
    public List<TblElnote> getAuthElCaseNotesOfLegalInternal(String elcode, TblUser tblUser) {
        List<TblElnote> tblElnotes = tblElnoteRepo.getAuthElCaseNotesOfLegalInternal(Long.valueOf(elcode),
                tblUser.getUsercode());
        if (tblElnotes != null && tblElnotes.size() > 0) {
            for (TblElnote tblElnote : tblElnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblElnote.getUsercode()).orElse(null);
                tblElnote.setUserName(tblUser1.getLoginid());
                tblElnote.setSelf(tblElnote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
            }

            return tblElnotes;
        } else {
            return null;
        }
    }




    @Override
    public TblElclaim findElCaseById(Long elcode, TblUser tblUser) {
        return tblElclaimRepo.findById(elcode).orElse(null);
    }


    @Override
    public List<ElCaseList> getAuthElCasesUserAndCategoryWise(String usercode, String categorycode) {
        List<ViewElclaim> viewElclaimList = viewElclaimRepo.getAuthElCasesUserAndCategoryWise(new BigDecimal(usercode), new BigDecimal(categorycode));
        if (viewElclaimList != null && viewElclaimList.size() > 0) {
            List<ElCaseList> elCaseLists = new ArrayList<>();
            for (ViewElclaim viewElclaim : viewElclaimList) {
                ElCaseList elCaseList = new ElCaseList();

                elCaseList.setElClaimCode(viewElclaim.getElclaimcode());
                elCaseList.setCreated(viewElclaim.getCreatedate());
                elCaseList.setCode(viewElclaim.getElcode());
                elCaseList.setClient(viewElclaim.getFirstname() + (viewElclaim.getMiddlename() != null?viewElclaim.getMiddlename():"")+" "+viewElclaim.getLastname());
//                elCaseList.setTaskDue;
                elCaseList.setCurrentTask(viewElclaim.getEltaskname());
                elCaseList.setStatus(viewElclaim.getStatus());
                elCaseList.setLastUpdated(viewElclaim.getUpdatedate());
                elCaseList.setIntroducer(viewElclaim.getIntroducerUser());

                elCaseLists.add(elCaseList);
            }
            return elCaseLists;


        } else {
            return null;
        }
    }

    @Override
    public List<ElCaseList> getAuthElCasesUserAndCategoryWiseAndStatusWise(String usercode, String status, String categorycode) {
        List<ViewElclaim> viewElclaimList = viewElclaimRepo.getAuthElCasesUserAndCategoryWiseAndStatusWise(new BigDecimal(usercode), new BigDecimal(categorycode), new BigDecimal(status));
        if (viewElclaimList != null && viewElclaimList.size() > 0) {
            List<ElCaseList> elCaseLists = new ArrayList<>();
            for (ViewElclaim viewElclaim : viewElclaimList) {
                ElCaseList elCaseList = new ElCaseList();

                elCaseList.setElClaimCode(viewElclaim.getElclaimcode());
                elCaseList.setCreated(viewElclaim.getCreatedate());
                elCaseList.setCode(viewElclaim.getElcode());
                elCaseList.setClient(viewElclaim.getFirstname() + (viewElclaim.getMiddlename() != null?viewElclaim.getMiddlename():"")+" "+viewElclaim.getLastname());
//                elCaseList.setTaskDue;
                elCaseList.setCurrentTask(viewElclaim.getEltaskname());
                elCaseList.setStatus(viewElclaim.getStatus());
                elCaseList.setLastUpdated(viewElclaim.getUpdatedate());
                elCaseList.setIntroducer(viewElclaim.getIntroducerUser());

                elCaseLists.add(elCaseList);
            }
            return elCaseLists;


        } else {
            return null;
        }
    }

    @Override
    public List<RtaAuditLogResponse> getElAuditLogs(Long elCode) {
        List<RtaAuditLogResponse> rtaAuditLogResponses = new ArrayList<>();
        RtaAuditLogResponse rtaAuditLogResponse = null;
        List<Object> auditlogsObject = tblElclaimRepo.getElAuditLogs(elCode);
        RtaStatusCountList PlStatusCountList;
        if (auditlogsObject != null && auditlogsObject.size() > 0) {
            for (Object record : auditlogsObject) {
                rtaAuditLogResponse = new RtaAuditLogResponse();
                Object[] row = (Object[]) record;

                rtaAuditLogResponse.setFieldName((String) row[0]);
                rtaAuditLogResponse.setOldValue((String) row[1]);
                rtaAuditLogResponse.setNewValue((String) row[2]);
                rtaAuditLogResponse.setAuditDate((Date) row[3]);
                rtaAuditLogResponse.setLoggedUser((String) row[4]);

                rtaAuditLogResponses.add(rtaAuditLogResponse);
            }
            return rtaAuditLogResponses;
        } else {
            return null;
        }
    }
}
