package com.laportal.service.el;

import com.laportal.dto.*;
import com.laportal.model.*;

import java.util.List;

public interface ElService {

    List<ElCaseList> getElCases(TblUser tblUser);

    TblElclaim findElCaseByIdForView(long ElCaseId, TblUser tblUser);

    TblCompanyprofile getCompanyProfile(String companycode);

    List<ElStatusCountList> getAllElStatusCounts();

    List<ElCaseList> getElCasesByStatus(long statusId);

    List<TblEllog> getElCaseLogs(String Elcode);

    List<TblElnote> getElCaseNotes(String Elcode, TblUser tblUser);

    List<TblElmessage> getElCaseMessages(String Elcode);

    boolean isELCaseAllowed(String companycode, String s);

    TblElclaim saveElRequest(TblElclaim tblElclaim);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    TblElnote addTblElNote(TblElnote tblElnote);

    TblElmessage getTblElMessageById(String Elmessagecode);

    TblEmail saveTblEmail(TblEmail tblEmail);

    TblElclaim assignCaseToSolicitor(AssignElCasetoSolicitor assignElCasetoSolicitor, TblUser tblUser);

    TblElclaim performRejectCancelActionOnEl(String ElClaimCode, String toStatus, String reason, TblUser tblUser);

    TblElclaim performActionOnEl(String ElClaimCode, String toStatus, TblUser tblUser);

    TblElclaim findElCaseByIdWithoutUser(long elCode);

    TblCompanyprofile getCompanyProfileAgainstElCode(long Elclaimcode);

    TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType);

    String getElDbColumnValue(String key, long Elclaimcode);

    TblEldocument saveTblElDocument(TblEldocument tblEldocument);

    TblEltask getElTaskByElCodeAndTaskCode(long Elclaimcode, long taskCode);

    int updateTblElTask(TblEltask tblEltask);

    TblTask getTaskAgainstCode(long taskCode);

    List<TblEltask> findTblElTaskByElClaimCode(long Elclaimcode);


    List<ElStatusCountList> getAllElStatusCountsForIntroducers(String companycode);

    List<ElStatusCountList> getAllElStatusCountsForSolicitor(String companycode);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblElclaim tblElclaim,
                      TblUser tblUser);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblElclaim tblElclaim,
                      TblUser tblUser, String attachment);

    TblEmail resendEmail(String elmessagecode);

    List<TblEltask> getELTaskAgainstELCodeAndStatus(String elClaimCode, String status);

    TblElclaim performRevertActionOnEL(String elClaimCode, String toStatus, String reason, TblUser tblUser);

    TblElclaim performActionOnElFromDirectIntro(PerformHotKeyOnRtaRequest performHotKeyOnElRequest, TblUser tblUser);

    List<TblEldocument> addElDocument(List<TblEldocument> tblEldocuments);

    TblEmailTemplate findByEmailType(String emailType);

    TblUser findByUserId(String userCode);

    TblElsolicitor getELSolicitorsOfElClaim(long elClaimCode);

    int updateCurrentTask(long taskCode, long elCode, String current);

    List<TblEltask> getAuthElCaseTasks(String valueOf);

    void deleteElDocument(long eldoccode);

    TblCompanydoc getCompanyDocs(String companycode);

    List<TblEldocument> getAuthElCasedocuments(long elclaimcode);

    TblElclaim updateElCase(TblElclaim tblElclaim);

    TblEldocument addElSingleDocumentSingle(TblEldocument tblEldocument);

    List<TblElnote> getAuthElCaseNotesOfLegalInternal(String elcode, TblUser tblUser);


    TblElclaim findElCaseById(Long elcode, TblUser tblUser);

    List<ElCaseList> getAuthElCasesUserAndCategoryWise(String usercode, String categorycode);

    List<ElCaseList> getAuthElCasesUserAndCategoryWiseAndStatusWise(String usercode, String status, String categorycode);

    List<RtaAuditLogResponse> getElAuditLogs(Long elCode);
}
