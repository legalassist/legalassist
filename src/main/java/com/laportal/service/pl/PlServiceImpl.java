package com.laportal.service.pl;

import com.laportal.Repo.*;
import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class PlServiceImpl extends AbstractApi implements PlService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;

    @Autowired
    private TblPlclaimRepo tblPlclaimRepo;

    @Autowired
    private TblPllogRepo tblPllogRepo;

    @Autowired
    private TblPlmessageRepo tblPlmessageRepo;

    @Autowired
    private TblPlnoteRepo tblPlnoteRepo;

    @Autowired
    private TblPlsolicitorRepo tblPlsolicitorRepo;

    @Autowired
    private TblTaskRepo tblTaskRepo;

    @Autowired
    private TblUsersRepo tblUsersRepo;

    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;

    @Autowired
    private TblEmailRepo tblEmailRepo;

    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;

    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;

    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;

    @Autowired
    private TblPldocumentRepo tblPldocumentRepo;

    @Autowired
    private TblPltaskRepo tblPltaskRepo;

    @Autowired
    private TblEmailTemplateRepo tblEmailTemplateRepo;

    @Autowired
    private TblSolicitorInvoiceDetailRepo tblSolicitorInvoiceDetailRepo;

    @Autowired
    private TblEsignStatusRepo tblEsignStatusRepo;

    @Autowired
    private ViewPlclaimRepo viewPlclaimRepo;

    @Autowired
    private ViewStatuscountRepo viewStatuscountRepo;
@Autowired
private TblRtaflowRepo tblRtaflowRepo;





    @Override
    public TblPlclaim findPlCaseById(long PlCaseId, TblUser tblUser) {
        TblPlclaim tblPlclaim = tblPlclaimRepo.findById(PlCaseId).orElse(null);
        TblRtastatus tblRtastatus = tblRtastatusRepo.findById(tblPlclaim.getStatus().longValue()).orElse(null);
        if (tblPlclaim != null) {

            List<TblPldocument> tblPldocuments = tblPldocumentRepo.findByTblPlclaimPlclaimcode(PlCaseId);
            if (tblPldocuments != null && tblPldocuments.size() > 0) {
                tblPlclaim.setTblPldocuments(tblPldocuments);
            }

            List<RtaActionButton> rtaActionButtons = new ArrayList<>();
            List<RtaActionButton> rtaActionButtonsForLA = new ArrayList<>();
            RtaActionButton rtaActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    tblPlclaim.getStatus().longValue(), "8",
                    tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetailForLA = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    tblPlclaim.getStatus().longValue(), "8",
                    tblCompanyprofile.getTblUsercategory().getCategorycode());

            TblRtaflow tblRtaflow = tblRtaflowRepo.findByTblCompaignCompaigncodeAndTblRtastatusStatuscode("8",tblPlclaim.getStatus().longValue());
            if(tblRtaflow != null) {
                tblPlclaim.setEditFlag(tblRtaflow.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode()) ? tblRtaflow.getEditflag() : "N");
            }

            /// for introducer and solicitor
            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    rtaActionButton = new RtaActionButton();
                    rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                    rtaActionButtons.add(rtaActionButton);
                }

                // for LEGAL INTERNAL action buttons in dropdown

                if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                        rtaActionButton = new RtaActionButton();
                        rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                        rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                        rtaActionButtonsForLA.add(rtaActionButton);
                    }
                    tblPlclaim.setPlActionButtonForLA(rtaActionButtonsForLA);

                    if(tblRtaflowdetailForLA.get(0).getTblRtaflow().getTaskflag().equalsIgnoreCase("Y")) {
                        List<TblPltask> tblPltasks = tblPltaskRepo.findByTblPlclaimPlclaimcode(PlCaseId);
                        tblPlclaim.setTblPltasks(tblPltasks);
                    }
                }


                tblPlclaim.setPlActionButtons(rtaActionButtons);
                tblPlclaim.setStatusDescr(tblRtastatus.getDescr());

                if (tblPlclaim.getIntroducer() != null && tblPlclaim.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblPlclaim.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo
                            .findById(String.valueOf(tblPlclaim.getIntroducer())).orElse(null);

                    TblPlsolicitor tblPlsolicitor = tblPlsolicitorRepo
                            .findByTblPlclaimPlclaimcodeAndStatus(PlCaseId, "Y");
                    if (tblPlsolicitor != null) {
                        tblPlclaim.setSolicitorcompany(tblCompanyprofileRepo
                                .findById(String.valueOf(tblPlsolicitor.getCompanycode())).orElse(null).getName());
                        tblPlclaim.setSolicitorusername(tblUsersRepo
                                .findByUsercode(String.valueOf(tblPlsolicitor.getUsercode())).getLoginid());
                    }
                    tblPlclaim.setAdvisorname(user == null ? null : user.getLoginid());
                    tblPlclaim.setIntroducername(companyprofile == null ? null : companyprofile.getName());
                }

                List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("8", new BigDecimal(tblPlclaim.getPlclaimcode()));
                if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                    for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                        TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                        if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                            tblPlclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblPlclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                            tblPlclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblPlclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        }

                    }
                }


                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("8", new BigDecimal(tblPlclaim.getPlclaimcode()));
                tblPlclaim.setTblEsignStatus(tblEsignStatus);


                return tblPlclaim;

            } else {

                if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                        rtaActionButton = new RtaActionButton();
                        rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                        rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                        rtaActionButtonsForLA.add(rtaActionButton);
                    }
                    tblPlclaim.setPlActionButtons(rtaActionButtonsForLA);

                    if(tblRtaflowdetailForLA.get(0).getTblRtaflow().getTaskflag().equalsIgnoreCase("Y")) {
                        List<TblPltask> tblPltasks = tblPltaskRepo.findByTblPlclaimPlclaimcode(PlCaseId);
                        tblPlclaim.setTblPltasks(tblPltasks);
                    }
                }

                if (tblPlclaim.getIntroducer() != null && tblPlclaim.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblPlclaim.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo
                            .findById(String.valueOf(tblPlclaim.getIntroducer())).orElse(null);

                    TblPlsolicitor tblElsolicitor = tblPlsolicitorRepo
                            .findByTblPlclaimPlclaimcodeAndStatus(PlCaseId, "Y");
                    if (tblElsolicitor != null) {
                        tblPlclaim.setSolicitorcompany(tblCompanyprofileRepo
                                .findById(String.valueOf(tblElsolicitor.getCompanycode())).orElse(null).getName());
                        tblPlclaim.setSolicitorusername(tblUsersRepo
                                .findByUsercode(String.valueOf(tblElsolicitor.getUsercode())).getLoginid());
                    }
                    tblPlclaim.setAdvisorname(user.getLoginid());
                    tblPlclaim.setIntroducername(companyprofile.getName());
                }


                List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("8", new BigDecimal(tblPlclaim.getPlclaimcode()));
                if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                    for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                        TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                        if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                            tblPlclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblPlclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                            tblPlclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            tblPlclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        }

                    }
                }
                tblPlclaim.setStatusDescr(tblRtastatus.getDescr());

                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("8", new BigDecimal(tblPlclaim.getPlclaimcode()));
                tblPlclaim.setTblEsignStatus(tblEsignStatus);




                return tblPlclaim;
            }
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public List<PlStatusCountList> getAllPlStatusCounts(String companycode, String campaignCode) {
        List<PlStatusCountList> plStatusCountLists = new ArrayList<>();
        List<ViewStatuscount> OlStatusCountListObject = viewStatuscountRepo.getStatusCount(Long.valueOf(companycode), Long.valueOf(campaignCode));
        PlStatusCountList plStatusCountList;
        if (OlStatusCountListObject != null && OlStatusCountListObject.size() > 0) {
            for (ViewStatuscount viewStatuscount : OlStatusCountListObject) {
                plStatusCountList = new PlStatusCountList();


                plStatusCountList.setStatusCount(viewStatuscount.getStatuscount());
                plStatusCountList.setStatusName(viewStatuscount.getDescr());
                plStatusCountList.setStatusCode(viewStatuscount.getStatuscode());

                plStatusCountLists.add(plStatusCountList);
            }
        }
        if (plStatusCountLists != null && plStatusCountLists.size() > 0) {
            return plStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<PlCaseList> getPlCasesByStatus(String usercode, String status, String categorycode) {
        List<PlCaseList> PlCaseLists = new ArrayList<>();
        List<ViewPlclaim> PlCasesListForSolicitor = viewPlclaimRepo.getAuthPlCasesUserAndCategoryWiseAndStatusWise(new BigDecimal(usercode), new BigDecimal(categorycode), new BigDecimal(status));
        PlCaseList PlCaseList;
        if (PlCasesListForSolicitor != null && PlCasesListForSolicitor.size() > 0) {
            for (ViewPlclaim viewPlclaim : PlCasesListForSolicitor) {
                PlCaseList = new PlCaseList();

                PlCaseList.setPlClaimCode(viewPlclaim.getPlclaimcode());
                PlCaseList.setCreated(viewPlclaim.getCreatedate());
                PlCaseList.setCode(viewPlclaim.getPlcode());
                PlCaseList.setClient(viewPlclaim.getFirstname() +(viewPlclaim.getMiddlename() != null?viewPlclaim.getMiddlename():"")+viewPlclaim.getLastname());
//                PlCaseList.setTaskDue((String) row[4]);
                PlCaseList.setTaskName(viewPlclaim.getPltaskname());
                PlCaseList.setStatus(viewPlclaim.getStatus());
//                PlCaseList.setEmail();
                PlCaseList.setAddress(viewPlclaim.getPostalcode()+" "+viewPlclaim.getAddress1()+" "+viewPlclaim.getAddress2()+" "+viewPlclaim.getAddress3());
                PlCaseList.setContactNo(viewPlclaim.getMobile());
                PlCaseList.setLastUpdated(viewPlclaim.getUpdatedate());
                PlCaseList.setIntroducer(viewPlclaim.getIntroducerUser());
//                PlCaseList.setLastNote((Date) row[12]);
                PlCaseLists.add(PlCaseList);
            }
        }
        if (PlCaseLists != null && PlCaseLists.size() > 0) {
            return PlCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblPllog> getPlCaseLogs(String Plcode) {
        List<TblPllog> tblPllogs = tblPllogRepo.findByTblPlclaimPlclaimcode(Long.valueOf(Plcode));
        if (tblPllogs != null && tblPllogs.size() > 0) {
            for (TblPllog tblPllog : tblPllogs) {
                TblUser tblUser = tblUsersRepo.findById(tblPllog.getUsercode()).orElse(null);
                tblPllog.setUserName(tblUser.getLoginid());
            }
            return tblPllogs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblPlnote> getPlCaseNotes(String Plcode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblPlnote> tblPlnotes = tblPlnoteRepo.findNotesOnPlbyUserAndCategory(
                Long.valueOf(Plcode), tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (tblPlnotes != null && tblPlnotes.size() > 0) {
            for (TblPlnote tblPlnote : tblPlnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblPlnote.getUsercode()).orElse(null);
                tblPlnote.setUserName(tblUser1.getUsername());
                tblPlnote.setSelf(tblPlnote.getUsercode() == tblUser.getUsercode() ? true : false);
            }
            return tblPlnotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblPlmessage> getPlCaseMessages(String Plcode) {
        List<TblPlmessage> tblPlmessageList = tblPlmessageRepo.findByTblPlclaimPlclaimcode(Long.valueOf(Plcode));
        if (tblPlmessageList != null && tblPlmessageList.size() > 0) {
            for (TblPlmessage tblPlmessage : tblPlmessageList) {
                TblUser tblUser = tblUsersRepo.findById(tblPlmessage.getUsercode()).orElse(null);
                tblPlmessage.setUserName(tblUser.getUsername());
            }
            return tblPlmessageList;
        } else {
            return null;
        }
    }


    @Override
    public TblPlclaim savePlRequest(TblPlclaim tblPlclaim) {
        tblPlclaim = tblPlclaimRepo.save(tblPlclaim);
        TblPllog tblPllog = new TblPllog();

        TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(559))
                .orElse(null);


        tblPllog.setCreatedon(new Date());
        tblPllog.setDescr(tblStatus.getDescr());
        tblPllog.setUsercode(tblPlclaim.getCreateuser().toString());
        tblPllog.setTblPlclaim(tblPlclaim);
        tblPllog = tblPllogRepo.save(tblPllog);
        return tblPlclaim;
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public TblPlnote addTblPlNote(TblPlnote tblPlnote) {
        tblPlnote = tblPlnoteRepo.save(tblPlnote);
        TblPlclaim tblPlclaim = tblPlclaimRepo.findById(tblPlnote.getTblPlclaim().getPlclaimcode()).orElse(null);
        TblUser legalUser = tblUsersRepo.findByUsercode("2");
        // for legal internal

        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("note");
        String legalbody = tblEmailTemplate.getEmailtemplate();

        legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
        legalbody = legalbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
        legalbody = legalbody.replace("[CLIENT_NAME]",
                tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
        legalbody = legalbody.replace("[NOTE]", tblPlnote.getNote());
        legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

        saveEmail(legalUser.getUsername(), legalbody, tblPlclaim.getPlcode() + " | Processing Note",
                tblPlclaim, legalUser);

        if (!tblPlnote.getUsercategorycode().equals("4")) {
            // for introducer
            if (tblPlnote.getUsercategorycode().equals("1")) {
                TblUser introducerUser = tblUsersRepo
                        .findByUsercode(String.valueOf(tblPlclaim.getAdvisor()));
                String introducerBody = tblEmailTemplate.getEmailtemplate();

                introducerBody = introducerBody.replace("[USER_NAME]", introducerUser.getLoginid());
                introducerBody = introducerBody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
                introducerBody = introducerBody.replace("[CLIENT_NAME]",
                        tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
                introducerBody = introducerBody.replace("[NOTE]", tblPlnote.getNote());
                introducerBody = introducerBody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

                saveEmail(introducerUser.getUsername(), introducerBody,
                        tblPlclaim.getPlcode() + " | Processing Note", tblPlclaim,
                        introducerUser);
            } else if (tblPlnote.getUsercategorycode().equals("2")) {

                TblPlsolicitor tblPlsolicitor = tblPlsolicitorRepo
                        .findByTblPlclaimPlclaimcodeAndStatus(tblPlclaim.getPlclaimcode(), "Y");

                if (tblPlsolicitor != null) {
                    TblUser solicitorUser = tblUsersRepo.findById(tblPlsolicitor.getUsercode()).orElse(null);

                    String solicitorBody = tblEmailTemplate.getEmailtemplate();

                    solicitorBody = solicitorBody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorBody = solicitorBody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
                    solicitorBody = solicitorBody.replace("[CLIENT_NAME]",
                            tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());

//                        + " "  + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
//                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
//                        + rtanote.getTblRtaclaim().getLastname());
                    solicitorBody = solicitorBody.replace("[NOTE]", tblPlnote.getNote());
                    solicitorBody = solicitorBody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

                    saveEmail(solicitorUser.getUsername(), solicitorBody,
                            tblPlclaim.getPlcode() + " | Processing Note", tblPlclaim,
                            solicitorUser);

                }
            }

        }

        return tblPlnote;
    }


    @Override
    public TblEmail saveTblEmail(TblEmail tblEmail) {
        return tblEmailRepo.save(tblEmail);
    }

    @Override
    public TblPlclaim performRejectCancelActionOnPl(String PlClaimCode, String toStatus, String reason, TblUser tblUser) {
        TblPlclaim tblPlclaim = tblPlclaimRepo.findById(Long.valueOf(PlClaimCode)).orElse(null);
        TblPlsolicitor tblPlsolicitor = tblPlsolicitorRepo.findByTblPlclaimPlclaimcodeAndStatus(Long.valueOf(PlClaimCode), "Y");
        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status-Reject");
        TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

        if (tblPlsolicitor != null) {
            int update = tblPlsolicitorRepo.updateRemarksReason(reason, "N", Long.parseLong(PlClaimCode));


            TblUser SolicitorUser = tblUsersRepo.findById(tblPlsolicitor.getUsercode()).orElse(null);
            TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblPlsolicitor.getCompanycode())
                    .orElse(null);

            TblPlnote tblPlnote = new TblPlnote();
            // NOte for Legal internal user and NOTE for introducer
            tblPlnote.setTblPlclaim(tblPlclaim);
            tblPlnote.setNote(SolicitorCompany.getName() + " has rejected the case. Reason :" + reason);
            tblPlnote.setUsercategorycode("1");
            tblPlnote.setCreatedon(new Date());
            tblPlnote.setUsercode("1");

            addTblPlNote(tblPlnote);

            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            // Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(tblPlclaim.getAdvisor())).orElse(null);

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
            introducerbody = introducerbody.replace("[SOLICITOR_NAME]", SolicitorCompany.getName());
            introducerbody = introducerbody.replace("[REASON]", reason);
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

            saveEmail(introducerUser.getUsername(), introducerbody, tblPlclaim.getPlcode() + " | " + tblStatus.getDescr(),
                    tblPlclaim, introducerUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
            legalbody = legalbody.replace("[SOLICITOR_NAME]", SolicitorCompany.getName());
            legalbody = legalbody.replace("[REASON]", reason);
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

            saveEmail(legalUser.getUsername(), legalbody, tblPlclaim.getPlcode() + " | " + tblStatus.getDescr(),
                    tblPlclaim, introducerUser);

        } else {

            TblPlnote tblPlnote = new TblPlnote();
            // NOte for Legal internal user and NOTE for introducer
            tblPlnote.setTblPlclaim(tblPlclaim);
            tblPlnote.setNote(tblUser.getLoginid() + " has rejected the case. Reason :" + reason);
            tblPlnote.setUsercategorycode("1");
            tblPlnote.setCreatedon(new Date());
            tblPlnote.setUsercode("1");

            addTblPlNote(tblPlnote);

            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(tblPlclaim.getAdvisor())).orElse(null);

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
            introducerbody = introducerbody.replace("[SOLICITOR_NAME]", tblUser.getLoginid());
            introducerbody = introducerbody.replace("[REASON]", reason);
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

            saveEmail(introducerUser.getUsername(), introducerbody, tblPlclaim.getPlcode() + " | " + tblStatus.getDescr(),
                    tblPlclaim, introducerUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
            legalbody = legalbody.replace("[SOLICITOR_NAME]", tblUser.getLoginid());
            legalbody = legalbody.replace("[REASON]", reason);
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

            saveEmail(legalUser.getUsername(), legalbody, tblPlclaim.getPlcode() + " | " + tblStatus.getDescr(),
                    tblPlclaim, introducerUser);
        }

        TblPllog tblPllog = new TblPllog();


        tblPllog.setCreatedon(new Date());
        tblPllog.setDescr(tblStatus.getDescr());
        tblPllog.setUsercode(tblUser.getUsercode());
        tblPllog.setTblPlclaim(tblPlclaim);
        tblPllog.setOldstatus(tblPlclaim.getStatus().longValue());
        tblPllog.setNewstatus(Long.valueOf(toStatus));

        tblPllog = tblPllogRepo.save(tblPllog);
        int update = tblPlclaimRepo.performRejectCancelAction(reason, toStatus, tblPlclaim.getPlclaimcode(), tblUser.getUsercode());

        return tblPlclaim;
    }

    @Override
    public TblPlclaim performActionOnPl(String PlClaimCode, String toStatus, TblUser tblUser) {
        int update = tblPlclaimRepo.performAction(new BigDecimal(toStatus), Long.valueOf(PlClaimCode), tblUser.getUsercode());
        if (update > 0) {

            TblPlclaim tblPlclaim = tblPlclaimRepo.findById(Long.valueOf(PlClaimCode)).orElse(null);
            TblPllog tblPllog = new TblPllog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblPllog.setCreatedon(new Date());
            tblPllog.setDescr(tblStatus.getDescr());
            tblPllog.setUsercode(tblUser.getUsercode());
            tblPllog.setTblPlclaim(tblPlclaim);
            tblPllog.setOldstatus(tblPlclaim.getStatus().longValue());
            tblPllog.setNewstatus(Long.valueOf(toStatus));

            tblPllog = tblPllogRepo.save(tblPllog);
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status");

            // for introducer
            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblPlclaim.getAdvisor()));
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, introducerUser);

            // for legal internal
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
            legalbody = legalbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, legalUser);

            if (toStatus.equals(559)) {
                // when reverting a case takes to new status remove all solcitors from case if
                // any
                int updateSolicitor = tblPlsolicitorRepo.updateSolicitor("N", Long.parseLong(PlClaimCode));
                TblPlsolicitor tblPlsolicitor = tblPlsolicitorRepo
                        .findByTblPlclaimPlclaimcodeAndStatus(tblPlclaim.getPlclaimcode(), "Y");
                if (tblPlsolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblPlsolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            } else {
                // for normal emails
                TblPlsolicitor tblPlsolicitor = tblPlsolicitorRepo
                        .findByTblPlclaimPlclaimcodeAndStatus(tblPlclaim.getPlclaimcode(), "Y");
                if (tblPlsolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblPlsolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                    rtaclaim.getFirstname() + " "
//                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
//                            + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            }

            // if submmitting a case add clawback date current date + 90 days
            TblPlclaim tblPlclaimForClawBack = tblPlclaimRepo.findById(tblPlclaim.getPlclaimcode()).orElse(null);
            if (toStatus.equals("565")) {
                Date clawbackDate = addDaysToDate(90);
                tblPlclaimRepo.updateClawbackdate(clawbackDate, tblPlclaim.getPlclaimcode());
                tblPlclaimRepo.updateSubmitDate(new Date(), tblPlclaim.getPlclaimcode());
            } else if (toStatus.equals("576")) {
                Date clawbackDate = addDaysToSpecificDate(tblPlclaimForClawBack.getClawbackDate(), 30);
                tblPlclaimRepo.updateClawbackdate(clawbackDate, tblPlclaim.getPlclaimcode());
            } else if (toStatus.equals("578")) {
                Date clawbackDate = addDaysToSpecificDate(tblPlclaimForClawBack.getClawbackDate(), 60);
                tblPlclaimRepo.updateClawbackdate(clawbackDate, tblPlclaim.getPlclaimcode());
            } else if (toStatus.equals("579")) {
                Date clawbackDate = addDaysToSpecificDate(tblPlclaimForClawBack.getClawbackDate(), 90);
                tblPlclaimRepo.updateClawbackdate(clawbackDate, tblPlclaim.getPlclaimcode());
            } else if (toStatus.equals("563")) {
                TblPlnote tblPlnote = new TblPlnote();
                TblPlsolicitor tblPlsolicitor = tblPlsolicitorRepo
                        .findByTblPlclaimPlclaimcodeAndStatus(tblPlclaim.getPlclaimcode(), "Y");
                TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblPlsolicitor.getCompanycode())
                        .orElse(null);
                // NOte for Legal internal user and NOTE for introducer
                tblPlnote.setTblPlclaim(tblPlclaim);
                tblPlnote.setNote("Case Accepted By " + SolicitorCompany.getName());
                tblPlnote.setUsercategorycode("1");
                tblPlnote.setCreatedon(new Date());
                tblPlnote.setUsercode("1");

                addTblPlNote(tblPlnote);
            }
            return tblPlclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblPlclaim findPlCaseByIdWithoutUser(long rtaCode) {
        return tblPlclaimRepo.findById(rtaCode).orElse(null);
    }

    @Override
    public TblCompanyprofile getCompanyProfileAgainstPlCode(long Plclaimcode) {
        return tblCompanyprofileRepo.getCompanyProfileAgainstPlCode(Plclaimcode);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndTblCompaignCompaigncode(companycode,"8");
    }

    @Override
    public String getPlDbColumnValue(String key, long Plclaimcode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from TBL_PLCLAIM  WHERE tenancyclaimcode = " + Plclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT FIRSTNAME || ' ' || NVL(MIDDLENAME,'') || ' ' || NVL(LASTNAME, '') from TBL_PLCLAIM  WHERE tenancyclaimcode = " + Plclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT POSTALCODE || ' ' || ADDRESS1 || ' ' || ADDRESS2  || ' ' || ADDRESS3 || ' ' || CITY || ' ' || REGION from TBL_PLCLAIM  WHERE tenancyclaimcode = " + Plclaimcode;
            query = em.createNativeQuery(sql);
        } else {
            String sql = "SELECT " + key + " from Tbl_tenancyclaim  WHERE TBL_PLCLAIM = " + Plclaimcode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")
        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    @Override
    public int updateTblPlTask(TblPltask tblPltask) {
        return tblPltaskRepo.updatetask(tblPltask.getStatus(), tblPltask.getRemarks(), tblPltask.getPltaskcode(), tblPltask.getTblPlclaim().getPlclaimcode());
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public List<TblPltask> findTblPlTaskByPlClaimCode(long Plclaimcode) {
        return tblPltaskRepo.findByTblPlclaimPlclaimcode(Plclaimcode);
    }

    @Override
    public List<PlStatusCountList> getAllPlStatusCountsForIntroducers() {
        List<PlStatusCountList> PlStatusCountLists = new ArrayList<>();
        List<Object> PlStatusCountListObject = tblPlclaimRepo.getAllPlStatusCountsForIntroducers();
        PlStatusCountList PlStatusCountList;
        if (PlStatusCountListObject != null && PlStatusCountListObject.size() > 0) {
            for (Object record : PlStatusCountListObject) {
                PlStatusCountList = new PlStatusCountList();
                Object[] row = (Object[]) record;

                PlStatusCountList.setStatusCount((BigDecimal) row[0]);
                PlStatusCountList.setStatusName((String) row[1]);
                PlStatusCountList.setStatusCode((BigDecimal) row[2]);

                PlStatusCountLists.add(PlStatusCountList);
            }
        }
        if (PlStatusCountLists != null && PlStatusCountLists.size() > 0) {
            return PlStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<PlStatusCountList> getAllPlStatusCountsForSolicitor() {
        List<PlStatusCountList> PlStatusCountLists = new ArrayList<>();
        List<Object> PlStatusCountListObject = tblPlclaimRepo.getAllPlStatusCountsForSolicitors();
        PlStatusCountList PlStatusCountList;
        if (PlStatusCountListObject != null && PlStatusCountListObject.size() > 0) {
            for (Object record : PlStatusCountListObject) {
                PlStatusCountList = new PlStatusCountList();
                Object[] row = (Object[]) record;

                PlStatusCountList.setStatusCount((BigDecimal) row[0]);
                PlStatusCountList.setStatusName((String) row[1]);
                PlStatusCountList.setStatusCode((BigDecimal) row[2]);

                PlStatusCountLists.add(PlStatusCountList);
            }
        }
        if (PlStatusCountLists != null && PlStatusCountLists.size() > 0) {
            return PlStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public TblPlclaim updatePlClaim(TblPlclaim tblPlclaim) {
        return tblPlclaimRepo.saveAndFlush(tblPlclaim);
    }

    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblPlclaim tblPlclaim,
                             TblUser tblUser) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblPlmessage tblPlmessage = new TblPlmessage();

        tblPlmessage.setTblPlclaim(tblPlclaim);
        tblPlmessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblPlmessage.setCreatedon(new Date());
        tblPlmessage.setMessage(tblEmail.getEmailbody());
        tblPlmessage.setSentto(tblEmail.getEmailaddress());
        tblPlmessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblPlmessage.setEmailcode(tblEmail);

        tblPlmessage = tblPlmessageRepo.save(tblPlmessage);

        return true;
    }

    @Override
    public TblEmail resendEmail(String plmessagecode) {
        TblPlmessage tblElmessage = tblPlmessageRepo.findById(Long.valueOf(plmessagecode)).orElse(null);

        TblEmail tblEmail = tblEmailRepo.findById(tblElmessage.getEmailcode().getEmailcode()).orElse(null);

        tblEmail.setSenflag(new BigDecimal(0));

        return tblEmailRepo.saveAndFlush(tblEmail);
    }

    @Override
    public TblPlclaim performActionOnPlFromDirectIntro(PerformHotKeyOnRtaRequest assignPlCasetoSolicitor, TblUser tblUser) {
        TblPlclaim tblPlclaim = tblPlclaimRepo.findById(Long.valueOf(assignPlCasetoSolicitor.getPlcode()))
                .orElse(null);

        int update = tblPlclaimRepo.performAction(new BigDecimal(assignPlCasetoSolicitor.getToStatus()),
                Long.valueOf(assignPlCasetoSolicitor.getPlcode()), tblUser.getUsercode());
        if (update > 0) {

            TblPllog tblPllog = new TblPllog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(assignPlCasetoSolicitor.getToStatus()))
                    .orElse(null);

            tblPllog.setCreatedon(new Date());
            tblPllog.setDescr(tblStatus.getDescr());
            tblPllog.setUsercode(tblUser.getUsercode());
            tblPllog.setTblPlclaim(tblPlclaim);
            tblPllog.setOldstatus(tblPlclaim.getStatus().longValue());
            tblPllog.setNewstatus(Long.valueOf(assignPlCasetoSolicitor.getToStatus()));

            int updateSolicitor = tblPlsolicitorRepo.updateSolicitor("N", tblPlclaim.getPlclaimcode());

            TblPlsolicitor tblPlsolicitor = new TblPlsolicitor();
            tblPlsolicitor.setCompanycode(assignPlCasetoSolicitor.getSolicitorCode());
            tblPlsolicitor.setCreatedon(new Date());
            tblPlsolicitor.setTblPlclaim(tblPlclaim);
            tblPlsolicitor.setStatus("Y");
            tblPlsolicitor.setUsercode(assignPlCasetoSolicitor.getSolicitorUserCode());

            tblPlsolicitor = tblPlsolicitorRepo.save(tblPlsolicitor);
            tblPllog = tblPllogRepo.save(tblPllog);

            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblPlclaim.getAdvisor()));
            TblUser solicitorUser = tblUsersRepo
                    .findByUsercode(String.valueOf(assignPlCasetoSolicitor.getSolicitorUserCode()));
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status-hotkey");

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                    tblElclaim.getFirstname() + " "
//                            + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                            + tblElclaim.getLastname());
//            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    tblElclaim.getRtanumber() + " | HotKey", tblElclaim, introducerUser);

            // for solicitor
            String solicitorbody = tblEmailTemplate.getEmailtemplate();

            solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
            solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
            solicitorbody = solicitorbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                    tblElclaim.getFirstname() + " "
//                            + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                            + tblElclaim.getLastname());
//            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

            saveEmail(solicitorUser.getUsername(), solicitorbody, tblPlclaim.getPlcode() + " | Processing Note",
                    tblPlclaim, solicitorUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", introducerUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
            legalbody = legalbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                    tblElclaim.getFirstname() + " "
//                            + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                            + tblElclaim.getLastname());

            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.el") + tblPlclaim.getPlclaimcode());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    tblElclaim.getRtanumber() + " | HotKey", tblElclaim, legalUser);


            return tblPlclaim;

        } else {
            return null;
        }
    }

    @Override
    public List<TblPltask> getPlTaskAgainstPLCodeAndStatus(String plCode, String Status) {
        return tblPltaskRepo.findByTblPlclaimPlclaimcodeAndStatus(Long.parseLong(plCode),Status);
    }

    @Override
    public TblPlclaim performRevertActionOnPl(String plCode, String toStatus, String reason, TblUser tblUser) {
        return null;
    }

    @Override
    public List<TblPldocument> getAuthPlCasedocuments(long plclaimcode) {
        return tblPldocumentRepo.findByTblPlclaimPlclaimcode(plclaimcode);
    }

    @Override
    public TblPldocument addPlSingleDocumentSingle(TblPldocument tblPldocument) {
        return tblPldocumentRepo.save(tblPldocument);
    }

    @Override
    public TblUser findByUserId(String userCode) {
        return tblUsersRepo.findByUsercode(userCode);
    }

    @Override
    public TblEmailTemplate findByEmailType(String emailType) {
        return tblEmailTemplateRepo.findByEmailtype(emailType);
    }

    @Override
    public void saveEmail(String emailAddress, String emailBody, String emailSubject, TblPlclaim tblPlclaim, TblUser tblUser, String attachment) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setEmailattachment(attachment);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblPlmessage tblPlmessage = new TblPlmessage();

        tblPlmessage.setTblPlclaim(tblPlclaim);
        tblPlmessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblPlmessage.setCreatedon(new Date());
        tblPlmessage.setMessage(tblEmail.getEmailbody());
        tblPlmessage.setSentto(tblEmail.getEmailaddress());
        tblPlmessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblPlmessage.setEmailcode(tblEmail);

        tblPlmessage = tblPlmessageRepo.save(tblPlmessage);
    }

    @Override
    public TblPlsolicitor getPLSolicitorsOfPlclaim(long plClaimCode) {
        return tblPlsolicitorRepo.findByTblPlclaimPlclaimcodeAndStatus(plClaimCode,"Y");
    }

    @Override
    public List<TblPldocument> addPlDocument(List<TblPldocument> tblPldocuments) {
        return tblPldocumentRepo.saveAll(tblPldocuments);
    }

    @Override
    public int updateCurrentTask(long taskCode, long plCode, String current) {
        tblPltaskRepo.setAllTask("N", plCode);
        return tblPltaskRepo.setCurrentTask(current, taskCode, plCode);
    }

    @Override
    public void deletePlDocument(long doccode) {
        tblPldocumentRepo.deleteById(doccode);
    }

    @Override
    public boolean isPlCaseAllowed(String companycode, String campaignCode) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companycode,
                campaignCode);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<TblPlnote> getAuthPlCaseNotesOfLegalInternal(String plcode, TblUser tblUser) {
        List<TblPlnote> tblPlnotes = tblPlnoteRepo.getAuthPlCaseNotesOfLegalInternal(Long.valueOf(plcode),
                tblUser.getUsercode());
        if (tblPlnotes != null && tblPlnotes.size() > 0) {
            for (TblPlnote tblElnote : tblPlnotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblElnote.getUsercode()).orElse(null);
                tblElnote.setUserName(tblUser1.getLoginid());
                tblElnote.setSelf(tblElnote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
            }

            return tblPlnotes;
        } else {
            return null;
        }
    }

    @Override
    public List<PlCaseList> getAuthPlCasesUserAndCategoryWise(String usercode, String companyCode) {
        List<PlCaseList> PlCaseLists = new ArrayList<>();
        List<ViewPlclaim> PlCasesListForSolicitor = viewPlclaimRepo.getAuthPlCasesUserAndCategoryWise(new BigDecimal(usercode),new BigDecimal(companyCode));
        PlCaseList PlCaseList;
        if (PlCasesListForSolicitor != null && PlCasesListForSolicitor.size() > 0) {
            for (ViewPlclaim viewPlclaim : PlCasesListForSolicitor) {
                PlCaseList = new PlCaseList();

                PlCaseList.setPlClaimCode(viewPlclaim.getPlclaimcode());
                PlCaseList.setCreated(viewPlclaim.getCreatedate());
                PlCaseList.setCode(viewPlclaim.getPlcode());
                PlCaseList.setClient(viewPlclaim.getFirstname() +(viewPlclaim.getMiddlename() != null?viewPlclaim.getMiddlename():"")+viewPlclaim.getLastname());
//                PlCaseList.setTaskDue((String) row[4]);
                PlCaseList.setTaskName(viewPlclaim.getPltaskname());
                PlCaseList.setStatus(viewPlclaim.getStatus());
//                PlCaseList.setEmail();
                PlCaseList.setAddress(viewPlclaim.getPostalcode()+" "+viewPlclaim.getAddress1()+" "+viewPlclaim.getAddress2()+" "+viewPlclaim.getAddress3());
                PlCaseList.setContactNo(viewPlclaim.getMobile());
                PlCaseList.setLastUpdated(viewPlclaim.getUpdatedate());
                PlCaseList.setIntroducer(viewPlclaim.getIntroducerUser());
//                PlCaseList.setLastNote((Date) row[12]);
                PlCaseLists.add(PlCaseList);
            }
        }
        if (PlCaseLists != null && PlCaseLists.size() > 0) {
            return PlCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<RtaAuditLogResponse> getPlAuditLogs(Long plCode) {
        List<RtaAuditLogResponse> rtaAuditLogResponses = new ArrayList<>();
        RtaAuditLogResponse rtaAuditLogResponse = null;
        List<Object> auditlogsObject = tblPlclaimRepo.getPlAuditLogs(plCode);
        RtaStatusCountList PlStatusCountList;
        if (auditlogsObject != null && auditlogsObject.size() > 0) {
            for (Object record : auditlogsObject) {
                rtaAuditLogResponse = new RtaAuditLogResponse();
                Object[] row = (Object[]) record;

                rtaAuditLogResponse.setFieldName((String) row[0]);
                rtaAuditLogResponse.setOldValue((String) row[1]);
                rtaAuditLogResponse.setNewValue((String) row[2]);
                rtaAuditLogResponse.setAuditDate((Date) row[3]);
                rtaAuditLogResponse.setLoggedUser((String) row[4]);

                rtaAuditLogResponses.add(rtaAuditLogResponse);
            }
            return rtaAuditLogResponses;
        } else {
            return null;
        }
    }
}
