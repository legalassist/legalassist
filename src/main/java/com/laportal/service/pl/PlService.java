package com.laportal.service.pl;

import com.laportal.dto.PerformHotKeyOnRtaRequest;
import com.laportal.dto.PlCaseList;
import com.laportal.dto.PlStatusCountList;
import com.laportal.dto.RtaAuditLogResponse;
import com.laportal.model.*;

import java.util.List;

public interface PlService {


    TblPlclaim findPlCaseById(long PlCaseId, TblUser tblUser);

    TblCompanyprofile getCompanyProfile(String companycode);

    List<PlStatusCountList> getAllPlStatusCounts(String companycode, String campaignCode);

    List<PlCaseList> getPlCasesByStatus(String usercode, String status, String categorycode);

    List<TblPllog> getPlCaseLogs(String Plcode);

    List<TblPlnote> getPlCaseNotes(String Plcode, TblUser tblUser);

    List<TblPlmessage> getPlCaseMessages(String Plcode);

    TblPlclaim savePlRequest(TblPlclaim tblPlclaim);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    TblPlnote addTblPlNote(TblPlnote tblPlnote);

    TblEmail saveTblEmail(TblEmail tblEmail);

    TblPlclaim performRejectCancelActionOnPl(String PlClaimCode, String toStatus, String reason, TblUser tblUser);

    TblPlclaim performActionOnPl(String PlClaimCode, String toStatus, TblUser tblUser);

    TblPlclaim findPlCaseByIdWithoutUser(long rtaCode);

    TblCompanyprofile getCompanyProfileAgainstPlCode(long Plclaimcode);

    TblCompanydoc getCompanyDocs(String companycode);

    String getPlDbColumnValue(String key, long Plclaimcode);

    int updateTblPlTask(TblPltask tblPltask);

    TblTask getTaskAgainstCode(long taskCode);

    List<TblPltask> findTblPlTaskByPlClaimCode(long Plclaimcode);

    List<PlStatusCountList> getAllPlStatusCountsForIntroducers();

    List<PlStatusCountList> getAllPlStatusCountsForSolicitor();

    TblPlclaim updatePlClaim(TblPlclaim tblPlclaim);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblPlclaim tblPlclaim,
                      TblUser tblUser);

    TblEmail resendEmail(String plmessagecode);

    TblPlclaim performActionOnPlFromDirectIntro(PerformHotKeyOnRtaRequest assignPlCasetoSolicitor, TblUser tblUser);

    List<TblPltask> getPlTaskAgainstPLCodeAndStatus(String plCode, String Status);

    TblPlclaim performRevertActionOnPl(String plCode, String toStatus, String reason, TblUser tblUser);

    List<TblPldocument> getAuthPlCasedocuments(long plclaimcode);

    TblPldocument addPlSingleDocumentSingle(TblPldocument tblPldocument);

    TblUser findByUserId(String userCode);

    TblEmailTemplate findByEmailType(String emailType);

    void saveEmail(String emailAddress, String emailBody, String emailSubject, TblPlclaim tblPlclaim, TblUser tblUser, String attachment);

    TblPlsolicitor getPLSolicitorsOfPlclaim(long plClaimCode);

    List<TblPldocument> addPlDocument(List<TblPldocument> tblPldocuments);

    int updateCurrentTask(long taskCode, long plCode, String current);

    void deletePlDocument(long doccode);

    boolean isPlCaseAllowed(String companycode, String campaignCode);

    List<TblPlnote> getAuthPlCaseNotesOfLegalInternal(String plcode, TblUser tblUser);

    List<PlCaseList> getAuthPlCasesUserAndCategoryWise(String usercode, String companyCode);

    List<RtaAuditLogResponse> getPlAuditLogs(Long plCode);
}
