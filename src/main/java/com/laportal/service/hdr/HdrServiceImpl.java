package com.laportal.service.hdr;

import com.laportal.Repo.*;
import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class HdrServiceImpl extends AbstractApi implements HdrService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;

    @Autowired
    private TblHdrclaimRepo tblHdrclaimRepo;

    @Autowired
    private TblHdrclaimantRepo tblHdrclaimantRepo;

    @Autowired
    private TblHdraffectedperRepo tblHdraffectedperRepo;

    @Autowired
    private TblHdraffectedroomRepo tblHdraffectedroomRepo;

    @Autowired
    private TblHdrjointtenancyRepo tblHdrjointtenancyRepo;

    @Autowired
    private TblHdrtenancyRepo tblHdrtenancyRepo;

    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;

    @Autowired
    private TblHdrnoteRepo tblHdrnoteRepo;

    @Autowired
    private TblHdrlogRepo tblHdrlogRepo;

    @Autowired
    private TblHdrmessageRepo tblHdrmessageRepo;

    @Autowired
    private TblUsersRepo tblUsersRepo;

    @Autowired
    private TblHdrdocumentRepo tblHdrdocumentRepo;

    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;

    @Autowired
    private TblHdrtaskRepo tblHdrtaskRepo;

    @Autowired
    private TblEmailRepo tblEmailRepo;

    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;

    @Autowired
    private TblHdrsolicitorRepo tblHdrsolicitorRepo;

    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;

    @Autowired
    private TblTaskRepo tblTaskRepo;

    @Autowired
    private TblHirebusinessRepo tblHirebusinessRepo;

    @Autowired
    private TblEmailTemplateRepo tblEmailTemplateRepo;
    @Autowired
    private ViewHdrcasereportRepo viewHdrcasereportRepo;
    @Autowired
    private TblSolicitorInvoiceDetailRepo tblSolicitorInvoiceDetailRepo;
    @Autowired
    private TblEsignStatusRepo tblEsignStatusRepo;

    @Autowired
    private TblRtaflowRepo tblRtaflowRepo;

    @Autowired
    private TblHdrclaimcopyRepo tblHdrclaimcopyRepo;


    @Override
    public boolean isHdrCaseAllowed(String companycode, String s) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companycode,
                s);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TblHdrclaim saveHdrRequest(TblHdrclaim tblHdrclaim, TblHdrclaimant tblHdrclaimant, TblHdrjointtenancy tblHdrjointtenancy, TblHdrtenancy tblHdrtenancy, List<TblHdraffectedper> tblHdraffectedperList, List<TblHdraffectedroom> tblHdraffectedroomList) {
        tblHdrclaim = tblHdrclaimRepo.save(tblHdrclaim);

        tblHdrclaimant.setTblHdrclaim(tblHdrclaim);
        tblHdrclaimant = tblHdrclaimantRepo.save(tblHdrclaimant);

        if (tblHdrjointtenancy != null) {
            tblHdrjointtenancy.setTblHdrclaim(tblHdrclaim);
            tblHdrjointtenancy = tblHdrjointtenancyRepo.save(tblHdrjointtenancy);
        }

        tblHdrtenancy.setTblHdrclaim(tblHdrclaim);
        tblHdrtenancy = tblHdrtenancyRepo.save(tblHdrtenancy);

        if (tblHdraffectedperList != null && tblHdraffectedperList.size() > 0) {
            for (TblHdraffectedper tblHdraffectedper : tblHdraffectedperList) {
                tblHdraffectedper.setTblHdrclaim(tblHdrclaim);
            }
            tblHdraffectedperList = tblHdraffectedperRepo.saveAll(tblHdraffectedperList);
        }


        if (tblHdraffectedroomList != null && tblHdraffectedroomList.size() > 0) {
            for (TblHdraffectedroom tblHdraffectedroom : tblHdraffectedroomList) {
                tblHdraffectedroom.setTblHdrclaim(tblHdrclaim);
            }
            tblHdraffectedroomList = tblHdraffectedroomRepo.saveAll(tblHdraffectedroomList);
        }


        return tblHdrclaim;
    }

    @Override
    public TblHdrclaim findHdrClaimByCode(String hdrClaimCode) {
        return tblHdrclaimRepo.findById(Long.valueOf(hdrClaimCode)).orElse(null);
    }

    @Override
    public List<HdrCasesList> getHdrCases(TblUser tblUser) {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getHdrCasesListUserWise(tblUser.getUsercode());
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);
                hdrCasesList.setEmail((String) row[7]);
                hdrCasesList.setAddress((String) row[8]);
                hdrCasesList.setContactNo((String) row[9]);
                hdrCasesList.setLastUpdated((Date) row[10]);
                hdrCasesList.setIntroducer((String) row[11]);
                hdrCasesList.setLastNote((Date) row[12]);
                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }

    }

    @Override
    public TblHdrclaim findHdrCaseById(long hdrCaseId, TblUser tblUser) {
        TblHdrclaim tblHdrclaim = tblHdrclaimRepo.findById(hdrCaseId).orElse(null);
        if (tblHdrclaim != null) {
            TblRtastatus tblRtastatus = tblRtastatusRepo.findById(Long.valueOf(tblHdrclaim.getStatuscode().toString())).orElse(null);

            tblHdrclaim.setStatusDescr(tblRtastatus.getDescr());

            List<HdrActionButton> rtaActionButtons = new ArrayList<>();
            List<HdrActionButton> rtaActionButtonsForLA = new ArrayList<>();
            HdrActionButton hdrActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);

            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    Long.valueOf(tblHdrclaim.getStatuscode().toString()), "3", tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetailForLA = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    Long.valueOf(tblHdrclaim.getStatuscode().toString()), "3", tblCompanyprofile.getTblUsercategory().getCategorycode());

            TblRtaflow tblRtaflow = tblRtaflowRepo.findByTblCompaignCompaigncodeAndTblRtastatusStatuscode("3", Long.valueOf(tblHdrclaim.getStatuscode().toString()));
            if(tblRtaflow!= null) {
                tblHdrclaim.setEditFlag(tblRtaflow.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode()) ? tblRtaflow.getEditflag() : "N");
            }
            TblHdrsolicitor tblHdrsolicitor = tblHdrsolicitorRepo.findByTblHdrclaimHdrclaimcodeAndStatus(hdrCaseId, "Y");
            if (tblHdrsolicitor != null) {
                List<TblHdrsolicitor> tblHdrsolicitors = new ArrayList<>();
                tblHdrsolicitors.add(tblHdrsolicitor);
                tblHdrclaim.setTblHdrsolicitors(tblHdrsolicitors);
            } else {
                tblHdrclaim.setTblHdrsolicitors(null);
            }
            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    hdrActionButton = new HdrActionButton();
                    hdrActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hdrActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hdrActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hdrActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    hdrActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());
                    hdrActionButton.setAcceptDialog(tblRtaflowdetail.getCaseacceptdialog());

                    rtaActionButtons.add(hdrActionButton);
                }
                tblHdrclaim.setHdrActionButton(rtaActionButtons);
            }
            if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                    hdrActionButton = new HdrActionButton();
                    hdrActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hdrActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hdrActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hdrActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    hdrActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());
                    hdrActionButton.setAcceptDialog(tblRtaflowdetail.getCaseacceptdialog());

                    rtaActionButtonsForLA.add(hdrActionButton);
                }
                tblHdrclaim.setHdrActionButtonForLA(rtaActionButtonsForLA);

            }

            for (TblHdrlog tblHdrLog : tblHdrclaim.getTblHdrlogs()) {
                TblUser tblUser1 = tblUsersRepo.findById(tblHdrLog.getUsercode()).orElse(null);
                tblHdrLog.setUserName(tblUser1.getUsername());
            }

            TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblHdrclaim.getAdvisor()));
            TblCompanyprofile companyprofile = tblCompanyprofileRepo
                    .findById(String.valueOf(tblHdrclaim.getIntroducer())).orElse(null);

            tblHdrclaim.setAdvisorname(user.getLoginid());
            tblHdrclaim.setIntroducername(companyprofile.getName());


            List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("3", BigDecimal.valueOf(tblHdrclaim.getHdrclaimcode()));
            if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                    companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                    if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                        tblHdrclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                        tblHdrclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                    } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                        tblHdrclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                        tblHdrclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                    }

                }
            }
            TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("3", new BigDecimal(hdrCaseId));
            tblHdrclaim.setTblEsignStatus(tblEsignStatus);

            ///// copy logs

            List<HdrCopyLog> hdrCopyLogs = new ArrayList<>();
            List<Object> hdrCasesListObject = tblHdrclaimRepo.getHdrCopyLogs(tblHdrclaim.getHdrclaimcode());
            HdrCopyLog hdrCopyLog;
            if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
                for (Object record : hdrCasesListObject) {
                    hdrCopyLog = new HdrCopyLog();
                    Object[] row = (Object[]) record;

                    hdrCopyLog.setHdrcode(((BigDecimal) row[0]).toString());
                    hdrCopyLog.setStatusDescr((String) row[1]);
                    hdrCopyLog.setCreateDate((Date) row[2]);
                    hdrCopyLog.setSolicitorName((String) row[3]);
                    hdrCopyLog.setHdrClaimCode((String) row[4]);


                    hdrCopyLogs.add(hdrCopyLog);
                }
                tblHdrclaim.setHdrCopyLogs(hdrCopyLogs);
            }


            /// copy logs

            return tblHdrclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public List<HdrStatusCountList> getAllHdrStatusCounts() {
        List<HdrStatusCountList> HdrStatusCountListS = new ArrayList<>();
        List<Object> HdrStatusCountListObject = tblHdrclaimRepo.getAllHdrStatusCounts();
        HdrStatusCountList hdrStatusCountList;
        if (HdrStatusCountListObject != null && HdrStatusCountListObject.size() > 0) {
            for (Object record : HdrStatusCountListObject) {
                hdrStatusCountList = new HdrStatusCountList();
                Object[] row = (Object[]) record;

                hdrStatusCountList.setStatusCount((BigDecimal) row[0]);
                hdrStatusCountList.setStatusName((String) row[1]);
                hdrStatusCountList.setStatusCode((BigDecimal) row[2]);

                HdrStatusCountListS.add(hdrStatusCountList);
            }
        }
        if (HdrStatusCountListS != null && HdrStatusCountListS.size() > 0) {
            return HdrStatusCountListS;
        } else {
            return null;
        }
    }

    @Override
    public List<HdrCasesList> getHdrCasesByStatus(long statusId) {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getHdrCasesListStatusWise(statusId);
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);

                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }
    }

    @Override
    public TblHdrnote addTblHdrNote(TblHdrnote tblHdrnote) {
        tblHdrnote = tblHdrnoteRepo.save(tblHdrnote);
        TblHdrclaim tblHdrclaim = tblHdrclaimRepo.findById(tblHdrnote.getTblHdrclaim().getHdrclaimcode()).orElse(null);
        TblHdrclaimant tblHdrclaimant = tblHdrclaimantRepo.findByTblHdrclaimHdrclaimcode(tblHdrnote.getTblHdrclaim().getHdrclaimcode());
        TblUser legalUser = tblUsersRepo.findByUsercode("3");
        // for legal internal

        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("note");
        String legalbody = tblEmailTemplate.getEmailtemplate();

        legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
        legalbody = legalbody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
        legalbody = legalbody.replace("[CLIENT_NAME]", tblHdrclaimant.getCfname() + " " + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ") + tblHdrclaimant.getCsname());
        legalbody = legalbody.replace("[NOTE]", tblHdrnote.getNote());
        legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());

        saveEmail(legalUser.getUsername(), legalbody,
                tblHdrclaim.getClaimcode() + " | Processing Note", tblHdrclaim, legalUser);

        if (!tblHdrnote.getUsercategorycode().equals("4")) {
            // for introducer
            if (tblHdrnote.getUsercategorycode().equals("1")) {
                TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblHdrclaim.getAdvisor()));
                String introducerBody = tblEmailTemplate.getEmailtemplate();

                introducerBody = introducerBody.replace("[USER_NAME]", legalUser.getLoginid());
                introducerBody = introducerBody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
                introducerBody = introducerBody.replace("[CLIENT_NAME]", tblHdrclaimant.getCfname() + " " + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ") + tblHdrclaimant.getCsname());
                introducerBody = introducerBody.replace("[NOTE]", tblHdrnote.getNote());
                introducerBody = introducerBody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());

                saveEmail(introducerUser.getUsername(), introducerBody,
                        tblHdrclaim.getClaimcode() + " | Processing Note", tblHdrclaim, introducerUser);
            } else if (tblHdrnote.getUsercategorycode().equals("2")) {

                TblHdrsolicitor tblHdrsolicitor = tblHdrsolicitorRepo.findByTblHdrclaimHdrclaimcodeAndStatus(tblHdrclaim.getHdrclaimcode(), "Y");

                if (tblHdrsolicitor != null) {
                    TblUser solicitorUser = tblUsersRepo.findById(tblHdrsolicitor.getUsercode()).orElse(null);

                    String solicitorBody = tblEmailTemplate.getEmailtemplate();

                    solicitorBody = solicitorBody.replace("[USER_NAME]", legalUser.getLoginid());
                    solicitorBody = solicitorBody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
                    solicitorBody = solicitorBody.replace("[CLIENT_NAME]", tblHdrclaimant.getCfname() + " " + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ") + tblHdrclaimant.getCsname());
                    solicitorBody = solicitorBody.replace("[NOTE]", tblHdrnote.getNote());
                    solicitorBody = solicitorBody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());


                    saveEmail(solicitorUser.getUsername(), solicitorBody,
                            tblHdrclaim.getClaimcode() + " | Processing Note", tblHdrclaim, solicitorUser);

                }
            }

        }
        return tblHdrnote;


    }

    @Override
    public List<TblHdrlog> getHdrCaseLogs(String hdrcode) {
        List<TblHdrlog> tblHdrlogs = tblHdrlogRepo.findByTblHdrclaimHdrclaimcodeOrderByHdrlogcodeDesc(Long.valueOf(hdrcode));
        if (tblHdrlogs != null && tblHdrlogs.size() > 0) {
            for (TblHdrlog tblHdrLog : tblHdrlogs) {
                TblUser tblUser = tblUsersRepo.findById(tblHdrLog.getUsercode()).orElse(null);
                tblHdrLog.setUserName(tblUser.getUsername());
            }
            return tblHdrlogs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblHdrnote> getHdrCaseNotes(String hdrcode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblHdrnote> tblHdrNotes = tblHdrnoteRepo.findNotesOnHdrbyUserAndCategory(
                Long.valueOf(hdrcode), tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (tblHdrNotes != null && tblHdrNotes.size() > 0) {
            for (TblHdrnote tblHdrnote : tblHdrNotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblHdrnote.getUsercode()).orElse(null);
                tblHdrnote.setUserName(tblUser1.getUsername());
                tblHdrnote.setSelf(tblHdrnote.getUsercode() == tblUser.getUsercode() ? true : false);
            }
            return tblHdrNotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblHdrmessage> getHdrCaseMessages(String hdrcode) {
        List<TblHdrmessage> tblHdrMessages = tblHdrmessageRepo.findByTblHdrclaimHdrclaimcode(Long.valueOf(hdrcode));
        if (tblHdrMessages != null && tblHdrMessages.size() > 0) {
            for (TblHdrmessage tblHdrmessage : tblHdrMessages) {
                if (tblHdrmessage.getUsercode() != null) {
                    TblUser tblUser = tblUsersRepo.findById(tblHdrmessage.getUsercode()).orElse(null);

                    tblHdrmessage.setUserName(tblUser.getUsername());
                }
            }
            return tblHdrMessages;
        } else {
            return null;
        }
    }

    @Override
    public List<TblHdrdocument> saveTblHdrDocuments(List<TblHdrdocument> saveTblHdrDocuments) {
        return tblHdrdocumentRepo.saveAll(saveTblHdrDocuments);
    }

    @Override
    public TblCompanyprofile getCompanyProfileAgainstHdrCode(long hdrclaimcode) {
        return tblCompanyprofileRepo.getCompanyProfileAgainstHireCode(hdrclaimcode);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companyCode, String jointenency) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndTblCompaignCompaigncodeAndJointtenency(companyCode, "3", jointenency);
    }

    @Override
    public String getHdrDbColumnValue(String key, long hdrclaimcode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from TBL_HDRCLAIMANT  WHERE HDR_CLAIMCODE = " + hdrclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT C_FNAME || ' ' || NVL(C_MNAME,'') || ' ' || NVL(C_SNAME, '') from TBL_HDRCLAIMANT  WHERE HDR_CLAIMCODE = " + hdrclaimcode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT C_POSTCODE || ' ' || C_ADDRESS1 || ' ' || C_ADDRESS2  || ' ' || C_ADDRESS3 || ' ' || C_CITY || ' ' || C_REGION from TBL_HDRCLAIMANT  WHERE HDR_CLAIMCODE = " + hdrclaimcode;
            query = em.createNativeQuery(sql);
        } else {
//            String sql = "SELECT " + key + " from TBL_HDRCLAIMANT  WHERE HDR_CLAIMCODE = " + hdrclaimcode;
            String sql = "SELECT " + key + " \n" +
                    "FROM TBL_HDRCLAIM HC\n" +
                    "INNER JOIN TBL_HDRCLAIMANT CMT ON HC.HDRCLAIMCODE = CMT.HDR_CLAIMCODE\n" +
                    "LEFT JOIN TBL_HDRJOINTTENANCY HJT ON HC.HDRCLAIMCODE = HJT.HDR_CLAIMCODE\n" +
                    "WHERE HC.HDRCLAIMCODE = " + hdrclaimcode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")
        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    @Override
    public TblHdrdocument saveTblHdrDocument(TblHdrdocument tblHdrdocument) {
        return tblHdrdocumentRepo.save(tblHdrdocument);
    }

    @Override
    public TblHdrclaim updateHdrCase(TblHdrclaim tblHdrclaim) {
        return tblHdrclaimRepo.save(tblHdrclaim);
    }

    @Override
    public TblHdrtask updateTblHdrTask(TblHdrtask tblHdrtask) {
        TblHdrtask update = tblHdrtaskRepo.saveAndFlush(tblHdrtask);
        return update;
    }

    @Override
    public TblHdrmessage getTblHdrMessageById(String hdrmessagecode) {
        return tblHdrmessageRepo.findById(Long.valueOf(hdrmessagecode)).orElse(null);
    }

    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblHdrclaim tblHdrclaim,
                             TblUser tblUser) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblHdrmessage tblHdrmessage = new TblHdrmessage();

        tblHdrmessage.setTblHdrclaim(tblHdrclaim);
        tblHdrmessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblHdrmessage.setCreatedon(new Date());
        tblHdrmessage.setMessage(tblEmail.getEmailbody());
        tblHdrmessage.setSentto(tblEmail.getEmailaddress());
        tblHdrmessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblHdrmessage.setEmailcode(tblEmail);

        tblHdrmessage = tblHdrmessageRepo.save(tblHdrmessage);

        return true;
    }

    @Override
    public TblHdrdocument findHdrDocumentById(long hdrCaseId) {
        return tblHdrdocumentRepo.findById(hdrCaseId).orElse(null);
    }

    @Override
    public TblHdrclaim assignCaseToSolicitor(AssignHdrCasetoSolicitorOnHdr assignHdrCasetoSolicitorOnHdr, TblUser tblUser) {
        int update = tblHdrclaimRepo.performAction(new BigDecimal("41"), Long.valueOf(assignHdrCasetoSolicitorOnHdr.getHdrClaimCode()));
        if (update > 0) {

            TblHdrclaim tblHdrclaim = tblHdrclaimRepo.findById(Long.valueOf(assignHdrCasetoSolicitorOnHdr.getHdrClaimCode())).orElse(null);
            TblHdrclaimant tblHdrclaimant = tblHdrclaimantRepo.findByTblHdrclaimHdrclaimcode(tblHdrclaim.getHdrclaimcode());
            TblHdrlog tblHdrlog = new TblHdrlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(41))
                    .orElse(null);

            tblHdrlog.setCreatedon(new java.util.Date());
            tblHdrlog.setDescr(tblStatus.getDescr());
            tblHdrlog.setUsercode(tblUser.getUsercode());
            tblHdrlog.setTblHdrclaim(tblHdrclaim);
            tblHdrlog.setOldStatus("39");
            tblHdrlog.setNewStatus("41");

            int updateSolicitor = tblHdrsolicitorRepo.updateSolicitor("N", Long.valueOf(assignHdrCasetoSolicitorOnHdr.getHdrClaimCode()));

            TblHdrsolicitor tblHdrsolicitor = new TblHdrsolicitor();
            tblHdrsolicitor.setCompanycode(assignHdrCasetoSolicitorOnHdr.getSolicitorUserCode());
            tblHdrsolicitor.setCreatedon(new java.util.Date());
            tblHdrsolicitor.setTblHdrclaim(tblHdrclaim);
            tblHdrsolicitor.setStatus("Y");
            tblHdrsolicitor.setUsercode(assignHdrCasetoSolicitorOnHdr.getSolicitorCode());

            tblHdrsolicitor = tblHdrsolicitorRepo.save(tblHdrsolicitor);
            tblHdrlog = tblHdrlogRepo.save(tblHdrlog);

            TblHdrclaim hdrclaim = tblHdrclaimRepo.findById(tblHdrclaim.getHdrclaimcode()).orElse(null);

            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("hdr-status-hotkey");

            String solicitorbody = tblEmailTemplate.getEmailtemplate();
            TblUser solicitorUser = tblUsersRepo.findByUsercode(assignHdrCasetoSolicitorOnHdr.getSolicitorCode());

            solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
            solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
            solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                    tblHdrclaimant.getCfname() + " "
                            + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                            + tblHdrclaimant.getCsname());
            solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());

            saveEmail(solicitorUser.getUsername(), solicitorbody, tblHdrclaim.getClaimcode() + " | Processing Note",
                    tblHdrclaim, solicitorUser);


            return tblHdrclaimRepo.findById(Long.valueOf(assignHdrCasetoSolicitorOnHdr.getHdrClaimCode())).orElse(null);

        } else {
            return null;
        }
    }

    @Override
    public TblHdrclaim findHdrCaseByIdwithoutuser(long rtaCode) {
        return tblHdrclaimRepo.findById(rtaCode).orElse(null);
    }

    @Override
    public TblTask findTaskById(long hdrtaskcode) {
        return tblTaskRepo.findById(hdrtaskcode).orElse(null);
    }

    @Override
    public List<TblHdrtask> getHdrTaskAgainstHdrCodeAndStatus(String hdrClaimCode, String status) {
        return tblHdrtaskRepo.findByTaskStatusAndMandotry(Long.valueOf(hdrClaimCode), status);
    }

    @Override
    public TblHdrclaim performRejectCancelActionOnHdr(String hdrClaimCode, String toStatus, String reason, TblUser tblUser) {
        TblHdrsolicitor tblHdrsolicitor = tblHdrsolicitorRepo.findByTblHdrclaimHdrclaimcodeAndStatus(Long.valueOf(hdrClaimCode), "Y");
        TblCompanyprofile solicitorCompany = tblCompanyprofileRepo.findById(tblHdrsolicitor.getCompanycode()).orElse(null);
        if (tblHdrsolicitor != null) {
            TblHdrclaim tblHdrclaim = tblHdrclaimRepo.findById(Long.valueOf(hdrClaimCode)).orElse(null);
            int update = tblHdrsolicitorRepo.updateRemarksReason(reason, "N", Long.valueOf(hdrClaimCode));

            TblHdrclaimant tblHdrclaimant = tblHdrclaimantRepo.findByTblHdrclaimHdrclaimcode(tblHdrclaim.getHdrclaimcode());

            TblHdrlog tblHdrlog = new TblHdrlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblHdrlog.setCreatedon(new Date());
            tblHdrlog.setDescr(tblStatus.getDescr());
            tblHdrlog.setUsercode(tblUser.getUsercode());
            tblHdrlog.setTblHdrclaim(tblHdrclaim);
            tblHdrlog.setOldStatus(tblHdrclaim.getStatuscode().toString());
            tblHdrlog.setNewStatus(toStatus);

            tblHdrlog = tblHdrlogRepo.save(tblHdrlog);

            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("hdr-status");

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblHdrclaim.getCreateuser().toString()).orElse(null);

            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]",
                    tblHdrclaimant.getCfname() + " "
                            + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                            + tblHdrclaimant.getCsname());
            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, introducerUser);


            //Send Email to Solicitor

            if (tblHdrsolicitor != null) {
                TblUser solicitorUser = tblUsersRepo.findById(tblHdrsolicitor.getUsercode()).orElse(null);

                String solicitorbody = tblEmailTemplate.getEmailtemplate();

                solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
                solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                        tblHdrclaimant.getCfname() + " "
                                + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                                + tblHdrclaimant.getCsname());
                solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
            }

            //Send Email to Legal Assist
            TblUser legalUser = tblUsersRepo.findByUsercode("3");
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
            legalbody = legalbody.replace("[CLIENT_NAME]",
                    tblHdrclaimant.getCfname() + " "
                            + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                            + tblHdrclaimant.getCsname());
            legalbody = legalbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());

            saveEmail(legalUser.getUsername(), legalbody,
                    tblHdrclaim.getClaimcode() + " | Processing Note", tblHdrclaim, legalUser);

            TblHdrnote tblHdrnote = new TblHdrnote();
            // NOte for Legal internal user and NOTE for introducer
            tblHdrnote.setTblHdrclaim(tblHdrclaim);
            tblHdrnote.setNote(solicitorCompany.getName()+" has rejected the case. Reason :" + reason);
            tblHdrnote.setUsercategorycode("1");
            tblHdrnote.setCreatedon(new Date());
            tblHdrnote.setUsercode("1");

            addTblHdrNote(tblHdrnote);

            return tblHdrclaim;

        } else {
            TblHdrclaim tblHdrclaimOld = tblHdrclaimRepo.findById(Long.valueOf(hdrClaimCode)).orElse(null);
            TblHdrclaimant tblHdrclaimant = tblHdrclaimantRepo.findByTblHdrclaimHdrclaimcode(tblHdrclaimOld.getHdrclaimcode());

            int update = tblHdrclaimRepo.performRejectCancelAction(reason, new BigDecimal(toStatus), Long.valueOf(hdrClaimCode));

            TblHdrclaim tblHdrclaim = tblHdrclaimRepo.findById(Long.valueOf(hdrClaimCode)).orElse(null);
            TblHdrlog tblHdrlog = new TblHdrlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblHdrlog.setCreatedon(new Date());
            tblHdrlog.setDescr(tblStatus.getDescr());
            tblHdrlog.setUsercode(tblUser.getUsercode());
            tblHdrlog.setTblHdrclaim(tblHdrclaim);
            tblHdrlog.setOldStatus(tblHdrclaim.getStatuscode().toString());
            tblHdrlog.setNewStatus(toStatus);

            tblHdrlog = tblHdrlogRepo.save(tblHdrlog);

            //Send Email to Introducer
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("hdr-status");

            //Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(tblHdrclaim.getIntroducer().toString()).orElse(null);

            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]",
                    tblHdrclaimant.getCfname() + " "
                            + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                            + tblHdrclaimant.getCsname());
            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());

            saveEmail(introducerUser.getUsername(), introducerbody,
                    tblHdrclaim.getClaimcode() + " | Processing Note", tblHdrclaim, introducerUser);

            //Send Email to Legal Assist
            TblUser legalUser = tblUsersRepo.findByUsercode("3");
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
            legalbody = legalbody.replace("[CLIENT_NAME]",
                    tblHdrclaimant.getCfname() + " "
                            + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                            + tblHdrclaimant.getCsname());
            legalbody = legalbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());

            saveEmail(legalUser.getUsername(), legalbody,
                    tblHdrclaim.getClaimcode() + " | Processing Note", tblHdrclaim, legalUser);


            TblHdrnote tblHdrnote = new TblHdrnote();
            // NOte for Legal internal user and NOTE for introducer
            tblHdrnote.setTblHdrclaim(tblHdrclaim);
            tblHdrnote.setNote(tblUser.getLoginid()+" has rejected the case. Reason :" + reason);
            tblHdrnote.setUsercategorycode("1");
            tblHdrnote.setCreatedon(new Date());
            tblHdrnote.setUsercode("1");

            addTblHdrNote(tblHdrnote);


            return tblHdrclaim;

        }
    }

    @Override
    public TblHdrclaim performActionOnHdr(String hdrClaimCode, String toStatus, TblUser tblUser) {
        TblHdrclaim hdrclaimOld = tblHdrclaimRepo.findById(Long.valueOf(hdrClaimCode)).orElse(null);
        TblHdrclaimant tblHdrclaimant = tblHdrclaimantRepo.findByTblHdrclaimHdrclaimcode(hdrclaimOld.getHdrclaimcode());

        int update = tblHdrclaimRepo.performAction(new BigDecimal(toStatus), Long.valueOf(hdrClaimCode));
        if (update > 0) {

            if (toStatus.equals("50")) {
                Date clawbackDate = addDaysToDate(90);
                tblHdrclaimRepo.updateClawbackdate(clawbackDate, Long.valueOf(hdrClaimCode));
                tblHdrclaimRepo.updateSubmitDate(new Date(), Long.valueOf(hdrClaimCode));
            }

            TblHdrclaim hdrclaim = tblHdrclaimRepo.findById(Long.valueOf(hdrClaimCode)).orElse(null);
            TblHdrlog tblHdrlog = new TblHdrlog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);

            tblHdrlog.setCreatedon(new Date());
            tblHdrlog.setDescr(tblStatus.getDescr());
            tblHdrlog.setUsercode(tblUser.getUsercode());
            tblHdrlog.setTblHdrclaim(hdrclaimOld);
            tblHdrlog.setOldStatus(hdrclaimOld.getStatuscode().toString());
            tblHdrlog.setNewStatus(toStatus);

            tblHdrlog = tblHdrlogRepo.save(tblHdrlog);
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("hdr-status");
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(hdrclaimOld.getAdvisor())).orElse(null);

            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", hdrclaimOld.getClaimcode());
            introducerbody = introducerbody.replace("[CLIENT_NAME]",
                    tblHdrclaimant.getCfname() + " "
                            + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                            + tblHdrclaimant.getCsname());
            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + hdrclaim.getHdrclaimcode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    tblHdrclaim.getClaimcode() + " | Processing Note", tblHdrclaim, introducerUser);

            //Send Email to Solicitor
            TblHdrsolicitor tblHdrsolicitor = tblHdrsolicitorRepo.findByTblHdrclaimHdrclaimcodeAndStatus(hdrclaim.getHdrclaimcode(), "Y");
            if (tblHdrsolicitor != null) {
                TblUser solicitorUser = tblUsersRepo.findById(tblHdrsolicitor.getUsercode()).orElse(null);

                String solicitorbody = tblEmailTemplate.getEmailtemplate();

                solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                solicitorbody = solicitorbody.replace("[CASE_NUMBER]", hdrclaimOld.getClaimcode());
                solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                        tblHdrclaimant.getCfname() + " "
                                + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                                + tblHdrclaimant.getCsname());
                solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + hdrclaim.getHdrclaimcode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            tblHdrclaim.getClaimcode() + " | Processing Note", tblHdrclaim, solicitorUser);
            }

            TblUser legalUser = tblUsersRepo.findByUsercode("3");
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", hdrclaimOld.getClaimcode());
            legalbody = legalbody.replace("[CLIENT_NAME]",
                    tblHdrclaimant.getCfname() + " "
                            + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                            + tblHdrclaimant.getCsname());
            legalbody = legalbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + hdrclaim.getHdrclaimcode());

            saveEmail(legalUser.getUsername(), legalbody,
                    hdrclaimOld.getClaimcode() + " | Processing Note", hdrclaimOld, legalUser);

            return hdrclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public TblHdrtask getHdrTaskByHdrCodeAndTaskCode(long hdrclaimcode, long taskCode) {
        return tblHdrtaskRepo.findByTblHdrclaimHdrclaimcodeAndTaskcode(hdrclaimcode, new BigDecimal(taskCode));
    }

    @Override
    public List<TblHdrtask> findtblHdrTaskByHdrClaim(long hdrclaimcode) {
        return tblHdrtaskRepo.findByTblHdrclaimHdrclaimcode(hdrclaimcode);
    }

    @Override
    public TblHdrclaim updateHdrRequest(TblHdrclaim tblHdrclaim, TblHdrclaimant tblHdrclaimant, TblHdrjointtenancy tblHdrjointtenancy, TblHdrtenancy tblHdrtenancy, List<TblHdraffectedper> tblHdraffectedperList, List<TblHdraffectedroom> tblHdraffectedroomList) {
        tblHdrclaim = tblHdrclaimRepo.saveAndFlush(tblHdrclaim);
        tblHdrclaimant = tblHdrclaimantRepo.saveAndFlush(tblHdrclaimant);
        tblHdrjointtenancy = tblHdrjointtenancyRepo.saveAndFlush(tblHdrjointtenancy);
        tblHdrtenancy = tblHdrtenancyRepo.saveAndFlush(tblHdrtenancy);
        tblHdraffectedperList = tblHdraffectedperRepo.saveAll(tblHdraffectedperList);
        tblHdraffectedroomList = tblHdraffectedroomRepo.saveAll(tblHdraffectedroomList);

        return tblHdrclaim;
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public List<TblTask> getTasksForHdr() {
        return tblTaskRepo.findTaskById("3");
    }

    @Override
    public List<TblHdrtask> saveHdrTasks(List<TblHdrtask> tblHdrtasks) {
        return tblHdrtaskRepo.saveAll(tblHdrtasks);
    }

    @Override
    public List<HdrCasesList> getAuthHdrCasesIntroducers(String usercode) {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getHdrCasesListForIntroducers(usercode);
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);
                hdrCasesList.setEmail((String) row[7]);
                hdrCasesList.setAddress((String) row[8]);
                hdrCasesList.setContactNo((String) row[9]);
                hdrCasesList.setLastUpdated((Date) row[10]);
                hdrCasesList.setIntroducer((String) row[11]);
                hdrCasesList.setLastNote((Date) row[12]);
                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }

    }

    @Override
    public List<HdrCasesList> getAuthHdrCasesSolicitors(String usercode) {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getHdrCasesListForSolicitor(usercode);
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);
                hdrCasesList.setEmail((String) row[7]);
                hdrCasesList.setAddress((String) row[8]);
                hdrCasesList.setContactNo((String) row[9]);
                hdrCasesList.setLastUpdated((Date) row[10]);
                hdrCasesList.setIntroducer((String) row[11]);
                hdrCasesList.setLastNote((Date) row[12]);
                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }

    }

    @Override
    public List<HdrCasesList> getAuthHdrCasesLegalAssist() {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getHdrCasesList();
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);
                hdrCasesList.setEmail((String) row[7]);
                hdrCasesList.setAddress((String) row[8]);
                hdrCasesList.setContactNo((String) row[9]);
                hdrCasesList.setLastUpdated((Date) row[10]);
                hdrCasesList.setIntroducer((String) row[11]);
                hdrCasesList.setLastNote((Date) row[12]);
                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }
    }

    @Override
    public List<HdrStatusCountList> getAllHdrStatusCountsForIntroducers(String companycode) {
        List<HdrStatusCountList> HdrStatusCountListS = new ArrayList<>();
        List<Object> HdrStatusCountListObject = tblHdrclaimRepo.getAllHdrStatusCountsForIntroducers(companycode);
        HdrStatusCountList hdrStatusCountList;
        if (HdrStatusCountListObject != null && HdrStatusCountListObject.size() > 0) {
            for (Object record : HdrStatusCountListObject) {
                hdrStatusCountList = new HdrStatusCountList();
                Object[] row = (Object[]) record;

                hdrStatusCountList.setStatusCount((BigDecimal) row[0]);
                hdrStatusCountList.setStatusName((String) row[1]);
                hdrStatusCountList.setStatusCode((BigDecimal) row[2]);

                HdrStatusCountListS.add(hdrStatusCountList);
            }
        }
        if (HdrStatusCountListS != null && HdrStatusCountListS.size() > 0) {
            return HdrStatusCountListS;
        } else {
            return null;
        }
    }

    @Override
    public List<HdrStatusCountList> getAllHdrStatusCountsForSolicitor(String companycode) {
        List<HdrStatusCountList> HdrStatusCountListS = new ArrayList<>();
        List<Object> HdrStatusCountListObject = tblHdrclaimRepo.getAllHdrStatusCountsForSolicitor(companycode);
        HdrStatusCountList hdrStatusCountList;
        if (HdrStatusCountListObject != null && HdrStatusCountListObject.size() > 0) {
            for (Object record : HdrStatusCountListObject) {
                hdrStatusCountList = new HdrStatusCountList();
                Object[] row = (Object[]) record;

                hdrStatusCountList.setStatusCount((BigDecimal) row[0]);
                hdrStatusCountList.setStatusName((String) row[1]);
                hdrStatusCountList.setStatusCode((BigDecimal) row[2]);

                HdrStatusCountListS.add(hdrStatusCountList);
            }
        }
        if (HdrStatusCountListS != null && HdrStatusCountListS.size() > 0) {
            return HdrStatusCountListS;
        } else {
            return null;
        }
    }

    @Override
    public List<TblHdrtask> getHdrTaskAgainstHdrCode(String hdrClaimCode) {
        return tblHdrtaskRepo.findByTblHdrclaimHdrclaimcode(Long.valueOf(hdrClaimCode));
    }

    @Override
    public TblHdrclaimant findHdrClaimantByClaimId(long hdrclaimcode) {
        return tblHdrclaimantRepo.findByTblHdrclaimHdrclaimcode(hdrclaimcode);
    }

    @Override
    public TblCompanyprofile findHireBuisnessByHdrCaseById(long hdrclaimcode) {
        List<TblHirebusiness> tblHirebusinesses = tblHirebusinessRepo.findByStatusAndTblHireclaimHirecode("Y", hdrclaimcode);
        return tblCompanyprofileRepo.findById(String.valueOf(tblHirebusinesses.get(0).getHirebusinessescode())).orElse(null);

    }


    @Override
    public TblHdrsolicitor findHdrSolicitorByHdrCode(long hdrCaseId) {
        return tblHdrsolicitorRepo.findByTblHdrclaimHdrclaimcodeAndStatus(hdrCaseId, "Y");

    }

    @Override
    public TblEmail resendHdrEmail(String hdrmessagecode) {
        TblHdrmessage tblHdrmessage = tblHdrmessageRepo.findById(Long.valueOf(hdrmessagecode)).orElse(null);

        TblEmail tblEmail = tblEmailRepo.findById(tblHdrmessage.getEmailcode().getEmailcode()).orElse(null);

        tblEmail.setSenflag(new BigDecimal(0));

        return tblEmailRepo.saveAndFlush(tblEmail);
    }

    @Override
    public TblUser findByUserId(String usercode) {
        return tblUsersRepo.findByUsercode(usercode);
    }

    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblHdrclaim tblHdrclaim,
                             TblUser tblUser, String attachment) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setEmailattachment(attachment);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblHdrmessage tblHdrmessage = new TblHdrmessage();

        tblHdrmessage.setTblHdrclaim(tblHdrclaim);
        tblHdrmessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblHdrmessage.setCreatedon(new Date());
        tblHdrmessage.setMessage(tblEmail.getEmailbody());
        tblHdrmessage.setSentto(tblEmail.getEmailaddress());
        tblHdrmessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblHdrmessage.setEmailcode(tblEmail);

        tblHdrmessage = tblHdrmessageRepo.save(tblHdrmessage);

        return true;
    }

    @Override
    public List<RtaAuditLogResponse> getHdrAuditLogs(String rtacasecode) {
        List<RtaAuditLogResponse> rtaAuditLogResponses = new ArrayList<>();
        RtaAuditLogResponse hdrAuditLogResponse = null;
        List<Object> auditlogsObject = tblHdrclaimRepo.getHdrAuditLogs(Long.valueOf(rtacasecode));
        if (auditlogsObject != null && auditlogsObject.size() > 0) {
            for (Object record : auditlogsObject) {
                hdrAuditLogResponse = new RtaAuditLogResponse();
                Object[] row = (Object[]) record;

                hdrAuditLogResponse.setFieldName((String) row[0]);
                hdrAuditLogResponse.setOldValue((String) row[1]);
                hdrAuditLogResponse.setNewValue((String) row[2]);
                hdrAuditLogResponse.setAuditDate((Date) row[3]);
                hdrAuditLogResponse.setLoggedUser((String) row[4]);

                rtaAuditLogResponses.add(hdrAuditLogResponse);
            }
            return rtaAuditLogResponses;
        } else {
            return null;
        }
    }

    @Override
    public List<TblHdrnote> getAuthHdrCaseNotesOfLegalInternal(String hdrcode, TblUser tblUser) {
        List<TblHdrnote> tblRtanotes = tblHdrnoteRepo.getAuthRtaCaseNotesOfLegalInternal(Long.valueOf(hdrcode),
                tblUser.getUsercode());
        if (tblRtanotes != null && tblRtanotes.size() > 0) {
            for (TblHdrnote tblRtanote : tblRtanotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblRtanote.getUsercode()).orElse(null);
                tblRtanote.setUserName(tblUser1.getLoginid());
                tblRtanote.setSelf(tblRtanote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
            }

            return tblRtanotes;
        } else {
            return null;
        }
    }

    @Override
    public void deleteHdrDocument(long rtadoccode) {
        tblHdrdocumentRepo.deleteById(rtadoccode);
    }

    @Override
    public int updateCurrentTask(long taskCode, long hdrCode, String current) {
        tblHdrtaskRepo.setAllTask("N", hdrCode);
        return tblHdrtaskRepo.setCurrentTask(current, taskCode, hdrCode);
    }

    @Override
    public List<TblHdrdocument> getHdrDocumetsByHdrCode(long hdrclaimcode) {
        return tblHdrdocumentRepo.findByTblHdrclaimHdrclaimcode(hdrclaimcode);
    }

    @Override
    public List<ViewHdrcasereport> hdrCaseReport(HdrCaseReportRequest hdrCaseReportRequest) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
        Date fromDate = formatter.parse(hdrCaseReportRequest.getFromDate());
        Date toDate = formatter.parse(hdrCaseReportRequest.getToDate());

        String dateFrom = formatter1.format(fromDate);
        String dateTo = formatter1.format(toDate);

        return viewHdrcasereportRepo.viewReport(dateTo, dateFrom);
    }

    @Override
    public List<HdrCasesList> getFiltreAuthHdrCasesIntroducers(FilterRequest filterRequest, String usercode) {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getFilterHdrCasesListForIntroducers(filterRequest.getName(), filterRequest.getPostCode(), filterRequest.getPortalCode(),
                filterRequest.getMobileNo(), filterRequest.getEmail(), filterRequest.getNiNumber(), usercode);
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);
                hdrCasesList.setEmail((String) row[7]);
                hdrCasesList.setAddress((String) row[8]);
                hdrCasesList.setContactNo((String) row[9]);
                hdrCasesList.setLastUpdated((Date) row[10]);
                hdrCasesList.setIntroducer((String) row[11]);
                hdrCasesList.setLastNote((Date) row[12]);
                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }
    }

    @Override
    public List<HdrCasesList> getFiltreAuthHdrCasesSolicitors(FilterRequest filterRequest, String usercode) {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getFiletrHdrCasesListForSolicitor(filterRequest.getName(), filterRequest.getPostCode(), filterRequest.getPortalCode(),
                filterRequest.getMobileNo(), filterRequest.getEmail(), filterRequest.getNiNumber(), usercode);
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);
                hdrCasesList.setEmail((String) row[7]);
                hdrCasesList.setAddress((String) row[8]);
                hdrCasesList.setContactNo((String) row[9]);
                hdrCasesList.setLastUpdated((Date) row[10]);
                hdrCasesList.setIntroducer((String) row[11]);
                hdrCasesList.setLastNote((Date) row[12]);
                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }
    }

    @Override
    public List<HdrCasesList> getFiltreAuthHdrCasesLegalAssist(FilterRequest filterRequest) {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getFilterHdrCasesList(filterRequest.getName(), filterRequest.getPostCode(), filterRequest.getPortalCode(),
                filterRequest.getMobileNo(), filterRequest.getEmail(), filterRequest.getNiNumber());
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);
                hdrCasesList.setEmail((String) row[7]);
                hdrCasesList.setAddress((String) row[8]);
                hdrCasesList.setContactNo((String) row[9]);
                hdrCasesList.setLastUpdated((Date) row[10]);
                hdrCasesList.setIntroducer((String) row[11]);
                hdrCasesList.setLastNote((Date) row[12]);
                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }
    }

    @Override
    public List<HdrCasesList> getAuthHdrCasesIntroducersStatusWise(String companyCode, long statusId) {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getAuthHdrCasesIntroducersStatusWise(companyCode, statusId);
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);

                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }
    }

    @Override
    public List<HdrCasesList> getAuthHdrCasesLegalAssistStatusWise(long statusId) {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getHdrCasesListStatusWise(statusId);
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);

                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }
    }

    @Override
    public List<HdrCasesList> getAuthHdrCasesSolicitorsStatusWise(String companycode, long statusId) {
        List<HdrCasesList> hdrCasesLists = new ArrayList<>();
        List<Object> hdrCasesListObject = tblHdrclaimRepo.getHdrCasesListSolicitorStatusWise(companycode, statusId);
        HdrCasesList hdrCasesList;
        if (hdrCasesListObject != null && hdrCasesListObject.size() > 0) {
            for (Object record : hdrCasesListObject) {
                hdrCasesList = new HdrCasesList();
                Object[] row = (Object[]) record;

                hdrCasesList.setHdrClaimCode((BigDecimal) row[0]);
                hdrCasesList.setCreated((String) row[1]);
                hdrCasesList.setCode((String) row[2]);
                hdrCasesList.setClient((String) row[3]);
                hdrCasesList.setTaskDue((String) row[4]);
                hdrCasesList.setTaskName((String) row[5]);
                hdrCasesList.setStatus((String) row[6]);

                hdrCasesLists.add(hdrCasesList);
            }
        }
        if (hdrCasesLists != null && hdrCasesLists.size() > 0) {
            return hdrCasesLists;
        } else {
            return null;
        }
    }

    @Override
    public TblHdrclaim performRevertOnHdr(String hdrClaimCode, TblUser tblUser) {
        Session session = em.unwrap(Session.class);
        final String[] msg = new String[1];
        final int[] status = new int[1];

        session.doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                CallableStatement call = connection.prepareCall("{call PROC_REVERT(?,?,?,?) }");
                call.setString(1, "HDR");
                call.setLong(2, Long.parseLong(hdrClaimCode));
                call.registerOutParameter(3, Types.INTEGER);
                call.registerOutParameter(4, Types.VARCHAR);
                call.execute();
                status[0] = call.getInt(3);
                msg[0] = call.getString(4);
            }
        });

        if (status[0] == 1) {
            return tblHdrclaimRepo.findById(Long.valueOf(hdrClaimCode)).orElse(null);
        } else {
            return null;
        }
    }

    @Override
    public List<RtaDuplicatesResponse> getHdrDuplicates(String hdrcasecode) {
        TblHdrclaimant tblHdrclaimant = tblHdrclaimantRepo.findByTblHdrclaimHdrclaimcode(Long.valueOf(hdrcasecode));
        if (tblHdrclaimant != null) {
            List<RtaDuplicatesResponse> rtaDuplicatesResponses = new ArrayList<>();
            RtaDuplicatesResponse rtaDuplicatesResponse = new RtaDuplicatesResponse();

            List<Object> NameDuplicates = tblHdrclaimantRepo.duplicatesForName(tblHdrclaimant.getCfname(),
                    tblHdrclaimant.getCmname(), tblHdrclaimant.getCsname());

            List<Object> addressDuplicates = tblHdrclaimantRepo.duplicatesForAddress(tblHdrclaimant.getCpostalcode(),
                    tblHdrclaimant.getCaddress1(),
                    tblHdrclaimant.getCaddress2(),
                    tblHdrclaimant.getCaddress3());



            if (tblHdrclaimant.getCninumber() != null && !tblHdrclaimant.getCninumber().equals("Will be provided to the solicitor")) {
                List<Object> niDuplicates = tblHdrclaimantRepo.duplicatesForNiNumber(tblHdrclaimant.getCninumber());

                if (niDuplicates != null && niDuplicates.size() > 0) {
                    for (Object record : niDuplicates) {
                        rtaDuplicatesResponse = new RtaDuplicatesResponse();
                        Object[] row = (Object[]) record;

                        rtaDuplicatesResponse.setDuplicateType("Ni-Number");
                        rtaDuplicatesResponse.setCaseNumber((String) row[1]);
                        rtaDuplicatesResponse.setFullName((String) row[2]);
                        rtaDuplicatesResponse.setHdrcode(((BigDecimal) row[3]).toString());

                        rtaDuplicatesResponses.add(rtaDuplicatesResponse);

                    }
                }
            }


            if (NameDuplicates != null && NameDuplicates.size() > 0) {
                for (Object record : NameDuplicates) {
                    rtaDuplicatesResponse = new RtaDuplicatesResponse();
                    Object[] row = (Object[]) record;

                    rtaDuplicatesResponse.setDuplicateType("NAME");
                    rtaDuplicatesResponse.setCaseNumber((String) row[1]);
                    rtaDuplicatesResponse.setFullName((String) row[2]);
                    rtaDuplicatesResponse.setHdrcode(((BigDecimal) row[3]).toString());

                    rtaDuplicatesResponses.add(rtaDuplicatesResponse);

                }
            }

            if (addressDuplicates != null && addressDuplicates.size() > 0) {
                for (Object record : addressDuplicates) {
                    rtaDuplicatesResponse = new RtaDuplicatesResponse();
                    Object[] row = (Object[]) record;

                    rtaDuplicatesResponse.setDuplicateType("ADDRESS");
                    rtaDuplicatesResponse.setCaseNumber((String) row[1]);
                    rtaDuplicatesResponse.setFullName((String) row[2]);
                    rtaDuplicatesResponse.setHdrcode(((BigDecimal) row[3]).toString());

                    rtaDuplicatesResponses.add(rtaDuplicatesResponse);

                }
            }


            return rtaDuplicatesResponses;

        } else {
            return null;
        }
    }

    @Override
    public TblHdrclaimcopy checkforChildRecord(long hdrclaimcode) {
        return tblHdrclaimcopyRepo.findByTblHdrclaim1Hdrclaimcode(hdrclaimcode);
    }

    @Override
    public TblHdrclaimcopy saveCopyLog(TblHdrclaimcopy copyLog) {
        return tblHdrclaimcopyRepo.saveAndFlush(copyLog);
    }
}
