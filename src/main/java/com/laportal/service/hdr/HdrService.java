package com.laportal.service.hdr;

import com.laportal.dto.*;
import com.laportal.model.*;

import java.text.ParseException;
import java.util.List;

public interface HdrService {

    List<TblHdrlog> getHdrCaseLogs(String hdrcode);

    List<TblHdrnote> getHdrCaseNotes(String hdrcode, TblUser tblUser);

    List<TblHdrmessage> getHdrCaseMessages(String hdrcode);

    boolean isHdrCaseAllowed(String companycode, String s);

    TblHdrclaim saveHdrRequest(TblHdrclaim tblHdrclaim, TblHdrclaimant tblHdrclaimant, TblHdrjointtenancy tblHdrjointtenancy, TblHdrtenancy tblHdrtenancy, List<TblHdraffectedper> tblHdraffectedperList, List<TblHdraffectedroom> tblHdraffectedroomList);

    TblHdrclaim findHdrClaimByCode(String hdrClaimCode);

    List<HdrCasesList> getHdrCases(TblUser tblUser);

    TblHdrclaim findHdrCaseById(long hdrCaseId, TblUser tblUser);

    TblCompanyprofile getCompanyProfile(String companycode);

    List<HdrStatusCountList> getAllHdrStatusCounts();

    List<HdrCasesList> getHdrCasesByStatus(long statusId);

    TblHdrnote addTblHdrNote(TblHdrnote tblHdrnote);

    List<TblHdrdocument> saveTblHdrDocuments(List<TblHdrdocument> saveTblHdrDocuments);

    TblCompanyprofile getCompanyProfileAgainstHdrCode(long hdrclaimcode);

    TblCompanydoc getCompanyDocs(String companyCode,  String jointenency);

    String getHdrDbColumnValue(String key, long hdrclaimcode);

    TblHdrdocument saveTblHdrDocument(TblHdrdocument tblHdrdocument);

    TblHdrclaim updateHdrCase(TblHdrclaim tblHdrclaim);

    TblHdrtask updateTblHdrTask(TblHdrtask tblHdrtask);

    TblHdrmessage getTblHdrMessageById(String hdrmessagecode);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblHdrclaim tblHdrclaim,
                      TblUser tblUser) ;

    TblHdrdocument findHdrDocumentById(long hdrCaseId);

    TblHdrclaim assignCaseToSolicitor(AssignHdrCasetoSolicitorOnHdr assignHdrCasetoSolicitorOnHdr, TblUser tblUser);

    TblHdrclaim findHdrCaseByIdwithoutuser(long rtaCode);

    TblTask findTaskById(long hdrtaskcode);

    List<TblHdrtask> getHdrTaskAgainstHdrCodeAndStatus(String hdrClaimCode, String status);

    TblHdrclaim performRejectCancelActionOnHdr(String hdrClaimCode, String toStatus, String reason, TblUser tblUser);

    TblHdrclaim performActionOnHdr(String hdrClaimCode, String toStatus, TblUser tblUser);

    TblTask getTaskAgainstCode(long taskCode);

    TblHdrtask getHdrTaskByHdrCodeAndTaskCode(long hdrclaimcode, long taskCode);

    List<TblHdrtask> findtblHdrTaskByHdrClaim(long hdrclaimcode);

    TblHdrclaim updateHdrRequest(TblHdrclaim tblHdrclaim, TblHdrclaimant tblHdrclaimant, TblHdrjointtenancy tblHdrjointtenancy, TblHdrtenancy tblHdrtenancy, List<TblHdraffectedper> tblHdraffectedperList, List<TblHdraffectedroom> tblHdraffectedroomList);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    List<TblTask> getTasksForHdr();

    List<TblHdrtask> saveHdrTasks(List<TblHdrtask> tblHdrtasks);

    List<HdrCasesList> getAuthHdrCasesIntroducers(String usercode);

    List<HdrCasesList> getAuthHdrCasesSolicitors(String usercode);

    List<HdrCasesList> getAuthHdrCasesLegalAssist();

    List<HdrStatusCountList> getAllHdrStatusCountsForIntroducers(String companycode);

    List<HdrStatusCountList> getAllHdrStatusCountsForSolicitor(String companycode);

    List<TblHdrtask> getHdrTaskAgainstHdrCode(String hdrClaimCode);

    TblHdrclaimant findHdrClaimantByClaimId(long hdrclaimcode);

    TblCompanyprofile findHireBuisnessByHdrCaseById(long hdrclaimcode);

    TblHdrsolicitor findHdrSolicitorByHdrCode(long hdrCaseId);

    TblEmail resendHdrEmail(String hdrmessagecode);

    TblUser findByUserId(String usercode);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblHdrclaim tblHdrclaim,
                      TblUser tblUser, String attachment);

    List<RtaAuditLogResponse> getHdrAuditLogs(String hdrcasecode);

    List<TblHdrnote> getAuthHdrCaseNotesOfLegalInternal(String hdrcode, TblUser tblUser);

    void deleteHdrDocument(long rtadoccode);

    int updateCurrentTask(long taskCode, long hdrCode, String current);

    List<TblHdrdocument> getHdrDocumetsByHdrCode(long hdrclaimcode);

    List<ViewHdrcasereport> hdrCaseReport(HdrCaseReportRequest hdrCaseReportRequest) throws ParseException;

    List<HdrCasesList> getFiltreAuthHdrCasesIntroducers(FilterRequest filterRequest, String usercode);

    List<HdrCasesList> getFiltreAuthHdrCasesSolicitors(FilterRequest filterRequest, String usercode);

    List<HdrCasesList> getFiltreAuthHdrCasesLegalAssist(FilterRequest filterRequest);

    List<HdrCasesList> getAuthHdrCasesIntroducersStatusWise(String companyCode, long statusId);

    List<HdrCasesList> getAuthHdrCasesLegalAssistStatusWise(long statusId);

    List<HdrCasesList> getAuthHdrCasesSolicitorsStatusWise(String companycode, long statusId);

    TblHdrclaim performRevertOnHdr(String hdrClaimCode, TblUser tblUser);

    List<RtaDuplicatesResponse> getHdrDuplicates(String hdrcasecode);

    TblHdrclaimcopy checkforChildRecord(long hdrclaimcode);

    TblHdrclaimcopy saveCopyLog(TblHdrclaimcopy copyLog);
}
