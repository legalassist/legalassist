package com.laportal.service.hire;

import com.laportal.Repo.*;
import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional(rollbackFor = Exception.class)
public class HireServiceImpl extends AbstractApi implements HireService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;
    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;
    @Autowired
    private TblHireclaimRepo tblHireClaimRepo;
    @Autowired
    private TblHiredocumentRepo tblHiredocumentRepo;
    @Autowired
    private TblUsersRepo tblUsersRepo;
    @Autowired
    private TblHirelogRepo tblHirelogRepo;
    @Autowired
    private TblHirenoteRepo tblHirenoteRepo;
    @Autowired
    private TblHiremessageRepo tblHiremessageRepo;
    @Autowired
    private TblRolesRepo tblRolesRepo;
    @Autowired
    private TblHirebusinessRepo tblHirebusinessRepo;
    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;
    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;
    @Autowired
    private TblEmailRepo tblEmailRepo;
    @Autowired
    private TblHirebusinessdetailRepo tblHirebusinessdetailRepo;
    @Autowired
    private ViewRtaclaminRepo viewRtaclaminRepo;
    @Autowired
    private TblEmailTemplateRepo tblEmailTemplateRepo;
    @Autowired
    private ViewHirecasereportRepo viewHirecasereportRepo;
    @Autowired
    private TblSolicitorInvoiceDetailRepo tblSolicitorInvoiceDetailRepo;
    @Autowired
    private TblHiretaskRepo tblHiretaskRepo;
    @Autowired
    private TblTaskRepo tblTaskRepo;
    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;
    @Autowired
    private TblEsignStatusRepo tblEsignStatusRepo;
    @Autowired
    private TblRtaflowRepo tblRtaflowRepo;

    @Override
    public boolean isHireCaseAllowed(String companycode, String campaigncode) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companycode,
                campaigncode);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public int getHireDocNumber() {
        return tblHireClaimRepo.getHireDocNumber();
    }

    @Override
    public TblHireclaim addNewHireCase(TblHireclaim tblHireclaim, List<HireFileUploadRequest> files) {

        TblHireclaim hireclaim = tblHireClaimRepo.save(tblHireclaim);

        if (hireclaim != null && hireclaim.getHirecode() > 0) {
            if (files != null && files.size() > 0) {
                for (HireFileUploadRequest hireFileUploadRequest : files) {
                    TblHiredocument tblHiredocument = new TblHiredocument();
                    String milisecond = String.valueOf(System.currentTimeMillis());
                    tblHiredocument.setDocname(hireFileUploadRequest.getFileName() + milisecond + "."
                            + hireFileUploadRequest.getFileExt());
                    tblHiredocument.setDoctype("Image");
                    tblHiredocument.setDocbase64(hireFileUploadRequest.getFileBase64());
                    tblHiredocument.setTblHireclaim(hireclaim);
                    tblHiredocument.setCreatedon(new Date());
                    tblHiredocument.setUsercode(tblHireclaim.getUsercode());

                    tblHiredocumentRepo.save(tblHiredocument);
                }
            }

            TblHirelog tblHirelog = new TblHirelog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(tblHireclaim.getTblRtastatus().getStatuscode())).orElse(null);

            tblHirelog.setCreatedon(new Date());
            tblHirelog.setDescr(tblStatus.getDescr());
            tblHirelog.setUsercode(String.valueOf(tblHireclaim.getAdvisor()));
            tblHirelog.setTblHireclaim(tblHireclaim);


            tblHirelog = tblHirelogRepo.save(tblHirelog);

            return hireclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblHireclaim getTblHireClaimById(long hirecode) {
        return tblHireClaimRepo.findById(hirecode).orElse(null);
    }

    @Override
    public TblHireclaim updateHireCase(TblHireclaim tblHireclaim) {
        TblHireclaim hireclaim = tblHireClaimRepo.save(tblHireclaim);
        return hireclaim;
    }

    @Override
    public List<TblHiredocument> addHireDocument(List<TblHiredocument> tblHiredocuments) {
        return tblHiredocumentRepo.saveAll(tblHiredocuments);
    }

    @Override
    public List<TblHireclaim> getHireCases(TblUser tblUser) {
        return tblHireClaimRepo.findByUsercode(tblUser.getUsercode());
    }

    @Override
    public TblHireclaim getHireCaseById(long hirecode, TblUser tblUser) {
        TblHireclaim tblHireclaim = tblHireClaimRepo.findByHirecode(hirecode);

        if (tblHireclaim != null) {
            tblHireclaim.setStatusDescr(tblHireclaim.getTblRtastatus().getDescr());
            List<HireActionButton> hireActionButtons = new ArrayList<HireActionButton>();
            List<HireActionButton> hireActionButtonsForLa = new ArrayList<HireActionButton>();
            HireActionButton hireActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    Long.valueOf(tblHireclaim.getTblRtastatus().getStatuscode()), "2", tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetailsForLa = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    Long.valueOf(tblHireclaim.getTblRtastatus().getStatuscode()), "2", tblCompanyprofile.getTblUsercategory().getCategorycode());

            TblRtaflow tblRtaflow = tblRtaflowRepo.findByTblCompaignCompaigncodeAndTblRtastatusStatuscode("2", tblHireclaim.getTblRtastatus().getStatuscode());
            if (tblRtaflow != null) {
                tblHireclaim.setEditFlag(tblRtaflow.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode()) ? tblRtaflow.getEditflag() : "N");
            }

            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    hireActionButton = new HireActionButton();
                    hireActionButton.setCaseacceptdialog(tblRtaflowdetail.getCaseacceptdialog());
                    hireActionButton.setCaseassigndialog(tblRtaflowdetail.getCaseassigndialog());
                    hireActionButton.setCaserejectdialog(tblRtaflowdetail.getCaserejectdialog());
                    hireActionButton.setCasecanceldialog(tblRtaflowdetail.getCasecanceldialog());
                    hireActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    hireActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    hireActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    hireActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());


                    hireActionButtons.add(hireActionButton);
                }
                tblHireclaim.setHireActionButtons(hireActionButtons);

                if (tblRtaflowdetailsForLa != null && tblRtaflowdetailsForLa.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailsForLa) {
                        hireActionButton = new HireActionButton();
                        hireActionButton.setCaseacceptdialog(tblRtaflowdetail.getCaseacceptdialog());
                        hireActionButton.setCaseassigndialog(tblRtaflowdetail.getCaseassigndialog());
                        hireActionButton.setCaserejectdialog(tblRtaflowdetail.getCaserejectdialog());
                        hireActionButton.setCasecanceldialog(tblRtaflowdetail.getCasecanceldialog());
                        hireActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        hireActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        hireActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        hireActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());

                        hireActionButtonsForLa.add(hireActionButton);
                    }
                    tblHireclaim.setHireActionButtonForLA(hireActionButtonsForLa);
                }


                if (tblHireclaim.getTblHirelogs() != null && tblHireclaim.getTblHirelogs().size() > 0) {
                    for (TblHirelog tblHireLog : tblHireclaim.getTblHirelogs()) {
                        TblUser tblUser1 = tblUsersRepo.findById(tblHireLog.getUsercode()).orElse(null);
                        tblHireLog.setUserName(tblUser1.getUsername());
                    }
                }

                List<TblHirebusiness> tblHirebusinesses = new ArrayList<TblHirebusiness>();

                if (tblHireclaim.getTblHirebusinesses() != null && tblHireclaim.getTblHirebusinesses().size() > 0) {
                    for (TblHirebusiness tblHirebusiness : tblHireclaim.getTblHirebusinesses()) {
                        TblCompanyprofile hirCompany = tblCompanyprofileRepo.findById(tblHirebusiness.getTblCompanyprofile().getCompanycode()).orElse(null);
                        if (hirCompany.getTblUsercategory().getCategorycode().equals(tblCompanyprofile.getTblUsercategory().getCategorycode())
                                && hirCompany.getTblUsercategory().getCategorycode().equals(2)) {
                            tblHirebusinesses.add(tblHirebusiness);
                        }
                    }
                }

                if (tblHirebusinesses != null && tblHirebusinesses.size() > 0) {
                    tblHireclaim.setTblHirebusinesses(null);
                    tblHireclaim.setTblHirebusinesses(tblHirebusinesses);
                }
                for (TblHirebusiness tblHirebusiness : tblHireclaim.getTblHirebusinesses()) {
                    TblUser user = tblUsersRepo.findByUsercode(tblHirebusiness.getStatususer());
                    tblHirebusiness.setStatususername(user.getLoginid());
                    tblHirebusiness.setHandleremail(user.getUsername());
                }
                if (tblHireclaim.getIntroducer() != null && tblHireclaim.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblHireclaim.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(String.valueOf(tblHireclaim.getIntroducer())).orElse(null);

                    tblHireclaim.setAdvisorname(user.getLoginid());
                    tblHireclaim.setIntroducername(companyprofile.getName());
                }


                // GET HIRE CASE TASK ////////

                List<TblHiretask> tblHiretasks = tblHiretaskRepo.findByTblHireclaimHirecode(tblHireclaim.getHirecode());
                if (tblHiretasks != null && tblHiretasks.size() > 0) {
                    tblHireclaim.setHireCaseTasks(tblHiretasks);
                }


                ////////////////////////////////

                if (tblHireclaim.getInvoiceDate() != null) {
                    List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("2", BigDecimal.valueOf(tblHireclaim.getHirecode()));
                    if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                        for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                            TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                            if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                                tblHireclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                                tblHireclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                            } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                                tblHireclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                                tblHireclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                            }

                        }
                    }
                }
                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("2", new BigDecimal(hirecode));
                tblHireclaim.setTblEsignStatus(tblEsignStatus);
                return tblHireclaim;
            } else {

                if (tblRtaflowdetailsForLa != null && tblRtaflowdetailsForLa.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailsForLa) {
                        hireActionButton = new HireActionButton();
                        hireActionButton.setCaseacceptdialog(tblRtaflowdetail.getCaseacceptdialog());
                        hireActionButton.setCaseassigndialog(tblRtaflowdetail.getCaseassigndialog());
                        hireActionButton.setCaserejectdialog(tblRtaflowdetail.getCaserejectdialog());
                        hireActionButton.setCasecanceldialog(tblRtaflowdetail.getCasecanceldialog());
                        hireActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        hireActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        hireActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        hireActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());

                        hireActionButtonsForLa.add(hireActionButton);
                    }
                    tblHireclaim.setHireActionButtonForLA(hireActionButtonsForLa);
                }

                List<TblHirebusiness> tblHirebusinesses = new ArrayList<TblHirebusiness>();

                if (tblHireclaim.getTblHirebusinesses() != null && tblHireclaim.getTblHirebusinesses().size() > 0) {
                    for (TblHirebusiness tblHirebusiness : tblHireclaim.getTblHirebusinesses()) {
                        if (tblHirebusiness.getTblCompanyprofile().getTblUsercategory().getCategorycode().equals(tblCompanyprofile.getTblUsercategory().getCategorycode())
                                && tblHirebusiness.getTblCompanyprofile().getTblUsercategory().getCategorycode().equals(2)) {
                            tblHirebusinesses.add(tblHirebusiness);
                        }
                    }
                }

                if (tblHirebusinesses != null && tblHirebusinesses.size() > 0) {
                    tblHireclaim.setTblHirebusinesses(null);
                    tblHireclaim.setTblHirebusinesses(tblHirebusinesses);
                }
                for (TblHirebusiness tblHirebusiness : tblHireclaim.getTblHirebusinesses()) {
                    TblUser user = tblUsersRepo.findByUsercode(tblHirebusiness.getStatususer());
                    tblHirebusiness.setStatususername(user.getLoginid());
                    tblHirebusiness.setHandleremail(user.getUsername());
                }
                if (tblHireclaim.getIntroducer() != null && tblHireclaim.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(tblHireclaim.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo.getOne(String.valueOf(tblHireclaim.getIntroducer()));

                    tblHireclaim.setAdvisorname(user.getLoginid());
                    tblHireclaim.setIntroducername(companyprofile.getName());
                }


                // GET HIRE CASE TASK ////////

                List<TblHiretask> tblHiretasks = tblHiretaskRepo.findByTblHireclaimHirecode(tblHireclaim.getHirecode());
                if (tblHiretasks != null && tblHiretasks.size() > 0) {
                    tblHireclaim.setHireCaseTasks(tblHiretasks);
                }

                ////////////////////////////////

                if (tblHireclaim.getInvoiceDate() != null) {
                    List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("2", BigDecimal.valueOf(tblHireclaim.getHirecode()));
                    if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                        for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                            TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                            if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                                tblHireclaim.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                                tblHireclaim.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                            } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                                tblHireclaim.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                                tblHireclaim.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                            }

                        }
                    }
                }

                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("2", new BigDecimal(hirecode));
                tblHireclaim.setTblEsignStatus(tblEsignStatus);


                return tblHireclaim;
            }
        } else {
            return null;
        }
    }

    @Override
    public List<TblHirelog> getHireCaseLogs(String hireCode) {
        List<TblHirelog> tblHirelogs = tblHirelogRepo.findByTblHireclaimHirecode(Long.valueOf(hireCode));
        if (tblHirelogs != null && tblHirelogs.size() > 0) {
            for (TblHirelog tblHireLog : tblHirelogs) {
                TblUser tblUser = tblUsersRepo.findById(tblHireLog.getUsercode()).orElse(null);
                tblHireLog.setUserName(tblUser.getUsername());
            }
            return tblHirelogs;
        } else {
            return null;
        }
    }

    @Override
    public List<TblHirenote> getHireCaseNotes(String hirecode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblHirenote> tblHireNotes = tblHirenoteRepo.findNotesOnHirebyUserAndCategory(
                Long.valueOf(hirecode), tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (tblHireNotes != null && tblHireNotes.size() > 0) {
            for (TblHirenote tblHirenote : tblHireNotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblHirenote.getUsercode()).orElse(null);
                tblHirenote.setUserName(tblUser1.getUsername());
                tblHirenote.setSelf(tblHirenote.getUsercode() == tblUser.getUsercode() ? true : false);
            }
            return tblHireNotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblHiremessage> getHireCaseMessages(String hirecode) {
        List<TblHiremessage> tblHireMessages = tblHiremessageRepo.findAllByTblHireclaimHirecode(Long.valueOf(hirecode));
        if (tblHireMessages != null && tblHireMessages.size() > 0) {
            for (TblHiremessage tblHireMessage : tblHireMessages) {
                TblUser tblUser = tblUsersRepo.findById(tblHireMessage.getUsercode()).orElse(null);
                tblHireMessage.setUserName(tblUser.getUsername());
            }
            return tblHireMessages;
        } else {
            return null;
        }
    }

    @Override
    public TblHirenote addTblHireNote(TblHirenote tblHirenote) {
        tblHirenote = tblHirenoteRepo.save(tblHirenote);
        TblHireclaim tblHireclaim = tblHireClaimRepo.findByHirecode(tblHirenote.getTblHireclaim().getHirecode());

        TblUser legalUser = tblUsersRepo.findByUsercode("2");
        // for legal internal

        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("note");
        String legalbody = tblEmailTemplate.getEmailtemplate();

        legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
        legalbody = legalbody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
        legalbody = legalbody.replace("[CLIENT_NAME]", tblHireclaim.getFirstname() + " " + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ") + tblHireclaim.getLastname());
        legalbody = legalbody.replace("[NOTE]", tblHirenote.getNote());
        legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());

        saveEmail(legalUser.getUsername(), legalbody,
                tblHireclaim.getHirenumber() + " | Processing Note", tblHireclaim, legalUser);

        if (!tblHirenote.getUsercategorycode().equals("4")) {
            // for introducer
            if (tblHirenote.getUsercategorycode().equals("1")) {
                TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblHireclaim.getAdvisor()));
                String introducerBody = tblEmailTemplate.getEmailtemplate();

                introducerBody = introducerBody.replace("[USER_NAME]", legalUser.getLoginid());
                introducerBody = introducerBody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
                introducerBody = introducerBody.replace("[CLIENT_NAME]", tblHireclaim.getFirstname() + " " + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ") + tblHireclaim.getLastname());
                introducerBody = introducerBody.replace("[NOTE]", tblHirenote.getNote());
                introducerBody = introducerBody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());

                saveEmail(introducerUser.getUsername(), introducerBody,
                        tblHireclaim.getHirenumber() + " | Processing Note", tblHireclaim, introducerUser);
            } else if (tblHirenote.getUsercategorycode().equals("2")) {

                List<TblHirebusiness> tblHirebusinesses = tblHirebusinessRepo.findByStatusAndTblHireclaimHirecode("Y", tblHirenote.getTblHireclaim().getHirecode());

                if (tblHirebusinesses != null && tblHirebusinesses.size() > 0) {
                    TblUser solicitorUser = tblUsersRepo.findById(tblHirebusinesses.get(0).getTblUser().getUsercode()).orElse(null);

                    String solicitorBody = tblEmailTemplate.getEmailtemplate();

                    solicitorBody = solicitorBody.replace("[USER_NAME]", legalUser.getLoginid());
                    solicitorBody = solicitorBody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
                    solicitorBody = solicitorBody.replace("[CLIENT_NAME]", tblHireclaim.getFirstname() + " " + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ") + tblHireclaim.getLastname());
                    solicitorBody = solicitorBody.replace("[NOTE]", tblHirenote.getNote());
                    solicitorBody = solicitorBody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());

                    saveEmail(solicitorUser.getUsername(), solicitorBody,
                            tblHireclaim.getHirenumber() + " | Processing Note", tblHireclaim, solicitorUser);

                }
            }

        }

        return tblHirenote;
    }

    @Override
    public List<TblCompanyprofile> getAllCompaniesProfile() {
        List<TblCompanyprofile> tblCompanyprofiles = tblCompanyprofileRepo.findAll();
        List<TblCompanyprofile> Companyprofiles = new ArrayList<>();
        if (tblCompanyprofiles != null && tblCompanyprofiles.size() > 0) {
            for (TblCompanyprofile tblCompanyprofile : tblCompanyprofiles) {
                if (tblCompanyprofile.getCompanycode() != null) {
                    List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycode(tblCompanyprofile.getCompanycode());
                    List<TblUser> tblUsers = tblUsersRepo.findByCompanycode(tblCompanyprofile.getCompanycode());
                    List<LovResponse> lovResponses = new ArrayList<>();
                    LovResponse lovResponse = null;
                    for (TblUser tblUser : tblUsers) {
                        List<TblRole> tblRoles = tblRolesRepo.getAllRoleFromUserRoles(tblUser.getUsercode());
                        for (TblRole tblRole : tblRoles) {
                            lovResponse = new LovResponse();
                            lovResponse.setCode(tblRole.getRolecode());
                            lovResponse.setName(tblRole.getRolename());
                            lovResponses.add(lovResponse);
                        }
                        tblUser.setLovResponse(lovResponses);
                    }
                    tblCompanyprofile.setTblCompanyjobs(tblCompanyjobs);
                    tblCompanyprofile.setTblUsers(tblUsers);
                }
                Companyprofiles.add(tblCompanyprofile);
            }
            return Companyprofiles;
        } else {
            return null;
        }
    }

    @Override
    public List<TblUser> getAllUsers() {
        return tblUsersRepo.findAll();
    }

    @Override
    public TblHirebusiness addTblHireBusiness(TblHirebusiness tblHirebusiness) {
        TblHireclaim tblHireclaim = tblHireClaimRepo.findById(tblHirebusiness.getTblHireclaim().getHirecode()).orElse(null);
        int update = tblHireClaimRepo.performAction(String.valueOf(tblHirebusiness.getTblHireclaim().getTblRtastatus().getStatuscode()), tblHirebusiness.getCreatedby(), Long.valueOf(tblHirebusiness.getTblHireclaim().getHirecode()));
        TblRtastatus tblRtastatus = tblRtastatusRepo.findById(tblHirebusiness.getTblHireclaim().getTblRtastatus().getStatuscode()).orElse(null);

        TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblHireclaim.getAdvisor()));
        TblUser solicitorUser = tblUsersRepo
                .findByUsercode(tblHirebusiness.getStatususer());
        TblUser legalUser = tblUsersRepo.findByUsercode("4");
        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("hire-status-hotkey");

        // for introducer
/*        String introducerbody = tblEmailTemplate.getEmailtemplate();

        introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
        introducerbody = introducerbody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
        introducerbody = introducerbody.replace("[CLIENT_NAME]",
                tblHireclaim.getFirstname() + " "
                        + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ")
                        + tblHireclaim.getLastname());

        saveEmail(introducerUser.getUsername(), introducerbody,
                tblHireclaim.getHirenumber() + " | HotKey", tblHireclaim, introducerUser);*/

        // for solicitor
        String solicitorbody = tblEmailTemplate.getEmailtemplate();

        solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
        solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
        solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                tblHireclaim.getFirstname() + " "
                        + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ")
                        + tblHireclaim.getLastname());
        solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());

        saveEmail(solicitorUser.getUsername(), solicitorbody, tblHireclaim.getHirenumber() + " | Processing Note",
                tblHireclaim, solicitorUser);

        // for legal internal
        String legalbody = tblEmailTemplate.getEmailtemplate();

        legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
        legalbody = legalbody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
        legalbody = legalbody.replace("[CLIENT_NAME]",
                tblHireclaim.getFirstname() + " "
                        + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ")
                        + tblHireclaim.getLastname());
        legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());

        saveEmail(legalUser.getUsername(), legalbody,
                tblHireclaim.getHirenumber() + " | HotKey", tblHireclaim, legalUser);
/// for logging
        TblHirelog tblHirelog = new TblHirelog();

        TblRtastatus tblStatus = tblRtastatusRepo.findById(tblHirebusiness.getTblHireclaim().getTblRtastatus().getStatuscode()).orElse(null);


        tblHirelog.setCreatedon(new Date());
        tblHirelog.setDescr(tblStatus.getDescr());
        tblHirelog.setUsercode(tblHirebusiness.getCreatedby());
        tblHirelog.setTblHireclaim(tblHireclaim);
        tblHirelog.setNewstatus(tblHirebusiness.getTblHireclaim().getTblRtastatus().getStatuscode());
        tblHirelog.setOldstatus(tblHireclaim.getTblRtastatus().getStatuscode());

        tblHirelog = tblHirelogRepo.save(tblHirelog);
        return tblHirebusinessRepo.saveAndFlush(tblHirebusiness);
    }

    @Override
    public TblHirebusiness getHireBusinessAgainstHireAndCompanyCode(long hireCode, String companyCode) {
        return tblHirebusinessRepo.findByTblHireclaimHirecodeAndTblCompanyprofileCompanycode(hireCode, companyCode);
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public TblHirelog saveTblHireLogs(TblHirelog tblHirelog) {
        return tblHirelogRepo.save(tblHirelog);
    }

    @Override
    public TblHireclaim performActionOnHire(String hireCode, String toStatus, TblUser tblUser) {
        TblHireclaim tblHireclaim = tblHireClaimRepo.findByHirecode(Long.parseLong(hireCode));
        int update = tblHireClaimRepo.performAction(toStatus, tblUser.getUsercode(), Long.valueOf(hireCode));
        TblRtastatus tblRtastatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);
        if (update > 0) {
            TblHireclaim hireclaim = tblHireClaimRepo.findById(Long.valueOf(hireCode)).orElse(null);
            if (toStatus.equals("155")) {
                Date clawbackDate = addDaysToDate(90);
                tblHireClaimRepo.updateClawbackdate(clawbackDate, Long.valueOf(hireCode));
                tblHireClaimRepo.updateSubmitDate(new Date(), Long.valueOf(hireCode));
            }
            TblHirelog tblHirelog = new TblHirelog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);


            tblHirelog.setCreatedon(new Date());
            tblHirelog.setDescr(tblStatus.getDescr());
            tblHirelog.setUsercode(tblUser.getUsercode());
            tblHirelog.setTblHireclaim(hireclaim);
            tblHirelog.setNewstatus(Long.valueOf(toStatus));
            tblHirelog.setOldstatus(tblHireclaim.getTblRtastatus().getStatuscode());

            tblHirelog = tblHirelogRepo.save(tblHirelog);
            String[] introducerEmailStatus = {
                    "38", "30", "322", "323", "324", "325", "326", "327", "328", "329", "330", "331",
                    "37", "301", "302", "300", "312", "313", "314", "315", "316", "317", "318", "319", "320", "321",
                    "22", "23", "24"
            };

            if (Arrays.asList(introducerEmailStatus).contains(toStatus)) {

                TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblHireclaim.getAdvisor()));
                TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("hire-status");

                // for introducer
                String introducerbody = tblEmailTemplate.getEmailtemplate();

                introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
                introducerbody = introducerbody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
                introducerbody = introducerbody.replace("[CLIENT_NAME]",
                        tblHireclaim.getFirstname() + " "
                                + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ")
                                + tblHireclaim.getLastname());
                introducerbody = introducerbody.replace("[STATUS_DESCR]", tblRtastatus.getDescr());
                introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());

                saveEmail(introducerUser.getUsername(), introducerbody,
                        tblHireclaim.getHirenumber() + " | Processing Note", tblHireclaim, introducerUser);
            }


            return hireclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblHirebusinessdetail addTblHireBusinessDetail(TblHirebusinessdetail tblHirebusinessdetail) {
        return tblHirebusinessdetailRepo.save(tblHirebusinessdetail);
    }

    @Override
    public List<TblHirebusiness> getHireBusinessesAgainstHireCode(long hirecode) {
        List<TblHirebusiness> tblHirebusinesses = tblHirebusinessRepo.findByTblHireclaimHirecode(hirecode);
        for (TblHirebusiness tblHirebusiness : tblHirebusinesses) {
            TblUser tblUser = tblUsersRepo.findById(tblHirebusiness.getStatususer()).orElse(null);
            tblHirebusiness.setStatususername(tblUser.getUsername());
        }
        return tblHirebusinessRepo.findByTblHireclaimHirecode(hirecode);
    }

    @Override
    public List<TblHiredocument> getHireCasedocuments(long hireCode) {
        return tblHiredocumentRepo.findByTblHireclaimHirecode(hireCode);
    }

    @Override
    public ViewRtaclamin copyHireToRta(String rtaCode) {
        Session session = em.unwrap(Session.class);
        final int[] hireCode = new int[1];
        final int[] status = new int[1];

        session.doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                CallableStatement call = connection.prepareCall("{call COPYHIRETORTA(?,?,?) }");
                call.setLong(1, Long.valueOf(rtaCode));
                call.registerOutParameter(2, Types.INTEGER);
                call.registerOutParameter(3, Types.INTEGER);
                call.execute();
                hireCode[0] = call.getInt(2);
                status[0] = call.getInt(3);
            }
        });

        if (status[0] == 1) {
            return viewRtaclaminRepo.findById(new BigDecimal(hireCode[0])).orElse(null);
        } else {
            return null;
        }
    }

    @Override
    public List<HireCaseList> getAuthHireCasesIntroducers(String usercode) {
        List<HireCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListForIntroducers = tblHireClaimRepo.getHireCasesListForIntroducers(usercode);
        HireCaseList immigrationCaseList;
        if (DbCasesListForIntroducers != null && DbCasesListForIntroducers.size() > 0) {
            for (Object record : DbCasesListForIntroducers) {
                immigrationCaseList = new HireCaseList();
                Object[] row = (Object[]) record;

                immigrationCaseList.setHirecode((BigDecimal) row[0]);
                immigrationCaseList.setCreated((String) row[1]);
                immigrationCaseList.setCode((String) row[2]);
                immigrationCaseList.setClient((String) row[3]);
                immigrationCaseList.setTaskDue((String) row[4]);
                immigrationCaseList.setTaskName((String) row[5]);
                immigrationCaseList.setStatus((String) row[6]);
                immigrationCaseList.setEmail((String) row[7]);
                immigrationCaseList.setAddress((String) row[8]);
                immigrationCaseList.setContactNo((String) row[9]);
                immigrationCaseList.setLastUpdated((Date) row[10]);
                immigrationCaseList.setIntroducer((String) row[11]);
                immigrationCaseList.setLastNote((Date) row[12]);
                immigrationCaseList.setContactDue((Date) row[13]);
                immigrationCaseList.setLandline((String) row[14]);
                immigrationCaseList.setDescription((String) row[15]);
                immigrationCaseList.setInjurydescription((String) row[16]);
                immigrationCaseList.setAccdate((Date) row[17]);
                immigrationCaseList.setAcctime((String) row[18]);
                DbCaseLists.add(immigrationCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<HireCaseList> getAuthHireCasesSolicitors(String usercode) {
        List<HireCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListForSolicitor = tblHireClaimRepo.getHireCasesListForSolicitor(usercode);
        HireCaseList immigrationCaseList;
        if (DbCasesListForSolicitor != null && DbCasesListForSolicitor.size() > 0) {
            for (Object record : DbCasesListForSolicitor) {
                immigrationCaseList = new HireCaseList();
                Object[] row = (Object[]) record;

                immigrationCaseList.setHirecode((BigDecimal) row[0]);
                immigrationCaseList.setCreated((String) row[1]);
                immigrationCaseList.setCode((String) row[2]);
                immigrationCaseList.setClient((String) row[3]);
                immigrationCaseList.setTaskDue((String) row[4]);
                immigrationCaseList.setTaskName((String) row[5]);
                immigrationCaseList.setStatus((String) row[6]);
                immigrationCaseList.setEmail((String) row[7]);
                immigrationCaseList.setAddress((String) row[8]);
                immigrationCaseList.setContactNo((String) row[9]);
                immigrationCaseList.setLastUpdated((Date) row[10]);
                immigrationCaseList.setIntroducer((String) row[11]);
                immigrationCaseList.setLastNote((Date) row[12]);
                immigrationCaseList.setContactDue((Date) row[13]);
                immigrationCaseList.setLandline((String) row[14]);
                immigrationCaseList.setDescription((String) row[15]);
                immigrationCaseList.setInjurydescription((String) row[16]);
                immigrationCaseList.setAccdate((Date) row[17]);
                immigrationCaseList.setAcctime((String) row[18]);
                DbCaseLists.add(immigrationCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<HireCaseList> getAuthHireCasesLegalAssist() {
        List<HireCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesList = tblHireClaimRepo.getHireCasesList();
        HireCaseList immigrationCaseList;
        if (DbCasesList != null && DbCasesList.size() > 0) {
            for (Object record : DbCasesList) {
                immigrationCaseList = new HireCaseList();
                Object[] row = (Object[]) record;

                immigrationCaseList.setHirecode((BigDecimal) row[0]);
                immigrationCaseList.setCreated((String) row[1]);
                immigrationCaseList.setCode((String) row[2]);
                immigrationCaseList.setClient((String) row[3]);
                immigrationCaseList.setTaskDue((String) row[4]);
                immigrationCaseList.setTaskName((String) row[5]);
                immigrationCaseList.setStatus((String) row[6]);
                immigrationCaseList.setEmail((String) row[7]);
                immigrationCaseList.setAddress((String) row[8]);
                immigrationCaseList.setContactNo((String) row[9]);
                immigrationCaseList.setLastUpdated((Date) row[10]);
                immigrationCaseList.setIntroducer((String) row[11]);
                immigrationCaseList.setLastNote((Date) row[12]);
                immigrationCaseList.setContactDue((Date) row[13]);
                immigrationCaseList.setLandline((String) row[14]);
                immigrationCaseList.setDescription((String) row[15]);
                immigrationCaseList.setInjurydescription((String) row[16]);
                immigrationCaseList.setAccdate((Date) row[17]);
                immigrationCaseList.setAcctime((String) row[18]);
                DbCaseLists.add(immigrationCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<ImigrationStatusCountList> getAllHireStatusCountsForIntroducers(String companyCode) {
        List<ImigrationStatusCountList> DbStatusCountLists = new ArrayList<>();
        List<Object> DbStatusCountListObject = tblHireClaimRepo.getAllHireStatusCountsForIntroducers(companyCode);
        ImigrationStatusCountList DbStatusCountList;
        if (DbStatusCountListObject != null && DbStatusCountListObject.size() > 0) {
            for (Object record : DbStatusCountListObject) {
                DbStatusCountList = new ImigrationStatusCountList();
                Object[] row = (Object[]) record;

                DbStatusCountList.setStatusCount((BigDecimal) row[0]);
                DbStatusCountList.setStatusName((String) row[1]);
                DbStatusCountList.setStatusCode((BigDecimal) row[2]);

                DbStatusCountLists.add(DbStatusCountList);
            }
        }
        if (DbStatusCountLists != null && DbStatusCountLists.size() > 0) {
            return DbStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<ImigrationStatusCountList> getAllHireStatusCountsForSolicitor(String companyCode) {
        List<ImigrationStatusCountList> DbStatusCountLists = new ArrayList<>();
        List<Object> DbStatusCountListObject = tblHireClaimRepo.getAllHireStatusCountsForSolicitors(companyCode);
        ImigrationStatusCountList DbStatusCountList;
        if (DbStatusCountListObject != null && DbStatusCountListObject.size() > 0) {
            for (Object record : DbStatusCountListObject) {
                DbStatusCountList = new ImigrationStatusCountList();
                Object[] row = (Object[]) record;

                DbStatusCountList.setStatusCount((BigDecimal) row[0]);
                DbStatusCountList.setStatusName((String) row[1]);
                DbStatusCountList.setStatusCode((BigDecimal) row[2]);

                DbStatusCountLists.add(DbStatusCountList);
            }
        }
        if (DbStatusCountLists != null && DbStatusCountLists.size() > 0) {
            return DbStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<ImigrationStatusCountList> getAllHireStatusCounts() {
        List<ImigrationStatusCountList> DbStatusCountLists = new ArrayList<>();
        List<Object> DbStatusCountListObject = tblHireClaimRepo.getAllHireStatusCounts();
        ImigrationStatusCountList DbStatusCountList;
        if (DbStatusCountListObject != null && DbStatusCountListObject.size() > 0) {
            for (Object record : DbStatusCountListObject) {
                DbStatusCountList = new ImigrationStatusCountList();
                Object[] row = (Object[]) record;

                DbStatusCountList.setStatusCount((BigDecimal) row[0]);
                DbStatusCountList.setStatusName((String) row[1]);
                DbStatusCountList.setStatusCode((BigDecimal) row[2]);

                DbStatusCountLists.add(DbStatusCountList);
            }
        }
        if (DbStatusCountLists != null && DbStatusCountLists.size() > 0) {
            return DbStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<HireCaseList> getHireCasesByStatus(long statusId) {
        List<HireCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListObject = tblHireClaimRepo.getHireCasesListStatusWise(statusId);
        HireCaseList immigrationCaseList;
        if (DbCasesListObject != null && DbCasesListObject.size() > 0) {
            for (Object record : DbCasesListObject) {
                immigrationCaseList = new HireCaseList();
                Object[] row = (Object[]) record;

                immigrationCaseList.setHirecode((BigDecimal) row[0]);
                immigrationCaseList.setCreated((String) row[1]);
                immigrationCaseList.setCode((String) row[2]);
                immigrationCaseList.setClient((String) row[3]);
                immigrationCaseList.setTaskDue((String) row[4]);
                immigrationCaseList.setTaskName((String) row[5]);
                immigrationCaseList.setStatus((String) row[6]);
                immigrationCaseList.setEmail((String) row[7]);
                immigrationCaseList.setAddress((String) row[8]);
                immigrationCaseList.setContactNo((String) row[9]);
                immigrationCaseList.setLastUpdated((Date) row[10]);
                immigrationCaseList.setIntroducer((String) row[11]);
                immigrationCaseList.setLastNote((Date) row[12]);
                immigrationCaseList.setContactDue((Date) row[13]);
                immigrationCaseList.setLandline((String) row[14]);
                immigrationCaseList.setDescription((String) row[15]);
                immigrationCaseList.setInjurydescription((String) row[16]);
                immigrationCaseList.setAccdate((Date) row[17]);
                immigrationCaseList.setAcctime((String) row[18]);

                DbCaseLists.add(immigrationCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public TblHirebusiness updateHirebusiness(TblHirebusiness tblHirebusiness) {
        return tblHirebusinessRepo.saveAndFlush(tblHirebusiness);
    }


    @Override
    public List<HireCaseList> getAuthHireCasesIntroducersByStatus(long statusCode, String companyCode) {
        List<HireCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListForIntroducers = tblHireClaimRepo.getHireCasesListForIntroducers(companyCode, statusCode);
        HireCaseList immigrationCaseList;
        if (DbCasesListForIntroducers != null && DbCasesListForIntroducers.size() > 0) {
            for (Object record : DbCasesListForIntroducers) {
                immigrationCaseList = new HireCaseList();
                Object[] row = (Object[]) record;

                immigrationCaseList.setHirecode((BigDecimal) row[0]);
                immigrationCaseList.setCreated((String) row[1]);
                immigrationCaseList.setCode((String) row[2]);
                immigrationCaseList.setClient((String) row[3]);
                immigrationCaseList.setTaskDue((String) row[4]);
                immigrationCaseList.setTaskName((String) row[5]);
                immigrationCaseList.setStatus((String) row[6]);
                immigrationCaseList.setEmail((String) row[7]);
                immigrationCaseList.setAddress((String) row[8]);
                immigrationCaseList.setContactNo((String) row[9]);
                immigrationCaseList.setLastUpdated((Date) row[10]);
                immigrationCaseList.setIntroducer((String) row[11]);
                immigrationCaseList.setLastNote((Date) row[12]);
                immigrationCaseList.setContactDue((Date) row[13]);
                immigrationCaseList.setLandline((String) row[14]);
                immigrationCaseList.setDescription((String) row[15]);
                immigrationCaseList.setInjurydescription((String) row[16]);
                immigrationCaseList.setAccdate((Date) row[17]);
                immigrationCaseList.setAcctime((String) row[18]);
                DbCaseLists.add(immigrationCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<HireCaseList> getAuthHireCasesSolicitorsByStatus(long statusCode, String companyCode) {
        List<HireCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesListForSolicitor = tblHireClaimRepo.getHireCasesListForSolicitor(companyCode, statusCode);
        HireCaseList immigrationCaseList;
        if (DbCasesListForSolicitor != null && DbCasesListForSolicitor.size() > 0) {
            for (Object record : DbCasesListForSolicitor) {
                immigrationCaseList = new HireCaseList();
                Object[] row = (Object[]) record;

                immigrationCaseList.setHirecode((BigDecimal) row[0]);
                immigrationCaseList.setCreated((String) row[1]);
                immigrationCaseList.setCode((String) row[2]);
                immigrationCaseList.setClient((String) row[3]);
                immigrationCaseList.setTaskDue((String) row[4]);
                immigrationCaseList.setTaskName((String) row[5]);
                immigrationCaseList.setStatus((String) row[6]);
                immigrationCaseList.setEmail((String) row[7]);
                immigrationCaseList.setAddress((String) row[8]);
                immigrationCaseList.setContactNo((String) row[9]);
                immigrationCaseList.setLastUpdated((Date) row[10]);
                immigrationCaseList.setIntroducer((String) row[11]);
                immigrationCaseList.setLastNote((Date) row[12]);
                immigrationCaseList.setContactDue((Date) row[13]);
                immigrationCaseList.setLandline((String) row[14]);
                immigrationCaseList.setDescription((String) row[15]);
                immigrationCaseList.setInjurydescription((String) row[16]);
                immigrationCaseList.setAccdate((Date) row[17]);
                immigrationCaseList.setAcctime((String) row[18]);
                DbCaseLists.add(immigrationCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<HireCaseList> getAuthHireCasesLegalAssistByStatus(long status) {
        List<HireCaseList> DbCaseLists = new ArrayList<>();
        List<Object> DbCasesList = tblHireClaimRepo.getHireCasesList(status);
        HireCaseList immigrationCaseList;
        if (DbCasesList != null && DbCasesList.size() > 0) {
            for (Object record : DbCasesList) {
                immigrationCaseList = new HireCaseList();
                Object[] row = (Object[]) record;

                immigrationCaseList.setHirecode((BigDecimal) row[0]);
                immigrationCaseList.setCreated((String) row[1]);
                immigrationCaseList.setCode((String) row[2]);
                immigrationCaseList.setClient((String) row[3]);
                immigrationCaseList.setTaskDue((String) row[4]);
                immigrationCaseList.setTaskName((String) row[5]);
                immigrationCaseList.setStatus((String) row[6]);
                immigrationCaseList.setEmail((String) row[7]);
                immigrationCaseList.setAddress((String) row[8]);
                immigrationCaseList.setContactNo((String) row[9]);
                immigrationCaseList.setLastUpdated((Date) row[10]);
                immigrationCaseList.setIntroducer((String) row[11]);
                immigrationCaseList.setLastNote((Date) row[12]);
                immigrationCaseList.setContactDue((Date) row[13]);
                immigrationCaseList.setLandline((String) row[14]);
                immigrationCaseList.setDescription((String) row[15]);
                immigrationCaseList.setInjurydescription((String) row[16]);
                immigrationCaseList.setAccdate((Date) row[17]);
                immigrationCaseList.setAcctime((String) row[18]);
                DbCaseLists.add(immigrationCaseList);
            }
        }
        if (DbCaseLists != null && DbCaseLists.size() > 0) {
            return DbCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblHireclaim tblHireclaim, TblUser tblUser) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblHiremessage tblHiremessage = new TblHiremessage();

        tblHiremessage.setTblHireclaim(tblHireclaim);
        tblHiremessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblHiremessage.setCreatedon(new Date());
        tblHiremessage.setMessage(tblEmail.getEmailbody());
        tblHiremessage.setSentto(tblEmail.getEmailaddress());
        tblHiremessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());

        tblHiremessage = tblHiremessageRepo.save(tblHiremessage);

        return true;
    }


    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblHireclaim tblHireclaim,
                             TblUser tblUser, String attachment) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setEmailattachment(attachment);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblHiremessage tblHiremessage = new TblHiremessage();

        tblHiremessage.setTblHireclaim(tblHireclaim);
        tblHiremessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblHiremessage.setCreatedon(new Date());
        tblHiremessage.setMessage(tblEmail.getEmailbody());
        tblHiremessage.setSentto(tblEmail.getEmailaddress());
        tblHiremessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());

        tblHiremessage = tblHiremessageRepo.save(tblHiremessage);

        return true;
    }

    @Override
    public TblHireclaim performRejectOrCancelAction(String hireclaimcode, String toStatus, String reason, TblUser tblUser) {
        List<TblHirebusiness> tblHirebusinesses = tblHirebusinessRepo.findByTblHireclaimHirecode(Long.valueOf(hireclaimcode));
        TblHireclaim tblHireclaimOld = tblHireClaimRepo.findByHirecode(Long.parseLong(hireclaimcode));
        if (tblHirebusinesses != null && tblHirebusinesses.size() > 0) {
            // if Case has Hire Buisness
            for (TblHirebusiness tblHirebusiness : tblHirebusinesses) {
                tblHirebusiness.setStatus("C");
//                tblHirebusiness.setStatususer(tblUser.getUsercode());
                tblHirebusinessRepo.saveAndFlush(tblHirebusiness);
            }
            int update = tblHireClaimRepo.performAction(toStatus, tblUser.getUsercode(), Long.valueOf(hireclaimcode));

            TblHirenote tblHirenote = new TblHirenote();
            TblHireclaim tblHireclaim = new TblHireclaim();

            tblHireclaim.setHirecode(Long.valueOf(hireclaimcode));
            tblHirenote.setTblHireclaim(tblHireclaim);
            tblHirenote.setNote("Case Has Been Rejected By Reason : " + reason);
            tblHirenote.setUsercategorycode("4");
            tblHirenote.setCreatedon(new Date());
            tblHirenote.setUsercode(tblUser.getUsercode());

            addTblHireNote(tblHirenote);


        } else {
            int update = tblHireClaimRepo.performAction(toStatus, tblUser.getUsercode(), Long.valueOf(hireclaimcode));

            TblHirenote tblHirenote = new TblHirenote();
            TblHireclaim tblHireclaim = new TblHireclaim();

            tblHireclaim.setHirecode(Long.valueOf(hireclaimcode));
            tblHirenote.setTblHireclaim(tblHireclaim);
            tblHirenote.setNote("Case Has Been Rejected By Reason : " + reason);
            tblHirenote.setUsercategorycode("4");
            tblHirenote.setCreatedon(new Date());
            tblHirenote.setUsercode(tblUser.getUsercode());

            addTblHireNote(tblHirenote);
        }

        TblHirelog tblHirelog = new TblHirelog();
        TblRtastatus tblStatus = getTblRtaStatus(Long.valueOf(toStatus));
        tblHirelog.setCreatedon(new Date());
        tblHirelog.setDescr(tblStatus.getDescr());
        tblHirelog.setUsercode(tblUser.getUsercode());
        tblHirelog.setTblHireclaim(tblHireclaimOld);
        tblHirelog.setNewstatus(Long.valueOf(toStatus));
        tblHirelog.setOldstatus(tblHireclaimOld.getTblRtastatus().getStatuscode());
        tblHirelog = saveTblHireLogs(tblHirelog);

        return tblHireClaimRepo.findByHirecode(Long.valueOf(hireclaimcode));
    }

    @Override
    public List<TblHirenote> getAuthHdrCaseNotesOfLegalInternal(String hirecode, TblUser tblUser) {
        List<TblHirenote> tblHirenotes = tblHirenoteRepo.getAuthRtaCaseNotesOfLegalInternal(Long.valueOf(hirecode),
                tblUser.getUsercode());
        if (tblHirenotes != null && tblHirenotes.size() > 0) {
            for (TblHirenote tblHirenote : tblHirenotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblHirenote.getUsercode()).orElse(null);
                tblHirenote.setUserName(tblUser1.getLoginid());
                tblHirenote.setSelf(tblHirenote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
            }

            return tblHirenotes;
        } else {
            return null;
        }
    }

    @Override
    public List<ViewHirecasereport> hireCaseReport(HdrCaseReportRequest hireCaseReportRequest) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
        Date fromDate = formatter.parse(hireCaseReportRequest.getFromDate());
        Date toDate = formatter.parse(hireCaseReportRequest.getToDate());

        String dateFrom = formatter1.format(fromDate);
        String dateTo = formatter1.format(toDate);
        return viewHirecasereportRepo.viewReport(dateTo, dateFrom);
    }

    @Override
    public List<HireCaseList> getFilterAuthHireCasesIntroducers(FilterRequest filterRequest, String usercode) {
        List<HireCaseList> hireCaseLists = new ArrayList<>();
        List<Object> filterHireCasesListForIntroducers = tblHireClaimRepo.getFilterHireCasesListForIntroducers(filterRequest.getName(), filterRequest.getPostCode(), filterRequest.getPortalCode(),
                filterRequest.getMobileNo(), filterRequest.getEmail(), filterRequest.getNiNumber(), filterRequest.getRegNo(), usercode);
        HireCaseList hireCaseList;
        if (filterHireCasesListForIntroducers != null && filterHireCasesListForIntroducers.size() > 0) {
            for (Object record : filterHireCasesListForIntroducers) {
                hireCaseList = new HireCaseList();
                Object[] row = (Object[]) record;

                hireCaseList.setHirecode((BigDecimal) row[0]);
                hireCaseList.setCreated((String) row[1]);
                hireCaseList.setCode((String) row[2]);
                hireCaseList.setClient((String) row[3]);
                hireCaseList.setTaskDue((String) row[4]);
                hireCaseList.setTaskName((String) row[5]);
                hireCaseList.setStatus((String) row[6]);
                hireCaseList.setEmail((String) row[7]);
                hireCaseList.setAddress((String) row[8]);
                hireCaseList.setContactNo((String) row[9]);
                hireCaseList.setLastUpdated((Date) row[10]);
                hireCaseList.setIntroducer((String) row[11]);
                hireCaseList.setLastNote((Date) row[12]);
                hireCaseList.setContactDue((Date) row[13]);
                hireCaseList.setLandline((String) row[14]);
                hireCaseList.setDescription((String) row[15]);
                hireCaseList.setInjurydescription((String) row[16]);
                hireCaseList.setAccdate((Date) row[17]);
                hireCaseList.setAcctime((String) row[18]);
                hireCaseLists.add(hireCaseList);
            }
        }
        if (hireCaseLists != null && hireCaseLists.size() > 0) {
            return hireCaseLists;
        } else {
            return null;
        }

    }

    @Override
    public List<HireCaseList> getFilterAuthHireCasesSolicitors(FilterRequest filterRequest, String usercode) {
        List<HireCaseList> hireCaseLists = new ArrayList<>();
        List<Object> filterHireCasesListForIntroducers = tblHireClaimRepo.getFilterHireCasesListForSolicitor(filterRequest.getName(), filterRequest.getPostCode(), filterRequest.getPortalCode(),
                filterRequest.getMobileNo(), filterRequest.getEmail(), filterRequest.getNiNumber(), filterRequest.getRegNo(), usercode);
        HireCaseList hireCaseList;
        if (filterHireCasesListForIntroducers != null && filterHireCasesListForIntroducers.size() > 0) {
            for (Object record : filterHireCasesListForIntroducers) {
                hireCaseList = new HireCaseList();
                Object[] row = (Object[]) record;

                hireCaseList.setHirecode((BigDecimal) row[0]);
                hireCaseList.setCreated((String) row[1]);
                hireCaseList.setCode((String) row[2]);
                hireCaseList.setClient((String) row[3]);
                hireCaseList.setTaskDue((String) row[4]);
                hireCaseList.setTaskName((String) row[5]);
                hireCaseList.setStatus((String) row[6]);
                hireCaseList.setEmail((String) row[7]);
                hireCaseList.setAddress((String) row[8]);
                hireCaseList.setContactNo((String) row[9]);
                hireCaseList.setLastUpdated((Date) row[10]);
                hireCaseList.setIntroducer((String) row[11]);
                hireCaseList.setLastNote((Date) row[12]);
                hireCaseList.setContactDue((Date) row[13]);
                hireCaseList.setLandline((String) row[14]);
                hireCaseList.setDescription((String) row[15]);
                hireCaseList.setInjurydescription((String) row[16]);
                hireCaseList.setAccdate((Date) row[17]);
                hireCaseList.setAcctime((String) row[18]);
                hireCaseLists.add(hireCaseList);
            }
        }
        if (hireCaseLists != null && hireCaseLists.size() > 0) {
            return hireCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<HireCaseList> getFilterAuthHireCasesLegalAssist(FilterRequest filterRequest) {
        List<HireCaseList> hireCaseLists = new ArrayList<>();
        List<Object> filterHireCasesListForIntroducers = tblHireClaimRepo.getFilterHireCasesList(filterRequest.getName(), filterRequest.getPostCode(), filterRequest.getPortalCode(),
                filterRequest.getMobileNo(), filterRequest.getEmail(), filterRequest.getNiNumber(), filterRequest.getRegNo());
        HireCaseList hireCaseList;
        if (filterHireCasesListForIntroducers != null && filterHireCasesListForIntroducers.size() > 0) {
            for (Object record : filterHireCasesListForIntroducers) {
                hireCaseList = new HireCaseList();
                Object[] row = (Object[]) record;

                hireCaseList.setHirecode((BigDecimal) row[0]);
                hireCaseList.setCreated((String) row[1]);
                hireCaseList.setCode((String) row[2]);
                hireCaseList.setClient((String) row[3]);
                hireCaseList.setTaskDue((String) row[4]);
                hireCaseList.setTaskName((String) row[5]);
                hireCaseList.setStatus((String) row[6]);
                hireCaseList.setEmail((String) row[7]);
                hireCaseList.setAddress((String) row[8]);
                hireCaseList.setContactNo((String) row[9]);
                hireCaseList.setLastUpdated((Date) row[10]);
                hireCaseList.setIntroducer((String) row[11]);
                hireCaseList.setLastNote((Date) row[12]);
                hireCaseList.setContactDue((Date) row[13]);
                hireCaseList.setLandline((String) row[14]);
                hireCaseList.setDescription((String) row[15]);
                hireCaseList.setInjurydescription((String) row[16]);
                hireCaseList.setAccdate((Date) row[17]);
                hireCaseList.setAcctime((String) row[18]);
                hireCaseLists.add(hireCaseList);
            }
        }
        if (hireCaseLists != null && hireCaseLists.size() > 0) {
            return hireCaseLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblHiretask> getHireTaskAgainstHireCode(long hireclaimcode) {
        return tblHiretaskRepo.findByTblHireclaimHirecode(hireclaimcode);
    }

    @Override
    public List<TblTask> getTasksForHire() {
        return tblTaskRepo.findTaskById("2");
    }

    @Override
    public List<TblHiretask> saveHireTasks(List<TblHiretask> tblHdrtasks) {
        return tblHiretaskRepo.saveAll(tblHdrtasks);
    }

    @Override
    public List<TblHirebusiness> getHireBusinessByStatus(long hireClaimCode, String status) {
        return tblHirebusinessRepo.findByStatusAndTblHireclaimHirecode(status, hireClaimCode);
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public TblHiretask getHireTaskByHireCodeAndTaskCode(long hirecode, long taskCode) {
        return tblHiretaskRepo.findByTblHireclaimHirecodeAndTblTaskTaskcode(hirecode, taskCode);
    }

    @Override
    public TblHiretask updateTblHireTask(TblHiretask tblHiretask) {
        return tblHiretaskRepo.saveAndFlush(tblHiretask);
    }

    @Override
    public TblCompanydoc getHireCompanyDocs(String companycode, String compaignCode) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndTblCompaignCompaigncode(compaignCode, compaignCode);
    }

    @Override
    public String getHireDbColumnValue(String key, long hirecode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from TBL_HIRECLAIM  WHERE HIRECODE = " + hirecode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT FIRSTNAME || ' ' || NVL(MIDDLENAME,'') || ' ' || NVL(LASTNAME, '') from TBL_HIRECLAIM  WHERE HIRECODE = "
                    + hirecode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT POSTALCODE || ' ' || ADDRESS1 || ' ' || ADDRESS2  || ' ' || ADDRESS3 || ' ' || CITY || ' ' || REGION from TBL_HIRECLAIM  WHERE HIRECODE = "
                    + hirecode;
            query = em.createNativeQuery(sql);
        } else {
            String sql = "SELECT " + key + " from TBL_HIRECLAIM  WHERE HIRECODE = " + hirecode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")

        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    @Override
    public TblHiredocument addHireDocumentSingle(TblHiredocument tblHiredocument) {
        return tblHiredocumentRepo.save(tblHiredocument);
    }

    @Override
    public void sendHireEmailToAll(TblHireclaim tblHireclaim, TblHirebusiness tblHirebusiness) {
        TblRtastatus tblRtastatus = tblRtastatusRepo.findById(tblHireclaim.getTblRtastatus().getStatuscode()).orElse(null);

        TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(tblHireclaim.getAdvisor()));
        TblUser solicitorUser = tblUsersRepo
                .findByUsercode(tblHirebusiness.getStatususer());
        TblUser legalUser = tblUsersRepo.findByUsercode("4");
        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("hire-status");

        // for introducer
        String introducerbody = tblEmailTemplate.getEmailtemplate();

        introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
        introducerbody = introducerbody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
        introducerbody = introducerbody.replace("[CLIENT_NAME]",
                tblHireclaim.getFirstname() + " "
                        + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ")
                        + tblHireclaim.getLastname());
        introducerbody = introducerbody.replace("[STATUS_DESCR]", tblRtastatus.getDescr());
        introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());

        saveEmail(introducerUser.getUsername(), introducerbody,
                tblHireclaim.getHirenumber() + " | Processing Note", tblHireclaim, introducerUser);

        // for solicitor
        String solicitorbody = tblEmailTemplate.getEmailtemplate();

        solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
        solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
        solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                tblHireclaim.getFirstname() + " "
                        + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ")
                        + tblHireclaim.getLastname());
        solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblRtastatus.getDescr());
        solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());

        saveEmail(solicitorUser.getUsername(), solicitorbody, tblHireclaim.getHirenumber() + " | Processing Note",
                tblHireclaim, solicitorUser);

        // for legal internal
        String legalbody = tblEmailTemplate.getEmailtemplate();

        legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
        legalbody = legalbody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
        legalbody = legalbody.replace("[CLIENT_NAME]",
                tblHireclaim.getFirstname() + " "
                        + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ")
                        + tblHireclaim.getLastname());
        legalbody = legalbody.replace("[STATUS_DESCR]", tblRtastatus.getDescr());
        legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());

        saveEmail(legalUser.getUsername(), legalbody,
                tblHireclaim.getHirenumber() + " | Processing Note", tblHireclaim, legalUser);
    }

    @Override
    public List<RtaAuditLogResponse> getHireAuditLogs(String hirecasecode) {
        List<RtaAuditLogResponse> rtaAuditLogResponses = new ArrayList<>();
        RtaAuditLogResponse hdrAuditLogResponse = null;
        List<Object> auditlogsObject = tblHireClaimRepo.getHireAuditLogs(Long.valueOf(hirecasecode));
        if (auditlogsObject != null && auditlogsObject.size() > 0) {
            for (Object record : auditlogsObject) {
                hdrAuditLogResponse = new RtaAuditLogResponse();
                Object[] row = (Object[]) record;

                hdrAuditLogResponse.setFieldName((String) row[0]);
                hdrAuditLogResponse.setOldValue((String) row[1]);
                hdrAuditLogResponse.setNewValue((String) row[2]);
                hdrAuditLogResponse.setAuditDate((Date) row[3]);
                hdrAuditLogResponse.setLoggedUser((String) row[4]);

                rtaAuditLogResponses.add(hdrAuditLogResponse);
            }
            return rtaAuditLogResponses;
        } else {
            return null;
        }
    }

    @Override
    public TblHireclaim getTblHireClaimByRefNo(String claimRefNo) {
        return tblHireClaimRepo.findByHirenumber(claimRefNo);
    }

    @Override
    public List<RtaDuplicatesResponse> getHireDuplicates(String hirecasecode) {
        TblHireclaim tblHireclaim = tblHireClaimRepo.findById(Long.valueOf(hirecasecode)).orElse(null);
        if (tblHireclaim != null) {
            List<RtaDuplicatesResponse> rtaDuplicatesResponses = new ArrayList<>();
            RtaDuplicatesResponse rtaDuplicatesResponse = new RtaDuplicatesResponse();

            List<Object> vehicalDuplicates = tblHireClaimRepo.duplicatesForVehicle(tblHireclaim.getRegisterationno());
            List<Object> mobileDuplicates = tblHireClaimRepo.duplicatesForMobile(tblHireclaim.getMobile());
            List<Object> partyRegNoDuplicates = tblHireClaimRepo.duplicatesForMobile(tblHireclaim.getMobile());

            if (tblHireclaim.getNinumber() != null && !tblHireclaim.getNinumber().equals("Will be provided to the solicitor")) {
                List<Object> niDuplicates = tblHireClaimRepo.duplicatesForNiNumber(tblHireclaim.getNinumber());

                if (niDuplicates != null && niDuplicates.size() > 0) {
                    for (Object record : niDuplicates) {
                        rtaDuplicatesResponse = new RtaDuplicatesResponse();
                        Object[] row = (Object[]) record;

                        rtaDuplicatesResponse.setDuplicateType("Ni-Number");
                        rtaDuplicatesResponse.setCaseNumber((String) row[1]);
                        rtaDuplicatesResponse.setFullName((String) row[2]);
                        rtaDuplicatesResponse.setHirecode(((BigDecimal) row[3]).toString());

                        rtaDuplicatesResponses.add(rtaDuplicatesResponse);

                    }
                }
            }

            if (vehicalDuplicates != null && vehicalDuplicates.size() > 0) {
                for (Object record : vehicalDuplicates) {
                    rtaDuplicatesResponse = new RtaDuplicatesResponse();
                    Object[] row = (Object[]) record;

                    rtaDuplicatesResponse.setDuplicateType("VRN");
                    rtaDuplicatesResponse.setCaseNumber((String) row[1]);
                    rtaDuplicatesResponse.setFullName((String) row[2]);
                    rtaDuplicatesResponse.setHirecode(((BigDecimal) row[3]).toString());

                    rtaDuplicatesResponses.add(rtaDuplicatesResponse);

                }
            }

			/*if (mobileDuplicates != null && mobileDuplicates.size() > 0) {
				for (Object record : mobileDuplicates) {
					rtaDuplicatesResponse = new RtaDuplicatesResponse();
					Object[] row = (Object[]) record;

					rtaDuplicatesResponse.setDuplicateTyep("MOB");
					rtaDuplicatesResponse.setCaseNumber((String) row[1]);
					rtaDuplicatesResponse.setFullName((String) row[2]);
					rtaDuplicatesResponse.setHirecode(((BigDecimal) row[3]).toString());

					rtaDuplicatesResponses.add(rtaDuplicatesResponse);

				}
			}*/

            if (partyRegNoDuplicates != null && partyRegNoDuplicates.size() > 0) {
                for (Object record : partyRegNoDuplicates) {
                    rtaDuplicatesResponse = new RtaDuplicatesResponse();
                    Object[] row = (Object[]) record;

                    rtaDuplicatesResponse.setDuplicateType("TP-VRN");
                    rtaDuplicatesResponse.setCaseNumber((String) row[1]);
                    rtaDuplicatesResponse.setFullName((String) row[2]);
                    rtaDuplicatesResponse.setHirecode(((BigDecimal) row[3]).toString());

                    rtaDuplicatesResponses.add(rtaDuplicatesResponse);

                }
            }

            return rtaDuplicatesResponses;

        } else {
            return null;
        }
    }

    @Override
    public int updateHireBuisnessDetails(UpdateHireBuisnessDetail updateHireBuisnessDetail) {
        return tblHirebusinessdetailRepo.updateHireBuisnessDetails(updateHireBuisnessDetail.getBookingdate(), updateHireBuisnessDetail.getBookingmode(),
                updateHireBuisnessDetail.getHirestartdate(), updateHireBuisnessDetail.getHireenddate(), updateHireBuisnessDetail.getHirebusinessdetailcode());

    }

    @Override
    public TblRtastatus getTblRtaStatus(Long statusCode) {
        return tblRtastatusRepo.findById(statusCode).orElse(null);
    }

    @Override
    public TblHireclaim performRevertActionOnHire(String hireclaimcode, String toStatus, String reason, TblUser tblUser) {
        Session session = em.unwrap(Session.class);
        final String[] msg = new String[1];
        final int[] status = new int[1];

        session.doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                CallableStatement call = connection.prepareCall("{call PROC_REVERT(?,?,?,?) }");
                call.setString(1, "HIRE");
                call.setLong(2, Long.parseLong(hireclaimcode));
                call.registerOutParameter(3, Types.INTEGER);
                call.registerOutParameter(4, Types.VARCHAR);
                call.execute();
                status[0] = call.getInt(3);
                msg[0] = call.getString(4);
            }
        });

        if (status[0] == 1) {
            return tblHireClaimRepo.findById(Long.valueOf(hireclaimcode)).orElse(null);
        } else {
            return null;
        }
    }
}
