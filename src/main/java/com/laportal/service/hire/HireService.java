package com.laportal.service.hire;

import com.laportal.dto.*;
import com.laportal.model.*;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface HireService {

    boolean isHireCaseAllowed(String companycode, String campaigncode);

    TblCompanyprofile getCompanyProfile(String companycode);

    int getHireDocNumber();

    TblHireclaim addNewHireCase(TblHireclaim tblHireclaim, List<HireFileUploadRequest> files);

    TblHireclaim getTblHireClaimById(long hirecode);

    TblHireclaim updateHireCase(TblHireclaim tblHireclaim);

    List<TblHiredocument> addHireDocument(List<TblHiredocument> tblHiredocuments);

    List<TblHireclaim> getHireCases(TblUser tblUser);

    TblHireclaim getHireCaseById(long hirecode, TblUser tblUser);

    List<TblHirelog> getHireCaseLogs(String hireCode);

    List<TblHirenote> getHireCaseNotes(String hirecode, TblUser tblUser);

    List<TblHiremessage> getHireCaseMessages(String hirecode);

    TblHirenote addTblHireNote(TblHirenote tblHirenote);

    List<TblCompanyprofile> getAllCompaniesProfile();

    List<TblUser> getAllUsers();

    TblHirebusiness addTblHireBusiness(TblHirebusiness tblHirebusiness);

    TblHirebusiness getHireBusinessAgainstHireAndCompanyCode(long hireCode, String companyCode);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    TblHirelog saveTblHireLogs(TblHirelog tblHirelog);

    TblHireclaim performActionOnHire(String hireCode, String toStatus, TblUser tblUser);

    TblHirebusinessdetail addTblHireBusinessDetail(TblHirebusinessdetail tblHirebusinessdetail);

    List<TblHirebusiness> getHireBusinessesAgainstHireCode(long hirecode);

    List<TblHiredocument> getHireCasedocuments(long hireCode);

    ViewRtaclamin copyHireToRta(String hireCode);

    List<HireCaseList> getAuthHireCasesIntroducers(String usercode);

    List<HireCaseList> getAuthHireCasesSolicitors(String usercode);

    List<HireCaseList> getAuthHireCasesLegalAssist();

    List<ImigrationStatusCountList> getAllHireStatusCountsForIntroducers(String companyCode);

    List<ImigrationStatusCountList> getAllHireStatusCountsForSolicitor(String usercode);

    List<ImigrationStatusCountList> getAllHireStatusCounts();

    List<HireCaseList> getHireCasesByStatus(long statusId);

    TblHirebusiness updateHirebusiness(TblHirebusiness tblHirebusiness);

    List<HireCaseList> getAuthHireCasesIntroducersByStatus(long statusId, String userCode);

    List<HireCaseList> getAuthHireCasesSolicitorsByStatus(long statusId, String userCode);

    List<HireCaseList> getAuthHireCasesLegalAssistByStatus(long statusId);

    TblHireclaim performRejectOrCancelAction(String hireclaimcode, String toStatus,String reason, TblUser tblUser);

    List<TblHirenote> getAuthHdrCaseNotesOfLegalInternal(String hirecode, TblUser tblUser);

    List<ViewHirecasereport> hireCaseReport(HdrCaseReportRequest hireCaseReportRequest) throws ParseException;

    List<HireCaseList> getFilterAuthHireCasesIntroducers(FilterRequest filterRequest, String usercode);

    List<HireCaseList> getFilterAuthHireCasesSolicitors(FilterRequest filterRequest, String usercode);

    List<HireCaseList> getFilterAuthHireCasesLegalAssist(FilterRequest filterRequest);

    List<TblHiretask> getHireTaskAgainstHireCode(long hireclaimcode);

    List<TblTask> getTasksForHire();

    List<TblHiretask> saveHireTasks(List<TblHiretask> tblHdrtasks);

    List<TblHirebusiness> getHireBusinessByStatus(long hireClaimCode, String status);

    TblTask getTaskAgainstCode(long taskCode);

    TblHiretask getHireTaskByHireCodeAndTaskCode(long hirecode, long taskCode);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblHireclaim tblHireclaim, TblUser tblUser);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblHireclaim tblHireclaim,
                      TblUser tblUser, String attachment);

    TblHiretask updateTblHireTask(TblHiretask tblHiretask);

    TblCompanydoc getHireCompanyDocs(String companycode, String compaignCode);

    String getHireDbColumnValue(String key, long hirecode);

    TblHiredocument addHireDocumentSingle(TblHiredocument tblHiredocument);

    void sendHireEmailToAll(TblHireclaim hireclaim, TblHirebusiness tblHirebusiness);

    List<RtaAuditLogResponse> getHireAuditLogs(String hirecasecode);

    TblHireclaim getTblHireClaimByRefNo(String claimRefNo);

    List<RtaDuplicatesResponse> getHireDuplicates(String hirecasecode);

    int updateHireBuisnessDetails(UpdateHireBuisnessDetail updateHireBuisnessDetail);

    TblRtastatus getTblRtaStatus(Long statusCode);

    TblHireclaim performRevertActionOnHire(String hireclaimcode, String toStatus, String reason, TblUser tblUser);
}
