package com.laportal.service.rta;

import com.laportal.dto.*;
import com.laportal.model.*;

import java.text.ParseException;
import java.util.List;

public interface RtaService {

    boolean isRtaCaseAllowed(String companyCode, String compaingnCode);

    TblCompanyprofile getCompanyProfile(String companycode);

    int getRtaDocNumber();

    TblRtaclaim addNewRtaCase(TblRtaclaim tblRtaclaim, List<TblRtapassenger> tblRtapassengers,
                              List<RtaFileUploadRequest> files);

    List<TblRtapassenger> addNewRtaPassengers(TblRtaclaim tblRtaclaim, List<TblRtapassenger> tblRtapassengers);

    TblRtaclaim updateRtaCase(TblRtaclaim tblRtaclaim, List<UpdateRtaFileUploadRequest> files);

    TblRtaclaim updateRtaCase(TblRtaclaim tblRtaclaim);

    List<TblRtapassenger> updateRtaPassengers(TblRtaclaim tblRtaclaim, List<TblRtapassenger> tblRtapassengers);

    List<ViewRtalistsolicitors> getAuthRtaCasesSolicitors(String usercode);

    List<ViewRtalistintorducers> getAuthRtaCasesIntroducers(String usercode);

    List<ViewRtalistintorducers> getAuthRtaCasesLegalAssist();

    List<TblRtapassenger> getAuthRtaCasePassengers(String rtacode);

    ViewRtaclamin getAuthRtaCase(String rtacode, TblUser tblUser) throws ParseException;

    TblRtanote addNoteToRta(TblRtanote tblRtanote);

    List<TblRtanote> getAuthRtaCaseNotes(String rtacode, TblUser tblUser);

    TblRtaclaim performActionOnRta(String rtaCode, String toStatus, TblUser tblUser);

    TblRtaclaim performActionOnRtaFromDirectIntro(PerformHotKeyOnRtaRequest performHotKeyOnRtaRequest, TblUser tblUser);

    List<TblRtalog> getAuthRtaCaseLogs(String rtacode) throws ParseException;

    List<TblRtatask> getAuthRtaCaseTasks(String rtacode);

    List<TblRtadocument> getAuthRtaCasedocuments(String rtacode);

    TblRtaflow addRtaFlow(TblRtaflow tblRtaflow);

    List<TblRtaflow> getWorkFlow();

    TblRtaflowdetail addWorkFlowDetail(TblRtaflowdetail tblRtaflowdetail);

    List<TblRtadocument> addRtaDocument(List<TblRtadocument> tblRtadocuments);

    List<TblRtamessage> getAuthRtaCaseMessages(String rtacode);

    TblRtaclaim getTblRtaclaimById(long rtacode);

    TblCompanyprofile getCompanyProfileAgainstRtaCode(long rtacode);

    String getRtaDbColumnValue(String key, long rtaCode);

    TblRtadocument addRtaDocumentSingle(TblRtadocument tblRtadocument);


    TblRtamessage getTblRtaMessageById(String rtamessagecode);

    TblTask getTaskAgainstCode(long taskCode);

    TblRtatask getRtaTaskAgainstRtaCodeAndTaskCode(long rtaCode, long taskCode);

    TblRtatask saveTblRtaTask(TblRtatask tblRtatask);

    TblEmail saveTblEmail(TblEmail tblEmail);

    int updateTblRtaTask(TblRtatask tblRtatask, long rtaCode);

    List<TblRtatask> getRtaTaskAgainstRtaCodeAndStatus(String rtaCode, String status);

    TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile);

    TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType);

    TblRtaclaim addRtaTaskDocument(List<RtaFileUploadRequest> files, long rtaCode, TblUser tblUser, TblTask tblTask);

    TblHireclaim copyRtaToHire(String rtaCode);

    int updateCurrentTask(long taskCode, long rtaCode, String current);

    List<ViewRtacasereport> getRtaCaseReport(RtaCaseReportRequest rtaCaseReportRequest) throws ParseException;

    TblRtaclaim performRejectCancelActionOnRta(String rtaCode, String toStatus, String reason, TblUser tblUser);

    List<TblRtaclaim> getSubRtaClaims(String rtacasecode);

    List<RtaStatusCountList> getAllRtaStatusCountsForIntroducers(String usercode);

    List<RtaStatusCountList> getAllRtaStatusCountsForSolicitor(String usercode);

    List<RtaStatusCountList> getAllRtaStatusCounts();

    List<TblRtaclaim> getRtaCasesByStatus(long statusId);

    List<ViewRtalistintorducers> getAuthRtaCasesIntroducersStatusWise(String usercode, String statusId);

    List<ViewRtalistsolicitors> getAuthRtaCasesSolicitorsStatusWise(String usercode, String statusId);

    List<ViewRtalistintorducers> getAuthRtaCasesLegalAssistStatusWise(String statusId);

    TblRtaclaim findRtaById(String caseId);

    TblRtasolicitor getRtaSolicitorsOfRtaClaim(String rtacode);

    void deleteRtaDocument(long rtadoccode);

    List<RtaDuplicatesResponse> getRtaDuplicates(String rtacasecode);

    TblRtaclaim performRevertActionOnRta(String rtacode, String toStatus, String reason, TblUser tblUser);

    List<RtaAuditLogResponse> getRtaAuditLogs(String rtacasecode);

    TblEmail resendRtaEmail(String rtamessagecode);

    TblEmailTemplate findByEmailType(String emailType);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblRtaclaim tblRtaclaim, TblUser tblUser);

    TblUser findByUserId(String valueOf);

    List<TblRtainjury> getInjuriesOfRta(long rtacode);

    TblCompanyprofile getCompanyProfileByUser(String usercode);

    List<TblRtanote> getAuthRtaCaseNotesOfLegalInternal(String rtacode, TblUser tblUser);

    List<RtaAuditLogResponse> getTodayRtaAuditLogs(long rtacode);

    boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblRtaclaim tblRtaclaim,
                      TblUser tblUser, String attachment);

    TblCompanydoc getCompanyDocsRtaForBike(String companycode);

    List<ViewRtalistintorducers> getFilterAuthRtaCasesIntroducers(FilterRequest filterRequest, String usercode);

    List<ViewRtalistsolicitors> getFilterAuthRtaCasesSolicitors(FilterRequest filterRequest, String usercode);

    List<ViewRtalistintorducers> getFilterAuthRtaCasesLegalAssist(FilterRequest filterRequest);

    TblRtaclaim getTblRtaclaimByRefNo(String claimRefNo);
}
