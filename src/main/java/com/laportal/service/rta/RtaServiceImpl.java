package com.laportal.service.rta;

import com.laportal.Repo.*;
import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
public class RtaServiceImpl extends AbstractApi implements RtaService {

    @PersistenceContext
    @Autowired
    EntityManager em;

    @Autowired
    private Environment env;
    @Autowired
    private TblCompanyjobsRepo tblCompanyjobsRepo;
    @Autowired
    private TblCompanyprofileRepo tblCompanyprofileRepo;
    @Autowired
    private TblRtaclaimRepo tblRtaclaimRepo;
    @Autowired
    private TblRtapassengerRepo tblRtapassengerRepo;
    @Autowired
    private TblRtadocumentRepo tblRtadocumentRepo;
    @Autowired
    private ViewRtalistSolicitorsRepo viewRtalistSolicitorsRepo;
    @Autowired
    private ViewRtalistintorducersRepo viewRtalistintorducersRepo;
    @Autowired
    private ViewRtaclaminRepo viewRtaclaminRepo;
    @Autowired
    private TblRtanoteRepo tblRtanoteRepo;
    @Autowired
    private TblRtamessageRepo tblRtamessageRepo;
    @Autowired
    private TblRtaflowdetailRepo tblRtaflowdetailRepo;
    @Autowired
    private TblRtalogRepo tblRtalogRepo;
    @Autowired
    private TblRtastatusRepo tblRtastatusRepo;
    @Autowired
    private TblRtasolicitorRepo tblRtasolicitorRepo;
    @Autowired
    private TblRtataskRepo tblRtataskRepo;
    @Autowired
    private TblUsersRepo tblUsersRepo;
    @Autowired
    private TblRtaflowRepo tblRtaflowRepo;
    @Autowired
    private TblEmailRepo tblEmailRepo;
    @Autowired
    private TblTaskRepo tblTaskRepo;
    @Autowired
    private TblCompanydocRepo tblCompanydocRepo;
    @Autowired
    private ViewRtacasereportRepo viewRtacasereportRepo;
    @Autowired
    private TblHireclaimRepo tblHireClaimRepo;
    @Autowired
    private TblEmailTemplateRepo tblEmailTemplateRepo;
    @Autowired
    private TblRtainjuryRepo tblRtainjuryRepo;
    @Autowired
    private TblRtaPassengerInjuryRepo tblRtaPassengerInjuryRepo;
    @Autowired
    private TblSolicitorInvoiceDetailRepo tblSolicitorInvoiceDetailRepo;
    @Autowired
    private TblEsignStatusRepo tblEsignStatusRepo;

    @Override
    public boolean isRtaCaseAllowed(String companyCode, String compaingnCode) {
        List<TblCompanyjob> tblCompanyjobs = tblCompanyjobsRepo.findByCompanycodeAndTblCompaignCompaigncode(companyCode,
                compaingnCode);

        if (tblCompanyjobs != null && tblCompanyjobs.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfile(String companycode) {
        return tblCompanyprofileRepo.findById(companycode).orElse(null);
    }

    @Override
    public int getRtaDocNumber() {
        return tblRtaclaimRepo.getRtaDocNumber();
    }

    @Override
    public TblRtaclaim addNewRtaCase(TblRtaclaim tblRtaclaim, List<TblRtapassenger> tblRtapassengers,
                                     List<RtaFileUploadRequest> files) {

        TblRtaclaim rtaclaim = tblRtaclaimRepo.save(tblRtaclaim);

        if (rtaclaim != null && rtaclaim.getRtacode() > 0) {
            // saving injuries
            for (TblRtainjury tblRtainjury : rtaclaim.getInjclasscodes()) {
                tblRtainjury.setTblRtaclaim(rtaclaim);
            }
            tblRtainjuryRepo.saveAll(rtaclaim.getInjclasscodes());

            if (files != null && files.size() > 0) {
                for (RtaFileUploadRequest rtaFileUploadRequest : files) {
                    TblRtadocument tblRtadocument = new TblRtadocument();
                    String milisecond = String.valueOf(System.currentTimeMillis());
                    tblRtadocument.setDocname(
                            rtaFileUploadRequest.getFileName() + milisecond + "." + rtaFileUploadRequest.getFileExt());
                    tblRtadocument.setDoctype("Image");
                    tblRtadocument.setDocbase64(rtaFileUploadRequest.getFileBase64());
                    tblRtadocument.setTblRtaclaim(tblRtaclaim);
                    tblRtadocument.setCreatedon(new Date());
                    tblRtadocument.setUsercode(tblRtaclaim.getUsercode());
                    tblRtadocument.setTblTask(null);

                    tblRtadocumentRepo.save(tblRtadocument);
                }
            }

            TblRtalog tblRtalog = new TblRtalog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(rtaclaim.getStatuscode())).orElse(null);

            tblRtalog.setCreatedon(new Date());
            tblRtalog.setDescr(tblStatus.getDescr());
            tblRtalog.setUsercode(String.valueOf(rtaclaim.getAdvisor()));
            tblRtalog.setTblRtaclaim(rtaclaim);
            tblRtalog.setOldstatus(Long.valueOf(rtaclaim.getStatuscode()));
            tblRtalog.setNewstatus(null);

            tblRtalog = tblRtalogRepo.save(tblRtalog);

            return rtaclaim;
        } else {
            return null;
        }
    }

    @Override
    public List<TblRtapassenger> addNewRtaPassengers(TblRtaclaim rtaclaim, List<TblRtapassenger> tblRtapassengers) {
        if (rtaclaim != null && rtaclaim.getRtacode() > 0) {
            List<TblRtapassenger> rtapassengers = new ArrayList<>();
            TblRtapassenger rtapassenger = new TblRtapassenger();
            List<TblRtapassenger> rtaPassengers = tblRtapassengerRepo.findByTblRtaclaimRtacode(rtaclaim.getRtacode());
            char alphabet = 'A';
            if (rtaPassengers != null && rtaPassengers.size() > 0) {
                alphabet += rtaPassengers.size();
            }
            for (TblRtapassenger tblRtapassenger : tblRtapassengers) {
                tblRtapassenger.setTblRtaclaim(rtaclaim);

                rtapassenger = tblRtapassengerRepo.save(tblRtapassenger);

                for (TblRtaPassengerInjury tblRtaPassengerInjury : tblRtapassenger.getPasengerTblRtainjuries()) {
                    tblRtaPassengerInjury.setTblRtapassenger(rtapassenger);
                    tblRtaPassengerInjury.setCreatedate(new Date());
                }
                tblRtaPassengerInjuryRepo.saveAll(tblRtapassenger.getPasengerTblRtainjuries());

                ///// creating new cases for passengers in TBL _RTACLAIM///////////
                TblRtaclaim rtaclaimpassenger = new TblRtaclaim();
                rtaclaimpassenger.setAddress1(tblRtapassenger.getAddress1());
                rtaclaimpassenger.setAddress2(tblRtapassenger.getAddress2());
                rtaclaimpassenger.setAddress3(tblRtapassenger.getAddress3());
                rtaclaimpassenger.setCity(tblRtapassenger.getCity());
                rtaclaimpassenger.setCreatedon(new Date());
                rtaclaimpassenger.setDob(tblRtapassenger.getDob());
                rtaclaimpassenger.setDriverpassenger(tblRtapassenger.getDriverpassenger());
                rtaclaimpassenger.setEmail(tblRtapassenger.getEmail());
                rtaclaimpassenger.setEnglishlevel(tblRtapassenger.getEnglishlevel());
                rtaclaimpassenger.setFirstname(tblRtapassenger.getFirstname());
                rtaclaimpassenger.setInjdescription(tblRtapassenger.getInjdescr());
                rtaclaimpassenger.setLandline(tblRtapassenger.getLandline());
                rtaclaimpassenger.setLastname(tblRtapassenger.getLastname());
                rtaclaimpassenger.setMedicalinfo(tblRtapassenger.getMedicalinfo());
                rtaclaimpassenger.setMiddlename(tblRtapassenger.getMiddlename());
                rtaclaimpassenger.setMobile(tblRtapassenger.getMobile());
                rtaclaimpassenger.setNinumber(tblRtapassenger.getNinumber());
                rtaclaimpassenger.setOngoing(tblRtapassenger.getOngoing());
                rtaclaimpassenger.setPostalcode(tblRtapassenger.getPostalcode());
                rtaclaimpassenger.setRegion(tblRtapassenger.getRegion());
                rtaclaimpassenger.setRtalinkcode(rtaclaim.getRtanumber());
                rtaclaimpassenger.setTitle(tblRtapassenger.getTitle());
                rtaclaimpassenger.setAlternativenumber(tblRtapassenger.getAlternativenumber());
                rtaclaimpassenger.setInjlength(tblRtapassenger.getInjlength() == null ? null
                        : new BigDecimal(String.valueOf(tblRtapassenger.getInjlength())));
                rtaclaimpassenger.setMedicalevidence(tblRtapassenger.getEvidencedetails());

                rtaclaimpassenger.setGaddress1(tblRtapassenger.getGaddress1());
                rtaclaimpassenger.setGaddress2(tblRtapassenger.getGaddress2());
                rtaclaimpassenger.setGaddress3(tblRtapassenger.getGaddress3());
                rtaclaimpassenger.setGcity(tblRtapassenger.getGcity());
                rtaclaimpassenger.setGemail(tblRtapassenger.getGemail());
                rtaclaimpassenger.setGfirstname(tblRtapassenger.getGfirstname());
                rtaclaimpassenger.setGlandline(tblRtapassenger.getGlandline());
                rtaclaimpassenger.setGlastname(tblRtapassenger.getGlastname());
                rtaclaimpassenger.setGmiddlename(tblRtapassenger.getGmiddlename());
                rtaclaimpassenger.setGmobile(tblRtapassenger.getGmobile());
                rtaclaimpassenger.setGpostalcode(tblRtapassenger.getGpostalcode());
                rtaclaimpassenger.setGregion(tblRtapassenger.getGregion());
                rtaclaimpassenger.setGtitle(tblRtapassenger.getGtitle());
                rtaclaimpassenger.setGdob(tblRtapassenger.getGdob());

                rtaclaimpassenger.setAccdate(rtaclaim.getAccdate());
                rtaclaimpassenger.setAcctime(rtaclaim.getAcctime());
                rtaclaimpassenger.setContactdue(rtaclaim.getContactdue());
                rtaclaimpassenger.setDescription(rtaclaim.getDescription());
                rtaclaimpassenger.setInsurer(rtaclaim.getInsurer());
                rtaclaimpassenger.setLocation(rtaclaim.getLocation());
                rtaclaimpassenger.setMakemodel(rtaclaim.getMakemodel());
                rtaclaimpassenger.setPartyaddress(rtaclaim.getPartyaddress());
                rtaclaimpassenger.setPartycontactno(rtaclaim.getPartycontactno());
                rtaclaimpassenger.setPartyinsurer(rtaclaim.getPartyinsurer());
                rtaclaimpassenger.setPartymakemodel(rtaclaim.getPartymakemodel());
                rtaclaimpassenger.setPartyname(rtaclaim.getPartyname());
                rtaclaimpassenger.setPartypolicyno(rtaclaim.getPartypolicyno());
                rtaclaimpassenger.setPartyrefno(rtaclaim.getPartyrefno());
                rtaclaimpassenger.setPartyregno(rtaclaim.getPartyregno());
                rtaclaimpassenger.setRdweathercond(rtaclaim.getRdweathercond());
                rtaclaimpassenger.setRegion(rtaclaim.getRegion());
                rtaclaimpassenger.setRegisterationno(rtaclaim.getRegisterationno());
                rtaclaimpassenger.setReportedtopolice(rtaclaim.getReportedtopolice());
                rtaclaimpassenger.setReportedon(rtaclaim.getReportedon());
                rtaclaimpassenger.setAirbagopened(rtaclaim.getAirbagopened());
                rtaclaimpassenger.setPolicyno(rtaclaim.getPolicyno());
                rtaclaimpassenger.setGreencardno(rtaclaim.getGreencardno());
                rtaclaimpassenger.setReferencenumber(rtaclaim.getReferencenumber());
                rtaclaimpassenger.setRefno(rtaclaim.getRefno());
                rtaclaimpassenger.setScotland(rtaclaim.getScotland());
                rtaclaimpassenger.setYearofmanufacture(rtaclaim.getYearofmanufacture());
                rtaclaimpassenger.setVehiclecondition(rtaclaim.getVehiclecondition());
                rtaclaimpassenger.setTblCircumstance(rtaclaim.getTblCircumstance());

                rtaclaimpassenger.setUsercode(rtaclaim.getUsercode());
                rtaclaimpassenger.setIntroducer(rtaclaim.getIntroducer());
                rtaclaimpassenger.setAdvisor(rtaclaim.getAdvisor());
                rtaclaimpassenger.setVehicleType(rtaclaim.getVehicleType());

                rtaclaimpassenger.setRtanumber(rtaclaim.getRtanumber() + "-" + alphabet);
                rtaclaimpassenger.setStatuscode("1");

                alphabet++;
                rtaclaimpassenger.setEsig("N");
                TblRtaclaim pasengerRtaclaim = tblRtaclaimRepo.save(rtaclaimpassenger);

                for (TblRtaPassengerInjury tblRtaPassengerInjury : tblRtapassenger.getPasengerTblRtainjuries()) {
                    TblRtainjury tblRtainjury = new TblRtainjury();
                    TblInjclass tblInjclass = new TblInjclass();
                    tblInjclass.setInjclasscode(tblRtaPassengerInjury.getInjclasscode());
                    tblRtainjury.setTblRtaclaim(pasengerRtaclaim);
                    tblRtainjury.setTblInjclass(tblInjclass);
                    tblRtainjuryRepo.save(tblRtainjury);
                }

                TblRtalog tblRtalog = new TblRtalog();

                TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(rtaclaimpassenger.getStatuscode()))
                        .orElse(null);

                tblRtalog.setCreatedon(new Date());
                tblRtalog.setDescr(tblStatus.getDescr());
                tblRtalog.setUsercode(String.valueOf(rtaclaimpassenger.getAdvisor()));
                tblRtalog.setTblRtaclaim(rtaclaimpassenger);
                tblRtalog.setOldstatus(Long.valueOf(rtaclaimpassenger.getStatuscode()));
                tblRtalog.setNewstatus(null);

                tblRtalog = tblRtalogRepo.save(tblRtalog);
                //////////////// END OF CREATING CASES FOR PASSENGERS //////////////
                rtapassengers.add(rtapassenger);


                List<RtaDuplicatesResponse> rtaDuplicates = getRtaDuplicates(String.valueOf(rtaclaim.getRtacode()));
                if (rtaDuplicates != null && rtaDuplicates.size() > 0) {

                    TblEmailTemplate tblEmailTemplate = findByEmailType("Rta-Duplicate");
                    String legalbody = tblEmailTemplate.getEmailtemplate();
                    TblUser legalUser = findByUserId("2");

                    legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
                    legalbody = legalbody.replace("[CASE_NUMBER]", pasengerRtaclaim.getRtanumber());
                    legalbody = legalbody.replace("[CLIENT_NAME]",
                            pasengerRtaclaim.getFirstname() + " "
                                    + (pasengerRtaclaim.getMiddlename() == null ? "" : pasengerRtaclaim.getMiddlename() + " ")
                                    + pasengerRtaclaim.getLastname());

                    String duplicateReferenceNumber = rtaDuplicates.stream()
                            .map(RtaDuplicatesResponse::getCaseNumber)
                            .collect(Collectors.joining(", "));

                    legalbody = legalbody.replace("[DUPLICATE_REFRENCES]", duplicateReferenceNumber);
                    legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

                    saveEmail(legalUser.getUsername(), legalbody,
                            pasengerRtaclaim.getRtanumber() + " | Processing Note", pasengerRtaclaim, legalUser);


                }


            }
            return rtapassengers;
        } else {
            return null;
        }
    }

    @SuppressWarnings({"unused", "resource"})
    private boolean uploadFileToServer(String path, String fileName, String fileBase64) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                if (file.mkdir()) {
                    System.out.println("Directory is created!");
                    byte[] imageByte = Base64.decodeBase64(fileBase64);
                    String directory = file.getAbsolutePath() + fileName;

                    new FileOutputStream(directory).write(imageByte);

                    System.out.println("File Upload SuccessFull");
                    return true;
                } else {
                    System.out.println("Failed to create directory!");
                    return false;
                }

            } else {
                System.out.println("Directory is created!");
                byte[] imageByte = Base64.decodeBase64(fileBase64);
                String directory = file.getAbsolutePath() + fileName;

                new FileOutputStream(directory).write(imageByte);
                System.out.println("File Upload SuccessFull");
                return true;
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File Upload SuccessFull");
            return false;
        }
    }

    @Override
    public List<ViewRtalistsolicitors> getAuthRtaCasesSolicitors(String usercode) {
        return viewRtalistSolicitorsRepo.findBySolictercode(usercode);
    }

    @Override
    public List<ViewRtalistintorducers> getAuthRtaCasesIntroducers(String usercode) {
        return viewRtalistintorducersRepo.findByUsercode(usercode);
    }

    @Override
    public List<ViewRtalistintorducers> getAuthRtaCasesLegalAssist() {
        return viewRtalistintorducersRepo.findAll();
    }

    @Override
    public ViewRtaclamin getAuthRtaCase(String rtacode, TblUser tblUser) throws ParseException {
        ViewRtaclamin viewRtaclamin = viewRtaclaminRepo.findByRtacode(new BigDecimal(rtacode));
        if (viewRtaclamin != null) {

            List<TblRtadocument> tblRtadocuments = tblRtadocumentRepo.findByTblRtaclaimRtacode(Long.valueOf(rtacode));
            if (tblRtadocuments != null && tblRtadocuments.size() > 0) {
                viewRtaclamin.setTblRtadocuments(tblRtadocuments);
            }

            // get multiple injuries
            List<Object> tblRtainjuries = tblRtainjuryRepo
                    .getAllInjuriesWithDescription(viewRtaclamin.getRtacode().longValue());
            List<LovResponse> injclasscodes = new ArrayList<>();
            for (Object tblRtainjury : tblRtainjuries) {
                Object[] row = (Object[]) tblRtainjury;
                LovResponse injclasscode = new LovResponse();
                injclasscode.setCode(((BigDecimal) row[1]).toString());
                injclasscode.setName((String) row[4]);
                injclasscodes.add(injclasscode);
            }
            viewRtaclamin.setInjclasscodes(injclasscodes);

            List<RtaActionButton> rtaActionButtons = new ArrayList<>();
            List<RtaActionButton> rtaActionButtonsForLA = new ArrayList<>();
            RtaActionButton rtaActionButton = null;
            TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
            List<TblRtaflowdetail> tblRtaflowdetails = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlow(
                    Long.valueOf(viewRtaclamin.getStatuscode()), "1",
                    tblCompanyprofile.getTblUsercategory().getCategorycode());

            List<TblRtaflowdetail> tblRtaflowdetailForLA = tblRtaflowdetailRepo.getCompaingAndStatusWiseFlowForLAUser(
                    Long.valueOf(viewRtaclamin.getStatuscode().toString()), "1",
                    tblCompanyprofile.getTblUsercategory().getCategorycode());


            TblRtaflow tblRtaflow = tblRtaflowRepo.findByTblCompaignCompaigncodeAndTblRtastatusStatuscode("1", Long.valueOf(viewRtaclamin.getStatuscode()));
            if (tblRtaflow != null) {
                viewRtaclamin.setEditFlag(tblRtaflow.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode()) ? tblRtaflow.getEditflag() : "N");
            }

            if (viewRtaclamin.getRtalinkcode() != null && !viewRtaclamin.getRtalinkcode().isEmpty()) {
                TblRtaclaim linkRtaClaim = tblRtaclaimRepo.findByRtanumber(viewRtaclamin.getRtalinkcode());
                viewRtaclamin.setRtaLinkCaseCode(linkRtaClaim.getRtanumber());
                viewRtaclamin.setRtaLinkCaseid(linkRtaClaim.getRtacode());
                TblRtastatus tblRtastatus = tblRtastatusRepo.findById(Long.valueOf(linkRtaClaim.getStatuscode()))
                        .orElse(null);
                viewRtaclamin.setRtaLinkCaseStatus(tblRtastatus.getDescr());
            }

            /// for introducer and solicitor
            if (tblRtaflowdetails != null && tblRtaflowdetails.size() > 0) {
                for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetails) {
                    rtaActionButton = new RtaActionButton();
                    rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                    rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                    rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                    rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                    rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                    rtaActionButtons.add(rtaActionButton);
                }

                // for LEGAL INTERNAL action buttons in dropdown

                if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                        rtaActionButton = new RtaActionButton();
                        rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                        rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                        rtaActionButtonsForLA.add(rtaActionButton);
                    }
                    viewRtaclamin.setRtaDropdownBtns(rtaActionButtonsForLA);
                }

                List<TblRtapassenger> tblRtapassengers = tblRtapassengerRepo
                        .findByTblRtaclaimRtacode(viewRtaclamin.getRtacode().longValue());
                if (tblRtapassengers != null && tblRtapassengers.size() > 0) {
                    viewRtaclamin.setPassengers(BigDecimal.valueOf(tblRtapassengers.size()));
                    viewRtaclamin.setRtapassengers(tblRtapassengers);

                    List<TblRtaclaim> passengerrtaclaims = tblRtaclaimRepo
                            .getSubRtaClaimsBySolicitor(viewRtaclamin.getRtanumber());
                    if (passengerrtaclaims != null && passengerrtaclaims.size() > 0) {
                        List<PassengerRtaClaims> passengerRtaClaims = new ArrayList<>();
                        PassengerRtaClaims passengerRtaClaim = null;
                        for (TblRtaclaim tblRtaclaim : passengerrtaclaims) {
                            passengerRtaClaim = new PassengerRtaClaims();
                            String s = tblRtaclaim.getTitle() + " " + tblRtaclaim.getFirstname() + " "
                                    + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename()) + " "
                                    + tblRtaclaim.getLastname();
                            passengerRtaClaim.setName(s);
                            passengerRtaClaim.setRtacode(String.valueOf(tblRtaclaim.getRtacode()));
                            passengerRtaClaim.setRtanumber(tblRtaclaim.getRtanumber());
                            passengerRtaClaim.setStatus(tblRtastatusRepo.findById(Long.valueOf(tblRtaclaim.getStatuscode()))
                                    .orElse(null).getDescr());

                            passengerRtaClaims.add(passengerRtaClaim);
                        }
                        viewRtaclamin.setPassengerRtaClaims(passengerRtaClaims);
                    }
                }

                viewRtaclamin.setRtaActionButtons(rtaActionButtons);
                viewRtaclamin.setStatusDescr(viewRtaclamin.getStatus());

                if (viewRtaclamin.getIntroducer() != null && viewRtaclamin.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(viewRtaclamin.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo
                            .findById(String.valueOf(viewRtaclamin.getIntroducer())).orElse(null);

                    TblRtasolicitor tblRtasolicitor = tblRtasolicitorRepo
                            .findByRtacodeAndStatus(viewRtaclamin.getRtacode(), "Y");
                    if (tblRtasolicitor != null) {
                        viewRtaclamin.setSolicitorcompany(tblCompanyprofileRepo
                                .findById(String.valueOf(tblRtasolicitor.getCompanycode())).orElse(null).getName());
                        viewRtaclamin.setSolicitorusername(tblUsersRepo
                                .findByUsercode(String.valueOf(tblRtasolicitor.getUsercode())).getLoginid());
                    }
                    viewRtaclamin.setAdvisorname(user == null ? null : user.getLoginid());
                    viewRtaclamin.setIntroducername(companyprofile == null ? null : companyprofile.getName());
                }

                List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("1", viewRtaclamin.getRtacode());
                if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                    for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                        TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                        if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                            viewRtaclamin.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            viewRtaclamin.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                            viewRtaclamin.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            viewRtaclamin.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        }

                    }
                }

                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("1", new BigDecimal(rtacode));
                viewRtaclamin.setTblEsignStatus(tblEsignStatus);

                return viewRtaclamin;

            } else {

                if (tblRtaflowdetailForLA != null && tblRtaflowdetailForLA.size() > 0) {
                    for (TblRtaflowdetail tblRtaflowdetail : tblRtaflowdetailForLA) {
                        rtaActionButton = new RtaActionButton();
                        rtaActionButton.setApiflag(tblRtaflowdetail.getApiflag());
                        rtaActionButton.setButtonname(tblRtaflowdetail.getButtonname());
                        rtaActionButton.setButtonvalue(tblRtaflowdetail.getButtonvalue());
                        rtaActionButton.setTblRtastatus(tblRtaflowdetail.getTblRtastatus());
                        rtaActionButton.setRejectDialog(tblRtaflowdetail.getCaserejectdialog());

                        rtaActionButtonsForLA.add(rtaActionButton);
                    }
                    viewRtaclamin.setRtaDropdownBtns(rtaActionButtonsForLA);
                }

                if (viewRtaclamin.getIntroducer() != null && viewRtaclamin.getAdvisor() != null) {
                    TblUser user = tblUsersRepo.findByUsercode(String.valueOf(viewRtaclamin.getAdvisor()));
                    TblCompanyprofile companyprofile = tblCompanyprofileRepo
                            .findById(String.valueOf(viewRtaclamin.getIntroducer())).orElse(null);

                    TblRtasolicitor tblRtasolicitor = tblRtasolicitorRepo
                            .findByRtacodeAndStatus(viewRtaclamin.getRtacode(), "Y");
                    if (tblRtasolicitor != null) {
                        viewRtaclamin.setSolicitorcompany(tblCompanyprofileRepo
                                .findById(String.valueOf(tblRtasolicitor.getCompanycode())).orElse(null).getName());
                        viewRtaclamin.setSolicitorusername(tblUsersRepo
                                .findByUsercode(String.valueOf(tblRtasolicitor.getUsercode())).getLoginid());
                    }
                    viewRtaclamin.setAdvisorname(user.getLoginid());
                    viewRtaclamin.setIntroducername(companyprofile.getName());
                }

                List<TblRtapassenger> tblRtapassengers = tblRtapassengerRepo
                        .findByTblRtaclaimRtacode(viewRtaclamin.getRtacode().longValue());
                if (tblRtapassengers != null && tblRtapassengers.size() > 0) {
                    viewRtaclamin.setPassengers(BigDecimal.valueOf(tblRtapassengers.size()));
                    viewRtaclamin.setRtapassengers(tblRtapassengers);

                    List<TblRtaclaim> passengerrtaclaims = tblRtaclaimRepo
                            .getSubRtaClaims(viewRtaclamin.getRtanumber());
                    List<PassengerRtaClaims> passengerRtaClaims = new ArrayList<>();
                    PassengerRtaClaims passengerRtaClaim = null;
                    for (TblRtaclaim tblRtaclaim : passengerrtaclaims) {
                        passengerRtaClaim = new PassengerRtaClaims();
                        String s = tblRtaclaim.getTitle() + " " + tblRtaclaim.getFirstname() + " "
                                + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename()) + " "
                                + tblRtaclaim.getLastname();
                        passengerRtaClaim.setName(s);
                        passengerRtaClaim.setRtacode(String.valueOf(tblRtaclaim.getRtacode()));
                        passengerRtaClaim.setRtanumber(tblRtaclaim.getRtanumber());
                        passengerRtaClaim.setStatus(tblRtastatusRepo.findById(Long.valueOf(tblRtaclaim.getStatuscode()))
                                .orElse(null).getDescr());

                        passengerRtaClaims.add(passengerRtaClaim);
                    }
                    viewRtaclamin.setPassengerRtaClaims(passengerRtaClaims);
                }
                viewRtaclamin.setStatusDescr(viewRtaclamin.getStatus());

                List<TblRtalog> rtaCaseLogs = getAuthRtaCaseLogs(rtacode);

                viewRtaclamin.setTblRtalogs(rtaCaseLogs);
                viewRtaclamin.setStatusDescr(viewRtaclamin.getStatus());

                List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetailRepo.findByTblCompaignCompaigncodeAndCasecode("1", viewRtaclamin.getRtacode());
                if (tblSolicitorInvoiceDetails != null && tblSolicitorInvoiceDetails.size() > 0) {
                    for (TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail : tblSolicitorInvoiceDetails) {
                        TblCompanyprofile companyprofile = tblCompanyprofileRepo.findById(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getCompanycode()).orElse(null);
                        if (companyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                            viewRtaclamin.setIntroducerInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            viewRtaclamin.setIntroducerInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        } else if (companyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                            viewRtaclamin.setSolicitorInvoiceDate(tblSolicitorInvoiceDetail.getCreatedate());
                            viewRtaclamin.setSolicitorInvoiceHeadId(BigDecimal.valueOf(tblSolicitorInvoiceDetail.getTblSolicitorInvoiceHead().getInvoiceheadid()));
                        }

                    }
                }

                TblEsignStatus tblEsignStatus = tblEsignStatusRepo.findByTblCompaignCompaigncodeAndClaimcode("1", new BigDecimal(rtacode));
                viewRtaclamin.setTblEsignStatus(tblEsignStatus);

                return viewRtaclamin;
            }
        } else {
            return null;
        }
    }

    @Override
    public List<TblRtapassenger> getAuthRtaCasePassengers(String rtacode) {
        return tblRtapassengerRepo.findByTblRtaclaimRtacode(Long.valueOf(rtacode));
    }

    @Override
    public TblRtaclaim updateRtaCase(TblRtaclaim tblRtaclaim, List<UpdateRtaFileUploadRequest> files) {

        tblRtainjuryRepo.deleteAllInjuriesOfRta(tblRtaclaim.getRtacode());

        for (TblRtainjury tblRtainjury : tblRtaclaim.getInjclasscodes()) {
            tblRtainjury.setTblRtaclaim(tblRtaclaim);
        }
        tblRtainjuryRepo.saveAll(tblRtaclaim.getInjclasscodes());

        tblRtaclaim = tblRtaclaimRepo.save(tblRtaclaim);
        if (tblRtaclaim.getNinumber().contains("Solicitor obtained Number")) {
            TblRtatask tblRtatask = tblRtataskRepo.getRtaTaskAgainstRtaCodeAndTaskCode(tblRtaclaim.getRtacode(), 6);
            if (!tblRtatask.getStatus().equals("C")) {
                tblRtatask.setStatus("C");
                tblRtatask.setCompletedon(new Date());
                tblRtataskRepo.saveAndFlush(tblRtatask);
            }
        }
        return tblRtaclaim;
    }

    @Override
    public TblRtaclaim updateRtaCase(TblRtaclaim tblRtaclaim) {
        return tblRtaclaimRepo.saveAndFlush(tblRtaclaim);
    }

    @Override
    public List<TblRtapassenger> updateRtaPassengers(TblRtaclaim tblRtaclaim, List<TblRtapassenger> tblRtapassengers) {
        List<TblRtapassenger> rtapassengers = new ArrayList<>();

        for (TblRtapassenger tblRtapassenger : tblRtapassengers) {
            tblRtapassenger = tblRtapassengerRepo.save(tblRtapassenger);
            rtapassengers.add(tblRtapassenger);
        }
        return tblRtapassengers;
    }

    @Override
    public TblRtaclaim getTblRtaclaimById(long rtacode) {
        return tblRtaclaimRepo.findById(rtacode).orElse(null);

    }

    @Override
    public List<TblRtanote> getAuthRtaCaseNotes(String rtacode, TblUser tblUser) {
        TblCompanyprofile tblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
        List<TblRtanote> tblRtanotes = tblRtanoteRepo.findNotesOnRtabyUserAndCategory(Long.valueOf(rtacode),
                tblCompanyprofile.getTblUsercategory().getCategorycode(), tblUser.getUsercode());
        if (tblRtanotes != null && tblRtanotes.size() > 0) {
            for (TblRtanote tblRtanote : tblRtanotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblRtanote.getUsercode()).orElse(null);
                tblRtanote.setUserName(tblUser1.getLoginid());
                tblRtanote.setSelf(tblRtanote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
                if (!tblRtanote.getUsercode().equalsIgnoreCase(tblUser.getUsercode())) {
                    TblCompanyprofile recevingTblCompanyprofile = tblCompanyprofileRepo.findById(tblUser.getCompanycode()).orElse(null);
                    tblRtanote.setUsercategorycode(recevingTblCompanyprofile.getTblUsercategory().getCategorycode());
                }
            }

            return tblRtanotes;
        } else {
            return null;
        }
    }

    @Override
    public List<TblRtanote> getAuthRtaCaseNotesOfLegalInternal(String rtacode, TblUser tblUser) {
        List<TblRtanote> tblRtanotes = tblRtanoteRepo.getAuthRtaCaseNotesOfLegalInternal(Long.valueOf(rtacode),
                tblUser.getUsercode());
        if (tblRtanotes != null && tblRtanotes.size() > 0) {
            for (TblRtanote tblRtanote : tblRtanotes) {
                TblUser tblUser1 = tblUsersRepo.findById(tblRtanote.getUsercode()).orElse(null);
                tblRtanote.setUserName(tblUser1.getLoginid());
                tblRtanote.setSelf(tblRtanote.getUsercode().equalsIgnoreCase(tblUser.getUsercode()) ? true : false);
            }

            return tblRtanotes;
        } else {
            return null;
        }
    }

    @Override
    public TblCompanyprofile getCompanyProfileByUser(String usercode) {
        return tblCompanyprofileRepo.getCompanyProfileByUser(usercode);
    }

    @Override
    public TblRtanote addNoteToRta(TblRtanote tblRtanote) {
        TblRtanote rtanote = tblRtanoteRepo.save(tblRtanote);
        rtanote.setTblRtaclaim(tblRtaclaimRepo.findById(rtanote.getTblRtaclaim().getRtacode()).orElse(null));
        TblUser legalUser = tblUsersRepo.findByUsercode("2");
        // for legal internal

        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("note");
        String legalbody = tblEmailTemplate.getEmailtemplate();

        legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
        legalbody = legalbody.replace("[CASE_NUMBER]", rtanote.getTblRtaclaim().getRtanumber());
        legalbody = legalbody.replace("[CLIENT_NAME]",
                rtanote.getTblRtaclaim().getFirstname() + " "
                        + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
                        : rtanote.getTblRtaclaim().getMiddlename() + " ")
                        + rtanote.getTblRtaclaim().getLastname());
        legalbody = legalbody.replace("[NOTE]", rtanote.getNote());
        legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtanote.getTblRtaclaim().getRtacode());

        saveEmail(legalUser.getUsername(), legalbody, rtanote.getTblRtaclaim().getRtanumber() + " | Processing Note",
                rtanote.getTblRtaclaim(), legalUser);

        if (!rtanote.getUsercategorycode().equals("4")) {
            // for introducer
            if (rtanote.getUsercategorycode().equals("1")) {
                TblUser introducerUser = tblUsersRepo
                        .findByUsercode(String.valueOf(rtanote.getTblRtaclaim().getAdvisor()));
                String introducerBody = tblEmailTemplate.getEmailtemplate();

                introducerBody = introducerBody.replace("[USER_NAME]", introducerUser.getLoginid());
                introducerBody = introducerBody.replace("[CASE_NUMBER]", rtanote.getTblRtaclaim().getRtanumber());
                introducerBody = introducerBody.replace("[CLIENT_NAME]",
                        rtanote.getTblRtaclaim().getFirstname() + " "
                                + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
                                : rtanote.getTblRtaclaim().getMiddlename() + " ")
                                + rtanote.getTblRtaclaim().getLastname());
                introducerBody = introducerBody.replace("[NOTE]", rtanote.getNote());
                introducerBody = introducerBody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtanote.getTblRtaclaim().getRtacode());

                saveEmail(introducerUser.getUsername(), introducerBody,
                        rtanote.getTblRtaclaim().getRtanumber() + " | Processing Note", rtanote.getTblRtaclaim(),
                        introducerUser);
            } else if (rtanote.getUsercategorycode().equals("2")) {

                TblRtasolicitor tblRtasolicitor = tblRtasolicitorRepo
                        .findByRtacodeAndStatus(new BigDecimal(rtanote.getTblRtaclaim().getRtacode()), "Y");

                if (tblRtasolicitor != null) {
                    TblUser solicitorUser = tblUsersRepo.findById(tblRtasolicitor.getUsercode()).orElse(null);

                    String solicitorBody = tblEmailTemplate.getEmailtemplate();

                    solicitorBody = solicitorBody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorBody = solicitorBody.replace("[CASE_NUMBER]", rtanote.getTblRtaclaim().getRtanumber());
                    solicitorBody = solicitorBody.replace("[CLIENT_NAME]",
                            rtanote.getTblRtaclaim().getFirstname() + " "
                                    + (rtanote.getTblRtaclaim().getMiddlename() == null ? ""
                                    : rtanote.getTblRtaclaim().getMiddlename() + " ")
                                    + rtanote.getTblRtaclaim().getLastname());
                    solicitorBody = solicitorBody.replace("[NOTE]", rtanote.getNote());
                    solicitorBody = solicitorBody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtanote.getTblRtaclaim().getRtacode());

                    saveEmail(solicitorUser.getUsername(), solicitorBody,
                            rtanote.getTblRtaclaim().getRtanumber() + " | Processing Note", rtanote.getTblRtaclaim(),
                            solicitorUser);

                }
            }

        }

        return rtanote;
    }

    @Override
    public List<TblRtamessage> getAuthRtaCaseMessages(String rtacode) {

        List<TblRtamessage> tblRtamessages = tblRtamessageRepo.findAllByTblRtaclaimRtacode(Long.valueOf(rtacode));
        if (tblRtamessages != null && tblRtamessages.size() > 0) {
            for (TblRtamessage tblRtamessage : tblRtamessages) {
                if (tblRtamessage.getUsercode() != null) {
                    TblUser tblUser = tblUsersRepo.findById(tblRtamessage.getUsercode()).orElse(null);
                    tblRtamessage.setUserName(tblUser.getLoginid());
                }
            }
            return tblRtamessages;
        } else {
            return null;
        }

    }

    @Override
    public TblRtaclaim performActionOnRta(String rtaCode, String toStatus, TblUser tblUser) {

        TblRtaclaim rtaclaim = tblRtaclaimRepo.findById(Long.valueOf(rtaCode)).orElse(null);

        int update = tblRtaclaimRepo.performAction(toStatus, Long.valueOf(rtaCode), tblUser.getUsercode());
        if (update > 0) {

            TblRtaclaim tblRtaclaim = new TblRtaclaim();
            TblRtalog tblRtalog = new TblRtalog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);
            tblRtaclaim.setRtacode(Long.valueOf(rtaCode));

            tblRtalog.setCreatedon(new Date());
            tblRtalog.setDescr(tblStatus.getDescr());
            tblRtalog.setUsercode(tblUser.getUsercode());
            tblRtalog.setTblRtaclaim(tblRtaclaim);
            tblRtalog.setOldstatus(Long.valueOf(rtaclaim.getStatuscode()));
            tblRtalog.setNewstatus(Long.valueOf(toStatus));

            tblRtalog = tblRtalogRepo.save(tblRtalog);
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status");

            // for introducer
            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(rtaclaim.getAdvisor()));
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            introducerbody = introducerbody.replace("[CLIENT_NAME]",
                    rtaclaim.getFirstname() + " "
                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                            + rtaclaim.getLastname());
            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, introducerUser);

            // for legal internal
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            legalbody = legalbody.replace("[CLIENT_NAME]",
                    rtaclaim.getFirstname() + " "
                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                            + rtaclaim.getLastname());
            legalbody = legalbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, legalUser);

            if (toStatus.equals(1)) {
                // when reverting a case takes to new status remove all solcitors from case if
                // any
                int updateSolicitor = tblRtasolicitorRepo.updateSolicitor("N", new BigDecimal(rtaCode));
                TblRtasolicitor tblRtasolicitor = tblRtasolicitorRepo
                        .findByRtacodeAndStatus(new BigDecimal(rtaclaim.getRtacode()), "Y");
                if (tblRtasolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblRtasolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                            rtaclaim.getFirstname() + " "
                                    + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                                    + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            } else {
                // for normal emails
                TblRtasolicitor tblRtasolicitor = tblRtasolicitorRepo
                        .findByRtacodeAndStatus(new BigDecimal(rtaclaim.getRtacode()), "Y");
                if (tblRtasolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblRtasolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                            rtaclaim.getFirstname() + " "
                                    + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                                    + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            }

            // if submmitting a case add clawback date current date + 90 days
            TblRtaclaim tblRtaclaimForClawBack = tblRtaclaimRepo.findById(Long.valueOf(rtaCode)).orElse(null);
            if (toStatus.equals("7")) {
                Date clawbackDate = addDaysToDate(90);
                tblRtaclaimRepo.updateClawbackdate(clawbackDate, Long.valueOf(rtaCode));
                tblRtaclaimRepo.updateSubmitDate(new Date(), Long.valueOf(rtaCode));
            } else if (toStatus.equals("18")) {
                Date clawbackDate = addDaysToSpecificDate(tblRtaclaimForClawBack.getClawbackDate(), 30);
                tblRtaclaimRepo.updateClawbackdate(clawbackDate, Long.valueOf(rtaCode));
            } else if (toStatus.equals("19")) {
                Date clawbackDate = addDaysToSpecificDate(tblRtaclaimForClawBack.getClawbackDate(), 60);
                tblRtaclaimRepo.updateClawbackdate(clawbackDate, Long.valueOf(rtaCode));
            } else if (toStatus.equals("20")) {
                Date clawbackDate = addDaysToSpecificDate(tblRtaclaimForClawBack.getClawbackDate(), 90);
                tblRtaclaimRepo.updateClawbackdate(clawbackDate, Long.valueOf(rtaCode));
            } else if (toStatus.equals("5")) {
                TblRtanote tblRtanote = new TblRtanote();
                TblRtasolicitor tblRtasolicitor = tblRtasolicitorRepo
                        .findByRtacodeAndStatus(new BigDecimal(rtaclaim.getRtacode()), "Y");
                TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblRtasolicitor.getCompanycode())
                        .orElse(null);
                // NOte for Legal internal user and NOTE for introducer
                tblRtanote.setTblRtaclaim(rtaclaim);
                tblRtanote.setNote("Case Accepted By " + SolicitorCompany.getName());
                tblRtanote.setUsercategorycode("1");
                tblRtanote.setCreatedon(new Date());
                tblRtanote.setUsercode("1");

                addNoteToRta(tblRtanote);
            }
            return rtaclaim;
        } else {
            return null;
        }
    }

    @Override
    public TblRtaclaim performActionOnRtaFromDirectIntro(PerformHotKeyOnRtaRequest performHotKeyOnRtaRequest,
                                                         TblUser tblUser) {
        TblRtaclaim rtaclaim = tblRtaclaimRepo.findById(Long.valueOf(performHotKeyOnRtaRequest.getRtacode()))
                .orElse(null);

        int update = tblRtaclaimRepo.performAction(performHotKeyOnRtaRequest.getToStatus(),
                Long.valueOf(performHotKeyOnRtaRequest.getRtacode()), tblUser.getUsercode());
        if (update > 0) {

            TblRtaclaim tblRtaclaim = new TblRtaclaim();
            TblRtalog tblRtalog = new TblRtalog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(performHotKeyOnRtaRequest.getToStatus()))
                    .orElse(null);
            tblRtaclaim.setRtacode(Long.valueOf(performHotKeyOnRtaRequest.getRtacode()));

            tblRtalog.setCreatedon(new Date());
            tblRtalog.setDescr(tblStatus.getDescr());
            tblRtalog.setUsercode(tblUser.getUsercode());
            tblRtalog.setTblRtaclaim(tblRtaclaim);
            tblRtalog.setOldstatus(Long.valueOf(rtaclaim.getStatuscode()));
            tblRtalog.setNewstatus(Long.valueOf(performHotKeyOnRtaRequest.getToStatus()));

            int updateSolicitor = tblRtasolicitorRepo.updateSolicitor("N",
                    new BigDecimal(performHotKeyOnRtaRequest.getRtacode()));

            TblRtasolicitor tblRtasolicitor = new TblRtasolicitor();
            tblRtasolicitor.setCompanycode(performHotKeyOnRtaRequest.getSolicitorCode());
            tblRtasolicitor.setCreatedon(new Date());
            tblRtasolicitor.setRtacode(new BigDecimal(performHotKeyOnRtaRequest.getRtacode()));
            tblRtasolicitor.setStatus("Y");
            tblRtasolicitor.setUsercode(performHotKeyOnRtaRequest.getSolicitorUserCode());

            tblRtasolicitor = tblRtasolicitorRepo.save(tblRtasolicitor);
            tblRtalog = tblRtalogRepo.save(tblRtalog);

            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(rtaclaim.getAdvisor()));
            TblUser solicitorUser = tblUsersRepo
                    .findByUsercode(String.valueOf(performHotKeyOnRtaRequest.getSolicitorUserCode()));
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status-hotkey");

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            introducerbody = introducerbody.replace("[CLIENT_NAME]",
                    rtaclaim.getFirstname() + " "
                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                            + rtaclaim.getLastname());
//            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    rtaclaim.getRtanumber() + " | HotKey", rtaclaim, introducerUser);

            // for solicitor
            String solicitorbody = tblEmailTemplate.getEmailtemplate();

            solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
            solicitorbody = solicitorbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                    rtaclaim.getFirstname() + " "
                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                            + rtaclaim.getLastname());
//            solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

            saveEmail(solicitorUser.getUsername(), solicitorbody, rtaclaim.getRtanumber() + " | Processing Note",
                    rtaclaim, solicitorUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            legalbody = legalbody.replace("[CLIENT_NAME]",
                    rtaclaim.getFirstname() + " "
                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                            + rtaclaim.getLastname());
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());
//            legalbody = legalbody.replace("[STATUS_DESCR]", tblStatus.getDescr());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    rtaclaim.getRtanumber() + " | HotKey", rtaclaim, legalUser);

            // checking if the case has any serious injuries and then send email to legal
            // dept
            List<TblRtainjury> tblRtainjuries = tblRtainjuryRepo.findByTblRtaclaimRtacode(tblRtaclaim.getRtacode());
            TblEmailTemplate tblEmailTemplateinjury = tblEmailTemplateRepo.findByEmailtype("serious-injury-alert");
            for (TblRtainjury tblRtainjury : tblRtainjuries) {
                if (tblRtainjury.getTblInjclass().getInjtype().equals("S")) {
                    String piDeptbody = tblEmailTemplateinjury.getEmailtemplate();

                    piDeptbody = piDeptbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
                    piDeptbody = piDeptbody.replace("[CLIENT_NAME]",
                            rtaclaim.getFirstname() + " "
                                    + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                                    + rtaclaim.getLastname());
                    piDeptbody = piDeptbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

                    saveEmail(legalUser.getUsername(), piDeptbody,
                            rtaclaim.getRtanumber() + " | HotKey | SERIOUS INJURIES", rtaclaim, legalUser);
                    break;
                }
            }

            return tblRtaclaimRepo.findById(Long.valueOf(performHotKeyOnRtaRequest.getRtacode())).orElse(null);

        } else {
            return null;
        }
    }

    @Override
    public List<TblRtalog> getAuthRtaCaseLogs(String rtacode) throws ParseException {
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        List<TblRtalog> tblRtalogs = tblRtalogRepo.findByTblRtaclaimRtacodeOrderByRtalogcodeDesc(Long.valueOf(rtacode));
        if (tblRtalogs != null && tblRtalogs.size() > 0) {
            for (TblRtalog tblRtanote : tblRtalogs) {
                TblUser tblUser = tblUsersRepo.findById(tblRtanote.getUsercode()).orElse(null);
                tblRtanote.setUserName(tblUser.getLoginid());
                tblRtanote.setCreatedon(formatter1.parse(formatter1.format(tblRtanote.getCreatedon())));
            }
            return tblRtalogs;
        } else {
            return null;
        }

    }

    @Override
    public List<TblRtatask> getAuthRtaCaseTasks(String rtacode) {

        return tblRtataskRepo.findByTblRtaclaimRtacode(Long.valueOf(rtacode));

    }

    @Override
    public List<TblRtadocument> getAuthRtaCasedocuments(String rtacode) {
        List<TblRtadocument> tblRtadocuments = tblRtadocumentRepo.findByTblRtaclaimRtacode(Long.valueOf(rtacode));
        if (tblRtadocuments != null) {
            return tblRtadocuments;
        } else {
            return null;
        }

    }

    @Override
    public TblRtaflow addRtaFlow(TblRtaflow tblRtaflow) {
        return tblRtaflowRepo.save(tblRtaflow);
    }

    @Override
    public List<TblRtaflow> getWorkFlow() {
        return tblRtaflowRepo.findAll();
    }

    @Override
    public TblRtaflowdetail addWorkFlowDetail(TblRtaflowdetail tblRtaflowdetail) {
        return tblRtaflowdetailRepo.save(tblRtaflowdetail);
    }

    @Override
    public List<TblRtadocument> addRtaDocument(List<TblRtadocument> tblRtadocuments) {
        return tblRtadocumentRepo.saveAll(tblRtadocuments);
    }

    @Override
    public TblCompanyprofile getCompanyProfileAgainstRtaCode(long rtacode) {
        return tblCompanyprofileRepo.getCompanyProfileAgainstRtaCode(rtacode);
    }

    @Override
    public String getRtaDbColumnValue(String key, long rtaCode) {
        Query query = null;
        if (key.contains("DATE")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from Tbl_Rtaclaim  WHERE rtacode = " + rtaCode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("NAME")) {
            String sql = "SELECT FIRSTNAME || ' ' || NVL(MIDDLENAME,'') || ' ' || NVL(LASTNAME, '') from Tbl_Rtaclaim  WHERE rtacode = "
                    + rtaCode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("ADDRESS")) {
            String sql = "SELECT POSTALCODE || ' ' || ADDRESS1 || ' ' || ADDRESS2  || ' ' || ADDRESS3 || ' ' || CITY || ' ' || REGION from Tbl_Rtaclaim  WHERE rtacode = "
                    + rtaCode;
            query = em.createNativeQuery(sql);
        } else if (key.contains("DOB")) {
            String sql = "SELECT TO_CHAR(" + key + ",'DD-MM-YYYY') from Tbl_Rtaclaim  WHERE rtacode = " + rtaCode;
            query = em.createNativeQuery(sql);
        } else {
            String sql = "SELECT " + key + " from Tbl_Rtaclaim  WHERE rtacode = " + rtaCode;
            query = em.createNativeQuery(sql);
        }
        @SuppressWarnings("unchecked")

        List<String> keyValue = (List<String>) query.getResultList();

        return keyValue.get(0);
    }

    public TblRtadocument addRtaDocumentSingle(TblRtadocument tblRtadocument) {
        return tblRtadocumentRepo.save(tblRtadocument);
    }

    @Override
    public TblTask getTaskAgainstCode(long taskCode) {
        return tblTaskRepo.findById(taskCode).orElse(null);
    }

    @Override
    public TblRtatask getRtaTaskAgainstRtaCodeAndTaskCode(long rtaCode, long taskCode) {
        return tblRtataskRepo.getRtaTaskAgainstRtaCodeAndTaskCode(rtaCode, taskCode);
    }

    @Override
    public TblRtatask saveTblRtaTask(TblRtatask tblRtatask) {
        return tblRtataskRepo.save(tblRtatask);
    }

    @Override
    public TblRtamessage getTblRtaMessageById(String rtamessagecode) {
        return tblRtamessageRepo.findById(Long.valueOf(rtamessagecode)).orElse(null);
    }

    @Override
    public TblEmail saveTblEmail(TblEmail tblEmail) {
        return tblEmailRepo.save(tblEmail);
    }

    @Override
    public int updateTblRtaTask(TblRtatask tblRtatask, long rtaCode) {
        int update = tblRtataskRepo.updatetask(tblRtatask.getStatus(), tblRtatask.getRemarks(),
                tblRtatask.getRtataskcode(), rtaCode);
        if (update > 0) {
            return update;
        } else {
            return 0;
        }
    }

    @Override
    public List<TblRtatask> getRtaTaskAgainstRtaCodeAndStatus(String rtaCode, String status) {
        return tblRtataskRepo.findByTblRtaclaimRtacodeAndStatusAndTblTaskMandatory(Long.valueOf(rtaCode), status,"Y");
    }

    @Override
    public TblCompanyprofile saveCompanyProfile(TblCompanyprofile tblCompanyprofile) {
        return tblCompanyprofileRepo.save(tblCompanyprofile);
    }

    @Override
    public TblCompanydoc getCompanyDocs(String companycode, String ageNature, String countryType) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndAgenatureAndCountrytypeAndTblCompaignCompaigncode(
                companycode, ageNature, countryType, "1");
    }

    @Override
    public TblRtaclaim addRtaTaskDocument(List<RtaFileUploadRequest> files, long rtaCode, TblUser tblUser,
                                          TblTask tblTask) {

        TblRtaclaim tblRtaclaim = tblRtaclaimRepo.getOne(rtaCode);

        if (files != null && files.size() > 0) {
            for (RtaFileUploadRequest rtaFileUploadRequest : files) {
                TblRtadocument tblRtadocument = new TblRtadocument();
                String milisecond = String.valueOf(System.currentTimeMillis());
                tblRtadocument.setDocname(
                        rtaFileUploadRequest.getFileName() + milisecond + "." + rtaFileUploadRequest.getFileExt());
                tblRtadocument.setDoctype("Image");
                tblRtadocument.setDocbase64(rtaFileUploadRequest.getFileBase64());
                tblRtadocument.setTblRtaclaim(tblRtaclaim);
                tblRtadocument.setUsercode(tblUser.getUsercode());
                tblRtadocument.setCreatedon(new Date());
                tblRtadocument.setTblTask(tblTask);

                tblRtadocumentRepo.save(tblRtadocument);
            }
        }
        return tblRtaclaim;
    }

    @Override
    public TblHireclaim copyRtaToHire(String hireCode) {
        Session session = em.unwrap(Session.class);
        final int[] rtaCode = new int[1];
        final int[] status = new int[1];

        session.doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                CallableStatement call = connection.prepareCall("{call COPYRTATOHIRE(?,?,?) }");
                call.setLong(1, Long.valueOf(hireCode));
                call.registerOutParameter(2, Types.INTEGER);
                call.registerOutParameter(3, Types.INTEGER);
                call.execute();
                rtaCode[0] = call.getInt(2);
                status[0] = call.getInt(3);
            }
        });

        if (status[0] == 1) {
            return tblHireClaimRepo.findByHirecode(rtaCode[0]);
        } else {
            return null;
        }
    }

    @Override
    public int updateCurrentTask(long taskCode, long rtaCode, String current) {
        tblRtataskRepo.setAllTask("N", rtaCode);
        return tblRtataskRepo.setCurrentTask(current, taskCode, rtaCode);
    }

    @Override
    public List<ViewRtacasereport> getRtaCaseReport(RtaCaseReportRequest rtaCaseReportRequest) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
        Date fromDate = formatter.parse(rtaCaseReportRequest.getFromDate());
        Date toDate = formatter.parse(rtaCaseReportRequest.getToDate());

        String dateFrom = formatter1.format(fromDate);
        String dateTo = formatter1.format(toDate);

        return viewRtacasereportRepo.viewReport(dateTo, dateFrom);

    }

    @Override
    public TblRtaclaim performRejectCancelActionOnRta(String rtaCode, String toStatus, String reason, TblUser tblUser) {

        TblRtaclaim rtaclaim = tblRtaclaimRepo.findById(Long.valueOf(rtaCode)).orElse(null);
        TblRtasolicitor tblRtasolicitor = tblRtasolicitorRepo.findByRtacodeAndStatus(new BigDecimal(rtaCode), "Y");
        TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status-Reject");
        TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(toStatus)).orElse(null);


        if (tblRtasolicitor != null) {
            int update = tblRtasolicitorRepo.updateRemarksReason(reason, "N", new BigDecimal(rtaCode));


            TblUser SolicitorUser = tblUsersRepo.findById(tblRtasolicitor.getUsercode()).orElse(null);
            TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblRtasolicitor.getCompanycode())
                    .orElse(null);

            TblRtanote tblRtanote = new TblRtanote();
            // NOte for Legal internal user and NOTE for introducer
            tblRtanote.setTblRtaclaim(rtaclaim);
            tblRtanote.setNote(SolicitorCompany.getName() + " has rejected the case. Reason :" + reason);
            tblRtanote.setUsercategorycode("1");
            tblRtanote.setCreatedon(new Date());
            tblRtanote.setUsercode("1");

            addNoteToRta(tblRtanote);
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            // Send Email to Introducer
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(rtaclaim.getAdvisor())).orElse(null);

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", rtaclaim.getFirstname() + " "
                    + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ") + rtaclaim.getLastname());
            introducerbody = introducerbody.replace("[SOLICITOR_NAME]", SolicitorCompany.getName());
            introducerbody = introducerbody.replace("[REASON]", reason);
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

            saveEmail(introducerUser.getUsername(), introducerbody, rtaclaim.getRtanumber() + " | " + tblStatus.getDescr(),
                    rtaclaim, introducerUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            legalbody = legalbody.replace("[CLIENT_NAME]", rtaclaim.getFirstname() + " "
                    + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ") + rtaclaim.getLastname());
            legalbody = legalbody.replace("[SOLICITOR_NAME]", SolicitorCompany.getName());
            legalbody = legalbody.replace("[REASON]", reason);
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

            saveEmail(legalUser.getUsername(), legalbody, rtaclaim.getRtanumber() + " | " + tblStatus.getDescr(), rtaclaim,
                    legalUser);

        } else {

            TblRtanote tblRtanote = new TblRtanote();
            // NOte for Legal internal user and NOTE for introducer
            tblRtanote.setTblRtaclaim(rtaclaim);
            tblRtanote.setNote(tblUser.getLoginid() + " has rejected the case. Reason :" + reason);
            tblRtanote.setUsercategorycode("1");
            tblRtanote.setCreatedon(new Date());
            tblRtanote.setUsercode("1");

            addNoteToRta(tblRtanote);

            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            TblUser introducerUser = tblUsersRepo.findById(String.valueOf(rtaclaim.getAdvisor())).orElse(null);

            // for introducer
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            introducerbody = introducerbody.replace("[CLIENT_NAME]", rtaclaim.getFirstname() + " "
                    + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ") + rtaclaim.getLastname());
            introducerbody = introducerbody.replace("[SOLICITOR_NAME]", tblUser.getLoginid());
            introducerbody = introducerbody.replace("[REASON]", reason);
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

            saveEmail(introducerUser.getUsername(), introducerbody, rtaclaim.getRtanumber() + " | " + tblStatus.getDescr(),
                    rtaclaim, introducerUser);

            // for legal internal
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            legalbody = legalbody.replace("[CLIENT_NAME]", rtaclaim.getFirstname() + " "
                    + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ") + rtaclaim.getLastname());
            legalbody = legalbody.replace("[SOLICITOR_NAME]", tblUser.getLoginid());
            legalbody = legalbody.replace("[REASON]", reason);
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

            saveEmail(legalUser.getUsername(), legalbody, rtaclaim.getRtanumber() + " | " + tblStatus.getDescr(), rtaclaim,
                    legalUser);
        }

        TblRtalog tblRtalog = new TblRtalog();


        tblRtalog.setCreatedon(new Date());
        tblRtalog.setDescr(tblStatus.getDescr());
        tblRtalog.setUsercode(tblUser.getUsercode());
        tblRtalog.setTblRtaclaim(rtaclaim);
        tblRtalog.setOldstatus(Long.valueOf(rtaclaim.getStatuscode()));
        tblRtalog.setNewstatus(Long.valueOf(toStatus));

        tblRtalog = tblRtalogRepo.save(tblRtalog);
        int update = tblRtaclaimRepo.performRejectCancelAction(reason, toStatus, Long.valueOf(rtaCode), tblUser.getUsercode());

        return rtaclaim;

    }

    @Override
    public List<TblRtaclaim> getSubRtaClaims(String rtacasecode) {
        return tblRtaclaimRepo.getSubRtaClaims(rtacasecode);
    }

    @Override
    public List<RtaStatusCountList> getAllRtaStatusCountsForIntroducers(String CompanyCode) {
        List<RtaStatusCountList> PlStatusCountLists = new ArrayList<>();
        List<Object> PlStatusCountListObject = tblRtaclaimRepo.getAllRtaStatusCountsForIntroducers(CompanyCode);
        RtaStatusCountList PlStatusCountList;
        if (PlStatusCountListObject != null && PlStatusCountListObject.size() > 0) {
            for (Object record : PlStatusCountListObject) {
                PlStatusCountList = new RtaStatusCountList();
                Object[] row = (Object[]) record;

                PlStatusCountList.setStatusCount((BigDecimal) row[0]);
                PlStatusCountList.setStatusName((String) row[1]);
                PlStatusCountList.setStatusCode((BigDecimal) row[2]);

                PlStatusCountLists.add(PlStatusCountList);
            }
        }
        if (PlStatusCountLists != null && PlStatusCountLists.size() > 0 && !(PlStatusCountLists.size() == 0)) {
            return PlStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<RtaStatusCountList> getAllRtaStatusCountsForSolicitor(String CompanyCode) {
        List<RtaStatusCountList> PlStatusCountLists = new ArrayList<>();
        List<Object> PlStatusCountListObject = tblRtaclaimRepo.getAllRtaStatusCountsForSolicitors(CompanyCode);
        RtaStatusCountList PlStatusCountList;
        if (PlStatusCountListObject != null && PlStatusCountListObject.size() > 0) {
            for (Object record : PlStatusCountListObject) {
                PlStatusCountList = new RtaStatusCountList();
                Object[] row = (Object[]) record;

                PlStatusCountList.setStatusCount((BigDecimal) row[0]);
                PlStatusCountList.setStatusName((String) row[1]);
                PlStatusCountList.setStatusCode((BigDecimal) row[2]);

                PlStatusCountLists.add(PlStatusCountList);
            }
        }
        if (PlStatusCountLists != null && PlStatusCountLists.size() > 0) {
            return PlStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<RtaStatusCountList> getAllRtaStatusCounts() {
        List<RtaStatusCountList> PlStatusCountLists = new ArrayList<>();
        List<Object> PlStatusCountListObject = tblRtaclaimRepo.getAllPlStatusCounts();
        RtaStatusCountList PlStatusCountList;
        if (PlStatusCountListObject != null && PlStatusCountListObject.size() > 0) {
            for (Object record : PlStatusCountListObject) {
                PlStatusCountList = new RtaStatusCountList();
                Object[] row = (Object[]) record;

                PlStatusCountList.setStatusCount((BigDecimal) row[0]);
                PlStatusCountList.setStatusName((String) row[1]);
                PlStatusCountList.setStatusCode((BigDecimal) row[2]);

                PlStatusCountLists.add(PlStatusCountList);
            }
        }
        if (PlStatusCountLists != null && PlStatusCountLists.size() > 0) {
            return PlStatusCountLists;
        } else {
            return null;
        }
    }

    @Override
    public List<TblRtaclaim> getRtaCasesByStatus(long statusId) {
        return tblRtaclaimRepo.findByStatuscode(String.valueOf(statusId));
    }

    @Override
    public List<ViewRtalistintorducers> getAuthRtaCasesIntroducersStatusWise(String companyCode, String statusId) {
        return viewRtalistintorducersRepo.findByIntroducerAndStatuscode(companyCode, statusId);
    }

    @Override
    public List<ViewRtalistsolicitors> getAuthRtaCasesSolicitorsStatusWise(String companyCode, String statusId) {
        return viewRtalistSolicitorsRepo.findBySolictercodeAndStatuscode(companyCode, statusId);
    }

    @Override
    public List<ViewRtalistintorducers> getAuthRtaCasesLegalAssistStatusWise(String statusId) {
        return viewRtalistintorducersRepo.findByStatuscode(statusId);
    }

    @Override
    public TblRtaclaim findRtaById(String caseId) {
        return tblRtaclaimRepo.findById(Long.valueOf(caseId)).orElse(null);
    }

    @Override
    public TblRtasolicitor getRtaSolicitorsOfRtaClaim(String rtacode) {
        return tblRtasolicitorRepo.findByRtacodeAndStatus(new BigDecimal(rtacode), "Y");
    }

    @Override
    public void deleteRtaDocument(long rtadoccode) {
        tblRtadocumentRepo.delete(tblRtadocumentRepo.findById(rtadoccode).orElse(null));
    }

    @Override
    public List<RtaDuplicatesResponse> getRtaDuplicates(String rtacasecode) {
        TblRtaclaim tblRtaclaim = tblRtaclaimRepo.findById(Long.valueOf(rtacasecode)).orElse(null);
        if (tblRtaclaim != null) {
            List<RtaDuplicatesResponse> rtaDuplicatesResponses = new ArrayList<>();
            RtaDuplicatesResponse rtaDuplicatesResponse = new RtaDuplicatesResponse();

            List<Object> vehicalDuplicates = tblRtaclaimRepo.duplicatesForVehicle(tblRtaclaim.getRegisterationno());
            List<Object> mobileDuplicates = tblRtaclaimRepo.duplicatesForMobile(tblRtaclaim.getMobile());
            List<Object> partyRegNoDuplicates = tblRtaclaimRepo.duplicatesForMobile(tblRtaclaim.getPartycontactno());

            if(tblRtaclaim.getNinumber() != null) {
                if (!tblRtaclaim.getNinumber().equalsIgnoreCase("Will be provided to the solicitor") ||
                        !tblRtaclaim.getNinumber().equalsIgnoreCase("Solicitor obtained Number") ||
                        !tblRtaclaim.getNinumber().equalsIgnoreCase("Minor")) {
                    List<Object> niDuplicates = tblRtaclaimRepo.duplicatesForNiNumber(tblRtaclaim.getNinumber());

                    if (niDuplicates != null && niDuplicates.size() > 0) {
                        for (Object record : niDuplicates) {
                            rtaDuplicatesResponse = new RtaDuplicatesResponse();
                            Object[] row = (Object[]) record;

                            rtaDuplicatesResponse.setDuplicateType("Ni-Number");
                            rtaDuplicatesResponse.setCaseNumber((String) row[1]);
                            rtaDuplicatesResponse.setFullName((String) row[2]);
                            rtaDuplicatesResponse.setRtacode(((BigDecimal) row[3]).toString());

                            rtaDuplicatesResponses.add(rtaDuplicatesResponse);

                        }
                    }
                }
            }

            if (vehicalDuplicates != null && vehicalDuplicates.size() > 0) {
                for (Object record : vehicalDuplicates) {
                    rtaDuplicatesResponse = new RtaDuplicatesResponse();
                    Object[] row = (Object[]) record;

                    rtaDuplicatesResponse.setDuplicateType("VRN");
                    rtaDuplicatesResponse.setCaseNumber((String) row[1]);
                    rtaDuplicatesResponse.setFullName((String) row[2]);
                    rtaDuplicatesResponse.setRtacode(((BigDecimal) row[3]).toString());

                    rtaDuplicatesResponses.add(rtaDuplicatesResponse);

                }
            }

			/*if (mobileDuplicates != null && mobileDuplicates.size() > 0) {
				for (Object record : mobileDuplicates) {
					rtaDuplicatesResponse = new RtaDuplicatesResponse();
					Object[] row = (Object[]) record;

					rtaDuplicatesResponse.setDuplicateTyep("MOB");
					rtaDuplicatesResponse.setCaseNumber((String) row[1]);
					rtaDuplicatesResponse.setFullName((String) row[2]);
					rtaDuplicatesResponse.setRtacode(((BigDecimal) row[3]).toString());

					rtaDuplicatesResponses.add(rtaDuplicatesResponse);

				}
			}*/

            if (partyRegNoDuplicates != null && partyRegNoDuplicates.size() > 0) {
                for (Object record : partyRegNoDuplicates) {
                    rtaDuplicatesResponse = new RtaDuplicatesResponse();
                    Object[] row = (Object[]) record;

                    rtaDuplicatesResponse.setDuplicateType("TP-VRN");
                    rtaDuplicatesResponse.setCaseNumber((String) row[1]);
                    rtaDuplicatesResponse.setFullName((String) row[2]);
                    rtaDuplicatesResponse.setRtacode(((BigDecimal) row[3]).toString());

                    rtaDuplicatesResponses.add(rtaDuplicatesResponse);

                }
            }

            return rtaDuplicatesResponses;

        } else {
            return null;
        }
    }

    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblRtaclaim tblRtaclaim,
                             TblUser tblUser) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblRtamessage tblRtamessage = new TblRtamessage();

        tblRtamessage.setTblRtaclaim(tblRtaclaim);
        tblRtamessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblRtamessage.setCreatedon(new Date());
        tblRtamessage.setMessage(tblEmail.getEmailbody());
        tblRtamessage.setSentto(tblEmail.getEmailaddress());
        tblRtamessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());
        tblRtamessage.setEmailcode(tblEmail);

        tblRtamessage = tblRtamessageRepo.save(tblRtamessage);

        return true;
    }

    @Override
    public boolean saveEmail(String emailAddress, String emailBody, String emailSubject, TblRtaclaim tblRtaclaim,
                             TblUser tblUser, String attachment) {
        TblEmail tblEmail = new TblEmail();
        tblEmail.setSenflag(new BigDecimal(0));
        tblEmail.setEmailaddress(emailAddress);
        tblEmail.setEmailbody(emailBody);
        tblEmail.setEmailsubject(emailSubject);
        tblEmail.setEmailattachment(attachment);
        tblEmail.setCreatedon(new Date());

        tblEmail = tblEmailRepo.save(tblEmail);

        TblRtamessage tblRtamessage = new TblRtamessage();

        tblRtamessage.setTblRtaclaim(tblRtaclaim);
        tblRtamessage.setUserName(tblUser == null ? null : tblUser.getLoginid());
        tblRtamessage.setCreatedon(new Date());
        tblRtamessage.setMessage(tblEmail.getEmailbody());
        tblRtamessage.setSentto(tblEmail.getEmailaddress());
        tblRtamessage.setUsercode(tblUser == null ? null : tblUser.getUsercode());

        tblRtamessage = tblRtamessageRepo.save(tblRtamessage);

        return true;
    }

    @Override
    public TblRtaclaim performRevertActionOnRta(String rtacode, String toStatus, String reason, TblUser tblUser) {
       /* TblRtaclaim oldTblRtaclaim = tblRtaclaimRepo.findById(Long.valueOf(rtacode)).orElse(null);
        TblRtalog tblRtalog = null;
        List<TblRtalog> tblRtalogs = null;
        if (oldTblRtaclaim.getRtalogcode() != null) {
            tblRtalogs = tblRtalogRepo.getRtaAuditLogs(Long.valueOf(oldTblRtaclaim.getRtalogcode().toString()), oldTblRtaclaim.getRtacode());
            tblRtalog = tblRtalogs.get(0);
        } else {
            tblRtalogs = tblRtalogRepo
                    .findByTblRtaclaimRtacodeOrderByRtalogcodeDesc(Long.valueOf(rtacode));
            tblRtalog = tblRtalogs.get(0);
        }

        if (oldTblRtaclaim.getStatuscode().equals("2")) {
            int updateSolicitor = tblRtasolicitorRepo.updateSolicitor("N", new BigDecimal(rtacode));
        }

        TblRtaclaim tblRtaclaim = performActionRevertOnRta(rtacode, tblRtalog, tblUser);

        if (tblRtaclaim != null) {
            return tblRtaclaim;
        } else {
            return null;
        }*/

        Session session = em.unwrap(Session.class);
        final String[] msg = new String[1];
        final int[] status = new int[1];

        session.doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                CallableStatement call = connection.prepareCall("{call PROC_REVERT(?,?,?,?) }");
                call.setString(1, "RTA");
                call.setLong(2, Long.parseLong(rtacode));
                call.registerOutParameter(3, Types.INTEGER);
                call.registerOutParameter(4, Types.VARCHAR);
                call.execute();
                status[0] = call.getInt(3);
                msg[0] = call.getString(4);
            }
        });

        if (status[0] == 1) {
            return tblRtaclaimRepo.findById(Long.valueOf(rtacode)).orElse(null);
        } else {
            return null;
        }
    }

    private TblRtaclaim performActionRevertOnRta(String rtaCode, TblRtalog tblRtalog, TblUser tblUser) {

        TblRtaclaim rtaclaim = tblRtaclaimRepo.findById(Long.valueOf(rtaCode)).orElse(null);

        int update = tblRtaclaimRepo.performRevertAction(String.valueOf(tblRtalog.getOldstatus()), Long.valueOf(rtaCode),
                new BigDecimal(tblRtalog.getRtalogcode()));
        if (update > 0) {

            TblRtaclaim tblRtaclaim = new TblRtaclaim();
            TblRtalog tblRtalogObject = new TblRtalog();

            TblRtastatus tblStatus = tblRtastatusRepo.findById(Long.valueOf(tblRtalog.getOldstatus())).orElse(null);
            tblRtaclaim.setRtacode(Long.valueOf(rtaCode));

            tblRtalogObject.setCreatedon(new Date());
            tblRtalogObject.setDescr(tblStatus.getDescr());
            tblRtalogObject.setUsercode(tblUser.getUsercode());
            tblRtalogObject.setTblRtaclaim(tblRtaclaim);
            tblRtalogObject.setOldstatus(Long.valueOf(rtaclaim.getStatuscode()));
            tblRtalogObject.setNewstatus(Long.valueOf(tblRtalog.getOldstatus()));

            tblRtalogObject = tblRtalogRepo.save(tblRtalogObject);
            TblEmailTemplate tblEmailTemplate = tblEmailTemplateRepo.findByEmailtype("rta-status");

            // for introducer
            TblUser introducerUser = tblUsersRepo.findByUsercode(String.valueOf(rtaclaim.getAdvisor()));
            String introducerbody = tblEmailTemplate.getEmailtemplate();

            introducerbody = introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
            introducerbody = introducerbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            introducerbody = introducerbody.replace("[CLIENT_NAME]",
                    rtaclaim.getFirstname() + " "
                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                            + rtaclaim.getLastname());
            introducerbody = introducerbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            introducerbody = introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

//            saveEmail(introducerUser.getUsername(), introducerbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, introducerUser);

            // for legal internal
            TblUser legalUser = tblUsersRepo.findByUsercode("2");
            String legalbody = tblEmailTemplate.getEmailtemplate();

            legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
            legalbody = legalbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
            legalbody = legalbody.replace("[CLIENT_NAME]",
                    rtaclaim.getFirstname() + " "
                            + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                            + rtaclaim.getLastname());
            legalbody = legalbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
            legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

//            saveEmail(legalUser.getUsername(), legalbody,
//                    rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, legalUser);

            if (tblRtalog.getOldstatus().equals(1)) {
                // when reverting a case takes to new status remove all solcitors from case if
                // any
                int updateSolicitor = tblRtasolicitorRepo.updateSolicitor("N", new BigDecimal(rtaCode));
                TblRtasolicitor tblRtasolicitor = tblRtasolicitorRepo
                        .findByRtacodeAndStatus(new BigDecimal(rtaclaim.getRtacode()), "Y");
                if (tblRtasolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblRtasolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                            rtaclaim.getFirstname() + " "
                                    + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                                    + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            } else {
                // for normal emails
                TblRtasolicitor tblRtasolicitor = tblRtasolicitorRepo
                        .findByRtacodeAndStatus(new BigDecimal(rtaclaim.getRtacode()), "Y");
                if (tblRtasolicitor != null) {
                    // for solicitor
                    TblUser solicitorUser = tblUsersRepo.findById(tblRtasolicitor.getUsercode()).orElse(null);
                    String solicitorbody = tblEmailTemplate.getEmailtemplate();

                    solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                    solicitorbody = solicitorbody.replace("[CASE_NUMBER]", rtaclaim.getRtanumber());
                    solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                            rtaclaim.getFirstname() + " "
                                    + (rtaclaim.getMiddlename() == null ? "" : rtaclaim.getMiddlename() + " ")
                                    + rtaclaim.getLastname());
                    solicitorbody = solicitorbody.replace("[STATUS_DESCR]", tblStatus.getDescr());
                    solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + rtaclaim.getRtacode());

//                    saveEmail(solicitorUser.getUsername(), solicitorbody,
//                            rtaclaim.getRtanumber() + " | Processing Note", rtaclaim, solicitorUser);
                }
            }

            // if submmitting a case add clawback date current date + 90 days
            TblRtaclaim tblRtaclaimForClawBack = tblRtaclaimRepo.findById(Long.valueOf(rtaCode)).orElse(null);
            if (tblRtalog.getOldstatus().equals("7")) {
                Date clawbackDate = addDaysToDate(90);
                tblRtaclaimRepo.updateSubmitDate(new Date(), Long.valueOf(rtaCode));
                tblRtaclaimRepo.updateClawbackdate(clawbackDate, Long.valueOf(rtaCode));
            } else if (tblRtalog.getOldstatus().equals("18")) {
                Date clawbackDate = addDaysToSpecificDate(tblRtaclaimForClawBack.getClawbackDate(), 30);
                tblRtaclaimRepo.updateClawbackdate(clawbackDate, Long.valueOf(rtaCode));
            } else if (tblRtalog.getOldstatus().equals("19")) {
                Date clawbackDate = addDaysToSpecificDate(tblRtaclaimForClawBack.getClawbackDate(), 60);
                tblRtaclaimRepo.updateClawbackdate(clawbackDate, Long.valueOf(rtaCode));
            } else if (tblRtalog.getOldstatus().equals("20")) {
                Date clawbackDate = addDaysToSpecificDate(tblRtaclaimForClawBack.getClawbackDate(), 90);
                tblRtaclaimRepo.updateClawbackdate(clawbackDate, Long.valueOf(rtaCode));
            } else if (tblRtalog.getOldstatus().equals("5")) {
                TblRtanote tblRtanote = new TblRtanote();
                TblRtasolicitor tblRtasolicitor = tblRtasolicitorRepo
                        .findByRtacodeAndStatus(new BigDecimal(rtaclaim.getRtacode()), "Y");
                TblCompanyprofile SolicitorCompany = tblCompanyprofileRepo.findById(tblRtasolicitor.getCompanycode())
                        .orElse(null);
                // NOte for Legal internal user and NOTE for introducer
                tblRtanote.setTblRtaclaim(rtaclaim);
                tblRtanote.setNote("Case Accepted By " + SolicitorCompany.getName());
                tblRtanote.setUsercategorycode("1");
                tblRtanote.setCreatedon(new Date());
                tblRtanote.setUsercode("1");

                addNoteToRta(tblRtanote);
            }
            return rtaclaim;
        } else {
            return null;
        }
    }

    @Override
    public List<RtaAuditLogResponse> getRtaAuditLogs(String rtacasecode) {
        List<RtaAuditLogResponse> rtaAuditLogResponses = new ArrayList<>();
        RtaAuditLogResponse rtaAuditLogResponse = null;
        List<Object> auditlogsObject = tblRtaclaimRepo.getRtaAuditLogs(Long.valueOf(rtacasecode));
        RtaStatusCountList PlStatusCountList;
        if (auditlogsObject != null && auditlogsObject.size() > 0) {
            for (Object record : auditlogsObject) {
                rtaAuditLogResponse = new RtaAuditLogResponse();
                Object[] row = (Object[]) record;

                rtaAuditLogResponse.setFieldName((String) row[0]);
                rtaAuditLogResponse.setOldValue((String) row[1]);
                rtaAuditLogResponse.setNewValue((String) row[2]);
                rtaAuditLogResponse.setAuditDate((Date) row[3]);
                rtaAuditLogResponse.setLoggedUser((String) row[4]);

                rtaAuditLogResponses.add(rtaAuditLogResponse);
            }
            return rtaAuditLogResponses;
        } else {
            return null;
        }
    }

    @Override
    public TblEmail resendRtaEmail(String rtamessagecode) {
        TblRtamessage tblRtamessage = tblRtamessageRepo.findById(Long.valueOf(rtamessagecode)).orElse(null);

        TblEmail tblEmail = tblEmailRepo.findById(tblRtamessage.getEmailcode().getEmailcode()).orElse(null);

        tblEmail.setSenflag(new BigDecimal(0));

        return tblEmailRepo.saveAndFlush(tblEmail);
    }

    @Override
    public TblEmailTemplate findByEmailType(String emailType) {
        return tblEmailTemplateRepo.findByEmailtype(emailType);
    }

    @Override
    public TblUser findByUserId(String valueOf) {
        return tblUsersRepo.findByUsercode(valueOf);
    }

    @Override
    public List<TblRtainjury> getInjuriesOfRta(long rtacode) {
        return tblRtainjuryRepo.findByTblRtaclaimRtacode(rtacode);
    }


    @Override
    public List<RtaAuditLogResponse> getTodayRtaAuditLogs(long rtacode) {
        List<RtaAuditLogResponse> rtaAuditLogResponses = new ArrayList<>();
        RtaAuditLogResponse rtaAuditLogResponse = null;
        List<Object> auditlogsObject = tblRtaclaimRepo.getTodayRtaAuditLogs(rtacode);
        RtaStatusCountList PlStatusCountList;
        if (auditlogsObject != null && auditlogsObject.size() > 0) {
            for (Object record : auditlogsObject) {
                rtaAuditLogResponse = new RtaAuditLogResponse();
                Object[] row = (Object[]) record;

                rtaAuditLogResponse.setFieldName((String) row[0]);
                rtaAuditLogResponse.setOldValue((String) row[1]);
                rtaAuditLogResponse.setNewValue((String) row[2]);
                rtaAuditLogResponse.setAuditDate((Date) row[3]);
                rtaAuditLogResponse.setLoggedUser((String) row[4]);

                rtaAuditLogResponses.add(rtaAuditLogResponse);
            }
            return rtaAuditLogResponses;
        } else {
            return null;
        }
    }

    @Override
    public TblCompanydoc getCompanyDocsRtaForBike(String companycode) {
        return tblCompanydocRepo.findByTblCompanyprofileCompanycodeAndTblCompaignCompaigncodeAndBike(companycode, "1", "Y");
    }

    @Override
    public List<ViewRtalistintorducers> getFilterAuthRtaCasesIntroducers(FilterRequest filterRequest, String usercode) {
        return viewRtalistintorducersRepo.getFilterRtaCasesListForIntroducer(filterRequest.getName(), filterRequest.getPostCode(), filterRequest.getPortalCode(),
                filterRequest.getMobileNo(), filterRequest.getEmail(), filterRequest.getNiNumber(), filterRequest.getRegNo(), usercode);
    }

    @Override
    public List<ViewRtalistsolicitors> getFilterAuthRtaCasesSolicitors(FilterRequest filterRequest, String usercode) {
        return viewRtalistSolicitorsRepo.getFilterRtaCasesListForSolicitor(filterRequest.getName(), filterRequest.getPostCode(), filterRequest.getPortalCode(),
                filterRequest.getMobileNo(), filterRequest.getEmail(), filterRequest.getNiNumber(), filterRequest.getRegNo(), usercode);
    }

    @Override
    public List<ViewRtalistintorducers> getFilterAuthRtaCasesLegalAssist(FilterRequest filterRequest) {
        return viewRtalistintorducersRepo.getFilterRtaCasesListForLegalAssist(filterRequest.getName(), filterRequest.getPostCode(), filterRequest.getPortalCode(),
                filterRequest.getMobileNo(), filterRequest.getEmail(), filterRequest.getNiNumber(), filterRequest.getRegNo());
    }

    @Override
    public TblRtaclaim getTblRtaclaimByRefNo(String claimRefNo) {
        return tblRtaclaimRepo.findByRtanumber(claimRefNo);
    }
}
