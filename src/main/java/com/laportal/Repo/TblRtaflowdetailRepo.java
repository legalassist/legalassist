package com.laportal.Repo;

import com.laportal.model.TblRtaflowdetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblRtaflowdetailRepo extends JpaRepository<TblRtaflowdetail, Long> {

    @Query(value = " select d.* from  Tbl_Rtaflowdetail d ,Tbl_Rtaflow f where d.rtaflowcode = f.rtaflowcode And f.compaigncode = :compaingCode And f.statuscode = :statusCode And d.listusercategory  like '%'||:usercatcode||'%'  and d.listdisplay = 'N'", nativeQuery = true)
    List<TblRtaflowdetail> getCompaingAndStatusWiseFlow(@Param("statusCode") Long statusCode, @Param("compaingCode") String compaingCode, @Param("usercatcode") String usercatcode);

    @Query(value = " select d.* from  Tbl_Rtaflowdetail d ,Tbl_Rtaflow f where d.rtaflowcode = f.rtaflowcode And f.compaigncode = :compaingCode And f.statuscode = :statusCode And d.listusercategory  like '%'||:usercatcode||'%'  and d.listdisplay = 'Y'", nativeQuery = true)
    List<TblRtaflowdetail> getCompaingAndStatusWiseFlowForLAUser(@Param("statusCode") Long statusCode, @Param("compaingCode") String compaingCode, @Param("usercatcode") String usercatcode);

}
