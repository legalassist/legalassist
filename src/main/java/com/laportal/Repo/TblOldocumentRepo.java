package com.laportal.Repo;

import com.laportal.model.TblOldocument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblOldocumentRepo extends JpaRepository<TblOldocument, Long> {

    List<TblOldocument> findByTblOlclaimOlclaimcode(Long olclaimcode);
}
