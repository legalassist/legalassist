package com.laportal.Repo;

import com.laportal.model.TblPldocument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblPldocumentRepo extends JpaRepository<TblPldocument, Long> {

    List<TblPldocument> findByTblPlclaimPlclaimcode(long plClaimCode);
}
