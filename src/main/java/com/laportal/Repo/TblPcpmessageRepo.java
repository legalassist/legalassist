package com.laportal.Repo;

import com.laportal.model.TblPcpmessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblPcpmessageRepo extends JpaRepository<TblPcpmessage, Long> {
    List<TblPcpmessage> findByTblPcpclaimPcpclaimcode(Long pcpcode);
}
