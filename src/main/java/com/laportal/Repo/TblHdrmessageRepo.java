package com.laportal.Repo;

import com.laportal.model.TblHdrmessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblHdrmessageRepo extends JpaRepository<TblHdrmessage, Long> {

    List<TblHdrmessage> findByTblHdrclaimHdrclaimcode(Long hdrClaimCode);
}
