package com.laportal.Repo;

import com.laportal.model.ViewRtaclamin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ViewRtaclaminRepo extends JpaRepository<ViewRtaclamin, BigDecimal> {

    ViewRtaclamin findByRtacode(BigDecimal rtaCode);


}
