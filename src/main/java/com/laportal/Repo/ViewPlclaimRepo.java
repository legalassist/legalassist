package com.laportal.Repo;

import com.laportal.model.ViewElclaim;
import com.laportal.model.ViewOlclaim;
import com.laportal.model.ViewPlclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ViewPlclaimRepo extends JpaRepository<ViewPlclaim, BigDecimal> {
    @Query(value = "SELECT *\n" +
            "  FROM VIEW_PLCLAIM\n" +
            " WHERE :LOGINUSER = CASE WHEN :USERCATEGORY = 1 THEN ADVISOR\n" +
            "                         WHEN :USERCATEGORY = 2 THEN SOLICITOR_USERCODE\n" +
            "                         WHEN :USERCATEGORY = 4 THEN :LOGINUSER\n" +
            "                    END", nativeQuery = true)
    List<ViewPlclaim> getAuthPlCasesUserAndCategoryWise(@Param("LOGINUSER") BigDecimal usercode,@Param("USERCATEGORY") BigDecimal companyCode);

    @Query(value = "SELECT *\n" +
            "  FROM VIEW_PLCLAIM\n" +
            " WHERE :LOGINUSER = CASE WHEN :USERCATEGORY = 1 THEN ADVISOR\n" +
            "                         WHEN :USERCATEGORY = 2 THEN SOLICITOR_USERCODE\n" +
            "                         WHEN :USERCATEGORY = 4 THEN :LOGINUSER\n" +
            "                    END \n" +
            "And statuscode = :statusCode \n" +
            "order by createdate desc",nativeQuery = true)
    List<ViewPlclaim> getAuthPlCasesUserAndCategoryWiseAndStatusWise(@Param("LOGINUSER") BigDecimal userCode, @Param("USERCATEGORY") BigDecimal userCatCode, @Param("statusCode") BigDecimal statusCode);
}
