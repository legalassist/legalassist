package com.laportal.Repo;

import com.laportal.model.TblDbclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TblDbclaimRepo extends JpaRepository<TblDbclaim, Long> {

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE\n" +
            "               FROM TBL_DbCLAIM C  \n" +
            "               RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE WHERE S.COMPAIGNCODE = 3  \n" +
            "               GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllDbStatusCounts();

    @Query(value = "select c.Dbclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, \n" +
            "       c.Dbcode as code, CLAIMANT_NAME as client, s.descr status \n" +
            "    from tbl_Dbclaim c \n" +
            "    left outer  join tbl_rtastatus s on s.statuscode = c.status \n", nativeQuery = true)
    List<Object> getDbCasesListUserWise();

    @Query(value = "select c.Dbclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.Dbcode as code, CLAIMANT_NAME as client, s.descr status \n" +
            "            from TBL_DbCLAIM c \n" +
            "            left outer  join tbl_rtastatus s on s.statuscode = c.status \n" +
            "            where c.status = :statusId", nativeQuery = true)
    List<Object> getDbCasesListStatusWise(@Param("statusId") long statusId);

    @Modifying
    @Query(value = " UPDATE  TblDbclaim SET status = :statusCode WHERE Dbclaimcode = :Dbclaimcode")
    int performAction(@Param("statusCode") BigDecimal statusCode, @Param("Dbclaimcode") Long Dbclaimcode);

    @Modifying
    @Query(value = " UPDATE  TblDbclaim SET remarks = :reason, status = :statusCode WHERE Dbclaimcode = :Dbclaimcode")
    int performRejectCancelAction(@Param("reason") String reason, @Param("statusCode") String toStatus, @Param("Dbclaimcode") Long Dbclaimcode);

    @Query(value = "SELECT Dbclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, CREATEDBY\n" +
            "FROM VIEW_DbCLAIMINTRODUCERS\n" +
            "WHERE CREATEDBY = :userId", nativeQuery = true)
    List<Object> getDbCasesListForIntroducers(@Param("userId") String usercode);

    @Query(value = "SELECT Dbclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, SOLICITORCODE\n" +
            "FROM VIEW_DbCLAIMSOLICITORS\n" +
            "WHERE SOLICITORCODE = :userId", nativeQuery = true)
    List<Object> getDbCasesListForSolicitor(@Param("userId") String usercode);

    @Query(value = "SELECT Dbclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate\n" +
            "FROM VIEW_DbCLAIM", nativeQuery = true)
    List<Object> getDbCasesList();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_DbCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "INNER JOIN TBL_USERS U ON U.USERCODE = C.CREATEUSER\n" +
            "inner join tbl_Dbsolicitor hs on c.Dbclaimcode = hs.Dbclaimcode and hs.status = 'Y'\n" +
            "inner join tbl_users uu on uu.usercode = hs.usercode\n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode\n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllDbStatusCountsForSolicitors();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_DbCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllDbStatusCountsForIntroducers();
}
