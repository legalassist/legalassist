package com.laportal.Repo;

import com.laportal.model.TblRtaPassengerInjury;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblRtaPassengerInjuryRepo extends JpaRepository<TblRtaPassengerInjury, Long> {
}
