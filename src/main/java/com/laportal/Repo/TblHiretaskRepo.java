package com.laportal.Repo;

import com.laportal.model.TblHiretask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblHiretaskRepo extends JpaRepository<TblHiretask,Long> {

    List<TblHiretask> findByTblHireclaimHirecode(Long hirecode);
    TblHiretask findByTblHireclaimHirecodeAndTblTaskTaskcode(Long hirecode,Long taskCode);
}
