package com.laportal.Repo;

import com.laportal.model.ViewRtalistsolicitors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;


public interface ViewRtalistSolicitorsRepo extends JpaRepository<ViewRtalistsolicitors, BigDecimal> {


    List<ViewRtalistsolicitors> findByUsercode(String userCode);

    List<ViewRtalistsolicitors> findByCompanycode(String companyCode);

    List<ViewRtalistsolicitors> findBySolictercode(String usercode);

    List<ViewRtalistsolicitors> findBySolictercodeAndStatuscode(String usercode, String statusId);

    @Query(value = "SELECT * \n" +
            "  FROM VIEW_RTALISTSOLICITORS\n" +
            " WHERE UPPER(CLIENT) LIKE UPPER('%'||:NAME||'%')\n" +
            "   AND NVL(UPPER(POSTALCODE),'0') LIKE NVL(UPPER('%'||:POSTALCODE||'%'),'0')\n" +
            "   AND UPPER(RTANUMBER) LIKE UPPER('%'||:PORTALCODE||'%')\n" +
            "   AND NVL(MOBILE,'0') LIKE NVL('%'||:MOBILENO||'%','0')\n" +
            "   AND NVL(UPPER(EMAIL),'0') LIKE NVL(UPPER('%'||:EMAIL||'%'),'0')\n" +
            "   AND NVL(UPPER(NINUMBER),'0') LIKE NVL(UPPER('%'||:NINUMBER||'%'),'0')\n" +
            "   AND NVL(UPPER(REGISTERATIONNO),'0') LIKE NVL(UPPER('%'||:REGISTERATIONNO||'%'),'0')" +
            "   AND solictercode = :solicitorCode", nativeQuery = true)
    List<ViewRtalistsolicitors> getFilterRtaCasesListForSolicitor(@Param("NAME") String name,
                                                    @Param("POSTALCODE") String postcode,
                                                    @Param("PORTALCODE") String portalCode,
                                                    @Param("MOBILENO") String mobileNo,
                                                    @Param("EMAIL") String email,
                                                    @Param("NINUMBER") String niNumber,
                                                    @Param("REGISTERATIONNO") String regNo,
                                                                  @Param("solicitorCode") String solicitorCode);
}
