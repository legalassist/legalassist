package com.laportal.Repo;

import com.laportal.model.TblRtamessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblRtamessageRepo extends JpaRepository<TblRtamessage, Long> {

    List<TblRtamessage> findAllByTblRtaclaimRtacode(long rtaCode);
}
