package com.laportal.Repo;

import com.laportal.model.TblTenancydocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblTenancydocumentRepo extends JpaRepository<TblTenancydocument, Long> {
}
