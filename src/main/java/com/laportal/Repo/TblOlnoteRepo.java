package com.laportal.Repo;

import com.laportal.model.TblElnote;
import com.laportal.model.TblOlnote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblOlnoteRepo extends JpaRepository<TblOlnote, Long> {

    @Query(value = "select * from (select *   \n" +
            "                            from tbl_Olnotes  \n" +
            "                           where usercode = :usercode    \n" +
            "                             and olClaimCode = :olClaimCode   \n" +
            "                              \n" +
            "                        union   \n" +
            "                              \n" +
            "                         select *   \n" +
            "                           from tbl_Olnotes   \n" +
            "                          where usercategorycode = :categorycode    \n" +
            "                            and olClaimCode = :olClaimCode   )order by createdon asc ", nativeQuery = true)
    List<TblOlnote> findNotesOnOlbyUserAndCategory(@Param("olClaimCode") Long olClaimCode, @Param("categorycode") String categorycode, @Param("usercode") String usercode);

    @Query(value = "  select n.* from TBL_COMPANYPROFILE c, tbl_users u,tbl_Olnotes n\n" +
            "             where c.companycode = u.companycode\n" +
            "             and c.categorycode = '4'\n" +
            "              and n.usercategorycode = '4'\n" +
            "             and u.usercode = n.usercode\n" +
            "             and u.usercode = :usercode" +
            "             and n.olClaimCode = :olClaimCode" +
            "             order by n.createdon asc ", nativeQuery = true)
    List<TblOlnote> getAuthOlCaseNotesOfLegalInternal(@Param("olClaimCode")Long elcode, @Param("usercode") String usercode);
}
