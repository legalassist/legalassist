package com.laportal.Repo;


import com.laportal.model.TblTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblTaskRepo extends JpaRepository<TblTask, Long> {

    @Query(value = "select * from tbl_tasks where COMPAIGNCODE = :compaigncode and status = 'Y'", nativeQuery = true)
    List<TblTask> findTaskById(@Param("compaigncode") String compaingCode);
}
