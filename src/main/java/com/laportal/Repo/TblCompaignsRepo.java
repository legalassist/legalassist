package com.laportal.Repo;

import com.laportal.model.TblCompaign;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblCompaignsRepo extends JpaRepository<TblCompaign, String> {
    //    List<TblCompaign> findByStatus(String status);
    TblCompaign findByCompaignname(String code);
}
