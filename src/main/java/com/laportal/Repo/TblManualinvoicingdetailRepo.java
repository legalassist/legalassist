package com.laportal.Repo;

import com.laportal.model.TblManualinvoicingdetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblManualinvoicingdetailRepo extends JpaRepository<TblManualinvoicingdetail, Long> {
}
