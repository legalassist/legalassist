package com.laportal.Repo;

import com.laportal.model.TblPcpclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TblPcpclaimRepo extends JpaRepository<TblPcpclaim, Long> {

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE\n" +
            "               FROM TBL_PCPCLAIM C  \n" +
            "               RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE WHERE S.COMPAIGNCODE = 3  \n" +
            "               GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllPcpStatusCounts();

    @Query(value = "select c.pcpclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, \n" +
            "       c.pcpcode as code, full_name as client, s.descr status \n" +
            "    from tbl_pcpclaim c \n" +
            "    left outer  join tbl_rtastatus s on s.statuscode = c.status \n", nativeQuery = true)
    List<Object> getPcpCasesListUserWise();

    @Query(value = "select c.pcpclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.pcpcode as code, full_name as client, s.descr status \n" +
            "            from TBL_PCPCLAIM c \n" +
            "            left outer  join tbl_rtastatus s on s.statuscode = c.status \n" +
            "            where c.status = :statusId", nativeQuery = true)
    List<Object> getPcpCasesListStatusWise(@Param("statusId") long statusId);

    @Modifying
    @Query(value = " UPDATE  TblPcpclaim SET status = :statusCode WHERE pcpclaimcode = :pcpclaimcode")
    int performAction(@Param("statusCode") BigDecimal statusCode, @Param("pcpclaimcode") Long pcpclaimcode);

    @Modifying
    @Query(value = " UPDATE  TblPcpclaim SET remarks = :reason, status = :statusCode WHERE pcpclaimcode = :pcpclaimcode")
    int performRejectCancelAction(@Param("reason") String reason, @Param("statusCode") String toStatus, @Param("pcpclaimcode") Long pcpclaimcode);

    @Query(value = "SELECT pcpclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, CREATEDBY\n" +
            "FROM VIEW_PCPCLAIMINTRODUCERS\n" +
            "WHERE CREATEDBY = :userId", nativeQuery = true)
    List<Object> getPcpCasesListForIntroducers(@Param("userId") String usercode);

    @Query(value = "SELECT pcpclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, SOLICITORCODE\n" +
            "FROM VIEW_PCPCLAIMSOLICITORS\n" +
            "WHERE SOLICITORCODE = :userId", nativeQuery = true)
    List<Object> getPcpCasesListForSolicitor(@Param("userId") String usercode);

    @Query(value = "SELECT pcpclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate\n" +
            "FROM VIEW_PCPCLAIM", nativeQuery = true)
    List<Object> getPcpCasesList();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_PCPCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "INNER JOIN TBL_USERS U ON U.USERCODE = C.CREATEUSER\n" +
            "inner join tbl_pcpsolicitor hs on c.pcpclaimcode = hs.pcpclaimcode and hs.status = 'Y'\n" +
            "inner join tbl_users uu on uu.usercode = hs.usercode\n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode\n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllPcpStatusCountsForSolicitors();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_PCPCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllPcpStatusCountsForIntroducers();
}
