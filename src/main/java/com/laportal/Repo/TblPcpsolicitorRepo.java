package com.laportal.Repo;

import com.laportal.model.TblPcpsolicitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TblPcpsolicitorRepo extends JpaRepository<TblPcpsolicitor, Long> {
    TblPcpsolicitor findByTblPcpclaimPcpclaimcodeAndStatus(long pcpCaseId, String status);

    @Modifying
    @Query(value = " UPDATE TblPcpsolicitor SET status = :statusCode WHERE tblPcpclaim.pcpclaimcode = :pcpclaimcode")
    int updateSolicitor(@Param("statusCode") String statusCode, @Param("pcpclaimcode") long pcpclaimcode);

    @Modifying
    @Query(value = " UPDATE TblPcpsolicitor SET remarks = :remarks,status = :statusCode WHERE tblPcpclaim.pcpclaimcode = :pcpclaimcode")
    int updateRemarksReason(@Param("remarks") String remarks, @Param("statusCode") String statusCode, @Param("pcpclaimcode") long pcpclaimcode);
}
