package com.laportal.Repo;

import com.laportal.model.TblImmigrationdocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblImmigrationdocumentRepo extends JpaRepository<TblImmigrationdocument, Long> {
}
