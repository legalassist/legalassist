package com.laportal.Repo;

import com.laportal.model.TblRtadocument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblRtadocumentRepo extends JpaRepository<TblRtadocument, Long> {


    List<TblRtadocument> findByTblRtaclaimRtacode(Long rtaCode);
}
