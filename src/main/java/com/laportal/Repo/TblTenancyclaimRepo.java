package com.laportal.Repo;

import com.laportal.model.TblTenancyclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblTenancyclaimRepo extends JpaRepository<TblTenancyclaim, Long> {

    @Query(value = "select c.tenancyclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, \n" +
            "       c.tenancy_code as code, full_name as client, s.descr status \n" +
            "    from tbl_tenancyclaim c \n" +
            "    left outer  join tbl_rtastatus s on s.statuscode = c.status \n", nativeQuery = true)
    List<Object> getHdrCasesListUserWise();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE\n" +
            "               FROM TBL_TENANCYCLAIM C  \n" +
            "               RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE WHERE S.COMPAIGNCODE = 3  \n" +
            "               GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllTenancyStatusCounts();

    @Query(value = "select c.tenancyclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.tenancy_code as code, full_name as client, s.descr status \n" +
            "            from tbl_tenancyclaim c \n" +
            "            left outer  join tbl_rtastatus s on s.statuscode = c.status \n" +
            "            where c.status = :statusId", nativeQuery = true)
    List<Object> getTenancyCasesListStatusWise(@Param("statusId") long statusId);

    @Modifying
    @Query(value = " UPDATE  TblTenancyclaim SET status = :statusCode WHERE tenancyclaimcode = :tenancyclaimcode")
    int performAction(@Param("statusCode") Long statusCode, @Param("tenancyclaimcode") Long tenancyclaimcode);

    @Modifying
    @Query(value = " UPDATE  TblTenancyclaim SET remarks = :reason, status = :statusCode WHERE tenancyclaimcode = :tenancyclaimcode")
    int performRejectCancelAction(@Param("reason") String reason, @Param("statusCode") String toStatus, @Param("tenancyclaimcode") Long tenancyclaimcode);

    @Query(value = "SELECT tenancyclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, CREATEDBY\n" +
            "FROM VIEW_TENANCYCLAIMINTRODUCERS\n" +
            "WHERE CREATEDBY = :userId", nativeQuery = true)
    List<Object> getTenancyCasesListForIntroducers(@Param("userId") String usercode);

    @Query(value = "SELECT tenancyclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, SOLICITORCODE\n" +
            "FROM VIEW_TENANCYCLAIMSOLICITORS\n" +
            "WHERE SOLICITORCODE = :userId", nativeQuery = true)
    List<Object> getTenancyCasesListForSolicitor(@Param("userId") String usercode);

    @Query(value = "SELECT tenancyclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate\n" +
            "FROM VIEW_TENANCYCLAIM", nativeQuery = true)
    List<Object> getTenancyCasesList();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_TENANCYCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllTenancyStatusCountsForIntroducers();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_TENANCYCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "INNER JOIN TBL_USERS U ON U.USERCODE = C.CREATEUSER\n" +
            "inner join tbl_tenancysolicitor hs on c.tenancyclaimcode = hs.tenancyclaimcode and hs.status = 'Y'\n" +
            "inner join tbl_users uu on uu.usercode = hs.usercode\n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode\n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllTenancyStatusCountsForSolicitors();
}
