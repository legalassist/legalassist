package com.laportal.Repo;

import com.laportal.model.TblEllog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblEllogRepo extends JpaRepository<TblEllog, Long> {
    List<TblEllog> findByTblElclaimElclaimcode(Long elClaimCode);
}
