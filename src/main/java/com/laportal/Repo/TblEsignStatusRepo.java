package com.laportal.Repo;

import com.laportal.model.TblEsignStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;

public interface TblEsignStatusRepo extends JpaRepository<TblEsignStatus,Long> {

    TblEsignStatus findByTblCompaignCompaigncodeAndClaimcode(String compaignCode, BigDecimal claimCode);
}
