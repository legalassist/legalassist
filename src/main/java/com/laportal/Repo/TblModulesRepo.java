package com.laportal.Repo;

import com.laportal.model.TblModule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblModulesRepo extends JpaRepository<TblModule, String> {
    @Query(value = "SELECT SEQ_TBL_MODULES.nextval from dual", nativeQuery = true)
    int getSeq();

/*
    @Query(value = "select DISTINCT b.*\n" + "from tbl_userroles a,tbl_modules b,tbl_rolepages c,tbl_pages d\n"
            + "where a.rolecode = c.rolecode\n" + "and d.modulecode = b.modulecode\n" + "and c.pagecode = d.pagecode\n"
            + "and a.rolecode in (:roleCodes) ", nativeQuery = true)
    List<TblModule> getRoleWiseModule(@Param("roleCodes") List<String> roleCodes);
*/

    @Query(value = "select DISTINCT b.*\n" + "from tbl_modules b,tbl_rolepages c,tbl_pages d\n"
            + "where  d.modulecode = b.modulecode\n" + "and c.pagecode = d.pagecode\n"
            + "and c.rolecode in (:roleCodes) ", nativeQuery = true)
    List<TblModule> getRoleWiseModule(@Param("roleCodes") List<String> roleCodes);


    List<TblModule> findByModulestatus(String status);

}
