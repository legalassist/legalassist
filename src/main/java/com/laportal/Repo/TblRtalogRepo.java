package com.laportal.Repo;

import com.laportal.model.TblRtalog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblRtalogRepo extends JpaRepository<TblRtalog, Long> {

	@Query(value = "select * from tbl_rtalogs where rtacode = :rtacode"
			+ "   and rtalogcode < :rtalogcode order by rtalogcode desc fetch first 1 rows only ", nativeQuery = true)
    List<TblRtalog> getRtaAuditLogs(@Param("rtalogcode") Long rtalogcode, @Param("rtacode") Long rtacode);
	
    List<TblRtalog> findByTblRtaclaimRtacodeOrderByRtalogcodeDesc(Long rtaCode);

}
