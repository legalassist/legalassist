package com.laportal.Repo;

import com.laportal.model.TblHiremessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblHiremessageRepo extends JpaRepository<TblHiremessage, Long> {

    List<TblHiremessage> findAllByTblHireclaimHirecode(Long valueOf);
}
