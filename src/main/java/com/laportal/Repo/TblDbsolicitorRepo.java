package com.laportal.Repo;

import com.laportal.model.TblDbsolicitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TblDbsolicitorRepo extends JpaRepository<TblDbsolicitor, Long> {
    TblDbsolicitor findByTblDbclaimDbclaimcodeAndStatus(long DbCaseId, String status);

    @Modifying
    @Query(value = " UPDATE TblDbsolicitor SET status = :statusCode WHERE tblDbclaim.dbclaimcode = :Dbclaimcode")
    int updateSolicitor(@Param("statusCode") String statusCode, @Param("Dbclaimcode") long Dbclaimcode);

    @Modifying
    @Query(value = " UPDATE TblDbsolicitor SET remarks = :remarks,status = :statusCode WHERE tblDbclaim.dbclaimcode = :Dbclaimcode")
    int updateRemarksReason(@Param("remarks") String remarks, @Param("statusCode") String statusCode, @Param("Dbclaimcode") long Dbclaimcode);
}
