package com.laportal.Repo;

import com.laportal.model.TblInjclass;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblInjclassRepo extends JpaRepository<TblInjclass, Long> {


    List<TblInjclass> findByStatus(String status);

    List<TblInjclass> findByStatusAndAirbag(String status, String airBag);


}
