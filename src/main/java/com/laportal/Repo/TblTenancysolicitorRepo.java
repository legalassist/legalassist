package com.laportal.Repo;

import com.laportal.model.TblTenancysolicitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TblTenancysolicitorRepo extends JpaRepository<TblTenancysolicitor, Long> {

    @Modifying
    @Query(value = " UPDATE TblTenancysolicitor SET status = :statusCode WHERE tblTenancyclaim.tenancyclaimcode = :tenancyclaimcode")
    int updateSolicitor(@Param("statusCode") String statusCode, @Param("tenancyclaimcode") Long tenancyclaimcode);

    TblTenancysolicitor findByTblTenancyclaimTenancyclaimcodeAndStatus(Long tenancyClaimCode, String status);

    @Modifying
    @Query(value = " UPDATE TblTenancysolicitor SET remarks = :remarks,status = :statusCode WHERE tblTenancyclaim.tenancyclaimcode = :tenancyclaimcode")
    int updateRemarksReason(@Param("remarks") String remarks, @Param("statusCode") String statusCode, @Param("tenancyclaimcode") long tenancyclaimcode);
}
