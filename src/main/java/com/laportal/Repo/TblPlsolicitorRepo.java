package com.laportal.Repo;

import com.laportal.model.TblPlsolicitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TblPlsolicitorRepo extends JpaRepository<TblPlsolicitor, Long> {

    TblPlsolicitor findByTblPlclaimPlclaimcodeAndStatus(long DbCaseId, String status);

    @Modifying
    @Query(value = " UPDATE TblPlsolicitor SET status = :statusCode WHERE tblPlclaim.plclaimcode = :plclaimcode")
    int updateSolicitor(@Param("statusCode") String statusCode, @Param("plclaimcode") long plclaimcode);

    @Modifying
    @Query(value = " UPDATE TblPlsolicitor SET remarks = :remarks,status = :statusCode WHERE tblPlclaim.plclaimcode = :plclaimcode")
    int updateRemarksReason(@Param("remarks") String remarks, @Param("statusCode") String statusCode, @Param("plclaimcode") long plclaimcode);
}
