package com.laportal.Repo;

import com.laportal.model.TblHdrclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface TblHdrclaimRepo extends JpaRepository<TblHdrclaim, Long> {

    @Query(value = "select c.hdrclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.claimcode as code, hc.c_sname || ' ' || hc.c_fname || ' ' || hc.c_mname as client, ht.createdon as taskdue, t.name as taskname, s.descr status,\n" +
            "hc.c_email as email, hc.c_address1 || ' ' || hc.c_address2 || ' ' || hc.c_address3 as address, hc.c_mobileno as contactno, c.updatedate as lastUpdated, u.username as introducer, \n" +
            " (select hn.createdon from tbl_hdrnotes hn where hn.hdrclaimcode = c.hdrclaimcode order by createdon desc fetch first 1 rows only) as lastNotedate \n " +
            "            from tbl_hdrclaim c \n" +
            "            left outer  join tbl_hdrtasks ht on ht.hdrcode = c.hdrclaimcode and ht.currenttask = 'Y' \n" +
            "            left outer join tbl_hdrclaimant hc on hc.hdr_claimcode = c.hdrclaimcode\n" +
            "            left outer  join tbl_tasks t on t.taskcode = ht.taskcode \n" +
            "            left outer  join tbl_rtastatus s on s.statuscode = c.statuscode\n" +
            "            left outer join tbl_users u on u.usercode = c.createuser\n", nativeQuery = true)
    List<Object> getHdrCasesListUserWise(@Param("userId") String userId);

    @Query(value = "SELECT NVL(COUNT(C.STATUSCODE),0) STATUSCOUNT, S.DESCR, S.STATUSCODE " +
            "   FROM TBL_HDRCLAIM C " +
            "   RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUSCODE = S.STATUSCODE WHERE S.COMPAIGNCODE = 3 " +
            "AND s.show_status is null \n" +
            "GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc", nativeQuery = true)
    List<Object> getAllHdrStatusCounts();

    @Query(value = "select c.hdrclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.claimcode as code, hc.c_sname || ' ' || hc.c_fname || ' ' || hc.c_mname as client, ht.createdon as taskdue, t.name as taskname, s.descr status\n" +
            "from tbl_hdrclaim c\n" +
            "left outer  join tbl_hdrtasks ht on ht.hdrcode = c.hdrclaimcode and ht.currenttask = 'Y'\n" +
            "left outer  join tbl_hdrclaimant hc on hc.hdr_claimcode = c.hdrclaimcode\n" +
            "left outer  join tbl_tasks t on t.taskcode = ht.taskcode\n" +
            "left outer  join tbl_rtastatus s on s.statuscode = c.statuscode\n" +
            "where c.statuscode = :statusId", nativeQuery = true)
    List<Object> getHdrCasesListStatusWise(@Param("statusId") long statusId);

    @Modifying
    @Query(value = " UPDATE  TblHdrclaim SET statuscode = :statusCode WHERE hdrclaimcode = :hdrclaimcode")
    int performAction(@Param("statusCode") BigDecimal statusCode, @Param("hdrclaimcode") Long hdrclaimcode);

    @Modifying
    @Query(value = " UPDATE  TblHdrclaim SET remarks = :reason,statuscode = :statusCode WHERE hdrclaimcode = :hdrclaimcode")
    int performRejectCancelAction(@Param("reason") String reason, @Param("statusCode") BigDecimal statusCode, @Param("hdrclaimcode") Long hdrclaimcode);

    @Query(value = "SELECT hdrclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate\n" +
            "FROM VIEW_HDRCLAIM", nativeQuery = true)
    List<Object> getHdrCasesList();

    @Query(value = "SELECT hdrclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, SOLICITORCODE\n" +
            "FROM VIEW_HDRCLAIMSOLICITORS\n" +
            "WHERE SOLICITORCODE = :userId", nativeQuery = true)
    List<Object> getHdrCasesListForSolicitor(@Param("userId") String usercode);

    @Query(value = "SELECT hdrclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, CREATEDBY\n" +
            "FROM VIEW_HDRCLAIMINTRODUCERS\n" +
            "WHERE CREATEDBY = :userId", nativeQuery = true)
    List<Object> getHdrCasesListForIntroducers(@Param("userId") String usercode);

    @Query(value = "SELECT NVL(COUNT(C.STATUSCODE),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_HDRCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUSCODE = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 3  " +
            "AND C.introducer = :companyCode\n" +
            "AND s.show_status is null \n" +
            "GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc", nativeQuery = true)
    List<Object> getAllHdrStatusCountsForIntroducers(@Param("companyCode") String companyCode);

    @Query(value = "SELECT NVL(COUNT(C.STATUSCODE),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_HDRCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUSCODE = S.STATUSCODE \n" +
            "INNER JOIN TBL_USERS U ON U.USERCODE = C.CREATEUSER\n" +
            "inner join tbl_hdrsolicitor hs on c.hdrclaimcode = hs.hdrclaimcode and hs.status = 'Y' and hs.companycode = :companyCode\n" +
            "inner join tbl_users uu on uu.usercode = hs.usercode\n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode\n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "AND s.show_status is null \n" +
            "GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc", nativeQuery = true)
    List<Object> getAllHdrStatusCountsForSolicitor(@Param("companyCode") String companyCode);

    @Query(value = "select c.* \n" +
            "from tbl_hdrclaim c, tbl_hdrsolicitor s\n" +
            "where c.statuscode in (50,159)\n" +
            " and c.hdrclaimcode = s.hdrclaimcode" +
            " and s.usercode = :userCode", nativeQuery = true)
    List<TblHdrclaim> getGetCaseForInvoicing(@Param("userCode") String userCode);


    @Query(value = "SELECT *\n" +
            "FROM (\n" +
            "SELECT R.ATTRIBUTE_NAME, OLD_VALUE, NEW_VALUE, AUDIT_DATE, U.LOGINID, R.REFPK HDRCLAIMCODE\n" +
            "FROM LAUSER_AUDIT.TBL_HDRCLAIM R\n" +
            "LEFT JOIN TBL_USERS U ON R.USER_ID = U.USERCODE\n" +
            "\n" +
            "UNION ALL\n" +
            "\n" +
            "SELECT R.ATTRIBUTE_NAME, OLD_VALUE, NEW_VALUE, AUDIT_DATE, U.LOGINID, R.HDRCLAIMCODE\n" +
            "FROM LAUSER_AUDIT.TBL_HDRCLAIMANT R\n" +
            "LEFT JOIN TBL_USERS U ON R.USER_ID = U.USERCODE\n" +
            "\n" +
            "UNION ALL\n" +
            "\n" +
            "SELECT R.ATTRIBUTE_NAME, OLD_VALUE, NEW_VALUE, AUDIT_DATE, U.LOGINID, R.HDRCLAIMCODE\n" +
            "FROM LAUSER_AUDIT.TBL_HDRAFFECTEDPER R\n" +
            "LEFT JOIN TBL_USERS U ON R.USER_ID = U.USERCODE\n" +
            "\n" +
            "UNION ALL\n" +
            "\n" +
            "SELECT R.ATTRIBUTE_NAME, OLD_VALUE, NEW_VALUE, AUDIT_DATE, U.LOGINID, R.HDRCLAIMCODE\n" +
            "FROM LAUSER_AUDIT.TBL_HDRAFFECTEDROOM R\n" +
            "LEFT JOIN TBL_USERS U ON R.USER_ID = U.USERCODE\n" +
            "\n" +
            "UNION ALL\n" +
            "\n" +
            "SELECT R.ATTRIBUTE_NAME, OLD_VALUE, NEW_VALUE, AUDIT_DATE, U.LOGINID, R.HDRCLAIMCODE\n" +
            "FROM LAUSER_AUDIT.TBL_HDRJOINTTENANCY R\n" +
            "LEFT JOIN TBL_USERS U ON R.USER_ID = U.USERCODE\n" +
            "\n" +
            "UNION ALL\n" +
            "\n" +
            "SELECT R.ATTRIBUTE_NAME, OLD_VALUE, NEW_VALUE, AUDIT_DATE, U.LOGINID, R.HDRCLAIMCODE\n" +
            "FROM LAUSER_AUDIT.TBL_HDRTENANCY R\n" +
            "LEFT JOIN TBL_USERS U ON R.USER_ID = U.USERCODE\n" +
            "\n" +
            "UNION ALL\n" +
            "\n" +
            "SELECT R.ATTRIBUTE_NAME, OLD_VALUE, NEW_VALUE, AUDIT_DATE, U.LOGINID, R.HDRCLAIMCODE\n" +
            "FROM LAUSER_AUDIT.TBL_HDRSOLICITOR R\n" +
            "LEFT JOIN TBL_USERS U ON R.USER_ID = U.USERCODE\n" +
            "\n" +
            "UNION ALL\n" +
            "\n" +
            "SELECT R.ATTRIBUTE_NAME, OLD_VALUE, NEW_VALUE, AUDIT_DATE, U.LOGINID, R.HDRCLAIMCODE\n" +
            "FROM LAUSER_AUDIT.TBL_HDRTASKS R\n" +
            "LEFT JOIN TBL_USERS U ON R.USER_ID = U.USERCODE)\n" +
            "WHERE HDRCLAIMCODE = :hdrclaimcode\n" +
            "ORDER BY AUDIT_DATE", nativeQuery = true)
    List<Object> getHdrAuditLogs(Long hdrclaimcode);

    @Query(value = "SELECT * \n" +
            "  FROM VIEW_HDRCLAIMINTRODUCERS\n" +
            " WHERE UPPER(CLIENT) LIKE UPPER('%'||:NAME||'%')\n" +
            "   AND NVL(UPPER(POSTCODE),'0') LIKE NVL(UPPER('%'||:POSTCODE||'%'),'0')\n" +
            "   AND UPPER(CODE) LIKE UPPER('%'||:PORTALCODE||'%')\n" +
            "   AND NVL(CONTACTNO,'0') LIKE NVL('%'||:MOBILENO||'%','0')\n" +
            "   AND NVL(UPPER(EMAIL),'0') LIKE NVL(UPPER('%'||:EMAIL||'%'),'0')\n" +
            "   AND NVL(UPPER(NINUMBER),'0') LIKE NVL(UPPER('%'||:NINUMBER||'%'),'0')" +
            "   AND ADVISOR = :advisor", nativeQuery = true)
    List<Object> getFilterHdrCasesListForIntroducers(@Param("NAME") String name,
                                                     @Param("POSTCODE") String postcode,
                                                     @Param("PORTALCODE") String portalCode,
                                                     @Param("MOBILENO") String mobileNo,
                                                     @Param("EMAIL") String email,
                                                     @Param("NINUMBER") String niNumber,
                                                     @Param("advisor") String advisor);

    @Query(value = "SELECT * \n" +
            "  FROM VIEW_HDRCLAIMSOLICITORS\n" +
            " WHERE UPPER(CLIENT) LIKE UPPER('%'||:NAME||'%')\n" +
            "   AND NVL(UPPER(POSTCODE),'0') LIKE NVL(UPPER('%'||:POSTCODE||'%'),'0')\n" +
            "   AND UPPER(CODE) LIKE UPPER('%'||:PORTALCODE||'%')\n" +
            "   AND NVL(CONTACTNO,'0') LIKE NVL('%'||:MOBILENO||'%','0')\n" +
            "   AND NVL(UPPER(EMAIL),'0') LIKE NVL(UPPER('%'||:EMAIL||'%'),'0')\n" +
            "   AND NVL(UPPER(NINUMBER),'0') LIKE NVL(UPPER('%'||:NINUMBER||'%'),'0')" +
            "   AND SOLICITORCODE = :solicitorCode", nativeQuery = true)
    List<Object> getFiletrHdrCasesListForSolicitor(@Param("NAME") String name,
                                                   @Param("POSTCODE") String postcode,
                                                   @Param("PORTALCODE") String portalCode,
                                                   @Param("MOBILENO") String mobileNo,
                                                   @Param("EMAIL") String email,
                                                   @Param("NINUMBER") String niNumber,@Param("solicitorCode") String solicitorCode);

    @Query(value = "SELECT * \n" +
            "  FROM VIEW_HDRCLAIM\n" +
            " WHERE UPPER(CLIENT) LIKE UPPER('%'||:NAME||'%')\n" +
            "   AND NVL(UPPER(POSTCODE),'0') LIKE NVL(UPPER('%'||:POSTCODE||'%'),'0')\n" +
            "   AND UPPER(CODE) LIKE UPPER('%'||:PORTALCODE||'%')\n" +
            "   AND NVL(CONTACTNO,'0') LIKE NVL('%'||:MOBILENO||'%','0')\n" +
            "   AND NVL(UPPER(EMAIL),'0') LIKE NVL(UPPER('%'||:EMAIL||'%'),'0')\n" +
            "   AND NVL(UPPER(NINUMBER),'0') LIKE NVL(UPPER('%'||:NINUMBER||'%'),'0')", nativeQuery = true)
    List<Object> getFilterHdrCasesList(@Param("NAME") String name,
                                       @Param("POSTCODE") String postcode,
                                       @Param("PORTALCODE") String portalCode,
                                       @Param("MOBILENO") String mobileNo,
                                       @Param("EMAIL") String email,
                                       @Param("NINUMBER") String niNumber);


    @Query(value = "select c.hdrclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.claimcode as code, hc.c_sname || ' ' || hc.c_fname || ' ' || hc.c_mname as client, ht.createdon as taskdue, t.name as taskname, s.descr status\n" +
            "from tbl_hdrclaim c\n" +
            "left outer  join tbl_hdrtasks ht on ht.hdrcode = c.hdrclaimcode and ht.currenttask = 'Y'\n" +
            "left outer  join tbl_hdrclaimant hc on hc.hdr_claimcode = c.hdrclaimcode\n" +
            "left outer  join tbl_tasks t on t.taskcode = ht.taskcode\n" +
            "left outer  join tbl_rtastatus s on s.statuscode = c.statuscode\n" +
            "where c.statuscode = :statusId" +
            "and c.introducer = :companycode", nativeQuery = true)
    List<Object> getAuthHdrCasesIntroducersStatusWise(@Param("companycode")String companycode, @Param("statusId")long statusId);


    @Query(value = "select c.hdrclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.claimcode as code,\n" +
            "        hc.c_sname || ' ' || hc.c_fname || ' ' || hc.c_mname as client, \n" +
            "        ht.createdon as taskdue, t.name as taskname, s.descr status\n" +
            "            from tbl_hdrclaim c\n" +
            "            left outer  join tbl_hdrtasks ht on ht.hdrcode = c.hdrclaimcode and ht.currenttask = 'Y'\n" +
            "            left outer  join tbl_hdrclaimant hc on hc.hdr_claimcode = c.hdrclaimcode\n" +
            "            left outer  join tbl_tasks t on t.taskcode = ht.taskcode\n" +
            "            left outer  join tbl_rtastatus s on s.statuscode = c.statuscode\n" +
            "            left outer  join TBL_HDRSOLICITOR hs on hs.hdrclaimcode = c.hdrclaimcode and hs.companycode = :companyCode\n" +
            "            where c.statuscode = :statusId\n" , nativeQuery = true)
    List<Object> getHdrCasesListSolicitorStatusWise(@Param("companyCode")String companycode, @Param("statusId")long statusId);

    @Modifying
    @Query(value = " UPDATE  TblHdrclaim SET submitDate = :submitDate WHERE hdrclaimcode = :hdrClaimCode")
    int updateSubmitDate(@Param("submitDate") Date submitDate, @Param("hdrClaimCode") Long hdrClaimCode);


    @Modifying
    @Query(value = " UPDATE  TblHdrclaim SET clawbackDate = :clawbackDate WHERE hdrclaimcode = :hdrClaimCode")
    int updateClawbackdate(@Param("clawbackDate") Date clawbackDate, @Param("hdrClaimCode") Long hdrClaimCode);

    @Query(value = "SELECT H.HDRCLAIMCODE, S.DESCR STATUSDESCR, H.CREATEDATE, R.SOLICITOR_NAME,h.claimcode\n" +
            "  FROM TBL_HDRCLAIM H \n" +
            " INNER JOIN TBL_RTASTATUS S ON H.STATUSCODE = S.STATUSCODE \n" +
            "  LEFT JOIN (SELECT S.HDRCLAIMCODE, C.NAME SOLICITOR_NAME\n" +
            "               FROM TBL_HDRSOLICITOR S\n" +
            "              INNER JOIN TBL_COMPANYPROFILE C ON S.COMPANYCODE = C.COMPANYCODE\n" +
            "              WHERE HDRSOLCODE = (SELECT MAX(HDRSOLCODE)\n" +
            "                                    FROM TBL_HDRSOLICITOR\n" +
            "                                   WHERE HDRCLAIMCODE = S.HDRCLAIMCODE)) R ON H.HDRCLAIMCODE = R.HDRCLAIMCODE \n" +
            " WHERE H.HDRCLAIMCODE <> :hdrclaimcode\n" +
            "   AND H.HDRCLAIMCODE IN (SELECT CHILD_HDRCLAIMCODE\n" +
            "                            FROM TBL_HDRCLAIMCOPY\n" +
            "                           WHERE PARENT_HDRCLAIMCODE IN (:hdrclaimcode)\n" +
            "                           UNION\n" +
            "                          SELECT CHILD_HDRCLAIMCODE\n" +
            "                            FROM TBL_HDRCLAIMCOPY\n" +
            "                           WHERE PARENT_HDRCLAIMCODE IN (SELECT PARENT_HDRCLAIMCODE\n" +
            "                                                           FROM TBL_HDRCLAIMCOPY\n" +
            "                                                          WHERE CHILD_HDRCLAIMCODE = :hdrclaimcode)\n" +
            "                           UNION                                \n" +
            "                          SELECT PARENT_HDRCLAIMCODE\n" +
            "                            FROM TBL_HDRCLAIMCOPY\n" +
            "                           WHERE CHILD_HDRCLAIMCODE = :hdrclaimcode)" , nativeQuery = true)
    List<Object> getHdrCopyLogs(@Param("hdrclaimcode") long hdrclaimcode);
}
