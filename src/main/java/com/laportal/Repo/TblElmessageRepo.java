package com.laportal.Repo;

import com.laportal.model.TblElmessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblElmessageRepo extends JpaRepository<TblElmessage, Long> {
    List<TblElmessage> findByTblElclaimElclaimcodeOrderByCreatedon(Long elclaimcode);
}
