package com.laportal.Repo;

import com.laportal.model.TblRtasolicitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface TblRtasolicitorRepo extends JpaRepository<TblRtasolicitor, Long> {

    @Modifying
    @Query(value = " UPDATE TblRtasolicitor SET status = :statusCode WHERE rtacode = :rtaCode")
    int updateSolicitor(@Param("statusCode") String statusCode, @Param("rtaCode") BigDecimal rtaCode);

    TblRtasolicitor findByRtacodeAndStatus(BigDecimal rtaCode, String status);

    @Modifying
    @Query(value = " UPDATE TblRtasolicitor SET remarks = :remarks,status = :statusCode WHERE rtacode = :rtaCode")
    int updateRemarksReason(@Param("remarks") String remarks, @Param("statusCode") String statusCode, @Param("rtaCode") BigDecimal rtaCode);

}
