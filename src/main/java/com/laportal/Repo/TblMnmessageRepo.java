package com.laportal.Repo;

import com.laportal.model.TblMnmessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblMnmessageRepo extends JpaRepository<TblMnmessage, Long> {

    List<TblMnmessage> findByTblMnclaimMnclaimcode(Long valueOf);

}
