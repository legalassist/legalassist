package com.laportal.Repo;

import com.laportal.model.ViewInvoice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ViewInvoiceRepo extends JpaRepository<ViewInvoice, String> {
}
