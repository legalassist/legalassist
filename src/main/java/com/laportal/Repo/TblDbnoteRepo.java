package com.laportal.Repo;

import com.laportal.model.TblDbnote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblDbnoteRepo extends JpaRepository<TblDbnote, Long> {

    @Query(value = "select * from (select *   \n" +
            "                            from tbl_Dbnotes  \n" +
            "                           where usercode = :usercode    \n" +
            "                             and DbClaimCode = :DbClaimCode   \n" +
            "                              \n" +
            "                        union   \n" +
            "                              \n" +
            "                         select *   \n" +
            "                           from tbl_Dbnotes   \n" +
            "                          where usercategorycode = :categorycode    \n" +
            "                            and DbClaimCode = :DbClaimCode   ) ", nativeQuery = true)
    List<TblDbnote> findNotesOnDbbyUserAndCategory(@Param("DbClaimCode") Long DbClaimCode, @Param("categorycode") String categorycode, @Param("usercode") String usercode);
}
