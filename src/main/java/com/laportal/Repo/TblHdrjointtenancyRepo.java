package com.laportal.Repo;

import com.laportal.model.TblHdrjointtenancy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblHdrjointtenancyRepo extends JpaRepository<TblHdrjointtenancy, Long> {
}
