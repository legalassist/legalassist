package com.laportal.Repo;

import com.laportal.model.TblOlsolicitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TblOlsolicitorRepo extends JpaRepository<TblOlsolicitor, Long> {
    TblOlsolicitor findByTblOlclaimOlclaimcodeAndStatus(long OlCaseId, String status);

    @Modifying
    @Query(value = " UPDATE TblOlsolicitor SET status = :statusCode WHERE tblOlclaim.olclaimcode = :olclaimcode")
    int updateSolicitor(@Param("statusCode") String statusCode, @Param("olclaimcode") long olclaimcode);

    @Modifying
    @Query(value = " UPDATE TblOlsolicitor SET remarks = :remarks,status = :statusCode WHERE tblOlclaim.olclaimcode = :olclaimcode")
    int updateRemarksReason(@Param("remarks") String remarks, @Param("statusCode") String statusCode, @Param("olclaimcode") long olclaimcode);
}
