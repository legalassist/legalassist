package com.laportal.Repo;

import com.laportal.model.TblRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblRolesRepo extends JpaRepository<TblRole, String> {

    @Query(value = "SELECT SEQ_TBL_ROLES.nextval from dual", nativeQuery = true)
    int getSeq();

    List<TblRole> findByRolestatus(String status);


    @Query(value = "SELECT * FROM TBL_ROLES" +
            " WHERE CATEGORYCODE = NVL(:categoryCode, CATEGORYCODE) " +
            "AND COMPAIGNCODE = NVL(:compaignCode, COMPAIGNCODE) ", nativeQuery = true)
    List<TblRole> getRoles(@Param("categoryCode") String categoryCode, @Param("compaignCode") String compaignCode);

    @Query(value = " select r.*\n" +
            "from tbl_roles r\n" +
            "inner join tbl_userroles ur on ur.rolecode = r.rolecode\n" +
            "where ur.usercode = :usercode ", nativeQuery = true)
    List<TblRole> getAllRoleFromUserRoles(String usercode);
}
