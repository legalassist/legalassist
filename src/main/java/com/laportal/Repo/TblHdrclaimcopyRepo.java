package com.laportal.Repo;

import com.laportal.model.TblHdrclaimcopy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblHdrclaimcopyRepo extends JpaRepository<TblHdrclaimcopy,Long> {

    TblHdrclaimcopy findByTblHdrclaim1Hdrclaimcode(Long hdrClaimCode);
}
