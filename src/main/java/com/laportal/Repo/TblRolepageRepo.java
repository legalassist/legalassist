package com.laportal.Repo;

import com.laportal.model.TblRolepage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblRolepageRepo extends JpaRepository<TblRolepage, Long> {

    List<TblRolepage> findByTblRoleRolecodeIn(List<String> roleCodes);

    List<TblRolepage> findByTblRoleRolecode(String roleCodes);

}
