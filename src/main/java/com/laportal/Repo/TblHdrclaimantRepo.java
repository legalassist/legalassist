package com.laportal.Repo;

import com.laportal.model.TblHdrclaimant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblHdrclaimantRepo extends JpaRepository<TblHdrclaimant, Long> {

    TblHdrclaimant findByTblHdrclaimHdrclaimcode(Long hdrClaimCode);

    @Query(value = " SELECT COUNT(1),H.CLAIMCODE,c.c_fname || ' ' || c.c_mname || ' ' || c.c_sname, H.HDRCLAIMCODE\n" +
            "  FROM TBL_HDRCLAIMANT C\n" +
            " INNER JOIN TBL_HDRCLAIM H ON C.HDR_CLAIMCODE = H.HDRCLAIMCODE \n" +
            " WHERE NVL(C.C_FNAME,'A') = NVL(:fname,NVL(C.C_FNAME,'A'))\n" +
            "   AND NVL(C.C_MNAME,'A') = NVL(:mname,NVL(C.C_MNAME,'A'))\n" +
            "   AND NVL(C.C_SNAME,'A') = NVL(:sname,NVL(C.C_SNAME,'A'))\n" +
            " GROUP BY  H.CLAIMCODE,c.c_fname || ' ' || c.c_mname || ' ' || c.c_sname, H.HDRCLAIMCODE", nativeQuery = true)
    List<Object> duplicatesForName(@Param("fname")String fname,@Param("mname")String mname,@Param("sname")String sname);

    @Query(value = " SELECT COUNT(1),H.CLAIMCODE,c.c_fname || ' ' || c.c_mname || ' ' || c.c_sname, H.HDRCLAIMCODE\n" +
            "  FROM TBL_HDRCLAIMANT C\n" +
            " INNER JOIN TBL_HDRCLAIM H ON C.HDR_CLAIMCODE = H.HDRCLAIMCODE \n" +
            " WHERE NVL(C.C_POSTCODE,'A') = NVL(:postCode,NVL(C.C_POSTCODE,'A'))\n" +
            "   AND NVL(C.C_ADDRESS1,'A') = NVL(:address1,NVL(C.C_ADDRESS1,'A'))\n" +
            "   AND NVL(C.C_ADDRESS2,'A') = NVL(:address2,NVL(C.C_ADDRESS2,'A'))\n" +
            "   AND NVL(C.C_ADDRESS3,'A') = NVL(:address3,NVL(C.C_ADDRESS3,'A')) \n" +
            "  -- AND NVL(C.C_CITY,'A') = NVL(:city,NVL(C.C_CITY,'A'))\n" +
            "  -- AND NVL(C.C_REGION,'A') = NVL(:region,NVL(C.C_REGION,'A'))\n" +
            " GROUP BY  H.CLAIMCODE,c.c_fname || ' ' || c.c_mname || ' ' || c.c_sname, H.HDRCLAIMCODE", nativeQuery = true)
    List<Object> duplicatesForAddress(@Param("postCode")String postCode,@Param("address1")String address1,
                                      @Param("address2")String address2,@Param("address3")String address3);

    @Query(value = "SELECT COUNT(*),c.claimcode,r.c_fname || ' ' || r.c_mname || ' ' || r.c_sname,r.hdr_claimcode\n" +
            "                        FROM TBL_HDRCLAIMANT r,TBL_HDRCLAIM c\n" +
            "                        WHERE c.hdrclaimcode = r.hdr_claimcode\n" +
            "                        AND r.c_ninumber = :ninumber\n" +
            "                        group by c.claimcode,r.c_fname || ' ' || r.c_mname || ' ' || r.c_sname,r.hdr_claimcode", nativeQuery = true)
    List<Object> duplicatesForNiNumber(@Param("ninumber")String cninumber);
}
