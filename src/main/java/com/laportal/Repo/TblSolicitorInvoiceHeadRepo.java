package com.laportal.Repo;

import com.laportal.model.TblSolicitorInvoiceHead;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TblSolicitorInvoiceHeadRepo extends JpaRepository<TblSolicitorInvoiceHead, Long> {

    List<TblSolicitorInvoiceHead> findByStatus(BigDecimal status);
    List<TblSolicitorInvoiceHead> findAllByOrderByCreatedonDesc();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE   \n" +
            "  FROM TBL_SOLICITOR_INVOICE_HEAD C   \n" +
            " RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE  \n" +
            " WHERE S.COMPAIGNCODE IS NULL   \n" +
            " GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllInvoicesStatusCount();

    @Modifying
    @Query(value = " UPDATE  TblSolicitorInvoiceHead SET status = :statusCode,statuscode = :statusdescr  WHERE invoiceheadid = :invoiceheadid")
    int performAction(@Param("statusCode") BigDecimal statusCode, @Param("invoiceheadid") Long invoiceheadid, @Param("statusdescr") String statusdescr);
    @Modifying
    @Query(value = " UPDATE  TblSolicitorInvoiceHead SET status = :statusCode,statuscode = :statusdescr,tblEmail.emailcode = :emailcode  WHERE invoiceheadid = :invoiceheadid")
    int performAction(@Param("statusCode") BigDecimal statusCode, @Param("invoiceheadid") Long invoiceheadid, @Param("statusdescr") String statusdescr, @Param("emailcode") Long emailcode);


    @Query(value = "select distinct invh.*  \n" +
            "            from TBL_SOLICITOR_INVOICE_HEAD invh,TBL_SOLICITOR_INVOICE_DETAIL invd \n" +
            "            where invh.companycode = nvl(:companyCode,invh.companycode) \n" +
            "            and  invh.status =  NVL(:status,invh.status) \n" +
            "         --   and invh.invoicecode =  NVL(:invoiceCode,invh.invoicecode) \n" +
            "            and invh.invoiceheadid = invd.invoiceheadid " +
            "     --       and invh.compaigntype =  NVL(:caseNumber,invh.compaigntype)\n" +
            "            and invh.createdate between  NVL(:datefrom,invh.createdate) and  NVL(:dateto,invh.createdate)", nativeQuery = true)
    List<TblSolicitorInvoiceHead> invoiceSearch(@Param("status") BigDecimal status,
                                                @Param("companyCode") String companycode,
                                                @Param("datefrom") String datefrom, @Param("dateto") String dateto);


    @Query(value = "SELECT * FROM VIEW_INVOICE_DETAIL WHERE INVOICEHEADID = :invoiceHeadId", nativeQuery = true)
    List<Object> getByInvoiceheadid(@Param("invoiceHeadId") BigDecimal invoiceDetailId);
}
