package com.laportal.Repo;

import com.laportal.model.TblMndocument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblMndocumentRepo extends JpaRepository<TblMndocument, Long> {

    List<TblMndocument> findByTblMnclaimMnclaimcode(Long mnClaimCode);
}
