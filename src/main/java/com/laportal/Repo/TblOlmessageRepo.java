package com.laportal.Repo;

import com.laportal.model.TblOlmessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblOlmessageRepo extends JpaRepository<TblOlmessage, Long> {
    List<TblOlmessage> findByTblOlclaimOlclaimcode(Long olclaimcode);
}
