package com.laportal.Repo;

import com.laportal.model.TblHirebusinessdetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface TblHirebusinessdetailRepo extends JpaRepository<TblHirebusinessdetail, Long> {

    @Modifying
    @Query(value = " UPDATE TblHirebusinessdetail SET bookingdate = :bookingdate, bookingmode = :bookingmode," +
            "hirestartdate = :hirestartdate , hireenddate = :hireenddate WHERE hirebusinessdetailcode = :hirebusinessdetailcode")
    int updateHireBuisnessDetails(@Param("bookingdate") Date bookingdate,
                                  @Param("bookingmode") String bookingmode,
                                  @Param("hirestartdate") Date hirestartdate,
                                  @Param("hireenddate") Date hireenddate,
                                  @Param("hirebusinessdetailcode") long Dbclaimcode);


}
