package com.laportal.Repo;

import com.laportal.model.TblRtastatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TblRtastatusRepo extends JpaRepository<TblRtastatus, Long> {

    List<TblRtastatus> findByStatus(String status);

    @Query(value = "select * from tbl_rtastatus where compaigncode is null and status = 'Y'", nativeQuery = true)
    List<TblRtastatus> findStatusforInvoices();

    List<TblRtastatus> findByCompaigncodeIsNull();

}
