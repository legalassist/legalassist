package com.laportal.Repo;

import com.laportal.model.TblElsolicitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TblElsolicitorRepo extends JpaRepository<TblElsolicitor, Long> {
    TblElsolicitor findByTblElclaimElclaimcodeAndStatus(long elCaseId, String status);

    @Modifying
    @Query(value = " UPDATE TblElsolicitor SET status = :statusCode WHERE tblElclaim.elclaimcode = :elclaimcode")
    int updateSolicitor(@Param("statusCode") String statusCode, @Param("elclaimcode") long elclaimcode);

    @Modifying
    @Query(value = " UPDATE TblElsolicitor SET remarks = :remarks,status = :statusCode WHERE tblElclaim.elclaimcode = :elclaimcode")
    int updateRemarksReason(@Param("remarks") String remarks, @Param("statusCode") String statusCode, @Param("elclaimcode") long elclaimcode);
}
