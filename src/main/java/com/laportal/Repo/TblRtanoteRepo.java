package com.laportal.Repo;

import com.laportal.model.TblRtanote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblRtanoteRepo extends JpaRepository<TblRtanote, Long> {

    List<TblRtanote> findAllByTblRtaclaimRtacodeAndUsercategorycodeOrUsercode(Long rtacode, String usercategorycode, String usercode);

    @Query(value = "select *\n" +
            "from (\n" +
            "select *\n" +
            "  from tbl_rtanotes\n" +
            " where usercode = :usercode \n" +
            "   and rtacode = :rtacode\n" +
            "   \n" +
            "union\n" +
            "   \n" +
            "select *\n" +
            "  from tbl_rtanotes\n" +
            " where usercategorycode = :usercategorycode \n" +
            "   and rtacode = :rtacode) order by createdon asc ", nativeQuery = true)
    List<TblRtanote> findNotesOnRtabyUserAndCategory(@Param("rtacode") Long rtacode, @Param("usercategorycode") String usercategorycode, @Param("usercode") String usercode);


    @Query(value = "  select n.* from TBL_COMPANYPROFILE c, tbl_users u,tbl_rtanotes n\n" +
            "             where c.companycode = u.companycode\n" +
            "             and c.categorycode = '4'\n" +
            "              and n.usercategorycode = '4'\n" +
            "             and u.usercode = n.usercode\n" +
            "             and u.usercode = :usercode" +
            "             and n.rtacode = :rtacode" +
            "             order by n.createdon asc ", nativeQuery = true)
    List<TblRtanote> getAuthRtaCaseNotesOfLegalInternal(@Param("rtacode")Long rtacode, @Param("usercode") String usercode);
}
