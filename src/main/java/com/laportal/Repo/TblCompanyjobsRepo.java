package com.laportal.Repo;

import com.laportal.model.TblCompanyjob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TblCompanyjobsRepo extends JpaRepository<TblCompanyjob, String> {


    List<TblCompanyjob> findByCompanycode(String companyCode);


    @Query(value = "SELECT SEQ_TBL_COMPANYJOBS.nextval from dual", nativeQuery = true)
    int getSeq();

   /* @Modifying
    @Query("update TblCompanyjob j set " +
            "j.adultfee = :adultfee," +
            "j.childfee = :childfee," +
            "j.scotadultfee = :scotadultfee," +
            "j.scotchildfee = :scotchildfee," +
            "j.status = :status,j.adultpostreforms = :adultfeepostreform, j.childpostreforms = :childfeepostreform where j.companyjobcode = :companyjobcode")
    int updateCompanyJobs(@Param("adultfee") BigDecimal adultfee,
                          @Param("childfee") BigDecimal childfee,
                          @Param("scotadultfee") BigDecimal scotadultfee,
                          @Param("scotchildfee") BigDecimal scotchildfee,
                          @Param("status") String status,
                          @Param("adultfeepostreform") BigDecimal adultfeepostreform,@Param("childfeepostreform") BigDecimal childfeepostreform, @Param("companyjobcode") String companyjobcode);
*/

    List<TblCompanyjob> findByCompanycodeAndTblCompaignCompaigncode(String companyCode, String compaingnCode);
}
