package com.laportal.Repo;

import com.laportal.model.ViewInvoiceLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ViewInvoiceLineRepo extends JpaRepository<ViewInvoiceLine, String> {
}
