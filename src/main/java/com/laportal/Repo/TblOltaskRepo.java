package com.laportal.Repo;

import com.laportal.model.TblOltask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TblOltaskRepo extends JpaRepository<TblOltask, Long> {
    List<TblOltask> findByTblOlclaimOlclaimcode(long olclaimcode);

    TblOltask findByTblOlclaimOlclaimcodeAndTblTaskTaskcode(long olclaimcode, BigDecimal taskcode);

    List<TblOltask> findByTblOlclaimOlclaimcodeAndStatus(long olclaimCode, String status);

    @Modifying
    @Query(value = "update TBL_OLTASKS  set status = :status, remarks = :remarks where taskcode = :olTaskCode and OLCLAIMCODE = :OLCLAIMCODE", nativeQuery = true)
    int updatetask(@Param("status") String status, @Param("remarks") String remarks, @Param("olTaskCode") long eltaskcode, @Param("OLCLAIMCODE") long Elcode);

    @Modifying
    @Query(value = "update TBL_OLTASKS  set CURRENTTASK = :status where OLCLAIMCODE = :olCode", nativeQuery = true)
    int setAllTask(@Param("status") String status, @Param("olCode") long olCode);

    @Modifying
    @Query(value = "update TBL_OLTASKS  set CURRENTTASK = :status where OLTASKCODE = :oltaskcode and OLCLAIMCODE = :elcode", nativeQuery = true)
    int setCurrentTask(@Param("status") String status, @Param("oltaskcode") long eltaskcode, @Param("elcode") long elcode);
}
