package com.laportal.Repo;

import com.laportal.model.TblHdrlog;
import com.spire.ms.System.Collections.Generic.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblHdrlogRepo extends JpaRepository<TblHdrlog, Long> {

    List<TblHdrlog> findByTblHdrclaimHdrclaimcodeOrderByHdrlogcodeDesc(long hdrCode);

}
