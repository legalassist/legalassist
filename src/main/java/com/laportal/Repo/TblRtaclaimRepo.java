package com.laportal.Repo;

import com.laportal.model.TblRtaclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface TblRtaclaimRepo extends JpaRepository<TblRtaclaim, Long> {

    @Query(value = "SELECT SEQ_RTADOCNUMBER.nextval from dual", nativeQuery = true)
    int getRtaDocNumber();

    @Query(value = "SELECT SEQ_RTACLAIM.nextval from dual", nativeQuery = true)
    int getSeq();

    TblRtaclaim findByRtanumber(String rtanumber);

    @Modifying
    @Query(value = " UPDATE  TblRtaclaim SET statuscode = :statusCode, rtalogcode = null,lastupdateuser = :lastupdateuser  WHERE rtacode = :rtaCode")
    int performAction(@Param("statusCode") String statusCode, @Param("rtaCode") Long rtaCode, @Param("lastupdateuser") String lastupdateuser);

    @Modifying
    @Query(value = " UPDATE  TblRtaclaim SET remarks = :reason,statuscode = :statusCode, rtalogcode = null,lastupdateuser = :lastupdateuser WHERE rtacode = :rtaCode")
    int performRejectCancelAction(@Param("reason") String reason, @Param("statusCode") String statusCode, @Param("rtaCode") Long rtaCode, @Param("lastupdateuser") String lastupdateuser);

    @Query(value = "select * from tbl_rtaclaim where rtalinkcode = :rtanumber", nativeQuery = true)
    List<TblRtaclaim> getSubRtaClaims(@Param("rtanumber") String rtanumber);

    @Query(value = "SELECT NVL(COUNT(C.STATUSCODE),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_RTACLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUSCODE = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 1 and  c.introducer = :companyCode and s.show_status is null \n" +
            "GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc ", nativeQuery = true)
    List<Object> getAllRtaStatusCountsForIntroducers(@Param("companyCode") String companyCode);

    @Query(value = "SELECT NVL(COUNT(C.STATUSCODE),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_RTACLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUSCODE = S.STATUSCODE \n" +
            "INNER JOIN TBL_USERS U ON U.USERCODE = C.usercode\n" +
            "inner join tbl_rtasolicitor hs on c.rtacode = hs.rtacode and hs.status = 'Y'\n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode and hs.companycode = :companyCode\n" +
            "WHERE S.COMPAIGNCODE = 1" +
            " and s.show_status is null \n" +
            "GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc", nativeQuery = true)
    List<Object> getAllRtaStatusCountsForSolicitors(@Param("companyCode") String companyCode);

    @Query(value = "SELECT NVL(COUNT(C.STATUSCODE),0) STATUSCOUNT, S.DESCR, S.STATUSCODE\n" +
            "                           FROM TBL_RTACLAIM C  \n" +
            "                           RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUSCODE = S.STATUSCODE WHERE S.COMPAIGNCODE = 1   and s.show_status is null \n" +
            "                          GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc", nativeQuery = true)
    List<Object> getAllPlStatusCounts();

    List<TblRtaclaim> findByStatuscode(String statusCode);


    @Query(value = "select c.* \n" +
            "from TBL_RTACLAIM c, TBL_RTASOLICITOR s\n" +
            "where c.statuscode in (7,159)\n" +
            " and c.RTACODE = s.RTACODE" +
            " and s.usercode = :userCode", nativeQuery = true)
    List<TblRtaclaim> getGetCaseForInvoicing(@Param("userCode") String solicitorId);

    @Query(value = "SELECT COUNT(*),r.rtanumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.rtacode\n" +
            "            FROM TBL_RTACLAIM r\n" +
            "            WHERE REGISTERATIONNO = :vehicleReg\n" +
            "            group by r.rtanumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.rtacode", nativeQuery = true)
    List<Object> duplicatesForVehicle(@Param("vehicleReg") String vehicleReg);

    @Query(value = "SELECT COUNT(*),r.rtanumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.rtacode\n" +
            "            FROM TBL_RTACLAIM r\n" +
            "            WHERE MOBILE =:mobile\n" +
            "             group by r.rtanumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.rtacode", nativeQuery = true)
    List<Object> duplicatesForMobile(@Param("mobile") String mobile);


    @Query(value = "SELECT COUNT(*),r.rtanumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.rtacode\n" +
            "            FROM TBL_RTACLAIM r\n" +
            "            WHERE PARTYCONTACTNO =:mobile\n" +
            "             group by r.rtanumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.rtacode", nativeQuery = true)
    List<Object> duplicatesForThirPartyMobile(@Param("mobile") String mobile);

    @Query(value = "SELECT COUNT(*),r.rtanumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.rtacode\n" +
            "            FROM TBL_RTACLAIM r\n" +
            "            WHERE r.ninumber =:ninumber\n" +
            "             group by r.rtanumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.rtacode", nativeQuery = true)
    List<Object> duplicatesForNiNumber(@Param("ninumber") String ninumber);

    @Query(value = "SELECT R.ATTRIBUTE_NAME, OLD_VALUE, NEW_VALUE, AUDIT_DATE, U.LOGINID\n" +
            "FROM LAUSER_AUDIT.TBL_RTACLAIM R\n" +
            "INNER JOIN TBL_USERS U ON R.USER_ID = U.USERCODE\n" +
            "WHERE R.REFPK = :rtacode \n" +
            "ORDER BY R.AUDIT_ID DESC", nativeQuery = true)
    List<Object> getRtaAuditLogs(@Param("rtacode") Long rtacode);

    @Modifying
    @Query(value = " UPDATE  TblRtaclaim SET clawbackDate = :clawbackDate WHERE rtacode = :rtaCode")
    int updateClawbackdate(@Param("clawbackDate") Date clawbackDate, @Param("rtaCode") Long rtaCode);

    @Modifying
    @Query(value = " UPDATE  TblRtaclaim SET statuscode = :statusCode, rtalogcode = :rtalogcode WHERE rtacode = :rtacode")
	int performRevertAction(@Param("statusCode") String statusCode, @Param("rtacode") Long rtacode, @Param("rtalogcode") BigDecimal rtalogcode);


    @Query(value = "SELECT COUNT(*),r.rtanumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.rtacode\n" +
            "            FROM TBL_RTACLAIM r\n" +
            "            WHERE partyregno =:partyregno\n" +
            "             group by r.rtanumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.rtacode", nativeQuery = true)
    List<Object> duplicatesForPartyregno(@Param("partyregno") String mobile);

    @Query(value = "SELECT R.ATTRIBUTE_NAME, OLD_VALUE, NEW_VALUE, AUDIT_DATE, U.LOGINID\n" +
            "FROM LAUSER_AUDIT.TBL_RTACLAIM R\n" +
            "INNER JOIN TBL_USERS U ON R.USER_ID = U.USERCODE\n" +
            "WHERE R.REFPK = :rtacode \n" +
            "AND TRUNC(AUDIT_DATE) = TRUNC(SYSDATE)\n" +
            "ORDER BY R.AUDIT_ID desc", nativeQuery = true)
    List<Object> getTodayRtaAuditLogs(long rtacode);

    @Query(value = "select * from tbl_rtaclaim where rtalinkcode = :rtanumber", nativeQuery = true)
    List<TblRtaclaim> getSubRtaClaimsBySolicitor(@Param("rtanumber") String rtanumber);

    @Modifying
    @Query(value = " UPDATE  TblRtaclaim SET submitDate = :submitDate WHERE rtacode = :rtaCode")
    int updateSubmitDate(@Param("submitDate") Date submitDate, @Param("rtaCode") Long rtaCode);
}
