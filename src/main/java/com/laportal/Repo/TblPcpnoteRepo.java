package com.laportal.Repo;

import com.laportal.model.TblPcpnote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblPcpnoteRepo extends JpaRepository<TblPcpnote, Long> {

    @Query(value = "select * from (select *   \n" +
            "                            from tbl_pcpnotes  \n" +
            "                           where usercode = :usercode    \n" +
            "                             and pcpClaimCode = :pcpClaimCode   \n" +
            "                              \n" +
            "                        union   \n" +
            "                              \n" +
            "                         select *   \n" +
            "                           from tbl_pcpnotes   \n" +
            "                          where usercategorycode = :categorycode    \n" +
            "                            and pcpClaimCode = :pcpClaimCode   ) ", nativeQuery = true)
    List<TblPcpnote> findNotesOnPcpbyUserAndCategory(@Param("pcpClaimCode") Long pcpClaimCode, @Param("categorycode") String categorycode, @Param("usercode") String usercode);
}
