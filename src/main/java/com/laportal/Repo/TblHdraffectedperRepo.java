package com.laportal.Repo;

import com.laportal.model.TblHdraffectedper;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblHdraffectedperRepo extends JpaRepository<TblHdraffectedper, Long> {
}
