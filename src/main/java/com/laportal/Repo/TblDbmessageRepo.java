package com.laportal.Repo;

import com.laportal.model.TblDbmessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblDbmessageRepo extends JpaRepository<TblDbmessage, Long> {
    List<TblDbmessage> findByTblDbclaimDbclaimcode(Long Dbcode);
}
