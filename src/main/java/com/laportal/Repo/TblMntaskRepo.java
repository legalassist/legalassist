package com.laportal.Repo;

import com.laportal.model.TblMntask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TblMntaskRepo extends JpaRepository<TblMntask, Long> {

    TblMntask findByTblMnclaimMnclaimcodeAndTaskcode(long mnClaimcode, BigDecimal taskCode);

    List<TblMntask> findByTblMnclaimMnclaimcode(long mnClaimcode);
    List<TblMntask> findByTblMnclaimMnclaimcodeAndStatus(long mnClaimcode,String status);

    @Modifying
    @Query(value = "update TBL_MNTASKS  set status = :status, remarks = :remarks where taskcode = :taskcode and MNCLAIMCODE = :mncode", nativeQuery = true)
    int updatetask(@Param("status") String status, @Param("remarks") String remarks, @Param("taskcode") long eltaskcode, @Param("mncode") long mncode);

    @Modifying
    @Query(value = "update TBL_MNTASKS  set CURRENTTASK = :status where mntaskcode = :mntaskcode and MNCLAIMCODE = :mncode", nativeQuery = true)
    int setCurrentTask(@Param("status") String status, @Param("mntaskcode") long mntaskcode, @Param("mncode") long mncode);

    @Modifying
    @Query(value = "update TBL_MNTASKS  set CURRENTTASK = :status where MNCLAIMCODE = :mncode", nativeQuery = true)
    int setAllTask(@Param("status") String status, @Param("mncode") long Elcode);

}
