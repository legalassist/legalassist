package com.laportal.Repo;

import com.laportal.model.TblImmigrationclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TblImmigrationclaimRepo extends JpaRepository<TblImmigrationclaim, Long> {

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE\n" +
            "               FROM TBL_immigrationCLAIM C  \n" +
            "               RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE WHERE S.COMPAIGNCODE = 3  \n" +
            "               GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllDbStatusCounts();

    @Query(value = "select c.Dbclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, \n" +
            "       c.Dbcode as code, CLAIMANT_NAME as client, s.descr status \n" +
            "    from tbl_Dbclaim c \n" +
            "    left outer  join tbl_rtastatus s on s.statuscode = c.status \n", nativeQuery = true)
    List<Object> getDbCasesListUserWise();

    @Query(value = "select c.immigrationclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.immigrationcode as code, clientname as client, s.descr status \n" +
            "            from TBL_immigrationCLAIM c \n" +
            "            left outer  join tbl_rtastatus s on s.statuscode = c.status \n" +
            "            where c.status = :statusId", nativeQuery = true)
    List<Object> getDbCasesListStatusWise(@Param("statusId") long statusId);

    @Modifying
    @Query(value = " UPDATE  TblImmigrationclaim SET status = :statusCode WHERE Immigrationclaimcode = :Dbclaimcode")
    int performAction(@Param("statusCode") BigDecimal statusCode, @Param("Dbclaimcode") Long Dbclaimcode);

    @Modifying
    @Query(value = " UPDATE  TblImmigrationclaim SET remarks = :reason, status = :statusCode WHERE Immigrationclaimcode = :Dbclaimcode")
    int performRejectCancelAction(@Param("reason") String reason, @Param("statusCode") String toStatus, @Param("Dbclaimcode") Long Dbclaimcode);

    @Query(value = "SELECT immigrationclaimcode, created, code, client, taskdue, taskname, status, address, contactno, lastupdated, introducer, lastnotedate, CREATEDBY\n" +
            "FROM VIEW_immigrationCLAIMINTRODUCERS\n" +
            "WHERE CREATEDBY = :userId", nativeQuery = true)
    List<Object> getDbCasesListForIntroducers(@Param("userId") String usercode);

    @Query(value = "SELECT immigrationclaimcode, created, code, client, taskdue, taskname, status, address, contactno, lastupdated, introducer, lastnotedate, SOLICITORCODE\n" +
            "FROM VIEW_immigrationCLAIMSOLICITORS\n" +
            "WHERE SOLICITORCODE = :userId", nativeQuery = true)
    List<Object> getDbCasesListForSolicitor(@Param("userId") String usercode);

    @Query(value = "SELECT immigrationclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate\n" +
            "FROM VIEW_immigrationCLAIM", nativeQuery = true)
    List<Object> getDbCasesList();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_immigrationCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "INNER JOIN TBL_USERS U ON U.USERCODE = C.CREATEUSER\n" +
            "inner join tbl_immigrationsolicitor hs on c.immigrationclaimcode = hs.immigrationclaimcode and hs.status = 'Y'\n" +
            "inner join tbl_users uu on uu.usercode = hs.usercode\n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode\n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllDbStatusCountsForSolicitors();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_immigrationCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllDbStatusCountsForIntroducers();

}
