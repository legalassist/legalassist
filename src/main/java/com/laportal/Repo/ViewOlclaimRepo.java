package com.laportal.Repo;

import com.laportal.model.ViewMnclaim;
import com.laportal.model.ViewOlclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ViewOlclaimRepo extends JpaRepository<ViewOlclaim, BigDecimal> {

    @Query(value = "SELECT *\n" +
            "  FROM VIEW_OLCLAIM\n" +
            " WHERE :LOGINUSER = CASE WHEN :USERCATEGORY = 1 THEN ADVISOR\n" +
            "                         WHEN :USERCATEGORY = 2 THEN SOLICITOR_USERCODE\n" +
            "                         WHEN :USERCATEGORY = 4 THEN :LOGINUSER\n" +
            "                    END", nativeQuery = true)
    List<ViewOlclaim> getAuthOlCasesUserAndCategoryWise(@Param("LOGINUSER")Long LOGINUSER, @Param("USERCATEGORY") Long USERCATEGORY);

    @Query(value = "SELECT *\n" +
            "  FROM VIEW_OLCLAIM\n" +
            " WHERE :LOGINUSER = CASE WHEN :USERCATEGORY = 1 THEN ADVISOR\n" +
            "                         WHEN :USERCATEGORY = 2 THEN SOLICITOR_USERCODE\n" +
            "                         WHEN :USERCATEGORY = 4 THEN :LOGINUSER\n" +
            "                    END \n" +
            "And statuscode = :statusCode \n" +
            "order by createdate desc",nativeQuery = true)
    List<ViewOlclaim> getAuthOlCasesUserAndCategoryWiseAndStatusWise(@Param("LOGINUSER") BigDecimal userCode, @Param("USERCATEGORY") BigDecimal userCatCode, @Param("statusCode") BigDecimal statusCode);


}
