package com.laportal.Repo;

import com.laportal.model.TblUserrole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblUserrolesRepo extends JpaRepository<TblUserrole, String> {


    @Query(value = "SELECT SEQ_TBL_USERROLES.nextval from dual", nativeQuery = true)
    int getSeq();


    List<TblUserrole> findByTblUserUsercode(String userCode);

    @Modifying
    @Query("delete from TblUserrole u where u.tblUser.usercode = :usercode ")
    int deletAllUserRole(@Param("usercode") String usercode);
}
