package com.laportal.Repo;

import com.laportal.model.ViewElclaim;
import com.laportal.model.ViewMnclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ViewMnclaimRepo extends JpaRepository<ViewMnclaim, BigDecimal> {

    @Query(value = "SELECT *\n" +
            "  FROM VIEW_MNCLAIM\n" +
            " WHERE :LOGINUSER = CASE WHEN :USERCATEGORY = 1 THEN ADVISOR\n" +
            "                         WHEN :USERCATEGORY = 2 THEN SOLICITOR_USERCODE\n" +
            "                         WHEN :USERCATEGORY = 4 THEN :LOGINUSER\n" +
            "                    END\n" +
            "order by createdate desc",nativeQuery = true)
    List<ViewMnclaim> getAuthMnCasesUserAndCategoryWise(@Param("LOGINUSER") BigDecimal userCode, @Param("USERCATEGORY") BigDecimal userCatCode);

    @Query(value = "SELECT *\n" +
            "  FROM VIEW_MNCLAIM\n" +
            " WHERE :LOGINUSER = CASE WHEN :USERCATEGORY = 1 THEN ADVISOR\n" +
            "                         WHEN :USERCATEGORY = 2 THEN SOLICITOR_USERCODE\n" +
            "                         WHEN :USERCATEGORY = 4 THEN :LOGINUSER\n" +
            "                    END \n" +
            "And statuscode = :statusCode \n" +
            "order by createdate desc",nativeQuery = true)
    List<ViewMnclaim> getAuthMnCasesUserAndCategoryWiseAndStatusWise(@Param("LOGINUSER") BigDecimal userCode,@Param("USERCATEGORY") BigDecimal userCatCode,@Param("statusCode") BigDecimal statusCode);

}
