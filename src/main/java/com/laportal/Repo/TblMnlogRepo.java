package com.laportal.Repo;

import com.laportal.model.TblMnlog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblMnlogRepo extends JpaRepository<TblMnlog, Long> {

    List<TblMnlog> findByTblMnclaimMnclaimcode(Long valueOf);

}
