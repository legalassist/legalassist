package com.laportal.Repo;

import com.laportal.model.TblHdrtenancy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblHdrtenancyRepo extends JpaRepository<TblHdrtenancy, Long> {
}
