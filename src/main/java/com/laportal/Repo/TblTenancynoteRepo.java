package com.laportal.Repo;

import com.laportal.model.TblTenancynote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblTenancynoteRepo extends JpaRepository<TblTenancynote, Long> {


    @Query(value = "select * from (select *   \n" +
            "                            from tbl_tenancynotes  \n" +
            "                           where usercode = :usercode    \n" +
            "                             and tenancyclaimcode = :tenancyClaimCode   \n" +
            "                              \n" +
            "                        union   \n" +
            "                              \n" +
            "                         select *   \n" +
            "                           from tbl_tenancynotes   \n" +
            "                          where usercategorycode = :categorycode    \n" +
            "                            and tenancyclaimcode = :tenancyClaimCode   ) ", nativeQuery = true)
    List<TblTenancynote> findNotesOnTenancybyUserAndCategory(@Param("tenancyClaimCode") Long tenancyClaimCode, @Param("categorycode") String categorycode, @Param("usercode") String usercode);
}
