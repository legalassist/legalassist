package com.laportal.Repo;

import com.laportal.model.TblHirenote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblHirenoteRepo extends JpaRepository<TblHirenote, Long> {


    @Query(value = "select * \n" +
            "            from ( \n" +
            "            select * \n" +
            "              from tbl_hirenotes\n" +
            "             where usercode = :usercode  \n" +
            "               and hirecode = :hireCode \n" +
            "                \n" +
            "            union \n" +
            "                \n" +
            "            select * \n" +
            "              from tbl_hirenotes \n" +
            "             where usercategorycode = :usercategorycode  \n" +
            "               and hirecode = :hireCode) ", nativeQuery = true)
    List<TblHirenote> findNotesOnHirebyUserAndCategory(@Param("hireCode") Long hireCode, @Param("usercategorycode") String usercategorycod, @Param("usercode") String usercode);


    @Query(value = "  select n.* from TBL_COMPANYPROFILE c, tbl_users u,tbl_hirenotes n\n" +
            "                                   where c.companycode = u.companycode\n" +
            "                                   and c.categorycode = '4'\n" +
            "                                    and n.usercategorycode = '4'\n" +
            "                                   and u.usercode = n.usercode\n" +
            "                                   and u.usercode = :usercode\n" +
            "                                   and n.hirecode = :hireCode\n" +
            "                                   order by n.createdon asc    ", nativeQuery = true)
    List<TblHirenote> getAuthRtaCaseNotesOfLegalInternal(@Param("hireCode") Long hireCode,@Param("usercode")  String usercode);
}
