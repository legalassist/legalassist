package com.laportal.Repo;

import com.laportal.model.TblSystem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblSystemRepo extends JpaRepository<TblSystem, String> {
}
