package com.laportal.Repo;

import com.laportal.model.TblHdrdocument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblHdrdocumentRepo extends JpaRepository<TblHdrdocument, Long> {

    List<TblHdrdocument> findByTblHdrclaimHdrclaimcode(long hdrClaimCode);
}
