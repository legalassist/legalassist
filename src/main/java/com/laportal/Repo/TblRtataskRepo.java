package com.laportal.Repo;

import com.laportal.model.TblRtatask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblRtataskRepo extends JpaRepository<TblRtatask, Long> {

    List<TblRtatask> findByTblRtaclaimRtacode(Long rtaCode);

    @Query(value = "select * from tbl_rtatasks where rtacode = :rtaCode and taskcode = :taskCode", nativeQuery = true)
    TblRtatask getRtaTaskAgainstRtaCodeAndTaskCode(@Param("rtaCode") long rtaCode, @Param("taskCode") long taskCode);

    @Modifying
    @Query(value = "update Tbl_Rtatasks  set status = :status, remarks = :remarks where taskcode = :rtataskcode and rtacode = :rtacode", nativeQuery = true)
    int updatetask(@Param("status") String status, @Param("remarks") String remarks, @Param("rtataskcode") long rtataskcode, @Param("rtacode") long rtacode);

    @Modifying
    @Query(value = "update Tbl_Rtatasks  set currenttask = :status where rtataskcode = :rtataskcode and rtacode = :rtacode", nativeQuery = true)
    int setCurrentTask(@Param("status") String status, @Param("rtataskcode") long rtataskcode, @Param("rtacode") long rtacode);

    @Modifying
    @Query(value = "update Tbl_Rtatasks  set currenttask = :status where rtacode = :rtacode", nativeQuery = true)
    int setAllTask(@Param("status") String status, @Param("rtacode") long rtacode);

    List<TblRtatask> findByTblRtaclaimRtacodeAndStatusAndTblTaskMandatory(long rtacode, String status,String mandatory);
}
