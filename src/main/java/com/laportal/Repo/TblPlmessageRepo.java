package com.laportal.Repo;

import com.laportal.model.TblPlmessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblPlmessageRepo extends JpaRepository<TblPlmessage, Long> {
    List<TblPlmessage> findByTblPlclaimPlclaimcode(Long plclaimcode);
}
