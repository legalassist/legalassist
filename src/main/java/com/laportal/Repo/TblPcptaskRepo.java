package com.laportal.Repo;

import com.laportal.model.TblPcptask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;

public interface TblPcptaskRepo extends JpaRepository<TblPcptask, Long> {
    List<TblPcptask> findByTblPcpclaimPcpclaimcode(long pcpclaimcode);

    TblPcptask findByTblPcpclaimPcpclaimcodeAndTaskcode(long pcpclaimcode, BigDecimal bigDecimal);
}
