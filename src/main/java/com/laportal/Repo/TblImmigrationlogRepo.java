package com.laportal.Repo;

import com.laportal.model.TblImmigrationlog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblImmigrationlogRepo extends JpaRepository<TblImmigrationlog, Long> {

    List<TblImmigrationlog> findByTblImmigrationclaimImmigrationclaimcode(Long valueOf);

}
