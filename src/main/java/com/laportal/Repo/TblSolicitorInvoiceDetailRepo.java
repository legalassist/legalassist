package com.laportal.Repo;

import com.laportal.model.TblSolicitorInvoiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;

public interface TblSolicitorInvoiceDetailRepo extends JpaRepository<TblSolicitorInvoiceDetail, Long> {

    List<TblSolicitorInvoiceDetail> findByTblSolicitorInvoiceHeadInvoiceheadid(long invoiceHeadId);

    List<TblSolicitorInvoiceDetail> findByTblCompaignCompaigncodeAndCasecode(String compaignCode,BigDecimal caseCode);

}
