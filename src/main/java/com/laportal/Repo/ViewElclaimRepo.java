package com.laportal.Repo;

import com.laportal.model.ViewElclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ViewElclaimRepo extends JpaRepository<ViewElclaim, BigDecimal> {


    @Query(value = "SELECT *\n" +
            "  FROM VIEW_ELCLAIM\n" +
            " WHERE :LOGINUSER = CASE WHEN :USERCATEGORY = 1 THEN ADVISOR\n" +
            "                         WHEN :USERCATEGORY = 2 THEN SOLICITOR_USERCODE\n" +
            "                         WHEN :USERCATEGORY = 4 THEN :LOGINUSER\n" +
            "                    END\n" +
            "order by createdate desc",nativeQuery = true)
    List<ViewElclaim> getAuthElCasesUserAndCategoryWise(@Param("LOGINUSER") BigDecimal userCode,@Param("USERCATEGORY") BigDecimal userCatCode);

    @Query(value = "SELECT *\n" +
            "  FROM VIEW_ELCLAIM\n" +
            " WHERE :LOGINUSER = CASE WHEN :USERCATEGORY = 1 THEN ADVISOR\n" +
            "                         WHEN :USERCATEGORY = 2 THEN SOLICITOR_USERCODE\n" +
            "                         WHEN :USERCATEGORY = 4 THEN :LOGINUSER\n" +
            "                    END \n" +
            "And statuscode = :statusCode \n" +
            "order by createdate desc",nativeQuery = true)
    List<ViewElclaim> getAuthElCasesUserAndCategoryWiseAndStatusWise(@Param("LOGINUSER") BigDecimal userCode,@Param("USERCATEGORY") BigDecimal userCatCode,@Param("statusCode") BigDecimal statusCode);
}
