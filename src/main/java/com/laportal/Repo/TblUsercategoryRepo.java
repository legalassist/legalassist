package com.laportal.Repo;

import com.laportal.model.TblUsercategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblUsercategoryRepo extends JpaRepository<TblUsercategory, String> {


    List<TblUsercategory> findByStatus(String status);
}
