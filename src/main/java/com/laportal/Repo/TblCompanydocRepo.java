package com.laportal.Repo;

import com.laportal.model.TblCompanydoc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblCompanydocRepo extends JpaRepository<TblCompanydoc, Long> {

    TblCompanydoc findByTblCompanyprofileCompanycodeAndAgenatureAndCountrytypeAndTblCompaignCompaigncode(String companyCode, String ageNature, String countryType, String compaigncode);
    TblCompanydoc findByTblCompanyprofileCompanycodeAndTblCompaignCompaigncodeAndJointtenency(String companyCode, String compaigncode, String jointenency);
    TblCompanydoc findByTblCompanyprofileCompanycodeAndTblCompaignCompaigncodeAndBike(String companyCode, String compaigncode, String bike);
    TblCompanydoc findByTblCompanyprofileCompanycodeAndTblCompaignCompaigncode(String companyCode, String compaigncode);

    List<TblCompanydoc> findByTblCompanyprofileCompanycode(String companyCode);
}
