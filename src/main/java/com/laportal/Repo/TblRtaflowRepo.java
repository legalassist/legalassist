package com.laportal.Repo;

import com.laportal.model.TblRtaflow;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblRtaflowRepo extends JpaRepository<TblRtaflow, Long> {

    List<TblRtaflow> findByTblCompaignCompaigncode(String compaigncode);

    TblRtaflow findByTblCompaignCompaigncodeAndTblRtastatusStatuscode(String campaignCode,Long status);
}
