package com.laportal.Repo;

import com.laportal.model.TblPcpdocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblPcpdocumentRepo extends JpaRepository<TblPcpdocument, Long> {
}
