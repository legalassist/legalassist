package com.laportal.Repo;

import com.laportal.model.TblElclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface TblElclaimRepo extends JpaRepository<TblElclaim, Long> {

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE\n" +
            "               FROM TBL_ElCLAIM C  \n" +
            "               RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE WHERE S.COMPAIGNCODE = 9  \n" +
            "               and s.show_status is null \n" +
            "               GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc", nativeQuery = true)
    List<Object> getAllElStatusCounts();

    @Query(value = "select c.Elclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, \n" +
            "       c.Elcode as code, fullname as client, s.descr status \n" +
            "    from tbl_Elclaim c \n" +
            "    left outer  join tbl_rtastatus s on s.statuscode = c.status \n", nativeQuery = true)
    List<Object> getElCasesListUserWise();

    @Query(value = "select c.Elclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.Elcode as code, fullname as client, s.descr status \n" +
            "            from TBL_ElCLAIM c \n" +
            "            left outer  join tbl_rtastatus s on s.statuscode = c.status \n" +
            "            where c.status = :statusId", nativeQuery = true)
    List<Object> getElCasesListStatusWise(@Param("statusId") long statusId);

    @Modifying
    @Query(value = " UPDATE  TblElclaim SET status = :statusCode,lastupdateuser = :lastupdateuser WHERE elclaimcode = :elclaimcode")
    int performAction(@Param("statusCode") BigDecimal statusCode, @Param("elclaimcode") Long elclaimcode, @Param("lastupdateuser") String lastupdateuser);

    @Modifying
    @Query(value = " UPDATE  TblElclaim SET remarks = :reason, status = :statusCode,lastupdateuser = :lastupdateuser WHERE elclaimcode = :elclaimcode")
    int performRejectCancelAction(@Param("reason") String reason, @Param("statusCode") String toStatus, @Param("elclaimcode") Long elclaimcode, @Param("lastupdateuser") String lastupdateuser);

    @Query(value = "SELECT ELCLAIMCODE,CREATEDATE,ELCODE,CLIENTNAME,STATUS,CLIENTEMAIL,ADDRESS,CLIENTMOBILE,UPDATEDATE,ADVISOR\n" +
            "FROM VIEW_ElCLAIMINTRODUCERS\n" +
            "WHERE CREATEDBY = :userId", nativeQuery = true)
    List<Object> getElCasesListForIntroducers(@Param("userId") String usercode);

    @Query(value = "SELECT Elclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, SOLICITORCODE\n" +
            "FROM VIEW_ElCLAIMSOLICITORS\n" +
            "WHERE SOLICITORCODE = :userId", nativeQuery = true)
    List<Object> getElCasesListForSolicitor(@Param("userId") String usercode);

    @Query(value = "SELECT ELCLAIMCODE,CREATEDATE,ELCODE,CLIENTNAME,STATUS,CLIENTEMAIL,ADDRESS,CLIENTMOBILE,UPDATEDATE,ADVISOR\n" +
            "FROM VIEW_ElCLAIM", nativeQuery = true)
    List<Object> getElCasesList();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_ElCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "INNER JOIN TBL_USERS U ON U.USERCODE = C.CREATEUSER\n" +
            "inner join tbl_Elsolicitor hs on c.Elclaimcode = hs.Elclaimcode and hs.status = 'Y'\n" +
            "inner join tbl_users uu on uu.usercode = hs.usercode\n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode and cp.companycode = :companyCode \n" +
            "WHERE S.COMPAIGNCODE = 9  \n" +
            " and s.show_status is null \n" +
            "GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc", nativeQuery = true)
    List<Object> getAllElStatusCountsForSolicitors(@Param("companyCode") String companyCode);

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_ElCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 9 " +
            "And  c.introducer = :companyCode " +
            "and s.show_status is null \n" +
            "GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc ", nativeQuery = true)
    List<Object> getAllElStatusCountsForIntroducers(@Param("companyCode") String companyCode);

    @Modifying
    @Query(value = " UPDATE  TblElclaim SET clawbackDate = :clawbackDate WHERE elclaimcode = :elclaimcode")
    int updateClawbackdate(@Param("clawbackDate") Date clawbackDate, @Param("elclaimcode") Long elclaimcode);

    @Modifying
    @Query(value = " UPDATE  TblElclaim SET submitDate = :submitDate WHERE elclaimcode = :elclaimcode")
    int updateSubmitDate(@Param("submitDate") Date submitDate, @Param("elclaimcode") Long elclaimcode);

    @Query(value = "SELECT Elclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, CREATEDBY\n" +
            "FROM VIEW_ElCLAIMINTRODUCERS\n" +
            "WHERE CREATEDBY = :userId and status = :statusId", nativeQuery = true)
    List<Object> getAuthElCasesIntroducersAndStatus(@Param("userId") String usercode,@Param("statusId") String statusId);


    @Query(value = "SELECT Elclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, SOLICITORCODE\n" +
            "FROM VIEW_ElCLAIMSOLICITORS\n" +
            "WHERE SOLICITORCODE = :userId and status = :statusId", nativeQuery = true)
    List<Object> getElCasesListForSolicitorAndStatus(@Param("userId") String usercode,@Param("statusId") String statusId);

    @Query(value = "SELECT elclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate\n" +
            "FROM VIEW_ElCLAIM and status = :statusId", nativeQuery = true)
    List<Object> getElCasesListAndStatus(@Param("statusId") String statusId);


    @Query(value = "SELECT E.ATTRIBUTE_NAME, E.OLD_VALUE, E.NEW_VALUE, E.AUDIT_DATE, U.LOGINID\n" +
            "  FROM LAUSER_AUDIT.TBL_ELCLAIM E\n" +
            "  LEFT JOIN TBL_USERS U ON E.USER_ID = U.USERCODE\n" +
            " WHERE E.REFPK = :elcode \n" +
            " ORDER BY E.AUDIT_ID DESC", nativeQuery = true)
    List<Object> getElAuditLogs(@Param("elcode")Long rtacode);
}
