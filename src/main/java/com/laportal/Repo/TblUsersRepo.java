package com.laportal.Repo;

import com.laportal.model.TblUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblUsersRepo extends JpaRepository<TblUser, String> {

    List<TblUser> findByCompanycode(String companyCode);

    @Query(value = "SELECT SEQ_TBL_USERS.nextval from dual", nativeQuery = true)
    int getSeq();

    @Query(value = "select u from TblUser u where UPPER(u.loginid) = upper(:loginId) ")
    TblUser getUserbyLoginId(@Param("loginId") String loginId);


    @Query(value = "select u from TblUser u where UPPER(u.loginid) = upper(:loginId) and u.password = :pwd")
    TblUser getUserbyLoginIdAndPassword(@Param("loginId") String loginId,@Param("pwd") String pwd);

    TblUser findByUsername(String userName);

    TblUser findByUsercode(String usercode);

    @Modifying
    @Query("update TblUser u set u.password = :password, u.pwdlastupdated = sysdate, u.pwdupdateflag = 'Y' where u.usercode = :usercode")
    int changeUserPassword(@Param("password") String password, @Param("usercode") String usercode);


    @Query(value = "select u.* \n" +
            "from TBL_USERS u , TBL_COMPANYPROFILE c\n" +
            "where c.COMPANYCODE = u.COMPANYCODE\n" +
            "and c.CATEGORYCODE = '4'" +
            "and u.status = 'Y'\n" +
            "and c.companystatus =  'Y'", nativeQuery = true)
    List<TblUser> lovInternalUser();

    @Query(value = "select u.* \n" +
            "from TBL_USERS u , TBL_COMPANYPROFILE c\n" +
            "where c.COMPANYCODE = u.COMPANYCODE\n" +
            "and c.CATEGORYCODE = '2'" +
            "and u.status = 'Y'\n" +
            "and c.companystatus =  'Y'", nativeQuery = true)
    List<TblUser> lovSolicitorUser();

    @Query(value = "select u.* \n" +
            "from TBL_USERS u , TBL_COMPANYPROFILE c\n" +
            "where c.COMPANYCODE = u.COMPANYCODE\n" +
            "and c.CATEGORYCODE = '1'" +
            "and u.status = 'Y'\n" +
            "and c.companystatus =  'Y'", nativeQuery = true)
    List<TblUser> lovintorducerUser();

}
