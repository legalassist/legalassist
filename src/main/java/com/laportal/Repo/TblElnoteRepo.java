package com.laportal.Repo;

import com.laportal.model.TblElnote;
import com.laportal.model.TblRtanote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblElnoteRepo extends JpaRepository<TblElnote, Long> {

    @Query(value = "select * from (select *   \n" +
            "                            from tbl_elnotes  \n" +
            "                           where usercode = :usercode    \n" +
            "                             and elClaimCode = :elClaimCode   \n" +
            "                              \n" +
            "                        union   \n" +
            "                              \n" +
            "                         select *   \n" +
            "                           from tbl_elnotes   \n" +
            "                          where usercategorycode = :categorycode    \n" +
            "                            and elClaimCode = :elClaimCode   ) order by createdon asc", nativeQuery = true)
    List<TblElnote> findNotesOnElbyUserAndCategory(@Param("elClaimCode") Long elClaimCode, @Param("categorycode") String categorycode, @Param("usercode") String usercode);


    @Query(value = "  select n.* from TBL_COMPANYPROFILE c, tbl_users u,tbl_elnotes n\n" +
            "             where c.companycode = u.companycode\n" +
            "             and c.categorycode = '4'\n" +
            "              and n.usercategorycode = '4'\n" +
            "             and u.usercode = n.usercode\n" +
            "             and u.usercode = :usercode" +
            "             and n.ELCLAIMCODE = :elcode" +
            "             order by n.createdon asc ", nativeQuery = true)
    List<TblElnote> getAuthElCaseNotesOfLegalInternal(@Param("elcode")Long elcode, @Param("usercode") String usercode);
}
