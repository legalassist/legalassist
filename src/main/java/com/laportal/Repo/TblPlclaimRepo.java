package com.laportal.Repo;

import com.laportal.model.TblPlclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface TblPlclaimRepo extends JpaRepository<TblPlclaim, Long> {

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE\n" +
            "               FROM TBL_PLCLAIM C  \n" +
            "               RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE WHERE S.COMPAIGNCODE = 3  \n" +
            "               GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllPlStatusCounts();

    @Query(value = "select c.plclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, \n" +
            "       c.plcode as code, fullname as client, s.descr status \n" +
            "    from tbl_plclaim c \n" +
            "    left outer  join tbl_rtastatus s on s.statuscode = c.status \n", nativeQuery = true)
    List<Object> getPlCasesListUserWise();

    @Query(value = "select c.plclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.plcode as code, fullname as client, s.descr status \n" +
            "            from TBL_PLCLAIM c \n" +
            "            left outer  join tbl_rtastatus s on s.statuscode = c.status \n" +
            "            where c.status = :statusId", nativeQuery = true)
    List<Object> getPlCasesListStatusWise(@Param("statusId") long statusId);

    @Modifying
    @Query(value = " UPDATE  TblPlclaim SET status = :statusCode,lastupdateuser = :lastupdateuser WHERE plclaimcode = :plclaimcode")
    int performAction(@Param("statusCode") BigDecimal statusCode, @Param("plclaimcode") Long plclaimcode,@Param("lastupdateuser") String lastupdateuser);

    @Modifying
    @Query(value = " UPDATE  TblPlclaim SET remarks = :reason, status = :statusCode ,lastupdateuser = :lastupdateuser WHERE plclaimcode = :plclaimcode")
    int performRejectCancelAction(@Param("reason") String reason, @Param("statusCode") String toStatus, @Param("plclaimcode") Long plclaimcode, @Param("lastupdateuser") String lastupdateuser);

    @Query(value = "SELECT pcpclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, CREATEDBY\n" +
            "FROM VIEW_PLCLAIMINTRODUCERS\n" +
            "WHERE CREATEDBY = :userId", nativeQuery = true)
    List<Object> getPlCasesListForIntroducers(@Param("userId") String usercode);

    @Query(value = "SELECT pcpclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, SOLICITORCODE\n" +
            "FROM VIEW_PLCLAIMSOLICITORS\n" +
            "WHERE SOLICITORCODE = :userId", nativeQuery = true)
    List<Object> getPlCasesListForSolicitor(@Param("userId") String usercode);

    @Query(value = "SELECT plclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate\n" +
            "FROM VIEW_PLCLAIM", nativeQuery = true)
    List<Object> getPlCasesList();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_PlCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "INNER JOIN TBL_USERS U ON U.USERCODE = C.CREATEUSER\n" +
            "inner join tbl_plsolicitor hs on c.plclaimcode = hs.plclaimcode and hs.status = 'Y'\n" +
            "inner join tbl_users uu on uu.usercode = hs.usercode\n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode\n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllPlStatusCountsForSolicitors();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_PlCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllPlStatusCountsForIntroducers();

    @Modifying
    @Query(value = " UPDATE  TblPlclaim SET clawbackDate = :clawbackDate WHERE plclaimcode = :plclaimcode")
    int updateClawbackdate(@Param("clawbackDate") Date clawbackDate, @Param("plclaimcode") Long plclaimcode);

    @Modifying
    @Query(value = " UPDATE  TblPlclaim SET submitDate = :submitDate WHERE plclaimcode = :plclaimcode")
    int updateSubmitDate(@Param("submitDate") Date submitDate, @Param("plclaimcode") Long plclaimcode);

    @Query(value = "SELECT E.ATTRIBUTE_NAME, E.OLD_VALUE, E.NEW_VALUE, E.AUDIT_DATE, U.LOGINID\n" +
            "  FROM LAUSER_AUDIT.TBL_PLCLAIM E\n" +
            "  LEFT JOIN TBL_USERS U ON E.USER_ID = U.USERCODE\n" +
            " WHERE E.REFPK = :plcode \n" +
            " ORDER BY E.AUDIT_ID DESC", nativeQuery = true)
    List<Object> getPlAuditLogs(@Param("plcode")Long plcode);
}
