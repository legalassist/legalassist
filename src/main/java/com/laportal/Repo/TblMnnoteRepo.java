package com.laportal.Repo;

import com.laportal.model.TblMnnote;
import com.laportal.model.TblPlnote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblMnnoteRepo extends JpaRepository<TblMnnote, Long> {
    @Query(value = "select * from (select *   \n" +
            "                            from tbl_mnnotes  \n" +
            "                           where usercode = :usercode    \n" +
            "                             and mnClaimCode = :DbClaimCode   \n" +
            "                              \n" +
            "                        union   \n" +
            "                              \n" +
            "                         select *   \n" +
            "                           from tbl_mnnotes   \n" +
            "                          where usercategorycode = :categorycode    \n" +
            "                            and mnClaimCode = :mnClaimCode   )order by createdon asc  ", nativeQuery = true)
    List<TblMnnote> findNotesOnMnbyUserAndCategory(@Param("mnClaimCode") Long mnClaimCode, @Param("categorycode") String categorycode, @Param("usercode") String usercode);

    @Query(value = "  select n.* from TBL_COMPANYPROFILE c, tbl_users u,tbl_mnnotes n\n" +
            "             where c.companycode = u.companycode\n" +
            "             and c.categorycode = '4'\n" +
            "              and n.usercategorycode = '4'\n" +
            "             and u.usercode = n.usercode\n" +
            "             and u.usercode = :usercode" +
            "             and n.mnClaimCode = :mnClaimCode" +
            "             order by n.createdon asc ", nativeQuery = true)
    List<TblMnnote> getAuthMnCaseNotesOfLegalInternal(@Param("mnClaimCode") Long mnClaimCode, @Param("usercode") String usercode);

}
