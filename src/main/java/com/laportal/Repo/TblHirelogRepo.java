package com.laportal.Repo;

import com.laportal.model.TblHirelog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblHirelogRepo extends JpaRepository<TblHirelog, Long> {

    List<TblHirelog> findByTblHireclaimHirecode(Long hireCode);

}
