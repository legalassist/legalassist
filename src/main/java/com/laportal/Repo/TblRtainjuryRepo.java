package com.laportal.Repo;

import com.laportal.model.TblRtainjury;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblRtainjuryRepo extends JpaRepository<TblRtainjury, Long> {

    @Query(value = "SELECT I.RTACODE, I.INJCLASSCODE, C.INJCLASSNAME, C.INJLEVEL, C.DESCR\n" +
            "              FROM TBL_RTAINJURY I\n" +
            "             INNER JOIN TBL_INJCLASS C ON I.INJCLASSCODE = C.INJCLASSCODE\n" +
            "             WHERE I.rtacode = :rtacode ", nativeQuery = true)
    List<Object> getAllInjuriesWithDescription(@Param("rtacode") Long rtacode);

    @Modifying
    @Query(value = "delete from tbl_rtaInjury where rtacode = :rtacode", nativeQuery = true)
    void deleteAllInjuriesOfRta(@Param("rtacode") Long rtacode);

    List<TblRtainjury> findByTblRtaclaimRtacode(long rtacode);
}
