package com.laportal.Repo;

import com.laportal.model.TblHdrsolicitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TblHdrsolicitorRepo extends JpaRepository<TblHdrsolicitor, Long> {

    @Modifying
    @Query(value = " UPDATE TblHdrsolicitor SET status = :statusCode WHERE tblHdrclaim.hdrclaimcode = :hdrclaimcode")
    int updateSolicitor(@Param("statusCode") String statusCode, @Param("hdrclaimcode") long hdrclaimcode);

    TblHdrsolicitor findByTblHdrclaimHdrclaimcodeAndStatus(long hdrClaimCode, String status);

    @Modifying
    @Query(value = " UPDATE TblHdrsolicitor SET remarks = :remarks,status = :statusCode WHERE tblHdrclaim.hdrclaimcode = :hdrclaimcode")
    int updateRemarksReason(@Param("remarks") String remarks, @Param("statusCode") String statusCode, @Param("hdrclaimcode") long hdrclaimcode);

}
