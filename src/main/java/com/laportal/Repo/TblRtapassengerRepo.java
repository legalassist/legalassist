package com.laportal.Repo;

import com.laportal.model.TblRtapassenger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblRtapassengerRepo extends JpaRepository<TblRtapassenger, Long> {


    List<TblRtapassenger> findByTblRtaclaimRtacode(Long rtaCode);


}
