package com.laportal.Repo;

import com.laportal.model.TblOlclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface TblOlclaimRepo extends JpaRepository<TblOlclaim, Long> {

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE\n" +
            "               FROM TBL_OLCLAIM C  \n" +
            "               RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE WHERE S.COMPAIGNCODE = 3  \n" +
            "               GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllOlStatusCounts();

    @Query(value = "select c.olclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, \n" +
            "       c.olcode as code, fullname as client, s.descr status \n" +
            "    from tbl_olclaim c \n" +
            "    left outer  join tbl_rtastatus s on s.statuscode = c.status \n", nativeQuery = true)
    List<Object> getOlCasesListUserWise();

    @Query(value = "select c.olclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.olcode as code, fullname as client, s.descr status \n" +
            "            from TBL_OLCLAIM c \n" +
            "            left outer  join tbl_rtastatus s on s.statuscode = c.status \n" +
            "            where c.status = :statusId", nativeQuery = true)
    List<Object> getOlCasesListStatusWise(@Param("statusId") long statusId);

    @Modifying
    @Query(value = " UPDATE  TblOlclaim SET status = :statusCode,lastupdateuser = :lastupdateuser WHERE olclaimcode = :olclaimcode")
    int performAction(@Param("statusCode") BigDecimal statusCode, @Param("olclaimcode") Long olclaimcode, @Param("lastupdateuser") String lastupdateuser);

    @Modifying
    @Query(value = " UPDATE  TblOlclaim SET remarks = :reason, status = :statusCode ,lastupdateuser = :lastupdateuser WHERE olclaimcode = :olclaimcode")
    int performRejectCancelAction(@Param("reason") String reason, @Param("statusCode") String toStatus, @Param("olclaimcode") Long olclaimcode, @Param("lastupdateuser") String lastupdateuser);

    @Query(value = "SELECT olclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, CREATEDBY\n" +
            "FROM VIEW_OLCLAIMINTRODUCERS\n" +
            "WHERE CREATEDBY = :userId", nativeQuery = true)
    List<Object> getOlCasesListForIntroducers(@Param("userId") String usercode);

    @Query(value = "SELECT olclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, SOLICITORCODE\n" +
            "FROM VIEW_OLCLAIMSOLICITORS\n" +
            "WHERE SOLICITORCODE = :userId", nativeQuery = true)
    List<Object> getOlCasesListForSolicitor(@Param("userId") String usercode);

    @Query(value = "SELECT olclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate\n" +
            "FROM VIEW_OLCLAIM", nativeQuery = true)
    List<Object> getOlCasesList();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_OLCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "INNER JOIN TBL_USERS U ON U.USERCODE = C.CREATEUSER\n" +
            "inner join tbl_pcpsolicitor hs on c.olclaimcode = hs.olclaimcode and hs.status = 'Y'\n" +
            "inner join tbl_users uu on uu.usercode = hs.usercode\n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode\n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllOlStatusCountsForSolicitors();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_OLCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllOlStatusCountsForIntroducers();


    @Modifying
    @Query(value = " UPDATE  TblOlclaim SET clawbackDate = :clawbackDate WHERE olclaimcode = :olclaimcode")
    int updateClawbackdate(@Param("clawbackDate") Date clawbackDate, @Param("olclaimcode") Long olclaimcode);

    @Modifying
    @Query(value = " UPDATE  TblOlclaim SET submitDate = :submitDate WHERE olclaimcode = :olclaimcode")
    int updateSubmitDate(@Param("submitDate") Date submitDate, @Param("olclaimcode") Long olclaimcode);


    @Query(value = "SELECT E.ATTRIBUTE_NAME, E.OLD_VALUE, E.NEW_VALUE, E.AUDIT_DATE, U.LOGINID\n" +
            "  FROM LAUSER_AUDIT.TBL_OLCLAIM E\n" +
            "  LEFT JOIN TBL_USERS U ON E.USER_ID = U.USERCODE\n" +
            " WHERE E.REFPK = :olcode \n" +
            " ORDER BY E.AUDIT_ID DESC", nativeQuery = true)
    List<Object> getOlAuditLogs(@Param("olcode")Long olcode);



}
