package com.laportal.Repo;

import com.laportal.model.TblEmail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;

public interface TblEmailRepo extends JpaRepository<TblEmail, Long> {

    List<TblEmail> findBySenflag(BigDecimal sendFlag);

}
