package com.laportal.Repo;

import com.laportal.model.TblPlnote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblPlnoteRepo extends JpaRepository<TblPlnote, Long> {

    @Query(value = "select * from (select *   \n" +
            "                            from tbl_plnotes  \n" +
            "                           where usercode = :usercode    \n" +
            "                             and plClaimCode = :plClaimCode   \n" +
            "                              \n" +
            "                        union   \n" +
            "                              \n" +
            "                         select *   \n" +
            "                           from tbl_plnotes   \n" +
            "                          where usercategorycode = :categorycode    \n" +
            "                            and plClaimCode = :plClaimCode   )order by createdon asc  ", nativeQuery = true)
    List<TblPlnote> findNotesOnPlbyUserAndCategory(@Param("plClaimCode") Long plClaimCode, @Param("categorycode") String categorycode, @Param("usercode") String usercode);


    @Query(value = "  select n.* from TBL_COMPANYPROFILE c, tbl_users u,tbl_plnotes n\n" +
            "             where c.companycode = u.companycode\n" +
            "             and c.categorycode = '4'\n" +
            "              and n.usercategorycode = '4'\n" +
            "             and u.usercode = n.usercode\n" +
            "             and u.usercode = :usercode" +
            "             and n.plClaimCode = :plClaimCode" +
            "             order by n.createdon asc ", nativeQuery = true)
    List<TblPlnote> getAuthPlCaseNotesOfLegalInternal(@Param("plClaimCode") Long plClaimCode,@Param("usercode") String usercode);
}
