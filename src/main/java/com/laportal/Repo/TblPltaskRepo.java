package com.laportal.Repo;

import com.laportal.model.TblPltask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TblPltaskRepo extends JpaRepository<TblPltask, Long> {

    List<TblPltask> findByTblPlclaimPlclaimcode(long plclaimcode);
    List<TblPltask> findByTblPlclaimPlclaimcodeAndStatus(long plclaimcode,String status);

    TblPltask findByTblPlclaimPlclaimcodeAndTblTaskTaskcode(long plclaimcode, BigDecimal taskCode);

    @Modifying
    @Query(value = "update TBL_PLTASKS  set status = :status, remarks = :remarks where taskcode = :plTaskCode and PLCLAIMCODE = :PLCLAIMCODE", nativeQuery = true)
    int updatetask(@Param("status") String status, @Param("remarks") String remarks, @Param("plTaskCode") long eltaskcode, @Param("PLCLAIMCODE") long Elcode);

    @Modifying
    @Query(value = "update TBL_PLTASKS  set CURRENTTASK = :status where PLCLAIMCODE = :PLCLAIMCODE", nativeQuery = true)
    int setAllTask(@Param("status") String status, @Param("PLCLAIMCODE") long PLCLAIMCODE);

    @Modifying
    @Query(value = "update TBL_PLTASKS  set CURRENTTASK = :status where PLTASKCODE = :pltaskcode and PLCLAIMCODE = :PLCLAIMCODE", nativeQuery = true)
    int setCurrentTask(@Param("status") String status, @Param("pltaskcode") long pltaskcode, @Param("PLCLAIMCODE") long PLCLAIMCODE);

}
