package com.laportal.Repo;

import com.laportal.model.TblBroadcast;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblBroadcastRepo extends JpaRepository<TblBroadcast, Long> {
    List<TblBroadcast> findByStatus(String status);

}
