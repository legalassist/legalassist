package com.laportal.Repo;

import com.laportal.model.TblHiredocument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblHiredocumentRepo extends JpaRepository<TblHiredocument, Long> {

    List<TblHiredocument> findByTblHireclaimHirecode(long hireCode);

}
