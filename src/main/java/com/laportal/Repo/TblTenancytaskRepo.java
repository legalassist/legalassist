package com.laportal.Repo;

import com.laportal.model.TblTenancytask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;

public interface TblTenancytaskRepo extends JpaRepository<TblTenancytask, Long> {

    TblTenancytask findByTblTenancyclaimTenancyclaimcodeAndTaskcode(long tenancyclaimcode, BigDecimal bigDecimal);

    List<TblTenancytask> findByTblTenancyclaimTenancyclaimcode(long tenancyclaimcode);
}
