package com.laportal.Repo;

import com.laportal.model.TblImmigrationtask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;

public interface TblImmigrationtaskRepo extends JpaRepository<TblImmigrationtask, Long> {

    TblImmigrationtask findByTblImmigrationclaimImmigrationclaimcodeAndTaskcode(long dbclaimcode, BigDecimal bigDecimal);

    List<TblImmigrationtask> findByTblImmigrationclaimImmigrationclaimcode(long dbclaimcode);

}
