package com.laportal.Repo;

import com.laportal.model.TblHdrtask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TblHdrtaskRepo extends JpaRepository<TblHdrtask, Long> {

    @Modifying
    @Query(value = "update Tbl_Hdrtasks  set status = :status, remarks = :remarks where hdrtaskcode = :hdrtaskcode and hdrcode = :hdrclaimcode", nativeQuery = true)
    int updatetask(@Param("status") String status, @Param("remarks") String remarks, @Param("hdrtaskcode") long hdrtaskcode, @Param("hdrclaimcode") long hdrclaimcode);


    @Query(value = "select h.*\n" +
            "from TBL_HDRTASKS h ,TBL_TASKS t\n" +
            "where h.taskcode = t.taskcode\n" +
            "and h.hdrcode = :hdrClaimcocde\n" +
            "and h.status = :status\n" +
            "and t.mandatory = 'Y'", nativeQuery = true)
    List<TblHdrtask> findByTaskStatusAndMandotry(@Param("hdrClaimcocde") Long hdrClaimcocde,@Param("status")  String status);

    TblHdrtask findByTblHdrclaimHdrclaimcodeAndTaskcode(long hdrClaimCode, BigDecimal taskCode);

    List<TblHdrtask> findByTblHdrclaimHdrclaimcode(long hdrClaimCode);


    @Modifying
    @Query(value = "update Tbl_Hdrtasks  set currenttask = :status where hdrtaskcode = :hdrtaskcode and hdrcode = :hdrcode", nativeQuery = true)
    int setCurrentTask(@Param("status") String status, @Param("hdrtaskcode") long hdrtaskcode, @Param("hdrcode") long rtacode);

    @Modifying
    @Query(value = "update Tbl_Hdrtasks  set currenttask = :status where hdrcode = :hdrcode", nativeQuery = true)
    int setAllTask(@Param("status") String status, @Param("hdrcode") long hdrcode);

}
