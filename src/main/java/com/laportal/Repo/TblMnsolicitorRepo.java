package com.laportal.Repo;

import com.laportal.model.TblMnsolicitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TblMnsolicitorRepo extends JpaRepository<TblMnsolicitor, Long> {

    TblMnsolicitor findByTblMnclaimMnclaimcodeAndStatus(long dbCaseId, String y);

    @Modifying
    @Query(value = " UPDATE TblMnsolicitor SET status = :statusCode WHERE tblMnclaim.mnclaimcode = :Dbclaimcode")
    int updateSolicitor(@Param("statusCode") String statusCode, @Param("Dbclaimcode") long Dbclaimcode);

    @Modifying
    @Query(value = " UPDATE TblMnsolicitor SET remarks = :remarks,status = :statusCode WHERE tblMnclaim.mnclaimcode = :Dbclaimcode")
    int updateRemarksReason(@Param("remarks") String remarks, @Param("statusCode") String statusCode, @Param("Dbclaimcode") long Dbclaimcode);

}
