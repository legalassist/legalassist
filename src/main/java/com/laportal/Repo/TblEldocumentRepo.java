package com.laportal.Repo;

import com.laportal.model.TblEldocument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblEldocumentRepo extends JpaRepository<TblEldocument, Long> {

    List<TblEldocument> findByTblElclaimElclaimcode(long elClaimCode);
}
