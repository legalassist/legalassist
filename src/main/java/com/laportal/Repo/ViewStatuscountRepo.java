package com.laportal.Repo;

import com.laportal.model.ViewOlclaim;
import com.laportal.model.ViewStatuscount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ViewStatuscountRepo extends JpaRepository<ViewStatuscount, BigDecimal> {

    @Query(value = " SELECT *\n" +
            "   FROM VIEW_STATUSCOUNT\n" +
            "  WHERE INTRODUCER = :companyCode\n" +
            "    AND COMPAIGNCODE = :campaignCode\n" +
            "  ORDER BY SEQ", nativeQuery = true)
    List<ViewStatuscount> getStatusCount(@Param("companyCode")Long companyCode, @Param("campaignCode") Long campaignCode);
}
