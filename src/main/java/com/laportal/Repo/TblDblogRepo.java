package com.laportal.Repo;

import com.laportal.model.TblDblog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblDblogRepo extends JpaRepository<TblDblog, Long> {
    List<TblDblog> findByTblDbclaimDbclaimcode(Long DbClaimCode);
}
