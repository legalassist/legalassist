package com.laportal.Repo;

import com.laportal.model.TblInvoiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblInvoiceDetailRepo extends JpaRepository<TblInvoiceDetail, Long> {
}
