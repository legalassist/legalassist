package com.laportal.Repo;

import com.laportal.model.TblPllog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblPllogRepo extends JpaRepository<TblPllog, Long> {
    List<TblPllog> findByTblPlclaimPlclaimcode(Long plClaimCode);
}
