package com.laportal.Repo;

import com.laportal.model.TblHirebusiness;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblHirebusinessRepo extends JpaRepository<TblHirebusiness, Long> {

    List<TblHirebusiness> findByStatusAndTblHireclaimHirecode(String status, long hireCode);

    TblHirebusiness findByTblHireclaimHirecodeAndTblCompanyprofileCompanycode(long hireCode, String companyCode);

    List<TblHirebusiness> findByTblHireclaimHirecode(long hireCode);

}
