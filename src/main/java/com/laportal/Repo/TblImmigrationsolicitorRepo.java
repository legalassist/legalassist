package com.laportal.Repo;

import com.laportal.model.TblImmigrationsolicitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TblImmigrationsolicitorRepo extends JpaRepository<TblImmigrationsolicitor, Long> {

    TblImmigrationsolicitor findByTblImmigrationclaimImmigrationclaimcodeAndStatus(long dbCaseId, String y);

    @Modifying
    @Query(value = " UPDATE TblImmigrationsolicitor SET status = :statusCode WHERE tblImmigrationclaim.immigrationclaimcode = :Dbclaimcode")
    int updateSolicitor(@Param("statusCode") String statusCode, @Param("Dbclaimcode") long Dbclaimcode);

    @Modifying
    @Query(value = " UPDATE TblImmigrationsolicitor SET remarks = :remarks,status = :statusCode WHERE tblImmigrationclaim.immigrationclaimcode = :Dbclaimcode")
    int updateRemarksReason(@Param("remarks") String remarks, @Param("statusCode") String statusCode, @Param("Dbclaimcode") long Dbclaimcode);

}
