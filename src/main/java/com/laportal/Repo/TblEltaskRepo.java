package com.laportal.Repo;

import com.laportal.model.TblEltask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TblEltaskRepo extends JpaRepository<TblEltask, Long> {
    List<TblEltask> findByTblElclaimElclaimcode(long elclaimcode);
    List<TblEltask> findByTblElclaimElclaimcodeAndStatus(long elclaimcode,String status);

    TblEltask findByTblElclaimElclaimcodeAndTblTaskTaskcode(long elclaimcode, BigDecimal taskcode);

    @Modifying
    @Query(value = "update TBL_ELTASKS  set status = :status, remarks = :remarks where taskcode = :eltaskcode and ELCLAIMCODE = :elcode", nativeQuery = true)
    int updatetask(@Param("status") String status, @Param("remarks") String remarks, @Param("eltaskcode") long eltaskcode, @Param("elcode") long Elcode);

    @Modifying
    @Query(value = "update TBL_ELTASKS  set CURRENTTASK = :status where ELTASKCODE = :eltaskcode and ELCLAIMCODE = :elcode", nativeQuery = true)
    int setCurrentTask(@Param("status") String status, @Param("eltaskcode") long eltaskcode, @Param("elcode") long elcode);

    @Modifying
    @Query(value = "update TBL_ELTASKS  set CURRENTTASK = :status where ELCLAIMCODE = :elcode", nativeQuery = true)
    int setAllTask(@Param("status") String status, @Param("elcode") long Elcode);
}
