package com.laportal.Repo;

import com.laportal.model.TblImmigrationnote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblImmigrationnoteRepo extends JpaRepository<TblImmigrationnote, Long> {
    @Query(value = "select * from (select *   \n" +
            "                            from tbl_immigrationnotes  \n" +
            "                           where usercode = :usercode    \n" +
            "                             and immigrationClaimCode = :DbClaimCode   \n" +
            "                              \n" +
            "                        union   \n" +
            "                              \n" +
            "                         select *   \n" +
            "                           from tbl_immigrationnotes   \n" +
            "                          where usercategorycode = :categorycode    \n" +
            "                            and immigrationClaimCode = :DbClaimCode   ) ", nativeQuery = true)
    List<TblImmigrationnote> findNotesOnImmigrationbyUserAndCategory(@Param("DbClaimCode") Long DbClaimCode, @Param("categorycode") String categorycode, @Param("usercode") String usercode);

}
