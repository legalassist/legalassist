package com.laportal.Repo;

import com.laportal.model.TblLogintoken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface TblLogintokenRepo extends JpaRepository<TblLogintoken, Long> {

    @Query("SELECT t FROM TblLogintoken t WHERE t.token = :token " +
            "AND :givenDate BETWEEN t.effectivefrom AND t.effectiveto")
    TblLogintoken findByTokenBetweenEffectiveFrom(@Param("token") String token,@Param("givenDate") Date givenDate);
}
