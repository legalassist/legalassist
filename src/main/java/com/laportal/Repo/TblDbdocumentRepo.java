package com.laportal.Repo;

import com.laportal.model.TblDbdocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblDbdocumentRepo extends JpaRepository<TblDbdocument, Long> {
}
