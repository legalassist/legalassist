package com.laportal.Repo;

import com.laportal.model.TblInvoicenote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TblInvoicenoteRepo extends JpaRepository<TblInvoicenote,Long> {


    List<TblInvoicenote> findByTblSolicitorInvoiceHeadInvoiceheadid (Long invoiceHeadId);
}
