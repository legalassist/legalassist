package com.laportal.Repo;

import com.laportal.model.ViewRtacasereport;
import com.spire.ms.System.Collections.Generic.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface ViewRtacasereportRepo extends JpaRepository<ViewRtacasereport, BigDecimal> {


    @Query(value = "SELECT *\n" +
            "  FROM VIEW_RTACASEREPORT\n" +
            " WHERE DATE_CREATED BETWEEN TO_DATE(:FROM_DATE ||' 00:00:00','DD-MON-YYYY HH24:MI:SS') \n" +
            "                        AND TO_DATE(:TO_DATE ||' 23:59:59','DD-MON-YYYY HH24:MI:SS')", nativeQuery = true)
    List<ViewRtacasereport> viewReport(@Param("TO_DATE") String toDate, @Param("FROM_DATE") String fromDate);

//    @Query(value = "SELECT *\n" +
//            "  FROM VIEW_RTACASEREPORT\n" +
//            " WHERE DATE_CREATED between TO_DATE(NVL(:FROM_DATE,DATE_CREATED), 'DD-MON-YYYY') and TO_DATE(NVL(:TO_DATE,DATE_CREATED), 'DD-MON-YYYY') \n" +
//            " -- AND REFERENCE_NUMBER = NVL(:REFERENCE_NUMBER,REFERENCE_NUMBER)\n" +
//            "  -- AND CLIENT_NAME = NVL(:CLIENT_NAME,CLIENT_NAME)\n" +
//            "  -- AND ACCIDENT_DATE = NVL(:ACCIDENT_DATE,ACCIDENT_DATE)\n" +
//            " -- AND INTRODUCER = NVL(:INTRODUCER,INTRODUCER)\n" +
//            "  -- AND SOLICITOR = NVL(:SOLICITOR,SOLICITOR)\n" +
//            "  -- AND STATUS = NVL(:STATUS,STATUS)\n" +
//            "  -- AND CURRENT_TASK = NVL(:CURRENT_TASK,CURRENT_TASK)\n" +
//            "  -- AND AML = NVL(:AML,AML)\n" +
//            "  -- AND SUBMITTED_ON = TO_DATE(NVL(:SUBMITTED_ON,SUBMITTED_ON), 'DD-MON-YYYY')\n" +
//            " -- AND PROOF_OF_ID = NVL(:PROOF_OF_ID,PROOF_OF_ID)\n" +
//            " --  AND PROOF_OF_ADDRESS = NVL(:PROOF_OF_ADDRESS,PROOF_OF_ADDRESS)\n" +
//            " --  AND CFA = NVL(:CFA,CFA)\n" +
//            "  -- AND NATIONAL_INSURANCE_NUMBER = NVL(:NATIONAL_INSURANCE_NUMBER,NATIONAL_INSURANCE_NUMBER)", nativeQuery = true)
//    List<ViewRtacasereport> viewReport(@Param("TO_DATE") String toDate, @Param("FROM_DATE") String fromDate);
                                      // @Param("REFERENCE_NUMBER") String rtanumber);
//                                       @Param("CLIENT_NAME") String clientname,
//                                       @Param("ACCIDENT_DATE") String accdatetime,
//                                       @Param("INTRODUCER") String introducer,
//                                       @Param("SOLICITOR") String solicitor,
//                                       @Param("STATUS") String status,
//                                       @Param("CURRENT_TASK") String currtask,
//                                       @Param("AML") String aml,
//                                       @Param("SUBMITTED_ON") String submittedOn);
//    ,
//                                       @Param("PROOF_OF_ID") String profofId,
//                                       @Param("PROOF_OF_ADDRESS") String proofOfAddress,
//                                       @Param("CFA") String cfa,
//                                       @Param("NATIONAL_INSURANCE_NUMBER") String niNumber);

}
