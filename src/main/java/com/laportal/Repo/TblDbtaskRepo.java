package com.laportal.Repo;

import com.laportal.model.TblDbtask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;

public interface TblDbtaskRepo extends JpaRepository<TblDbtask, Long> {
    List<TblDbtask> findByTblDbclaimDbclaimcode(long Dbclaimcode);

    TblDbtask findByTblDbclaimDbclaimcodeAndTaskcode(long Dbclaimcode, BigDecimal bigDecimal);
}
