package com.laportal.Repo;

import com.laportal.model.Onelink;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OnelinkRepo extends JpaRepository<Onelink, Long> {

    List<Onelink> findByStatusIsNull();
}
