package com.laportal.Repo;

import com.laportal.model.TblHdraffectedroom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblHdraffectedroomRepo extends JpaRepository<TblHdraffectedroom, Long> {
}
