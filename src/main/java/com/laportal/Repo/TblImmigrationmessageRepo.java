package com.laportal.Repo;

import com.laportal.model.TblImmigrationmessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblImmigrationmessageRepo extends JpaRepository<TblImmigrationmessage, Long> {

    List<TblImmigrationmessage> findByTblImmigrationclaimImmigrationclaimcode(Long valueOf);

}
