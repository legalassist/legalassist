package com.laportal.Repo;

import com.laportal.model.TblTenancylog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblTenancylogRepo extends JpaRepository<TblTenancylog, Long> {
    List<TblTenancylog> findByTblTenancyclaimTenancyclaimcode(Long tenancyClaimCode);
}
