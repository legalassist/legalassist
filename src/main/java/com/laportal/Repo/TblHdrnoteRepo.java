package com.laportal.Repo;

import com.laportal.model.TblHdrnote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblHdrnoteRepo extends JpaRepository<TblHdrnote, Long> {

    @Query(value = "select * from (select *  \n" +
            "                from tbl_hdrnotes \n" +
            "               where usercode = :usercode   \n" +
            "                 and hdrclaimcode = :hdrCode  \n" +
            "                 \n" +
            "            union  \n" +
            "                 \n" +
            "             select *  \n" +
            "               from tbl_hdrnotes  \n" +
            "              where usercategorycode = :categorycode   \n" +
            "                and hdrclaimcode = :hdrCode) ", nativeQuery = true)
    List<TblHdrnote> findNotesOnHdrbyUserAndCategory(@Param("hdrCode") Long hdrCode, @Param("categorycode") String categorycode, @Param("usercode") String usercode);


    @Query(value = "  select n.* from TBL_COMPANYPROFILE c, tbl_users u,tbl_hdrnotes n\n" +
            "                       where c.companycode = u.companycode\n" +
            "                       and c.categorycode = '4'\n" +
            "                        and n.usercategorycode = '4'\n" +
            "                       and u.usercode = n.usercode\n" +
            "                       and u.usercode = :usercode\n" +
            "                       and n.hdrclaimcode = :hdrcode\n" +
            "                       order by n.createdon asc  ", nativeQuery = true)
    List<TblHdrnote> getAuthRtaCaseNotesOfLegalInternal(@Param("hdrcode") Long hdrcode, @Param("usercode") String usercode);
}
