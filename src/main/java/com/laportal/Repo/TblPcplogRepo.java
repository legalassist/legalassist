package com.laportal.Repo;

import com.laportal.model.TblPcplog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblPcplogRepo extends JpaRepository<TblPcplog, Long> {
    List<TblPcplog> findByTblPcpclaimPcpclaimcode(Long pcpClaimCode);
}
