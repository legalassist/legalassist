package com.laportal.Repo;

import com.laportal.model.ViewRtaclamin;
import com.laportal.model.ViewRtalistintorducers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ViewRtalistintorducersRepo extends JpaRepository<ViewRtalistintorducers, BigDecimal> {
    List<ViewRtalistintorducers> findByUsercode(String usercode);

    List<ViewRtalistintorducers> findByUsercodeAndStatuscode(String usercode, String statusCode);
    List<ViewRtalistintorducers> findByIntroducerAndStatuscode(String usercode, String statusCode);

    List<ViewRtalistintorducers> findByStatuscode(String statusCode);

    @Query(value = "SELECT * \n" +
            "  FROM VIEW_RTALISTINTRODUCERS\n" +
            " WHERE UPPER(CLIENT) LIKE UPPER('%'||:NAME||'%')\n" +
            "   AND NVL(UPPER(POSTALCODE),'0') LIKE NVL(UPPER('%'||:POSTALCODE||'%'),'0')\n" +
            "   AND UPPER(RTANUMBER) LIKE UPPER('%'||:PORTALCODE||'%')\n" +
            "   AND NVL(MOBILE,'0') LIKE NVL('%'||:MOBILENO||'%','0')\n" +
            "   AND NVL(UPPER(EMAIL),'0') LIKE NVL(UPPER('%'||:EMAIL||'%'),'0')\n" +
            "   AND NVL(UPPER(NINUMBER),'0') LIKE NVL(UPPER('%'||:NINUMBER||'%'),'0')\n" +
            "   AND NVL(UPPER(REGISTERATIONNO),'0') LIKE NVL(UPPER('%'||:REGISTERATIONNO||'%'),'0')" +
            "   AND advisor = :advisor", nativeQuery = true)
    List<ViewRtalistintorducers> getFilterRtaCasesListForIntroducer(@Param("NAME") String name,
                                                    @Param("POSTALCODE") String postcode,
                                                    @Param("PORTALCODE") String portalCode,
                                                    @Param("MOBILENO") String mobileNo,
                                                    @Param("EMAIL") String email,
                                                    @Param("NINUMBER") String niNumber,
                                                    @Param("REGISTERATIONNO") String regNo,
                                                    @Param("advisor") String advisor);

    @Query(value ="SELECT * \n" +
            "  FROM VIEW_RTALISTINTRODUCERS\n" +
            " WHERE UPPER(CLIENT) LIKE UPPER('%'||:NAME||'%')\n" +
            "   AND NVL(UPPER(POSTALCODE),'0') LIKE NVL(UPPER('%'||:POSTALCODE||'%'),'0')\n" +
            "   AND UPPER(RTANUMBER) LIKE UPPER('%'||:PORTALCODE||'%')\n" +
            "   AND NVL(MOBILE,'0') LIKE NVL('%'||:MOBILENO||'%','0')\n" +
            "   AND NVL(UPPER(EMAIL),'0') LIKE NVL(UPPER('%'||:EMAIL||'%'),'0')\n" +
            "   AND NVL(UPPER(NINUMBER),'0') LIKE NVL(UPPER('%'||:NINUMBER||'%'),'0')\n" +
            "   AND NVL(UPPER(REGISTERATIONNO),'0') LIKE NVL(UPPER('%'||:REGISTERATIONNO||'%'),'0')", nativeQuery = true)
    List<ViewRtalistintorducers> getFilterRtaCasesListForLegalAssist(@Param("NAME") String name,
                                                            @Param("POSTALCODE") String postcode,
                                                            @Param("PORTALCODE") String portalCode,
                                                            @Param("MOBILENO") String mobileNo,
                                                            @Param("EMAIL") String email,
                                                            @Param("NINUMBER") String niNumber,
                                                            @Param("REGISTERATIONNO") String regNo);
}
