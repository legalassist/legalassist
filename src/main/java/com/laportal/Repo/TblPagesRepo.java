package com.laportal.Repo;

import com.laportal.model.TblPage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblPagesRepo extends JpaRepository<TblPage, String> {

    @Query(value = "SELECT SEQ_TBL_PAGES.nextval from dual", nativeQuery = true)
    int getSeq();


    @Query(value = "SELECT * FROM TBL_PAGES" +
            " WHERE MODULECODE = NVL(:moduleCode, MODULECODE) " +
            "AND COMPANYCODE = NVL(:companyCode, COMPANYCODE)" +
            "AND COMPAIGNCODE = NVL(:compaignCode, COMPAIGNCODE) ", nativeQuery = true)
    List<TblPage> getMenuPages(@Param("moduleCode") String moduleCode, @Param("companyCode") String companyCode, @Param("compaignCode") String compaignCode);

    List<TblPage> findByTblModuleModulecode(String moduleCode);

/*
    @Query(value = "select DISTINCT d.*\n" +
            "from tbl_userroles a,tbl_modules b,tbl_rolepages c,tbl_pages d\n" +
            "where a.rolecode = c.rolecode\n" +
            "and d.modulecode = b.modulecode\n" +
            "and c.pagecode = d.pagecode\n" +
            "and b.modulecode = :moduleCode\n" +
            "and a.rolecode in (:roleCodes) ", nativeQuery = true)
    List<TblPage> getRoleAndModuelWisePages(@Param("moduleCode") String moduleCode, @Param("roleCodes") List<String> roleCodes);
*/

    @Query(value = "select DISTINCT d.*\n" +
            "from tbl_modules b,tbl_rolepages c,tbl_pages d\n" +
            "where d.modulecode = b.modulecode\n" +
            "and c.pagecode = d.pagecode\n" +
            "and b.modulecode = :moduleCode\n" +
            "and c.rolecode in (:roleCodes) ", nativeQuery = true)
    List<TblPage> getRoleAndModuelWisePages(@Param("moduleCode") String moduleCode, @Param("roleCodes") List<String> roleCodes);


}
