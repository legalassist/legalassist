package com.laportal.Repo;

import com.laportal.model.TblHireclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface TblHireclaimRepo extends JpaRepository<TblHireclaim, Long> {

    @Query(value = "SELECT SEQ_HIREDOCNUMBER.nextval from dual", nativeQuery = true)
    int getHireDocNumber();

    List<TblHireclaim> findByUsercode(String userCode);

    TblHireclaim findByHirecodeAndUsercode(long hireCode, String userCode);

    TblHireclaim findByHirecode(long hireCode);

    TblHireclaim findByHirenumber(String hireNumber);

    @Modifying
    @Query(value = " UPDATE  Tbl_Hireclaim SET statuscode = :statusCode , lastupdateuser = :lastupdateuser  WHERE hirecode = :hireCode", nativeQuery = true)
    int performAction(String statusCode,String lastupdateuser, long hireCode);

    @Query(value = "SELECT hirecode, created, code, client, taskdue, taskname, status, email, address, contactno, \n" +
            "lastupdated, introducer, lastnotedate, contactdue, landline, description, injurydescription, accdate, acctime\n" +
            "      FROM VIEW_hireclaimintroducers\n" +
            "WHERE ADVISOR = :userId", nativeQuery = true)
    List<Object> getHireCasesListForIntroducers(@Param("userId") String usercode);

    @Query(value = "SELECT hirecode, created, code, client, taskdue, taskname, status, email, address, contactno, \n" +
            "lastupdated, introducer, lastnotedate, contactdue, landline, description, injurydescription, accdate, acctime\n" +
            "      FROM VIEW_hireclaimsolicitors\n" +
            "WHERE SOLICITORCODE = :userId", nativeQuery = true)
    List<Object> getHireCasesListForSolicitor(@Param("userId") String usercode);

    @Query(value = "SELECT hirecode, created, code, client, taskdue, taskname, status, email, address, contactno, \n" +
            "lastupdated, introducer, lastnotedate, contactdue, landline, description, injurydescription, accdate, acctime\n" +
            "      FROM VIEW_hireclaim", nativeQuery = true)
    List<Object> getHireCasesList();

    @Query(value = "select c.hirecode, c.created, c.code, c.client, c.taskdue, c.taskname, c.status, c.email, c.address, c.contactno, \n" +
            "   c.lastupdated, c.introducer, c.lastnotedate, c.contactdue, c.landline, c.description, c.injurydescription, c.accdate, c.acctime\n" +
            "                  from view_hireclaim c\n" +
            "                  inner join tbl_rtastatus s on c.status = s.descr\n" +
            "            where s.statuscode = :statusId", nativeQuery = true)
    List<Object> getHireCasesListStatusWise(@Param("statusId") long statusId);

    @Query(value = "SELECT NVL(COUNT(C.STATUSCODE),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_HIRECLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUSCODE = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 2  \n" +
            "AND C.introducer = :companyCode \n" +
            "AND s.show_status is null \n" +
            "GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc", nativeQuery = true)
    List<Object> getAllHireStatusCountsForIntroducers(@Param("companyCode") String companyCode);

    @Query(value = "SELECT NVL(COUNT(C.STATUSCODE),0) , S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_HIRECLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUSCODE = S.STATUSCODE \n" +
            "inner join tbl_hirebusinesses hs on c.HIRECODE = hs.HIRECODE \n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode and cp.companycode = :companyCode\n" +
            "WHERE S.COMPAIGNCODE = 2  \n" +
            "AND s.show_status is null \n" +
            "GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc", nativeQuery = true)
    List<Object> getAllHireStatusCountsForSolicitors(@Param("companyCode") String companyCode);

    @Query(value = "SELECT NVL(COUNT(C.STATUScode),0) STATUSCOUNT, S.DESCR, S.STATUSCODE\n" +
            "               FROM TBL_hireCLAIM C  \n" +
            "               RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUScode = S.STATUSCODE WHERE S.COMPAIGNCODE = 2  \n" +
            "AND s.show_status is null \n" +
            "GROUP BY S.DESCR, S.STATUSCODE,s.seq Order By s.seq asc", nativeQuery = true)
    List<Object> getAllHireStatusCounts();


    @Query(value = "SELECT hirecode, created, code, client, taskdue, taskname, status, email, address, contactno, \n" +
            "lastupdated, introducer, lastnotedate, contactdue, landline, description, injurydescription, accdate, acctime\n" +
            "      FROM VIEW_hireclaimintroducers\n" +
            "WHERE INTRODUCER_COMP_CODE = :companyCode and STATUSCODE = :statusCode", nativeQuery = true)
    List<Object> getHireCasesListForIntroducers(@Param("companyCode") String companyCode, @Param("statusCode") long statusCode);

    @Query(value = "SELECT hirecode, created, code, client, taskdue, taskname, status, email, address, contactno, \n" +
            "lastupdated, introducer, lastnotedate, contactdue, landline, description, injurydescription, accdate, acctime\n" +
            "      FROM VIEW_hireclaimsolicitors\n" +
            "WHERE SOLICITOR_COMP_CODE = :companyCode and STATUSCODE = :statusCode", nativeQuery = true)
    List<Object> getHireCasesListForSolicitor(@Param("companyCode") String companyCode, @Param("statusCode") long statusCode);


    @Query(value = "SELECT hirecode, created, code, client, taskdue, taskname, status, email, address, contactno, \n" +
            "lastupdated, introducer, lastnotedate, contactdue, landline, description, injurydescription, accdate, acctime\n" +
            "      FROM VIEW_hireclaim " +
            "WHERE STATUSCODE = :statusCode ", nativeQuery = true)
    List<Object> getHireCasesList(@Param("statusCode") long status);


    @Query(value = "SELECT hirecode, created, code, client, taskdue, taskname, status, email, address, contactno, \n" +
            "lastupdated, introducer, lastnotedate, contactdue, landline, description, injurydescription, accdate, acctime\n" +
            "  FROM VIEW_HIRECLAIMINTRODUCERS\n" +
            " WHERE UPPER(CLIENT) LIKE UPPER('%'||:NAME||'%')\n" +
            "   AND NVL(UPPER(POSTALCODE),'0') LIKE NVL(UPPER('%'||:POSTALCODE||'%'),'0')\n" +
            "   AND UPPER(CODE) LIKE UPPER('%'||:PORTALCODE||'%')\n" +
            "   AND NVL(CONTACTNO,'0') LIKE NVL('%'||:MOBILENO||'%','0')\n" +
            "   AND NVL(UPPER(EMAIL),'0') LIKE NVL(UPPER('%'||:EMAIL||'%'),'0')\n" +
            "   AND NVL(UPPER(NINUMBER),'0') LIKE NVL(UPPER('%'||:NINUMBER||'%'),'0')\n" +
            "   AND NVL(UPPER(REGISTERATIONNO),'0') LIKE NVL(UPPER('%'||:REGISTERATIONNO||'%'),'0')" +
            "   AND advisor = :advisor", nativeQuery = true)
    List<Object> getFilterHireCasesListForIntroducers(@Param("NAME") String name,
                                                @Param("POSTALCODE") String postcode,
                                                @Param("PORTALCODE") String portalCode,
                                                @Param("MOBILENO") String mobileNo,
                                                @Param("EMAIL") String email,
                                                @Param("NINUMBER") String niNumber,
                                                      @Param("REGISTERATIONNO") String regNo,
                                                      @Param("advisor") String advisor);

    @Query(value = "SELECT hirecode, created, code, client, taskdue, taskname, status, email, address, contactno, \n" +
            "lastupdated, introducer, lastnotedate, contactdue, landline, description, injurydescription, accdate, acctime\n" +
            "  FROM VIEW_HIRECLAIMSOLICITORS\n" +
            " WHERE UPPER(CLIENT) LIKE UPPER('%'||:NAME||'%')\n" +
            "   AND NVL(UPPER(POSTALCODE),'0') LIKE NVL(UPPER('%'||:POSTALCODE||'%'),'0')\n" +
            "   AND UPPER(CODE) LIKE UPPER('%'||:PORTALCODE||'%')\n" +
            "   AND NVL(CONTACTNO,'0') LIKE NVL('%'||:MOBILENO||'%','0')\n" +
            "   AND NVL(UPPER(EMAIL),'0') LIKE NVL(UPPER('%'||:EMAIL||'%'),'0')\n" +
            "   AND NVL(UPPER(NINUMBER),'0') LIKE NVL(UPPER('%'||:NINUMBER||'%'),'0')\n" +
            "   AND NVL(UPPER(REGISTERATIONNO),'0') LIKE NVL(UPPER('%'||:REGISTERATIONNO||'%'),'0')" +
            "   AND SOLICITORCODE = :solicitorCode", nativeQuery = true)
    List<Object> getFilterHireCasesListForSolicitor(@Param("NAME") String name,
                                                    @Param("POSTALCODE") String postcode,
                                                    @Param("PORTALCODE") String portalCode,
                                                    @Param("MOBILENO") String mobileNo,
                                                    @Param("EMAIL") String email,
                                                    @Param("NINUMBER") String niNumber,
                                                    @Param("REGISTERATIONNO") String regNo,@Param("solicitorCode") String solicitorCode);

    @Query(value ="SELECT hirecode, created, code, client, taskdue, taskname, status, email, address, contactno, \n" +
            "lastupdated, introducer, lastnotedate, contactdue, landline, description, injurydescription, accdate, acctime\n" +
            "  FROM VIEW_HIRECLAIM\n" +
            " WHERE UPPER(CLIENT) LIKE UPPER('%'||:NAME||'%')\n" +
            "   AND NVL(UPPER(POSTALCODE),'0') LIKE NVL(UPPER('%'||:POSTALCODE||'%'),'0')\n" +
            "   AND UPPER(CODE) LIKE UPPER('%'||:PORTALCODE||'%')\n" +
            "   AND NVL(CONTACTNO,'0') LIKE NVL('%'||:MOBILENO||'%','0')\n" +
            "   AND NVL(UPPER(EMAIL),'0') LIKE NVL(UPPER('%'||:EMAIL||'%'),'0')\n" +
            "   AND NVL(UPPER(NINUMBER),'0') LIKE NVL(UPPER('%'||:NINUMBER||'%'),'0')\n" +
            "   AND NVL(UPPER(REGISTERATIONNO),'0') LIKE NVL(UPPER('%'||:REGISTERATIONNO||'%'),'0')", nativeQuery = true)
    List<Object> getFilterHireCasesList(@Param("NAME") String name,
                                        @Param("POSTALCODE") String postcode,
                                        @Param("PORTALCODE") String portalCode,
                                        @Param("MOBILENO") String mobileNo,
                                        @Param("EMAIL") String email,
                                        @Param("NINUMBER") String niNumber,
                                        @Param("REGISTERATIONNO") String regNo);



    @Modifying
    @Query(value = " UPDATE  TblHireclaim SET submitDate = :submitDate WHERE hirecode = :hirecode")
    int updateSubmitDate(@Param("submitDate") Date submitDate, @Param("hirecode") Long hirecode);


    @Modifying
    @Query(value = " UPDATE  TblHireclaim SET clawbackDate = :clawbackDate WHERE hirecode = :hirecode")
    int updateClawbackdate(@Param("clawbackDate") Date clawbackDate, @Param("hirecode") Long hirecode);

    @Query(value = "SELECT H.ATTRIBUTE_NAME, H.OLD_VALUE, H.NEW_VALUE, H.AUDIT_DATE, U.LOGINID\n" +
            "FROM LAUSER_AUDIT.TBL_HIRECLAIM H\n" +
            "LEFT JOIN TBL_USERS U ON H.USER_ID = U.USERCODE\n" +
            "WHERE H.REFPK = :hirecode\n" +
            "ORDER BY H.AUDIT_ID DESC", nativeQuery = true)
    List<Object> getHireAuditLogs(Long hirecode);


    @Query(value = "SELECT COUNT(*),r.hirenumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.hirecode\n" +
            "                        FROM TBL_HIRECLAIM r\n" +
            "                        WHERE REGISTERATIONNO = :vehicleReg\n" +
            "                        group by r.hirenumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.hirecode", nativeQuery = true)
    List<Object> duplicatesForVehicle(@Param("vehicleReg") String vehicleReg);

    @Query(value = "SELECT COUNT(*),r.hirenumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.hirecode\n" +
            "                        FROM TBL_HIRECLAIM r\n" +
            "                        WHERE MOBILE = :mobile\n" +
            "                        group by r.hirenumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.hirecode", nativeQuery = true)
    List<Object> duplicatesForMobile(@Param("mobile") String mobile);


    @Query(value = "SELECT COUNT(*),r.hirenumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.hirecode\n" +
            "                        FROM TBL_HIRECLAIM r\n" +
            "                        WHERE PARTYCONTACTNO = :mobile\n" +
            "                        group by r.hirenumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.hirecode", nativeQuery = true)
    List<Object> duplicatesForThirPartyMobile(@Param("mobile") String mobile);

    @Query(value = "SELECT COUNT(*),r.hirenumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.hirecode\n" +
            "                        FROM TBL_HIRECLAIM r\n" +
            "                        WHERE NINUMBER = :ninumber\n" +
            "                        group by r.hirenumber,r.firstname || ' ' || r.middlename || ' ' || r.lastname,r.hirecode", nativeQuery = true)
    List<Object> duplicatesForNiNumber(@Param("ninumber") String ninumber);




}
