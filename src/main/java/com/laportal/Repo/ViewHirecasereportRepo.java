package com.laportal.Repo;

import com.laportal.model.ViewHirecasereport;
import com.laportal.model.ViewRtacasereport;
import com.spire.ms.System.Collections.Generic.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ViewHirecasereportRepo extends JpaRepository<ViewHirecasereport,String> {

    @Query(value = "SELECT *\n" +
            "FROM VIEW_HIRECASEREPORT\n" +
            "WHERE CREATEDON BETWEEN TO_DATE(:FROM_DATE ||' 00:00:00','DD-MON-YYYY HH24:MI:SS')\n" +
            "AND TO_DATE(:TO_DATE ||' 23:59:59','DD-MON-YYYY HH24:MI:SS')", nativeQuery = true)
    List<ViewHirecasereport> viewReport(@Param("TO_DATE") String toDate, @Param("FROM_DATE") String fromDate);
}
