package com.laportal.Repo;

import com.laportal.model.TblMnclaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface TblMnclaimRepo extends JpaRepository<TblMnclaim, Long> {

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE\n" +
            "               FROM TBL_mnCLAIM C  \n" +
            "               RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE WHERE S.COMPAIGNCODE = 3  \n" +
            "               GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllDbStatusCounts();

    @Query(value = "select c.mnclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, \n" +
            "       c.Dbcode as code, clientname as client, s.descr status \n" +
            "    from TBL_mnCLAIM c \n" +
            "    left outer  join tbl_rtastatus s on s.statuscode = c.status \n", nativeQuery = true)
    List<Object> getDbCasesListUserWise();

    @Query(value = "select c.mnclaimcode, to_char(c.createdate, 'dd-mon-yyyy') as created, c.Dbcode as code, CLAIMANT_NAME as client, s.descr status \n" +
            "            from TBL_mnCLAIM c \n" +
            "            left outer  join tbl_rtastatus s on s.statuscode = c.status \n" +
            "            where c.status = :statusId", nativeQuery = true)
    List<Object> getDbCasesListStatusWise(@Param("statusId") long statusId);

    @Modifying
    @Query(value = " UPDATE  TblMnclaim SET status = :statusCode ,lastupdateuser = :lastupdateuser WHERE mnclaimcode = :mnclaimcode")
    int performAction(@Param("statusCode") BigDecimal statusCode, @Param("mnclaimcode") Long mnclaimcode, @Param("lastupdateuser") String lastupdateuser);

    @Modifying
    @Query(value = " UPDATE  TblMnclaim SET remarks = :reason, status = :statusCode ,lastupdateuser = :lastupdateuser WHERE mnclaimcode = :mnclaimcode")
    int performRejectCancelAction(@Param("reason") String reason, @Param("statusCode") String toStatus, @Param("mnclaimcode") Long mnclaimcode, @Param("lastupdateuser") String lastupdateuser);

    @Query(value = "SELECT mnclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, CREATEDBY\n" +
            "FROM VIEW_MnCLAIMINTRODUCERS\n" +
            "WHERE CREATEDBY = :userId", nativeQuery = true)
    List<Object> getDbCasesListForIntroducers(@Param("userId") String usercode);

    @Query(value = "SELECT mnclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate, SOLICITORCODE\n" +
            "FROM VIEW_mnCLAIMSOLICITORS\n" +
            "WHERE SOLICITORCODE = :userId", nativeQuery = true)
    List<Object> getDbCasesListForSolicitor(@Param("userId") String usercode);

    @Query(value = "SELECT mnclaimcode, created, code, client, taskdue, taskname, status, email, address, contactno, lastupdated, introducer, lastnotedate\n" +
            "FROM VIEW_mnCLAIM", nativeQuery = true)
    List<Object> getDbCasesList();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_mnCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "INNER JOIN TBL_USERS U ON U.USERCODE = C.CREATEUSER\n" +
            "inner join tbl_mnsolicitor hs on c.mnclaimcode = hs.mnclaimcode and hs.status = 'Y'\n" +
            "inner join tbl_users uu on uu.usercode = hs.usercode\n" +
            "inner join tbl_companyprofile cp on cp.companycode = hs.companycode\n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllDbStatusCountsForSolicitors();

    @Query(value = "SELECT NVL(COUNT(C.STATUS),0) STATUSCOUNT, S.DESCR, S.STATUSCODE  \n" +
            "FROM TBL_mnCLAIM C  \n" +
            "RIGHT OUTER JOIN TBL_RTASTATUS S ON C.STATUS = S.STATUSCODE \n" +
            "WHERE S.COMPAIGNCODE = 3  \n" +
            "GROUP BY S.DESCR, S.STATUSCODE", nativeQuery = true)
    List<Object> getAllDbStatusCountsForIntroducers();

    @Modifying
    @Query(value = " UPDATE  TblMnclaim SET clawbackDate = :clawbackDate WHERE mnclaimcode = :mnclaimcode")
    int updateClawbackdate(@Param("clawbackDate") Date clawbackDate, @Param("mnclaimcode") Long mnclaimcode);

    @Modifying
    @Query(value = " UPDATE  TblMnclaim SET submitDate = :submitDate WHERE mnclaimcode = :mnclaimcode")
    int updateSubmitDate(@Param("submitDate") Date submitDate, @Param("mnclaimcode") Long mnclaimcode);

    @Query(value = "SELECT E.ATTRIBUTE_NAME, E.OLD_VALUE, E.NEW_VALUE, E.AUDIT_DATE, U.LOGINID\n" +
            "  FROM LAUSER_AUDIT.TBL_MNCLAIM E\n" +
            "  LEFT JOIN TBL_USERS U ON E.USER_ID = U.USERCODE\n" +
            " WHERE E.REFPK = :mncode \n" +
            " ORDER BY E.AUDIT_ID DESC", nativeQuery = true)
    List<Object> getMnAuditLogs(@Param("mncode")Long mncode);
}
