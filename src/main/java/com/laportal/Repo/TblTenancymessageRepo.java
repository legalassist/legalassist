package com.laportal.Repo;

import com.laportal.model.TblTenancymessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblTenancymessageRepo extends JpaRepository<TblTenancymessage, Long> {
    List<TblTenancymessage> findByTblTenancyclaimTenancyclaimcode(Long valueOf);
}
