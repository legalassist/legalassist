package com.laportal.Repo;

import com.laportal.model.TblCompanyprofile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TblCompanyprofileRepo extends JpaRepository<TblCompanyprofile, String> {

    @Query(value = "SELECT SEQ_TBL_COMPANYPROFILE.nextval from dual", nativeQuery = true)
    int getSeq();

    @Modifying
    @Query("update TblCompanyprofile c set c.accountno = :accountno," + "c.addressline1 = :addressline1,"
            + "c.addressline2= :addressline2," + "c.city= :city," + "c.companyregno= :companyregno,"
            + "c.companystatus= :companystatus," + "c.contactperson= :contactperson," + "c.email= :email,"
            + "c.name= :name," + "c.phone= :phone,c.phone2 = :phone2," + "c.postcode= :postcode," + "c.region= :region,"
            + "c.varegno= :varegno," + "c.website= :website," + "c.remarks = :remarks, c.directintroducer = :directintroducer ," +
            " c.billtoemail = :billtoemail ,c.billtoname = :billtoname,c.accountemail = :accountemail, c.secondaryaccountemail = :secondryaccountemail ,c.vat = :vat," +
            " c.jurisdiction = :jurisdiction"
            + "  where c.companycode = :companycode")
    int updateCompanyProfile(@Param("accountno") String accountno, @Param("addressline1") String addressline1,
                             @Param("addressline2") String addressline2, @Param("city") String city,
                             @Param("companyregno") String companyregno, @Param("companystatus") String companystatus,
                             @Param("contactperson") String contactperson, @Param("email") String email, @Param("name") String name,
                             @Param("phone") String phone, @Param("phone2") String phone2, @Param("postcode") String postcode, @Param("region") String region,
                             @Param("remarks") String remarks, @Param("varegno") String varegno, @Param("website") String website,
                             @Param("directintroducer") String directintroducer, @Param("billtoemail") String billtoemail, @Param("billtoname") String billtoname,
                             @Param("accountemail") String accountemail, @Param("secondryaccountemail") String secondryaccountemail, @Param("vat") String vat,
                             @Param("jurisdiction") String jurisdiction, @Param("companycode") String companycode);

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B WHERE A.COMPANYCODE = B.COMPANYCODE AND B.COMPAIGNCODE = 1 AND B.STATUS = 'Y' AND a.categorycode = 2 ", nativeQuery = true)
    List<TblCompanyprofile> getAllRtaCompanyProfile();

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B WHERE A.COMPANYCODE = B.COMPANYCODE AND B.STATUS = 'Y' AND a.categorycode = 1 ", nativeQuery = true)
    List<TblCompanyprofile> getAllIntroducerCompanyProfile();

    @Query(value = "select c.* from tbl_companyprofile c   inner join tbl_rtasolicitor s on s.companycode = c.companycode and s.status = 'Y'   where s.rtacode = ?", nativeQuery = true)
    TblCompanyprofile getCompanyProfileAgainstRtaCode(@Param("rtacode") long rtacode);

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B WHERE A.COMPANYCODE = B.COMPANYCODE AND B.COMPAIGNCODE = 2 AND B.STATUS = 'Y' AND a.categorycode = 2 ", nativeQuery = true)
    List<TblCompanyprofile> getAllHireCompanyProfile();

    @Query(value = "select c.* from tbl_companyprofile c   inner join tbl_hdrsolicitor s on s.companycode = c.companycode and s.status = 'Y'   where s.hdrclaimcode = ?", nativeQuery = true)
    TblCompanyprofile getCompanyProfileAgainstHireCode(@Param("hdrclaimcode") long hdrclaimcode);

    @Query(value = "select c.* from tbl_companyprofile c   inner join tbl_tenancysolicitor s on s.companycode = c.companycode and s.status = 'Y'   where s.tenancyclaimcode = ?", nativeQuery = true)
    TblCompanyprofile getCompanyProfileAgainstTenancyCode(@Param("tenancyclaimcode") long tenancyclaimcode);

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B WHERE A.COMPANYCODE = B.COMPANYCODE AND B.COMPAIGNCODE = 3 AND B.STATUS = 'Y' AND a.categorycode = 2 ", nativeQuery = true)
    List<TblCompanyprofile> getAllHdrCompanyProfile();

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B WHERE A.COMPANYCODE = B.COMPANYCODE AND B.COMPAIGNCODE = 3 AND B.STATUS = 'Y' AND a.categorycode = 2 ", nativeQuery = true)
    List<TblCompanyprofile> getAllPcpCompanyProfile();

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B WHERE A.COMPANYCODE = B.COMPANYCODE AND B.COMPAIGNCODE = 3 AND B.STATUS = 'Y' AND a.categorycode = 2 ", nativeQuery = true)
    List<TblCompanyprofile> getAllTenancyCompanyProfile();

    @Query(value = "select c.* from tbl_companyprofile c   inner join tbl_pcpsolicitor s on s.companycode = c.companycode and s.status = 'Y'   where s.pcpclaimcode = ?", nativeQuery = true)
    TblCompanyprofile getCompanyProfileAgainstPcpCode(@Param("pcpclaimcode") long pcpclaimcode);

    @Query(value = "select c.* from tbl_companyprofile c   inner join tbl_dbsolicitor s on s.companycode = c.companycode and s.status = 'Y'   where s.dbclaimcode = ?", nativeQuery = true)
    TblCompanyprofile getCompanyProfileAgainstDbCode(@Param("dbclaimcode") long dbclaimcode);

    @Query(value = "select c.* from tbl_companyprofile c   inner join TBL_MNSOLICITOR s on s.companycode = c.companycode and s.status = 'Y'   where s.dbclaimcode = ?", nativeQuery = true)
    TblCompanyprofile getCompanyProfileAgainstMnCode(@Param("mnCode") long mnCode);

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B WHERE A.COMPANYCODE = B.COMPANYCODE AND B.COMPAIGNCODE = 3 AND B.STATUS = 'Y' AND a.categorycode = 2 ", nativeQuery = true)
    List<TblCompanyprofile> getAllDbCompanyProfile();

    @Query(value = "select c.* from tbl_companyprofile c   inner join tbl_plsolicitor s on s.companycode = c.companycode and s.status = 'Y'   where s.plclaimcode = ?", nativeQuery = true)
    TblCompanyprofile getCompanyProfileAgainstPlCode(@Param("plclaimcode") long plclaimcode);

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B WHERE A.COMPANYCODE = B.COMPANYCODE AND B.COMPAIGNCODE = 9 AND B.STATUS = 'Y' AND a.categorycode = 2 ", nativeQuery = true)
    List<TblCompanyprofile> getAllElCompanyProfile();

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B WHERE A.COMPANYCODE = B.COMPANYCODE AND B.COMPAIGNCODE = 8 AND B.STATUS = 'Y' AND a.categorycode = 2 ", nativeQuery = true)
    List<TblCompanyprofile> getAllPlCompanyProfile();

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B WHERE A.COMPANYCODE = B.COMPANYCODE AND B.COMPAIGNCODE = 7 AND B.STATUS = 'Y' AND a.categorycode = 2 ", nativeQuery = true)
    List<TblCompanyprofile> getAllOlCompanyProfile();

    @Query(value = "select c.* from tbl_companyprofile c   inner join tbl_olsolicitor s on s.companycode = c.companycode and s.status = 'Y'   where s.olclaimcode = ?", nativeQuery = true)
    TblCompanyprofile getCompanyProfileAgainstOlCode(@Param("olclaimcode") long olclaimcode);

    @Query(value = "select c.* from tbl_companyprofile c   inner join tbl_elsolicitor s on s.companycode = c.companycode and s.status = 'Y'   where s.elclaimcode = ?", nativeQuery = true)
    TblCompanyprofile getCompanyProfileAgainstElCode(@Param("elclaimcode") long elclaimcode);

    List<TblCompanyprofile> findAllByOrderByNameAsc();

    @Query(value = "SELECT DISTINCT A.* FROM TBL_COMPANYPROFILE A , TBL_COMPANYJOBS B " +
            "WHERE A.COMPANYCODE = B.COMPANYCODE " +
            "AND B.COMPAIGNCODE = 1 " +
            "AND B.STATUS = 'Y' " +
            "AND a.categorycode = 2 " +
            "AND A.jurisdiction = :jurisdiction " +
            "--AND A.airbag_case = :airbagCase ", nativeQuery = true)
    List<TblCompanyprofile> getAllRtaCompanyProfileByJurisdictionAndAirBagCase(@Param("jurisdiction") String jurisdiction);
//    List<TblCompanyprofile> getAllRtaCompanyProfileByJurisdictionAndAirBagCase(@Param("jurisdiction") String jurisdiction, @Param("airbagCase") String airbagCase);


    @Query(value = "select c.* from TBL_COMPANYPROFILE c, tbl_users u\n" +
            "             where c.companycode = u.companycode\n" +
            "             and u.usercode = :usercode", nativeQuery = true)
    TblCompanyprofile getCompanyProfileByUser(@Param("usercode") String usercode);
}
