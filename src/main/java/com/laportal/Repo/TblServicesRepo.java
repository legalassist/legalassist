package com.laportal.Repo;

import com.laportal.model.TblService;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TblServicesRepo extends JpaRepository<TblService, String> {
}
