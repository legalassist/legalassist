package com.laportal.Repo;

import com.laportal.model.TblEmailTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;

public interface TblEmailTemplateRepo extends JpaRepository<TblEmailTemplate, Long> {
    TblEmailTemplate findByUserccategorycode(BigDecimal userCategoryCode);

    TblEmailTemplate findByEmailtype(String emailType);

    TblEmailTemplate findByEmailtypeAndUserccategorycode(String emailType, BigDecimal userCategoryCode);
}
