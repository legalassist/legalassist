package com.laportal.Repo;

import com.laportal.model.TblCircumstance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblCircumstanceRepo extends JpaRepository<TblCircumstance, Long> {

    List<TblCircumstance> findByStatus(String status);
}
