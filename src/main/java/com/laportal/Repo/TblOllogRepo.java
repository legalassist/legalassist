package com.laportal.Repo;

import com.laportal.model.TblOllog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TblOllogRepo extends JpaRepository<TblOllog, Long> {
    List<TblOllog> findByTblOlclaimOlclaimcode(Long DbClaimCode);
}
