package com.laportal.Repo;

import com.laportal.model.TblManualinvoicing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TblManualinvoicingRepo extends JpaRepository<TblManualinvoicing, Long> {

    @Query(value = " Select SEQ_MANUALINVOICENUMBER.nextval from dual ", nativeQuery = true)
    int getmanualInvoiceNumber();
}
