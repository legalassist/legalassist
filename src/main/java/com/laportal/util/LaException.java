package com.laportal.util;

/**
 * Class is used to Handle Bbs Exceptions.
 *
 * @version 1.0
 * @see RuntimeException
 * @since 29 january 2020
 */
public class LaException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String errorMessage;

    /**
     * Getter Method of errorMessage
     *
     * @see LaException
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Setter Method of errorMessage
     *
     * @param errorMessage receives error message
     * @see LaException
     */
    public LaException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    /**
     * No Argument Constructor of Class
     *
     * @see LaException
     */
    public LaException() {
        super();
    }
}
