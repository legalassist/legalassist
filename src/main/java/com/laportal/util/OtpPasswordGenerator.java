package com.laportal.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OtpPasswordGenerator {

    public String generateOTP() {
        String otp = "";
        List<Integer> numbers = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            numbers.add(i);
        }

        Collections.shuffle(numbers);
        for (int i = 0; i < 6; i++) {
            otp += numbers.get(i).toString();
        }

        return otp;
    }

    public String generatePassword() {
        String password = "";
        List<Integer> numbers = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            numbers.add(i);
        }

        Collections.shuffle(numbers);
        for (int i = 0; i < 6; i++) {
            password += numbers.get(i).toString();
        }

        return password;
    }
}
