package com.laportal.util;

public class ErrorResponse {
    private int errorCode;
    private String message;
    private String detail;

    /**
     * No Argument Constructor of Class
     *
     * @see ErrorResponse
     */
    public ErrorResponse() {
    }

    /**
     * 3 Argument Constructor of Class
     *
     * @param errorCode
     * @param message
     * @param detail
     * @see ErrorResponse
     */
    public ErrorResponse(final int errorCode, final String message, final String detail) {
        this.errorCode = errorCode;
        this.message = message;
        this.detail = detail;
    }

    /**
     * Getter Method of errorCode
     *
     * @see ErrorResponse
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * Setter Method of errorCode
     *
     * @param errorCode receives errorCode
     * @see ErrorResponse
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Getter Method of message
     *
     * @see ErrorResponse
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setter Method of message
     *
     * @param message receives message
     * @see ErrorResponse
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Getter Method of detail
     *
     * @see ErrorResponse
     */
    public String getDetail() {
        return detail;
    }

    /**
     * Setter Method of detail
     *
     * @param detail receives detail
     * @see ErrorResponse
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

}