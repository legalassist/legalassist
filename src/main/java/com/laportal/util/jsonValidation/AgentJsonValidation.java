package com.laportal.util.jsonValidation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AgentJsonValidation extends AbstractJsonValidation {

    public HashMap<String, HashMap<String, Object>> loginJsonValidate(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);
        String userName = null;
        String password = null;
        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        if (jObject.has("username")) {
            userName = jObject.getString("username") == null ? "" : jObject.getString("username");
            if (userName.isEmpty()) {
                error.put("username", "User Name is Empty");
            } else {
                data.put("username", userName);
            }
        } else {
            error.put("username", "User Name is Invalid");
        }

        if (jObject.has("password")) {
            password = jObject.getString("password") == null ? "" : jObject.getString("password");
            if (password.isEmpty()) {
                error.put("password", "Password is Empty");
            } else {
                data.put("password", password);
            }
        } else {
            error.put("password", "Password is Invalid");
        }

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> createAgentJsonValidate(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "address", error, data);
        validateJsonObjectWithError(jObject, "cnic", error, data);
        validateJsonObjectWithError(jObject, "cnicApplicant", error, data);
        validateJsonObjectWithError(jObject, "fatherHusbandName", error, data);
        validateJsonObjectWithError(jObject, "mobile", error, data);
        validateJsonObjectWithError(jObject, "mobileApplicant", error, data);
        validateJsonObjectWithError(jObject, "name", error, data);
        validateJsonObjectWithError(jObject, "nameOfApplicant", error, data);
        validateJsonObjectWithoutRequired(jObject, "nok", error, data);
        validateJsonObjectWithError(jObject, "occupation", error, data);
        validateJsonObjectWithError(jObject, "phone", error, data);
        validateJsonObjectWithError(jObject, "phoneApplicant", error, data);
        validateJsonObjectWithoutRequired(jObject, "relevantGpo", error, data);
        validateJsonObjectWithError(jObject, "residentialAddress", error, data);
        validateJsonObjectWithError(jObject, "status", error, data);
        validateJsonObjectWithoutRequired(jObject, "tehsil", error, data);
        validateJsonObjectWithoutRequired(jObject, "unionCouncil", error, data);
        validateJsonObjectWithError(jObject, "email", error, data);
        validateJsonObjectWithError(jObject, "lkpCity", error, data);
        validateJsonObjectWithError(jObject, "lkpDistrict", error, data);
        validateJsonObjectWithError(jObject, "lkpProvince", error, data);
        validateJsonObjectWithError(jObject, "roleId", error, data);
        validateJsonObjectWithoutRequired(jObject, "parentAgent", error, data);
        validateJsonObjectWithoutRequired(jObject, "lkpPostOffice", error, data);
        validateJsonObjectWithError(jObject, "lkpGpo", error, data);
        validateJsonObjectWithError(jObject, "lkpCircle", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;

    }

    public HashMap<String, HashMap<String, Object>> verifyAgentMpinJsonValidate(String json) throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "mpin", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> changeLoggedAgentPasswordJsonValidate(String json)
            throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "oldPass", error, data);
        validateJsonObjectWithError(jObject, "newPass", error, data);
        validateJsonObjectWithError(jObject, "confirmPass", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;

    }

    public HashMap<String, HashMap<String, Object>> changeAgentPasswordJsonValidate(String json) throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "agentName", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;

    }

    public HashMap<String, HashMap<String, Object>> changeLoggedAgentMpinJsonValidate(String json)
            throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "oldMpin", error, data);
        validateJsonObjectWithError(jObject, "newMpin", error, data);
        validateJsonObjectWithError(jObject, "confirmMpin", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;

    }

    public HashMap<String, HashMap<String, Object>> changeAgentMpinJsonValidate(String json) throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "agentName", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;

    }

    public HashMap<String, HashMap<String, Object>> changeAgentStatusPasswordJsonValidate(String json)
            throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "agentName", error, data);
        validateJsonObjectWithError(jObject, "status", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> titleFetchJsonValidate(String json) throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "accountNo", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> updateAgentJsonValidate(String json) throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "nok", error, data);
        validateJsonObjectWithError(jObject, "userName", error, data);
        validateJsonObjectWithError(jObject, "occupation", error, data);
        validateJsonObjectWithError(jObject, "phone", error, data);
        validateJsonObjectWithError(jObject, "phoneApplicant", error, data);
        validateJsonObjectWithoutRequired(jObject, "relevantGpo", error, data);
        validateJsonObjectWithError(jObject, "residentialAddress", error, data);
        validateJsonObjectWithoutRequired(jObject, "lkpPostOffice", error, data);
        validateJsonObjectWithError(jObject, "lkpGpo", error, data);
        validateJsonObjectWithError(jObject, "lkpCircle", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> createSubAgentJsonValidate(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "address", error, data);
        validateJsonObjectWithError(jObject, "cnic", error, data);
        validateJsonObjectWithError(jObject, "motherName", error, data);
        validateJsonObjectWithError(jObject, "mobile", error, data);
        validateJsonObjectWithError(jObject, "name", error, data);
        validateJsonObjectWithError(jObject, "status", error, data);
        validateJsonObjectWithError(jObject, "pob", error, data);
        validateJsonObjectWithError(jObject, "cnicExpiraydate", error, data);

        validateJsonObjectWithError(jObject, "transactionLimit", error, data);
        validateJsonObjectWithoutRequired(jObject, "maxAmtPerTxn", error, data);
        validateJsonObjectWithError(jObject, "agentType", error, data);

        validateJsonObjectWithoutRequired(jObject, "monthlyAmtLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "monthlyAmtLimitDr", error, data);
        validateJsonObjectWithoutRequired(jObject, "monthlyTransLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "monthlyTransLimitDr", error, data);

        validateJsonObjectWithoutRequired(jObject, "dailyAmtLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "dailyAmtLimitDr", error, data);
        validateJsonObjectWithoutRequired(jObject, "dailyTransLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "dailyTransLimitDr", error, data);

        validateJsonObjectWithoutRequired(jObject, "yearlyAmtLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "yearlyAmtLimitDr", error, data);
        validateJsonObjectWithoutRequired(jObject, "yearlyTransLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "yearlyTransLimitDr", error, data);

        validateJsonObjectWithError(jObject, "lkpCircle", error, data);
        validateJsonObjectWithError(jObject, "lkpGpo", error, data);
        validateJsonObjectWithoutRequired(jObject, "lkpPostOffice", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> InitiateOtcReceiveJsonValidate(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "cnic", error, data);
        validateJsonObjectWithError(jObject, "transRefNum", error, data);
        validateJsonObjectWithError(jObject, "otp", error, data);
        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> transactionAgentOtcReceiveMoneyJsonValidate(String json)
            throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithoutRequired(jObject, "comments", error, data);
        validateJsonObjectWithError(jObject, "transHeadId", error, data);
        validateJsonObjectWithError(jObject, "transactionAmount", error, data);
        validateJsonObjectWithError(jObject, "senderContact", error, data);
        validateJsonObjectWithError(jObject, "receiverContact", error, data);
        validateJsonObjectWithError(jObject, "bvs", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> initiateAgentOtcSendMoneyCashJsonValidate(String json)
            throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "transactionAmount", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> initiateAgentOtcSendMoneyJsonValidate(String json)
            throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithoutRequired(jObject, "senderCnic", error, data);
        validateJsonObjectWithoutRequired(jObject, "receiverCnic", error, data);
        validateJsonObjectWithoutRequired(jObject, "senderContact", error, data);
        validateJsonObjectWithoutRequired(jObject, "receiverContact", error, data);
        validateJsonObjectWithError(jObject, "transactionAmount", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> transactionAgentOtcSendMoneyJsonValidate(String json)
            throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "senderCnic", error, data);
        validateJsonObjectWithError(jObject, "senderContact", error, data);
        validateJsonObjectWithoutRequired(jObject, "senderName", error, data);
        validateJsonObjectWithError(jObject, "receiverCnic", error, data);
        validateJsonObjectWithError(jObject, "receiverContact", error, data);
        validateJsonObjectWithoutRequired(jObject, "receiverName", error, data);
        validateJsonObjectWithError(jObject, "transactionAmount", error, data);
        validateJsonObjectWithError(jObject, "bvs", error, data);
        validateJsonObjectWithoutRequired(jObject, "comments", error, data);
        validateJsonObjectWithError(jObject, "fee", error, data);
        validateJsonObjectWithError(jObject, "fed", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> editOtcSendMoneyJsonValidate(String json) throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "transHeadId", error, data);
        validateJsonObjectWithError(jObject, "senderCnic", error, data);
        validateJsonObjectWithError(jObject, "senderMobileNo", error, data);
        validateJsonObjectWithError(jObject, "senderName", error, data);
        validateJsonObjectWithError(jObject, "receiverCnic", error, data);
        validateJsonObjectWithError(jObject, "receiverMobile", error, data);
        validateJsonObjectWithError(jObject, "receiverName", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> otcTransSearchMoneyJsonValidate(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithoutRequired(jObject, "transRefNum", error, data);
        validateJsonObjectWithoutRequired(jObject, "senderCnic", error, data);
        validateJsonObjectWithoutRequired(jObject, "senderMobNo", error, data);
        validateJsonObjectWithoutRequired(jObject, "receiverCnic", error, data);
        validateJsonObjectWithoutRequired(jObject, "receiverMobNo", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;

    }

    public HashMap<String, HashMap<String, Object>> transactionStatusJsonValidate(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "transHeadId", error, data);
        validateJsonObjectWithError(jObject, "status", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> transactionReversalJsonValidate(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "senderName", error, data);
        validateJsonObjectWithError(jObject, "senderCnic", error, data);
        validateJsonObjectWithError(jObject, "senderContact", error, data);
        validateJsonObjectWithError(jObject, "receiverName", error, data);
        validateJsonObjectWithError(jObject, "receiverCnic", error, data);
        validateJsonObjectWithError(jObject, "receiverContact", error, data);
        validateJsonObjectWithError(jObject, "fee", error, data);
        validateJsonObjectWithError(jObject, "fed", error, data);
        validateJsonObjectWithError(jObject, "feeReversal", error, data);
        validateJsonObjectWithError(jObject, "transRefNum", error, data);
        validateJsonObjectWithError(jObject, "transAmount", error, data);
        validateJsonObjectWithError(jObject, "transDate", error, data);
        validateJsonObjectWithError(jObject, "transHeadId", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> initiateClientAgentOtcSendMoneyJsonValidate(String json)
            throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "senderCnic", error, data);
        validateJsonObjectWithError(jObject, "receiverCnic", error, data);
        validateJsonObjectWithError(jObject, "senderContact", error, data);
        validateJsonObjectWithError(jObject, "receiverContact", error, data);
        validateJsonObjectWithError(jObject, "transactionAmount", error, data);
        validateJsonObjectWithError(jObject, "clientId", error, data);
        validateJsonObjectWithError(jObject, "clientSecret", error, data);
        validateJsonObjectWithError(jObject, "channelId", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> transactionClientAgentOtcSendMoneyJsonValidate(String json)
            throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "senderCnic", error, data);
        validateJsonObjectWithError(jObject, "senderContact", error, data);
        validateJsonObjectWithoutRequired(jObject, "senderName", error, data);
        validateJsonObjectWithError(jObject, "receiverCnic", error, data);
        validateJsonObjectWithError(jObject, "receiverContact", error, data);
        validateJsonObjectWithoutRequired(jObject, "receiverName", error, data);
        validateJsonObjectWithError(jObject, "transactionAmount", error, data);
        validateJsonObjectWithError(jObject, "bvs", error, data);
        validateJsonObjectWithoutRequired(jObject, "comments", error, data);
        validateJsonObjectWithError(jObject, "fee", error, data);
        validateJsonObjectWithError(jObject, "fed", error, data);
        validateJsonObjectWithError(jObject, "clientId", error, data);
        validateJsonObjectWithError(jObject, "clientSecret", error, data);
        validateJsonObjectWithError(jObject, "channelId", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> InitiateClientOtcReceiveJsonValidate(String json)
            throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "cnic", error, data);
        validateJsonObjectWithError(jObject, "transRefNum", error, data);
        validateJsonObjectWithError(jObject, "otp", error, data);
        validateJsonObjectWithError(jObject, "clientId", error, data);
        validateJsonObjectWithError(jObject, "clientSecret", error, data);
        validateJsonObjectWithError(jObject, "channelId", error, data);
        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> transactionClientAgentOtcReceiveMoneyJsonValidate(String json)
            throws JSONException {

        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithoutRequired(jObject, "comments", error, data);
        validateJsonObjectWithError(jObject, "transHeadId", error, data);
        validateJsonObjectWithError(jObject, "transactionAmount", error, data);
        validateJsonObjectWithError(jObject, "senderContact", error, data);
        validateJsonObjectWithError(jObject, "receiverContact", error, data);
        validateJsonObjectWithError(jObject, "bvs", error, data);
        validateJsonObjectWithError(jObject, "clientId", error, data);
        validateJsonObjectWithError(jObject, "clientSecret", error, data);
        validateJsonObjectWithError(jObject, "channelId", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> updateSubAgentJsonValidate(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "address", error, data);
        validateJsonObjectWithError(jObject, "userName", error, data);
        validateJsonObjectWithError(jObject, "mobile", error, data);
        validateJsonObjectWithError(jObject, "cnic", error, data);
        validateJsonObjectWithError(jObject, "status", error, data);

        validateJsonObjectWithError(jObject, "transactionLimit", error, data);
        validateJsonObjectWithoutRequired(jObject, "maxAmtPerTxn", error, data);

        validateJsonObjectWithoutRequired(jObject, "monthlyAmtLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "monthlyAmtLimitDr", error, data);
        validateJsonObjectWithoutRequired(jObject, "monthlyTransLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "monthlyTransLimitDr", error, data);

        validateJsonObjectWithoutRequired(jObject, "dailyAmtLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "dailyAmtLimitDr", error, data);
        validateJsonObjectWithoutRequired(jObject, "dailyTransLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "dailyTransLimitDr", error, data);

        validateJsonObjectWithoutRequired(jObject, "yearlyAmtLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "yearlyAmtLimitDr", error, data);
        validateJsonObjectWithoutRequired(jObject, "yearlyTransLimitCr", error, data);
        validateJsonObjectWithoutRequired(jObject, "yearlyTransLimitDr", error, data);

        validateJsonObjectWithoutRequired(jObject, "lkpPostOffice", error, data);
        validateJsonObjectWithError(jObject, "lkpGpo", error, data);
        validateJsonObjectWithError(jObject, "lkpCircle", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;
    }

    public HashMap<String, HashMap<String, Object>> resendSmsJsonValidate(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "transHeadId", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;

    }

    public HashMap<String, HashMap<String, Object>> resetPasscodeJsonValidate(String json) throws JSONException {
        JSONObject jObject = new JSONObject(json);

        HashMap<String, HashMap<String, Object>> response = new HashMap<>();
        HashMap<String, Object> error = new HashMap<>();
        HashMap<String, Object> data = new HashMap<>();

        validateJsonObjectWithError(jObject, "transHeadId", error, data);
        validateJsonObjectWithError(jObject, "senderContact", error, data);

        response.put("data", data);
        response.put("error", error);
        return response;

    }

}
