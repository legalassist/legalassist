package com.laportal.util.jsonValidation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AbstractJsonValidation {

    public void validateJsonObjectWithoutRequired(JSONObject jObject, String key, HashMap<String, Object> error,
                                                  HashMap<String, Object> data) throws JSONException {
        String param = "";
        if (jObject.has(key)) {
            param = jObject.getString(key) == null ? "" : jObject.getString(key);
            data.put(key, param);
        } else {
            error.put(key, key.toUpperCase() + " is Invalid");
        }
    }

    public void validateJsonObjectWithError(JSONObject jObject, String key, HashMap<String, Object> error,
                                            HashMap<String, Object> data) throws JSONException {
        String param = "";
        if (jObject.has(key)) {
            param = jObject.getString(key) == null ? "" : jObject.getString(key);
            if (param.isEmpty()) {
                error.put(key, key.toUpperCase() + " is Empty");
            } else {
                data.put(key, param);
            }
        } else {
            error.put(key, key.toUpperCase() + " is Invalid");
        }
    }

    public void validateJsonArrayObjectWithError(JSONObject jObject, String arrayKey, String key,
                                                 HashMap<String, Object> error, HashMap<String, Object> data) throws JSONException {
        List<String> list = new ArrayList<String>();
        JSONArray jsonArray = new JSONArray();
        if (jObject.has(arrayKey)) {
            jsonArray = jObject.getJSONArray(arrayKey) == null ? null : jObject.getJSONArray(arrayKey);
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        list.add(jsonArray.getJSONObject(i).getString(key) == null ? ""
                                : jsonArray.getJSONObject(i).getString(key));
                    }
                    data.put(key, list);
                } else {
                    error.put(key, key.toUpperCase() + " is Empty");
                }
            } else {
                error.put(key, key.toUpperCase() + " is Empty");
            }
        } else {
            error.put(key, key.toUpperCase() + " is Invalid");
        }
    }

    public void validateJsonArrayObjectWithoutRequired(JSONObject jObject, String arrayKey, String key,
                                                       HashMap<String, Object> error, HashMap<String, Object> data) throws JSONException {
        List<String> list = new ArrayList<String>();
        JSONArray jsonArray = new JSONArray();
        if (jObject.has(arrayKey)) {
            jsonArray = jObject.getJSONArray(arrayKey) == null ? null : jObject.getJSONArray(arrayKey);
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        list.add(jsonArray.getJSONObject(i).getString(key) == null ? ""
                                : jsonArray.getJSONObject(i).getString(key));
                    }
                    data.put(key, list);
                }
            }
        } else {
            error.put(key, key.toUpperCase() + " is Invalid");
        }
    }

    public void validateJsonArrayToArrayWithError(JSONObject jObject, String arrayKey, List<String> keys,
                                                  HashMap<String, Object> error, HashMap<String, Object> data) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        List<HashMap<String, Object>> arrobj = new ArrayList<>();
        HashMap<String, Object> singleobj = null;

        if (jObject.has(arrayKey)) {
            jsonArray = jObject.getJSONArray(arrayKey) == null ? null : jObject.getJSONArray(arrayKey);
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        singleobj = new HashMap<>();
                        for (String key : keys) {
                            singleobj.put(key, jsonArray.getJSONObject(i).getString(key) == null ? ""
                                    : jsonArray.getJSONObject(i).getString(key));
                        }
                        arrobj.add(singleobj);
                    }
                    data.put(arrayKey, arrobj);

                } else {
                    error.put(arrayKey, arrayKey.toUpperCase() + " is Empty");
                }
            } else {
                error.put(arrayKey, arrayKey.toUpperCase() + " is Empty");
            }
        } else {
            error.put(arrayKey, arrayKey.toUpperCase() + " is Invalid");
        }
    }

}
