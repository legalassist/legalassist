package com.laportal.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;

@Component
@Order(1)
public class LaFilter extends GenericFilterBean implements Filter {
    Logger LOG = LoggerFactory.getLogger(LaFilter.class);

    @SuppressWarnings("unused")
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        httpResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpResponse.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        httpResponse.setHeader("Access-Control-Allow-Headers", "*");
        httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
        httpResponse.setHeader("Access-Control-Max-Age", "3600");

        if (httpRequest.getMethod().equalsIgnoreCase("OPTIONS")) {
            chain.doFilter(request, response);
            return;
        }

        Enumeration<String> s = httpRequest.getHeaderNames();

        final String headerValue = httpRequest.getHeader("Authorization");
        final String contextPath = httpRequest.getRequestURL().toString();
        if(contextPath.contains("getESignFields") || contextPath.contains("addESign")){
            chain.doFilter(request, response);
        } else if (contextPath.contains("refreshToken")){
            chain.doFilter(request, response);
        } else if (!contextPath.contains("la/login")) {
            if (contextPath.contains("la")) {


                if (headerValue != null && headerValue.startsWith("Bearer")) {
                    String jwt = headerValue.substring(7);
                    JWTSecurity jwtSecurity = new JWTSecurity();
                    HashMap<String, Object> data = jwtSecurity.parseJWT(jwt);
                    if (data != null) {
                        if (data.get("Expired").toString().contains("N")) {

                            String id = data.get("ID").toString();
                            String issue = data.get("Issuer").toString();
                            if (id.equalsIgnoreCase(getEnvironment().getProperty("jwtSecurityId")) && issue.equalsIgnoreCase(getEnvironment().getProperty("jwtSecurityIssuer"))) {
                                chain.doFilter(request, response);
                            } else {
                                throw new LaException("SESSION AUTHORIZATION FAILED");
                            }

                        } else if (data.get("Expired").toString().contains("Y")) {
                            throw new LaException("SESSION AUTHORIZATION FAILED");
                        }
                    } else {
                        LOG.info("\n INTERCEPTOR CLASS \n CUSTOMER NOT AUTHORIZED THROUGH JWT \n\n");
                        throw new LaException("SESSION AUTHORIZATION FAILED");
                    }

                } else {
                    throw new LaException("Invalid Authorization header value.");

                }
            }else {
                throw new LaException("Invalid Authorization header value.");

            }

        }else {
            chain.doFilter(request, response);
        }
    }

}