package com.laportal.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Component
public class JWTSecurity {




    private SecretKeySpec secretKey;

    private byte[] key;


    private void setKey() {
        MessageDigest sha = null;
        try {
            String myKey = "laJwtKey";// env.getProperty("jwtSecurityKey");
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 32);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String createJWT(String subject) {

        String id = "dfsJwtKey";// env.getProperty("jwtSecurityId");
        String issuer = "dfsJwtId";// env.getProperty("jwtSecurityIssuer");
        int ttlMints =15;

        // The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        // We will sign our JWT with our ApiKey secret
        setKey();
        Key signingKey = new SecretKeySpec(key, signatureAlgorithm.getJcaName());

        // Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id).setIssuedAt(new Date()).setSubject(subject).setIssuer(issuer)
                .signWith(signingKey);

        // if it has been specified, let's add the expiration

        if (ttlMints >= 0) {
            Calendar calendar = Calendar.getInstance();
            // Add 15 minutes to the calendar time
            calendar.add(Calendar.MINUTE, ttlMints);
            builder.setExpiration(null);
        }

        // Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public String createJWTWithClaims(String subject, Map<String, Object> claims,Environment env) {

        String id =  env.getProperty("jwtSecurityId");
        String issuer =  env.getProperty("jwtSecurityIssuer");
        int ttlMints = Integer.parseInt(env.getProperty("loginTokenTime"));

        // The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        // We will sign our JWT with our ApiKey secret
        setKey();
        Key signingKey = new SecretKeySpec(key, signatureAlgorithm.getJcaName());

        // Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id).setIssuedAt(new Date()).setSubject(subject).setIssuer(issuer)
                .addClaims(claims).signWith(signingKey);

        // if it has been specified, let's add the expiration

        if (ttlMints >= 0) {
            Calendar calendar = Calendar.getInstance();
            // Add 15 minutes to the calendar time
            calendar.add(Calendar.MINUTE, ttlMints);
            builder.setExpiration(calendar.getTime());
        }

        // Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public HashMap<String, Object> parseJWT(String jwt) {
        try {
            setKey();

            Claims claims = Jwts.parser().setSigningKey(key).parseClaimsJws(jwt).getBody();
            HashMap<String, Object> jwtmap = new HashMap<String, Object>();

            jwtmap.put("ID", claims.getId());
            jwtmap.put("Subject", claims.getSubject());
            jwtmap.put("Issuer", claims.getIssuer());
            jwtmap.put("Expiration", claims.getExpiration() == null ? "" : claims.getExpiration().toString());
            boolean isExpired = claims.getExpiration() != null && claims.getExpiration().after(new Date());
            jwtmap.put("Expired", isExpired ? "N" : "Y");

            jwtmap.put("userName", claims.get("userName"));
            jwtmap.put("companyCode", claims.get("companyCode"));
            jwtmap.put("userCode", claims.get("userCode"));
            return jwtmap;
        } catch (Exception e) {
            System.out.println("\n\n" + e.getMessage());
            if (e.getMessage().contains("JWT expired at")) {
                HashMap<String, Object> jwtmap = new HashMap<String, Object>();
                jwtmap.put("Expired", "Y");
                return jwtmap;
            }
            return null;
        }
    }

    @SuppressWarnings({})
    public String createNonExpirayJWT(String subject, Map<String, Object> claims) {

        String id = "dfsJwtKey";// env.getProperty("jwtSecurityId");
        String issuer = "dfsJwtId";// env.getProperty("jwtSecurityIssuer");

        // The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        // We will sign our JWT with our ApiKey secret
        setKey();
        Key signingKey = new SecretKeySpec(key, signatureAlgorithm.getJcaName());

        // Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id).setIssuedAt(new Date()).setSubject(subject).setIssuer(issuer)
                .setClaims(claims).signWith(signingKey);

        // if it has been specified, let's add the expiration

        builder.setExpiration(null);

        // Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

}
