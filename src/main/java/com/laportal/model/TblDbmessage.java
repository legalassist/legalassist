package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_DBMESSAGES database table.
 */
@Entity
@Table(name = "TBL_DBMESSAGES")
@NamedQuery(name = "TblDbmessage.findAll", query = "SELECT t FROM TblDbmessage t")
public class TblDbmessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_DBMESSAGES_DBMESSAGECODE_GENERATOR", sequenceName = "SEQ_TBL_DBMESSAGES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_DBMESSAGES_DBMESSAGECODE_GENERATOR")
    private long dbmessagecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Column(name = "\"MESSAGE\"")
    private String message;

    private String remarks;

    private String sentto;

    private String usercode;

    //bi-directional many-to-one association to TblDbclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "DBCLAIMCODE")
    private TblDbclaim tblDbclaim;

    @Transient
    private String userName;

    public TblDbmessage() {
    }

    public long getDbmessagecode() {
        return this.dbmessagecode;
    }

    public void setDbmessagecode(long dbmessagecode) {
        this.dbmessagecode = dbmessagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentto() {
        return this.sentto;
    }

    public void setSentto(String sentto) {
        this.sentto = sentto;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblDbclaim getTblDbclaim() {
        return this.tblDbclaim;
    }

    public void setTblDbclaim(TblDbclaim tblDbclaim) {
        this.tblDbclaim = tblDbclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}