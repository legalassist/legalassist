package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_RTAFLOW database table.
 */
@Entity
@Table(name = "TBL_RTAFLOW")
@NamedQuery(name = "TblRtaflow.findAll", query = "SELECT t FROM TblRtaflow t")
public class TblRtaflow implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTAFLOW_RTAFLOWCODE_GENERATOR", sequenceName = "SEQ_RTAFLOW", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTAFLOW_RTAFLOWCODE_GENERATOR")
    private long rtaflowcode;

    private String companycode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblCompaign
    @ManyToOne
    @JoinColumn(name = "COMPAIGNCODE")
    private TblCompaign tblCompaign;

    //bi-directional many-to-one association to TblRtastatus
    @ManyToOne
    @JoinColumn(name = "STATUSCODE")
    private TblRtastatus tblRtastatus;

    //bi-directional many-to-one association to TblRtaflowdetail
    @OneToMany(mappedBy = "tblRtaflow")
    private List<TblRtaflowdetail> tblRtaflowdetails;


    private String taskflag;
    private String editflag;
    private String usercategory;

    public TblRtaflow() {
    }

    public long getRtaflowcode() {
        return this.rtaflowcode;
    }

    public void setRtaflowcode(long rtaflowcode) {
        this.rtaflowcode = rtaflowcode;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblCompaign getTblCompaign() {
        return this.tblCompaign;
    }

    public void setTblCompaign(TblCompaign tblCompaign) {
        this.tblCompaign = tblCompaign;
    }

    public TblRtastatus getTblRtastatus() {
        return this.tblRtastatus;
    }

    public void setTblRtastatus(TblRtastatus tblRtastatus) {
        this.tblRtastatus = tblRtastatus;
    }

    public List<TblRtaflowdetail> getTblRtaflowdetails() {
        return this.tblRtaflowdetails;
    }

    public void setTblRtaflowdetails(List<TblRtaflowdetail> tblRtaflowdetails) {
        this.tblRtaflowdetails = tblRtaflowdetails;
    }

    public TblRtaflowdetail addTblRtaflowdetail(TblRtaflowdetail tblRtaflowdetail) {
        getTblRtaflowdetails().add(tblRtaflowdetail);
        tblRtaflowdetail.setTblRtaflow(this);

        return tblRtaflowdetail;
    }

    public TblRtaflowdetail removeTblRtaflowdetail(TblRtaflowdetail tblRtaflowdetail) {
        getTblRtaflowdetails().remove(tblRtaflowdetail);
        tblRtaflowdetail.setTblRtaflow(null);

        return tblRtaflowdetail;
    }


    public String getTaskflag() {
        return taskflag;
    }

    public void setTaskflag(String taskflag) {
        this.taskflag = taskflag;
    }

    public String getEditflag() {
        return editflag;
    }

    public void setEditflag(String editflag) {
        this.editflag = editflag;
    }

    public String getUsercategory() {
        return usercategory;
    }

    public void setUsercategory(String usercategory) {
        this.usercategory = usercategory;
    }
}