package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the TBL_TENANCYDOCUMENTS database table.
 */
@Entity
@Table(name = "TBL_TENANCYDOCUMENTS")
@NamedQuery(name = "TblTenancydocument.findAll", query = "SELECT t FROM TblTenancydocument t")
public class TblTenancydocument implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_TENANCYDOCUMENTS_TENANCYDOCUMENTSCODE_GENERATOR", sequenceName = "SEQ_TBL_TENANCYDOCUMENTS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_TENANCYDOCUMENTS_TENANCYDOCUMENTSCODE_GENERATOR")
    private long tenancydocumentscode;

    @Column(name = "DOCUMENT_PATH")
    private String documentPath;

    @Column(name = "DOCUMENT_TYPE")
    private String documentType;

    //bi-directional many-to-one association to TblTenancyclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "TENANCYCLAIMCODE")
    private TblTenancyclaim tblTenancyclaim;

    public TblTenancydocument() {
    }

    public long getTenancydocumentscode() {
        return this.tenancydocumentscode;
    }

    public void setTenancydocumentscode(long tenancydocumentscode) {
        this.tenancydocumentscode = tenancydocumentscode;
    }

    public String getDocumentPath() {
        return this.documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDocumentType() {
        return this.documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public TblTenancyclaim getTblTenancyclaim() {
        return this.tblTenancyclaim;
    }

    public void setTblTenancyclaim(TblTenancyclaim tblTenancyclaim) {
        this.tblTenancyclaim = tblTenancyclaim;
    }

}