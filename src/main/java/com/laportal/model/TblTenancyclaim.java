package com.laportal.model;

import com.laportal.dto.HdrActionButton;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_TENANCYCLAIM database table.
 */
@Entity
@Table(name = "TBL_TENANCYCLAIM")
@NamedQuery(name = "TblTenancyclaim.findAll", query = "SELECT t FROM TblTenancyclaim t")
public class TblTenancyclaim implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_TENANCYCLAIM_TENANCYCLAIMCODE_GENERATOR", sequenceName = "SEQ_TBL_TENANCYCLAIM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_TENANCYCLAIM_TENANCYCLAIMCODE_GENERATOR")
    private long tenancyclaimcode;

    @Column(name = "AMOUNT_OF_DEPOSIT_RETURNED")
    private String amountOfDepositReturned;

    private String arrears;

    @Column(name = "CLAIM_HOUSING_BENEFITS_LIVING")
    private String claimHousingBenefitsLiving;

    private String contact;

    @Column(name = "CURRENT_ADDRESS")
    private String currentAddress;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_MOVED_ON")
    private Date dateMovedOn;

    @Column(name = "DEPOSIT_PAID")
    private String depositPaid;

    @Temporal(TemporalType.DATE)
    @Column(name = "DEPOSIT_PAID_DATE")
    private Date depositPaidDate;

    @Column(name = "DEPOSIT_PAID_METHOD")
    private String depositPaidMethod;

    @Column(name = "DEPOSIT_PROTECTED")
    private String depositProtected;

    @Temporal(TemporalType.DATE)
    private Date dob;

    private String email;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "HAVE_LANDLORD_CONTACT")
    private String haveLandlordContact;

    @Column(name = "LANDLORD_ADDRESS")
    private String landlordAddress;

    @Column(name = "LANDLORD_CONTACT")
    private String landlordContact;

    @Column(name = "LANDLORD_DAMAGE_CLAIM")
    private String landlordDamageClaim;

    @Column(name = "LANDLORD_EMAIL_ADDRESS")
    private String landlordEmailAddress;

    @Column(name = "LANDLORD_LIVE_IN_PROPERTY")
    private String landlordLiveInProperty;

    @Column(name = "NAME_OF_AGENCY")
    private String nameOfAgency;

    @Column(name = "NO_OF_TENANCY_AGRMT_SIGNED")
    private String noOfTenancyAgrmtSigned;

    @Column(name = "ONLY_TENANT_REGISTERED")
    private String onlyTenantRegistered;

    @Column(name = "PAID_TO_LANDLORD_OR_AGENT")
    private String paidToLandlordOrAgent;

    @Column(name = "PAYMENT_PLAN")
    private String paymentPlan;

    @Column(name = "RECEIVING_BENEFITS")
    private String receivingBenefits;

    @Column(name = "RENT_AMOUNT")
    private String rentAmount;

    @Column(name = "RENT_ARREARS")
    private String rentArrears;

    @Column(name = "RENT_ARREARS_DETAIL")
    private String rentArrearsDetail;

    @Column(name = "STILL_LIVING_IN_PROPERTY")
    private String stillLivingInProperty;

    @Temporal(TemporalType.DATE)
    @Column(name = "TERM_DATE")
    private Date termDate;

    @Column(name = "THINK_REASON")
    private String thinkReason;

    @Column(name = "WITHHELD_REASON")
    private String withheldReason;

    @Column(name = "TENANCY_CODE")
    private String tenancyCode;

    private Date createdate;

    private BigDecimal createuser;

    private String remarks;

    private long status;

    private String esig;

    private Date esigdate;

    //bi-directional many-to-one association to TblTenancylog
    @OneToMany(mappedBy = "tblTenancyclaim")
    private List<TblTenancylog> tblTenancylogs;

    //bi-directional many-to-one association to TblTenancymessage
    @OneToMany(mappedBy = "tblTenancyclaim")
    private List<TblTenancymessage> tblTenancymessages;

    //bi-directional many-to-one association to TblTenancynote
    @OneToMany(mappedBy = "tblTenancyclaim")
    private List<TblTenancynote> tblTenancynotes;

    //bi-directional many-to-one association to TblTenancysolicitor
    @OneToMany(mappedBy = "tblTenancyclaim")
    private List<TblTenancysolicitor> tblTenancysolicitors;

    @Transient
    private List<HdrActionButton> hdrActionButton;

    @OneToMany(mappedBy = "tblTenancyclaim")
    private List<TblTenancydocument> tblTenancydocuments;

    @Transient
    private List<HdrActionButton> hdrActionButtonForLA;

    @Transient
    private String statusDescr;

    @Temporal(TemporalType.DATE)
    private Date updatedate;

    public TblTenancyclaim() {
    }

    public long getTenancyclaimcode() {
        return this.tenancyclaimcode;
    }

    public void setTenancyclaimcode(long tenancyclaimcode) {
        this.tenancyclaimcode = tenancyclaimcode;
    }

    public String getAmountOfDepositReturned() {
        return this.amountOfDepositReturned;
    }

    public void setAmountOfDepositReturned(String amountOfDepositReturned) {
        this.amountOfDepositReturned = amountOfDepositReturned;
    }

    public String getArrears() {
        return this.arrears;
    }

    public void setArrears(String arrears) {
        this.arrears = arrears;
    }

    public String getClaimHousingBenefitsLiving() {
        return this.claimHousingBenefitsLiving;
    }

    public void setClaimHousingBenefitsLiving(String claimHousingBenefitsLiving) {
        this.claimHousingBenefitsLiving = claimHousingBenefitsLiving;
    }

    public String getContact() {
        return this.contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCurrentAddress() {
        return this.currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public Date getDateMovedOn() {
        return this.dateMovedOn;
    }

    public void setDateMovedOn(Date dateMovedOn) {
        this.dateMovedOn = dateMovedOn;
    }

    public String getDepositPaid() {
        return this.depositPaid;
    }

    public void setDepositPaid(String depositPaid) {
        this.depositPaid = depositPaid;
    }

    public Date getDepositPaidDate() {
        return this.depositPaidDate;
    }

    public void setDepositPaidDate(Date depositPaidDate) {
        this.depositPaidDate = depositPaidDate;
    }

    public String getDepositPaidMethod() {
        return this.depositPaidMethod;
    }

    public void setDepositPaidMethod(String depositPaidMethod) {
        this.depositPaidMethod = depositPaidMethod;
    }

    public String getDepositProtected() {
        return this.depositProtected;
    }

    public void setDepositProtected(String depositProtected) {
        this.depositProtected = depositProtected;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getHaveLandlordContact() {
        return this.haveLandlordContact;
    }

    public void setHaveLandlordContact(String haveLandlordContact) {
        this.haveLandlordContact = haveLandlordContact;
    }

    public String getLandlordAddress() {
        return this.landlordAddress;
    }

    public void setLandlordAddress(String landlordAddress) {
        this.landlordAddress = landlordAddress;
    }

    public String getLandlordContact() {
        return this.landlordContact;
    }

    public void setLandlordContact(String landlordContact) {
        this.landlordContact = landlordContact;
    }

    public String getLandlordDamageClaim() {
        return this.landlordDamageClaim;
    }

    public void setLandlordDamageClaim(String landlordDamageClaim) {
        this.landlordDamageClaim = landlordDamageClaim;
    }

    public String getLandlordEmailAddress() {
        return this.landlordEmailAddress;
    }

    public void setLandlordEmailAddress(String landlordEmailAddress) {
        this.landlordEmailAddress = landlordEmailAddress;
    }

    public String getLandlordLiveInProperty() {
        return this.landlordLiveInProperty;
    }

    public void setLandlordLiveInProperty(String landlordLiveInProperty) {
        this.landlordLiveInProperty = landlordLiveInProperty;
    }

    public String getNameOfAgency() {
        return this.nameOfAgency;
    }

    public void setNameOfAgency(String nameOfAgency) {
        this.nameOfAgency = nameOfAgency;
    }

    public String getNoOfTenancyAgrmtSigned() {
        return this.noOfTenancyAgrmtSigned;
    }

    public void setNoOfTenancyAgrmtSigned(String noOfTenancyAgrmtSigned) {
        this.noOfTenancyAgrmtSigned = noOfTenancyAgrmtSigned;
    }

    public String getOnlyTenantRegistered() {
        return this.onlyTenantRegistered;
    }

    public void setOnlyTenantRegistered(String onlyTenantRegistered) {
        this.onlyTenantRegistered = onlyTenantRegistered;
    }

    public String getPaidToLandlordOrAgent() {
        return this.paidToLandlordOrAgent;
    }

    public void setPaidToLandlordOrAgent(String paidToLandlordOrAgent) {
        this.paidToLandlordOrAgent = paidToLandlordOrAgent;
    }

    public String getPaymentPlan() {
        return this.paymentPlan;
    }

    public void setPaymentPlan(String paymentPlan) {
        this.paymentPlan = paymentPlan;
    }

    public String getReceivingBenefits() {
        return this.receivingBenefits;
    }

    public void setReceivingBenefits(String receivingBenefits) {
        this.receivingBenefits = receivingBenefits;
    }

    public String getRentAmount() {
        return this.rentAmount;
    }

    public void setRentAmount(String rentAmount) {
        this.rentAmount = rentAmount;
    }

    public String getRentArrears() {
        return this.rentArrears;
    }

    public void setRentArrears(String rentArrears) {
        this.rentArrears = rentArrears;
    }

    public String getRentArrearsDetail() {
        return this.rentArrearsDetail;
    }

    public void setRentArrearsDetail(String rentArrearsDetail) {
        this.rentArrearsDetail = rentArrearsDetail;
    }

    public String getStillLivingInProperty() {
        return this.stillLivingInProperty;
    }

    public void setStillLivingInProperty(String stillLivingInProperty) {
        this.stillLivingInProperty = stillLivingInProperty;
    }

    public Date getTermDate() {
        return this.termDate;
    }

    public void setTermDate(Date termDate) {
        this.termDate = termDate;
    }

    public String getThinkReason() {
        return this.thinkReason;
    }

    public void setThinkReason(String thinkReason) {
        this.thinkReason = thinkReason;
    }

    public String getWithheldReason() {
        return this.withheldReason;
    }

    public void setWithheldReason(String withheldReason) {
        this.withheldReason = withheldReason;
    }

    public List<TblTenancylog> getTblTenancylogs() {
        return this.tblTenancylogs;
    }

    public void setTblTenancylogs(List<TblTenancylog> tblTenancylogs) {
        this.tblTenancylogs = tblTenancylogs;
    }

    public TblTenancylog addTblTenancylog(TblTenancylog tblTenancylog) {
        getTblTenancylogs().add(tblTenancylog);
        tblTenancylog.setTblTenancyclaim(this);

        return tblTenancylog;
    }

    public TblTenancylog removeTblTenancylog(TblTenancylog tblTenancylog) {
        getTblTenancylogs().remove(tblTenancylog);
        tblTenancylog.setTblTenancyclaim(null);

        return tblTenancylog;
    }

    public List<TblTenancymessage> getTblTenancymessages() {
        return this.tblTenancymessages;
    }

    public void setTblTenancymessages(List<TblTenancymessage> tblTenancymessages) {
        this.tblTenancymessages = tblTenancymessages;
    }

    public TblTenancymessage addTblTenancymessage(TblTenancymessage tblTenancymessage) {
        getTblTenancymessages().add(tblTenancymessage);
        tblTenancymessage.setTblTenancyclaim(this);

        return tblTenancymessage;
    }

    public TblTenancymessage removeTblTenancymessage(TblTenancymessage tblTenancymessage) {
        getTblTenancymessages().remove(tblTenancymessage);
        tblTenancymessage.setTblTenancyclaim(null);

        return tblTenancymessage;
    }

    public List<TblTenancynote> getTblTenancynotes() {
        return this.tblTenancynotes;
    }

    public void setTblTenancynotes(List<TblTenancynote> tblTenancynotes) {
        this.tblTenancynotes = tblTenancynotes;
    }

    public TblTenancynote addTblTenancynote(TblTenancynote tblTenancynote) {
        getTblTenancynotes().add(tblTenancynote);
        tblTenancynote.setTblTenancyclaim(this);

        return tblTenancynote;
    }

    public TblTenancynote removeTblTenancynote(TblTenancynote tblTenancynote) {
        getTblTenancynotes().remove(tblTenancynote);
        tblTenancynote.setTblTenancyclaim(null);

        return tblTenancynote;
    }

    public List<TblTenancysolicitor> getTblTenancysolicitors() {
        return this.tblTenancysolicitors;
    }

    public void setTblTenancysolicitors(List<TblTenancysolicitor> tblTenancysolicitors) {
        this.tblTenancysolicitors = tblTenancysolicitors;
    }

    public TblTenancysolicitor addTblTenancysolicitor(TblTenancysolicitor tblTenancysolicitor) {
        getTblTenancysolicitors().add(tblTenancysolicitor);
        tblTenancysolicitor.setTblTenancyclaim(this);

        return tblTenancysolicitor;
    }

    public TblTenancysolicitor removeTblTenancysolicitor(TblTenancysolicitor tblTenancysolicitor) {
        getTblTenancysolicitors().remove(tblTenancysolicitor);
        tblTenancysolicitor.setTblTenancyclaim(null);

        return tblTenancysolicitor;
    }

    public String getTenancyCode() {
        return tenancyCode;
    }

    public void setTenancyCode(String tenancyCode) {
        this.tenancyCode = tenancyCode;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public List<HdrActionButton> getHdrActionButton() {
        return hdrActionButton;
    }

    public void setHdrActionButton(List<HdrActionButton> hdrActionButton) {
        this.hdrActionButton = hdrActionButton;
    }

    public List<TblTenancydocument> getTblTenancydocuments() {
        return tblTenancydocuments;
    }

    public void setTblTenancydocuments(List<TblTenancydocument> tblTenancydocuments) {
        this.tblTenancydocuments = tblTenancydocuments;
    }

    public String getEsig() {
        return esig;
    }

    public void setEsig(String esig) {
        this.esig = esig;
    }

    public Date getEsigdate() {
        return esigdate;
    }

    public void setEsigdate(Date esigdate) {
        this.esigdate = esigdate;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public List<HdrActionButton> getHdrActionButtonForLA() {
        return hdrActionButtonForLA;
    }

    public void setHdrActionButtonForLA(List<HdrActionButton> hdrActionButtonForLA) {
        this.hdrActionButtonForLA = hdrActionButtonForLA;
    }
}