package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the TBL_USERROLES database table.
 */
@Entity
@Table(name = "TBL_USERROLES")
@NamedQuery(name = "TblUserrole.findAll", query = "SELECT t FROM TblUserrole t")
public class TblUserrole implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
//	@SequenceGenerator(name="TBL_USERROLES_USERROLECODE_GENERATOR", sequenceName="SEQ_TBL_USERROLES",allocationSize = 1)
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_USERROLES_USERROLECODE_GENERATOR")
    private String userrolecode;

    private String remarks;

    private String rolecode;

    private String status;

    //bi-directional many-to-one association to TblPage
    @ManyToOne
    @JoinColumn(name = "PAGECODE")
    private TblPage tblPage;

    //bi-directional many-to-one association to TblUser
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "USERCODE")
    private TblUser tblUser;

    @Transient
    private String roleName;

    public TblUserrole() {
    }

    public String getUserrolecode() {
        return this.userrolecode;
    }

    public void setUserrolecode(String userrolecode) {
        this.userrolecode = userrolecode;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRolecode() {
        return this.rolecode;
    }

    public void setRolecode(String rolecode) {
        this.rolecode = rolecode;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TblPage getTblPage() {
        return this.tblPage;
    }

    public void setTblPage(TblPage tblPage) {
        this.tblPage = tblPage;
    }

    public TblUser getTblUser() {
        return this.tblUser;
    }

    public void setTblUser(TblUser tblUser) {
        this.tblUser = tblUser;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}