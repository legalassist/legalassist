package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_PLSOLICITOR database table.
 */
@Entity
@Table(name = "TBL_PLSOLICITOR")
@NamedQuery(name = "TblPlsolicitor.findAll", query = "SELECT t FROM TblPlsolicitor t")
public class TblPlsolicitor implements Serializable {
    private static final long serialVersionUID = 1L;

    private String companycode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Id
    @SequenceGenerator(name = "TBL_PLSOLICITOR_PLSOLCODE_GENERATOR", sequenceName = "SEQ_TBL_PLSOLICITOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PLSOLICITOR_PLSOLCODE_GENERATOR")
    private long plsolcode;

    private String reference1;

    private String reference2;

    private String remarks;

    private String status;

    private String usercode;

    //bi-directional many-to-one association to TblPlclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PLCLAIMCODE")
    private TblPlclaim tblPlclaim;

    @Transient
    private TblCompanyprofile tblCompanyprofile;

    public TblPlsolicitor() {
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public long getPlsolcode() {
        return plsolcode;
    }

    public void setPlsolcode(long plsolcode) {
        this.plsolcode = plsolcode;
    }

    public String getReference1() {
        return this.reference1;
    }

    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getReference2() {
        return this.reference2;
    }

    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblPlclaim getTblPlclaim() {
        return this.tblPlclaim;
    }

    public void setTblPlclaim(TblPlclaim tblPlclaim) {
        this.tblPlclaim = tblPlclaim;
    }

    public TblCompanyprofile getTblCompanyprofile() {
        return tblCompanyprofile;
    }

    public void setTblCompanyprofile(TblCompanyprofile tblCompanyprofile) {
        this.tblCompanyprofile = tblCompanyprofile;
    }
}