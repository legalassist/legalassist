package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_MNSOLICITOR database table.
 */
@Entity
@Table(name = "TBL_MNSOLICITOR")
@NamedQuery(name = "TblMnsolicitor.findAll", query = "SELECT t FROM TblMnsolicitor t")
public class TblMnsolicitor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_MNSOLICITOR_MNSOLCODE_GENERATOR", sequenceName = "SEQ_TBL_MNSOLICITOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_MNSOLICITOR_MNSOLCODE_GENERATOR")
    private long mnsolcode;

    private String companycode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String reference1;

    private String reference2;

    private String remarks;

    private String status;

    private String usercode;

    //bi-directional many-to-one association to TblMnclaim
    @ManyToOne
    @JoinColumn(name = "MNCLAIMCODE")
    private TblMnclaim tblMnclaim;

    @Transient
    private TblCompanyprofile tblCompanyprofile;

    public TblMnsolicitor() {
    }

    public long getMnsolcode() {
        return this.mnsolcode;
    }

    public void setMnsolcode(long mnsolcode) {
        this.mnsolcode = mnsolcode;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getReference1() {
        return this.reference1;
    }

    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getReference2() {
        return this.reference2;
    }

    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblMnclaim getTblMnclaim() {
        return this.tblMnclaim;
    }

    public void setTblMnclaim(TblMnclaim tblMnclaim) {
        this.tblMnclaim = tblMnclaim;
    }

    public TblCompanyprofile getTblCompanyprofile() {
        return tblCompanyprofile;
    }

    public void setTblCompanyprofile(TblCompanyprofile tblCompanyprofile) {
        this.tblCompanyprofile = tblCompanyprofile;
    }

}