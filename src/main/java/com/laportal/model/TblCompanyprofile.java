package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_COMPANYPROFILE database table.
 */
@Entity
@Table(name = "TBL_COMPANYPROFILE")
@NamedQuery(name = "TblCompanyprofile.findAll", query = "SELECT t FROM TblCompanyprofile t")
public class TblCompanyprofile implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
//	@SequenceGenerator(name="TBL_COMPANYPROFILE_COMPANYCODE_GENERATOR", sequenceName="SEQ_TBL_COMPANYPROFILE")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_COMPANYPROFILE_COMPANYCODE_GENERATOR")
    private String companycode;

    private String accountno;

    private String addressline1;

    private String addressline2;

    private String bankname;

    private String city;

    private String companyregno;

    private String companystatus;

    private String contactperson;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String dataprocno;

    private String directintroducer;

    private String docpath;

    private String doctype;

    private String email;

    private String logopath;

    private String name;

    private String pattoname;

    private String phone;

    private String postcode;

    private String region;

    private String remarks;

    private String sortcode;

    private String tag;

    private String templatedoc;

    @Column(name = "\"TYPE\"")
    private String type;

    private String usercode;

    private String varegno;

    private String website;
    private String phone2;

    private long tagnextval;

    private String billtoemail;
    private String billtoname;
    private String accountemail;
    private String secondaryaccountemail;

    private String vat;
    @Column(name = "AIRBAG_CASE")
    private String airbagCase;

    @Transient
    private List<TblCompanyjob> tblCompanyjobs;

    //bi-directional many-to-one association to TblService
    @ManyToOne
    @JoinColumn(name = "SERVICECODE")
    private TblService tblService;

    //bi-directional many-to-one association to TblUsercategory
    @ManyToOne
    @JoinColumn(name = "CATEGORYCODE")
    private TblUsercategory tblUsercategory;

    @Transient
    private List<TblUser> tblUsers;

    //bi-directional many-to-one association to TblService
    @ManyToOne
    @JoinColumn(name = "BDM_USER")
    private TblUser bdmuser;

    private String jurisdiction;

    @Column(name = "ACCOUNT_NAME")
    private String accountName;

    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;

    public TblCompanyprofile() {
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getAccountno() {
        return this.accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getAddressline1() {
        return this.addressline1;
    }

    public void setAddressline1(String addressline1) {
        this.addressline1 = addressline1;
    }

    public String getAddressline2() {
        return this.addressline2;
    }

    public void setAddressline2(String addressline2) {
        this.addressline2 = addressline2;
    }

    public String getBankname() {
        return this.bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompanyregno() {
        return this.companyregno;
    }

    public void setCompanyregno(String companyregno) {
        this.companyregno = companyregno;
    }

    public String getCompanystatus() {
        return this.companystatus;
    }

    public void setCompanystatus(String companystatus) {
        this.companystatus = companystatus;
    }

    public String getContactperson() {
        return this.contactperson;
    }

    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDataprocno() {
        return this.dataprocno;
    }

    public void setDataprocno(String dataprocno) {
        this.dataprocno = dataprocno;
    }

    public String getDirectintroducer() {
        return this.directintroducer;
    }

    public void setDirectintroducer(String directintroducer) {
        this.directintroducer = directintroducer;
    }

    public String getDocpath() {
        return this.docpath;
    }

    public void setDocpath(String docpath) {
        this.docpath = docpath;
    }

    public String getDoctype() {
        return this.doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogopath() {
        return this.logopath;
    }

    public void setLogopath(String logopath) {
        this.logopath = logopath;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPattoname() {
        return this.pattoname;
    }

    public void setPattoname(String pattoname) {
        this.pattoname = pattoname;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getRegion() {
        return this.region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSortcode() {
        return this.sortcode;
    }

    public void setSortcode(String sortcode) {
        this.sortcode = sortcode;
    }

    public String getTag() {
        return this.tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTemplatedoc() {
        return this.templatedoc;
    }

    public void setTemplatedoc(String templatedoc) {
        this.templatedoc = templatedoc;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getVaregno() {
        return this.varegno;
    }

    public void setVaregno(String varegno) {
        this.varegno = varegno;
    }

    public String getWebsite() {
        return this.website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<TblCompanyjob> getTblCompanyjobs() {
        return this.tblCompanyjobs;
    }

    public void setTblCompanyjobs(List<TblCompanyjob> tblCompanyjobs) {
        this.tblCompanyjobs = tblCompanyjobs;
    }

    public TblCompanyjob addTblCompanyjob(TblCompanyjob tblCompanyjob) {
        getTblCompanyjobs().add(tblCompanyjob);

        return tblCompanyjob;
    }

    public TblCompanyjob removeTblCompanyjob(TblCompanyjob tblCompanyjob) {
        getTblCompanyjobs().remove(tblCompanyjob);

        return tblCompanyjob;
    }

    public TblService getTblService() {
        return this.tblService;
    }

    public void setTblService(TblService tblService) {
        this.tblService = tblService;
    }

    public TblUsercategory getTblUsercategory() {
        return this.tblUsercategory;
    }

    public void setTblUsercategory(TblUsercategory tblUsercategory) {
        this.tblUsercategory = tblUsercategory;
    }


    public List<TblUser> getTblUsers() {
        return this.tblUsers;
    }

    public void setTblUsers(List<TblUser> tblUsers) {
        this.tblUsers = tblUsers;
    }

    public TblUser addTblUser(TblUser tblUser) {
        getTblUsers().add(tblUser);

        return tblUser;
    }

    public TblUser removeTblUser(TblUser tblUser) {
        getTblUsers().remove(tblUser);

        return tblUser;
    }


    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }


    public long getTagnextval() {
        return tagnextval;
    }

    public void setTagnextval(long tagnextval) {
        this.tagnextval = tagnextval;
    }


    public String getBilltoemail() {
        return billtoemail;
    }

    public void setBilltoemail(String billtoemail) {
        this.billtoemail = billtoemail;
    }

    public String getBilltoname() {
        return billtoname;
    }

    public void setBilltoname(String billtoname) {
        this.billtoname = billtoname;
    }

    public String getAccountemail() {
        return accountemail;
    }

    public void setAccountemail(String accountemail) {
        this.accountemail = accountemail;
    }

    public String getSecondaryaccountemail() {
        return secondaryaccountemail;
    }

    public void setSecondaryaccountemail(String secondryaccountemail) {
        this.secondaryaccountemail = secondryaccountemail;
    }

    public TblUser getBdmuser() {
        return bdmuser;
    }

    public void setBdmuser(TblUser bdmuser) {
        this.bdmuser = bdmuser;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(String jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public String getAirbagCase() {
        return airbagCase;
    }

    public void setAirbagCase(String airbagCase) {
        this.airbagCase = airbagCase;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}