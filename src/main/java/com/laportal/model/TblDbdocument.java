package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the TBL_DBDOCUMENTS database table.
 */
@Entity
@Table(name = "TBL_DBDOCUMENTS")
@NamedQuery(name = "TblDbdocument.findAll", query = "SELECT t FROM TblDbdocument t")
public class TblDbdocument implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_DBDOCUMENTS_DBDOCUMENTSCODE_GENERATOR", sequenceName = "SEQ_TBL_DBDOCUMENTS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_DBDOCUMENTS_DBDOCUMENTSCODE_GENERATOR")
    private long dbdocumentscode;

    @Column(name = "DOCUMENT_PATH")
    private String documentPath;

    @Column(name = "DOCUMENT_TYPE")
    private String documentType;

    //bi-directional many-to-one association to TblDbclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "DBCLAIMCODE")
    private TblDbclaim tblDbclaim;

    public TblDbdocument() {
    }

    public long getDbdocumentscode() {
        return this.dbdocumentscode;
    }

    public void setDbdocumentscode(long dbdocumentscode) {
        this.dbdocumentscode = dbdocumentscode;
    }

    public String getDocumentPath() {
        return this.documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDocumentType() {
        return this.documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public TblDbclaim getTblDbclaim() {
        return this.tblDbclaim;
    }

    public void setTblDbclaim(TblDbclaim tblDbclaim) {
        this.tblDbclaim = tblDbclaim;
    }

}