package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HIRENOTES database table.
 */
@Entity
@Table(name = "TBL_HIRENOTES")
@NamedQuery(name = "TblHirenote.findAll", query = "SELECT t FROM TblHirenote t")
public class TblHirenote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HIRENOTES_HIRENOTECODE_GENERATOR", sequenceName = "TBL_HIRENOTES_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HIRENOTES_HIRENOTECODE_GENERATOR")
    private long hirenotecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String note;

    private String remarks;

    private String usercode;

    private String usercategorycode;

    //bi-directional many-to-one association to TblHireclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HIRECODE")
    private TblHireclaim tblHireclaim;

    @Transient
    private String userName;

    @Transient
    private boolean self;

    public TblHirenote() {
    }

    public long getHirenotecode() {
        return this.hirenotecode;
    }

    public void setHirenotecode(long hirenotecode) {
        this.hirenotecode = hirenotecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblHireclaim getTblHireclaim() {
        return this.tblHireclaim;
    }

    public void setTblHireclaim(TblHireclaim tblHireclaim) {
        this.tblHireclaim = tblHireclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }

    public String getUsercategorycode() {
        return usercategorycode;
    }

    public void setUsercategorycode(String usercategorycode) {
        this.usercategorycode = usercategorycode;
    }
}