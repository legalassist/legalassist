package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_SYSTEM database table.
 */
@Entity
@Table(name = "TBL_SYSTEM")
@NamedQuery(name = "TblSystem.findAll", query = "SELECT t FROM TblSystem t")
public class TblSystem implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_SYSTEM_SYSTEMCODE_GENERATOR", sequenceName = "SEQ_TBL_SYSTEM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_SYSTEM_SYSTEMCODE_GENERATOR")
    private String systemcode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String systemname;

    private String systemstatus;

    private String usercode;

    public TblSystem() {
    }

    public String getSystemcode() {
        return this.systemcode;
    }

    public void setSystemcode(String systemcode) {
        this.systemcode = systemcode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSystemname() {
        return this.systemname;
    }

    public void setSystemname(String systemname) {
        this.systemname = systemname;
    }

    public String getSystemstatus() {
        return this.systemstatus;
    }

    public void setSystemstatus(String systemstatus) {
        this.systemstatus = systemstatus;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

}