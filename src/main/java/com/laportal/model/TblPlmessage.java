package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_PLMESSAGES database table.
 */
@Entity
@Table(name = "TBL_PLMESSAGES")
@NamedQuery(name = "TblPlmessage.findAll", query = "SELECT t FROM TblPlmessage t")
public class TblPlmessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PLMESSAGES_PLMESSAGECODE_GENERATOR", sequenceName = "SEQ_TBL_PLMESSAGES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PLMESSAGES_PLMESSAGECODE_GENERATOR")
    private long code;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Lob
    @Column(name = "MESSAGE")
    private String message;

    private String remarks;

    private String sentto;

    private String usercode;

    //bi-directional many-to-one association to TblPlclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PLCLAIMCODE")
    private TblPlclaim tblPlclaim;

    @Transient
    private String userName;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "EMAILCODE")
    private TblEmail emailcode;

    public TblPlmessage() {
    }

    public long getCode() {
        return this.code;
    }

    public void setCode(long plmessagecode) {
        this.code = plmessagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentto() {
        return this.sentto;
    }

    public void setSentto(String sentto) {
        this.sentto = sentto;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblPlclaim getTblPlclaim() {
        return this.tblPlclaim;
    }

    public void setTblPlclaim(TblPlclaim tblPlclaim) {
        this.tblPlclaim = tblPlclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public TblEmail getEmailcode() {
        return emailcode;
    }

    public void setEmailcode(TblEmail emailcode) {
        this.emailcode = emailcode;
    }
}