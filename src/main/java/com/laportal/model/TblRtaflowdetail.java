package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the TBL_RTAFLOWDETAIL database table.
 */
@Entity
@Table(name = "TBL_RTAFLOWDETAIL")
@NamedQuery(name = "TblRtaflowdetail.findAll", query = "SELECT t FROM TblRtaflowdetail t")
public class TblRtaflowdetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTAFLOWDETAIL_RTAFLODETAILWCODE_GENERATOR", sequenceName = "SEQ_RTAFLOWDETAIL", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTAFLOWDETAIL_RTAFLODETAILWCODE_GENERATOR")
    private long rtaflodetailwcode;

    private String apiflag;

    private String buttonname;

    private String buttonvalue;

    private String listusercategory;


    private String caseacceptdialog;
    private String caseassigndialog;
    private String caserejectdialog;
    private String casecanceldialog;


    //bi-directional many-to-one association to TblRtaflow
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "RTAFLOWCODE")
    private TblRtaflow tblRtaflow;

    //bi-directional many-to-one association to TblRtastatus
    @ManyToOne
    @JoinColumn(name = "TOSTATUSCODE")
    private TblRtastatus tblRtastatus;

    public TblRtaflowdetail() {
    }

    public long getRtaflodetailwcode() {
        return this.rtaflodetailwcode;
    }

    public void setRtaflodetailwcode(long rtaflodetailwcode) {
        this.rtaflodetailwcode = rtaflodetailwcode;
    }

    public String getApiflag() {
        return this.apiflag;
    }

    public void setApiflag(String apiflag) {
        this.apiflag = apiflag;
    }

    public String getButtonname() {
        return this.buttonname;
    }

    public void setButtonname(String buttonname) {
        this.buttonname = buttonname;
    }

    public String getButtonvalue() {
        return this.buttonvalue;
    }

    public void setButtonvalue(String buttonvalue) {
        this.buttonvalue = buttonvalue;
    }

    public TblRtaflow getTblRtaflow() {
        return this.tblRtaflow;
    }

    public void setTblRtaflow(TblRtaflow tblRtaflow) {
        this.tblRtaflow = tblRtaflow;
    }

    public TblRtastatus getTblRtastatus() {
        return this.tblRtastatus;
    }

    public void setTblRtastatus(TblRtastatus tblRtastatus) {
        this.tblRtastatus = tblRtastatus;
    }

    public String getListusercategory() {
        return listusercategory;
    }

    public void setListusercategory(String listusercategory) {
        this.listusercategory = listusercategory;
    }

    public String getCaseacceptdialog() {
        return caseacceptdialog;
    }

    public void setCaseacceptdialog(String caseacceptdialog) {
        this.caseacceptdialog = caseacceptdialog;
    }

    public String getCaseassigndialog() {
        return caseassigndialog;
    }

    public void setCaseassigndialog(String caseassigndialog) {
        this.caseassigndialog = caseassigndialog;
    }

    public String getCaserejectdialog() {
        return caserejectdialog;
    }

    public void setCaserejectdialog(String caserejectdialog) {
        this.caserejectdialog = caserejectdialog;
    }

    public String getCasecanceldialog() {
        return casecanceldialog;
    }

    public void setCasecanceldialog(String casecanceldialog) {
        this.casecanceldialog = casecanceldialog;
    }
}