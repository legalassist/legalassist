package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_TENANCYMESSAGES database table.
 */
@Entity
@Table(name = "TBL_TENANCYMESSAGES")
@NamedQuery(name = "TblTenancymessage.findAll", query = "SELECT t FROM TblTenancymessage t")
public class TblTenancymessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_TENANCYMESSAGES_TENANCYMESSAGECODE_GENERATOR", sequenceName = "SEQ_TBL_TENANCYMESSAGES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_TENANCYMESSAGES_TENANCYMESSAGECODE_GENERATOR")
    private long tenancymessagecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Column(name = "\"MESSAGE\"")
    private String message;

    private String remarks;

    private String sentto;

    private String usercode;

    //bi-directional many-to-one association to TblTenancyclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "TENANCYCLAIMCODE")
    private TblTenancyclaim tblTenancyclaim;

    @Transient
    private String userName;

    public TblTenancymessage() {
    }

    public long getTenancymessagecode() {
        return this.tenancymessagecode;
    }

    public void setTenancymessagecode(long tenancymessagecode) {
        this.tenancymessagecode = tenancymessagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentto() {
        return this.sentto;
    }

    public void setSentto(String sentto) {
        this.sentto = sentto;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblTenancyclaim getTblTenancyclaim() {
        return this.tblTenancyclaim;
    }

    public void setTblTenancyclaim(TblTenancyclaim tblTenancyclaim) {
        this.tblTenancyclaim = tblTenancyclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}