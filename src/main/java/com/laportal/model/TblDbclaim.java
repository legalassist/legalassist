package com.laportal.model;

import com.laportal.dto.HdrActionButton;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_DBCLAIM database table.
 */
@Entity
@Table(name = "TBL_DBCLAIM")
@NamedQuery(name = "TblDbclaim.findAll", query = "SELECT t FROM TblDbclaim t")
public class TblDbclaim implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_DBCLAIM_DBCLAIMCODE_GENERATOR", sequenceName = "SEQ_TBL_DBCLAIM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_DBCLAIM_DBCLAIMCODE_GENERATOR")
    private long dbclaimcode;

    @Column(name = "ANY_FFM_SUFFERED_DB")
    private String anyFfmSufferedDb;

    @Column(name = "CLAIMANT_ADDRESS")
    private String claimantAddress;

    @Column(name = "CLAIMANT_CONTACTNO")
    private String claimantContactno;

    @Temporal(TemporalType.DATE)
    @Column(name = "CLAIMANT_DOB")
    private Date claimantDob;

    @Column(name = "CLAIMANT_EMAIL")
    private String claimantEmail;

    @Column(name = "CLAIMANT_NAME")
    private String claimantName;

    @Column(name = "COMPLAINT_DEFENDENT")
    private String complaintDefendent;

    @Column(name = "CONSENT_PI_SHARED")
    private String consentPiShared;

    @Temporal(TemporalType.DATE)
    private Date createdate;

    private BigDecimal createuser;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_OF_BREACH")
    private Date dateOfBreach;

    @Column(name = "DATE_OF_KNOWLEDGE")
    private String dateOfKnowledge;

    @Column(name = "DB_BY_COMP_BUSNS")
    private String dbByCompBusns;

    @Column(name = "DB_PREEXISTING_COND")
    private String dbPreexistingCond;

    @Column(name = "DB_RECTIFIED")
    private String dbRectified;

    private String dbcode;

    @Column(name = "DEFENDENT_ADDRESS")
    private String defendentAddress;

    @Column(name = "DEFENDENT_CONTACTNO")
    private String defendentContactno;

    @Column(name = "DEFENDENT_EMAIL")
    private String defendentEmail;

    @Column(name = "DEFENDENT_HANDLE_DB")
    private String defendentHandleDb;

    @Column(name = "DEFENDENT_NAME")
    private String defendentName;

    @Column(name = "DEFENDENT_REQ_CONSENT")
    private String defendentReqConsent;

    private String esig;

    @Temporal(TemporalType.DATE)
    private Date esigdate;

    @Column(name = "EVIDENCE_OF_DB")
    private String evidenceOfDb;

    @Column(name = "FRST_FOUND_ABT_DB")
    private String frstFoundAbtDb;

    @Column(name = "HAS_DB_AFFECTED_YOU")
    private String hasDbAffectedYou;

    @Column(name = "HOW_FEEL_YOUR_DB")
    private String howFeelYourDb;

    @Column(name = "HOW_KNOW_ABT_DB")
    private String howKnowAbtDb;

    @Column(name = "HOW_KNOW_DEFENDENT")
    private String howKnowDefendent;

    @Column(name = "INSTRUCTED_PRVNT_DB")
    private String instructedPrvntDb;

    @Column(name = "LITIGATION_FRIEND")
    private String litigationFriend;

    @Column(name = "OTHERPRBLM_WITH_DEFENDENT")
    private String otherprblmWithDefendent;

    @Column(name = "PI_DISCLOSED_IN_DB")
    private String piDisclosedInDb;

    @Column(name = "PI_SHARED")
    private String piShared;

    @Column(name = "RECURRING_DB_OF_PI")
    private String recurringDbOfPi;

    private String remarks;

    @Column(name = "SOUGHT_MEDICAL_AVDNCE")
    private String soughtMedicalAvdnce;

    private BigDecimal status;

    @Temporal(TemporalType.DATE)
    private Date updatedate;

    //bi-directional many-to-one association to TblDbdocument
    @OneToMany(mappedBy = "tblDbclaim")
    private List<TblDbdocument> tblDbdocuments;

    //bi-directional many-to-one association to TblDblog
    @OneToMany(mappedBy = "tblDbclaim")
    private List<TblDblog> tblDblogs;

    //bi-directional many-to-one association to TblDbmessage
    @OneToMany(mappedBy = "tblDbclaim")
    private List<TblDbmessage> tblDbmessages;

    //bi-directional many-to-one association to TblDbnote
    @OneToMany(mappedBy = "tblDbclaim")
    private List<TblDbnote> tblDbnotes;

    //bi-directional many-to-one association to TblDbsolicitor
    @OneToMany(mappedBy = "tblDbclaim")
    private List<TblDbsolicitor> tblDbsolicitors;

    //bi-directional many-to-one association to TblDbtask
    @OneToMany(mappedBy = "tblDbclaim")
    private List<TblDbtask> tblDbtasks;

    @Transient
    private String statusDescr;

    @Transient
    private List<HdrActionButton> hdrActionButton;

    @Transient
    private List<HdrActionButton> hdrActionButtonForLA;

    public TblDbclaim() {
    }

    public long getDbclaimcode() {
        return this.dbclaimcode;
    }

    public void setDbclaimcode(long dbclaimcode) {
        this.dbclaimcode = dbclaimcode;
    }

    public String getAnyFfmSufferedDb() {
        return this.anyFfmSufferedDb;
    }

    public void setAnyFfmSufferedDb(String anyFfmSufferedDb) {
        this.anyFfmSufferedDb = anyFfmSufferedDb;
    }

    public String getClaimantAddress() {
        return this.claimantAddress;
    }

    public void setClaimantAddress(String claimantAddress) {
        this.claimantAddress = claimantAddress;
    }

    public String getClaimantContactno() {
        return this.claimantContactno;
    }

    public void setClaimantContactno(String claimantContactno) {
        this.claimantContactno = claimantContactno;
    }

    public Date getClaimantDob() {
        return this.claimantDob;
    }

    public void setClaimantDob(Date claimantDob) {
        this.claimantDob = claimantDob;
    }

    public String getClaimantEmail() {
        return this.claimantEmail;
    }

    public void setClaimantEmail(String claimantEmail) {
        this.claimantEmail = claimantEmail;
    }

    public String getClaimantName() {
        return this.claimantName;
    }

    public void setClaimantName(String claimantName) {
        this.claimantName = claimantName;
    }

    public String getComplaintDefendent() {
        return this.complaintDefendent;
    }

    public void setComplaintDefendent(String complaintDefendent) {
        this.complaintDefendent = complaintDefendent;
    }

    public String getConsentPiShared() {
        return this.consentPiShared;
    }

    public void setConsentPiShared(String consentPiShared) {
        this.consentPiShared = consentPiShared;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public Date getDateOfBreach() {
        return this.dateOfBreach;
    }

    public void setDateOfBreach(Date dateOfBreach) {
        this.dateOfBreach = dateOfBreach;
    }

    public String getDateOfKnowledge() {
        return this.dateOfKnowledge;
    }

    public void setDateOfKnowledge(String dateOfKnowledge) {
        this.dateOfKnowledge = dateOfKnowledge;
    }

    public String getDbByCompBusns() {
        return this.dbByCompBusns;
    }

    public void setDbByCompBusns(String dbByCompBusns) {
        this.dbByCompBusns = dbByCompBusns;
    }

    public String getDbPreexistingCond() {
        return this.dbPreexistingCond;
    }

    public void setDbPreexistingCond(String dbPreexistingCond) {
        this.dbPreexistingCond = dbPreexistingCond;
    }

    public String getDbRectified() {
        return this.dbRectified;
    }

    public void setDbRectified(String dbRectified) {
        this.dbRectified = dbRectified;
    }

    public String getDbcode() {
        return this.dbcode;
    }

    public void setDbcode(String dbcode) {
        this.dbcode = dbcode;
    }

    public String getDefendentAddress() {
        return this.defendentAddress;
    }

    public void setDefendentAddress(String defendentAddress) {
        this.defendentAddress = defendentAddress;
    }

    public String getDefendentContactno() {
        return this.defendentContactno;
    }

    public void setDefendentContactno(String defendentContactno) {
        this.defendentContactno = defendentContactno;
    }

    public String getDefendentEmail() {
        return this.defendentEmail;
    }

    public void setDefendentEmail(String defendentEmail) {
        this.defendentEmail = defendentEmail;
    }

    public String getDefendentHandleDb() {
        return this.defendentHandleDb;
    }

    public void setDefendentHandleDb(String defendentHandleDb) {
        this.defendentHandleDb = defendentHandleDb;
    }

    public String getDefendentName() {
        return this.defendentName;
    }

    public void setDefendentName(String defendentName) {
        this.defendentName = defendentName;
    }

    public String getDefendentReqConsent() {
        return this.defendentReqConsent;
    }

    public void setDefendentReqConsent(String defendentReqConsent) {
        this.defendentReqConsent = defendentReqConsent;
    }

    public String getEsig() {
        return this.esig;
    }

    public void setEsig(String esig) {
        this.esig = esig;
    }

    public Date getEsigdate() {
        return this.esigdate;
    }

    public void setEsigdate(Date esigdate) {
        this.esigdate = esigdate;
    }

    public String getEvidenceOfDb() {
        return this.evidenceOfDb;
    }

    public void setEvidenceOfDb(String evidenceOfDb) {
        this.evidenceOfDb = evidenceOfDb;
    }

    public String getFrstFoundAbtDb() {
        return this.frstFoundAbtDb;
    }

    public void setFrstFoundAbtDb(String frstFoundAbtDb) {
        this.frstFoundAbtDb = frstFoundAbtDb;
    }

    public String getHasDbAffectedYou() {
        return this.hasDbAffectedYou;
    }

    public void setHasDbAffectedYou(String hasDbAffectedYou) {
        this.hasDbAffectedYou = hasDbAffectedYou;
    }

    public String getHowFeelYourDb() {
        return this.howFeelYourDb;
    }

    public void setHowFeelYourDb(String howFeelYourDb) {
        this.howFeelYourDb = howFeelYourDb;
    }

    public String getHowKnowAbtDb() {
        return this.howKnowAbtDb;
    }

    public void setHowKnowAbtDb(String howKnowAbtDb) {
        this.howKnowAbtDb = howKnowAbtDb;
    }

    public String getHowKnowDefendent() {
        return this.howKnowDefendent;
    }

    public void setHowKnowDefendent(String howKnowDefendent) {
        this.howKnowDefendent = howKnowDefendent;
    }

    public String getInstructedPrvntDb() {
        return this.instructedPrvntDb;
    }

    public void setInstructedPrvntDb(String instructedPrvntDb) {
        this.instructedPrvntDb = instructedPrvntDb;
    }

    public String getLitigationFriend() {
        return this.litigationFriend;
    }

    public void setLitigationFriend(String litigationFriend) {
        this.litigationFriend = litigationFriend;
    }

    public String getOtherprblmWithDefendent() {
        return this.otherprblmWithDefendent;
    }

    public void setOtherprblmWithDefendent(String otherprblmWithDefendent) {
        this.otherprblmWithDefendent = otherprblmWithDefendent;
    }

    public String getPiDisclosedInDb() {
        return this.piDisclosedInDb;
    }

    public void setPiDisclosedInDb(String piDisclosedInDb) {
        this.piDisclosedInDb = piDisclosedInDb;
    }

    public String getPiShared() {
        return this.piShared;
    }

    public void setPiShared(String piShared) {
        this.piShared = piShared;
    }

    public String getRecurringDbOfPi() {
        return this.recurringDbOfPi;
    }

    public void setRecurringDbOfPi(String recurringDbOfPi) {
        this.recurringDbOfPi = recurringDbOfPi;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSoughtMedicalAvdnce() {
        return this.soughtMedicalAvdnce;
    }

    public void setSoughtMedicalAvdnce(String soughtMedicalAvdnce) {
        this.soughtMedicalAvdnce = soughtMedicalAvdnce;
    }

    public BigDecimal getStatus() {
        return this.status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    public Date getUpdatedate() {
        return this.updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public List<TblDbdocument> getTblDbdocuments() {
        return this.tblDbdocuments;
    }

    public void setTblDbdocuments(List<TblDbdocument> tblDbdocuments) {
        this.tblDbdocuments = tblDbdocuments;
    }

    public TblDbdocument addTblDbdocument(TblDbdocument tblDbdocument) {
        getTblDbdocuments().add(tblDbdocument);
        tblDbdocument.setTblDbclaim(this);

        return tblDbdocument;
    }

    public TblDbdocument removeTblDbdocument(TblDbdocument tblDbdocument) {
        getTblDbdocuments().remove(tblDbdocument);
        tblDbdocument.setTblDbclaim(null);

        return tblDbdocument;
    }

    public List<TblDblog> getTblDblogs() {
        return this.tblDblogs;
    }

    public void setTblDblogs(List<TblDblog> tblDblogs) {
        this.tblDblogs = tblDblogs;
    }

    public TblDblog addTblDblog(TblDblog tblDblog) {
        getTblDblogs().add(tblDblog);
        tblDblog.setTblDbclaim(this);

        return tblDblog;
    }

    public TblDblog removeTblDblog(TblDblog tblDblog) {
        getTblDblogs().remove(tblDblog);
        tblDblog.setTblDbclaim(null);

        return tblDblog;
    }

    public List<TblDbmessage> getTblDbmessages() {
        return this.tblDbmessages;
    }

    public void setTblDbmessages(List<TblDbmessage> tblDbmessages) {
        this.tblDbmessages = tblDbmessages;
    }

    public TblDbmessage addTblDbmessage(TblDbmessage tblDbmessage) {
        getTblDbmessages().add(tblDbmessage);
        tblDbmessage.setTblDbclaim(this);

        return tblDbmessage;
    }

    public TblDbmessage removeTblDbmessage(TblDbmessage tblDbmessage) {
        getTblDbmessages().remove(tblDbmessage);
        tblDbmessage.setTblDbclaim(null);

        return tblDbmessage;
    }

    public List<TblDbnote> getTblDbnotes() {
        return this.tblDbnotes;
    }

    public void setTblDbnotes(List<TblDbnote> tblDbnotes) {
        this.tblDbnotes = tblDbnotes;
    }

    public TblDbnote addTblDbnote(TblDbnote tblDbnote) {
        getTblDbnotes().add(tblDbnote);
        tblDbnote.setTblDbclaim(this);

        return tblDbnote;
    }

    public TblDbnote removeTblDbnote(TblDbnote tblDbnote) {
        getTblDbnotes().remove(tblDbnote);
        tblDbnote.setTblDbclaim(null);

        return tblDbnote;
    }

    public List<TblDbsolicitor> getTblDbsolicitors() {
        return this.tblDbsolicitors;
    }

    public void setTblDbsolicitors(List<TblDbsolicitor> tblDbsolicitors) {
        this.tblDbsolicitors = tblDbsolicitors;
    }

    public TblDbsolicitor addTblDbsolicitor(TblDbsolicitor tblDbsolicitor) {
        getTblDbsolicitors().add(tblDbsolicitor);
        tblDbsolicitor.setTblDbclaim(this);

        return tblDbsolicitor;
    }

    public TblDbsolicitor removeTblDbsolicitor(TblDbsolicitor tblDbsolicitor) {
        getTblDbsolicitors().remove(tblDbsolicitor);
        tblDbsolicitor.setTblDbclaim(null);

        return tblDbsolicitor;
    }

    public List<TblDbtask> getTblDbtasks() {
        return this.tblDbtasks;
    }

    public void setTblDbtasks(List<TblDbtask> tblDbtasks) {
        this.tblDbtasks = tblDbtasks;
    }

    public TblDbtask addTblDbtask(TblDbtask tblDbtask) {
        getTblDbtasks().add(tblDbtask);
        tblDbtask.setTblDbclaim(this);

        return tblDbtask;
    }

    public TblDbtask removeTblDbtask(TblDbtask tblDbtask) {
        getTblDbtasks().remove(tblDbtask);
        tblDbtask.setTblDbclaim(null);

        return tblDbtask;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }

    public List<HdrActionButton> getHdrActionButton() {
        return hdrActionButton;
    }

    public void setHdrActionButton(List<HdrActionButton> hdrActionButton) {
        this.hdrActionButton = hdrActionButton;
    }

    public List<HdrActionButton> getHdrActionButtonForLA() {
        return hdrActionButtonForLA;
    }

    public void setHdrActionButtonForLA(List<HdrActionButton> hdrActionButtonForLA) {
        this.hdrActionButtonForLA = hdrActionButtonForLA;
    }
}