package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.laportal.dto.HireActionButton;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the TBL_HIRECLAIM database table.
 */
@Entity
@Table(name = "TBL_HIRECLAIM")
@NamedQuery(name = "TblHireclaim.findAll", query = "SELECT t FROM TblHireclaim t")
public class TblHireclaim implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HIRECLAIM_GENERATOR", sequenceName = "SEQ_HIRECLAIM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HIRECLAIM_GENERATOR")
    private long hirecode;

    @Temporal(TemporalType.DATE)
    private Date accdate;

    private String acctime;

    private String address1;

    private String address2;

    private String address3;

    private String city;

    private String claimrefno;

    private Date contactdue;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    private Date createdon;

    private String description;

    @Temporal(TemporalType.DATE)
    private Date dob;

    private String driverpassenger;

    private String email;

    private String firstname;

    private String greencardno;

    private String hirenumber;

    private String hiretype;

    private String injdescription;

    private BigDecimal injlength;

    private String instorage;

    private String insurer;

    private String landline;

    private String lastname;

    private Date lastupdated;

    private String location;

    private String makemodel;

    private String medicalinfo;

    private String middlename;

    private String mobile;

    private String ninumber;

    private String ongoing;

    private String outsourced;

    private String partyaddress;

    private String partycontactno;

    private String partyinsurer;

    private String partymakemodel;

    private String partyname;

    private String partypolicyno;

    private String partyrefno;

    private String partyregno;

    private String passengerinfo;

    private String policycover;

    private String policyholder;

    private String policyno;

    private String postalcode;

    private String rdweathercond;

    private String recovered;

    private String recoveredby;

    private String refno;

    private String region;

    private String registerationno;

    private String reportedtopolice;

    private String scotland;

    private String storage;

    private String title;

    private String usercode;

    private String vehiclecondition;
    private String vehicledamage;
    private String esign;

    @Transient
    private List<HireActionButton> hireActionButtons = new ArrayList<HireActionButton>();
    @Transient
    private List<HireActionButton> hireActionButtonForLA = new ArrayList<HireActionButton>();

    // bi-directional many-to-one association to TblRtastatus
    @ManyToOne
    @JoinColumn(name = "STATUSCODE")
    private TblRtastatus tblRtastatus;

    // bi-directional many-to-one association to TblHiredocument

    @OneToMany(mappedBy = "tblHireclaim")
    private List<TblHiredocument> tblHiredocuments;

    // bi-directional many-to-one association to TblHirelog
    @OneToMany(mappedBy = "tblHireclaim")
    private List<TblHirelog> tblHirelogs;

    // bi-directional many-to-one association to TblHiremessage
    @JsonIgnore
    @OneToMany(mappedBy = "tblHireclaim")
    private List<TblHiremessage> tblHiremessages;

    // bi-directional many-to-one association to TblHirenote
    @JsonIgnore
    @OneToMany(mappedBy = "tblHireclaim")
    private List<TblHirenote> tblHirenotes;

    // bi-directional many-to-one association to TblInjclass
    @ManyToOne
    @JoinColumn(name = "INJCLASSCODE")
    private TblInjclass tblInjclass;

    // bi-directional many-to-one association to TblCircumstance
    @ManyToOne
    @JoinColumn(name = "CIRCUMCODE")
    private TblCircumstance circumcode;

    //bi-directional many-to-one association to TblHirebusiness
    @OneToMany(mappedBy = "tblHireclaim")
    private List<TblHirebusiness> tblHirebusinesses;

    @Transient
    private String statusDescr;

    private Long advisor;
    private Long introducer;

    @Transient
    private String advisorname;
    @Transient
    private String introducername;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "CLAWBACK_DATE")
    private Date clawbackDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "SUBMIT_DATE")
    private Date submitDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "INVOICE_DATE")
    private Date invoiceDate;

    @Transient
    private BigDecimal introducerInvoiceHeadId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @Transient
    private Date introducerInvoiceDate;


    @Transient
    private BigDecimal solicitorInvoiceHeadId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @Transient
    private Date solicitorInvoiceDate;

    @Transient
    private List<TblHiretask> hireCaseTasks;

    @Transient
    private TblEsignStatus tblEsignStatus;

    @Column(name = "LASTUPDATEUSER")
    private String lastupdateuser;

    @Transient
    private String editFlag;


    public TblHireclaim() {
    }

    public long getHirecode() {
        return this.hirecode;
    }

    public void setHirecode(long hirecode) {
        this.hirecode = hirecode;
    }

    public Date getAccdate() {
        return this.accdate;
    }

    public void setAccdate(Date accdate) {
        this.accdate = accdate;
    }

    public String getAcctime() {
        return this.acctime;
    }

    public void setAcctime(String acctime) {
        this.acctime = acctime;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return this.address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getClaimrefno() {
        return this.claimrefno;
    }

    public void setClaimrefno(String claimrefno) {
        this.claimrefno = claimrefno;
    }

    public Date getContactdue() {
        return this.contactdue;
    }

    public void setContactdue(Date contactdue) {
        this.contactdue = contactdue;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getDriverpassenger() {
        return this.driverpassenger;
    }

    public void setDriverpassenger(String driverpassenger) {
        this.driverpassenger = driverpassenger;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGreencardno() {
        return this.greencardno;
    }

    public void setGreencardno(String greencardno) {
        this.greencardno = greencardno;
    }

    public String getHirenumber() {
        return this.hirenumber;
    }

    public void setHirenumber(String hirenumber) {
        this.hirenumber = hirenumber;
    }

    public String getHiretype() {
        return this.hiretype;
    }

    public void setHiretype(String hiretype) {
        this.hiretype = hiretype;
    }

    public String getInjdescription() {
        return this.injdescription;
    }

    public void setInjdescription(String injdescription) {
        this.injdescription = injdescription;
    }

    public BigDecimal getInjlength() {
        return this.injlength;
    }

    public void setInjlength(BigDecimal injlength) {
        this.injlength = injlength;
    }

    public String getInstorage() {
        return this.instorage;
    }

    public void setInstorage(String instorage) {
        this.instorage = instorage;
    }

    public String getInsurer() {
        return this.insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public String getLandline() {
        return this.landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getLastupdated() {
        return this.lastupdated;
    }

    public void setLastupdated(Date lastupdated) {
        this.lastupdated = lastupdated;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMakemodel() {
        return this.makemodel;
    }

    public void setMakemodel(String makemodel) {
        this.makemodel = makemodel;
    }

    public String getMedicalinfo() {
        return this.medicalinfo;
    }

    public void setMedicalinfo(String medicalinfo) {
        this.medicalinfo = medicalinfo;
    }

    public String getMiddlename() {
        return this.middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNinumber() {
        return this.ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getOngoing() {
        return this.ongoing;
    }

    public void setOngoing(String ongoing) {
        this.ongoing = ongoing;
    }

    public String getOutsourced() {
        return this.outsourced;
    }

    public void setOutsourced(String outsourced) {
        this.outsourced = outsourced;
    }

    public String getPartyaddress() {
        return this.partyaddress;
    }

    public void setPartyaddress(String partyaddress) {
        this.partyaddress = partyaddress;
    }

    public String getPartycontactno() {
        return this.partycontactno;
    }

    public void setPartycontactno(String partycontactno) {
        this.partycontactno = partycontactno;
    }

    public String getPartyinsurer() {
        return this.partyinsurer;
    }

    public void setPartyinsurer(String partyinsurer) {
        this.partyinsurer = partyinsurer;
    }

    public String getPartymakemodel() {
        return this.partymakemodel;
    }

    public void setPartymakemodel(String partymakemodel) {
        this.partymakemodel = partymakemodel;
    }

    public String getPartyname() {
        return this.partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getPartypolicyno() {
        return this.partypolicyno;
    }

    public void setPartypolicyno(String partypolicyno) {
        this.partypolicyno = partypolicyno;
    }

    public String getPartyrefno() {
        return this.partyrefno;
    }

    public void setPartyrefno(String partyrefno) {
        this.partyrefno = partyrefno;
    }

    public String getPartyregno() {
        return this.partyregno;
    }

    public void setPartyregno(String partyregno) {
        this.partyregno = partyregno;
    }

    public String getPassengerinfo() {
        return this.passengerinfo;
    }

    public void setPassengerinfo(String passengerinfo) {
        this.passengerinfo = passengerinfo;
    }

    public String getPolicycover() {
        return this.policycover;
    }

    public void setPolicycover(String policycover) {
        this.policycover = policycover;
    }

    public String getPolicyholder() {
        return this.policyholder;
    }

    public void setPolicyholder(String policyholder) {
        this.policyholder = policyholder;
    }

    public String getPolicyno() {
        return this.policyno;
    }

    public void setPolicyno(String policyno) {
        this.policyno = policyno;
    }

    public String getPostalcode() {
        return this.postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getRdweathercond() {
        return this.rdweathercond;
    }

    public void setRdweathercond(String rdweathercond) {
        this.rdweathercond = rdweathercond;
    }

    public String getRecovered() {
        return this.recovered;
    }

    public void setRecovered(String recovered) {
        this.recovered = recovered;
    }

    public String getRecoveredby() {
        return this.recoveredby;
    }

    public void setRecoveredby(String recoveredby) {
        this.recoveredby = recoveredby;
    }

    public String getRefno() {
        return this.refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getRegion() {
        return this.region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegisterationno() {
        return this.registerationno;
    }

    public void setRegisterationno(String registerationno) {
        this.registerationno = registerationno;
    }

    public String getReportedtopolice() {
        return this.reportedtopolice;
    }

    public void setReportedtopolice(String reportedtopolice) {
        this.reportedtopolice = reportedtopolice;
    }

    public String getScotland() {
        return this.scotland;
    }

    public void setScotland(String scotland) {
        this.scotland = scotland;
    }

    public String getStorage() {
        return this.storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getVehiclecondition() {
        return this.vehiclecondition;
    }

    public void setVehiclecondition(String vehiclecondition) {
        this.vehiclecondition = vehiclecondition;
    }

    public TblRtastatus getTblRtastatus() {
        return this.tblRtastatus;
    }

    public void setTblRtastatus(TblRtastatus tblRtastatus) {
        this.tblRtastatus = tblRtastatus;
    }

    public List<TblHiredocument> getTblHiredocuments() {
        return this.tblHiredocuments;
    }

    public void setTblHiredocuments(List<TblHiredocument> tblHiredocuments) {
        this.tblHiredocuments = tblHiredocuments;
    }

    public TblHiredocument addTblHiredocument(TblHiredocument tblHiredocument) {
        getTblHiredocuments().add(tblHiredocument);
        tblHiredocument.setTblHireclaim(this);

        return tblHiredocument;
    }

    public TblHiredocument removeTblHiredocument(TblHiredocument tblHiredocument) {
        getTblHiredocuments().remove(tblHiredocument);
        tblHiredocument.setTblHireclaim(null);

        return tblHiredocument;
    }

    public List<TblHirelog> getTblHirelogs() {
        return this.tblHirelogs;
    }

    public void setTblHirelogs(List<TblHirelog> tblHirelogs) {
        this.tblHirelogs = tblHirelogs;
    }

    public TblHirelog addTblHirelog(TblHirelog tblHirelog) {
        getTblHirelogs().add(tblHirelog);
        tblHirelog.setTblHireclaim(this);

        return tblHirelog;
    }

    public TblHirelog removeTblHirelog(TblHirelog tblHirelog) {
        getTblHirelogs().remove(tblHirelog);
        tblHirelog.setTblHireclaim(null);

        return tblHirelog;
    }

    public List<TblHiremessage> getTblHiremessages() {
        return this.tblHiremessages;
    }

    public void setTblHiremessages(List<TblHiremessage> tblHiremessages) {
        this.tblHiremessages = tblHiremessages;
    }

    public TblHiremessage addTblHiremessage(TblHiremessage tblHiremessage) {
        getTblHiremessages().add(tblHiremessage);
        tblHiremessage.setTblHireclaim(this);

        return tblHiremessage;
    }

    public TblHiremessage removeTblHiremessage(TblHiremessage tblHiremessage) {
        getTblHiremessages().remove(tblHiremessage);
        tblHiremessage.setTblHireclaim(null);

        return tblHiremessage;
    }

    public List<TblHirenote> getTblHirenotes() {
        return this.tblHirenotes;
    }

    public void setTblHirenotes(List<TblHirenote> tblHirenotes) {
        this.tblHirenotes = tblHirenotes;
    }

    public TblHirenote addTblHirenote(TblHirenote tblHirenote) {
        getTblHirenotes().add(tblHirenote);
        tblHirenote.setTblHireclaim(this);

        return tblHirenote;
    }

    public TblHirenote removeTblHirenote(TblHirenote tblHirenote) {
        getTblHirenotes().remove(tblHirenote);
        tblHirenote.setTblHireclaim(null);

        return tblHirenote;
    }

    public TblInjclass getTblInjclass() {
        return tblInjclass;
    }

    public void setTblInjclass(TblInjclass tblInjclass) {
        this.tblInjclass = tblInjclass;
    }

    public TblCircumstance getCircumcode() {
        return circumcode;
    }

    public void setCircumcode(TblCircumstance tblCircumstance) {
        this.circumcode = tblCircumstance;
    }

    public List<TblHirebusiness> getTblHirebusinesses() {
        return tblHirebusinesses;
    }

    public void setTblHirebusinesses(List<TblHirebusiness> tblHirebusinesses) {
        this.tblHirebusinesses = tblHirebusinesses;
    }

    public List<HireActionButton> getHireActionButtons() {
        return hireActionButtons;
    }

    public void setHireActionButtons(List<HireActionButton> hireActionButtons) {
        this.hireActionButtons = hireActionButtons;
    }

    public String getVehicledamage() {
        return vehicledamage;
    }

    public void setVehicledamage(String vehicledamage) {
        this.vehicledamage = vehicledamage;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }

    public Long getAdvisor() {
        return advisor;
    }

    public void setAdvisor(Long advisor) {
        this.advisor = advisor;
    }

    public Long getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Long introducer) {
        this.introducer = introducer;
    }

    public String getAdvisorname() {
        return advisorname;
    }

    public void setAdvisorname(String advisorname) {
        this.advisorname = advisorname;
    }

    public String getIntroducername() {
        return introducername;
    }

    public void setIntroducername(String introducername) {
        this.introducername = introducername;
    }

    public List<HireActionButton> getHireActionButtonForLA() {
        return hireActionButtonForLA;
    }

    public void setHireActionButtonForLA(List<HireActionButton> hireActionButtonForLA) {
        this.hireActionButtonForLA = hireActionButtonForLA;
    }

    public Date getClawbackDate() {
        return clawbackDate;
    }

    public void setClawbackDate(Date clawbackDate) {
        this.clawbackDate = clawbackDate;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public BigDecimal getIntroducerInvoiceHeadId() {
        return introducerInvoiceHeadId;
    }

    public void setIntroducerInvoiceHeadId(BigDecimal introducerInvoiceHeadId) {
        this.introducerInvoiceHeadId = introducerInvoiceHeadId;
    }

    public Date getIntroducerInvoiceDate() {
        return introducerInvoiceDate;
    }

    public void setIntroducerInvoiceDate(Date introducerInvoiceDate) {
        this.introducerInvoiceDate = introducerInvoiceDate;
    }

    public BigDecimal getSolicitorInvoiceHeadId() {
        return solicitorInvoiceHeadId;
    }

    public void setSolicitorInvoiceHeadId(BigDecimal solicitorInvoiceHeadId) {
        this.solicitorInvoiceHeadId = solicitorInvoiceHeadId;
    }

    public Date getSolicitorInvoiceDate() {
        return solicitorInvoiceDate;
    }

    public void setSolicitorInvoiceDate(Date solicitorInvoiceDate) {
        this.solicitorInvoiceDate = solicitorInvoiceDate;
    }

    public List<TblHiretask> getHireCaseTasks() {
        return hireCaseTasks;
    }

    public void setHireCaseTasks(List<TblHiretask> hireCaseTasks) {
        this.hireCaseTasks = hireCaseTasks;
    }

    public String getEsign() {
        return esign;
    }

    public void setEsign(String esign) {
        this.esign = esign;
    }

    public TblEsignStatus getTblEsignStatus() {
        return tblEsignStatus;
    }

    public void setTblEsignStatus(TblEsignStatus tblEsignStatus) {
        this.tblEsignStatus = tblEsignStatus;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }

    public String getEditFlag() {
        return editFlag;
    }

    public void setEditFlag(String editFlag) {
        this.editFlag = editFlag;
    }
}