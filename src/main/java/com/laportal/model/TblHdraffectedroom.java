package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRAFFECTEDROOM database table.
 */
@Entity
@Table(name = "TBL_HDRAFFECTEDROOM")
@NamedQuery(name = "TblHdraffectedroom.findAll", query = "SELECT t FROM TblHdraffectedroom t")
public class TblHdraffectedroom implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRAFFECTEDROOM_HDRAFFECTEDROOM_GENERATOR", sequenceName = "SEQ_TBL_HDRAFFECTEDROOM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRAFFECTEDROOM_HDRAFFECTEDROOM_GENERATOR")
    private long hdraffectedroom;

    @Column(name = "ROOM_NAME")
    private String roomName;

    @Column(name = "DAMAGE_LIST")
    private String damageList;

    @Column(name = "DISREPAIR_DETAIL")
    private String disrepairDetail;


    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_REPORTED")
    private Date lastReported;

    @Column(name = "PERSONAL_PROPERTYDAMAGE")
    private String personalPropertydamage;

    @Column(name = "REPORT_DETAILS")
    private String reportDetails;

    //bi-directional many-to-one association to TblHdrclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HDRCLAIMCODE")
    private TblHdrclaim tblHdrclaim;


    @Column(name = "LASTUPDATEUSER")
    private String lastupdateuser;


    public TblHdraffectedroom() {
    }

    public long getHdraffectedroom() {
        return this.hdraffectedroom;
    }

    public void setHdraffectedroom(long hdraffectedroom) {
        this.hdraffectedroom = hdraffectedroom;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getDamageList() {
        return this.damageList;
    }

    public void setDamageList(String damageList) {
        this.damageList = damageList;
    }

    public String getDisrepairDetail() {
        return this.disrepairDetail;
    }

    public void setDisrepairDetail(String disrepairDetail) {
        this.disrepairDetail = disrepairDetail;
    }

    public Date getLastReported() {
        return this.lastReported;
    }

    public void setLastReported(Date lastReported) {
        this.lastReported = lastReported;
    }

    public String getPersonalPropertydamage() {
        return this.personalPropertydamage;
    }

    public void setPersonalPropertydamage(String personalPropertydamage) {
        this.personalPropertydamage = personalPropertydamage;
    }

    public String getReportDetails() {
        return this.reportDetails;
    }

    public void setReportDetails(String reportDetails) {
        this.reportDetails = reportDetails;
    }

    public TblHdrclaim getTblHdrclaim() {
        return this.tblHdrclaim;
    }

    public void setTblHdrclaim(TblHdrclaim tblHdrclaim) {
        this.tblHdrclaim = tblHdrclaim;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }
}