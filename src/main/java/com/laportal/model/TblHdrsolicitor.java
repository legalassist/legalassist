package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRSOLICITOR database table.
 */
@Entity
@Table(name = "TBL_HDRSOLICITOR")
@NamedQuery(name = "TblHdrsolicitor.findAll", query = "SELECT t FROM TblHdrsolicitor t")
public class TblHdrsolicitor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRSOLICITOR_HDRSOLCODE_GENERATOR", sequenceName = "SEQ_TBL_HDRSOLICITOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRSOLICITOR_HDRSOLCODE_GENERATOR")
    private long hdrsolcode;

    private String companycode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String reference1;

    private String reference2;

    private String remarks;

    private String status;

    private String usercode;

    //bi-directional many-to-one association to TblHdrclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HDRCLAIMCODE")
    private TblHdrclaim tblHdrclaim;

    @Transient
    private TblCompanyprofile tblCompanyprofile;


    @Column(name = "LASTUPDATEUSER")
    private String lastupdateuser;


    public TblHdrsolicitor() {
    }

    public long getHdrsolcode() {
        return this.hdrsolcode;
    }

    public void setHdrsolcode(long hdrsolcode) {
        this.hdrsolcode = hdrsolcode;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getReference1() {
        return this.reference1;
    }

    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getReference2() {
        return this.reference2;
    }

    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblHdrclaim getTblHdrclaim() {
        return this.tblHdrclaim;
    }

    public void setTblHdrclaim(TblHdrclaim tblHdrclaim) {
        this.tblHdrclaim = tblHdrclaim;
    }

    public TblCompanyprofile getTblCompanyprofile() {
        return tblCompanyprofile;
    }

    public void setTblCompanyprofile(TblCompanyprofile tblCompanyprofile) {
        this.tblCompanyprofile = tblCompanyprofile;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }
}