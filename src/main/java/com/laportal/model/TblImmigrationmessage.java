package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_IMMIGRATIONMESSAGES database table.
 */
@Entity
@Table(name = "TBL_IMMIGRATIONMESSAGES")
@NamedQuery(name = "TblImmigrationmessage.findAll", query = "SELECT t FROM TblImmigrationmessage t")
public class TblImmigrationmessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_IMMIGRATIONMESSAGES_IMMIGRATIONMESSAGECODE_GENERATOR", sequenceName = "SEQ_TBL_IMMIGRATIONMESSAGES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_IMMIGRATIONMESSAGES_IMMIGRATIONMESSAGECODE_GENERATOR")
    private long immigrationmessagecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Column(name = "\"MESSAGE\"")
    private String message;

    private String remarks;

    private String sentto;

    private String usercode;

    //bi-directional many-to-one association to TblImmigrationclaim
    @ManyToOne
    @JoinColumn(name = "IMMIGRATIONCLAIMCODE")
    private TblImmigrationclaim tblImmigrationclaim;

    @Transient
    private String userName;

    public TblImmigrationmessage() {
    }

    public long getImmigrationmessagecode() {
        return this.immigrationmessagecode;
    }

    public void setImmigrationmessagecode(long immigrationmessagecode) {
        this.immigrationmessagecode = immigrationmessagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentto() {
        return this.sentto;
    }

    public void setSentto(String sentto) {
        this.sentto = sentto;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblImmigrationclaim getTblImmigrationclaim() {
        return this.tblImmigrationclaim;
    }

    public void setTblImmigrationclaim(TblImmigrationclaim tblImmigrationclaim) {
        this.tblImmigrationclaim = tblImmigrationclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}