package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_MNLOGS database table.
 */
@Entity
@Table(name = "TBL_MNLOGS")
@NamedQuery(name = "TblMnlog.findAll", query = "SELECT t FROM TblMnlog t")
public class TblMnlog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_MNLOGS_MNLOGCODE_GENERATOR", sequenceName = "SEQ_TBL_MNLOGS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_MNLOGS_MNLOGCODE_GENERATOR")
    private long mnlogcode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblMnclaim
    @ManyToOne
    @JoinColumn(name = "MNCLAIMCODE")
    private TblMnclaim tblMnclaim;

    @Transient
    private String userName;



    private Long oldstatus;

    private Long newstatus;

    public TblMnlog() {
    }

    public long getMnlogcode() {
        return this.mnlogcode;
    }

    public void setMnlogcode(long mnlogcode) {
        this.mnlogcode = mnlogcode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblMnclaim getTblMnclaim() {
        return this.tblMnclaim;
    }

    public void setTblMnclaim(TblMnclaim tblMnclaim) {
        this.tblMnclaim = tblMnclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getOldstatus() {
        return oldstatus;
    }

    public void setOldstatus(Long oldstatus) {
        this.oldstatus = oldstatus;
    }

    public Long getNewstatus() {
        return newstatus;
    }

    public void setNewstatus(Long newstatus) {
        this.newstatus = newstatus;
    }
}