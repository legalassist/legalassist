package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The persistent class for the VIEW_INVOICE database table.
 */
@Entity
@Table(name = "VIEW_INVOICE")
@NamedQuery(name = "ViewInvoice.findAll", query = "SELECT v FROM ViewInvoice v")
public class ViewInvoice implements Serializable {
    private static final long serialVersionUID = 1L;

    private BigDecimal amount;

    @Column(name = "BILL_TO_ADDRESS")
    private String billToAddress;

    @Column(name = "BILL_TO_NAME")
    private String billToName;

    @Column(name = "DATE_CREATED")
    private String dateCreated;

    @Column(name = "DATE_UPDATED")
    private String dateUpdated;

    @Id
    private String invoicecode;

    @Column(name = "ISSUED_BY")
    private String issuedBy;

    private String source;

    private String status;

    @Column(name = "URL_LINK")
    private String urlLink;

    public ViewInvoice() {
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBillToAddress() {
        return this.billToAddress;
    }

    public void setBillToAddress(String billToAddress) {
        this.billToAddress = billToAddress;
    }

    public String getBillToName() {
        return this.billToName;
    }

    public void setBillToName(String billToName) {
        this.billToName = billToName;
    }

    public String getDateCreated() {
        return this.dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateUpdated() {
        return this.dateUpdated;
    }

    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getInvoicecode() {
        return this.invoicecode;
    }

    public void setInvoicecode(String invoicecode) {
        this.invoicecode = invoicecode;
    }

    public String getIssuedBy() {
        return this.issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrlLink() {
        return this.urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }

}