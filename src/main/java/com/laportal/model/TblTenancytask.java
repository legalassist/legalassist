package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_TENANCYTASKS database table.
 */
@Entity
@Table(name = "TBL_TENANCYTASKS")
@NamedQuery(name = "TblTenancytask.findAll", query = "SELECT t FROM TblTenancytask t")
public class TblTenancytask implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_TENANCYTASKS_TENANCYTASKCODE_GENERATOR", sequenceName = "SEQ_TBL_TENANCYTASKS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_TENANCYTASKS_TENANCYTASKCODE_GENERATOR")
    private long tenancytaskcode;

    private String completedby;

    @Temporal(TemporalType.DATE)
    private Date completedon;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String currenttask;

    private String remarks;

    private String status;

    private BigDecimal taskcode;

    private BigDecimal tenancycode;

    //bi-directional one-to-one association to TblTenancyclaim
    @OneToOne
    @JoinColumn(name = "TENANCYCLAIMCODE")
    private TblTenancyclaim tblTenancyclaim;

    public TblTenancytask() {
    }

    public long getTenancytaskcode() {
        return this.tenancytaskcode;
    }

    public void setTenancytaskcode(long tenancytaskcode) {
        this.tenancytaskcode = tenancytaskcode;
    }

    public String getCompletedby() {
        return this.completedby;
    }

    public void setCompletedby(String completedby) {
        this.completedby = completedby;
    }

    public Date getCompletedon() {
        return this.completedon;
    }

    public void setCompletedon(Date completedon) {
        this.completedon = completedon;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getCurrenttask() {
        return this.currenttask;
    }

    public void setCurrenttask(String currenttask) {
        this.currenttask = currenttask;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getTaskcode() {
        return this.taskcode;
    }

    public void setTaskcode(BigDecimal taskcode) {
        this.taskcode = taskcode;
    }

    public BigDecimal getTenancycode() {
        return this.tenancycode;
    }

    public void setTenancycode(BigDecimal tenancycode) {
        this.tenancycode = tenancycode;
    }

    public TblTenancyclaim getTblTenancyclaim() {
        return this.tblTenancyclaim;
    }

    public void setTblTenancyclaim(TblTenancyclaim tblTenancyclaim) {
        this.tblTenancyclaim = tblTenancyclaim;
    }

    public void setTblTask(TblTask tblTask) {
    }
}