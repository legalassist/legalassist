package com.laportal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * The persistent class for the TBL_RTACLAIM database table.
 */
@Entity
@Table(name = "TBL_RTACLAIM")
@NamedQuery(name = "TblRtaclaim.findAll", query = "SELECT t FROM TblRtaclaim t")
public class TblRtaclaim implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TBL_RTACLAIM_RTACODE_GENERATOR", sequenceName = "SEQ_RTACLAIM", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTACLAIM_RTACODE_GENERATOR")
	private long rtacode;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date accdate;

	private String acctime;

	private String address1;

	private String address2;

	private String address3;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date amldate;

	private String amlmessage;

	private String amverfication;

	private String city;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date contactdue;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date createdon;

	private String description;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date dob;

	private String driverpassenger;

	private String email;

	private String englishlevel;

	private String esig;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date esigdate;

	private String firstname;

	private String gaddress1;

	private String gaddress2;

	private String gaddress3;

	private String gcity;

	private String gemail;

	private String gfirstname;

	private String glandline;

	private String glastname;

	private String gmiddlename;

	private String gmobile;

	private String gpostalcode;

	private String greencardno;

	private String gregion;

	private String gtitle;

	private String injdescription;

	private BigDecimal injlength;

	private String insurer;

	private String landline;

	private String lastname;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date lastupdated;

	private String location;

	private String makemodel;

	private String medicalinfo;

	private String middlename;

	private String mobile;

	private String ninumber;

	private String ongoing;

	private String partyaddress;

	private String partycontactno;

	private String partyinsurer;

	private String partymakemodel;

	private String partyname;

	private String partypolicyno;

	private String partyrefno;

	private String partyregno;

	private String passengerinfo;

	private String policyno;

	private String postalcode;

	private String rdweathercond;

	private String refno;

	private String region;

	private String registerationno;

	private String reportedtopolice;

	private String rtalinkcode;

	private String rtanumber;

	private String scotland;

	private String statuscode;

	private String title;

	private String usercode;

	private String vehiclecondition;

	private String password;
	private String translatordetails;
	private String alternativenumber;
	private String medicalevidence;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date reportedon;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date gdob;

	private String otherlanguages;

	private String remarks;
	private String yearofmanufacture;
	private BigDecimal rtalogcode;

	// bi-directional many-to-one association to TblCircumstance
	@ManyToOne
	@JoinColumn(name = "CIRCUMCODE")
	private TblCircumstance tblCircumstance;

	// bi-directional many-to-one association to TblInjclass
	@ManyToOne
	@JoinColumn(name = "INJCLASSCODE")
	private TblInjclass tblInjclass;

	// bi-directional many-to-one association to TblRtadocument
	@JsonIgnore
	@OneToMany(mappedBy = "tblRtaclaim")
	private List<TblRtadocument> tblRtadocuments;

	// bi-directional many-to-one association to TblRtalog
	@JsonIgnore
	@OneToMany(mappedBy = "tblRtaclaim")
	private List<TblRtalog> tblRtalogs;

	// bi-directional many-to-one association to TblRtamessage
	@JsonIgnore
	@OneToMany(mappedBy = "tblRtaclaim")
	private List<TblRtamessage> tblRtamessages;

	// bi-directional many-to-one association to TblRtanote
	@JsonIgnore
	@OneToMany(mappedBy = "tblRtaclaim")
	private List<TblRtanote> tblRtanotes;

	// bi-directional many-to-one association to TblRtapassenger
	@JsonIgnore
	@OneToMany(mappedBy = "tblRtaclaim")
	private List<TblRtapassenger> tblRtapassengers;

	@Column(name = "VEHICLE_TYPE")
	private String vehicleType;

	@Transient
	private List<TblRtapassenger> rtapassengers = new ArrayList<>();

	private Long advisor;
	private Long introducer;
	private String referencenumber;
	@Transient
	private String advisorname;
	@Transient
	private String introducername;

	private String airbagopened;

	private String lastupdateuser;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "UTC")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "CLAWBACK_DATE")
	private Date clawbackDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Column(name = "SUBMIT_DATE")
	private Date submitDate;


	@Transient
	List<TblRtainjury> injclasscodes = new ArrayList<>();

	@Column(name = "VD_IMAGES")
	private String vdImages;
	@Column(name = "INJURY_SUSTAINED")
	private String injurySustained;

	public TblRtaclaim() {
	}

	public long getRtacode() {
		return this.rtacode;
	}

	public void setRtacode(long rtacode) {
		this.rtacode = rtacode;
	}

	public Date getAccdate() {
		return this.accdate;
	}

	public void setAccdate(Date accdate) {
		this.accdate = accdate;
	}

	public String getAcctime() {
		return this.acctime;
	}

	public void setAcctime(String acctime) {
		this.acctime = acctime;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getContactdue() {
		return this.contactdue;
	}

	public void setContactdue(Date contactdue) {
		this.contactdue = contactdue;
	}

	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getDriverpassenger() {
		return this.driverpassenger;
	}

	public void setDriverpassenger(String driverpassenger) {
		this.driverpassenger = driverpassenger;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEnglishlevel() {
		return this.englishlevel;
	}

	public void setEnglishlevel(String englishlevel) {
		this.englishlevel = englishlevel;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getGreencardno() {
		return this.greencardno;
	}

	public void setGreencardno(String greencardno) {
		this.greencardno = greencardno;
	}

	public String getInjdescription() {
		return this.injdescription;
	}

	public void setInjdescription(String injdescription) {
		this.injdescription = injdescription;
	}

	public BigDecimal getInjlength() {
		return this.injlength;
	}

	public void setInjlength(BigDecimal injlength) {
		this.injlength = injlength;
	}

	public String getInsurer() {
		return this.insurer;
	}

	public void setInsurer(String insurer) {
		this.insurer = insurer;
	}

	public String getLandline() {
		return this.landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMakemodel() {
		return this.makemodel;
	}

	public void setMakemodel(String makemodel) {
		this.makemodel = makemodel;
	}

	public String getMedicalinfo() {
		return this.medicalinfo;
	}

	public void setMedicalinfo(String medicalinfo) {
		this.medicalinfo = medicalinfo;
	}

	public String getMiddlename() {
		return this.middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getNinumber() {
		return this.ninumber;
	}

	public void setNinumber(String ninumber) {
		this.ninumber = ninumber;
	}

	public String getOngoing() {
		return this.ongoing;
	}

	public void setOngoing(String ongoing) {
		this.ongoing = ongoing;
	}

	public String getPartyaddress() {
		return this.partyaddress;
	}

	public void setPartyaddress(String partyaddress) {
		this.partyaddress = partyaddress;
	}

	public String getPartycontactno() {
		return this.partycontactno;
	}

	public void setPartycontactno(String partycontactno) {
		this.partycontactno = partycontactno;
	}

	public String getPartyinsurer() {
		return this.partyinsurer;
	}

	public void setPartyinsurer(String partyinsurer) {
		this.partyinsurer = partyinsurer;
	}

	public String getPartymakemodel() {
		return this.partymakemodel;
	}

	public void setPartymakemodel(String partymakemodel) {
		this.partymakemodel = partymakemodel;
	}

	public String getPartyname() {
		return this.partyname;
	}

	public void setPartyname(String partyname) {
		this.partyname = partyname;
	}

	public String getPartypolicyno() {
		return this.partypolicyno;
	}

	public void setPartypolicyno(String partypolicyno) {
		this.partypolicyno = partypolicyno;
	}

	public String getPartyrefno() {
		return this.partyrefno;
	}

	public void setPartyrefno(String partyrefno) {
		this.partyrefno = partyrefno;
	}

	public String getPartyregno() {
		return this.partyregno;
	}

	public void setPartyregno(String partyregno) {
		this.partyregno = partyregno;
	}

	public String getPassengerinfo() {
		return this.passengerinfo;
	}

	public void setPassengerinfo(String passengerinfo) {
		this.passengerinfo = passengerinfo;
	}

	public String getPolicyno() {
		return this.policyno;
	}

	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}

	public String getPostalcode() {
		return this.postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getRdweathercond() {
		return this.rdweathercond;
	}

	public void setRdweathercond(String rdweathercond) {
		this.rdweathercond = rdweathercond;
	}

	public String getRefno() {
		return this.refno;
	}

	public void setRefno(String refno) {
		this.refno = refno;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRegisterationno() {
		return this.registerationno;
	}

	public void setRegisterationno(String registerationno) {
		this.registerationno = registerationno;
	}

	public String getReportedtopolice() {
		return this.reportedtopolice;
	}

	public void setReportedtopolice(String reportedtopolice) {
		this.reportedtopolice = reportedtopolice;
	}

	public String getRtalinkcode() {
		return this.rtalinkcode;
	}

	public void setRtalinkcode(String rtalinkcode) {
		this.rtalinkcode = rtalinkcode;
	}

	public String getRtanumber() {
		return this.rtanumber;
	}

	public void setRtanumber(String rtanumber) {
		this.rtanumber = rtanumber;
	}

	public String getScotland() {
		return this.scotland;
	}

	public void setScotland(String scotland) {
		this.scotland = scotland;
	}

	public String getStatuscode() {
		return this.statuscode;
	}

	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUsercode() {
		return this.usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public String getVehiclecondition() {
		return this.vehiclecondition;
	}

	public void setVehiclecondition(String vehiclecondition) {
		this.vehiclecondition = vehiclecondition;
	}

	public TblCircumstance getTblCircumstance() {
		return this.tblCircumstance;
	}

	public void setTblCircumstance(TblCircumstance tblCircumstance) {
		this.tblCircumstance = tblCircumstance;
	}

	public TblInjclass getTblInjclass() {
		return this.tblInjclass;
	}

	public void setTblInjclass(TblInjclass tblInjclass) {
		this.tblInjclass = tblInjclass;
	}

	public List<TblRtadocument> getTblRtadocuments() {
		return this.tblRtadocuments;
	}

	public void setTblRtadocuments(List<TblRtadocument> tblRtadocuments) {
		this.tblRtadocuments = tblRtadocuments;
	}

	public TblRtadocument addTblRtadocument(TblRtadocument tblRtadocument) {
		getTblRtadocuments().add(tblRtadocument);
		tblRtadocument.setTblRtaclaim(this);

		return tblRtadocument;
	}

	public TblRtadocument removeTblRtadocument(TblRtadocument tblRtadocument) {
		getTblRtadocuments().remove(tblRtadocument);
		tblRtadocument.setTblRtaclaim(null);

		return tblRtadocument;
	}

	public List<TblRtalog> getTblRtalogs() {
		return this.tblRtalogs;
	}

	public void setTblRtalogs(List<TblRtalog> tblRtalogs) {
		this.tblRtalogs = tblRtalogs;
	}

	public TblRtalog addTblRtalog(TblRtalog tblRtalog) {
		getTblRtalogs().add(tblRtalog);
		tblRtalog.setTblRtaclaim(this);

		return tblRtalog;
	}

	public TblRtalog removeTblRtalog(TblRtalog tblRtalog) {
		getTblRtalogs().remove(tblRtalog);
		tblRtalog.setTblRtaclaim(null);

		return tblRtalog;
	}

	public List<TblRtamessage> getTblRtamessages() {
		return this.tblRtamessages;
	}

	public void setTblRtamessages(List<TblRtamessage> tblRtamessages) {
		this.tblRtamessages = tblRtamessages;
	}

	public TblRtamessage addTblRtamessage(TblRtamessage tblRtamessage) {
		getTblRtamessages().add(tblRtamessage);
		tblRtamessage.setTblRtaclaim(this);

		return tblRtamessage;
	}

	public TblRtamessage removeTblRtamessage(TblRtamessage tblRtamessage) {
		getTblRtamessages().remove(tblRtamessage);
		tblRtamessage.setTblRtaclaim(null);

		return tblRtamessage;
	}

	public List<TblRtanote> getTblRtanotes() {
		return this.tblRtanotes;
	}

	public void setTblRtanotes(List<TblRtanote> tblRtanotes) {
		this.tblRtanotes = tblRtanotes;
	}

	public TblRtanote addTblRtanote(TblRtanote tblRtanote) {
		getTblRtanotes().add(tblRtanote);
		tblRtanote.setTblRtaclaim(this);

		return tblRtanote;
	}

	public TblRtanote removeTblRtanote(TblRtanote tblRtanote) {
		getTblRtanotes().remove(tblRtanote);
		tblRtanote.setTblRtaclaim(null);

		return tblRtanote;
	}

	public List<TblRtapassenger> getTblRtapassengers() {
		return this.tblRtapassengers;
	}

	public void setTblRtapassengers(List<TblRtapassenger> tblRtapassengers) {
		this.tblRtapassengers = tblRtapassengers;
	}

	public TblRtapassenger addTblRtapassenger(TblRtapassenger tblRtapassenger) {
		getTblRtapassengers().add(tblRtapassenger);
		tblRtapassenger.setTblRtaclaim(this);

		return tblRtapassenger;
	}

	public TblRtapassenger removeTblRtapassenger(TblRtapassenger tblRtapassenger) {
		getTblRtapassengers().remove(tblRtapassenger);
		tblRtapassenger.setTblRtaclaim(null);

		return tblRtapassenger;
	}

	public List<TblRtapassenger> getRtapassengers() {
		return rtapassengers;
	}

	public void setRtapassengers(List<TblRtapassenger> rtapassengers) {
		this.rtapassengers = rtapassengers;
	}

	public Date getAmldate() {
		return amldate;
	}

	public void setAmldate(Date amldate) {
		this.amldate = amldate;
	}

	public String getAmlmessage() {
		return amlmessage;
	}

	public void setAmlmessage(String amlmessage) {
		this.amlmessage = amlmessage;
	}

	public String getAmverfication() {
		return amverfication;
	}

	public void setAmverfication(String amverfication) {
		this.amverfication = amverfication;
	}

	public String getEsig() {
		return esig;
	}

	public void setEsig(String esig) {
		this.esig = esig;
	}

	public Date getEsigdate() {
		return esigdate;
	}

	public void setEsigdate(Date esigdate) {
		this.esigdate = esigdate;
	}

	public String getGaddress1() {
		return gaddress1;
	}

	public void setGaddress1(String gaddress1) {
		this.gaddress1 = gaddress1;
	}

	public String getGaddress2() {
		return gaddress2;
	}

	public void setGaddress2(String gaddress2) {
		this.gaddress2 = gaddress2;
	}

	public String getGaddress3() {
		return gaddress3;
	}

	public void setGaddress3(String gaddress3) {
		this.gaddress3 = gaddress3;
	}

	public String getGcity() {
		return gcity;
	}

	public void setGcity(String gcity) {
		this.gcity = gcity;
	}

	public String getGemail() {
		return gemail;
	}

	public void setGemail(String gemail) {
		this.gemail = gemail;
	}

	public String getGfirstname() {
		return gfirstname;
	}

	public void setGfirstname(String gfirstname) {
		this.gfirstname = gfirstname;
	}

	public String getGlandline() {
		return glandline;
	}

	public void setGlandline(String glandline) {
		this.glandline = glandline;
	}

	public String getGlastname() {
		return glastname;
	}

	public void setGlastname(String glastname) {
		this.glastname = glastname;
	}

	public String getGmiddlename() {
		return gmiddlename;
	}

	public void setGmiddlename(String gmiddlename) {
		this.gmiddlename = gmiddlename;
	}

	public String getGmobile() {
		return gmobile;
	}

	public void setGmobile(String gmobile) {
		this.gmobile = gmobile;
	}

	public String getGpostalcode() {
		return gpostalcode;
	}

	public void setGpostalcode(String gpostalcode) {
		this.gpostalcode = gpostalcode;
	}

	public String getGregion() {
		return gregion;
	}

	public void setGregion(String gregion) {
		this.gregion = gregion;
	}

	public String getGtitle() {
		return gtitle;
	}

	public void setGtitle(String gtitle) {
		this.gtitle = gtitle;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTranslatordetails() {
		return translatordetails;
	}

	public void setTranslatordetails(String translatordetail) {
		this.translatordetails = translatordetail;
	}

	public String getAlternativenumber() {
		return alternativenumber;
	}

	public void setAlternativenumber(String alternativenumber) {
		this.alternativenumber = alternativenumber;
	}

	public String getMedicalevidence() {
		return medicalevidence;
	}

	public void setMedicalevidence(String medicalevidence) {
		this.medicalevidence = medicalevidence;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getYearofmanufacture() {
		return yearofmanufacture;
	}

	public void setYearofmanufacture(String yearofmanufacture) {
		this.yearofmanufacture = yearofmanufacture;
	}

	public String getOtherlanguages() {
		return otherlanguages;
	}

	public void setOtherlanguages(String otherlanguages) {
		this.otherlanguages = otherlanguages;
	}

	public Date getReportedon() {
		return reportedon;
	}

	public void setReportedon(Date reportedon) {
		this.reportedon = reportedon;
	}

	public Date getGdob() {
		return gdob;
	}

	public void setGdob(Date gdob) {
		this.gdob = gdob;
	}

	public Long getAdvisor() {
		return advisor;
	}

	public void setAdvisor(Long advisor) {
		this.advisor = advisor;
	}

	public Long getIntroducer() {
		return introducer;
	}

	public void setIntroducer(Long introducer) {
		this.introducer = introducer;
	}

	public String getAdvisorname() {
		return advisorname;
	}

	public void setAdvisorname(String advisorname) {
		this.advisorname = advisorname;
	}

	public String getIntroducername() {
		return introducername;
	}

	public void setIntroducername(String introducername) {
		this.introducername = introducername;
	}

	public String getReferencenumber() {
		return referencenumber;
	}

	public void setReferencenumber(String referencenumber) {
		this.referencenumber = referencenumber;
	}

	public String getAirbagopened() {
		return airbagopened;
	}

	public void setAirbagopened(String airbagopened) {
		this.airbagopened = airbagopened;
	}

	public Date getLastupdated() {
		return lastupdated;
	}

	public void setLastupdated(Date lastupdated) {
		this.lastupdated = lastupdated;
	}

	public String getLastupdateuser() {
		return lastupdateuser;
	}

	public void setLastupdateuser(String lastupdateuser) {
		this.lastupdateuser = lastupdateuser;
	}

	public List<TblRtainjury> getInjclasscodes() {
		return injclasscodes;
	}

	public void setInjclasscodes(List<TblRtainjury> injclasscodes) {
		this.injclasscodes = injclasscodes;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public Date getClawbackDate() {
		return clawbackDate;
	}

	public void setClawbackDate(Date clawbackDate) {
		this.clawbackDate = clawbackDate;
	}

	public BigDecimal getRtalogcode() {
		return rtalogcode;
	}

	public void setRtalogcode(BigDecimal rtalogcode) {
		this.rtalogcode = rtalogcode;
	}

	public String getVdImages() {
		return vdImages;
	}

	public void setVdImages(String vdImages) {
		this.vdImages = vdImages;
	}

	public String getInjurySustained() {
		return injurySustained;
	}

	public void setInjurySustained(String injurySustained) {
		this.injurySustained = injurySustained;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}
}