package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_HIREBUSINESSES database table.
 */
@Entity
@Table(name = "TBL_HIREBUSINESSES")
@NamedQuery(name = "TblHirebusiness.findAll", query = "SELECT t FROM TblHirebusiness t")
public class TblHirebusiness implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HIREBUSINESSES_HIREBUSINESSESCODE_GENERATOR", sequenceName = "TBL_HIREBUSINESSES_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HIREBUSINESSES_HIREBUSINESSESCODE_GENERATOR")
    private long hirebusinessescode;

    private String createdby;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String message;

    private String note;

    private String status;

    private String statususer;

    @Transient
    private String statususername;
    @Transient
    private String handleremail;

    //bi-directional many-to-one association to TblCompanyprofile
    @ManyToOne
    @JoinColumn(name = "COMPANYCODE")
    private TblCompanyprofile tblCompanyprofile;

    //bi-directional many-to-one association to TblHireclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HIRECODE")
    private TblHireclaim tblHireclaim;

    //bi-directional many-to-one association to TblUser
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "USERCODE")
    private TblUser tblUser;

    //bi-directional many-to-one association to TblHirebusinessdetail
    @OneToMany(mappedBy = "tblHirebusiness")
    private List<TblHirebusinessdetail> tblHirebusinessdetails;

    public TblHirebusiness() {
    }

    public long getHirebusinessescode() {
        return this.hirebusinessescode;
    }

    public void setHirebusinessescode(long hirebusinessescode) {
        this.hirebusinessescode = hirebusinessescode;
    }

    public String getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TblCompanyprofile getTblCompanyprofile() {
        return this.tblCompanyprofile;
    }

    public void setTblCompanyprofile(TblCompanyprofile tblCompanyprofile) {
        this.tblCompanyprofile = tblCompanyprofile;
    }

    public TblHireclaim getTblHireclaim() {
        return this.tblHireclaim;
    }

    public void setTblHireclaim(TblHireclaim tblHireclaim) {
        this.tblHireclaim = tblHireclaim;
    }

    public TblUser getTblUser() {
        return this.tblUser;
    }

    public void setTblUser(TblUser tblUser) {
        this.tblUser = tblUser;
    }

    public List<TblHirebusinessdetail> getTblHirebusinessdetails() {
        return tblHirebusinessdetails;
    }

    public void setTblHirebusinessdetails(List<TblHirebusinessdetail> tblHirebusinessdetails) {
        this.tblHirebusinessdetails = tblHirebusinessdetails;
    }

    public String getStatususer() {
        return statususer;
    }

    public void setStatususer(String statususer) {
        this.statususer = statususer;
    }

    public String getStatususername() {
        return statususername;
    }

    public void setStatususername(String statususername) {
        this.statususername = statususername;
    }

    public String getHandleremail() {
        return handleremail;
    }

    public void setHandleremail(String handleremail) {
        this.handleremail = handleremail;
    }
}