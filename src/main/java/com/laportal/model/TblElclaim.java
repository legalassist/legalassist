package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.laportal.dto.RtaActionButton;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_ELCLAIM database table.
 */
@Entity
@Table(name = "TBL_ELCLAIM")
@NamedQuery(name = "TblElclaim.findAll", query = "SELECT t FROM TblElclaim t")
public class TblElclaim implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_ELCLAIM_ELCLAIMCODE_GENERATOR", sequenceName = "SEQ_TBL_ELCLAIM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_ELCLAIM_ELCLAIMCODE_GENERATOR")
    private long elclaimcode;

    private String accbookcopy;

    private String title;

    private String firstname;

    private String middlename;

    private String lastname;

    private String postalcode;

    private String address1;

    private String address2;

    private String address3;

    private String city;

    private String region;

    private String acccircumstances;

    private Date accdatetime;

    private String acclocation;

    private String additionalclaiminfo;

    private String agencyname;

    private String ambulancecalled;

    private String anyinvestigation;

    private String anyotherlosses;

    private String anypremedicalconds;

    private String anypreviousacc;

    private String anyriskassessments;

    private String anytreatmentreceived;

    private String anywitnesses;

    private String awaresimilaraccident;

    private String changestosystem;

    private String claimantsufferinjuries;

    private String clientattendedhospital;

    private String clientinjuries;

    private String clientpresentcond;

    private String clientseenaccbook;

    private String clientseengp;

    private String clientstillwork;

    private String companysickpay;

    private String contactno;

    private Date createdate;

    private BigDecimal createuser;

    private String dailyactivities;

    @Temporal(TemporalType.DATE)
    private Date dategpattendance;

    @Temporal(TemporalType.DATE)
    private Date datehospitalattendance;

    @Temporal(TemporalType.DATE)
    private Date dob;

    private String elcode;

    private String employeraddress;

    private String employercontactno;

    private String employername;

    private String employmentduration;

    private String esig;

    private Date esigdate;

    private String estimatednetweeklywage;

    private String gpaddress;

    private String gpadvisegiven;

    private String gpname;

    private String hospitaladdress;

    private String hospitaladvisegiven;

    private String hospitalname;

    private String injuriessymptomsstart;

    private String jobtitle;

    private String lossofearnings;

    private String ninumber;

    private String noworkadvised;

    private String otherlossesdetail;

    private String periodofabsence;

    private String premedicalconddetail;

    private String previousaccdetail;

    private String protectiveequipment;

    private String remarks;

    private String reportedbywhom;

    private BigDecimal status;

    private String trainingreceived;

    @Temporal(TemporalType.DATE)
    private Date updatedate;

    private String userofprocequip;

    private String witnessdetails;

    private String acctime;
    private String wasclientwearing;

    //bi-directional many-to-one association to TblEllog
    @OneToMany(mappedBy = "tblElclaim")
    private List<TblEllog> tblEllogs;

    //bi-directional many-to-one association to TblElmessage
    @OneToMany(mappedBy = "tblElclaim")
    private List<TblElmessage> tblElmessages;

    //bi-directional many-to-one association to TblElnote
    @OneToMany(mappedBy = "tblElclaim")
    private List<TblElnote> tblElnotes;

    //bi-directional many-to-one association to TblElsolicitor
    @OneToMany(mappedBy = "tblElclaim")
    private List<TblElsolicitor> tblElsolicitors;


    @Transient
    private List<TblEltask> tblEltasks;

    //bi-directional many-to-one association to TblEldocument
    @OneToMany(mappedBy = "tblElclaim")
    private List<TblEldocument> tblEldocuments;

    @Transient
    private String statusDescr;

    @Transient
    private List<RtaActionButton> elActionButtons;

    @Transient
    private List<RtaActionButton> elActionButtonForLA;

    private Long advisor;
    private Long introducer;

    private String lastupdateuser;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "UTC")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "CLAWBACK_DATE")
    private Date clawbackDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "SUBMIT_DATE")
    private Date submitDate;

    @Column(name = "CLIENTEMAIL")
    private String email;
    @Column(name = "CLIENTMOBILE")
    private String mobile;

    @Transient
    private String solicitorcompany;
    @Transient
    private String solicitorusername;
    @Transient
    private String advisorname;
    @Transient
    private String introducername;
    @Transient
    private Date introducerInvoiceDate;
    @Transient
    private BigDecimal introducerInvoiceHeadId;
    @Transient
    private Date solicitorInvoiceDate;
    @Transient
    private BigDecimal solicitorInvoiceHeadId;
    @Transient
    private TblEsignStatus tblEsignStatus;
    @Transient
    private String taskflag;
    @Transient
    private String editFlag;

    @Transient
    private String isEmployementHistory;

    public TblElclaim() {
    }

    public long getElclaimcode() {
        return this.elclaimcode;
    }

    public void setElclaimcode(long elclaimcode) {
        this.elclaimcode = elclaimcode;
    }

    public String getAccbookcopy() {
        return this.accbookcopy;
    }

    public void setAccbookcopy(String accbookcopy) {
        this.accbookcopy = accbookcopy;
    }

    public String getAcccircumstances() {
        return this.acccircumstances;
    }

    public void setAcccircumstances(String acccircumstances) {
        this.acccircumstances = acccircumstances;
    }

    public Date getAccdatetime() {
        return this.accdatetime;
    }

    public void setAccdatetime(Date accdatetime) {
        this.accdatetime = accdatetime;
    }

    public String getAcclocation() {
        return this.acclocation;
    }

    public void setAcclocation(String acclocation) {
        this.acclocation = acclocation;
    }

    public String getAdditionalclaiminfo() {
        return this.additionalclaiminfo;
    }

    public void setAdditionalclaiminfo(String additionalclaiminfo) {
        this.additionalclaiminfo = additionalclaiminfo;
    }


    public String getAgencyname() {
        return this.agencyname;
    }

    public void setAgencyname(String agencyname) {
        this.agencyname = agencyname;
    }

    public String getAmbulancecalled() {
        return this.ambulancecalled;
    }

    public void setAmbulancecalled(String ambulancecalled) {
        this.ambulancecalled = ambulancecalled;
    }

    public String getAnyinvestigation() {
        return this.anyinvestigation;
    }

    public void setAnyinvestigation(String anyinvestigation) {
        this.anyinvestigation = anyinvestigation;
    }

    public String getAnyotherlosses() {
        return this.anyotherlosses;
    }

    public void setAnyotherlosses(String anyotherlosses) {
        this.anyotherlosses = anyotherlosses;
    }

    public String getAnypremedicalconds() {
        return this.anypremedicalconds;
    }

    public void setAnypremedicalconds(String anypremedicalconds) {
        this.anypremedicalconds = anypremedicalconds;
    }

    public String getAnypreviousacc() {
        return this.anypreviousacc;
    }

    public void setAnypreviousacc(String anypreviousacc) {
        this.anypreviousacc = anypreviousacc;
    }

    public String getAnyriskassessments() {
        return this.anyriskassessments;
    }

    public void setAnyriskassessments(String anyriskassessments) {
        this.anyriskassessments = anyriskassessments;
    }

    public String getAnytreatmentreceived() {
        return this.anytreatmentreceived;
    }

    public void setAnytreatmentreceived(String anytreatmentreceived) {
        this.anytreatmentreceived = anytreatmentreceived;
    }

    public String getAnywitnesses() {
        return this.anywitnesses;
    }

    public void setAnywitnesses(String anywitnesses) {
        this.anywitnesses = anywitnesses;
    }

    public String getAwaresimilaraccident() {
        return this.awaresimilaraccident;
    }

    public void setAwaresimilaraccident(String awaresimilaraccident) {
        this.awaresimilaraccident = awaresimilaraccident;
    }

    public String getChangestosystem() {
        return this.changestosystem;
    }

    public void setChangestosystem(String changestosystem) {
        this.changestosystem = changestosystem;
    }

    public String getClaimantsufferinjuries() {
        return this.claimantsufferinjuries;
    }

    public void setClaimantsufferinjuries(String claimantsufferinjuries) {
        this.claimantsufferinjuries = claimantsufferinjuries;
    }

    public String getClientattendedhospital() {
        return this.clientattendedhospital;
    }

    public void setClientattendedhospital(String clientattendedhospital) {
        this.clientattendedhospital = clientattendedhospital;
    }

    public String getClientinjuries() {
        return this.clientinjuries;
    }

    public void setClientinjuries(String clientinjuries) {
        this.clientinjuries = clientinjuries;
    }


    public String getClientpresentcond() {
        return this.clientpresentcond;
    }

    public void setClientpresentcond(String clientpresentcond) {
        this.clientpresentcond = clientpresentcond;
    }

    public String getClientseenaccbook() {
        return this.clientseenaccbook;
    }

    public void setClientseenaccbook(String clientseenaccbook) {
        this.clientseenaccbook = clientseenaccbook;
    }

    public String getClientseengp() {
        return this.clientseengp;
    }

    public void setClientseengp(String clientseengp) {
        this.clientseengp = clientseengp;
    }

    public String getClientstillwork() {
        return this.clientstillwork;
    }

    public void setClientstillwork(String clientstillwork) {
        this.clientstillwork = clientstillwork;
    }

    public String getCompanysickpay() {
        return this.companysickpay;
    }

    public void setCompanysickpay(String companysickpay) {
        this.companysickpay = companysickpay;
    }

    public String getContactno() {
        return this.contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public String getDailyactivities() {
        return this.dailyactivities;
    }

    public void setDailyactivities(String dailyactivities) {
        this.dailyactivities = dailyactivities;
    }

    public Date getDategpattendance() {
        return this.dategpattendance;
    }

    public void setDategpattendance(Date dategpattendance) {
        this.dategpattendance = dategpattendance;
    }

    public Date getDatehospitalattendance() {
        return this.datehospitalattendance;
    }

    public void setDatehospitalattendance(Date datehospitalattendance) {
        this.datehospitalattendance = datehospitalattendance;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getElcode() {
        return this.elcode;
    }

    public void setElcode(String elcode) {
        this.elcode = elcode;
    }

    public String getEmployeraddress() {
        return this.employeraddress;
    }

    public void setEmployeraddress(String employeraddress) {
        this.employeraddress = employeraddress;
    }

    public String getEmployercontactno() {
        return this.employercontactno;
    }

    public void setEmployercontactno(String employercontactno) {
        this.employercontactno = employercontactno;
    }

    public String getEmployername() {
        return this.employername;
    }

    public void setEmployername(String employername) {
        this.employername = employername;
    }

    public String getEmploymentduration() {
        return this.employmentduration;
    }

    public void setEmploymentduration(String employmentduration) {
        this.employmentduration = employmentduration;
    }

    public String getEsig() {
        return this.esig;
    }

    public void setEsig(String esig) {
        this.esig = esig;
    }

    public Date getEsigdate() {
        return this.esigdate;
    }

    public void setEsigdate(Date esigdate) {
        this.esigdate = esigdate;
    }

    public String getEstimatednetweeklywage() {
        return this.estimatednetweeklywage;
    }

    public void setEstimatednetweeklywage(String estimatednetweeklywage) {
        this.estimatednetweeklywage = estimatednetweeklywage;
    }

    public String getGpaddress() {
        return this.gpaddress;
    }

    public void setGpaddress(String gpaddress) {
        this.gpaddress = gpaddress;
    }

    public String getGpadvisegiven() {
        return this.gpadvisegiven;
    }

    public void setGpadvisegiven(String gpadvisegiven) {
        this.gpadvisegiven = gpadvisegiven;
    }

    public String getGpname() {
        return this.gpname;
    }

    public void setGpname(String gpname) {
        this.gpname = gpname;
    }

    public String getHospitaladdress() {
        return this.hospitaladdress;
    }

    public void setHospitaladdress(String hospitaladdress) {
        this.hospitaladdress = hospitaladdress;
    }

    public String getHospitaladvisegiven() {
        return this.hospitaladvisegiven;
    }

    public void setHospitaladvisegiven(String hospitaladvisegiven) {
        this.hospitaladvisegiven = hospitaladvisegiven;
    }

    public String getHospitalname() {
        return this.hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname;
    }

    public String getInjuriessymptomsstart() {
        return this.injuriessymptomsstart;
    }

    public void setInjuriessymptomsstart(String injuriessymptomsstart) {
        this.injuriessymptomsstart = injuriessymptomsstart;
    }

    public String getJobtitle() {
        return this.jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public String getLossofearnings() {
        return this.lossofearnings;
    }

    public void setLossofearnings(String lossofearnings) {
        this.lossofearnings = lossofearnings;
    }

    public String getNinumber() {
        return this.ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getNoworkadvised() {
        return this.noworkadvised;
    }

    public void setNoworkadvised(String noworkadvised) {
        this.noworkadvised = noworkadvised;
    }

    public String getOtherlossesdetail() {
        return this.otherlossesdetail;
    }

    public void setOtherlossesdetail(String otherlossesdetail) {
        this.otherlossesdetail = otherlossesdetail;
    }

    public String getPeriodofabsence() {
        return this.periodofabsence;
    }

    public void setPeriodofabsence(String periodofabsence) {
        this.periodofabsence = periodofabsence;
    }

    public String getPremedicalconddetail() {
        return this.premedicalconddetail;
    }

    public void setPremedicalconddetail(String premedicalconddetail) {
        this.premedicalconddetail = premedicalconddetail;
    }

    public String getPreviousaccdetail() {
        return this.previousaccdetail;
    }

    public void setPreviousaccdetail(String previousaccdetail) {
        this.previousaccdetail = previousaccdetail;
    }

    public String getProtectiveequipment() {
        return this.protectiveequipment;
    }

    public void setProtectiveequipment(String protectiveequipment) {
        this.protectiveequipment = protectiveequipment;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getReportedbywhom() {
        return this.reportedbywhom;
    }

    public void setReportedbywhom(String reportedbywhom) {
        this.reportedbywhom = reportedbywhom;
    }

    public BigDecimal getStatus() {
        return this.status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    public String getTrainingreceived() {
        return this.trainingreceived;
    }

    public void setTrainingreceived(String trainingreceived) {
        this.trainingreceived = trainingreceived;
    }

    public Date getUpdatedate() {
        return this.updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public String getUserofprocequip() {
        return this.userofprocequip;
    }

    public void setUserofprocequip(String userofprocequip) {
        this.userofprocequip = userofprocequip;
    }

    public String getWitnessdetails() {
        return this.witnessdetails;
    }

    public void setWitnessdetails(String witnessdetails) {
        this.witnessdetails = witnessdetails;
    }

    public List<TblEllog> getTblEllogs() {
        return this.tblEllogs;
    }

    public void setTblEllogs(List<TblEllog> tblEllogs) {
        this.tblEllogs = tblEllogs;
    }

    public TblEllog addTblEllog(TblEllog tblEllog) {
        getTblEllogs().add(tblEllog);
        tblEllog.setTblElclaim(this);

        return tblEllog;
    }

    public TblEllog removeTblEllog(TblEllog tblEllog) {
        getTblEllogs().remove(tblEllog);
        tblEllog.setTblElclaim(null);

        return tblEllog;
    }

    public List<TblElmessage> getTblElmessages() {
        return this.tblElmessages;
    }

    public void setTblElmessages(List<TblElmessage> tblElmessages) {
        this.tblElmessages = tblElmessages;
    }

    public TblElmessage addTblElmessage(TblElmessage tblElmessage) {
        getTblElmessages().add(tblElmessage);
        tblElmessage.setTblElclaim(this);

        return tblElmessage;
    }

    public TblElmessage removeTblElmessage(TblElmessage tblElmessage) {
        getTblElmessages().remove(tblElmessage);
        tblElmessage.setTblElclaim(null);

        return tblElmessage;
    }

    public List<TblElnote> getTblElnotes() {
        return this.tblElnotes;
    }

    public void setTblElnotes(List<TblElnote> tblElnotes) {
        this.tblElnotes = tblElnotes;
    }

    public TblElnote addTblElnote(TblElnote tblElnote) {
        getTblElnotes().add(tblElnote);
        tblElnote.setTblElclaim(this);

        return tblElnote;
    }

    public TblElnote removeTblElnote(TblElnote tblElnote) {
        getTblElnotes().remove(tblElnote);
        tblElnote.setTblElclaim(null);

        return tblElnote;
    }

    public List<TblElsolicitor> getTblElsolicitors() {
        return this.tblElsolicitors;
    }

    public void setTblElsolicitors(List<TblElsolicitor> tblElsolicitors) {
        this.tblElsolicitors = tblElsolicitors;
    }

    public TblElsolicitor addTblElsolicitor(TblElsolicitor tblElsolicitor) {
        getTblElsolicitors().add(tblElsolicitor);
        tblElsolicitor.setTblElclaim(this);

        return tblElsolicitor;
    }

    public TblElsolicitor removeTblElsolicitor(TblElsolicitor tblElsolicitor) {
        getTblElsolicitors().remove(tblElsolicitor);
        tblElsolicitor.setTblElclaim(null);

        return tblElsolicitor;
    }

    public List<TblEltask> getTblEltasks() {
        return this.tblEltasks;
    }

    public void setTblEltasks(List<TblEltask> tblEltasks) {
        this.tblEltasks = tblEltasks;
    }

    public TblEltask addTblEltask(TblEltask tblEltask) {
        getTblEltasks().add(tblEltask);
        tblEltask.setTblElclaim(this);

        return tblEltask;
    }

    public TblEltask removeTblEltask(TblEltask tblEltask) {
        getTblEltasks().remove(tblEltask);
        tblEltask.setTblElclaim(null);

        return tblEltask;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }

    public List<RtaActionButton> getElActionButtons() {
        return elActionButtons;
    }

    public void setElActionButtons(List<RtaActionButton> elActionButtons) {
        this.elActionButtons = elActionButtons;
    }

    public List<RtaActionButton> getElActionButtonForLA() {
        return elActionButtonForLA;
    }

    public void setElActionButtonForLA(List<RtaActionButton> elActionButtonForLA) {
        this.elActionButtonForLA = elActionButtonForLA;
    }

    public List<TblEldocument> getTblEldocuments() {
        return tblEldocuments;
    }

    public void setTblEldocuments(List<TblEldocument> tblEldocuments) {
        this.tblEldocuments = tblEldocuments;
    }

    public Long getAdvisor() {
        return advisor;
    }

    public void setAdvisor(Long advisor) {
        this.advisor = advisor;
    }

    public Long getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Long introducer) {
        this.introducer = introducer;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }

    public Date getClawbackDate() {
        return clawbackDate;
    }

    public void setClawbackDate(Date clawbackDate) {
        this.clawbackDate = clawbackDate;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setSolicitorcompany(String solicitorcompany) {
        this.solicitorcompany = solicitorcompany;
    }

    public String getSolicitorcompany() {
        return solicitorcompany;
    }

    public void setSolicitorusername(String solicitorusername) {
        this.solicitorusername = solicitorusername;
    }

    public String getSolicitorusername() {
        return solicitorusername;
    }

    public void setAdvisorname(String advisorname) {
        this.advisorname = advisorname;
    }

    public String getAdvisorname() {
        return advisorname;
    }

    public void setIntroducername(String introducername) {
        this.introducername = introducername;
    }

    public String getIntroducername() {
        return introducername;
    }

    public void setIntroducerInvoiceDate(Date introducerInvoiceDate) {
        this.introducerInvoiceDate = introducerInvoiceDate;
    }

    public Date getIntroducerInvoiceDate() {
        return introducerInvoiceDate;
    }

    public void setIntroducerInvoiceHeadId(BigDecimal introducerInvoiceHeadId) {
        this.introducerInvoiceHeadId = introducerInvoiceHeadId;
    }

    public BigDecimal getIntroducerInvoiceHeadId() {
        return introducerInvoiceHeadId;
    }

    public void setSolicitorInvoiceDate(Date solicitorInvoiceDate) {
        this.solicitorInvoiceDate = solicitorInvoiceDate;
    }

    public Date getSolicitorInvoiceDate() {
        return solicitorInvoiceDate;
    }

    public void setSolicitorInvoiceHeadId(BigDecimal solicitorInvoiceHeadId) {
        this.solicitorInvoiceHeadId = solicitorInvoiceHeadId;
    }

    public BigDecimal getSolicitorInvoiceHeadId() {
        return solicitorInvoiceHeadId;
    }

    public void setTblEsignStatus(TblEsignStatus tblEsignStatus) {
        this.tblEsignStatus = tblEsignStatus;
    }

    public TblEsignStatus getTblEsignStatus() {
        return tblEsignStatus;
    }


    public String getTaskflag() {
        return taskflag;
    }

    public void setTaskflag(String taskflag) {
        this.taskflag = taskflag;
    }

    public String getEditFlag() {
        return editFlag;
    }

    public void setEditFlag(String editFlag) {
        this.editFlag = editFlag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getIsEmployementHistory() {
        return isEmployementHistory;
    }

    public void setIsEmployementHistory(String isEmployementHistory) {
        this.isEmployementHistory = isEmployementHistory;
    }

    public String getAcctime() {
        return acctime;
    }

    public void setAcctime(String acctime) {
        this.acctime = acctime;
    }

    public String getWasclientwearing() {
        return wasclientwearing;
    }

    public void setWasclientwearing(String wasclientwearing) {
        this.wasclientwearing = wasclientwearing;
    }
}