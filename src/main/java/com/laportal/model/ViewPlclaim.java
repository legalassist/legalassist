package com.laportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VIEW_PLCLAIM database table.
 * 
 */
@Entity
@Table(name="VIEW_PLCLAIM")
@NamedQuery(name="ViewPlclaim.findAll", query="SELECT v FROM ViewPlclaim v")
public class ViewPlclaim implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	private Date accdatetime;

	private String accdescription;

	private String acclocation;

	private String accreportedto;

	private String acctime;

	private String address1;

	private String address2;

	private String address3;

	private BigDecimal advisor;

	private String city;

	@Temporal(TemporalType.DATE)
	@Column(name="CLAWBACK_DATE")
	private Date clawbackDate;

	@Temporal(TemporalType.DATE)
	private Date createdate;

	private BigDecimal createuser;

	private String describeinjuries;

	@Temporal(TemporalType.DATE)
	private Date dob;

	private String esig;

	@Temporal(TemporalType.DATE)
	private Date esigdate;

	private String esignstatus;

	private String firstname;

	private String injuriesduration;

	private BigDecimal introducer;

	@Column(name="INTRODUCER_COMPANY")
	private String introducerCompany;

	@Column(name="INTRODUCER_USER")
	private String introducerUser;

	@Temporal(TemporalType.DATE)
	@Column(name="INVOICE_DATE")
	private Date invoiceDate;

	@Column(name="IS_INTRODUCER_INVOICED")
	private String isIntroducerInvoiced;

	@Column(name="IS_SOLICITOR_INVOICED")
	private String isSolicitorInvoiced;

	private String landline;

	private String lastname;

	private String lastupdateuser;

	private String medicalattention;

	private String middlename;

	private String mobile;

	private String ninumber;

	private String occupation;

	private String password;

	@Id
	private BigDecimal plclaimcode;

	private String plcode;

	private BigDecimal pltaskcode;

	private String pltaskname;

	private String postalcode;

	private String region;

	private String remarks;

	@Column(name="SOLICITOR_COMPANY")
	private String solicitorCompany;

	@Column(name="SOLICITOR_COMPANYCODE")
	private String solicitorCompanycode;

	@Column(name="SOLICITOR_USER")
	private String solicitorUser;

	@Column(name="SOLICITOR_USERCODE")
	private BigDecimal solicitorUsercode;

	private String status;

	private BigDecimal statuscode;

	@Temporal(TemporalType.DATE)
	@Column(name="SUBMIT_DATE")
	private Date submitDate;

	private String taskflag;

	private String title;

	@Temporal(TemporalType.DATE)
	private Date updatedate;

	private String witnesses;

	public ViewPlclaim() {
	}

	public Date getAccdatetime() {
		return this.accdatetime;
	}

	public void setAccdatetime(Date accdatetime) {
		this.accdatetime = accdatetime;
	}

	public String getAccdescription() {
		return this.accdescription;
	}

	public void setAccdescription(String accdescription) {
		this.accdescription = accdescription;
	}

	public String getAcclocation() {
		return this.acclocation;
	}

	public void setAcclocation(String acclocation) {
		this.acclocation = acclocation;
	}

	public String getAccreportedto() {
		return this.accreportedto;
	}

	public void setAccreportedto(String accreportedto) {
		this.accreportedto = accreportedto;
	}

	public String getAcctime() {
		return this.acctime;
	}

	public void setAcctime(String acctime) {
		this.acctime = acctime;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public BigDecimal getAdvisor() {
		return this.advisor;
	}

	public void setAdvisor(BigDecimal advisor) {
		this.advisor = advisor;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getClawbackDate() {
		return this.clawbackDate;
	}

	public void setClawbackDate(Date clawbackDate) {
		this.clawbackDate = clawbackDate;
	}

	public Date getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public BigDecimal getCreateuser() {
		return this.createuser;
	}

	public void setCreateuser(BigDecimal createuser) {
		this.createuser = createuser;
	}

	public String getDescribeinjuries() {
		return this.describeinjuries;
	}

	public void setDescribeinjuries(String describeinjuries) {
		this.describeinjuries = describeinjuries;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEsig() {
		return this.esig;
	}

	public void setEsig(String esig) {
		this.esig = esig;
	}

	public Date getEsigdate() {
		return this.esigdate;
	}

	public void setEsigdate(Date esigdate) {
		this.esigdate = esigdate;
	}

	public String getEsignstatus() {
		return this.esignstatus;
	}

	public void setEsignstatus(String esignstatus) {
		this.esignstatus = esignstatus;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getInjuriesduration() {
		return this.injuriesduration;
	}

	public void setInjuriesduration(String injuriesduration) {
		this.injuriesduration = injuriesduration;
	}

	public BigDecimal getIntroducer() {
		return this.introducer;
	}

	public void setIntroducer(BigDecimal introducer) {
		this.introducer = introducer;
	}

	public String getIntroducerCompany() {
		return this.introducerCompany;
	}

	public void setIntroducerCompany(String introducerCompany) {
		this.introducerCompany = introducerCompany;
	}

	public String getIntroducerUser() {
		return this.introducerUser;
	}

	public void setIntroducerUser(String introducerUser) {
		this.introducerUser = introducerUser;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getIsIntroducerInvoiced() {
		return this.isIntroducerInvoiced;
	}

	public void setIsIntroducerInvoiced(String isIntroducerInvoiced) {
		this.isIntroducerInvoiced = isIntroducerInvoiced;
	}

	public String getIsSolicitorInvoiced() {
		return this.isSolicitorInvoiced;
	}

	public void setIsSolicitorInvoiced(String isSolicitorInvoiced) {
		this.isSolicitorInvoiced = isSolicitorInvoiced;
	}

	public String getLandline() {
		return this.landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLastupdateuser() {
		return this.lastupdateuser;
	}

	public void setLastupdateuser(String lastupdateuser) {
		this.lastupdateuser = lastupdateuser;
	}

	public String getMedicalattention() {
		return this.medicalattention;
	}

	public void setMedicalattention(String medicalattention) {
		this.medicalattention = medicalattention;
	}

	public String getMiddlename() {
		return this.middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getNinumber() {
		return this.ninumber;
	}

	public void setNinumber(String ninumber) {
		this.ninumber = ninumber;
	}

	public String getOccupation() {
		return this.occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BigDecimal getPlclaimcode() {
		return this.plclaimcode;
	}

	public void setPlclaimcode(BigDecimal plclaimcode) {
		this.plclaimcode = plclaimcode;
	}

	public String getPlcode() {
		return this.plcode;
	}

	public void setPlcode(String plcode) {
		this.plcode = plcode;
	}

	public BigDecimal getPltaskcode() {
		return this.pltaskcode;
	}

	public void setPltaskcode(BigDecimal pltaskcode) {
		this.pltaskcode = pltaskcode;
	}

	public String getPltaskname() {
		return this.pltaskname;
	}

	public void setPltaskname(String pltaskname) {
		this.pltaskname = pltaskname;
	}

	public String getPostalcode() {
		return this.postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSolicitorCompany() {
		return this.solicitorCompany;
	}

	public void setSolicitorCompany(String solicitorCompany) {
		this.solicitorCompany = solicitorCompany;
	}

	public String getSolicitorCompanycode() {
		return this.solicitorCompanycode;
	}

	public void setSolicitorCompanycode(String solicitorCompanycode) {
		this.solicitorCompanycode = solicitorCompanycode;
	}

	public String getSolicitorUser() {
		return this.solicitorUser;
	}

	public void setSolicitorUser(String solicitorUser) {
		this.solicitorUser = solicitorUser;
	}

	public BigDecimal getSolicitorUsercode() {
		return this.solicitorUsercode;
	}

	public void setSolicitorUsercode(BigDecimal solicitorUsercode) {
		this.solicitorUsercode = solicitorUsercode;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getStatuscode() {
		return this.statuscode;
	}

	public void setStatuscode(BigDecimal statuscode) {
		this.statuscode = statuscode;
	}

	public Date getSubmitDate() {
		return this.submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public String getTaskflag() {
		return this.taskflag;
	}

	public void setTaskflag(String taskflag) {
		this.taskflag = taskflag;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getUpdatedate() {
		return this.updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public String getWitnesses() {
		return this.witnesses;
	}

	public void setWitnesses(String witnesses) {
		this.witnesses = witnesses;
	}

}