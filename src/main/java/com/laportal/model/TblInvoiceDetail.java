package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_INVOICE_DETAIL database table.
 */
@Entity
@Table(name = "TBL_INVOICE_DETAIL")
@NamedQuery(name = "TblInvoiceDetail.findAll", query = "SELECT t FROM TblInvoiceDetail t")
public class TblInvoiceDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_INVOICE_DETAIL_INVOICEDETAILID_GENERATOR", sequenceName = "TBL_INVOICE_DETAIL_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_INVOICE_DETAIL_INVOICEDETAILID_GENERATOR")
    @Column(name = "INVOICE_DETAIL_ID")
    private long invoiceDetailId;

    private Long adjust;

    private Long charge;

    @Temporal(TemporalType.DATE)
    private Date createdate;

    private Long createuser;

    @Temporal(TemporalType.DATE)
    private Date updatedate;

    private Long updateuser;

    private String description;

    //bi-directional many-to-one association to TblSolicitorInvoiceHead
    @ManyToOne
    @JoinColumn(name = "INVOICEHEADID")
    private TblSolicitorInvoiceHead tblSolicitorInvoiceHead;

    public TblInvoiceDetail() {
    }

    public long getInvoiceDetailId() {
        return this.invoiceDetailId;
    }

    public void setInvoiceDetailId(long invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public Long getAdjust() {
        return this.adjust;
    }

    public void setAdjust(Long adjust) {
        this.adjust = adjust;
    }


    public Long getCharge() {
        return this.charge;
    }

    public void setCharge(Long charge) {
        this.charge = charge;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Long getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(Long createuser) {
        this.createuser = createuser;
    }

    public Date getUpdatedate() {
        return this.updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public Long getUpdateuser() {
        return this.updateuser;
    }

    public void setUpdateuser(Long updateuser) {
        this.updateuser = updateuser;
    }

    public TblSolicitorInvoiceHead getTblSolicitorInvoiceHead() {
        return this.tblSolicitorInvoiceHead;
    }

    public void setTblSolicitorInvoiceHead(TblSolicitorInvoiceHead tblSolicitorInvoiceHead) {
        this.tblSolicitorInvoiceHead = tblSolicitorInvoiceHead;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}