package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.laportal.dto.HdrActionButton;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_IMMIGRATIONCLAIM database table.
 */
@Entity
@Table(name = "TBL_IMMIGRATIONCLAIM")
@NamedQuery(name = "TblImmigrationclaim.findAll", query = "SELECT t FROM TblImmigrationclaim t")
public class TblImmigrationclaim implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_IMMIGRATIONCLAIM_IMMIGRATIONCLAIMCODE_GENERATOR", sequenceName = "SEQ_TBL_IMMIGRATIONCLAIM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_IMMIGRATIONCLAIM_IMMIGRATIONCLAIMCODE_GENERATOR")
    private long immigrationclaimcode;

    private String advice;

    private String clientaddress;

    private String clientcontactno;

    private String clientemail;

    private String clientname;

    private String clienttitle;

    private String contacttime;

    @Temporal(TemporalType.DATE)
    private Date createdate;

    private BigDecimal createuser;

    private String esign;

    @Temporal(TemporalType.DATE)
    private Date esigndate;

    private String immigrationcode;

    @Temporal(TemporalType.DATE)
    private Date inquirydatetime;

    private String inquirydescription;

    private String inquirynature;

    private String inquiryreferal;

    private String nationality;

    private String notes;

    private String remarks;

    private BigDecimal status;

    @Temporal(TemporalType.DATE)
    private Date updatedate;

    //bi-directional many-to-one association to TblImmigrationdocument
    @OneToMany(mappedBy = "tblImmigrationclaim")
    private List<TblImmigrationdocument> tblImmigrationdocuments;

    //bi-directional many-to-one association to TblImmigrationlog
    @OneToMany(mappedBy = "tblImmigrationclaim")
    private List<TblImmigrationlog> tblImmigrationlogs;

    //bi-directional many-to-one association to TblImmigrationnote
    @OneToMany(mappedBy = "tblImmigrationclaim")
    private List<TblImmigrationnote> tblImmigrationnotes;

    //bi-directional many-to-one association to TblImmigrationsolicitor
    @OneToMany(mappedBy = "tblImmigrationclaim")
    private List<TblImmigrationsolicitor> tblImmigrationsolicitors;

    //bi-directional many-to-one association to TblImmigrationtask
    @OneToMany(mappedBy = "tblImmigrationclaim")
    private List<TblImmigrationtask> tblImmigrationtasks;

    //bi-directional many-to-one association to TblImmigrationmessage
    @JsonIgnore
    @OneToMany(mappedBy = "tblImmigrationclaim")
    private List<TblImmigrationmessage> tblImmigrationmessages;

    @Transient
    private String statusDescr;

    @Transient
    private List<HdrActionButton> hdrActionButton;

    @Transient
    private List<HdrActionButton> hdrActionButtonForLA;

    public TblImmigrationclaim() {
    }

    public long getImmigrationclaimcode() {
        return this.immigrationclaimcode;
    }

    public void setImmigrationclaimcode(long immigrationclaimcode) {
        this.immigrationclaimcode = immigrationclaimcode;
    }

    public String getAdvice() {
        return this.advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public String getClientaddress() {
        return this.clientaddress;
    }

    public void setClientaddress(String clientaddress) {
        this.clientaddress = clientaddress;
    }

    public String getClientcontactno() {
        return this.clientcontactno;
    }

    public void setClientcontactno(String clientcontactno) {
        this.clientcontactno = clientcontactno;
    }

    public String getClientemail() {
        return this.clientemail;
    }

    public void setClientemail(String clientemail) {
        this.clientemail = clientemail;
    }

    public String getClientname() {
        return this.clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public String getClienttitle() {
        return this.clienttitle;
    }

    public void setClienttitle(String clienttitle) {
        this.clienttitle = clienttitle;
    }

    public String getContacttime() {
        return this.contacttime;
    }

    public void setContacttime(String contacttime) {
        this.contacttime = contacttime;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public String getEsign() {
        return this.esign;
    }

    public void setEsign(String esign) {
        this.esign = esign;
    }

    public Date getEsigndate() {
        return this.esigndate;
    }

    public void setEsigndate(Date esigndate) {
        this.esigndate = esigndate;
    }

    public String getImmigrationcode() {
        return this.immigrationcode;
    }

    public void setImmigrationcode(String immigrationcode) {
        this.immigrationcode = immigrationcode;
    }

    public Date getInquirydatetime() {
        return this.inquirydatetime;
    }

    public void setInquirydatetime(Date inquirydatetime) {
        this.inquirydatetime = inquirydatetime;
    }

    public String getInquirydescription() {
        return this.inquirydescription;
    }

    public void setInquirydescription(String inquirydescription) {
        this.inquirydescription = inquirydescription;
    }

    public String getInquirynature() {
        return this.inquirynature;
    }

    public void setInquirynature(String inquirynature) {
        this.inquirynature = inquirynature;
    }

    public String getInquiryreferal() {
        return this.inquiryreferal;
    }

    public void setInquiryreferal(String inquiryreferal) {
        this.inquiryreferal = inquiryreferal;
    }

    public String getNationality() {
        return this.nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getStatus() {
        return this.status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    public Date getUpdatedate() {
        return this.updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public List<TblImmigrationdocument> getTblImmigrationdocuments() {
        return this.tblImmigrationdocuments;
    }

    public void setTblImmigrationdocuments(List<TblImmigrationdocument> tblImmigrationdocuments) {
        this.tblImmigrationdocuments = tblImmigrationdocuments;
    }

    public TblImmigrationdocument addTblImmigrationdocument(TblImmigrationdocument tblImmigrationdocument) {
        getTblImmigrationdocuments().add(tblImmigrationdocument);
        tblImmigrationdocument.setTblImmigrationclaim(this);

        return tblImmigrationdocument;
    }

    public TblImmigrationdocument removeTblImmigrationdocument(TblImmigrationdocument tblImmigrationdocument) {
        getTblImmigrationdocuments().remove(tblImmigrationdocument);
        tblImmigrationdocument.setTblImmigrationclaim(null);

        return tblImmigrationdocument;
    }

    public List<TblImmigrationlog> getTblImmigrationlogs() {
        return this.tblImmigrationlogs;
    }

    public void setTblImmigrationlogs(List<TblImmigrationlog> tblImmigrationlogs) {
        this.tblImmigrationlogs = tblImmigrationlogs;
    }

    public TblImmigrationlog addTblImmigrationlog(TblImmigrationlog tblImmigrationlog) {
        getTblImmigrationlogs().add(tblImmigrationlog);
        tblImmigrationlog.setTblImmigrationclaim(this);

        return tblImmigrationlog;
    }

    public TblImmigrationlog removeTblImmigrationlog(TblImmigrationlog tblImmigrationlog) {
        getTblImmigrationlogs().remove(tblImmigrationlog);
        tblImmigrationlog.setTblImmigrationclaim(null);

        return tblImmigrationlog;
    }

    public List<TblImmigrationnote> getTblImmigrationnotes() {
        return this.tblImmigrationnotes;
    }

    public void setTblImmigrationnotes(List<TblImmigrationnote> tblImmigrationnotes) {
        this.tblImmigrationnotes = tblImmigrationnotes;
    }

    public TblImmigrationnote addTblImmigrationnote(TblImmigrationnote tblImmigrationnote) {
        getTblImmigrationnotes().add(tblImmigrationnote);
        tblImmigrationnote.setTblImmigrationclaim(this);

        return tblImmigrationnote;
    }

    public TblImmigrationnote removeTblImmigrationnote(TblImmigrationnote tblImmigrationnote) {
        getTblImmigrationnotes().remove(tblImmigrationnote);
        tblImmigrationnote.setTblImmigrationclaim(null);

        return tblImmigrationnote;
    }

    public List<TblImmigrationsolicitor> getTblImmigrationsolicitors() {
        return this.tblImmigrationsolicitors;
    }

    public void setTblImmigrationsolicitors(List<TblImmigrationsolicitor> tblImmigrationsolicitors) {
        this.tblImmigrationsolicitors = tblImmigrationsolicitors;
    }

    public TblImmigrationsolicitor addTblImmigrationsolicitor(TblImmigrationsolicitor tblImmigrationsolicitor) {
        getTblImmigrationsolicitors().add(tblImmigrationsolicitor);
        tblImmigrationsolicitor.setTblImmigrationclaim(this);

        return tblImmigrationsolicitor;
    }

    public TblImmigrationsolicitor removeTblImmigrationsolicitor(TblImmigrationsolicitor tblImmigrationsolicitor) {
        getTblImmigrationsolicitors().remove(tblImmigrationsolicitor);
        tblImmigrationsolicitor.setTblImmigrationclaim(null);

        return tblImmigrationsolicitor;
    }

    public List<TblImmigrationtask> getTblImmigrationtasks() {
        return this.tblImmigrationtasks;
    }

    public void setTblImmigrationtasks(List<TblImmigrationtask> tblImmigrationtasks) {
        this.tblImmigrationtasks = tblImmigrationtasks;
    }

    public TblImmigrationtask addTblImmigrationtask(TblImmigrationtask tblImmigrationtask) {
        getTblImmigrationtasks().add(tblImmigrationtask);
        tblImmigrationtask.setTblImmigrationclaim(this);

        return tblImmigrationtask;
    }

    public TblImmigrationtask removeTblImmigrationtask(TblImmigrationtask tblImmigrationtask) {
        getTblImmigrationtasks().remove(tblImmigrationtask);
        tblImmigrationtask.setTblImmigrationclaim(null);

        return tblImmigrationtask;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }

    public List<HdrActionButton> getHdrActionButton() {
        return hdrActionButton;
    }

    public void setHdrActionButton(List<HdrActionButton> hdrActionButton) {
        this.hdrActionButton = hdrActionButton;
    }

    public List<HdrActionButton> getHdrActionButtonForLA() {
        return hdrActionButtonForLA;
    }

    public void setHdrActionButtonForLA(List<HdrActionButton> hdrActionButtonForLA) {
        this.hdrActionButtonForLA = hdrActionButtonForLA;
    }

    public List<TblImmigrationmessage> getTblImmigrationmessages() {
        return tblImmigrationmessages;
    }

    public void setTblImmigrationmessages(List<TblImmigrationmessage> tblImmigrationmessages) {
        this.tblImmigrationmessages = tblImmigrationmessages;
    }

}