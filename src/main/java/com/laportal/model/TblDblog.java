package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_DBLOGS database table.
 */
@Entity
@Table(name = "TBL_DBLOGS")
@NamedQuery(name = "TblDblog.findAll", query = "SELECT t FROM TblDblog t")
public class TblDblog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_DBLOGS_DBLOGCODE_GENERATOR", sequenceName = "SEQ_TBL_DBLOGS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_DBLOGS_DBLOGCODE_GENERATOR")
    private long dblogcode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblDbclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "DBCLAIMCODE")
    private TblDbclaim tblDbclaim;

    @Transient
    private String userName;

    public TblDblog() {
    }

    public long getDblogcode() {
        return this.dblogcode;
    }

    public void setDblogcode(long dblogcode) {
        this.dblogcode = dblogcode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblDbclaim getTblDbclaim() {
        return this.tblDbclaim;
    }

    public void setTblDbclaim(TblDbclaim tblDbclaim) {
        this.tblDbclaim = tblDbclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}