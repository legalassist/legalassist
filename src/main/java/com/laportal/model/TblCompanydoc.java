package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_COMPANYDOCS database table.
 */
@Entity
@Table(name = "TBL_COMPANYDOCS")
@NamedQuery(name = "TblCompanydoc.findAll", query = "SELECT t FROM TblCompanydoc t")
public class TblCompanydoc implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_COMPANYDOCS_COMPANYDOCSCODE_GENERATOR", sequenceName = "TBL_COMPANYDOCS_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_COMPANYDOCS_COMPANYDOCSCODE_GENERATOR")
    private long companydocscode;

    private String agenature;

    private String countrytype;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String path;

    //bi-directional many-to-one association to TblCompanyprofile
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "COMPANYCODE")
    private TblCompanyprofile tblCompanyprofile;

    //bi-directional many-to-one association to TblCompanyprofile
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "COMPAIGNCODE")
    private TblCompaign tblCompaign;

    //bi-directional many-to-one association to TblUser
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "CREATEDBY")
    private TblUser tblUser;

    @JoinColumn(name = "JOINTTENENCY")
    private String jointtenency;

    @Transient
    @Lob
    private String docBase64;

    private String bike;

    public TblCompanydoc() {
    }

    public long getCompanydocscode() {
        return this.companydocscode;
    }

    public void setCompanydocscode(long companydocscode) {
        this.companydocscode = companydocscode;
    }

    public String getAgenature() {
        return this.agenature;
    }

    public void setAgenature(String agenature) {
        this.agenature = agenature;
    }

    public String getCountrytype() {
        return this.countrytype;
    }

    public void setCountrytype(String countrytype) {
        this.countrytype = countrytype;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public TblCompanyprofile getTblCompanyprofile() {
        return this.tblCompanyprofile;
    }

    public void setTblCompanyprofile(TblCompanyprofile tblCompanyprofile) {
        this.tblCompanyprofile = tblCompanyprofile;
    }

    public TblUser getTblUser() {
        return this.tblUser;
    }

    public void setTblUser(TblUser tblUser) {
        this.tblUser = tblUser;
    }

    public String getDocBase64() {
        return docBase64;
    }

    public void setDocBase64(String docBase64) {
        this.docBase64 = docBase64;
    }

    public TblCompaign getTblCompaign() {
        return tblCompaign;
    }

    public void setTblCompaign(TblCompaign tblCompaign) {
        this.tblCompaign = tblCompaign;
    }

    public String getJointtenency() {
        return jointtenency;
    }

    public void setJointtenency(String jointtenency) {
        this.jointtenency = jointtenency;
    }

    public String getBike() {
        return bike;
    }

    public void setBike(String bike) {
        this.bike = bike;
    }
}