package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRJOINTTENANCY database table.
 */
@Entity
@Table(name = "TBL_HDRJOINTTENANCY")
@NamedQuery(name = "TblHdrjointtenancy.findAll", query = "SELECT t FROM TblHdrjointtenancy t")
public class TblHdrjointtenancy implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRJOINTTENANCY_HDRJOINTTENANCYCODE_GENERATOR", sequenceName = "SEQ_TBL_HDRJOINTTENANCY", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRJOINTTENANCY_HDRJOINTTENANCYCODE_GENERATOR")
    private long hdrjointtenancycode;


    @Column(name = "T_ADDRESS1")
    private String taddress1;


    @Column(name = "T_ADDRESS2")
    private String taddress2;


    @Column(name = "T_ADDRESS3")
    private String taddress3;



    @Column(name = "T_CITY")
    private String tcity;

    @JsonProperty("tDob")
    @Temporal(TemporalType.DATE)
    @Column(name = "T_DOB")
    private Date tDob;

    @JsonProperty("tEmail")
    @Column(name = "T_EMAIL")
    private String tEmail;

    @JsonProperty("tFname")
    @Column(name = "T_FNAME")
    private String tFname;

    @JsonProperty("tLandline")
    @Column(name = "T_LANDLINE")
    private String tLandline;

    @JsonProperty("tMname")
    @Column(name = "T_MNAME")
    private String tMname;

    @JsonProperty("tMobileno")
    @Column(name = "T_MOBILENO")
    private String tMobileno;

    @JsonProperty("tNinumber")
    @Column(name = "T_NICNUMBER")
    private String tNicnumber;


    @Column(name = "T_POSTCODE")
    private String tpostalcode;


    @Column(name = "T_REGION")
    private String tregion;

    @JsonProperty("tSname")
    @Column(name = "T_SNAME")
    private String tSname;

    @JsonProperty("tTitle")
    @Column(name = "T_TITLE")
    private String tTitle;

    //bi-directional many-to-one association to TblHdrclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HDR_CLAIMCODE")
    private TblHdrclaim tblHdrclaim;

    @Column(name = "LASTUPDATEUSER")
    private String lastupdateuser;

    public TblHdrjointtenancy() {
    }

    public long getHdrjointtenancycode() {
        return hdrjointtenancycode;
    }

    public void setHdrjointtenancycode(long hdrjointtenancycode) {
        this.hdrjointtenancycode = hdrjointtenancycode;
    }

    public String getTaddress1() {
        return taddress1;
    }

    public void setTaddress1(String taddress1) {
        this.taddress1 = taddress1;
    }

    public String getTaddress2() {
        return taddress2;
    }

    public void setTaddress2(String taddress2) {
        this.taddress2 = taddress2;
    }

    public String getTaddress3() {
        return taddress3;
    }

    public void setTaddress3(String taddress3) {
        this.taddress3 = taddress3;
    }

    public String getTcity() {
        return tcity;
    }

    public void setTcity(String tcity) {
        this.tcity = tcity;
    }

    public Date gettDob() {
        return tDob;
    }

    public void settDob(Date tDob) {
        this.tDob = tDob;
    }

    public String gettEmail() {
        return tEmail;
    }

    public void settEmail(String tEmail) {
        this.tEmail = tEmail;
    }

    public String gettFname() {
        return tFname;
    }

    public void settFname(String tFname) {
        this.tFname = tFname;
    }

    public String gettLandline() {
        return tLandline;
    }

    public void settLandline(String tLandline) {
        this.tLandline = tLandline;
    }

    public String gettMname() {
        return tMname;
    }

    public void settMname(String tMname) {
        this.tMname = tMname;
    }

    public String gettMobileno() {
        return tMobileno;
    }

    public void settMobileno(String tMobileno) {
        this.tMobileno = tMobileno;
    }

    public String gettNicnumber() {
        return tNicnumber;
    }

    public void settNicnumber(String tNicnumber) {
        this.tNicnumber = tNicnumber;
    }

    public String getTpostalcode() {
        return tpostalcode;
    }

    public void setTpostalcode(String tpostalcode) {
        this.tpostalcode = tpostalcode;
    }

    public String getTregion() {
        return tregion;
    }

    public void setTregion(String tregion) {
        this.tregion = tregion;
    }

    public String gettSname() {
        return tSname;
    }

    public void settSname(String tSname) {
        this.tSname = tSname;
    }

    public String gettTitle() {
        return tTitle;
    }

    public void settTitle(String tTitle) {
        this.tTitle = tTitle;
    }

    public TblHdrclaim getTblHdrclaim() {
        return tblHdrclaim;
    }

    public void setTblHdrclaim(TblHdrclaim tblHdrclaim) {
        this.tblHdrclaim = tblHdrclaim;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }
}