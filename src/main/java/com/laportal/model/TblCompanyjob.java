package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_COMPANYJOBS database table.
 */
@Entity
@Table(name = "TBL_COMPANYJOBS")
@NamedQuery(name = "TblCompanyjob.findAll", query = "SELECT t FROM TblCompanyjob t")
public class TblCompanyjob implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String companyjobcode;

    private BigDecimal bike;

    private String companycode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private BigDecimal faultrepairs;

    @Column(name = "HOUSING_FEE")
    private BigDecimal housingFee;

    private BigDecimal hybrid;

    private BigDecimal pedestrian;

    private BigDecimal prestigeroadworthy;

    private BigDecimal prestigeunroadworthy;

    private BigDecimal recovery;

    private String remarks;

    private BigDecimal repairs;

    private BigDecimal salvage;

    @Column(name = "SCOTISH_RTA")
    private BigDecimal scotishRta;

    private BigDecimal serious;

    private BigDecimal solicitorsfees;

    private BigDecimal standardroadworthy;

    private BigDecimal standardunroadworthy;

    private String status;

    @Column(name = "\"STORAGE\"")
    private BigDecimal storage;

    private String usercode;

    private BigDecimal wiplash;

    private BigDecimal minor;
    //bi-directional many-to-one association to TblCompaign
    @ManyToOne
    @JoinColumn(name = "COMPAIGNCODE")
    private TblCompaign tblCompaign;

    @Transient
    private String compaigndescr;

    public TblCompanyjob() {
    }

    public String getCompanyjobcode() {
        return this.companyjobcode;
    }

    public void setCompanyjobcode(String companyjobcode) {
        this.companyjobcode = companyjobcode;
    }

    public BigDecimal getBike() {
        return this.bike;
    }

    public void setBike(BigDecimal bike) {
        this.bike = bike;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public BigDecimal getFaultrepairs() {
        return this.faultrepairs;
    }

    public void setFaultrepairs(BigDecimal faultrepairs) {
        this.faultrepairs = faultrepairs;
    }

    public BigDecimal getHousingFee() {
        return this.housingFee;
    }

    public void setHousingFee(BigDecimal housingFee) {
        this.housingFee = housingFee;
    }

    public BigDecimal getHybrid() {
        return this.hybrid;
    }

    public void setHybrid(BigDecimal hybrid) {
        this.hybrid = hybrid;
    }

    public BigDecimal getPedestrian() {
        return this.pedestrian;
    }

    public void setPedestrian(BigDecimal pedestrian) {
        this.pedestrian = pedestrian;
    }

    public BigDecimal getPrestigeroadworthy() {
        return this.prestigeroadworthy;
    }

    public void setPrestigeroadworthy(BigDecimal prestigeroadworthy) {
        this.prestigeroadworthy = prestigeroadworthy;
    }

    public BigDecimal getPrestigeunroadworthy() {
        return this.prestigeunroadworthy;
    }

    public void setPrestigeunroadworthy(BigDecimal prestigeunroadworthy) {
        this.prestigeunroadworthy = prestigeunroadworthy;
    }

    public BigDecimal getRecovery() {
        return this.recovery;
    }

    public void setRecovery(BigDecimal recovery) {
        this.recovery = recovery;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getRepairs() {
        return this.repairs;
    }

    public void setRepairs(BigDecimal repairs) {
        this.repairs = repairs;
    }

    public BigDecimal getSalvage() {
        return this.salvage;
    }

    public void setSalvage(BigDecimal salvage) {
        this.salvage = salvage;
    }

    public BigDecimal getScotishRta() {
        return this.scotishRta;
    }

    public void setScotishRta(BigDecimal scotishRta) {
        this.scotishRta = scotishRta;
    }

    public BigDecimal getSerious() {
        return this.serious;
    }

    public void setSerious(BigDecimal serious) {
        this.serious = serious;
    }

    public BigDecimal getSolicitorsfees() {
        return this.solicitorsfees;
    }

    public void setSolicitorsfees(BigDecimal solicitorsfees) {
        this.solicitorsfees = solicitorsfees;
    }

    public BigDecimal getStandardroadworthy() {
        return this.standardroadworthy;
    }

    public void setStandardroadworthy(BigDecimal standardroadworthy) {
        this.standardroadworthy = standardroadworthy;
    }

    public BigDecimal getStandardunroadworthy() {
        return this.standardunroadworthy;
    }

    public void setStandardunroadworthy(BigDecimal standardunroadworthy) {
        this.standardunroadworthy = standardunroadworthy;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getStorage() {
        return this.storage;
    }

    public void setStorage(BigDecimal storage) {
        this.storage = storage;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public BigDecimal getWiplash() {
        return this.wiplash;
    }

    public void setWiplash(BigDecimal wiplash) {
        this.wiplash = wiplash;
    }

    public TblCompaign getTblCompaign() {
        return this.tblCompaign;
    }

    public void setTblCompaign(TblCompaign tblCompaign) {
        this.tblCompaign = tblCompaign;
    }

    public String getCompaigndescr() {
        return compaigndescr;
    }

    public void setCompaigndescr(String compaigndescr) {
        this.compaigndescr = compaigndescr;
    }

    public BigDecimal getMinor() {
        return minor;
    }

    public void setMinor(BigDecimal minor) {
        this.minor = minor;
    }
}