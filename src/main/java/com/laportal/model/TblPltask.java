package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_PLTASKS database table.
 */
@Entity
@Table(name = "TBL_PLTASKS")
@NamedQuery(name = "TblPltask.findAll", query = "SELECT t FROM TblPltask t")
public class TblPltask implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PLTASKS_PLTASKCODE_GENERATOR", sequenceName = "SEQ_TBL_PLTASKS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PLTASKS_PLTASKCODE_GENERATOR")
    private long pltaskcode;

    private String completedby;

    @Temporal(TemporalType.DATE)
    private Date completedon;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String currenttask;

    private String remarks;

    private String status;

    //bi-directional many-to-one association to TblTask
    @ManyToOne
    @JoinColumn(name = "TASKCODE")
    private TblTask tblTask;

    //bi-directional many-to-one association to TblPlclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PLCLAIMCODE")
    private TblPlclaim tblPlclaim;

    public TblPltask() {
    }

    public long getPltaskcode() {
        return this.pltaskcode;
    }

    public void setPltaskcode(long pltaskcode) {
        this.pltaskcode = pltaskcode;
    }

    public String getCompletedby() {
        return this.completedby;
    }

    public void setCompletedby(String completedby) {
        this.completedby = completedby;
    }

    public Date getCompletedon() {
        return this.completedon;
    }

    public void setCompletedon(Date completedon) {
        this.completedon = completedon;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getCurrenttask() {
        return this.currenttask;
    }

    public void setCurrenttask(String currenttask) {
        this.currenttask = currenttask;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TblTask getTblTask() {
        return tblTask;
    }

    public void setTblTask(TblTask tblTask) {
        this.tblTask = tblTask;
    }

    public TblPlclaim getTblPlclaim() {
        return this.tblPlclaim;
    }

    public void setTblPlclaim(TblPlclaim tblPlclaim) {
        this.tblPlclaim = tblPlclaim;
    }

}