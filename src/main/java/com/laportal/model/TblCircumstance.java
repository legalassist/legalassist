package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_CIRCUMSTANCES database table.
 */
@Entity
@Table(name = "TBL_CIRCUMSTANCES")
@NamedQuery(name = "TblCircumstance.findAll", query = "SELECT t FROM TblCircumstance t")
public class TblCircumstance implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_CIRCUMSTANCES_CIRCUMCODE_GENERATOR", sequenceName = "SEQ_CIRCUMSTANCES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_CIRCUMSTANCES_CIRCUMCODE_GENERATOR")
    private long circumcode;

    private String circumname;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String status;

    private String usercode;

    //bi-directional many-to-one association to TblRtaclaim
    @JsonIgnore
    @OneToMany(mappedBy = "tblCircumstance")
    private List<TblRtaclaim> tblRtaclaims;

    public TblCircumstance() {
    }

    public long getCircumcode() {
        return this.circumcode;
    }

    public void setCircumcode(long circumcode) {
        this.circumcode = circumcode;
    }

    public String getCircumname() {
        return this.circumname;
    }

    public void setCircumname(String circumname) {
        this.circumname = circumname;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public List<TblRtaclaim> getTblRtaclaims() {
        return this.tblRtaclaims;
    }

    public void setTblRtaclaims(List<TblRtaclaim> tblRtaclaims) {
        this.tblRtaclaims = tblRtaclaims;
    }

    public TblRtaclaim addTblRtaclaim(TblRtaclaim tblRtaclaim) {
        getTblRtaclaims().add(tblRtaclaim);
        tblRtaclaim.setTblCircumstance(this);

        return tblRtaclaim;
    }

    public TblRtaclaim removeTblRtaclaim(TblRtaclaim tblRtaclaim) {
        getTblRtaclaims().remove(tblRtaclaim);
        tblRtaclaim.setTblCircumstance(null);

        return tblRtaclaim;
    }

}