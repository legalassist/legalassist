package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_OLSOLICITOR database table.
 */
@Entity
@Table(name = "TBL_OLSOLICITOR")
@NamedQuery(name = "TblOlsolicitor.findAll", query = "SELECT t FROM TblOlsolicitor t")
public class TblOlsolicitor implements Serializable {
    private static final long serialVersionUID = 1L;

    private String companycode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Id
    @SequenceGenerator(name = "TBL_OLSOLICITOR_OLSOLCODE_GENERATOR", sequenceName = "SEQ_TBL_OLSOLICITOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_OLSOLICITOR_OLSOLCODE_GENERATOR")
    private long olsolcode;

    private String reference1;

    private String reference2;

    private String remarks;

    private String status;

    private String usercode;

    //bi-directional many-to-one association to TblOlclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "OLCLAIMCODE")
    private TblOlclaim tblOlclaim;

    @Transient
    private TblCompanyprofile tblCompanyprofile;

    public TblOlsolicitor() {
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public long getOlsolcode() {
        return olsolcode;
    }

    public void setOlsolcode(long olsolcode) {
        this.olsolcode = olsolcode;
    }

    public String getReference1() {
        return this.reference1;
    }

    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getReference2() {
        return this.reference2;
    }

    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblOlclaim getTblOlclaim() {
        return this.tblOlclaim;
    }

    public void setTblOlclaim(TblOlclaim tblOlclaim) {
        this.tblOlclaim = tblOlclaim;
    }

    public TblCompanyprofile getTblCompanyprofile() {
        return tblCompanyprofile;
    }

    public void setTblCompanyprofile(TblCompanyprofile tblCompanyprofile) {
        this.tblCompanyprofile = tblCompanyprofile;
    }
}