package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the TBL_COMPAIGNS database table.
 */
@Entity
@Table(name = "TBL_COMPAIGNS")
@NamedQuery(name = "TblCompaign.findAll", query = "SELECT t FROM TblCompaign t")
public class TblCompaign implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_COMPAIGNS_COMPAIGNCODE_GENERATOR", sequenceName = "SEQ_TBL_COMPAIGNS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_COMPAIGNS_COMPAIGNCODE_GENERATOR")
    private String compaigncode;

    private String compaignname;

    private String compaignstatus;

    private String descr;



    public TblCompaign() {
    }

    public String getCompaigncode() {
        return this.compaigncode;
    }

    public void setCompaigncode(String compaigncode) {
        this.compaigncode = compaigncode;
    }

    public String getCompaignname() {
        return this.compaignname;
    }

    public void setCompaignname(String compaignname) {
        this.compaignname = compaignname;
    }

    public String getCompaignstatus() {
        return this.compaignstatus;
    }

    public void setCompaignstatus(String compaignstatus) {
        this.compaignstatus = compaignstatus;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }


}