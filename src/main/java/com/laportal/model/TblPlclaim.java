package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.laportal.dto.RtaActionButton;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_PLCLAIM database table.
 */
@Entity
@Table(name = "TBL_PLCLAIM")
@NamedQuery(name = "TblPlclaim.findAll", query = "SELECT t FROM TblPlclaim t")
public class TblPlclaim implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PLCLAIM_PLCLAIMCODE_GENERATOR", sequenceName = "SEQ_TBL_PLCLAIM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PLCLAIM_PLCLAIMCODE_GENERATOR")
    private long plclaimcode;

    @Temporal(TemporalType.DATE)
    private Date accdatetime;

    private String accdescription;

    private String acclocation;

    private String accreportedto;


    private Date createdate;

    private BigDecimal createuser;

    private String describeinjuries;

    @Temporal(TemporalType.DATE)
    private Date dob;

    private String email;

    private String esig;

    private Date esigdate;

    private String injuriesduration;

    private String landline;

    private String medicalattention;

    private String mobile;

    private String ninumber;

    private String occupation;

    private String password;

    private String plcode;

    private String remarks;

    private BigDecimal status;

    private Date updatedate;

    private String witnesses;

    private String title;

    private String firstname;

    private String middlename;

    private String lastname;

    private String postalcode;

    private String address1;

    private String address2;

    private String address3;

    private String city;

    private String region;
    private Long advisor;
    private Long introducer;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "UTC")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "CLAWBACK_DATE")
    private Date clawbackDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "SUBMIT_DATE")
    private Date submitDate;

    private String acctime;

    private String lastupdateuser;

    //bi-directional many-to-one association to TblPllog
    @OneToMany(mappedBy = "tblPlclaim")
    private List<TblPllog> tblPllogs;

    //bi-directional many-to-one association to TblPlmessage
    @OneToMany(mappedBy = "tblPlclaim")
    private List<TblPlmessage> tblPlmessages;

    //bi-directional many-to-one association to TblPlnote
    @OneToMany(mappedBy = "tblPlclaim")
    private List<TblPlnote> tblPlnotes;

    //bi-directional many-to-one association to TblPlsolicitor
    @OneToMany(mappedBy = "tblPlclaim")
    private List<TblPlsolicitor> tblPlsolicitors;

    //bi-directional many-to-one association to TblPldocument
    @OneToMany(mappedBy = "tblPlclaim")
    private List<TblPldocument> tblPldocuments;

    @Transient
    private String statusDescr;

    @Transient
    private List<RtaActionButton> plActionButtons;

    @Transient
    private List<RtaActionButton> plActionButtonForLA;

    @Transient
    private List<TblPltask> tblPltasks;

    @Transient
    private String solicitorcompany;
    @Transient
    private String solicitorusername;
    @Transient
    private String advisorname;
    @Transient
    private String introducername;
    @Transient
    private Date introducerInvoiceDate;
    @Transient
    private BigDecimal introducerInvoiceHeadId;
    @Transient
    private Date solicitorInvoiceDate;
    @Transient
    private BigDecimal solicitorInvoiceHeadId;
    @Transient
    private TblEsignStatus tblEsignStatus;
    @Transient
    private String taskflag;
    @Transient
    private String editFlag;

    public TblPlclaim() {
    }

    public long getPlclaimcode() {
        return this.plclaimcode;
    }

    public void setPlclaimcode(long plclaimcode) {
        this.plclaimcode = plclaimcode;
    }

    public Date getAccdatetime() {
        return this.accdatetime;
    }

    public void setAccdatetime(Date accdatetime) {
        this.accdatetime = accdatetime;
    }

    public String getAccdescription() {
        return this.accdescription;
    }

    public void setAccdescription(String accdescription) {
        this.accdescription = accdescription;
    }

    public String getAcclocation() {
        return this.acclocation;
    }

    public void setAcclocation(String acclocation) {
        this.acclocation = acclocation;
    }

    public String getAccreportedto() {
        return this.accreportedto;
    }

    public void setAccreportedto(String accreportedto) {
        this.accreportedto = accreportedto;
    }


    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public String getDescribeinjuries() {
        return this.describeinjuries;
    }

    public void setDescribeinjuries(String describeinjuries) {
        this.describeinjuries = describeinjuries;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEsig() {
        return this.esig;
    }

    public void setEsig(String esig) {
        this.esig = esig;
    }

    public Date getEsigdate() {
        return this.esigdate;
    }

    public void setEsigdate(Date esigdate) {
        this.esigdate = esigdate;
    }


    public String getInjuriesduration() {
        return this.injuriesduration;
    }

    public void setInjuriesduration(String injuriesduration) {
        this.injuriesduration = injuriesduration;
    }

    public String getLandline() {
        return this.landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getMedicalattention() {
        return this.medicalattention;
    }

    public void setMedicalattention(String medicalattention) {
        this.medicalattention = medicalattention;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNinumber() {
        return this.ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getOccupation() {
        return this.occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPlcode() {
        return this.plcode;
    }

    public void setPlcode(String plcode) {
        this.plcode = plcode;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getStatus() {
        return this.status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    public Date getUpdatedate() {
        return this.updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public String getWitnesses() {
        return this.witnesses;
    }

    public void setWitnesses(String witnesses) {
        this.witnesses = witnesses;
    }

    public List<TblPllog> getTblPllogs() {
        return this.tblPllogs;
    }

    public void setTblPllogs(List<TblPllog> tblPllogs) {
        this.tblPllogs = tblPllogs;
    }

    public TblPllog addTblPllog(TblPllog tblPllog) {
        getTblPllogs().add(tblPllog);
        tblPllog.setTblPlclaim(this);

        return tblPllog;
    }

    public TblPllog removeTblPllog(TblPllog tblPllog) {
        getTblPllogs().remove(tblPllog);
        tblPllog.setTblPlclaim(null);

        return tblPllog;
    }

    public List<TblPlmessage> getTblPlmessages() {
        return this.tblPlmessages;
    }

    public void setTblPlmessages(List<TblPlmessage> tblPlmessages) {
        this.tblPlmessages = tblPlmessages;
    }

    public TblPlmessage addTblPlmessage(TblPlmessage tblPlmessage) {
        getTblPlmessages().add(tblPlmessage);
        tblPlmessage.setTblPlclaim(this);

        return tblPlmessage;
    }

    public TblPlmessage removeTblPlmessage(TblPlmessage tblPlmessage) {
        getTblPlmessages().remove(tblPlmessage);
        tblPlmessage.setTblPlclaim(null);

        return tblPlmessage;
    }

    public List<TblPlnote> getTblPlnotes() {
        return this.tblPlnotes;
    }

    public void setTblPlnotes(List<TblPlnote> tblPlnotes) {
        this.tblPlnotes = tblPlnotes;
    }

    public TblPlnote addTblPlnote(TblPlnote tblPlnote) {
        getTblPlnotes().add(tblPlnote);
        tblPlnote.setTblPlclaim(this);

        return tblPlnote;
    }

    public TblPlnote removeTblPlnote(TblPlnote tblPlnote) {
        getTblPlnotes().remove(tblPlnote);
        tblPlnote.setTblPlclaim(null);

        return tblPlnote;
    }

    public List<TblPlsolicitor> getTblPlsolicitors() {
        return this.tblPlsolicitors;
    }

    public void setTblPlsolicitors(List<TblPlsolicitor> tblPlsolicitors) {
        this.tblPlsolicitors = tblPlsolicitors;
    }

    public TblPlsolicitor addTblPlsolicitor(TblPlsolicitor tblPlsolicitor) {
        getTblPlsolicitors().add(tblPlsolicitor);
        tblPlsolicitor.setTblPlclaim(this);

        return tblPlsolicitor;
    }

    public TblPlsolicitor removeTblPlsolicitor(TblPlsolicitor tblPlsolicitor) {
        getTblPlsolicitors().remove(tblPlsolicitor);
        tblPlsolicitor.setTblPlclaim(null);

        return tblPlsolicitor;
    }

    public List<TblPltask> getTblPltasks() {
        return this.tblPltasks;
    }

    public void setTblPltasks(List<TblPltask> tblPltasks) {
        this.tblPltasks = tblPltasks;
    }

    public TblPltask addTblPltask(TblPltask tblPltask) {
        getTblPltasks().add(tblPltask);
        tblPltask.setTblPlclaim(this);

        return tblPltask;
    }

    public TblPltask removeTblPltask(TblPltask tblPltask) {
        getTblPltasks().remove(tblPltask);
        tblPltask.setTblPlclaim(null);

        return tblPltask;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }


    public List<TblPldocument> getTblPldocuments() {
        return tblPldocuments;
    }

    public void setTblPldocuments(List<TblPldocument> tblPldocuments) {
        this.tblPldocuments = tblPldocuments;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Long getAdvisor() {
        return advisor;
    }

    public void setAdvisor(Long advisor) {
        this.advisor = advisor;
    }

    public Long getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Long introducer) {
        this.introducer = introducer;
    }

    public Date getClawbackDate() {
        return clawbackDate;
    }

    public void setClawbackDate(Date clawbackDate) {
        this.clawbackDate = clawbackDate;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public String getAcctime() {
        return acctime;
    }

    public void setAcctime(String acctime) {
        this.acctime = acctime;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }

    public List<RtaActionButton> getPlActionButtons() {
        return plActionButtons;
    }

    public void setPlActionButtons(List<RtaActionButton> plActionButtons) {
        this.plActionButtons = plActionButtons;
    }

    public List<RtaActionButton> getPlActionButtonForLA() {
        return plActionButtonForLA;
    }

    public void setPlActionButtonForLA(List<RtaActionButton> plActionButtonForLA) {
        this.plActionButtonForLA = plActionButtonForLA;
    }

    public String getSolicitorcompany() {
        return solicitorcompany;
    }

    public void setSolicitorcompany(String solicitorcompany) {
        this.solicitorcompany = solicitorcompany;
    }

    public String getSolicitorusername() {
        return solicitorusername;
    }

    public void setSolicitorusername(String solicitorusername) {
        this.solicitorusername = solicitorusername;
    }

    public String getAdvisorname() {
        return advisorname;
    }

    public void setAdvisorname(String advisorname) {
        this.advisorname = advisorname;
    }

    public String getIntroducername() {
        return introducername;
    }

    public void setIntroducername(String introducername) {
        this.introducername = introducername;
    }

    public Date getIntroducerInvoiceDate() {
        return introducerInvoiceDate;
    }

    public void setIntroducerInvoiceDate(Date introducerInvoiceDate) {
        this.introducerInvoiceDate = introducerInvoiceDate;
    }

    public BigDecimal getIntroducerInvoiceHeadId() {
        return introducerInvoiceHeadId;
    }

    public void setIntroducerInvoiceHeadId(BigDecimal introducerInvoiceHeadId) {
        this.introducerInvoiceHeadId = introducerInvoiceHeadId;
    }

    public Date getSolicitorInvoiceDate() {
        return solicitorInvoiceDate;
    }

    public void setSolicitorInvoiceDate(Date solicitorInvoiceDate) {
        this.solicitorInvoiceDate = solicitorInvoiceDate;
    }

    public BigDecimal getSolicitorInvoiceHeadId() {
        return solicitorInvoiceHeadId;
    }

    public void setSolicitorInvoiceHeadId(BigDecimal solicitorInvoiceHeadId) {
        this.solicitorInvoiceHeadId = solicitorInvoiceHeadId;
    }

    public TblEsignStatus getTblEsignStatus() {
        return tblEsignStatus;
    }

    public void setTblEsignStatus(TblEsignStatus tblEsignStatus) {
        this.tblEsignStatus = tblEsignStatus;
    }

    public String getTaskflag() {
        return taskflag;
    }

    public void setTaskflag(String taskflag) {
        this.taskflag = taskflag;
    }

    public String getEditFlag() {
        return editFlag;
    }

    public void setEditFlag(String editFlag) {
        this.editFlag = editFlag;
    }
}