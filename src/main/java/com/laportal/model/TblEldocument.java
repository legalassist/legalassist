package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_ELDOCUMENTS database table.
 */
@Entity
@Table(name = "TBL_ELDOCUMENTS")
@NamedQuery(name = "TblEldocument.findAll", query = "SELECT t FROM TblEldocument t")
public class TblEldocument implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_ELDOCUMENTS_ELDOCUMENTSCODE_GENERATOR", sequenceName = "SEQ_TBL_ELDOCUMENTS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_ELDOCUMENTS_ELDOCUMENTSCODE_GENERATOR")
    private long eldocumentscode;

    @Column(name = "DOCUMENT_PATH")
    private String documentPath;


    //bi-directional many-to-one association to TblElclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ELCLAIMCODE")
    private TblElclaim tblElclaim;


    @JsonIgnore
    @Column(name = "CREATEDATE")
    private Date createddate;

    @Column(name = "DOCUMENT_NAME")
    private String docname;

    @Column(name = "DOCUMENT_TYPE")
    private String doctype;

    private String docurl;
    @JsonIgnore
    private String remarks;
    @JsonIgnore
    private String createuser;

    @Lob
    @Column(name = "DOCUMENT_BASE64")
    private String docbase64;

    // bi-directional many-to-one association to TblTasks

    @ManyToOne
    @JoinColumn(name = "TASKCODE")
    private TblTask tblTask;

    public TblEldocument() {
    }

    public long getEldocumentscode() {
        return eldocumentscode;
    }

    public void setEldocumentscode(long eldocumentscode) {
        this.eldocumentscode = eldocumentscode;
    }

    public String getDocumentPath() {
        return documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public TblElclaim getTblElclaim() {
        return tblElclaim;
    }

    public void setTblElclaim(TblElclaim tblElclaim) {
        this.tblElclaim = tblElclaim;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public String getDoctype() {
        return doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getDocurl() {
        return docurl;
    }

    public void setDocurl(String docurl) {
        this.docurl = docurl;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getDocbase64() {
        return docbase64;
    }

    public void setDocbase64(String docbase64) {
        this.docbase64 = docbase64;
    }

    public TblTask getTblTask() {
        return tblTask;
    }

    public void setTblTask(TblTask tblTask) {
        this.tblTask = tblTask;
    }
}