package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_RTAINJURY database table.
 */
@Entity
@Table(name = "TBL_RTAINJURY")
@NamedQuery(name = "TblRtainjury.findAll", query = "SELECT t FROM TblRtainjury t")
public class TblRtainjury implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTAINJURY_RTAINJURYID_GENERATOR", sequenceName = "SEQ_TBL_RTAINJURY", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTAINJURY_RTAINJURYID_GENERATOR")
    @Column(name = "RTAINJURY_ID")
    private long rtainjuryId;

    @Temporal(TemporalType.DATE)
    private Date createdate;

    private String createuser;

    //bi-directional many-to-one association to TblInjclass
    @ManyToOne
    @JoinColumn(name = "INJCLASSCODE")
    private TblInjclass tblInjclass;

    @Temporal(TemporalType.DATE)
    private Date lastupdatedate;

    private String lastupdateuser;

    //bi-directional many-to-one association to TblRtaclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "RTACODE")
    private TblRtaclaim tblRtaclaim;

    public TblRtainjury() {
    }

    public long getRtainjuryId() {
        return this.rtainjuryId;
    }

    public void setRtainjuryId(long rtainjuryId) {
        this.rtainjuryId = rtainjuryId;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public TblInjclass getTblInjclass() {
        return tblInjclass;
    }

    public void setTblInjclass(TblInjclass tblInjclass) {
        this.tblInjclass = tblInjclass;
    }

    public Date getLastupdatedate() {
        return this.lastupdatedate;
    }

    public void setLastupdatedate(Date lastupdatedate) {
        this.lastupdatedate = lastupdatedate;
    }

    public String getLastupdateuser() {
        return this.lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }

    public TblRtaclaim getTblRtaclaim() {
        return this.tblRtaclaim;
    }

    public void setTblRtaclaim(TblRtaclaim tblRtaclaim) {
        this.tblRtaclaim = tblRtaclaim;
    }

}