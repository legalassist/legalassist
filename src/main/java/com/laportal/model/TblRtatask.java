package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_RTATASKS database table.
 */
@Entity
@Table(name = "TBL_RTATASKS")
@NamedQuery(name = "TblRtatask.findAll", query = "SELECT t FROM TblRtatask t")
public class TblRtatask implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTATASKS_RTATASKCODE_GENERATOR", sequenceName = "SEQ_RTATASKS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTATASKS_RTATASKCODE_GENERATOR")
    private long rtataskcode;

    private String completedby;

    @JsonIgnore
    private Date completedon;
    @JsonIgnore
    private String remarks;

    private String status;

    //bi-directional many-to-one association to TblRtaclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "RTACODE")
    private TblRtaclaim tblRtaclaim;

    //bi-directional many-to-one association to TblTask
    @ManyToOne
    @JoinColumn(name = "TASKCODE")
    private TblTask tblTask;

    private String currenttask;

    @Transient
    private String userName;

    public TblRtatask() {
    }

    public long getRtataskcode() {
        return this.rtataskcode;
    }

    public void setRtataskcode(long rtataskcode) {
        this.rtataskcode = rtataskcode;
    }

    public String getCompletedby() {
        return this.completedby;
    }

    public void setCompletedby(String completedby) {
        this.completedby = completedby;
    }

    public Date getCompletedon() {
        return this.completedon;
    }

    public void setCompletedon(Date completedon) {
        this.completedon = completedon;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TblRtaclaim getTblRtaclaim() {
        return this.tblRtaclaim;
    }

    public void setTblRtaclaim(TblRtaclaim tblRtaclaim) {
        this.tblRtaclaim = tblRtaclaim;
    }

    public TblTask getTblTask() {
        return this.tblTask;
    }

    public void setTblTask(TblTask tblTask) {
        this.tblTask = tblTask;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCurrenttask() {
        return currenttask;
    }

    public void setCurrenttask(String currenttask) {
        this.currenttask = currenttask;
    }
}