package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_RTA_PASSENGER_INJURY database table.
 */
@Entity
@Table(name = "TBL_RTA_PASSENGER_INJURY")
@NamedQuery(name = "TblRtaPassengerInjury.findAll", query = "SELECT t FROM TblRtaPassengerInjury t")
public class TblRtaPassengerInjury implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTA_PASSENGER_INJURY_RTAPASSENGERINJURYID_GENERATOR", sequenceName = "SEQ_TBL_RTA_PASSENGER_INJURY", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTA_PASSENGER_INJURY_RTAPASSENGERINJURYID_GENERATOR")
    @Column(name = "RTA_PASSENGER_INJURY_ID")
    private long rtaPassengerInjuryId;

    @Temporal(TemporalType.DATE)
    private Date createdate;

    private String createuser;

    private Long injclasscode;

    private Date lastupdatedate;

    private String lastupdateuser;

    //bi-directional many-to-one association to TblRtapassenger
    @ManyToOne
    @JoinColumn(name = "PASSENGERCODE")
    private TblRtapassenger tblRtapassenger;

    public TblRtaPassengerInjury() {
    }

    public long getRtaPassengerInjuryId() {
        return this.rtaPassengerInjuryId;
    }

    public void setRtaPassengerInjuryId(long rtaPassengerInjuryId) {
        this.rtaPassengerInjuryId = rtaPassengerInjuryId;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public Long getInjclasscode() {
        return this.injclasscode;
    }

    public void setInjclasscode(Long injclasscode) {
        this.injclasscode = injclasscode;
    }

    public Date getLastupdatedate() {
        return this.lastupdatedate;
    }

    public void setLastupdatedate(Date lastupdatedate) {
        this.lastupdatedate = lastupdatedate;
    }

    public String getLastupdateuser() {
        return this.lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }

    public TblRtapassenger getTblRtapassenger() {
        return this.tblRtapassenger;
    }

    public void setTblRtapassenger(TblRtapassenger tblRtapassenger) {
        this.tblRtapassenger = tblRtapassenger;
    }

}