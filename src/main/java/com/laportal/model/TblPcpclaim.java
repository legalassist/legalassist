package com.laportal.model;

import com.laportal.dto.HdrActionButton;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_PCPCLAIM database table.
 */
@Entity
@Table(name = "TBL_PCPCLAIM")
@NamedQuery(name = "TblPcpclaim.findAll", query = "SELECT t FROM TblPcpclaim t")
public class TblPcpclaim implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PCPCLAIM_PCPCLAIMCODE_GENERATOR", sequenceName = "SEQ_TBL_PCPCLAIM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PCPCLAIM_PCPCLAIMCODE_GENERATOR")
    private long pcpclaimcode;

    @Column(name = "ADDRESS_OF_CLIENT")
    private String addressOfClient;

    private String brokeraddress;

    private String brokername;

    @Column(name = "CASH_DEPOSIT")
    private BigDecimal cashDeposit;

    @Column(name = "CHARGE_FOR_CREDIT")
    private BigDecimal chargeForCredit;

    @Column(name = "CONTACT_NO")
    private String contactNo;

    @Temporal(TemporalType.DATE)
    @Column(name = "CONTRACT_DATE")
    private Date contractDate;

    @Column(name = "CONTRACTUAL_APR")
    private String contractualApr;

    @Column(name = "CONTRACTUAL_MONTHLY_PAYMENT")
    private BigDecimal contractualMonthlyPayment;

    @Temporal(TemporalType.DATE)
    private Date createdate;

    private BigDecimal createuser;

    private String dealeraddress;

    private String dealername;

    @Temporal(TemporalType.DATE)
    private Date dob;

    @Column(name = "EMAIL_ADDRESS")
    private String emailAddress;

    private String esig;

    @Temporal(TemporalType.DATE)
    private Date esigdate;

    @Column(name = "FCA_NUMBER")
    private BigDecimal fcaNumber;

    @Column(name = "FULL_NAME")
    private String fullName;

    private String lenderaddress;

    private String lendername;

    @Column(name = "LOWEST_ADVERTISED_RATE")
    private BigDecimal lowestAdvertisedRate;

    private String makemodel;

    @Column(name = "NI_NUMBER")
    private String niNumber;

    @Column(name = "OPTION_TO_PURCHASE_FEE")
    private String optionToPurchaseFee;

    @Column(name = "PART_EXCHANGE")
    private String partExchange;

    private String pcpcode;

    @Temporal(TemporalType.DATE)
    @Column(name = "PURCHASE_DATE")
    private Date purchaseDate;

    @Column(name = "PURCHASE_PRICE")
    private BigDecimal purchasePrice;

    private String registration;

    private String remarks;

    @Column(name = "SETUP_FEES")
    private BigDecimal setupFees;

    private BigDecimal status;

    @Column(name = "TOTAL_PAYABLE")
    private BigDecimal totalPayable;

    //bi-directional many-to-one association to TblPcpdocument
    @OneToMany(mappedBy = "tblPcpclaim")
    private List<TblPcpdocument> tblPcpdocuments;

    //bi-directional many-to-one association to TblPcplog
    @OneToMany(mappedBy = "tblPcpclaim")
    private List<TblPcplog> tblPcplogs;

    //bi-directional many-to-one association to TblPcpmessage
    @OneToMany(mappedBy = "tblPcpclaim")
    private List<TblPcpmessage> tblPcpmessages;

    //bi-directional many-to-one association to TblPcpnote
    @OneToMany(mappedBy = "tblPcpclaim")
    private List<TblPcpnote> tblPcpnotes;

    //bi-directional many-to-one association to TblPcpsolicitor
    @OneToMany(mappedBy = "tblPcpclaim")
    private List<TblPcpsolicitor> tblPcpsolicitors;

    //bi-directional many-to-one association to TblPcptask
    @OneToMany(mappedBy = "tblPcpclaim")
    private List<TblPcptask> tblPcptasks;

    @Transient
    private String statusDescr;

    @Transient
    private List<HdrActionButton> hdrActionButton;

    @Transient
    private List<HdrActionButton> hdrActionButtonForLA;

    @Column(name = "CONTRACTUAL_TERM")
    private String contractualTerm;

    @Column(name = "END_OF_AGGREEMENT")
    private String endOfAgreement;

    @Temporal(TemporalType.DATE)
    private Date updatedate;

    public TblPcpclaim() {
    }

    public long getPcpclaimcode() {
        return this.pcpclaimcode;
    }

    public void setPcpclaimcode(long pcpclaimcode) {
        this.pcpclaimcode = pcpclaimcode;
    }

    public String getAddressOfClient() {
        return this.addressOfClient;
    }

    public void setAddressOfClient(String addressOfClient) {
        this.addressOfClient = addressOfClient;
    }

    public String getBrokeraddress() {
        return this.brokeraddress;
    }

    public void setBrokeraddress(String brokeraddress) {
        this.brokeraddress = brokeraddress;
    }

    public String getBrokername() {
        return this.brokername;
    }

    public void setBrokername(String brokername) {
        this.brokername = brokername;
    }

    public BigDecimal getCashDeposit() {
        return this.cashDeposit;
    }

    public void setCashDeposit(BigDecimal cashDeposit) {
        this.cashDeposit = cashDeposit;
    }

    public BigDecimal getChargeForCredit() {
        return this.chargeForCredit;
    }

    public void setChargeForCredit(BigDecimal chargeForCredit) {
        this.chargeForCredit = chargeForCredit;
    }

    public String getContactNo() {
        return this.contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public Date getContractDate() {
        return this.contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public String getContractualApr() {
        return this.contractualApr;
    }

    public void setContractualApr(String contractualApr) {
        this.contractualApr = contractualApr;
    }

    public BigDecimal getContractualMonthlyPayment() {
        return this.contractualMonthlyPayment;
    }

    public void setContractualMonthlyPayment(BigDecimal contractualMonthlyPayment) {
        this.contractualMonthlyPayment = contractualMonthlyPayment;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public String getDealeraddress() {
        return this.dealeraddress;
    }

    public void setDealeraddress(String dealeraddress) {
        this.dealeraddress = dealeraddress;
    }

    public String getDealername() {
        return this.dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmailAddress() {
        return this.emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEsig() {
        return this.esig;
    }

    public void setEsig(String esig) {
        this.esig = esig;
    }

    public Date getEsigdate() {
        return this.esigdate;
    }

    public void setEsigdate(Date esigdate) {
        this.esigdate = esigdate;
    }

    public BigDecimal getFcaNumber() {
        return this.fcaNumber;
    }

    public void setFcaNumber(BigDecimal fcaNumber) {
        this.fcaNumber = fcaNumber;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLenderaddress() {
        return this.lenderaddress;
    }

    public void setLenderaddress(String lenderaddress) {
        this.lenderaddress = lenderaddress;
    }

    public String getLendername() {
        return this.lendername;
    }

    public void setLendername(String lendername) {
        this.lendername = lendername;
    }

    public BigDecimal getLowestAdvertisedRate() {
        return this.lowestAdvertisedRate;
    }

    public void setLowestAdvertisedRate(BigDecimal lowestAdvertisedRate) {
        this.lowestAdvertisedRate = lowestAdvertisedRate;
    }

    public String getMakemodel() {
        return this.makemodel;
    }

    public void setMakemodel(String makemodel) {
        this.makemodel = makemodel;
    }

    public String getNiNumber() {
        return this.niNumber;
    }

    public void setNiNumber(String niNumber) {
        this.niNumber = niNumber;
    }

    public String getOptionToPurchaseFee() {
        return this.optionToPurchaseFee;
    }

    public void setOptionToPurchaseFee(String optionToPurchaseFee) {
        this.optionToPurchaseFee = optionToPurchaseFee;
    }

    public String getPartExchange() {
        return this.partExchange;
    }

    public void setPartExchange(String partExchange) {
        this.partExchange = partExchange;
    }

    public String getPcpcode() {
        return this.pcpcode;
    }

    public void setPcpcode(String pcpcode) {
        this.pcpcode = pcpcode;
    }

    public Date getPurchaseDate() {
        return this.purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public BigDecimal getPurchasePrice() {
        return this.purchasePrice;
    }

    public void setPurchasePrice(BigDecimal purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getRegistration() {
        return this.registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getSetupFees() {
        return this.setupFees;
    }

    public void setSetupFees(BigDecimal setupFees) {
        this.setupFees = setupFees;
    }

    public BigDecimal getStatus() {
        return this.status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    public BigDecimal getTotalPayable() {
        return this.totalPayable;
    }

    public void setTotalPayable(BigDecimal totalPayable) {
        this.totalPayable = totalPayable;
    }

    public List<TblPcpdocument> getTblPcpdocuments() {
        return this.tblPcpdocuments;
    }

    public void setTblPcpdocuments(List<TblPcpdocument> tblPcpdocuments) {
        this.tblPcpdocuments = tblPcpdocuments;
    }

    public TblPcpdocument addTblPcpdocument(TblPcpdocument tblPcpdocument) {
        getTblPcpdocuments().add(tblPcpdocument);
        tblPcpdocument.setTblPcpclaim(this);

        return tblPcpdocument;
    }

    public TblPcpdocument removeTblPcpdocument(TblPcpdocument tblPcpdocument) {
        getTblPcpdocuments().remove(tblPcpdocument);
        tblPcpdocument.setTblPcpclaim(null);

        return tblPcpdocument;
    }

    public List<TblPcplog> getTblPcplogs() {
        return this.tblPcplogs;
    }

    public void setTblPcplogs(List<TblPcplog> tblPcplogs) {
        this.tblPcplogs = tblPcplogs;
    }

    public TblPcplog addTblPcplog(TblPcplog tblPcplog) {
        getTblPcplogs().add(tblPcplog);
        tblPcplog.setTblPcpclaim(this);

        return tblPcplog;
    }

    public TblPcplog removeTblPcplog(TblPcplog tblPcplog) {
        getTblPcplogs().remove(tblPcplog);
        tblPcplog.setTblPcpclaim(null);

        return tblPcplog;
    }

    public List<TblPcpmessage> getTblPcpmessages() {
        return this.tblPcpmessages;
    }

    public void setTblPcpmessages(List<TblPcpmessage> tblPcpmessages) {
        this.tblPcpmessages = tblPcpmessages;
    }

    public TblPcpmessage addTblPcpmessage(TblPcpmessage tblPcpmessage) {
        getTblPcpmessages().add(tblPcpmessage);
        tblPcpmessage.setTblPcpclaim(this);

        return tblPcpmessage;
    }

    public TblPcpmessage removeTblPcpmessage(TblPcpmessage tblPcpmessage) {
        getTblPcpmessages().remove(tblPcpmessage);
        tblPcpmessage.setTblPcpclaim(null);

        return tblPcpmessage;
    }

    public List<TblPcpnote> getTblPcpnotes() {
        return this.tblPcpnotes;
    }

    public void setTblPcpnotes(List<TblPcpnote> tblPcpnotes) {
        this.tblPcpnotes = tblPcpnotes;
    }

    public TblPcpnote addTblPcpnote(TblPcpnote tblPcpnote) {
        getTblPcpnotes().add(tblPcpnote);
        tblPcpnote.setTblPcpclaim(this);

        return tblPcpnote;
    }

    public TblPcpnote removeTblPcpnote(TblPcpnote tblPcpnote) {
        getTblPcpnotes().remove(tblPcpnote);
        tblPcpnote.setTblPcpclaim(null);

        return tblPcpnote;
    }

    public List<TblPcpsolicitor> getTblPcpsolicitors() {
        return this.tblPcpsolicitors;
    }

    public void setTblPcpsolicitors(List<TblPcpsolicitor> tblPcpsolicitors) {
        this.tblPcpsolicitors = tblPcpsolicitors;
    }

    public TblPcpsolicitor addTblPcpsolicitor(TblPcpsolicitor tblPcpsolicitor) {
        getTblPcpsolicitors().add(tblPcpsolicitor);
        tblPcpsolicitor.setTblPcpclaim(this);

        return tblPcpsolicitor;
    }

    public TblPcpsolicitor removeTblPcpsolicitor(TblPcpsolicitor tblPcpsolicitor) {
        getTblPcpsolicitors().remove(tblPcpsolicitor);
        tblPcpsolicitor.setTblPcpclaim(null);

        return tblPcpsolicitor;
    }

    public List<TblPcptask> getTblPcptasks() {
        return this.tblPcptasks;
    }

    public void setTblPcptasks(List<TblPcptask> tblPcptasks) {
        this.tblPcptasks = tblPcptasks;
    }

    public TblPcptask addTblPcptask(TblPcptask tblPcptask) {
        getTblPcptasks().add(tblPcptask);
        tblPcptask.setTblPcpclaim(this);

        return tblPcptask;
    }

    public TblPcptask removeTblPcptask(TblPcptask tblPcptask) {
        getTblPcptasks().remove(tblPcptask);
        tblPcptask.setTblPcpclaim(null);

        return tblPcptask;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }

    public List<HdrActionButton> getHdrActionButton() {
        return hdrActionButton;
    }

    public void setHdrActionButton(List<HdrActionButton> hdrActionButton) {
        this.hdrActionButton = hdrActionButton;
    }

    public String getContractualTerm() {
        return contractualTerm;
    }

    public void setContractualTerm(String contractualTerm) {
        this.contractualTerm = contractualTerm;
    }

    public String getEndOfAgreement() {
        return endOfAgreement;
    }

    public void setEndOfAgreement(String endOfAgreement) {
        this.endOfAgreement = endOfAgreement;
    }


    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public List<HdrActionButton> getHdrActionButtonForLA() {
        return hdrActionButtonForLA;
    }

    public void setHdrActionButtonForLA(List<HdrActionButton> hdrActionButtonForLA) {
        this.hdrActionButtonForLA = hdrActionButtonForLA;
    }
}