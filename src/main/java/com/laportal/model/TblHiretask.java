package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_HIRETASKS database table.
 * 
 */
@Entity
@Table(name="TBL_HIRETASKS")
@NamedQuery(name="TblHiretask.findAll", query="SELECT t FROM TblHiretask t")
public class TblHiretask implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_HIRETASKS_HIRETASKCODE_GENERATOR", sequenceName="TBL_HIRETASKS_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_HIRETASKS_HIRETASKCODE_GENERATOR")
	private long hiretaskcode;

	private String completedby;

	@Temporal(TemporalType.DATE)
	private Date completedon;

	@Temporal(TemporalType.DATE)
	private Date createdon;

	private String currenttask;

	private String lastupdateuser;

	private String remarks;

	private String status;


	@ManyToOne
	@JoinColumn(name="TASKCODE")
	private TblTask tblTask;

	//bi-directional many-to-one association to TblHireclaim
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="HIRECODE")
	private TblHireclaim tblHireclaim;

	public TblHiretask() {
	}

	public long getHiretaskcode() {
		return this.hiretaskcode;
	}

	public void setHiretaskcode(long hiretaskcode) {
		this.hiretaskcode = hiretaskcode;
	}

	public String getCompletedby() {
		return this.completedby;
	}

	public void setCompletedby(String completedby) {
		this.completedby = completedby;
	}

	public Date getCompletedon() {
		return this.completedon;
	}

	public void setCompletedon(Date completedon) {
		this.completedon = completedon;
	}

	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getCurrenttask() {
		return this.currenttask;
	}

	public void setCurrenttask(String currenttask) {
		this.currenttask = currenttask;
	}

	public String getLastupdateuser() {
		return this.lastupdateuser;
	}

	public void setLastupdateuser(String lastupdateuser) {
		this.lastupdateuser = lastupdateuser;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public TblTask getTblTask() {
		return tblTask;
	}

	public void setTblTask(TblTask tblTask) {
		this.tblTask = tblTask;
	}

	public TblHireclaim getTblHireclaim() {
		return this.tblHireclaim;
	}

	public void setTblHireclaim(TblHireclaim tblHireclaim) {
		this.tblHireclaim = tblHireclaim;
	}

}