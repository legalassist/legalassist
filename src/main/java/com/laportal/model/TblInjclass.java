package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_INJCLASS database table.
 */
@Entity
@Table(name = "TBL_INJCLASS")
@NamedQuery(name = "TblInjclass.findAll", query = "SELECT t FROM TblInjclass t")
public class TblInjclass implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_INJCLASS_INJCLASSCODE_GENERATOR", sequenceName = "SEQ_INJCLASS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_INJCLASS_INJCLASSCODE_GENERATOR")
    private long injclasscode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String injclassname;

    private String injlevel;

    private String remarks;

    private String status;

    private String usercode;

    private String airbag;

    private String injtype;

    //bi-directional many-to-one association to TblRtaclaim
    @JsonIgnore
    @OneToMany(mappedBy = "tblInjclass")
    private List<TblRtaclaim> tblRtaclaims;

    public TblInjclass() {
    }

    public long getInjclasscode() {
        return this.injclasscode;
    }

    public void setInjclasscode(long injclasscode) {
        this.injclasscode = injclasscode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getInjclassname() {
        return this.injclassname;
    }

    public void setInjclassname(String injclassname) {
        this.injclassname = injclassname;
    }

    public String getInjlevel() {
        return this.injlevel;
    }

    public void setInjlevel(String injlevel) {
        this.injlevel = injlevel;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public List<TblRtaclaim> getTblRtaclaims() {
        return this.tblRtaclaims;
    }

    public void setTblRtaclaims(List<TblRtaclaim> tblRtaclaims) {
        this.tblRtaclaims = tblRtaclaims;
    }

    public TblRtaclaim addTblRtaclaim(TblRtaclaim tblRtaclaim) {
        getTblRtaclaims().add(tblRtaclaim);
        tblRtaclaim.setTblInjclass(this);

        return tblRtaclaim;
    }

    public TblRtaclaim removeTblRtaclaim(TblRtaclaim tblRtaclaim) {
        getTblRtaclaims().remove(tblRtaclaim);
        tblRtaclaim.setTblInjclass(null);

        return tblRtaclaim;
    }

    public String getAirbag() {
        return airbag;
    }

    public void setAirbag(String airbag) {
        this.airbag = airbag;
    }

    public String getInjtype() {
        return injtype;
    }

    public void setInjtype(String injtype) {
        this.injtype = injtype;
    }
}