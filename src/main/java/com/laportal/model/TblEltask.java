package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_ELTASKS database table.
 */
@Entity
@Table(name = "TBL_ELTASKS")
@NamedQuery(name = "TblEltask.findAll", query = "SELECT t FROM TblEltask t")
public class TblEltask implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_ELTASKS_ELTASKCODE_GENERATOR", sequenceName = "SEQ_TBL_ELTASKS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_ELTASKS_ELTASKCODE_GENERATOR")
    private long eltaskcode;

    private String completedby;

    @Temporal(TemporalType.DATE)
    private Date completedon;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String currenttask;

    private String remarks;

    private String status;

    @ManyToOne
    @JoinColumn(name = "TASKCODE")
    private TblTask tblTask;

    //bi-directional many-to-one association to TblElclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ELCLAIMCODE")
    private TblElclaim tblElclaim;

    public TblEltask() {
    }

    public long getEltaskcode() {
        return this.eltaskcode;
    }

    public void setEltaskcode(long eltaskcode) {
        this.eltaskcode = eltaskcode;
    }

    public String getCompletedby() {
        return this.completedby;
    }

    public void setCompletedby(String completedby) {
        this.completedby = completedby;
    }

    public Date getCompletedon() {
        return this.completedon;
    }

    public void setCompletedon(Date completedon) {
        this.completedon = completedon;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getCurrenttask() {
        return this.currenttask;
    }

    public void setCurrenttask(String currenttask) {
        this.currenttask = currenttask;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TblTask getTblTask() {
        return tblTask;
    }

    public void setTblTask(TblTask tblTask) {
        this.tblTask = tblTask;
    }

    public TblElclaim getTblElclaim() {
        return this.tblElclaim;
    }

    public void setTblElclaim(TblElclaim tblElclaim) {
        this.tblElclaim = tblElclaim;
    }

}