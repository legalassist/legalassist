package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_EMAIL_TEMPLATES database table.
 */
@Entity
@Table(name = "TBL_EMAIL_TEMPLATES")
@NamedQuery(name = "TblEmailTemplate.findAll", query = "SELECT t FROM TblEmailTemplate t")
public class TblEmailTemplate implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_EMAIL_TEMPLATES_EMAILTEMPLATECODE_GENERATOR", sequenceName = "SEQ_TBL_EMAIL_TEMPLATES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_EMAIL_TEMPLATES_EMAILTEMPLATECODE_GENERATOR")
    private long emailtemplatecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String emailtemplate;

    private BigDecimal userccategorycode;

    private String emailtype;

    public TblEmailTemplate() {
    }

    public long getEmailtemplatecode() {
        return this.emailtemplatecode;
    }

    public void setEmailtemplatecode(long emailtemplatecode) {
        this.emailtemplatecode = emailtemplatecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getEmailtemplate() {
        return this.emailtemplate;
    }

    public void setEmailtemplate(String emailtemplate) {
        this.emailtemplate = emailtemplate;
    }

    public BigDecimal getUserccategorycode() {
        return this.userccategorycode;
    }

    public void setUserccategorycode(BigDecimal userccategorycode) {
        this.userccategorycode = userccategorycode;
    }

    public String getEmailtype() {
        return emailtype;
    }

    public void setEmailtype(String emailtype) {
        this.emailtype = emailtype;
    }
}