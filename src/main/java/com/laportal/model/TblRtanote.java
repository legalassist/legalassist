package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_RTANOTES database table.
 */
@Entity
@Table(name = "TBL_RTANOTES")
@NamedQuery(name = "TblRtanote.findAll", query = "SELECT t FROM TblRtanote t")
public class TblRtanote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTANOTES_RTANOTECODE_GENERATOR", sequenceName = "SEQ_RTANOTES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTANOTES_RTANOTECODE_GENERATOR")
    private long rtanotecode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdon;

    private String note;

    private String remarks;

    private String usercode;

    private String usercategorycode;

    //bi-directional many-to-one association to TblRtaclaim
    @ManyToOne
    @JoinColumn(name = "RTACODE")
    private TblRtaclaim tblRtaclaim;

    @Transient
    private String userName;

    @Transient
    private boolean self;

    @Transient
    List<TblRtanote> rtanotesLegalOnly = new ArrayList<>();

    public TblRtanote() {
    }

    public long getRtanotecode() {
        return this.rtanotecode;
    }

    public void setRtanotecode(long rtanotecode) {
        this.rtanotecode = rtanotecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblRtaclaim getTblRtaclaim() {
        return this.tblRtaclaim;
    }

    public void setTblRtaclaim(TblRtaclaim tblRtaclaim) {
        this.tblRtaclaim = tblRtaclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUsercategorycode() {
        return usercategorycode;
    }

    public void setUsercategorycode(String usercategorycode) {
        this.usercategorycode = usercategorycode;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }

    public List<TblRtanote> getRtanotesLegalOnly() {
        return rtanotesLegalOnly;
    }

    public void setRtanotesLegalOnly(List<TblRtanote> rtanotesLegalOnly) {
        this.rtanotesLegalOnly = rtanotesLegalOnly;
    }
}