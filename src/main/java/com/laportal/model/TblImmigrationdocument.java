package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the TBL_IMMIGRATIONDOCUMENTS database table.
 */
@Entity
@Table(name = "TBL_IMMIGRATIONDOCUMENTS")
@NamedQuery(name = "TblImmigrationdocument.findAll", query = "SELECT t FROM TblImmigrationdocument t")
public class TblImmigrationdocument implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_IMMIGRATIONDOCUMENTS_IMMIGRATIONDOCUMENTSCODE_GENERATOR", sequenceName = "SEQ_TBL_IMMIGRATIONDOCUMENTS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_IMMIGRATIONDOCUMENTS_IMMIGRATIONDOCUMENTSCODE_GENERATOR")
    private long immigrationdocumentscode;

    @Column(name = "DOCUMENT_PATH")
    private String documentPath;

    @Column(name = "DOCUMENT_TYPE")
    private String documentType;

    //bi-directional many-to-one association to TblImmigrationclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "IMMIGRATIONCLAIMCODE")
    private TblImmigrationclaim tblImmigrationclaim;

    public TblImmigrationdocument() {
    }

    public long getImmigrationdocumentscode() {
        return this.immigrationdocumentscode;
    }

    public void setImmigrationdocumentscode(long immigrationdocumentscode) {
        this.immigrationdocumentscode = immigrationdocumentscode;
    }

    public String getDocumentPath() {
        return this.documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDocumentType() {
        return this.documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public TblImmigrationclaim getTblImmigrationclaim() {
        return this.tblImmigrationclaim;
    }

    public void setTblImmigrationclaim(TblImmigrationclaim tblImmigrationclaim) {
        this.tblImmigrationclaim = tblImmigrationclaim;
    }

}