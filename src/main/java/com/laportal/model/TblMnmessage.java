package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_MNMESSAGES database table.
 */
@Entity
@Table(name = "TBL_MNMESSAGES")
@NamedQuery(name = "TblMnmessage.findAll", query = "SELECT t FROM TblMnmessage t")
public class TblMnmessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_MNMESSAGES_MNMESSAGECODE_GENERATOR", sequenceName = "SEQ_TBL_MNMESSAGES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_MNMESSAGES_MNMESSAGECODE_GENERATOR")
    private long code;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Lob
    private String message;

    private String remarks;

    private String sentto;

    private String usercode;

    //bi-directional many-to-one association to TblMnclaim
    @ManyToOne
    @JoinColumn(name = "MNCLAIMCODE")
    private TblMnclaim tblMnclaim;

    @Transient
    private String userName;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "EMAILCODE")
    private TblEmail emailcode;

    public TblMnmessage() {
    }

    public long getCode() {
        return this.code;
    }

    public void setCode(long mnmessagecode) {
        this.code = mnmessagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentto() {
        return this.sentto;
    }

    public void setSentto(String sentto) {
        this.sentto = sentto;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblMnclaim getTblMnclaim() {
        return this.tblMnclaim;
    }

    public void setTblMnclaim(TblMnclaim tblMnclaim) {
        this.tblMnclaim = tblMnclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public TblEmail getEmailcode() {
        return emailcode;
    }

    public void setEmailcode(TblEmail emailcode) {
        this.emailcode = emailcode;
    }
}