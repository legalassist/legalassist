package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the TBL_SOLICITOR_INVOICE_DETAIL database table.
 */
@Entity
@Table(name = "TBL_SOLICITOR_INVOICE_DETAIL")
@NamedQuery(name = "TblSolicitorInvoiceDetail.findAll", query = "SELECT t FROM TblSolicitorInvoiceDetail t")
public class TblSolicitorInvoiceDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_SOLICITOR_INVOICE_DETAIL_INVOICEDETAILID_GENERATOR", sequenceName = "SEQ_TBL_SOLICITOR_INVOICE_DETAIL", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_SOLICITOR_INVOICE_DETAIL_INVOICEDETAILID_GENERATOR")
    private long invoicedetailid;

    private String address1;

    private String address2;

    private String address3;

    private BigDecimal amount;

    private BigDecimal adjust;

    private BigDecimal casecode;

    private String casenumber;

    private String city;

    private String email;

    private String firstname;

    private String lastname;

    private String middlename;

    private String mobile;

    private String ninumber;

    private String postalcode;

    private String region;

    private String title;

    //bi-directional many-to-one association to TblSolicitorInvoiceHead
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "INVOICEHEADID")
    private TblSolicitorInvoiceHead tblSolicitorInvoiceHead;

    private Date createdate;

    private BigDecimal createuser;


    private Date updatedate;

    private String caseinjtype;

    @ManyToOne
    @JoinColumn(name = "COMPAIGNCODE")
    private TblCompaign tblCompaign;

    public TblSolicitorInvoiceDetail() {
    }

    public long getInvoicedetailid() {
        return this.invoicedetailid;
    }

    public void setInvoicedetailid(long invoicedetailid) {
        this.invoicedetailid = invoicedetailid;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return this.address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getCasecode() {
        return this.casecode;
    }

    public void setCasecode(BigDecimal casecode) {
        this.casecode = casecode;
    }

    public String getCasenumber() {
        return this.casenumber;
    }

    public void setCasenumber(String casenumber) {
        this.casenumber = casenumber;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return this.middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNinumber() {
        return this.ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getPostalcode() {
        return this.postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getRegion() {
        return this.region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TblSolicitorInvoiceHead getTblSolicitorInvoiceHead() {
        return this.tblSolicitorInvoiceHead;
    }

    public void setTblSolicitorInvoiceHead(TblSolicitorInvoiceHead tblSolicitorInvoiceHead) {
        this.tblSolicitorInvoiceHead = tblSolicitorInvoiceHead;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public BigDecimal getAdjust() {
        return adjust;
    }

    public void setAdjust(BigDecimal adjust) {
        this.adjust = adjust;
    }

    public String getCaseinjtype() {
        return caseinjtype;
    }

    public void setCaseinjtype(String caseinjtype) {
        this.caseinjtype = caseinjtype;
    }

    public TblCompaign getTblCompaign() {
        return tblCompaign;
    }

    public void setTblCompaign(TblCompaign tblCompaign) {
        this.tblCompaign = tblCompaign;
    }
}