package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HIRELOGS database table.
 */
@Entity
@Table(name = "TBL_HIRELOGS")
@NamedQuery(name = "TblHirelog.findAll", query = "SELECT t FROM TblHirelog t")
public class TblHirelog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HIRELOGS_HIRELOGCODE_GENERATOR", sequenceName = "TBL_HIRELOGS_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HIRELOGS_HIRELOGCODE_GENERATOR")
    private long hirelogcode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblHireclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HIRECODE")
    private TblHireclaim tblHireclaim;

    @Transient
    private String userName;

    private Long oldstatus;

    private Long newstatus;

    public TblHirelog() {
    }

    public long getHirelogcode() {
        return this.hirelogcode;
    }

    public void setHirelogcode(long hirelogcode) {
        this.hirelogcode = hirelogcode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblHireclaim getTblHireclaim() {
        return this.tblHireclaim;
    }

    public void setTblHireclaim(TblHireclaim tblHireclaim) {
        this.tblHireclaim = tblHireclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getOldstatus() {
        return oldstatus;
    }

    public void setOldstatus(Long oldstatus) {
        this.oldstatus = oldstatus;
    }

    public Long getNewstatus() {
        return newstatus;
    }

    public void setNewstatus(Long newstatus) {
        this.newstatus = newstatus;
    }
}