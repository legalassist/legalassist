package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_EMAIL database table.
 */
@Entity
@Table(name = "TBL_EMAIL")
@NamedQuery(name = "TblEmail.findAll", query = "SELECT t FROM TblEmail t")
public class TblEmail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_EMAIL_EMAILCODE_GENERATOR", sequenceName = "SEQ_TBL_EMAIL", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_EMAIL_EMAILCODE_GENERATOR")
    private long emailcode;

    private String ccemailaddress;

    private Date createdon;

    private String emailaddress;


    private String emailattachment;

    @Lob
    private String emailbody;

    private String emailsubject;

    private BigDecimal senflag;

    public TblEmail() {
    }

    public long getEmailcode() {
        return this.emailcode;
    }

    public void setEmailcode(long emailcode) {
        this.emailcode = emailcode;
    }

    public String getCcemailaddress() {
        return this.ccemailaddress;
    }

    public void setCcemailaddress(String ccemailaddress) {
        this.ccemailaddress = ccemailaddress;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getEmailaddress() {
        return this.emailaddress;
    }

    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }

    public String getEmailattachment() {
        return this.emailattachment;
    }

    public void setEmailattachment(String emailattachment) {
        this.emailattachment = emailattachment;
    }

    public String getEmailbody() {
        return this.emailbody;
    }

    public void setEmailbody(String emailbody) {
        this.emailbody = emailbody;
    }

    public String getEmailsubject() {
        return this.emailsubject;
    }

    public void setEmailsubject(String emailsubject) {
        this.emailsubject = emailsubject;
    }

    public BigDecimal getSenflag() {
        return this.senflag;
    }

    public void setSenflag(BigDecimal senflag) {
        this.senflag = senflag;
    }

}