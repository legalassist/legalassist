package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the TBL_USERCATEGORY database table.
 */
@Entity
@Table(name = "TBL_USERCATEGORY")
@NamedQuery(name = "TblUsercategory.findAll", query = "SELECT t FROM TblUsercategory t")
public class TblUsercategory implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_USERCATEGORY_CATEGORYCODE_GENERATOR", sequenceName = "SEQ_TBL_USERCATEGORY", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_USERCATEGORY_CATEGORYCODE_GENERATOR")
    private String categorycode;

    private String categoryname;

    private String status;



    public TblUsercategory() {
    }

    public String getCategorycode() {
        return this.categorycode;
    }

    public void setCategorycode(String categorycode) {
        this.categorycode = categorycode;
    }

    public String getCategoryname() {
        return this.categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}