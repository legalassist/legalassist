package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_PAGES database table.
 */
@Entity
@Table(name = "TBL_PAGES")
@NamedQuery(name = "TblPage.findAll", query = "SELECT t FROM TblPage t")
public class TblPage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
//	@SequenceGenerator(name="TBL_PAGES_PAGECODE_GENERATOR", sequenceName="SEQ_TBL_PAGES",allocationSize = 1)
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_PAGES_PAGECODE_GENERATOR")
    private String pagecode;

    private String compaigncode;

    private String companycode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String pagedescr;

    private String pageicon;

    private String pagename;

    private String pagepath;

    private String pagestatus;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblModule
    @ManyToOne
    @JoinColumn(name = "MODULECODE")
    private TblModule tblModule;

    //bi-directional many-to-one association to TblUserrole
    @JsonIgnore
    @OneToMany(mappedBy = "tblPage")
    private List<TblUserrole> tblUserroles;

    private String isnewcase;
    private String islist;

    public TblPage() {
    }

    public String getPagecode() {
        return this.pagecode;
    }

    public void setPagecode(String pagecode) {
        this.pagecode = pagecode;
    }

    public String getCompaigncode() {
        return this.compaigncode;
    }

    public void setCompaigncode(String compaigncode) {
        this.compaigncode = compaigncode;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getPagedescr() {
        return this.pagedescr;
    }

    public void setPagedescr(String pagedescr) {
        this.pagedescr = pagedescr;
    }

    public String getPageicon() {
        return this.pageicon;
    }

    public void setPageicon(String pageicon) {
        this.pageicon = pageicon;
    }

    public String getPagename() {
        return this.pagename;
    }

    public void setPagename(String pagename) {
        this.pagename = pagename;
    }

    public String getPagepath() {
        return this.pagepath;
    }

    public void setPagepath(String pagepath) {
        this.pagepath = pagepath;
    }

    public String getPagestatus() {
        return this.pagestatus;
    }

    public void setPagestatus(String pagestatus) {
        this.pagestatus = pagestatus;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblModule getTblModule() {
        return this.tblModule;
    }

    public void setTblModule(TblModule tblModule) {
        this.tblModule = tblModule;
    }

    public List<TblUserrole> getTblUserroles() {
        return this.tblUserroles;
    }

    public void setTblUserroles(List<TblUserrole> tblUserroles) {
        this.tblUserroles = tblUserroles;
    }

    public TblUserrole addTblUserrole(TblUserrole tblUserrole) {
        getTblUserroles().add(tblUserrole);
        tblUserrole.setTblPage(this);

        return tblUserrole;
    }

    public TblUserrole removeTblUserrole(TblUserrole tblUserrole) {
        getTblUserroles().remove(tblUserrole);
        tblUserrole.setTblPage(null);

        return tblUserrole;
    }

    public String getIsnewcase() {
        return isnewcase;
    }

    public void setIsnewcase(String isnewcase) {
        this.isnewcase = isnewcase;
    }

    public String getIslist() {
        return islist;
    }

    public void setIslist(String islist) {
        this.islist = islist;
    }
}