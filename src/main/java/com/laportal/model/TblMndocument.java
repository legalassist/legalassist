package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_MNDOCUMENTS database table.
 */
@Entity
@Table(name = "TBL_MNDOCUMENTS")
@NamedQuery(name = "TblMndocument.findAll", query = "SELECT t FROM TblMndocument t")
public class TblMndocument implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_MNDOCUMENTS_MNDOCUMENTSCODE_GENERATOR", sequenceName = "SEQ_TBL_MNDOCUMENTS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_MNDOCUMENTS_MNDOCUMENTSCODE_GENERATOR")
    private long mndocumentscode;

    @Column(name = "DOCUMENT_PATH")
    private String documentPath;

    @Column(name = "DOCUMENT_TYPE")
    private String doctype;

    //bi-directional many-to-one association to TblMnclaim
    @ManyToOne
    @JoinColumn(name = "MNCLAIMCODE")
    private TblMnclaim tblMnclaim;

    @JsonIgnore
    @Column(name = "CREATEDATE")
    private Date createddate;

    @Column(name = "DOCUMENT_NAME")
    private String docname;

    private String docurl;
    @JsonIgnore
    private String remarks;
    @JsonIgnore
    private String createuser;

    @Lob
    @Column(name = "DOCUMENT_BASE64")
    private String docbase64;

    // bi-directional many-to-one association to TblTasks

    @ManyToOne
    @JoinColumn(name = "TASKCODE")
    private TblTask tblTask;

    public TblMndocument() {
    }

    public long getMndocumentscode() {
        return this.mndocumentscode;
    }

    public void setMndocumentscode(long mndocumentscode) {
        this.mndocumentscode = mndocumentscode;
    }

    public String getDocumentPath() {
        return this.documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDoctype() {
        return this.doctype;
    }

    public void setDoctype(String documentType) {
        this.doctype = documentType;
    }

    public TblMnclaim getTblMnclaim() {
        return this.tblMnclaim;
    }

    public void setTblMnclaim(TblMnclaim tblMnclaim) {
        this.tblMnclaim = tblMnclaim;
    }


    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public String getDocurl() {
        return docurl;
    }

    public void setDocurl(String docurl) {
        this.docurl = docurl;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getDocbase64() {
        return docbase64;
    }

    public void setDocbase64(String docbase64) {
        this.docbase64 = docbase64;
    }

    public TblTask getTblTask() {
        return tblTask;
    }

    public void setTblTask(TblTask tblTask) {
        this.tblTask = tblTask;
    }
}