package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRTASKS database table.
 */
@Entity
@Table(name = "TBL_HDRTASKS")
@NamedQuery(name = "TblHdrtask.findAll", query = "SELECT t FROM TblHdrtask t")
public class TblHdrtask implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRTASKS_HDRTASKCODE_GENERATOR", sequenceName = "SEQ_TBL_HDRTASKS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRTASKS_HDRTASKCODE_GENERATOR")
    private long hdrtaskcode;

    private String completedby;

    @Temporal(TemporalType.DATE)
    private Date completedon;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String currenttask;

    private String remarks;

    private String status;

    private BigDecimal taskcode;

    @Transient
    private TblTask tblTask;

    //bi-directional many-to-one association to
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HDRCODE")
    private TblHdrclaim tblHdrclaim;

    public TblHdrtask() {
    }

    public long getHdrtaskcode() {
        return this.hdrtaskcode;
    }

    public void setHdrtaskcode(long hdrtaskcode) {
        this.hdrtaskcode = hdrtaskcode;
    }

    public String getCompletedby() {
        return this.completedby;
    }

    public void setCompletedby(String completedby) {
        this.completedby = completedby;
    }

    public Date getCompletedon() {
        return this.completedon;
    }

    public void setCompletedon(Date completedon) {
        this.completedon = completedon;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getCurrenttask() {
        return this.currenttask;
    }

    public void setCurrenttask(String currenttask) {
        this.currenttask = currenttask;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getTaskcode() {
        return this.taskcode;
    }

    public void setTaskcode(BigDecimal taskcode) {
        this.taskcode = taskcode;
    }

    public TblHdrclaim getTblHdrclaim() {
        return tblHdrclaim;
    }

    public void setTblHdrclaim(TblHdrclaim tblHdrclaim) {
        this.tblHdrclaim = tblHdrclaim;
    }

    public TblTask getTblTask() {
        return tblTask;
    }

    public void setTblTask(TblTask tblTask) {
        this.tblTask = tblTask;
    }
}