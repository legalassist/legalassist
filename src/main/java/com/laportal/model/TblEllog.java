package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_ELLOGS database table.
 */
@Entity
@Table(name = "TBL_ELLOGS")
@NamedQuery(name = "TblEllog.findAll", query = "SELECT t FROM TblEllog t")
public class TblEllog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_ELLOGS_ELLOGCODE_GENERATOR", sequenceName = "SEQ_TBL_ELLOGS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_ELLOGS_ELLOGCODE_GENERATOR")
    private long ellogcode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblElclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ELCLAIMCODE")
    private TblElclaim tblElclaim;

    @Transient
    private String userName;


    private Long oldstatus;

    private Long newstatus;

    public TblEllog() {
    }

    public long getEllogcode() {
        return this.ellogcode;
    }

    public void setEllogcode(long ellogcode) {
        this.ellogcode = ellogcode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblElclaim getTblElclaim() {
        return this.tblElclaim;
    }

    public void setTblElclaim(TblElclaim tblElclaim) {
        this.tblElclaim = tblElclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getOldstatus() {
        return oldstatus;
    }

    public void setOldstatus(Long oldstatus) {
        this.oldstatus = oldstatus;
    }

    public Long getNewstatus() {
        return newstatus;
    }

    public void setNewstatus(Long newstatus) {
        this.newstatus = newstatus;
    }
}