package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HIREBUSINESSDETAIL database table.
 */
@Entity
@Table(name = "TBL_HIREBUSINESSDETAIL")
@NamedQuery(name = "TblHirebusinessdetail.findAll", query = "SELECT t FROM TblHirebusinessdetail t")
public class TblHirebusinessdetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HIREBUSINESSDETAIL_HIREBUSINESSDETAILCODE_GENERATOR", sequenceName = "TBL_HIREBUSINESSDETAIL_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HIREBUSINESSDETAIL_HIREBUSINESSDETAILCODE_GENERATOR")
    private long hirebusinessdetailcode;


    @Temporal(TemporalType.DATE)
    private Date bookingdate;

    private String bookingmode;

    private String createdby;

    @Temporal(TemporalType.DATE)
    private Date createdon;
    @Temporal(TemporalType.DATE)
    private Date hireenddate;

    @Temporal(TemporalType.DATE)
    private Date hirestartdate;

    private String outsourced;

    private String outsourcedto;

    private String reason;

    private String service;

    //bi-directional many-to-one association to TblHirebusiness
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "HIREBUSINESSCODE")
    private TblHirebusiness tblHirebusiness;

    public TblHirebusinessdetail() {
    }

    public long getHirebusinessdetailcode() {
        return this.hirebusinessdetailcode;
    }

    public void setHirebusinessdetailcode(long hirebusinessdetailcode) {
        this.hirebusinessdetailcode = hirebusinessdetailcode;
    }

    public Date getBookingdate() {
        return this.bookingdate;
    }

    public void setBookingdate(Date bookingdate) {
        this.bookingdate = bookingdate;
    }

    public String getBookingmode() {
        return this.bookingmode;
    }

    public void setBookingmode(String bookingmode) {
        this.bookingmode = bookingmode;
    }

    public String getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public Date getHireenddate() {
        return this.hireenddate;
    }

    public void setHireenddate(Date hireenddate) {
        this.hireenddate = hireenddate;
    }

    public Date getHirestartdate() {
        return this.hirestartdate;
    }

    public void setHirestartdate(Date hirestartdate) {
        this.hirestartdate = hirestartdate;
    }

    public String getOutsourced() {
        return this.outsourced;
    }

    public void setOutsourced(String outsourced) {
        this.outsourced = outsourced;
    }

    public String getOutsourcedto() {
        return this.outsourcedto;
    }

    public void setOutsourcedto(String outsourcedto) {
        this.outsourcedto = outsourcedto;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getService() {
        return this.service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public TblHirebusiness getTblHirebusiness() {
        return this.tblHirebusiness;
    }

    public void setTblHirebusiness(TblHirebusiness tblHirebusiness) {
        this.tblHirebusiness = tblHirebusiness;
    }

}