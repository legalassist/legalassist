package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_LOGINTOKEN database table.
 */
@Entity
@Table(name = "TBL_LOGINTOKEN")
@NamedQuery(name = "TblLogintoken.findAll", query = "SELECT t FROM TblLogintoken t")
public class TblLogintoken implements Serializable {
    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.DATE)
    private Date effectivefrom;

    @Temporal(TemporalType.DATE)
    private Date effectiveto;

    @Temporal(TemporalType.DATE)
    private Date logindate;

    @Temporal(TemporalType.DATE)
    private Date logoutdate;

    private String remarks;

    @Id
    @SequenceGenerator(name = "TBL_LOGINTOKEN_GENERATOR", sequenceName = "SEQ_TBL_TOKENVERIFICATION", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_LOGINTOKEN_GENERATOR")
    private long logintokenpk;

    private String token;

    private String usercode;

    public TblLogintoken() {
    }

    public Date getEffectivefrom() {
        return this.effectivefrom;
    }

    public void setEffectivefrom(Date effectivefrom) {
        this.effectivefrom = effectivefrom;
    }

    public Date getEffectiveto() {
        return this.effectiveto;
    }

    public void setEffectiveto(Date effectiveto) {
        this.effectiveto = effectiveto;
    }

    public Date getLogindate() {
        return this.logindate;
    }

    public void setLogindate(Date logindate) {
        this.logindate = logindate;
    }

    public Date getLogoutdate() {
        return this.logoutdate;
    }

    public void setLogoutdate(Date logoutdate) {
        this.logoutdate = logoutdate;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public long getLogintokenpk() {
        return logintokenpk;
    }

    public void setLogintokenpk(long logintokenpk) {
        this.logintokenpk = logintokenpk;
    }
}