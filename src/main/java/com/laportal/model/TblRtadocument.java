package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the TBL_RTADOCUMENTS database table.
 */
@Entity
@Table(name = "TBL_RTADOCUMENTS")
@NamedQuery(name = "TblRtadocument.findAll", query = "SELECT t FROM TblRtadocument t")
public class TblRtadocument implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTADOCUMENTS_RTADOCCODE_GENERATOR", sequenceName = "SEQ_RTADOCUMENTS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTADOCUMENTS_RTADOCCODE_GENERATOR")
    private long rtadoccode;


    @JsonIgnore
    private Date createdon;

    private String docname;

    private String doctype;

    private String docurl;
    @JsonIgnore
    private String remarks;
    @JsonIgnore
    private String usercode;
    @Lob
    private String docbase64;

    // bi-directional many-to-one association to TblRtaclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "RTACODE")
    private TblRtaclaim tblRtaclaim;

    // bi-directional many-to-one association to TblTasks

    @ManyToOne
    @JoinColumn(name = "TASKSCODE")
    private TblTask tblTask;


    public TblRtadocument() {
    }

    public long getRtadoccode() {
        return this.rtadoccode;
    }

    public void setRtadoccode(long rtadoccode) {
        this.rtadoccode = rtadoccode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDocname() {
        return this.docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public String getDoctype() {
        return this.doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getDocurl() {
        return this.docurl;
    }

    public void setDocurl(String docurl) {
        this.docurl = docurl;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblRtaclaim getTblRtaclaim() {
        return this.tblRtaclaim;
    }

    public void setTblRtaclaim(TblRtaclaim tblRtaclaim) {
        this.tblRtaclaim = tblRtaclaim;
    }

    public String getDocbase64() {
        return docbase64;
    }

    public void setDocbase64(String docbase64) {
        this.docbase64 = docbase64;
    }

    public TblTask getTblTask() {
        return tblTask;
    }

    public void setTblTask(TblTask tblTask) {
        this.tblTask = tblTask;
    }


}