package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.laportal.dto.LovResponse;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_USERS database table.
 */
@Entity
@Table(name = "TBL_USERS")
@NamedQuery(name = "TblUser.findAll", query = "SELECT t FROM TblUser t")
public class TblUser implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
//	@SequenceGenerator(name="TBL_USERS_USERCODE_GENERATOR", sequenceName="SEQ_TBL_USERS",allocationSize = 1)
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_USERS_USERCODE_GENERATOR")
    private String usercode;

    private String companycode;

    private String createdby;

    private Date createdon;

    private Date expireon;

    private String loginid;

    @JsonIgnore
    private String password;

    private String remarks;

    private String status;

    private String username;


    private Date pwdlastupdated;

    private String pwdupdateflag;

    //bi-directional many-to-one association to TblUserrole

    @OneToMany(mappedBy = "tblUser")
    private List<TblUserrole> tblUserroles;

    @Transient
    private List<LovResponse> lovResponse;

    public TblUser() {
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public Date getExpireon() {
        return this.expireon;
    }

    public void setExpireon(Date expireon) {
        this.expireon = expireon;
    }

    public String getLoginid() {
        return this.loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<TblUserrole> getTblUserroles() {
        return this.tblUserroles;
    }

    public void setTblUserroles(List<TblUserrole> tblUserroles) {
        this.tblUserroles = tblUserroles;
    }

    public TblUserrole addTblUserrole(TblUserrole tblUserrole) {
        getTblUserroles().add(tblUserrole);
        tblUserrole.setTblUser(this);

        return tblUserrole;
    }

    public TblUserrole removeTblUserrole(TblUserrole tblUserrole) {
        getTblUserroles().remove(tblUserrole);
        tblUserrole.setTblUser(null);

        return tblUserrole;
    }


    public List<LovResponse> getLovResponse() {
        return lovResponse;
    }

    public void setLovResponse(List<LovResponse> lovResponse) {
        this.lovResponse = lovResponse;
    }


    public Date getPwdlastupdated() {
        return pwdlastupdated;
    }

    public void setPwdlastupdated(Date pwdlastupdated) {
        this.pwdlastupdated = pwdlastupdated;
    }

    public String getPwdupdateflag() {
        return pwdupdateflag;
    }

    public void setPwdupdateflag(String pwdupdateflag) {
        this.pwdupdateflag = pwdupdateflag;
    }
}