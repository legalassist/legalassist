package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_PCPLOGS database table.
 */
@Entity
@Table(name = "TBL_PCPLOGS")
@NamedQuery(name = "TblPcplog.findAll", query = "SELECT t FROM TblPcplog t")
public class TblPcplog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PCPLOGS_PCPLOGCODE_GENERATOR", sequenceName = "SEQ_TBL_PCPLOGS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PCPLOGS_PCPLOGCODE_GENERATOR")
    private long pcplogcode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblPcpclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PCPCLAIMCODE")
    private TblPcpclaim tblPcpclaim;


    @Transient
    private String userName;

    public TblPcplog() {
    }

    public long getPcplogcode() {
        return this.pcplogcode;
    }

    public void setPcplogcode(long pcplogcode) {
        this.pcplogcode = pcplogcode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblPcpclaim getTblPcpclaim() {
        return this.tblPcpclaim;
    }

    public void setTblPcpclaim(TblPcpclaim tblPcpclaim) {
        this.tblPcpclaim = tblPcpclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}