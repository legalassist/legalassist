package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_PLLOGS database table.
 */
@Entity
@Table(name = "TBL_PLLOGS")
@NamedQuery(name = "TblPllog.findAll", query = "SELECT t FROM TblPllog t")
public class TblPllog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PLLOGS_PLLOGCODE_GENERATOR", sequenceName = "SEQ_TBL_PLLOGS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PLLOGS_PLLOGCODE_GENERATOR")
    private long pllogcode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblPlclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PLCLAIMCODE")
    private TblPlclaim tblPlclaim;

    @Transient
    private String userName;

    private Long oldstatus;

    private Long newstatus;

    public TblPllog() {
    }

    public long getPllogcode() {
        return this.pllogcode;
    }

    public void setPllogcode(long pllogcode) {
        this.pllogcode = pllogcode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblPlclaim getTblPlclaim() {
        return this.tblPlclaim;
    }

    public void setTblPlclaim(TblPlclaim tblPlclaim) {
        this.tblPlclaim = tblPlclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getOldstatus() {
        return oldstatus;
    }

    public void setOldstatus(Long oldstatus) {
        this.oldstatus = oldstatus;
    }

    public Long getNewstatus() {
        return newstatus;
    }

    public void setNewstatus(Long newstatus) {
        this.newstatus = newstatus;
    }
}