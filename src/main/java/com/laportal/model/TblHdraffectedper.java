package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRAFFECTEDPER database table.
 */
@Entity
@Table(name = "TBL_HDRAFFECTEDPER")
@NamedQuery(name = "TblHdraffectedper.findAll", query = "SELECT t FROM TblHdraffectedper t")
public class TblHdraffectedper implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRAFFECTEDPER_HDRAFFECTEDPERCODE_GENERATOR", sequenceName = "SEQ_TBL_HDRAFFECTEDPER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRAFFECTEDPER_HDRAFFECTEDPERCODE_GENERATOR")
    private long hdraffectedpercode;

    @Column(name = "EVIDENCE_DETAIL")
    private String evidenceDetail;

    @Column(name = "MEDICAL_EVIDENCE")
    private String medicalEvidence;


    @Temporal(TemporalType.DATE)
    private Date persondob;

    private String personname;

    @Column(name = "SUFFERING_FROM")
    private String sufferingFrom;

    //bi-directional many-to-one association to TblHdrclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HDRCLAIMCODE")
    private TblHdrclaim tblHdrclaim;

    @Column(name = "LASTUPDATEUSER")
    private String lastupdateuser;

    public TblHdraffectedper() {
    }

    public long getHdraffectedpercode() {
        return this.hdraffectedpercode;
    }

    public void setHdraffectedpercode(long hdraffectedpercode) {
        this.hdraffectedpercode = hdraffectedpercode;
    }

    public String getEvidenceDetail() {
        return this.evidenceDetail;
    }

    public void setEvidenceDetail(String evidenceDetail) {
        this.evidenceDetail = evidenceDetail;
    }

    public String getMedicalEvidence() {
        return this.medicalEvidence;
    }

    public void setMedicalEvidence(String medicalEvidence) {
        this.medicalEvidence = medicalEvidence;
    }

    public Date getPersondob() {
        return this.persondob;
    }

    public void setPersondob(Date persondob) {
        this.persondob = persondob;
    }

    public String getPersonname() {
        return this.personname;
    }

    public void setPersonname(String personname) {
        this.personname = personname;
    }

    public String getSufferingFrom() {
        return this.sufferingFrom;
    }

    public void setSufferingFrom(String sufferingFrom) {
        this.sufferingFrom = sufferingFrom;
    }

    public TblHdrclaim getTblHdrclaim() {
        return this.tblHdrclaim;
    }

    public void setTblHdrclaim(TblHdrclaim tblHdrclaim) {
        this.tblHdrclaim = tblHdrclaim;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }
}