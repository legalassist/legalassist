package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the TBL_HIREDOCUMENTS database table.
 */
@Entity
@Table(name = "TBL_HIREDOCUMENTS")
@NamedQuery(name = "TblHiredocument.findAll", query = "SELECT t FROM TblHiredocument t")
public class TblHiredocument implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HIREDOCUMENTS_HIREDOCCODE_GENERATOR", sequenceName = "TBL_HIREDOCUMENTS_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HIREDOCUMENTS_HIREDOCCODE_GENERATOR")
    private long hiredoccode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String docname;

    private String doctype;

    private String docurl;

    private String remarks;

    private String usercode;

    @Lob
    private String docbase64;

    // bi-directional many-to-one association to TblHireclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HIRECODE")
    private TblHireclaim tblHireclaim;

    @ManyToOne
    @JoinColumn(name = "TASKCODE")
    private TblTask tblTask;

    public TblHiredocument() {
    }

    public long getHiredoccode() {
        return this.hiredoccode;
    }

    public void setHiredoccode(long hiredoccode) {
        this.hiredoccode = hiredoccode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDocname() {
        return this.docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public String getDoctype() {
        return this.doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getDocurl() {
        return this.docurl;
    }

    public void setDocurl(String docurl) {
        this.docurl = docurl;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblHireclaim getTblHireclaim() {
        return this.tblHireclaim;
    }

    public void setTblHireclaim(TblHireclaim tblHireclaim) {
        this.tblHireclaim = tblHireclaim;
    }

    public String getDocbase64() {
        return docbase64;
    }

    public void setDocbase64(String docbase64) {
        this.docbase64 = docbase64;
    }

    public TblTask getTblTask() {
        return tblTask;
    }

    public void setTblTask(TblTask tblTask) {
        this.tblTask = tblTask;
    }
}