package com.laportal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The persistent class for the ONELINK database table.
 */
@Entity
@NamedQuery(name = "Onelink.findAll", query = "SELECT o FROM Onelink o")
public class Onelink implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "BANK_COMMENTS")
    private String bankComments;

    @Column(name = "BILL_AMOUNT")
    private String billAmount;

    private String comments;

    private String comments2;

    private String companycode;

    @Column(name = "CONSUMER#")
    private String consumer_;

    @Column(name = "DATE_DDMMYY")
    private String dateDdmmyy;

    @Column(name = "EXPECTED_RESULT_RESPONSE_CODE_RECEIVED")
    private String expectedResultResponseCodeReceived;

    @Id
    private BigDecimal id;

    @Column(name = "ONELINK_COMMENTS")
    private String onelinkComments;

    @Column(name = "STAN_AUTH_ID")
    private String stanAuthId;

    private String status;

    @Column(name = "TEST_CASE_NUMBER")
    private String testCaseNumber;

    @Column(name = "TEST_DESCRIPTION")
    private String testDescription;

    public Onelink() {
    }

    public String getBankComments() {
        return this.bankComments;
    }

    public void setBankComments(String bankComments) {
        this.bankComments = bankComments;
    }

    public String getBillAmount() {
        return this.billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments2() {
        return this.comments2;
    }

    public void setComments2(String comments2) {
        this.comments2 = comments2;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getConsumer_() {
        return this.consumer_;
    }

    public void setConsumer_(String consumer_) {
        this.consumer_ = consumer_;
    }

    public String getDateDdmmyy() {
        return this.dateDdmmyy;
    }

    public void setDateDdmmyy(String dateDdmmyy) {
        this.dateDdmmyy = dateDdmmyy;
    }

    public String getExpectedResultResponseCodeReceived() {
        return this.expectedResultResponseCodeReceived;
    }

    public void setExpectedResultResponseCodeReceived(String expectedResultResponseCodeReceived) {
        this.expectedResultResponseCodeReceived = expectedResultResponseCodeReceived;
    }

    public BigDecimal getId() {
        return this.id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getOnelinkComments() {
        return this.onelinkComments;
    }

    public void setOnelinkComments(String onelinkComments) {
        this.onelinkComments = onelinkComments;
    }

    public String getStanAuthId() {
        return this.stanAuthId;
    }

    public void setStanAuthId(String stanAuthId) {
        this.stanAuthId = stanAuthId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTestCaseNumber() {
        return this.testCaseNumber;
    }

    public void setTestCaseNumber(String testCaseNumber) {
        this.testCaseNumber = testCaseNumber;
    }

    public String getTestDescription() {
        return this.testDescription;
    }

    public void setTestDescription(String testDescription) {
        this.testDescription = testDescription;
    }

}