package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRCLAIMANT database table.
 */
@Entity
@Table(name = "TBL_HDRCLAIMANT")
@NamedQuery(name = "TblHdrclaimant.findAll", query = "SELECT t FROM TblHdrclaimant t")
public class TblHdrclaimant implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRCLAIMANT_HRDCLAIMANTCODE_GENERATOR", sequenceName = "SEQ_TBL_HDRCLAIMANT", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRCLAIMANT_HRDCLAIMANTCODE_GENERATOR")
    private long hrdclaimantcode;


    @Column(name = "C_ADDRESS1")
    private String caddress1;

    @Column(name = "C_ADDRESS2")
    private String caddress2;


    @Column(name = "C_ADDRESS3")
    private String caddress3;


    @Column(name = "C_CITY")
    private String ccity;

    @Column(name = "C_CONTACTTIME")
    private String ccontacttime;

    @Temporal(TemporalType.DATE)
    @Column(name = "C_DOB")
    private Date cdob;

    @Column(name = "C_EMAIL")
    private String cemail;

    @Column(name = "C_FNAME")
    private String cfname;

    @Column(name = "C_LANDLINE")
    private String clandline;

    @Column(name = "C_MNAME")
    private String cmname;

    @Column(name = "C_MOBILENO")
    private String cmobileno;

    @Column(name = "C_NINUMBER")
    private String cninumber;


    @Column(name = "C_POSTCODE")
    private String cpostalcode;


    @Column(name = "C_REGION")
    private String cregion;

    @Column(name = "C_SNAME")
    private String csname;

    @Column(name = "C_TITLE")
    private String ctitle;

    //bi-directional many-to-one association to TblHdrclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HDR_CLAIMCODE")
    private TblHdrclaim tblHdrclaim;

    @Column(name = "ISJOINTTENANCY")
    private String isJointTenancy;


    @Column(name = "LASTUPDATEUSER")
    private String lastupdateuser;


    public TblHdrclaimant() {
    }

    public long getHrdclaimantcode() {
        return hrdclaimantcode;
    }

    public void setHrdclaimantcode(long hrdclaimantcode) {
        this.hrdclaimantcode = hrdclaimantcode;
    }

    public String getCaddress1() {
        return caddress1;
    }

    public void setCaddress1(String caddress1) {
        this.caddress1 = caddress1;
    }

    public String getCaddress2() {
        return caddress2;
    }

    public void setCaddress2(String caddress2) {
        this.caddress2 = caddress2;
    }

    public String getCaddress3() {
        return caddress3;
    }

    public void setCaddress3(String caddress3) {
        this.caddress3 = caddress3;
    }

    public String getCcity() {
        return ccity;
    }

    public void setCcity(String ccity) {
        this.ccity = ccity;
    }

    public String getCcontacttime() {
        return ccontacttime;
    }

    public void setCcontacttime(String ccontacttime) {
        this.ccontacttime = ccontacttime;
    }

    public Date getCdob() {
        return cdob;
    }

    public void setCdob(Date cdob) {
        this.cdob = cdob;
    }

    public String getCemail() {
        return cemail;
    }

    public void setCemail(String cemail) {
        this.cemail = cemail;
    }

    public String getCfname() {
        return cfname;
    }

    public void setCfname(String cfname) {
        this.cfname = cfname;
    }

    public String getClandline() {
        return clandline;
    }

    public void setClandline(String clandline) {
        this.clandline = clandline;
    }

    public String getCmname() {
        return cmname;
    }

    public void setCmname(String cmname) {
        this.cmname = cmname;
    }

    public String getCmobileno() {
        return cmobileno;
    }

    public void setCmobileno(String cmobileno) {
        this.cmobileno = cmobileno;
    }

    public String getCninumber() {
        return cninumber;
    }

    public void setCninumber(String cninumber) {
        this.cninumber = cninumber;
    }

    public String getCpostalcode() {
        return cpostalcode;
    }

    public void setCpostalcode(String cpostalcode) {
        this.cpostalcode = cpostalcode;
    }

    public String getCregion() {
        return cregion;
    }

    public void setCregion(String cregion) {
        this.cregion = cregion;
    }

    public String getCsname() {
        return csname;
    }

    public void setCsname(String csname) {
        this.csname = csname;
    }

    public String getCtitle() {
        return ctitle;
    }

    public void setCtitle(String ctitle) {
        this.ctitle = ctitle;
    }

    public TblHdrclaim getTblHdrclaim() {
        return tblHdrclaim;
    }

    public void setTblHdrclaim(TblHdrclaim tblHdrclaim) {
        this.tblHdrclaim = tblHdrclaim;
    }

    public String getIsJointTenancy() {
        return isJointTenancy;
    }

    public void setIsJointTenancy(String isJointTenancy) {
        this.isJointTenancy = isJointTenancy;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }
}