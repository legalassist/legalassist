package com.laportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VIEW_RTACASEREPORT database table.
 * 
 */
@Entity
@Table(name="VIEW_RTACASEREPORT")
@NamedQuery(name="ViewRtacasereport.findAll", query="SELECT v FROM ViewRtacasereport v")
public class ViewRtacasereport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ACCIDENT_DATE")
	private String accidentDate;

	private String aml;

	private String cfa;

	@Column(name="CLIENT_NAME")
	private String clientName;

	@Column(name="CURRENT_TASK")
	private String currentTask;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_CREATED")
	private Date dateCreated;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_MODIFIED")
	private Date dateModified;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_OF_BIRTH")
	private Date dateOfBirth;

	@Lob
	private String injuries;

	@Column(name="INTRODUCER_COMPANY")
	private String introducerCompany;

	@Column(name="INTRODUCER_USER")
	private String introducerUser;

	@Temporal(TemporalType.DATE)
	@Column(name="INVOICE_DATE")
	private Date invoiceDate;

	@Column(name="NATIONAL_INSURANCE_NUMBER")
	private String nationalInsuranceNumber;

	private String ninumber;

	private String postalcode;

	@Column(name="PROOF_OF_ADDRESS")
	private String proofOfAddress;

	@Column(name="PROOF_OF_ID")
	private String proofOfId;

	@Column(name="REFERENCE_NUMBER")
	private String referenceNumber;

	@Column(name="REJECT_REASON")
	private String rejectReason;

	@Id
	private BigDecimal rtacode;

	@Column(name="SOLICITOR_COMPANY")
	private String solicitorCompany;

	@Column(name="SOLICITOR_USER")
	private String solicitorUser;

	private String status;

	@Temporal(TemporalType.DATE)
	@Column(name="SUBMITTED_ON")
	private Date submittedOn;

	public ViewRtacasereport() {
	}

	public String getAccidentDate() {
		return this.accidentDate;
	}

	public void setAccidentDate(String accidentDate) {
		this.accidentDate = accidentDate;
	}

	public String getAml() {
		return this.aml;
	}

	public void setAml(String aml) {
		this.aml = aml;
	}

	public String getCfa() {
		return this.cfa;
	}

	public void setCfa(String cfa) {
		this.cfa = cfa;
	}

	public String getClientName() {
		return this.clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCurrentTask() {
		return this.currentTask;
	}

	public void setCurrentTask(String currentTask) {
		this.currentTask = currentTask;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getInjuries() {
		return this.injuries;
	}

	public void setInjuries(String injuries) {
		this.injuries = injuries;
	}

	public String getIntroducerCompany() {
		return this.introducerCompany;
	}

	public void setIntroducerCompany(String introducerCompany) {
		this.introducerCompany = introducerCompany;
	}

	public String getIntroducerUser() {
		return this.introducerUser;
	}

	public void setIntroducerUser(String introducerUser) {
		this.introducerUser = introducerUser;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getNationalInsuranceNumber() {
		return this.nationalInsuranceNumber;
	}

	public void setNationalInsuranceNumber(String nationalInsuranceNumber) {
		this.nationalInsuranceNumber = nationalInsuranceNumber;
	}

	public String getNinumber() {
		return this.ninumber;
	}

	public void setNinumber(String ninumber) {
		this.ninumber = ninumber;
	}

	public String getPostalcode() {
		return this.postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getProofOfAddress() {
		return this.proofOfAddress;
	}

	public void setProofOfAddress(String proofOfAddress) {
		this.proofOfAddress = proofOfAddress;
	}

	public String getProofOfId() {
		return this.proofOfId;
	}

	public void setProofOfId(String proofOfId) {
		this.proofOfId = proofOfId;
	}

	public String getReferenceNumber() {
		return this.referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getRejectReason() {
		return this.rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public BigDecimal getRtacode() {
		return this.rtacode;
	}

	public void setRtacode(BigDecimal rtacode) {
		this.rtacode = rtacode;
	}

	public String getSolicitorCompany() {
		return this.solicitorCompany;
	}

	public void setSolicitorCompany(String solicitorCompany) {
		this.solicitorCompany = solicitorCompany;
	}

	public String getSolicitorUser() {
		return this.solicitorUser;
	}

	public void setSolicitorUser(String solicitorUser) {
		this.solicitorUser = solicitorUser;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getSubmittedOn() {
		return this.submittedOn;
	}

	public void setSubmittedOn(Date submittedOn) {
		this.submittedOn = submittedOn;
	}

}