package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The persistent class for the VIEW_INVOICE_LINES database table.
 */
@Entity
@Table(name = "VIEW_INVOICE_LINES")
@NamedQuery(name = "ViewInvoiceLine.findAll", query = "SELECT v FROM ViewInvoiceLine v")
public class ViewInvoiceLine implements Serializable {
    private static final long serialVersionUID = 1L;

    private BigDecimal adjust;

    private BigDecimal amount;

    private String charge;

    @Column(name = "DATE_CREATED")
    private String dateCreated;

    private String description;

    @Id
    private String invoicecode;

    private String source;

    private String statuscode;

    @Column(name = "URL_LINK")
    private String urlLink;

    public ViewInvoiceLine() {
    }

    public BigDecimal getAdjust() {
        return this.adjust;
    }

    public void setAdjust(BigDecimal adjust) {
        this.adjust = adjust;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCharge() {
        return this.charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getDateCreated() {
        return this.dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInvoicecode() {
        return this.invoicecode;
    }

    public void setInvoicecode(String invoicecode) {
        this.invoicecode = invoicecode;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getStatuscode() {
        return this.statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getUrlLink() {
        return this.urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }

}