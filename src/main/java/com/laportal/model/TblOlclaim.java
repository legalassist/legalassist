package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.laportal.dto.RtaActionButton;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_OLCLAIM database table.
 */
@Entity
@Table(name = "TBL_OLCLAIM")
@NamedQuery(name = "TblOlclaim.findAll", query = "SELECT t FROM TblOlclaim t")
public class TblOlclaim implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_OLCLAIM_OLCLAIMCODE_GENERATOR", sequenceName = "SEQ_TBL_OLCLAIM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_OLCLAIM_OLCLAIMCODE_GENERATOR")
    private long olclaimcode;

    @Temporal(TemporalType.DATE)
    private Date accdatetime;

    private String accdescription;

    private String acclocation;

    private String accreportedto;

    private String title;

    private String firstname;

    private String middlename;

    private String lastname;

    private String postalcode;

    private String address1;

    private String address2;

    private String address3;

    private String city;

    private String region;

    private Date createdate;

    private BigDecimal createuser;

    private String describeinjuries;

    @Temporal(TemporalType.DATE)
    private Date dob;

    private String email;

    private String esig;

    private Date esigdate;


    private String injuriesduration;

    private String landline;

    private String medicalattention;

    private String mobile;

    private String ninumber;

    private String occupation;

    private String olcode;

    private String password;

    private String remarks;

    private BigDecimal status;

    private Date updatedate;

    private String witnesses;
    private String anywitnesses;

    //bi-directional many-to-one association to TblOllog
    @OneToMany(mappedBy = "tblOlclaim")
    private List<TblOllog> tblOllogs;

    //bi-directional many-to-one association to TblOlmessage
    @OneToMany(mappedBy = "tblOlclaim")
    private List<TblOlmessage> tblOlmessages;

    //bi-directional many-to-one association to TblOlnote
    @OneToMany(mappedBy = "tblOlclaim")
    private List<TblOlnote> tblOlnotes;

    //bi-directional many-to-one association to TblOlsolicitor
    @OneToMany(mappedBy = "tblOlclaim")
    private List<TblOlsolicitor> tblOlsolicitors;


    //bi-directional many-to-one association to TblOldocument
    @OneToMany(mappedBy = "tblOlclaim")
    private List<TblOldocument> tblOldocuments;

    private Long advisor;
    private Long introducer;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "UTC")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "CLAWBACK_DATE")
    private Date clawbackDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "SUBMIT_DATE")
    private Date submitDate;

    private String acctime;

    private String lastupdateuser;

    @Transient
    private String statusDescr;

    @Transient
    private List<RtaActionButton> olActionButtons;

    @Transient
    private List<RtaActionButton> olActionButtonForLA;

    @Transient
    private List<TblOltask> tblOltasks;

    @Transient
    private String solicitorcompany;
    @Transient
    private String solicitorusername;
    @Transient
    private String advisorname;
    @Transient
    private String introducername;
    @Transient
    private Date introducerInvoiceDate;
    @Transient
    private BigDecimal introducerInvoiceHeadId;
    @Transient
    private Date solicitorInvoiceDate;
    @Transient
    private BigDecimal solicitorInvoiceHeadId;
    @Transient
    private TblEsignStatus tblEsignStatus;
    @Transient
    private String taskflag;
    @Transient
    private String editFlag;

    public TblOlclaim() {
    }

    public long getOlclaimcode() {
        return this.olclaimcode;
    }

    public void setOlclaimcode(long olclaimcode) {
        this.olclaimcode = olclaimcode;
    }

    public Date getAccdatetime() {
        return this.accdatetime;
    }

    public void setAccdatetime(Date accdatetime) {
        this.accdatetime = accdatetime;
    }

    public String getAccdescription() {
        return this.accdescription;
    }

    public void setAccdescription(String accdescription) {
        this.accdescription = accdescription;
    }

    public String getAcclocation() {
        return this.acclocation;
    }

    public void setAcclocation(String acclocation) {
        this.acclocation = acclocation;
    }

    public String getAccreportedto() {
        return this.accreportedto;
    }

    public void setAccreportedto(String accreportedto) {
        this.accreportedto = accreportedto;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public String getDescribeinjuries() {
        return this.describeinjuries;
    }

    public void setDescribeinjuries(String describeinjuries) {
        this.describeinjuries = describeinjuries;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEsig() {
        return this.esig;
    }

    public void setEsig(String esig) {
        this.esig = esig;
    }

    public Date getEsigdate() {
        return this.esigdate;
    }

    public void setEsigdate(Date esigdate) {
        this.esigdate = esigdate;
    }

    public String getInjuriesduration() {
        return this.injuriesduration;
    }

    public void setInjuriesduration(String injuriesduration) {
        this.injuriesduration = injuriesduration;
    }

    public String getLandline() {
        return this.landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getMedicalattention() {
        return this.medicalattention;
    }

    public void setMedicalattention(String medicalattention) {
        this.medicalattention = medicalattention;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNinumber() {
        return this.ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getOccupation() {
        return this.occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOlcode() {
        return this.olcode;
    }

    public void setOlcode(String olcode) {
        this.olcode = olcode;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getStatus() {
        return this.status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    public Date getUpdatedate() {
        return this.updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public String getWitnesses() {
        return this.witnesses;
    }

    public void setWitnesses(String witnesses) {
        this.witnesses = witnesses;
    }

    public List<TblOllog> getTblOllogs() {
        return this.tblOllogs;
    }

    public void setTblOllogs(List<TblOllog> tblOllogs) {
        this.tblOllogs = tblOllogs;
    }

    public TblOllog addTblOllog(TblOllog tblOllog) {
        getTblOllogs().add(tblOllog);
        tblOllog.setTblOlclaim(this);

        return tblOllog;
    }

    public TblOllog removeTblOllog(TblOllog tblOllog) {
        getTblOllogs().remove(tblOllog);
        tblOllog.setTblOlclaim(null);

        return tblOllog;
    }

    public List<TblOlmessage> getTblOlmessages() {
        return this.tblOlmessages;
    }

    public void setTblOlmessages(List<TblOlmessage> tblOlmessages) {
        this.tblOlmessages = tblOlmessages;
    }

    public TblOlmessage addTblOlmessage(TblOlmessage tblOlmessage) {
        getTblOlmessages().add(tblOlmessage);
        tblOlmessage.setTblOlclaim(this);

        return tblOlmessage;
    }

    public TblOlmessage removeTblOlmessage(TblOlmessage tblOlmessage) {
        getTblOlmessages().remove(tblOlmessage);
        tblOlmessage.setTblOlclaim(null);

        return tblOlmessage;
    }

    public List<TblOlnote> getTblOlnotes() {
        return this.tblOlnotes;
    }

    public void setTblOlnotes(List<TblOlnote> tblOlnotes) {
        this.tblOlnotes = tblOlnotes;
    }

    public TblOlnote addTblOlnote(TblOlnote tblOlnote) {
        getTblOlnotes().add(tblOlnote);
        tblOlnote.setTblOlclaim(this);

        return tblOlnote;
    }

    public TblOlnote removeTblOlnote(TblOlnote tblOlnote) {
        getTblOlnotes().remove(tblOlnote);
        tblOlnote.setTblOlclaim(null);

        return tblOlnote;
    }

    public List<TblOlsolicitor> getTblOlsolicitors() {
        return this.tblOlsolicitors;
    }

    public void setTblOlsolicitors(List<TblOlsolicitor> tblOlsolicitors) {
        this.tblOlsolicitors = tblOlsolicitors;
    }

    public TblOlsolicitor addTblOlsolicitor(TblOlsolicitor tblOlsolicitor) {
        getTblOlsolicitors().add(tblOlsolicitor);
        tblOlsolicitor.setTblOlclaim(this);

        return tblOlsolicitor;
    }

    public TblOlsolicitor removeTblOlsolicitor(TblOlsolicitor tblOlsolicitor) {
        getTblOlsolicitors().remove(tblOlsolicitor);
        tblOlsolicitor.setTblOlclaim(null);

        return tblOlsolicitor;
    }

    public List<TblOltask> getTblOltasks() {
        return this.tblOltasks;
    }

    public void setTblOltasks(List<TblOltask> tblOltasks) {
        this.tblOltasks = tblOltasks;
    }

    public TblOltask addTblOltask(TblOltask tblOltask) {
        getTblOltasks().add(tblOltask);
        tblOltask.setTblOlclaim(this);

        return tblOltask;
    }

    public TblOltask removeTblOltask(TblOltask tblOltask) {
        getTblOltasks().remove(tblOltask);
        tblOltask.setTblOlclaim(null);

        return tblOltask;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }


    public List<TblOldocument> getTblOldocuments() {
        return tblOldocuments;
    }

    public void setTblOldocuments(List<TblOldocument> tblOldocuments) {
        this.tblOldocuments = tblOldocuments;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Long getAdvisor() {
        return advisor;
    }

    public void setAdvisor(Long advisor) {
        this.advisor = advisor;
    }

    public Long getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Long introducer) {
        this.introducer = introducer;
    }

    public Date getClawbackDate() {
        return clawbackDate;
    }

    public void setClawbackDate(Date clawbackDate) {
        this.clawbackDate = clawbackDate;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public List<RtaActionButton> getOlActionButtons() {
        return olActionButtons;
    }

    public void setOlActionButtons(List<RtaActionButton> olActionButtons) {
        this.olActionButtons = olActionButtons;
    }

    public List<RtaActionButton> getOlActionButtonForLA() {
        return olActionButtonForLA;
    }

    public void setOlActionButtonForLA(List<RtaActionButton> olActionButtonForLA) {
        this.olActionButtonForLA = olActionButtonForLA;
    }

    public String getSolicitorcompany() {
        return solicitorcompany;
    }

    public void setSolicitorcompany(String solicitorcompany) {
        this.solicitorcompany = solicitorcompany;
    }

    public String getSolicitorusername() {
        return solicitorusername;
    }

    public void setSolicitorusername(String solicitorusername) {
        this.solicitorusername = solicitorusername;
    }

    public String getAdvisorname() {
        return advisorname;
    }

    public void setAdvisorname(String advisorname) {
        this.advisorname = advisorname;
    }

    public String getIntroducername() {
        return introducername;
    }

    public void setIntroducername(String introducername) {
        this.introducername = introducername;
    }

    public Date getIntroducerInvoiceDate() {
        return introducerInvoiceDate;
    }

    public void setIntroducerInvoiceDate(Date introducerInvoiceDate) {
        this.introducerInvoiceDate = introducerInvoiceDate;
    }

    public BigDecimal getIntroducerInvoiceHeadId() {
        return introducerInvoiceHeadId;
    }

    public void setIntroducerInvoiceHeadId(BigDecimal introducerInvoiceHeadId) {
        this.introducerInvoiceHeadId = introducerInvoiceHeadId;
    }

    public Date getSolicitorInvoiceDate() {
        return solicitorInvoiceDate;
    }

    public void setSolicitorInvoiceDate(Date solicitorInvoiceDate) {
        this.solicitorInvoiceDate = solicitorInvoiceDate;
    }

    public BigDecimal getSolicitorInvoiceHeadId() {
        return solicitorInvoiceHeadId;
    }

    public void setSolicitorInvoiceHeadId(BigDecimal solicitorInvoiceHeadId) {
        this.solicitorInvoiceHeadId = solicitorInvoiceHeadId;
    }

    public TblEsignStatus getTblEsignStatus() {
        return tblEsignStatus;
    }

    public void setTblEsignStatus(TblEsignStatus tblEsignStatus) {
        this.tblEsignStatus = tblEsignStatus;
    }

    public String getTaskflag() {
        return taskflag;
    }

    public void setTaskflag(String taskflag) {
        this.taskflag = taskflag;
    }

    public String getEditFlag() {
        return editFlag;
    }

    public void setEditFlag(String editFlag) {
        this.editFlag = editFlag;
    }

    public String getAcctime() {
        return acctime;
    }

    public void setAcctime(String acctime) {
        this.acctime = acctime;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }

    public String getAnywitnesses() {
        return anywitnesses;
    }

    public void setAnywitnesses(String anywitnesses) {
        this.anywitnesses = anywitnesses;
    }
}