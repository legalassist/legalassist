package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the TBL_PCPDOCUMENTS database table.
 */
@Entity
@Table(name = "TBL_PCPDOCUMENTS")
@NamedQuery(name = "TblPcpdocument.findAll", query = "SELECT t FROM TblPcpdocument t")
public class TblPcpdocument implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PCPDOCUMENTS_PCPDOCUMENTSCODE_GENERATOR", sequenceName = "SEQ_TBL_PCPDOCUMENTS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PCPDOCUMENTS_PCPDOCUMENTSCODE_GENERATOR")
    private long pcpdocumentscode;

    @Column(name = "DOCUMENT_PATH")
    private String documentPath;

    @Column(name = "DOCUMENT_TYPE")
    private String documentType;

    //bi-directional many-to-one association to TblPcpclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PCPCLAIMCODE")
    private TblPcpclaim tblPcpclaim;

    public TblPcpdocument() {
    }

    public long getPcpdocumentscode() {
        return this.pcpdocumentscode;
    }

    public void setPcpdocumentscode(long pcpdocumentscode) {
        this.pcpdocumentscode = pcpdocumentscode;
    }

    public String getDocumentPath() {
        return this.documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDocumentType() {
        return this.documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public TblPcpclaim getTblPcpclaim() {
        return this.tblPcpclaim;
    }

    public void setTblPcpclaim(TblPcpclaim tblPcpclaim) {
        this.tblPcpclaim = tblPcpclaim;
    }

}