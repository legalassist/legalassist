package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_DBNOTES database table.
 */
@Entity
@Table(name = "TBL_DBNOTES")
@NamedQuery(name = "TblDbnote.findAll", query = "SELECT t FROM TblDbnote t")
public class TblDbnote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_DBNOTES_DBNOTECODE_GENERATOR", sequenceName = "SEQ_TBL_DBNOTES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_DBNOTES_DBNOTECODE_GENERATOR")
    private long dbnotecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String note;

    private String remarks;

    private String usercategorycode;

    private String usercode;

    //bi-directional many-to-one association to TblDbclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "DBCLAIMCODE")
    private TblDbclaim tblDbclaim;

    @Transient
    private String userName;

    @Transient
    private boolean self;

    public TblDbnote() {
    }

    public long getDbnotecode() {
        return this.dbnotecode;
    }

    public void setDbnotecode(long dbnotecode) {
        this.dbnotecode = dbnotecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercategorycode() {
        return this.usercategorycode;
    }

    public void setUsercategorycode(String usercategorycode) {
        this.usercategorycode = usercategorycode;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblDbclaim getTblDbclaim() {
        return this.tblDbclaim;
    }

    public void setTblDbclaim(TblDbclaim tblDbclaim) {
        this.tblDbclaim = tblDbclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }
}