package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_OLMESSAGES database table.
 */
@Entity
@Table(name = "TBL_OLMESSAGES")
@NamedQuery(name = "TblOlmessage.findAll", query = "SELECT t FROM TblOlmessage t")
public class TblOlmessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_OLMESSAGES_OLMESSAGECODE_GENERATOR", sequenceName = "SEQ_TBL_OLMESSAGES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_OLMESSAGES_OLMESSAGECODE_GENERATOR")
    private long code;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Lob
    @Column(name = "MESSAGE")
    private String message;

    private String remarks;

    private String sentto;

    private String usercode;

    //bi-directional many-to-one association to TblOlclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "OLCLAIMCODE")
    private TblOlclaim tblOlclaim;

    @Transient
    private String userName;


    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "EMAILCODE")
    private TblEmail emailcode;

    public TblOlmessage() {
    }

    public long getCode() {
        return this.code;
    }

    public void setCode(long olmessagecode) {
        this.code = olmessagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentto() {
        return this.sentto;
    }

    public void setSentto(String sentto) {
        this.sentto = sentto;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblOlclaim getTblOlclaim() {
        return this.tblOlclaim;
    }

    public void setTblOlclaim(TblOlclaim tblOlclaim) {
        this.tblOlclaim = tblOlclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public TblEmail getEmailcode() {
        return emailcode;
    }

    public void setEmailcode(TblEmail emailcode) {
        this.emailcode = emailcode;
    }
}