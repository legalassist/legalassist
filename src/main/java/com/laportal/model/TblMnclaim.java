package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.laportal.dto.RtaActionButton;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_MNCLAIM database table.
 */
@Entity
@Table(name = "TBL_MNCLAIM")
@NamedQuery(name = "TblMnclaim.findAll", query = "SELECT t FROM TblMnclaim t")
public class TblMnclaim implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_MNCLAIM_MNCLAIMCODE_GENERATOR", sequenceName = "SEQ_TBL_MNCLAIM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_MNCLAIM_MNCLAIMCODE_GENERATOR")
    private long mnclaimcode;
    private String anynotes;
    private String anyotherlosses;
    @Temporal(TemporalType.DATE)
    private Date calldate;
    private String calltime;
    private String clientcontactno;
    @Temporal(TemporalType.DATE)
    private Date clientdob;
    private String clientemail;
    private String clientninumber;
    private String clientoccupation;
    private String complaintmade;
    private String complaintresponded;
    private Date createdate;
    private BigDecimal createuser;
    private String emergencysrvcattend;
    private String esign;
    private Date esigndate;
    private String gpdetails;
    private String hospitalattend;
    private String hospitaldetails;
    @Temporal(TemporalType.DATE)
    private Date incidentdatetime;
    private String injuries;
    private String location;
    @Temporal(TemporalType.DATE)
    private Date medicalattenddate;
    private String mncode;
    private String negligencedescr;
    private String notesdetail;
    private String other;
    private String password;
    private String policeref;
    private String remarks;
    private String separatecomplaintcontact;
    private BigDecimal status;
    private String stilloffwork;
    private String timeoffwork;
    private String timesincecomplaint;
    private String tpaddress;
    private String tpcompany;
    private String tpcontactno;
    private String tpname;
    private Date updatedate;
    private String witnesscontactdetails;
    private String witnesses;
    private String witnessname;
    //bi-directional many-to-one association to TblMndocument
    @OneToMany(mappedBy = "tblMnclaim")
    private List<TblMndocument> tblMndocuments;

    //bi-directional many-to-one association to TblMnlog
    @OneToMany(mappedBy = "tblMnclaim")
    private List<TblMnlog> tblMnlogs;

    //bi-directional many-to-one association to TblMnmessage
    @OneToMany(mappedBy = "tblMnclaim")
    private List<TblMnmessage> tblMnmessages;

    //bi-directional many-to-one association to TblMnnote
    @OneToMany(mappedBy = "tblMnclaim")
    private List<TblMnnote> tblMnnotes;

    //bi-directional many-to-one association to TblMnsolicitor
    @OneToMany(mappedBy = "tblMnclaim")
    private List<TblMnsolicitor> tblMnsolicitors;

    @Transient
    private List<TblMntask> tblMntasks;
    @Transient
    private String statusDescr;
    private String hotkeyed;
    private String refundedclientinformed;
    private String agentadviced;
    private String gdpr;

    private String title;

    private String firstname;

    private String middlename;

    private String lastname;

    private String postalcode;

    private String address1;

    private String address2;

    private String address3;

    private String city;

    private String region;
    private String acctime;

    @Transient
    private List<RtaActionButton> mnActionButtons;

    @Transient
    private List<RtaActionButton> mnActionButtonForLA;

    private Long advisor;
    private Long introducer;

    private String lastupdateuser;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "UTC")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "CLAWBACK_DATE")
    private Date clawbackDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "SUBMIT_DATE")
    private Date submitDate;

    @Transient
    private String solicitorcompany;
    @Transient
    private String solicitorusername;
    @Transient
    private String advisorname;
    @Transient
    private String introducername;
    @Transient
    private Date introducerInvoiceDate;
    @Transient
    private BigDecimal introducerInvoiceHeadId;
    @Transient
    private Date solicitorInvoiceDate;
    @Transient
    private BigDecimal solicitorInvoiceHeadId;
    @Transient
    private TblEsignStatus tblEsignStatus;
    @Transient
    private String taskflag;
    @Transient
    private String editFlag;




    public TblMnclaim() {
    }

    public long getMnclaimcode() {
        return this.mnclaimcode;
    }

    public void setMnclaimcode(long mnclaimcode) {
        this.mnclaimcode = mnclaimcode;
    }

    public String getAnynotes() {
        return this.anynotes;
    }

    public void setAnynotes(String anynotes) {
        this.anynotes = anynotes;
    }

    public String getAnyotherlosses() {
        return this.anyotherlosses;
    }

    public void setAnyotherlosses(String anyotherlosses) {
        this.anyotherlosses = anyotherlosses;
    }

    public Date getCalldate() {
        return this.calldate;
    }

    public void setCalldate(Date calldate) {
        this.calldate = calldate;
    }

    public String getCalltime() {
        return this.calltime;
    }

    public void setCalltime(String calltime) {
        this.calltime = calltime;
    }

    public String getClientcontactno() {
        return this.clientcontactno;
    }

    public void setClientcontactno(String clientcontactno) {
        this.clientcontactno = clientcontactno;
    }

    public Date getClientdob() {
        return this.clientdob;
    }

    public void setClientdob(Date clientdob) {
        this.clientdob = clientdob;
    }

    public String getClientemail() {
        return this.clientemail;
    }

    public void setClientemail(String clientemail) {
        this.clientemail = clientemail;
    }

    public String getClientninumber() {
        return this.clientninumber;
    }

    public void setClientninumber(String clientninumber) {
        this.clientninumber = clientninumber;
    }

    public String getClientoccupation() {
        return this.clientoccupation;
    }

    public void setClientoccupation(String clientoccupation) {
        this.clientoccupation = clientoccupation;
    }

    public String getComplaintmade() {
        return this.complaintmade;
    }

    public void setComplaintmade(String complaintmade) {
        this.complaintmade = complaintmade;
    }

    public String getComplaintresponded() {
        return this.complaintresponded;
    }

    public void setComplaintresponded(String complaintresponded) {
        this.complaintresponded = complaintresponded;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public String getEmergencysrvcattend() {
        return this.emergencysrvcattend;
    }

    public void setEmergencysrvcattend(String emergencysrvcattend) {
        this.emergencysrvcattend = emergencysrvcattend;
    }

    public String getEsign() {
        return this.esign;
    }

    public void setEsign(String esign) {
        this.esign = esign;
    }

    public Date getEsigndate() {
        return this.esigndate;
    }

    public void setEsigndate(Date esigndate) {
        this.esigndate = esigndate;
    }

    public String getGpdetails() {
        return this.gpdetails;
    }

    public void setGpdetails(String gpdetails) {
        this.gpdetails = gpdetails;
    }

    public String getHospitalattend() {
        return this.hospitalattend;
    }

    public void setHospitalattend(String hospitalattend) {
        this.hospitalattend = hospitalattend;
    }

    public String getHospitaldetails() {
        return this.hospitaldetails;
    }

    public void setHospitaldetails(String hospitaldetails) {
        this.hospitaldetails = hospitaldetails;
    }

    public Date getIncidentdatetime() {
        return this.incidentdatetime;
    }

    public void setIncidentdatetime(Date incidentdatetime) {
        this.incidentdatetime = incidentdatetime;
    }

    public String getInjuries() {
        return this.injuries;
    }

    public void setInjuries(String injuries) {
        this.injuries = injuries;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getMedicalattenddate() {
        return this.medicalattenddate;
    }

    public void setMedicalattenddate(Date medicalattenddate) {
        this.medicalattenddate = medicalattenddate;
    }

    public String getMncode() {
        return this.mncode;
    }

    public void setMncode(String mncode) {
        this.mncode = mncode;
    }

    public String getNegligencedescr() {
        return this.negligencedescr;
    }

    public void setNegligencedescr(String negligencedescr) {
        this.negligencedescr = negligencedescr;
    }

    public String getNotesdetail() {
        return this.notesdetail;
    }

    public void setNotesdetail(String notesdetail) {
        this.notesdetail = notesdetail;
    }

    public String getOther() {
        return this.other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPoliceref() {
        return this.policeref;
    }

    public void setPoliceref(String policeref) {
        this.policeref = policeref;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSeparatecomplaintcontact() {
        return this.separatecomplaintcontact;
    }

    public void setSeparatecomplaintcontact(String separatecomplaintcontact) {
        this.separatecomplaintcontact = separatecomplaintcontact;
    }

    public BigDecimal getStatus() {
        return this.status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    public String getStilloffwork() {
        return this.stilloffwork;
    }

    public void setStilloffwork(String stilloffwork) {
        this.stilloffwork = stilloffwork;
    }

    public String getTimeoffwork() {
        return this.timeoffwork;
    }

    public void setTimeoffwork(String timeoffwork) {
        this.timeoffwork = timeoffwork;
    }

    public String getTimesincecomplaint() {
        return this.timesincecomplaint;
    }

    public void setTimesincecomplaint(String timesincecomplaint) {
        this.timesincecomplaint = timesincecomplaint;
    }

    public String getTpaddress() {
        return this.tpaddress;
    }

    public void setTpaddress(String tpaddress) {
        this.tpaddress = tpaddress;
    }

    public String getTpcompany() {
        return this.tpcompany;
    }

    public void setTpcompany(String tpcompany) {
        this.tpcompany = tpcompany;
    }

    public String getTpcontactno() {
        return this.tpcontactno;
    }

    public void setTpcontactno(String tpcontactno) {
        this.tpcontactno = tpcontactno;
    }

    public String getTpname() {
        return this.tpname;
    }

    public void setTpname(String tpname) {
        this.tpname = tpname;
    }

    public Date getUpdatedate() {
        return this.updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public String getWitnesscontactdetails() {
        return this.witnesscontactdetails;
    }

    public void setWitnesscontactdetails(String witnesscontactdetails) {
        this.witnesscontactdetails = witnesscontactdetails;
    }

    public String getWitnesses() {
        return this.witnesses;
    }

    public void setWitnesses(String witnesses) {
        this.witnesses = witnesses;
    }

    public String getWitnessname() {
        return this.witnessname;
    }

    public void setWitnessname(String witnessname) {
        this.witnessname = witnessname;
    }

    public List<TblMndocument> getTblMndocuments() {
        return this.tblMndocuments;
    }

    public void setTblMndocuments(List<TblMndocument> tblMndocuments) {
        this.tblMndocuments = tblMndocuments;
    }

    public TblMndocument addTblMndocument(TblMndocument tblMndocument) {
        getTblMndocuments().add(tblMndocument);
        tblMndocument.setTblMnclaim(this);

        return tblMndocument;
    }

    public TblMndocument removeTblMndocument(TblMndocument tblMndocument) {
        getTblMndocuments().remove(tblMndocument);
        tblMndocument.setTblMnclaim(null);

        return tblMndocument;
    }

    public List<TblMnlog> getTblMnlogs() {
        return this.tblMnlogs;
    }

    public void setTblMnlogs(List<TblMnlog> tblMnlogs) {
        this.tblMnlogs = tblMnlogs;
    }

    public TblMnlog addTblMnlog(TblMnlog tblMnlog) {
        getTblMnlogs().add(tblMnlog);
        tblMnlog.setTblMnclaim(this);

        return tblMnlog;
    }

    public TblMnlog removeTblMnlog(TblMnlog tblMnlog) {
        getTblMnlogs().remove(tblMnlog);
        tblMnlog.setTblMnclaim(null);

        return tblMnlog;
    }

    public List<TblMnmessage> getTblMnmessages() {
        return this.tblMnmessages;
    }

    public void setTblMnmessages(List<TblMnmessage> tblMnmessages) {
        this.tblMnmessages = tblMnmessages;
    }

    public TblMnmessage addTblMnmessage(TblMnmessage tblMnmessage) {
        getTblMnmessages().add(tblMnmessage);
        tblMnmessage.setTblMnclaim(this);

        return tblMnmessage;
    }

    public TblMnmessage removeTblMnmessage(TblMnmessage tblMnmessage) {
        getTblMnmessages().remove(tblMnmessage);
        tblMnmessage.setTblMnclaim(null);

        return tblMnmessage;
    }

    public List<TblMnnote> getTblMnnotes() {
        return this.tblMnnotes;
    }

    public void setTblMnnotes(List<TblMnnote> tblMnnotes) {
        this.tblMnnotes = tblMnnotes;
    }

    public TblMnnote addTblMnnote(TblMnnote tblMnnote) {
        getTblMnnotes().add(tblMnnote);
        tblMnnote.setTblMnclaim(this);

        return tblMnnote;
    }

    public TblMnnote removeTblMnnote(TblMnnote tblMnnote) {
        getTblMnnotes().remove(tblMnnote);
        tblMnnote.setTblMnclaim(null);

        return tblMnnote;
    }

    public List<TblMnsolicitor> getTblMnsolicitors() {
        return this.tblMnsolicitors;
    }

    public void setTblMnsolicitors(List<TblMnsolicitor> tblMnsolicitors) {
        this.tblMnsolicitors = tblMnsolicitors;
    }

    public TblMnsolicitor addTblMnsolicitor(TblMnsolicitor tblMnsolicitor) {
        getTblMnsolicitors().add(tblMnsolicitor);
        tblMnsolicitor.setTblMnclaim(this);

        return tblMnsolicitor;
    }

    public TblMnsolicitor removeTblMnsolicitor(TblMnsolicitor tblMnsolicitor) {
        getTblMnsolicitors().remove(tblMnsolicitor);
        tblMnsolicitor.setTblMnclaim(null);

        return tblMnsolicitor;
    }

    public List<TblMntask> getTblMntasks() {
        return this.tblMntasks;
    }

    public void setTblMntasks(List<TblMntask> tblMntasks) {
        this.tblMntasks = tblMntasks;
    }

    public TblMntask addTblMntask(TblMntask tblMntask) {
        getTblMntasks().add(tblMntask);
        tblMntask.setTblMnclaim(this);

        return tblMntask;
    }

    public TblMntask removeTblMntask(TblMntask tblMntask) {
        getTblMntasks().remove(tblMntask);
        tblMntask.setTblMnclaim(null);

        return tblMntask;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }

    public String getHotkeyed() {
        return hotkeyed;
    }

    public void setHotkeyed(String hotkeyed) {
        this.hotkeyed = hotkeyed;
    }

    public String getRefundedclientinformed() {
        return refundedclientinformed;
    }

    public void setRefundedclientinformed(String refundedclientinformed) {
        this.refundedclientinformed = refundedclientinformed;
    }

    public String getAgentadviced() {
        return agentadviced;
    }

    public void setAgentadviced(String agentadviced) {
        this.agentadviced = agentadviced;
    }

    public String getGdpr() {
        return gdpr;
    }

    public void setGdpr(String gdpr) {
        this.gdpr = gdpr;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public List<RtaActionButton> getMnActionButtons() {
        return mnActionButtons;
    }

    public void setMnActionButtons(List<RtaActionButton> mnActionButtons) {
        this.mnActionButtons = mnActionButtons;
    }

    public List<RtaActionButton> getMnActionButtonForLA() {
        return mnActionButtonForLA;
    }

    public void setMnActionButtonForLA(List<RtaActionButton> mnActionButtonForLA) {
        this.mnActionButtonForLA = mnActionButtonForLA;
    }

    public Long getAdvisor() {
        return advisor;
    }

    public void setAdvisor(Long advisor) {
        this.advisor = advisor;
    }

    public Long getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Long introducer) {
        this.introducer = introducer;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }

    public Date getClawbackDate() {
        return clawbackDate;
    }

    public void setClawbackDate(Date clawbackDate) {
        this.clawbackDate = clawbackDate;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public String getSolicitorcompany() {
        return solicitorcompany;
    }

    public void setSolicitorcompany(String solicitorcompany) {
        this.solicitorcompany = solicitorcompany;
    }

    public String getSolicitorusername() {
        return solicitorusername;
    }

    public void setSolicitorusername(String solicitorusername) {
        this.solicitorusername = solicitorusername;
    }

    public String getAdvisorname() {
        return advisorname;
    }

    public void setAdvisorname(String advisorname) {
        this.advisorname = advisorname;
    }

    public String getIntroducername() {
        return introducername;
    }

    public void setIntroducername(String introducername) {
        this.introducername = introducername;
    }

    public Date getIntroducerInvoiceDate() {
        return introducerInvoiceDate;
    }

    public void setIntroducerInvoiceDate(Date introducerInvoiceDate) {
        this.introducerInvoiceDate = introducerInvoiceDate;
    }

    public BigDecimal getIntroducerInvoiceHeadId() {
        return introducerInvoiceHeadId;
    }

    public void setIntroducerInvoiceHeadId(BigDecimal introducerInvoiceHeadId) {
        this.introducerInvoiceHeadId = introducerInvoiceHeadId;
    }

    public Date getSolicitorInvoiceDate() {
        return solicitorInvoiceDate;
    }

    public void setSolicitorInvoiceDate(Date solicitorInvoiceDate) {
        this.solicitorInvoiceDate = solicitorInvoiceDate;
    }

    public BigDecimal getSolicitorInvoiceHeadId() {
        return solicitorInvoiceHeadId;
    }

    public void setSolicitorInvoiceHeadId(BigDecimal solicitorInvoiceHeadId) {
        this.solicitorInvoiceHeadId = solicitorInvoiceHeadId;
    }

    public TblEsignStatus getTblEsignStatus() {
        return tblEsignStatus;
    }

    public void setTblEsignStatus(TblEsignStatus tblEsignStatus) {
        this.tblEsignStatus = tblEsignStatus;
    }

    public String getTaskflag() {
        return taskflag;
    }

    public void setTaskflag(String taskflag) {
        this.taskflag = taskflag;
    }

    public String getEditFlag() {
        return editFlag;
    }

    public void setEditFlag(String editFlag) {
        this.editFlag = editFlag;
    }

    public String getAcctime() {
        return acctime;
    }

    public void setAcctime(String acctime) {
        this.acctime = acctime;
    }
}