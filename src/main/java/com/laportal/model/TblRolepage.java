package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_ROLEPAGES database table.
 */
@Entity
@Table(name = "TBL_ROLEPAGES")
@NamedQuery(name = "TblRolepage.findAll", query = "SELECT t FROM TblRolepage t")
public class TblRolepage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_ROLEPAGES_ROLEPAGECODE_GENERATOR", sequenceName = "SEQ_TBL_ROLEPAGES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_ROLEPAGES_ROLEPAGECODE_GENERATOR")
    private long rolepagecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String status;

    private String usercode;

    //bi-directional many-to-one association to TblPage
    @ManyToOne
    @JoinColumn(name = "PAGECODE")
    private TblPage tblPage;

    //bi-directional many-to-one association to TblRole
    @ManyToOne
    @JoinColumn(name = "ROLECODE")
    private TblRole tblRole;

    public TblRolepage() {
    }

    public long getRolepagecode() {
        return this.rolepagecode;
    }

    public void setRolepagecode(long rolepagecode) {
        this.rolepagecode = rolepagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblPage getTblPage() {
        return this.tblPage;
    }

    public void setTblPage(TblPage tblPage) {
        this.tblPage = tblPage;
    }

    public TblRole getTblRole() {
        return this.tblRole;
    }

    public void setTblRole(TblRole tblRole) {
        this.tblRole = tblRole;
    }

}