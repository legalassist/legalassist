package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_PCPMESSAGES database table.
 */
@Entity
@Table(name = "TBL_PCPMESSAGES")
@NamedQuery(name = "TblPcpmessage.findAll", query = "SELECT t FROM TblPcpmessage t")
public class TblPcpmessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PCPMESSAGES_PCPMESSAGECODE_GENERATOR", sequenceName = "SEQ_TBL_PCPMESSAGES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PCPMESSAGES_PCPMESSAGECODE_GENERATOR")
    private long pcpmessagecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Column(name = "\"MESSAGE\"")
    private String message;

    private String remarks;

    private String sentto;

    private String usercode;

    //bi-directional many-to-one association to TblPcpclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PCPCLAIMCODE")
    private TblPcpclaim tblPcpclaim;

    @Transient
    private String userName;

    public TblPcpmessage() {
    }

    public long getPcpmessagecode() {
        return this.pcpmessagecode;
    }

    public void setPcpmessagecode(long pcpmessagecode) {
        this.pcpmessagecode = pcpmessagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentto() {
        return this.sentto;
    }

    public void setSentto(String sentto) {
        this.sentto = sentto;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblPcpclaim getTblPcpclaim() {
        return this.tblPcpclaim;
    }

    public void setTblPcpclaim(TblPcpclaim tblPcpclaim) {
        this.tblPcpclaim = tblPcpclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}