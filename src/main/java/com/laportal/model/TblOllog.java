package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_OLLOGS database table.
 */
@Entity
@Table(name = "TBL_OLLOGS")
@NamedQuery(name = "TblOllog.findAll", query = "SELECT t FROM TblOllog t")
public class TblOllog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_OLLOGS_OLLOGCODE_GENERATOR", sequenceName = "SEQ_TBL_OLLOGS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_OLLOGS_OLLOGCODE_GENERATOR")
    private long ollogcode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblOlclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "OLCLAIMCODE")
    private TblOlclaim tblOlclaim;

    @Transient
    private String userName;
    private Long oldstatus;

    private Long newstatus;

    public TblOllog() {
    }

    public long getOllogcode() {
        return this.ollogcode;
    }

    public void setOllogcode(long ollogcode) {
        this.ollogcode = ollogcode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblOlclaim getTblOlclaim() {
        return this.tblOlclaim;
    }

    public void setTblOlclaim(TblOlclaim tblOlclaim) {
        this.tblOlclaim = tblOlclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getOldstatus() {
        return oldstatus;
    }

    public void setOldstatus(Long oldstatus) {
        this.oldstatus = oldstatus;
    }

    public Long getNewstatus() {
        return newstatus;
    }

    public void setNewstatus(Long newstatus) {
        this.newstatus = newstatus;
    }
}