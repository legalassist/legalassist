package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRDOCUMENTS database table.
 */
@Entity
@Table(name = "TBL_HDRDOCUMENTS")
@NamedQuery(name = "TblHdrdocument.findAll", query = "SELECT t FROM TblHdrdocument t")
public class TblHdrdocument implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRDOCUMENTS_HDRDOCUMENTSCODE_GENERATOR", sequenceName = "SEQ_TBL_HDRDOCUMENTS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRDOCUMENTS_HDRDOCUMENTSCODE_GENERATOR")
    private long hdrdocumentscode;

    @JsonProperty("docurl")
    @Column(name = "DOCUMENT_PATH")
    private String documentPath;

    @Column(name = "DOCUMENT_TYPE")
    private String documentType;

    //bi-directional many-to-one association to TblHdrclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HDRCLAIMCODE")
    private TblHdrclaim tblHdrclaim;

    @JsonIgnore
    private Date createdon;

    private String docname;

    @JsonIgnore
    private String remarks;
    @JsonIgnore
    private String usercode;
    @Lob
    private String docbase64;
    @ManyToOne
    @JoinColumn(name = "TASKCODE")
    private TblTask tblTask;

    public TblHdrdocument() {
    }

    public long getHdrdocumentscode() {
        return this.hdrdocumentscode;
    }

    public void setHdrdocumentscode(long hdrdocumentscode) {
        this.hdrdocumentscode = hdrdocumentscode;
    }

    public String getDocumentPath() {
        return this.documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDocumentType() {
        return this.documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public TblHdrclaim getTblHdrclaim() {
        return tblHdrclaim;
    }

    public void setTblHdrclaim(TblHdrclaim tblHdrclaim) {
        this.tblHdrclaim = tblHdrclaim;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getDocbase64() {
        return docbase64;
    }

    public void setDocbase64(String docbase64) {
        this.docbase64 = docbase64;
    }

    public TblTask getTblTask() {
        return tblTask;
    }

    public void setTblTask(TblTask tblTask) {
        this.tblTask = tblTask;
    }
}