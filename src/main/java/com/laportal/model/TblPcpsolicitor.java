package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_PCPSOLICITOR database table.
 */
@Entity
@Table(name = "TBL_PCPSOLICITOR")
@NamedQuery(name = "TblPcpsolicitor.findAll", query = "SELECT t FROM TblPcpsolicitor t")
public class TblPcpsolicitor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PCPSOLICITOR_PCPSOLCODE_GENERATOR", sequenceName = "SEQ_TBL_PCPSOLICITOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PCPSOLICITOR_PCPSOLCODE_GENERATOR")
    private long pcpsolcode;

    private String companycode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String reference1;

    private String reference2;

    private String remarks;

    private String status;

    private String usercode;

    //bi-directional many-to-one association to TblPcpclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PCPCLAIMCODE")
    private TblPcpclaim tblPcpclaim;

    @Transient
    private TblCompanyprofile tblCompanyprofile;

    public TblPcpsolicitor() {
    }

    public long getPcpsolcode() {
        return this.pcpsolcode;
    }

    public void setPcpsolcode(long pcpsolcode) {
        this.pcpsolcode = pcpsolcode;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getReference1() {
        return this.reference1;
    }

    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getReference2() {
        return this.reference2;
    }

    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblPcpclaim getTblPcpclaim() {
        return this.tblPcpclaim;
    }

    public void setTblPcpclaim(TblPcpclaim tblPcpclaim) {
        this.tblPcpclaim = tblPcpclaim;
    }

    public TblCompanyprofile getTblCompanyprofile() {
        return tblCompanyprofile;
    }

    public void setTblCompanyprofile(TblCompanyprofile tblCompanyprofile) {
        this.tblCompanyprofile = tblCompanyprofile;
    }
}