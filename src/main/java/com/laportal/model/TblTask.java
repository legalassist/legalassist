package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the TBL_TASKS database table.
 */
@Entity
@Table(name = "TBL_TASKS")
@NamedQuery(name = "TblTask.findAll", query = "SELECT t FROM TblTask t")
public class TblTask implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_TASKS_TASKCODE_GENERATOR", sequenceName = "SEQ_TASKS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_TASKS_TASKCODE_GENERATOR")
    private long taskcode;

    private String agetype;

    private String crdate;

    private String descr;

    private String name;

    private String userid;

    //bi-directional many-to-one association to TblRtatask
    @JsonIgnore
    @OneToMany(mappedBy = "tblTask")
    private List<TblRtatask> tblRtatasks;

    //bi-directional many-to-one association to TblCompaign
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "COMPAIGNCODE")
    private TblCompaign tblCompaign;

    private String mandatory;


    private String fileupload;


    public TblTask() {
    }

    public long getTaskcode() {
        return this.taskcode;
    }

    public void setTaskcode(long taskcode) {
        this.taskcode = taskcode;
    }

    public String getAgetype() {
        return this.agetype;
    }

    public void setAgetype(String agetype) {
        this.agetype = agetype;
    }

    public String getCrdate() {
        return this.crdate;
    }

    public void setCrdate(String crdate) {
        this.crdate = crdate;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public List<TblRtatask> getTblRtatasks() {
        return this.tblRtatasks;
    }

    public void setTblRtatasks(List<TblRtatask> tblRtatasks) {
        this.tblRtatasks = tblRtatasks;
    }

    public TblRtatask addTblRtatask(TblRtatask tblRtatask) {
        getTblRtatasks().add(tblRtatask);
        tblRtatask.setTblTask(this);

        return tblRtatask;
    }

    public TblRtatask removeTblRtatask(TblRtatask tblRtatask) {
        getTblRtatasks().remove(tblRtatask);
        tblRtatask.setTblTask(null);

        return tblRtatask;
    }

    public TblCompaign getTblCompaign() {
        return this.tblCompaign;
    }

    public void setTblCompaign(TblCompaign tblCompaign) {
        this.tblCompaign = tblCompaign;
    }

    public String getFileupload() {
        return fileupload;
    }

    public void setFileupload(String fileupload) {
        this.fileupload = fileupload;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }
}