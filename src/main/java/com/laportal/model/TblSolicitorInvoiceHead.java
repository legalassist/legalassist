package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_SOLICITOR_INVOICE_HEAD database table.
 */
@Entity
@Table(name = "TBL_SOLICITOR_INVOICE_HEAD")
@NamedQuery(name = "TblSolicitorInvoiceHead.findAll", query = "SELECT t FROM TblSolicitorInvoiceHead t")
public class TblSolicitorInvoiceHead implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name="TBL_SOLICITOR_INVOICE_HEAD_INVOICEHEADID_GENERATOR", sequenceName="SEQ_TBL_SOLICITOR_INVOICE_HEAD",allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_SOLICITOR_INVOICE_HEAD_INVOICEHEADID_GENERATOR")
    private long invoiceheadid;

    private BigDecimal amount;

    private String compaigntype;

    private String companycode;

    private String companyname;

    @Temporal(TemporalType.DATE)
    private Date createdate;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private BigDecimal createuser;

    private String description;

    @Column(name="INVOICE_TYPE")
    private String invoiceType;

    private String invoicecode;

    private BigDecimal status;

    private String statuscode;

    @Temporal(TemporalType.DATE)
    private Date updatedate;

    private String usercode;

    private String username;

    @Column(name="VAT_AMOUNT")
    private Double vatAmount;

    //bi-directional many-to-one association to TblEmail
    @ManyToOne
    @JoinColumn(name="EMAILCODE")
    private TblEmail tblEmail;

    //bi-directional many-to-one association to TblSolicitorInvoiceDetail
    @OneToMany(mappedBy = "tblSolicitorInvoiceHead")
    private List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails;

    @Transient
    private String compaingcode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
    @Transient
    private Date duedate;

    public TblSolicitorInvoiceHead() {
    }

    public long getInvoiceheadid() {
        return this.invoiceheadid;
    }

    public void setInvoiceheadid(long invoiceheadid) {
        this.invoiceheadid = invoiceheadid;
    }

    public String getCompaigntype() {
        return this.compaigntype;
    }

    public void setCompaigntype(String compaigntype) {
        this.compaigntype = compaigntype;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getCompanyname() {
        return this.companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getInvoicecode() {
        return this.invoicecode;
    }

    public void setInvoicecode(String invoicecode) {
        this.invoicecode = invoicecode;
    }

    public BigDecimal getStatus() {
        return this.status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    public String getStatuscode() {
        return this.statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }


    public String getCompaingcode() {
        return compaingcode;
    }

    public void setCompaingcode(String compaingcode) {
        this.compaingcode = compaingcode;
    }

    public TblEmail getTblEmail() {
        return tblEmail;
    }

    public void setTblEmail(TblEmail tblEmail) {
        this.tblEmail = tblEmail;
    }

    public Date getDuedate() {
        return duedate;
    }

    public void setDuedate(Date duedate) {
        this.duedate = duedate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public Double getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(Double vatAmount) {
        this.vatAmount = vatAmount;
    }

    public List<TblSolicitorInvoiceDetail> getTblSolicitorInvoiceDetails() {
        return tblSolicitorInvoiceDetails;
    }

    public void setTblSolicitorInvoiceDetails(List<TblSolicitorInvoiceDetail> tblSolicitorInvoiceDetails) {
        this.tblSolicitorInvoiceDetails = tblSolicitorInvoiceDetails;
    }
}