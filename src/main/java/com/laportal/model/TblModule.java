package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_MODULES database table.
 */
@Entity
@Table(name = "TBL_MODULES")
@NamedQuery(name = "TblModule.findAll", query = "SELECT t FROM TblModule t")
public class TblModule implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
//	@SequenceGenerator(name="TBL_MODULES_MODULECODE_GENERATOR", sequenceName="SEQ_TBL_MODULES",allocationSize = 1)
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_MODULES_MODULECODE_GENERATOR")
    private String modulecode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String modulename;

    private String modulestatus;

    private String remarks;

    private String systemcode;

    private String usercode;

    //bi-directional many-to-one association to TblPage
    @JsonIgnore
    @OneToMany(mappedBy = "tblModule")
    private List<TblPage> tblPages;

    public TblModule() {
    }

    public String getModulecode() {
        return this.modulecode;
    }

    public void setModulecode(String modulecode) {
        this.modulecode = modulecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getModulename() {
        return this.modulename;
    }

    public void setModulename(String modulename) {
        this.modulename = modulename;
    }

    public String getModulestatus() {
        return this.modulestatus;
    }

    public void setModulestatus(String modulestatus) {
        this.modulestatus = modulestatus;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSystemcode() {
        return this.systemcode;
    }

    public void setSystemcode(String systemcode) {
        this.systemcode = systemcode;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public List<TblPage> getTblPages() {
        return this.tblPages;
    }

    public void setTblPages(List<TblPage> tblPages) {
        this.tblPages = tblPages;
    }

    public TblPage addTblPage(TblPage tblPage) {
        getTblPages().add(tblPage);
        tblPage.setTblModule(this);

        return tblPage;
    }

    public TblPage removeTblPage(TblPage tblPage) {
        getTblPages().remove(tblPage);
        tblPage.setTblModule(null);

        return tblPage;
    }

}