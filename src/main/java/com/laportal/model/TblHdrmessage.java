package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRMESSAGES database table.
 */
@Entity
@Table(name = "TBL_HDRMESSAGES")
@NamedQuery(name = "TblHdrmessage.findAll", query = "SELECT t FROM TblHdrmessage t")
public class TblHdrmessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRMESSAGES_HDRMESSAGECODE_GENERATOR", sequenceName = "SEQ_TBL_HDRMESSAGES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRMESSAGES_HDRMESSAGECODE_GENERATOR")
    @JsonProperty("code")
    private long hdrmessagecode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Lob
    @Column(name = "MESSAGE")
    private String message;

    private String remarks;

    private String sentto;

    private String usercode;

    //bi-directional many-to-one association to TblHdrclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HDRCLAIMCODE")
    private TblHdrclaim tblHdrclaim;

    @Transient
    private String userName;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "EMAILCODE")
    private TblEmail emailcode;

    public TblHdrmessage() {
    }

    public long getHdrmessagecode() {
        return this.hdrmessagecode;
    }

    public void setHdrmessagecode(long hdrmessagecode) {
        this.hdrmessagecode = hdrmessagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentto() {
        return this.sentto;
    }

    public void setSentto(String sentto) {
        this.sentto = sentto;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblHdrclaim getTblHdrclaim() {
        return tblHdrclaim;
    }

    public void setTblHdrclaim(TblHdrclaim tblHdrclaim) {
        this.tblHdrclaim = tblHdrclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public TblEmail getEmailcode() {
        return emailcode;
    }

    public void setEmailcode(TblEmail emailcode) {
        this.emailcode = emailcode;
    }
}