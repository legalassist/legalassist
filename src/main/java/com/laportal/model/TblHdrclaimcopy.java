package com.laportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRCLAIMCOPY database table.
 * 
 */
@Entity
@Table(name="TBL_HDRCLAIMCOPY")
@NamedQuery(name="TblHdrclaimcopy.findAll", query="SELECT t FROM TblHdrclaimcopy t")
public class TblHdrclaimcopy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_HDRCLAIMCOPY_HDRCLAIMCOPYID_GENERATOR", sequenceName="SEQ_TBL_HDRCLAIMCOPY",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_HDRCLAIMCOPY_HDRCLAIMCOPYID_GENERATOR")
	@Column(name="HDRCLAIMCOPY_ID")
	private long hdrclaimcopyId;

	@Temporal(TemporalType.DATE)
	private Date createdate;

	private BigDecimal createuser;

	//bi-directional many-to-one association to TblHdrclaim
	@ManyToOne
	@JoinColumn(name="CHILD_HDRCLAIMCODE")
	private TblHdrclaim tblHdrclaim1;

	//bi-directional many-to-one association to TblHdrclaim
	@ManyToOne
	@JoinColumn(name="PARENT_HDRCLAIMCODE")
	private TblHdrclaim tblHdrclaim2;

	public TblHdrclaimcopy() {
	}

	public long getHdrclaimcopyId() {
		return this.hdrclaimcopyId;
	}

	public void setHdrclaimcopyId(long hdrclaimcopyId) {
		this.hdrclaimcopyId = hdrclaimcopyId;
	}

	public Date getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public BigDecimal getCreateuser() {
		return this.createuser;
	}

	public void setCreateuser(BigDecimal createuser) {
		this.createuser = createuser;
	}

	public TblHdrclaim getTblHdrclaim1() {
		return this.tblHdrclaim1;
	}

	public void setTblHdrclaim1(TblHdrclaim tblHdrclaim1) {
		this.tblHdrclaim1 = tblHdrclaim1;
	}

	public TblHdrclaim getTblHdrclaim2() {
		return this.tblHdrclaim2;
	}

	public void setTblHdrclaim2(TblHdrclaim tblHdrclaim2) {
		this.tblHdrclaim2 = tblHdrclaim2;
	}

}