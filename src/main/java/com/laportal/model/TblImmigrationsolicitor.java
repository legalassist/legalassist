package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_IMMIGRATIONSOLICITOR database table.
 */
@Entity
@Table(name = "TBL_IMMIGRATIONSOLICITOR")
@NamedQuery(name = "TblImmigrationsolicitor.findAll", query = "SELECT t FROM TblImmigrationsolicitor t")
public class TblImmigrationsolicitor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_IMMIGRATIONSOLICITOR_IMMIGRATIONSOLCODE_GENERATOR", sequenceName = "SEQ_TBL_IMMIGRATIONSOLICITOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_IMMIGRATIONSOLICITOR_IMMIGRATIONSOLCODE_GENERATOR")
    private long immigrationsolcode;

    private String companycode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String reference1;

    private String reference2;

    private String remarks;

    private String status;

    private String usercode;

    //bi-directional many-to-one association to TblImmigrationclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "IMMIGRATIONCLAIMCODE")
    private TblImmigrationclaim tblImmigrationclaim;

    @Transient
    private TblCompanyprofile tblCompanyprofile;

    public TblImmigrationsolicitor() {
    }

    public long getImmigrationsolcode() {
        return this.immigrationsolcode;
    }

    public void setImmigrationsolcode(long immigrationsolcode) {
        this.immigrationsolcode = immigrationsolcode;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getReference1() {
        return this.reference1;
    }

    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getReference2() {
        return this.reference2;
    }

    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblImmigrationclaim getTblImmigrationclaim() {
        return this.tblImmigrationclaim;
    }

    public void setTblImmigrationclaim(TblImmigrationclaim tblImmigrationclaim) {
        this.tblImmigrationclaim = tblImmigrationclaim;
    }

    public TblCompanyprofile getTblCompanyprofile() {
        return tblCompanyprofile;
    }

    public void setTblCompanyprofile(TblCompanyprofile tblCompanyprofile) {
        this.tblCompanyprofile = tblCompanyprofile;
    }
}