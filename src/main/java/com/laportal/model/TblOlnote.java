package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_OLNOTES database table.
 */
@Entity
@Table(name = "TBL_OLNOTES")
@NamedQuery(name = "TblOlnote.findAll", query = "SELECT t FROM TblOlnote t")
public class TblOlnote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_OLNOTES_OLNOTECODE_GENERATOR", sequenceName = "SEQ_TBL_OLNOTES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_OLNOTES_OLNOTECODE_GENERATOR")
    private long olnotecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String note;

    private String remarks;

    private String usercategorycode;

    private String usercode;

    //bi-directional many-to-one association to TblOlclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "OLCLAIMCODE")
    private TblOlclaim tblOlclaim;

    @Transient
    private String userName;

    @Transient
    private boolean self;

    public TblOlnote() {
    }

    public long getOlnotecode() {
        return this.olnotecode;
    }

    public void setOlnotecode(long olnotecode) {
        this.olnotecode = olnotecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercategorycode() {
        return this.usercategorycode;
    }

    public void setUsercategorycode(String usercategorycode) {
        this.usercategorycode = usercategorycode;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblOlclaim getTblOlclaim() {
        return this.tblOlclaim;
    }

    public void setTblOlclaim(TblOlclaim tblOlclaim) {
        this.tblOlclaim = tblOlclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }
}