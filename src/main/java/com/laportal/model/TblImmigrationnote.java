package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_IMMIGRATIONNOTES database table.
 */
@Entity
@Table(name = "TBL_IMMIGRATIONNOTES")
@NamedQuery(name = "TblImmigrationnote.findAll", query = "SELECT t FROM TblImmigrationnote t")
public class TblImmigrationnote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_IMMIGRATIONNOTES_IMMIGRATIONNOTECODE_GENERATOR", sequenceName = "SEQ_TBL_IMMIGRATIONNOTES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_IMMIGRATIONNOTES_IMMIGRATIONNOTECODE_GENERATOR")
    private long immigrationnotecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String note;

    private String remarks;

    private String usercategorycode;

    private String usercode;

    //bi-directional many-to-one association to TblImmigrationclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "IMMIGRATIONCLAIMCODE")
    private TblImmigrationclaim tblImmigrationclaim;

    @Transient
    private String userName;

    @Transient
    private boolean self;

    public TblImmigrationnote() {
    }

    public long getImmigrationnotecode() {
        return this.immigrationnotecode;
    }

    public void setImmigrationnotecode(long immigrationnotecode) {
        this.immigrationnotecode = immigrationnotecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercategorycode() {
        return this.usercategorycode;
    }

    public void setUsercategorycode(String usercategorycode) {
        this.usercategorycode = usercategorycode;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblImmigrationclaim getTblImmigrationclaim() {
        return this.tblImmigrationclaim;
    }

    public void setTblImmigrationclaim(TblImmigrationclaim tblImmigrationclaim) {
        this.tblImmigrationclaim = tblImmigrationclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }

}