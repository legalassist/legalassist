package com.laportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the VIEW_HDRCASEREPORT database table.
 * 
 */
@Entity
@Table(name="VIEW_HDRCASEREPORT")
@NamedQuery(name="ViewHdrcasereport.findAll", query="SELECT v FROM ViewHdrcasereport v")
public class ViewHdrcasereport implements Serializable {
	private static final long serialVersionUID = 1L;

	private String aml;

	@Column(name="C_FNAME")
	private String cFname;

	@Column(name="C_MNAME")
	private String cMname;

	@Column(name="C_POSTCODE")
	private String cPostcode;

	@Column(name="C_SNAME")
	private String cSname;

	@Column(name="C_TITLE")
	private String cTitle;

	@Column(name="CASE_STATUS")
	private String caseStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="CASE_STATUS_ON")
	private Date caseStatusOn;

	private String cfa;

	@Id
	private String claimcode;

	@Temporal(TemporalType.DATE)
	private Date createdate;

	@Column(name="INTRODUCER_COMPANY")
	private String introducerCompany;

	@Column(name="INTRODUCER_USER")
	private String introducerUser;

	@Temporal(TemporalType.DATE)
	@Column(name="INVOICE_DATE")
	private Date invoiceDate;

	private String landlord;

	@Column(name="PROOF_OF_ADDRESS")
	private String proofOfAddress;

	@Column(name="PROOF_OF_ID")
	private String proofOfId;

	@Column(name="SOLICITOR_COMPANY")
	private String solicitorCompany;

	@Column(name="SOLICITOR_USER")
	private String solicitorUser;

	@Column(name="TENANCY_AGREEMENT")
	private String tenancyAgreement;

	@Temporal(TemporalType.DATE)
	private Date updatedate;

	public ViewHdrcasereport() {
	}

	public String getAml() {
		return this.aml;
	}

	public void setAml(String aml) {
		this.aml = aml;
	}

	public String getCFname() {
		return this.cFname;
	}

	public void setCFname(String cFname) {
		this.cFname = cFname;
	}

	public String getCMname() {
		return this.cMname;
	}

	public void setCMname(String cMname) {
		this.cMname = cMname;
	}

	public String getCPostcode() {
		return this.cPostcode;
	}

	public void setCPostcode(String cPostcode) {
		this.cPostcode = cPostcode;
	}

	public String getCSname() {
		return this.cSname;
	}

	public void setCSname(String cSname) {
		this.cSname = cSname;
	}

	public String getCTitle() {
		return this.cTitle;
	}

	public void setCTitle(String cTitle) {
		this.cTitle = cTitle;
	}

	public String getCaseStatus() {
		return this.caseStatus;
	}

	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}

	public Date getCaseStatusOn() {
		return this.caseStatusOn;
	}

	public void setCaseStatusOn(Date caseStatusOn) {
		this.caseStatusOn = caseStatusOn;
	}

	public String getCfa() {
		return this.cfa;
	}

	public void setCfa(String cfa) {
		this.cfa = cfa;
	}

	public String getClaimcode() {
		return this.claimcode;
	}

	public void setClaimcode(String claimcode) {
		this.claimcode = claimcode;
	}

	public Date getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public String getIntroducerCompany() {
		return this.introducerCompany;
	}

	public void setIntroducerCompany(String introducerCompany) {
		this.introducerCompany = introducerCompany;
	}

	public String getIntroducerUser() {
		return this.introducerUser;
	}

	public void setIntroducerUser(String introducerUser) {
		this.introducerUser = introducerUser;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getLandlord() {
		return this.landlord;
	}

	public void setLandlord(String landlord) {
		this.landlord = landlord;
	}

	public String getProofOfAddress() {
		return this.proofOfAddress;
	}

	public void setProofOfAddress(String proofOfAddress) {
		this.proofOfAddress = proofOfAddress;
	}

	public String getProofOfId() {
		return this.proofOfId;
	}

	public void setProofOfId(String proofOfId) {
		this.proofOfId = proofOfId;
	}

	public String getSolicitorCompany() {
		return this.solicitorCompany;
	}

	public void setSolicitorCompany(String solicitorCompany) {
		this.solicitorCompany = solicitorCompany;
	}

	public String getSolicitorUser() {
		return this.solicitorUser;
	}

	public void setSolicitorUser(String solicitorUser) {
		this.solicitorUser = solicitorUser;
	}

	public String getTenancyAgreement() {
		return this.tenancyAgreement;
	}

	public void setTenancyAgreement(String tenancyAgreement) {
		this.tenancyAgreement = tenancyAgreement;
	}

	public Date getUpdatedate() {
		return this.updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

}