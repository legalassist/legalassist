package com.laportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the VIEW_HIRECASEREPORT database table.
 * 
 */
@Entity
@Table(name="VIEW_HIRECASEREPORT")
@NamedQuery(name="ViewHirecasereport.findAll", query="SELECT v FROM ViewHirecasereport v")
public class ViewHirecasereport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ACCIDENT_DATE")
	private String accidentDate;

	@Temporal(TemporalType.DATE)
	private Date bookingdate;

	@Column(name="CASE_STATUS")
	private String caseStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="CASE_STATUS_ON")
	private Date caseStatusOn;

	@Column(name="CLIENT_MAKEMODEL")
	private String clientMakemodel;

	@Column(name="CLIENT_VEHICLE_REGNO")
	private String clientVehicleRegno;

	@Id
	private String code;

	@Temporal(TemporalType.DATE)
	private Date createdon;

	private String firstname;

	@Column(name="HIRE_FIRM")
	private String hireFirm;

	@Temporal(TemporalType.DATE)
	private Date hireenddate;

	@Temporal(TemporalType.DATE)
	private Date hirestartdate;

	@Column(name="INTRODUCER_USER")
	private String introducerUser;

	private String lastname;

	@Temporal(TemporalType.DATE)
	private Date lastupdated;

	private String middlename;

	@Column(name="PARTY_VEHICLE_REGNO")
	private String partyVehicleRegno;

	private String partycontactno;

	private String partymakemodel;

	private String partyname;

	private String postalcode;

	@Column(name="\"SERVICE\"")
	private String service;

	private String title;

	private String vehiclecondition;

	public ViewHirecasereport() {
	}

	public String getAccidentDate() {
		return this.accidentDate;
	}

	public void setAccidentDate(String accidentDate) {
		this.accidentDate = accidentDate;
	}

	public Date getBookingdate() {
		return this.bookingdate;
	}

	public void setBookingdate(Date bookingdate) {
		this.bookingdate = bookingdate;
	}

	public String getCaseStatus() {
		return this.caseStatus;
	}

	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}

	public Date getCaseStatusOn() {
		return this.caseStatusOn;
	}

	public void setCaseStatusOn(Date caseStatusOn) {
		this.caseStatusOn = caseStatusOn;
	}

	public String getClientMakemodel() {
		return this.clientMakemodel;
	}

	public void setClientMakemodel(String clientMakemodel) {
		this.clientMakemodel = clientMakemodel;
	}

	public String getClientVehicleRegno() {
		return this.clientVehicleRegno;
	}

	public void setClientVehicleRegno(String clientVehicleRegno) {
		this.clientVehicleRegno = clientVehicleRegno;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getHireFirm() {
		return this.hireFirm;
	}

	public void setHireFirm(String hireFirm) {
		this.hireFirm = hireFirm;
	}

	public Date getHireenddate() {
		return this.hireenddate;
	}

	public void setHireenddate(Date hireenddate) {
		this.hireenddate = hireenddate;
	}

	public Date getHirestartdate() {
		return this.hirestartdate;
	}

	public void setHirestartdate(Date hirestartdate) {
		this.hirestartdate = hirestartdate;
	}

	public String getIntroducerUser() {
		return this.introducerUser;
	}

	public void setIntroducerUser(String introducerUser) {
		this.introducerUser = introducerUser;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getLastupdated() {
		return this.lastupdated;
	}

	public void setLastupdated(Date lastupdated) {
		this.lastupdated = lastupdated;
	}

	public String getMiddlename() {
		return this.middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getPartyVehicleRegno() {
		return this.partyVehicleRegno;
	}

	public void setPartyVehicleRegno(String partyVehicleRegno) {
		this.partyVehicleRegno = partyVehicleRegno;
	}

	public String getPartycontactno() {
		return this.partycontactno;
	}

	public void setPartycontactno(String partycontactno) {
		this.partycontactno = partycontactno;
	}

	public String getPartymakemodel() {
		return this.partymakemodel;
	}

	public void setPartymakemodel(String partymakemodel) {
		this.partymakemodel = partymakemodel;
	}

	public String getPartyname() {
		return this.partyname;
	}

	public void setPartyname(String partyname) {
		this.partyname = partyname;
	}

	public String getPostalcode() {
		return this.postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getService() {
		return this.service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVehiclecondition() {
		return this.vehiclecondition;
	}

	public void setVehiclecondition(String vehiclecondition) {
		this.vehiclecondition = vehiclecondition;
	}

}