package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_IMMIGRATIONLOGS database table.
 */
@Entity
@Table(name = "TBL_IMMIGRATIONLOGS")
@NamedQuery(name = "TblImmigrationlog.findAll", query = "SELECT t FROM TblImmigrationlog t")
public class TblImmigrationlog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_IMMIGRATIONLOGS_IMMIGRATIONLOGCODE_GENERATOR", sequenceName = "SEQ_TBL_IMMIGRATIONLOGS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_IMMIGRATIONLOGS_IMMIGRATIONLOGCODE_GENERATOR")
    private long immigrationlogcode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblImmigrationclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "IMMIGRATIONCLAIMCODE")
    private TblImmigrationclaim tblImmigrationclaim;

    @Transient
    private String userName;


    public TblImmigrationlog() {
    }

    public long getImmigrationlogcode() {
        return this.immigrationlogcode;
    }

    public void setImmigrationlogcode(long immigrationlogcode) {
        this.immigrationlogcode = immigrationlogcode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblImmigrationclaim getTblImmigrationclaim() {
        return this.tblImmigrationclaim;
    }

    public void setTblImmigrationclaim(TblImmigrationclaim tblImmigrationclaim) {
        this.tblImmigrationclaim = tblImmigrationclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}