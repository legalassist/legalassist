package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_PLNOTES database table.
 */
@Entity
@Table(name = "TBL_PLNOTES")
@NamedQuery(name = "TblPlnote.findAll", query = "SELECT t FROM TblPlnote t")
public class TblPlnote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PLNOTES_PLNOTECODE_GENERATOR", sequenceName = "SEQ_TBL_PLNOTES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PLNOTES_PLNOTECODE_GENERATOR")
    private long plnotecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String note;

    private String remarks;

    private String usercategorycode;

    private String usercode;

    //bi-directional many-to-one association to TblPlclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PLCLAIMCODE")
    private TblPlclaim tblPlclaim;

    @Transient
    private String userName;

    @Transient
    private boolean self;

    public TblPlnote() {
    }

    public long getPlnotecode() {
        return this.plnotecode;
    }

    public void setPlnotecode(long plnotecode) {
        this.plnotecode = plnotecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercategorycode() {
        return this.usercategorycode;
    }

    public void setUsercategorycode(String usercategorycode) {
        this.usercategorycode = usercategorycode;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblPlclaim getTblPlclaim() {
        return this.tblPlclaim;
    }

    public void setTblPlclaim(TblPlclaim tblPlclaim) {
        this.tblPlclaim = tblPlclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }
}