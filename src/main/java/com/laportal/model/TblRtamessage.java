package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_RTAMESSAGES database table.
 */
@Entity
@Table(name = "TBL_RTAMESSAGES")
@NamedQuery(name = "TblRtamessage.findAll", query = "SELECT t FROM TblRtamessage t")
public class TblRtamessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTAMESSAGES_RTAMESSAGECODE_GENERATOR", sequenceName = "SEQ_RTAMESSAGES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTAMESSAGES_RTAMESSAGECODE_GENERATOR")
    @JsonProperty("code")
    private long rtamessagecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Lob
    @Column(name = "MESSAGE")
    private String message;

    private String remarks;

    private String sentto;

    private String usercode;

    //bi-directional many-to-one association to TblRtaclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "RTACODE")
    private TblRtaclaim tblRtaclaim;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "EMAILCODE")
    private TblEmail emailcode;

    @Transient
    private String userName;

    public TblRtamessage() {
    }

    public long getRtamessagecode() {
        return this.rtamessagecode;
    }

    public void setRtamessagecode(long rtamessagecode) {
        this.rtamessagecode = rtamessagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentto() {
        return this.sentto;
    }

    public void setSentto(String sentto) {
        this.sentto = sentto;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblRtaclaim getTblRtaclaim() {
        return this.tblRtaclaim;
    }

    public void setTblRtaclaim(TblRtaclaim tblRtaclaim) {
        this.tblRtaclaim = tblRtaclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public TblEmail getEmailcode() {
        return emailcode;
    }

    public void setEmailcode(TblEmail emailcode) {
        this.emailcode = emailcode;
    }
}