package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_MNTASKS database table.
 */
@Entity
@Table(name = "TBL_MNTASKS")
@NamedQuery(name = "TblMntask.findAll", query = "SELECT t FROM TblMntask t")
public class TblMntask implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_MNTASKS_MNTASKCODE_GENERATOR", sequenceName = "SEQ_TBL_MNTASKS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_MNTASKS_MNTASKCODE_GENERATOR")
    private long mntaskcode;

    private String completedby;

    @Temporal(TemporalType.DATE)
    private Date completedon;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String currenttask;

    private String remarks;

    private String status;

    private BigDecimal taskcode;

    //bi-directional many-to-one association to TblMnclaim
    @ManyToOne
    @JoinColumn(name = "MNCLAIMCODE")
    private TblMnclaim tblMnclaim;

    public TblMntask() {
    }

    public long getMntaskcode() {
        return this.mntaskcode;
    }

    public void setMntaskcode(long mntaskcode) {
        this.mntaskcode = mntaskcode;
    }

    public String getCompletedby() {
        return this.completedby;
    }

    public void setCompletedby(String completedby) {
        this.completedby = completedby;
    }

    public Date getCompletedon() {
        return this.completedon;
    }

    public void setCompletedon(Date completedon) {
        this.completedon = completedon;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getCurrenttask() {
        return this.currenttask;
    }

    public void setCurrenttask(String currenttask) {
        this.currenttask = currenttask;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getTaskcode() {
        return this.taskcode;
    }

    public void setTaskcode(BigDecimal taskcode) {
        this.taskcode = taskcode;
    }

    public TblMnclaim getTblMnclaim() {
        return this.tblMnclaim;
    }

    public void setTblMnclaim(TblMnclaim tblMnclaim) {
        this.tblMnclaim = tblMnclaim;
    }

}