package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_MANUALINVOICINGDETAIL database table.
 */
@Entity
@Table(name = "TBL_MANUALINVOICINGDETAIL")
@NamedQuery(name = "TblManualinvoicingdetail.findAll", query = "SELECT t FROM TblManualinvoicingdetail t")
public class TblManualinvoicingdetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_MANUALINVOICINGDETAIL_MANUALINVOICINGDETAILID_GENERATOR", sequenceName = "SEQ_TBL_MANUALINVOICINGDETAIL", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_MANUALINVOICINGDETAIL_MANUALINVOICINGDETAILID_GENERATOR")
    private long manualinvoicingdetailid;

    private BigDecimal amount;

    @Temporal(TemporalType.DATE)
    private Date createdate;

    private String createuser;

    private String description;

    @Temporal(TemporalType.DATE)
    private Date lastupdatedate;

    private String lastupdateuser;

    //bi-directional many-to-one association to TblManualinvoicing
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "MANUALINVOICINGID")
    private TblManualinvoicing tblManualinvoicing;

    public TblManualinvoicingdetail() {
    }

    public long getManualinvoicingdetailid() {
        return this.manualinvoicingdetailid;
    }

    public void setManualinvoicingdetailid(long manualinvoicingdetailid) {
        this.manualinvoicingdetailid = manualinvoicingdetailid;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLastupdatedate() {
        return this.lastupdatedate;
    }

    public void setLastupdatedate(Date lastupdatedate) {
        this.lastupdatedate = lastupdatedate;
    }

    public String getLastupdateuser() {
        return this.lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }

    public TblManualinvoicing getTblManualinvoicing() {
        return this.tblManualinvoicing;
    }

    public void setTblManualinvoicing(TblManualinvoicing tblManualinvoicing) {
        this.tblManualinvoicing = tblManualinvoicing;
    }

}