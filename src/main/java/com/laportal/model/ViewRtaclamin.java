package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.laportal.dto.LovResponse;
import com.laportal.dto.PassengerRtaClaims;
import com.laportal.dto.RtaActionButton;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the VIEW_RTACLAMIN database table.
 */
@Entity
@Table(name = "VIEW_RTACLAMIN")
@NamedQuery(name = "ViewRtaclamin.findAll", query = "SELECT v FROM ViewRtaclamin v")
public class ViewRtaclamin implements Serializable {
    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.DATE)
    private Date accdate;

    private String acctime;

    private String address1;

    private String address2;

    private String address3;

    @Temporal(TemporalType.DATE)
    private Date amldate;

    private String amlmessage;

    private String amlstatus;

    private BigDecimal circumcode;

    private String circumdescr;

    private String circumname;

    private String city;

    @Temporal(TemporalType.DATE)
    private Date contactdue;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String description;


    @Temporal(TemporalType.DATE)
    private Date dob;

    private String driverpassenger;

    private String email;

    private String englishlevel;

    private String esig;


    @Temporal(TemporalType.DATE)
    private Date esigdate;

    private String esignstatus;

    private String firstname;

    private String gaddress1;

    private String gaddress2;

    private String gaddress3;

    private String gcity;

    private String gemail;

    private String gfirstname;

    private String glandline;

    private String glastname;

    private String gmiddlename;

    private String gmobile;

    private String gpostalcode;

    private String greencardno;

    private String gregion;

    private String gtitle;

    private BigDecimal injclasscode;

    private String injdescription;
    private String injlength;

    private String insurer;

    private String landline;

    private String lastname;

    @Temporal(TemporalType.DATE)
    private Date lastupdated;

    private String location;

    private String makemodel;

    private String medicalinfo;

    private String middlename;

    private String mobile;

    private String ninumber;

    private String ongoing;

    private String partyaddress;

    private String partycontactno;

    private String partyinsurer;

    private String partymakemodel;

    private String partyname;

    private String partypolicyno;

    private String partyrefno;

    private String partyregno;

    private String passengerinfo;

    private BigDecimal passengers;

    private String policyno;

    private String postalcode;

    private String rdweathercond;

    private String refno;

    private String region;

    private String registerationno;

    private String reportedtopolice;

    @Id
    private BigDecimal rtacode;

    private String rtalinkcode;

    private String rtanumber;

    private String scotland;

    private String status;

    private String statuscode;

    private String title;

    private String usercode;

    private String vehiclecondition;


    private String password;
    private String translatordetails;
    private String alternativenumber;
    private String medicalevidence;
    private String airbagopened;


    @Temporal(TemporalType.DATE)
    private Date reportedon;
    @Temporal(TemporalType.DATE)
    private Date gdob;

    private Long rtataskcode;
    private String rtataskname;

    @Column(name = "VEHICLE_TYPE")
    private String vehicleType;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @Column(name = "CLAWBACK_DATE")
    private Date clawbackDate;

    @Column(name = "VD_IMAGES")
    private String vdImages;
    @Column(name = "INJURY_SUSTAINED")
    private String injurySustained;

    @Transient
    private List<RtaActionButton> rtaActionButtons = new ArrayList<>();

    private String taskflag;

    @Transient
    private String statusDescr;

    @Transient
    private List<TblRtadocument> tblRtadocuments = new ArrayList<TblRtadocument>();

    private Long advisor;
    private Long introducer;

    @Transient
    private String advisorname;
    @Transient
    private String introducername;


    @Transient
    private String solicitorusername;
    @Transient
    private String solicitorcompany;

    @Transient
    List<TblRtatask> rtaCaseTasks = new ArrayList<>();

    @Transient
    List<TblRtapassenger> rtapassengers = new ArrayList<>();

    @Transient
    List<PassengerRtaClaims> passengerRtaClaims = new ArrayList<>();

    private String referencenumber;

    @Transient
    private List<TblRtalog> tblRtalogs = new ArrayList<>();
    @Transient
    private List<RtaActionButton> rtaDropdownBtns = new ArrayList<>();

    @Transient
    private String rtaLinkCaseCode;
    @Transient
    private String rtaLinkCaseStatus;
    @Transient
    private long rtaLinkCaseid;
    @Transient
    private List<LovResponse> injclasscodes = new ArrayList<>();

    @Transient
    private BigDecimal introducerInvoiceHeadId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @Transient
    private Date introducerInvoiceDate;


    @Transient
    private BigDecimal solicitorInvoiceHeadId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @Transient
    private Date solicitorInvoiceDate;

    @Transient
    private TblEsignStatus tblEsignStatus;

    @Transient
    private String editFlag;

    public ViewRtaclamin() {
    }

    public Date getAccdate() {
        return this.accdate;
    }

    public void setAccdate(Date accdate) {
        this.accdate = accdate;
    }

    public String getAcctime() {
        return this.acctime;
    }

    public void setAcctime(String acctime) {
        this.acctime = acctime;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return this.address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public Date getAmldate() {
        return this.amldate;
    }

    public void setAmldate(Date amldate) {
        this.amldate = amldate;
    }

    public String getAmlmessage() {
        return this.amlmessage;
    }

    public void setAmlmessage(String amlmessage) {
        this.amlmessage = amlmessage;
    }

    public String getAmlstatus() {
        return this.amlstatus;
    }

    public void setAmlstatus(String amlstatus) {
        this.amlstatus = amlstatus;
    }

    public BigDecimal getCircumcode() {
        return this.circumcode;
    }

    public void setCircumcode(BigDecimal circumcode) {
        this.circumcode = circumcode;
    }

    public String getCircumdescr() {
        return this.circumdescr;
    }

    public void setCircumdescr(String circumdescr) {
        this.circumdescr = circumdescr;
    }

    public String getCircumname() {
        return this.circumname;
    }

    public void setCircumname(String circumname) {
        this.circumname = circumname;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getContactdue() {
        return this.contactdue;
    }

    public void setContactdue(Date contactdue) {
        this.contactdue = contactdue;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getDriverpassenger() {
        return this.driverpassenger;
    }

    public void setDriverpassenger(String driverpassenger) {
        this.driverpassenger = driverpassenger;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnglishlevel() {
        return this.englishlevel;
    }

    public void setEnglishlevel(String englishlevel) {
        this.englishlevel = englishlevel;
    }

    public String getEsig() {
        return this.esig;
    }

    public void setEsig(String esig) {
        this.esig = esig;
    }

    public Date getEsigdate() {
        return this.esigdate;
    }

    public void setEsigdate(Date esigdate) {
        this.esigdate = esigdate;
    }

    public String getEsignstatus() {
        return this.esignstatus;
    }

    public void setEsignstatus(String esignstatus) {
        this.esignstatus = esignstatus;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGaddress1() {
        return this.gaddress1;
    }

    public void setGaddress1(String gaddress1) {
        this.gaddress1 = gaddress1;
    }

    public String getGaddress2() {
        return this.gaddress2;
    }

    public void setGaddress2(String gaddress2) {
        this.gaddress2 = gaddress2;
    }

    public String getGaddress3() {
        return this.gaddress3;
    }

    public void setGaddress3(String gaddress3) {
        this.gaddress3 = gaddress3;
    }

    public String getGcity() {
        return this.gcity;
    }

    public void setGcity(String gcity) {
        this.gcity = gcity;
    }

    public String getGemail() {
        return this.gemail;
    }

    public void setGemail(String gemail) {
        this.gemail = gemail;
    }

    public String getGfirstname() {
        return this.gfirstname;
    }

    public void setGfirstname(String gfirstname) {
        this.gfirstname = gfirstname;
    }

    public String getGlandline() {
        return this.glandline;
    }

    public void setGlandline(String glandline) {
        this.glandline = glandline;
    }

    public String getGlastname() {
        return this.glastname;
    }

    public void setGlastname(String glastname) {
        this.glastname = glastname;
    }

    public String getGmiddlename() {
        return this.gmiddlename;
    }

    public void setGmiddlename(String gmiddlename) {
        this.gmiddlename = gmiddlename;
    }

    public String getGmobile() {
        return this.gmobile;
    }

    public void setGmobile(String gmobile) {
        this.gmobile = gmobile;
    }

    public String getGpostalcode() {
        return this.gpostalcode;
    }

    public void setGpostalcode(String gpostalcode) {
        this.gpostalcode = gpostalcode;
    }

    public String getGreencardno() {
        return this.greencardno;
    }

    public void setGreencardno(String greencardno) {
        this.greencardno = greencardno;
    }

    public String getGregion() {
        return this.gregion;
    }

    public void setGregion(String gregion) {
        this.gregion = gregion;
    }

    public String getGtitle() {
        return this.gtitle;
    }

    public void setGtitle(String gtitle) {
        this.gtitle = gtitle;
    }

    public BigDecimal getInjclasscode() {
        return this.injclasscode;
    }

    public void setInjclasscode(BigDecimal injclasscode) {
        this.injclasscode = injclasscode;
    }


    public String getInsurer() {
        return this.insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public String getLandline() {
        return this.landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getLastupdated() {
        return this.lastupdated;
    }

    public void setLastupdated(Date lastupdated) {
        this.lastupdated = lastupdated;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMakemodel() {
        return this.makemodel;
    }

    public void setMakemodel(String makemodel) {
        this.makemodel = makemodel;
    }

    public String getMedicalinfo() {
        return this.medicalinfo;
    }

    public void setMedicalinfo(String medicalinfo) {
        this.medicalinfo = medicalinfo;
    }

    public String getMiddlename() {
        return this.middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNinumber() {
        return this.ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getOngoing() {
        return this.ongoing;
    }

    public void setOngoing(String ongoing) {
        this.ongoing = ongoing;
    }

    public String getPartyaddress() {
        return this.partyaddress;
    }

    public void setPartyaddress(String partyaddress) {
        this.partyaddress = partyaddress;
    }

    public String getPartycontactno() {
        return this.partycontactno;
    }

    public void setPartycontactno(String partycontactno) {
        this.partycontactno = partycontactno;
    }

    public String getPartyinsurer() {
        return this.partyinsurer;
    }

    public void setPartyinsurer(String partyinsurer) {
        this.partyinsurer = partyinsurer;
    }

    public String getPartymakemodel() {
        return this.partymakemodel;
    }

    public void setPartymakemodel(String partymakemodel) {
        this.partymakemodel = partymakemodel;
    }

    public String getPartyname() {
        return this.partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getPartypolicyno() {
        return this.partypolicyno;
    }

    public void setPartypolicyno(String partypolicyno) {
        this.partypolicyno = partypolicyno;
    }

    public String getPartyrefno() {
        return this.partyrefno;
    }

    public void setPartyrefno(String partyrefno) {
        this.partyrefno = partyrefno;
    }

    public String getPartyregno() {
        return this.partyregno;
    }

    public void setPartyregno(String partyregno) {
        this.partyregno = partyregno;
    }

    public String getPassengerinfo() {
        return this.passengerinfo;
    }

    public void setPassengerinfo(String passengerinfo) {
        this.passengerinfo = passengerinfo;
    }

    public BigDecimal getPassengers() {
        return this.passengers;
    }

    public void setPassengers(BigDecimal passengers) {
        this.passengers = passengers;
    }

    public String getPolicyno() {
        return this.policyno;
    }

    public void setPolicyno(String policyno) {
        this.policyno = policyno;
    }

    public String getPostalcode() {
        return this.postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getRdweathercond() {
        return this.rdweathercond;
    }

    public void setRdweathercond(String rdweathercond) {
        this.rdweathercond = rdweathercond;
    }

    public String getRefno() {
        return this.refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getRegion() {
        return this.region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegisterationno() {
        return this.registerationno;
    }

    public void setRegisterationno(String registerationno) {
        this.registerationno = registerationno;
    }

    public String getReportedtopolice() {
        return this.reportedtopolice;
    }

    public void setReportedtopolice(String reportedtopolice) {
        this.reportedtopolice = reportedtopolice;
    }

    public BigDecimal getRtacode() {
        return this.rtacode;
    }

    public void setRtacode(BigDecimal rtacode) {
        this.rtacode = rtacode;
    }

    public String getRtalinkcode() {
        return this.rtalinkcode;
    }

    public void setRtalinkcode(String rtalinkcode) {
        this.rtalinkcode = rtalinkcode;
    }

    public String getRtanumber() {
        return this.rtanumber;
    }

    public void setRtanumber(String rtanumber) {
        this.rtanumber = rtanumber;
    }

    public String getScotland() {
        return this.scotland;
    }

    public void setScotland(String scotland) {
        this.scotland = scotland;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return this.statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getVehiclecondition() {
        return this.vehiclecondition;
    }

    public void setVehiclecondition(String vehiclecondition) {
        this.vehiclecondition = vehiclecondition;
    }

    public List<RtaActionButton> getRtaActionButtons() {
        return rtaActionButtons;
    }

    public void setRtaActionButtons(List<RtaActionButton> rtaActionButtons) {
        this.rtaActionButtons = rtaActionButtons;
    }


    public String getTaskflag() {
        return taskflag;
    }

    public void setTaskflag(String taskflag) {
        this.taskflag = taskflag;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTranslatordetails() {
        return translatordetails;
    }

    public void setTranslatordetails(String translatordetail) {
        this.translatordetails = translatordetail;
    }

    public String getAlternativenumber() {
        return alternativenumber;
    }

    public void setAlternativenumber(String alternativenumber) {
        this.alternativenumber = alternativenumber;
    }

    public String getMedicalevidence() {
        return medicalevidence;
    }

    public void setMedicalevidence(String medicalevidence) {
        this.medicalevidence = medicalevidence;
    }

    public Date getReportedon() {
        return reportedon;
    }

    public void setReportedon(Date reportedon) {
        this.reportedon = reportedon;
    }

    public Date getGdob() {
        return gdob;
    }

    public void setGdob(Date gdob) {
        this.gdob = gdob;
    }

    public Long getRtataskcode() {
        return rtataskcode;
    }

    public void setRtataskcode(Long rtataskcode) {
        this.rtataskcode = rtataskcode;
    }

    public String getRtataskname() {
        return rtataskname;
    }

    public void setRtataskname(String rtataskname) {
        this.rtataskname = rtataskname;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }

    public List<TblRtadocument> getTblRtadocuments() {
        return tblRtadocuments;
    }

    public void setTblRtadocuments(List<TblRtadocument> tblRtadocuments) {
        this.tblRtadocuments = tblRtadocuments;
    }

    public Long getAdvisor() {
        return advisor;
    }

    public void setAdvisor(Long advisor) {
        this.advisor = advisor;
    }

    public Long getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Long introducer) {
        this.introducer = introducer;
    }

    public String getAdvisorname() {
        return advisorname;
    }

    public void setAdvisorname(String advisorname) {
        this.advisorname = advisorname;
    }

    public String getIntroducername() {
        return introducername;
    }

    public void setIntroducername(String introducername) {
        this.introducername = introducername;
    }

    public String getSolicitorusername() {
        return solicitorusername;
    }

    public void setSolicitorusername(String solicitorusername) {
        this.solicitorusername = solicitorusername;
    }

    public String getSolicitorcompany() {
        return solicitorcompany;
    }

    public void setSolicitorcompany(String solicitorcompany) {
        this.solicitorcompany = solicitorcompany;
    }

    public List<TblRtatask> getRtaCaseTasks() {
        return rtaCaseTasks;
    }

    public void setRtaCaseTasks(List<TblRtatask> rtaCaseTasks) {
        this.rtaCaseTasks = rtaCaseTasks;
    }

    public List<TblRtapassenger> getRtapassengers() {
        return rtapassengers;
    }

    public void setRtapassengers(List<TblRtapassenger> rtapassengers) {
        this.rtapassengers = rtapassengers;
    }

    public List<PassengerRtaClaims> getPassengerRtaClaims() {
        return passengerRtaClaims;
    }

    public void setPassengerRtaClaims(List<PassengerRtaClaims> passengerRtaClaims) {
        this.passengerRtaClaims = passengerRtaClaims;
    }

    public String getReferencenumber() {
        return referencenumber;
    }

    public void setReferencenumber(String referencenumber) {
        this.referencenumber = referencenumber;
    }

    public List<TblRtalog> getTblRtalogs() {
        return tblRtalogs;
    }

    public void setTblRtalogs(List<TblRtalog> tblRtalogs) {
        this.tblRtalogs = tblRtalogs;
    }

    public List<RtaActionButton> getRtaDropdownBtns() {
        return rtaDropdownBtns;
    }

    public void setRtaDropdownBtns(List<RtaActionButton> rtaDropdownBtns) {
        this.rtaDropdownBtns = rtaDropdownBtns;
    }

    public String getRtaLinkCaseCode() {
        return rtaLinkCaseCode;
    }

    public void setRtaLinkCaseCode(String rtaLinkCaseCode) {
        this.rtaLinkCaseCode = rtaLinkCaseCode;
    }

    public String getRtaLinkCaseStatus() {
        return rtaLinkCaseStatus;
    }

    public void setRtaLinkCaseStatus(String rtaLinkCaseStatus) {
        this.rtaLinkCaseStatus = rtaLinkCaseStatus;
    }

    public long getRtaLinkCaseid() {
        return rtaLinkCaseid;
    }

    public void setRtaLinkCaseid(long rtaLinkCaseid) {
        this.rtaLinkCaseid = rtaLinkCaseid;
    }

    public List<LovResponse> getInjclasscodes() {
        return injclasscodes;
    }

    public void setInjclasscodes(List<LovResponse> injclasscodes) {
        this.injclasscodes = injclasscodes;
    }

    public String getAirbagopened() {
        return airbagopened;
    }

    public void setAirbagopened(String airbagopened) {
        this.airbagopened = airbagopened;
    }

    public String getInjdescription() {
        return injdescription;
    }

    public void setInjdescription(String injdescription) {
        this.injdescription = injdescription;
    }

    public String getInjlength() {
        return injlength;
    }

    public void setInjlength(String injlength) {
        this.injlength = injlength;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Date getClawbackDate() {
        return clawbackDate;
    }

    public void setClawbackDate(Date clawbackDate) {
        this.clawbackDate = clawbackDate;
    }

    public String getVdImages() {
        return vdImages;
    }

    public void setVdImages(String vdImages) {
        this.vdImages = vdImages;
    }

    public String getInjurySustained() {
        return injurySustained;
    }

    public void setInjurySustained(String injurySustained) {
        this.injurySustained = injurySustained;
    }

    public BigDecimal getIntroducerInvoiceHeadId() {
        return introducerInvoiceHeadId;
    }

    public void setIntroducerInvoiceHeadId(BigDecimal introducerInvoiceHeadId) {
        this.introducerInvoiceHeadId = introducerInvoiceHeadId;
    }

    public Date getIntroducerInvoiceDate() {
        return introducerInvoiceDate;
    }

    public void setIntroducerInvoiceDate(Date introducerInvoiceDate) {
        this.introducerInvoiceDate = introducerInvoiceDate;
    }

    public BigDecimal getSolicitorInvoiceHeadId() {
        return solicitorInvoiceHeadId;
    }

    public void setSolicitorInvoiceHeadId(BigDecimal solicitorInvoiceHeadId) {
        this.solicitorInvoiceHeadId = solicitorInvoiceHeadId;
    }

    public Date getSolicitorInvoiceDate() {
        return solicitorInvoiceDate;
    }

    public void setSolicitorInvoiceDate(Date solicitorInvoiceDate) {
        this.solicitorInvoiceDate = solicitorInvoiceDate;
    }

    public TblEsignStatus getTblEsignStatus() {
        return tblEsignStatus;
    }

    public void setTblEsignStatus(TblEsignStatus tblEsignStatus) {
        this.tblEsignStatus = tblEsignStatus;
    }

    public String getEditFlag() {
        return editFlag;
    }

    public void setEditFlag(String editFlag) {
        this.editFlag = editFlag;
    }
}