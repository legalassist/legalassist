package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_ELNOTES database table.
 */
@Entity
@Table(name = "TBL_ELNOTES")
@NamedQuery(name = "TblElnote.findAll", query = "SELECT t FROM TblElnote t")
public class TblElnote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_ELNOTES_ELNOTECODE_GENERATOR", sequenceName = "SEQ_TBL_ELNOTES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_ELNOTES_ELNOTECODE_GENERATOR")
    private long elnotecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String note;

    private String remarks;

    private String usercategorycode;

    private String usercode;

    //bi-directional many-to-one association to TblElclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ELCLAIMCODE")
    private TblElclaim tblElclaim;

    @Transient
    private String userName;

    @Transient
    private boolean self;

    public TblElnote() {
    }

    public long getElnotecode() {
        return this.elnotecode;
    }

    public void setElnotecode(long elnotecode) {
        this.elnotecode = elnotecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercategorycode() {
        return this.usercategorycode;
    }

    public void setUsercategorycode(String usercategorycode) {
        this.usercategorycode = usercategorycode;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblElclaim getTblElclaim() {
        return this.tblElclaim;
    }

    public void setTblElclaim(TblElclaim tblElclaim) {
        this.tblElclaim = tblElclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }
}