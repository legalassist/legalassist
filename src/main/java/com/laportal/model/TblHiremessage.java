package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HIREMESSAGES database table.
 */
@Entity
@Table(name = "TBL_HIREMESSAGES")
@NamedQuery(name = "TblHiremessage.findAll", query = "SELECT t FROM TblHiremessage t")
public class TblHiremessage implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HIREMESSAGES_HIREMESSAGECODE_GENERATOR", sequenceName = "TBL_HIREMESSAGES_SEQ",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HIREMESSAGES_HIREMESSAGECODE_GENERATOR")
    private long hiremessagecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    @Lob
    private String message;

    private String remarks;

    private String sentto;

    private String usercode;

    //bi-directional many-to-one association to TblHireclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HIRECODE")
    private TblHireclaim tblHireclaim;

    @Transient
    private String userName;

    public TblHiremessage() {
    }

    public long getHiremessagecode() {
        return this.hiremessagecode;
    }

    public void setHiremessagecode(long hiremessagecode) {
        this.hiremessagecode = hiremessagecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSentto() {
        return this.sentto;
    }

    public void setSentto(String sentto) {
        this.sentto = sentto;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblHireclaim getTblHireclaim() {
        return this.tblHireclaim;
    }

    public void setTblHireclaim(TblHireclaim tblHireclaim) {
        this.tblHireclaim = tblHireclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}