package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_MANUALINVOICING database table.
 */
@Entity
@Table(name = "TBL_MANUALINVOICING")
@NamedQuery(name = "TblManualinvoicing.findAll", query = "SELECT t FROM TblManualinvoicing t")
public class TblManualinvoicing implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_MANUALINVOICING_MANUALINVOICINGID_GENERATOR", sequenceName = "SEQ_TBL_MANUALINVOICING", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_MANUALINVOICING_MANUALINVOICINGID_GENERATOR")
    private long manualinvoicingid;

    private Date createdate;

    private String createuser;

    private String customeraddress;

    private String customername;

    @Temporal(TemporalType.DATE)
    private Date duedate;

    private BigDecimal grandtotal;

    private String invoiceno;

    private Date lasteupdatedate;

    private String lastupdateuser;

    private String status;

    private String statusdescr;

    private BigDecimal tax;

    //bi-directional many-to-one association to TblManualinvoicingdetail
    @OneToMany(mappedBy = "tblManualinvoicing")
    private List<TblManualinvoicingdetail> tblManualinvoicingdetails;

    public TblManualinvoicing() {
    }

    public long getManualinvoicingid() {
        return this.manualinvoicingid;
    }

    public void setManualinvoicingid(long manualinvoicingid) {
        this.manualinvoicingid = manualinvoicingid;
    }


    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    public String getCustomeraddress() {
        return this.customeraddress;
    }

    public void setCustomeraddress(String customeraddress) {
        this.customeraddress = customeraddress;
    }

    public String getCustomername() {
        return this.customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public Date getDuedate() {
        return this.duedate;
    }

    public void setDuedate(Date duedate) {
        this.duedate = duedate;
    }

    public BigDecimal getGrandtotal() {
        return this.grandtotal;
    }

    public void setGrandtotal(BigDecimal grandtotal) {
        this.grandtotal = grandtotal;
    }

    public String getInvoiceno() {
        return this.invoiceno;
    }

    public void setInvoiceno(String invoiceno) {
        this.invoiceno = invoiceno;
    }

    public Date getLasteupdatedate() {
        return this.lasteupdatedate;
    }

    public void setLasteupdatedate(Date lasteupdatedate) {
        this.lasteupdatedate = lasteupdatedate;
    }

    public String getLastupdateuser() {
        return this.lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getTax() {
        return this.tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public List<TblManualinvoicingdetail> getTblManualinvoicingdetails() {
        return this.tblManualinvoicingdetails;
    }

    public void setTblManualinvoicingdetails(List<TblManualinvoicingdetail> tblManualinvoicingdetails) {
        this.tblManualinvoicingdetails = tblManualinvoicingdetails;
    }

    public TblManualinvoicingdetail addTblManualinvoicingdetail(TblManualinvoicingdetail tblManualinvoicingdetail) {
        getTblManualinvoicingdetails().add(tblManualinvoicingdetail);
        tblManualinvoicingdetail.setTblManualinvoicing(this);

        return tblManualinvoicingdetail;
    }

    public TblManualinvoicingdetail removeTblManualinvoicingdetail(TblManualinvoicingdetail tblManualinvoicingdetail) {
        getTblManualinvoicingdetails().remove(tblManualinvoicingdetail);
        tblManualinvoicingdetail.setTblManualinvoicing(null);

        return tblManualinvoicingdetail;
    }

    public String getStatusdescr() {
        return statusdescr;
    }

    public void setStatusdescr(String statusdescr) {
        this.statusdescr = statusdescr;
    }
}