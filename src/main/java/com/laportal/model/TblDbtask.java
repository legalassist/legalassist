package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_DBTASKS database table.
 */
@Entity
@Table(name = "TBL_DBTASKS")
@NamedQuery(name = "TblDbtask.findAll", query = "SELECT t FROM TblDbtask t")
public class TblDbtask implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_DBTASKS_DBTASKCODE_GENERATOR", sequenceName = "SEQ_TBL_DBTASKS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_DBTASKS_DBTASKCODE_GENERATOR")
    private long dbtaskcode;

    private String completedby;

    @Temporal(TemporalType.DATE)
    private Date completedon;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String currenttask;

    private String remarks;

    private String status;

    private BigDecimal taskcode;

    //bi-directional many-to-one association to TblDbclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "DBCLAIMCODE")
    private TblDbclaim tblDbclaim;

    public TblDbtask() {
    }

    public long getDbtaskcode() {
        return this.dbtaskcode;
    }

    public void setDbtaskcode(long dbtaskcode) {
        this.dbtaskcode = dbtaskcode;
    }

    public String getCompletedby() {
        return this.completedby;
    }

    public void setCompletedby(String completedby) {
        this.completedby = completedby;
    }

    public Date getCompletedon() {
        return this.completedon;
    }

    public void setCompletedon(Date completedon) {
        this.completedon = completedon;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getCurrenttask() {
        return this.currenttask;
    }

    public void setCurrenttask(String currenttask) {
        this.currenttask = currenttask;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getTaskcode() {
        return this.taskcode;
    }

    public void setTaskcode(BigDecimal taskcode) {
        this.taskcode = taskcode;
    }

    public TblDbclaim getTblDbclaim() {
        return this.tblDbclaim;
    }

    public void setTblDbclaim(TblDbclaim tblDbclaim) {
        this.tblDbclaim = tblDbclaim;
    }

}