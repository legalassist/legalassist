package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_RTALOGS database table.
 */
@Entity
@Table(name = "TBL_RTALOGS")
@NamedQuery(name = "TblRtalog.findAll", query = "SELECT t FROM TblRtalog t")
public class TblRtalog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTALOGS_RTALOGCODE_GENERATOR", sequenceName = "SEQ_RTALOGS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTALOGS_RTALOGCODE_GENERATOR")
    private long rtalogcode;

    private Date createdon;

    private String descr;

    private String remarks;

    private String usercode;

    //bi-directional many-to-one association to TblRtaclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "RTACODE")
    private TblRtaclaim tblRtaclaim;

    private Long oldstatus;

    private Long newstatus;


    @Transient
    private String userName;

    public TblRtalog() {
    }

    public long getRtalogcode() {
        return this.rtalogcode;
    }

    public void setRtalogcode(long rtalogcode) {
        this.rtalogcode = rtalogcode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblRtaclaim getTblRtaclaim() {
        return this.tblRtaclaim;
    }

    public void setTblRtaclaim(TblRtaclaim tblRtaclaim) {
        this.tblRtaclaim = tblRtaclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getOldstatus() {
        return oldstatus;
    }

    public void setOldstatus(Long oldstatus) {
        this.oldstatus = oldstatus;
    }

    public Long getNewstatus() {
        return newstatus;
    }

    public void setNewstatus(Long newstatus) {
        this.newstatus = newstatus;
    }
}