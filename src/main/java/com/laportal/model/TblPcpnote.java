package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_PCPNOTES database table.
 */
@Entity
@Table(name = "TBL_PCPNOTES")
@NamedQuery(name = "TblPcpnote.findAll", query = "SELECT t FROM TblPcpnote t")
public class TblPcpnote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_PCPNOTES_PCPNOTECODE_GENERATOR", sequenceName = "SEQ_TBL_PCPNOTES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_PCPNOTES_PCPNOTECODE_GENERATOR")
    private long pcpnotecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String note;

    private String remarks;

    private String usercategorycode;

    private String usercode;

    //bi-directional many-to-one association to TblPcpclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PCPCLAIMCODE")
    private TblPcpclaim tblPcpclaim;

    @Transient
    private String userName;

    @Transient
    private boolean self;

    public TblPcpnote() {
    }

    public long getPcpnotecode() {
        return this.pcpnotecode;
    }

    public void setPcpnotecode(long pcpnotecode) {
        this.pcpnotecode = pcpnotecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercategorycode() {
        return this.usercategorycode;
    }

    public void setUsercategorycode(String usercategorycode) {
        this.usercategorycode = usercategorycode;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblPcpclaim getTblPcpclaim() {
        return this.tblPcpclaim;
    }

    public void setTblPcpclaim(TblPcpclaim tblPcpclaim) {
        this.tblPcpclaim = tblPcpclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }
}