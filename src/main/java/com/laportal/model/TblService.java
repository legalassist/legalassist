package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the TBL_SERVICES database table.
 */
@Entity
@Table(name = "TBL_SERVICES")
@NamedQuery(name = "TblService.findAll", query = "SELECT t FROM TblService t")
public class TblService implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_SERVICES_SERVICECODE_GENERATOR", sequenceName = "SEQ_TBL_SERVICES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_SERVICES_SERVICECODE_GENERATOR")
    private String servicecode;

    private String servicename;

    private String status;



    public TblService() {
    }

    public String getServicecode() {
        return this.servicecode;
    }

    public void setServicecode(String servicecode) {
        this.servicecode = servicecode;
    }

    public String getServicename() {
        return this.servicename;
    }

    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}