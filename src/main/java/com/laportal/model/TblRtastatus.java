package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the TBL_RTASTATUS database table.
 */
@Entity
@Table(name = "TBL_RTASTATUS")
@NamedQuery(name = "TblRtastatus.findAll", query = "SELECT t FROM TblRtastatus t")
public class TblRtastatus implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTASTATUS_STATUSCODE_GENERATOR", sequenceName = "SEQ_RTASTATUS", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTASTATUS_STATUSCODE_GENERATOR")
    private long statuscode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String status;

    private String statusname;

    private String usercode;

    // bi-directional many-to-one association to TblRtaflow
    @JsonIgnore
    @OneToMany(mappedBy = "tblRtastatus")
    private List<TblRtaflow> tblRtaflows;

    // bi-directional many-to-one association to TblHireclaim
    @JsonIgnore
    @OneToMany(mappedBy = "tblRtastatus")
    private List<TblHireclaim> tblHireclaims;

    private String compaigncode;

    public TblRtastatus() {
    }

    public long getStatuscode() {
        return this.statuscode;
    }

    public void setStatuscode(long statuscode) {
        this.statuscode = statuscode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusname() {
        return this.statusname;
    }

    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public List<TblRtaflow> getTblRtaflows() {
        return this.tblRtaflows;
    }

    public void setTblRtaflows(List<TblRtaflow> tblRtaflows) {
        this.tblRtaflows = tblRtaflows;
    }

    public List<TblHireclaim> getTblHireclaims() {
        return tblHireclaims;
    }

    public void setTblHireclaims(List<TblHireclaim> tblHireclaims) {
        this.tblHireclaims = tblHireclaims;
    }


    public String getCompaigncode() {
        return compaigncode;
    }

    public void setCompaigncode(String compaigncode) {
        this.compaigncode = compaigncode;
    }
}