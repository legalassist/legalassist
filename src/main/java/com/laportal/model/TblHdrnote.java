package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRNOTES database table.
 */
@Entity
@Table(name = "TBL_HDRNOTES")
@NamedQuery(name = "TblHdrnote.findAll", query = "SELECT t FROM TblHdrnote t")
public class TblHdrnote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRNOTES_HDRNOTECODE_GENERATOR", sequenceName = "SEQ_TBL_HDRNOTES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRNOTES_HDRNOTECODE_GENERATOR")
    private long hdrnotecode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String note;

    private String remarks;

    private String usercategorycode;

    private String usercode;

    //bi-directional many-to-one association to TblHdrclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HDRCLAIMCODE")
    private TblHdrclaim tblHdrclaim;

    @Transient
    private String userName;

    @Transient
    private boolean self;

    public TblHdrnote() {
    }

    public long getHdrnotecode() {
        return this.hdrnotecode;
    }

    public void setHdrnotecode(long hdrnotecode) {
        this.hdrnotecode = hdrnotecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercategorycode() {
        return this.usercategorycode;
    }

    public void setUsercategorycode(String usercategorycode) {
        this.usercategorycode = usercategorycode;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblHdrclaim getTblHdrclaim() {
        return tblHdrclaim;
    }

    public void setTblHdrclaim(TblHdrclaim tblHdrclaim) {
        this.tblHdrclaim = tblHdrclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }
}