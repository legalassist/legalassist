package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_TENANCYNOTES database table.
 */
@Entity
@Table(name = "TBL_TENANCYNOTES")
@NamedQuery(name = "TblTenancynote.findAll", query = "SELECT t FROM TblTenancynote t")
public class TblTenancynote implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_TENANCYNOTES_TENANCYNOTECODE_GENERATOR", sequenceName = "SEQ_TBL_TENANCYNOTES", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_TENANCYNOTES_TENANCYNOTECODE_GENERATOR")
    private long tenancynotecode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String note;

    private String remarks;

    private String usercategorycode;

    private String usercode;

    //bi-directional many-to-one association to TblTenancyclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "TENANCYCLAIMCODE")
    private TblTenancyclaim tblTenancyclaim;

    @Transient
    private String userName;

    @Transient
    private boolean self;

    public TblTenancynote() {
    }

    public long getTenancynotecode() {
        return this.tenancynotecode;
    }

    public void setTenancynotecode(long tenancynotecode) {
        this.tenancynotecode = tenancynotecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUsercategorycode() {
        return this.usercategorycode;
    }

    public void setUsercategorycode(String usercategorycode) {
        this.usercategorycode = usercategorycode;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblTenancyclaim getTblTenancyclaim() {
        return this.tblTenancyclaim;
    }

    public void setTblTenancyclaim(TblTenancyclaim tblTenancyclaim) {
        this.tblTenancyclaim = tblTenancyclaim;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isSelf() {
        return self;
    }

    public void setSelf(boolean self) {
        this.self = self;
    }
}