package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_ESIGN_STATUS database table.
 * 
 */
@Entity
@Table(name="TBL_ESIGN_STATUS")
@NamedQuery(name="TblEsignStatus.findAll", query="SELECT t FROM TblEsignStatus t")
public class TblEsignStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_ESIGN_STATUS_ESIGNSTATUSID_GENERATOR", sequenceName="TBL_ESIGN_STATUS_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_ESIGN_STATUS_ESIGNSTATUSID_GENERATOR")
	@Column(name="ESIGN_STATUS_ID")
	private long esignStatusId;

	private BigDecimal claimcode;

	@Column(name="CLIENT_BROWSER")
	private String clientBrowser;

	@Column(name="CLIENT_DEVICE")
	private String clientDevice;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
	private Date createdate;

	private String createuser;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
	@Column(name="EMAIL_DATE")
	private Date emailDate;

	@Column(name="EMAIL_SENT")
	private String emailSent;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
	@Column(name="ESIGN_OPEN_DATE")
	private Date esignOpenDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
	@Column(name="ESIGN_SUBMIT_DATE")
	private Date esignSubmitDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
	private Date lastupdatedate;

	private String lastupdateuser;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
	@Column(name="SMS_DATE")
	private Date smsDate;

	@Column(name="SMS_SENT")
	private String smsSent;

	private BigDecimal updateindex;

	//bi-directional many-to-one association to TblCompaign
	@ManyToOne
	@JoinColumn(name="COMPAIGNCODE")
	private TblCompaign tblCompaign;

	public TblEsignStatus() {
	}

	public long getEsignStatusId() {
		return this.esignStatusId;
	}

	public void setEsignStatusId(long esignStatusId) {
		this.esignStatusId = esignStatusId;
	}

	public BigDecimal getClaimcode() {
		return this.claimcode;
	}

	public void setClaimcode(BigDecimal claimcode) {
		this.claimcode = claimcode;
	}

	public String getClientBrowser() {
		return this.clientBrowser;
	}

	public void setClientBrowser(String clientBrowser) {
		this.clientBrowser = clientBrowser;
	}

	public String getClientDevice() {
		return this.clientDevice;
	}

	public void setClientDevice(String clientDevice) {
		this.clientDevice = clientDevice;
	}

	public Date getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public String getCreateuser() {
		return this.createuser;
	}

	public void setCreateuser(String createuser) {
		this.createuser = createuser;
	}

	public Date getEmailDate() {
		return this.emailDate;
	}

	public void setEmailDate(Date emailDate) {
		this.emailDate = emailDate;
	}

	public String getEmailSent() {
		return this.emailSent;
	}

	public void setEmailSent(String emailSent) {
		this.emailSent = emailSent;
	}

	public Date getEsignOpenDate() {
		return this.esignOpenDate;
	}

	public void setEsignOpenDate(Date esignOpenDate) {
		this.esignOpenDate = esignOpenDate;
	}

	public Date getEsignSubmitDate() {
		return this.esignSubmitDate;
	}

	public void setEsignSubmitDate(Date esignSubmitDate) {
		this.esignSubmitDate = esignSubmitDate;
	}

	public Date getLastupdatedate() {
		return this.lastupdatedate;
	}

	public void setLastupdatedate(Date lastupdatedate) {
		this.lastupdatedate = lastupdatedate;
	}

	public String getLastupdateuser() {
		return this.lastupdateuser;
	}

	public void setLastupdateuser(String lastupdateuser) {
		this.lastupdateuser = lastupdateuser;
	}

	public Date getSmsDate() {
		return this.smsDate;
	}

	public void setSmsDate(Date smsDate) {
		this.smsDate = smsDate;
	}

	public String getSmsSent() {
		return this.smsSent;
	}

	public void setSmsSent(String smsSent) {
		this.smsSent = smsSent;
	}

	public BigDecimal getUpdateindex() {
		return this.updateindex;
	}

	public void setUpdateindex(BigDecimal updateindex) {
		this.updateindex = updateindex;
	}

	public TblCompaign getTblCompaign() {
		return this.tblCompaign;
	}

	public void setTblCompaign(TblCompaign tblCompaign) {
		this.tblCompaign = tblCompaign;
	}

}