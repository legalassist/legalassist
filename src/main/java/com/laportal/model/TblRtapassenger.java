package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_RTAPASSENGERS database table.
 */
@Entity
@Table(name = "TBL_RTAPASSENGERS")
@NamedQuery(name = "TblRtapassenger.findAll", query = "SELECT t FROM TblRtapassenger t")
public class TblRtapassenger implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTAPASSENGERS_PASSENGERCODE_GENERATOR", sequenceName = "SEQ_RTAPASSENGER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTAPASSENGERS_PASSENGERCODE_GENERATOR")
    private long passengercode;

    private String address1;

    private String address2;

    private String address3;

    private String city;


    @Temporal(TemporalType.DATE)
    private Date dob;

    private String driverpassenger;

    private String email;

    private String englishlevel;

    private String firstname;

    private String gaddress1;

    private String gaddress2;

    private String gaddress3;

    private String gcity;

    private String gemail;

    private String gfirstname;

    private String glandline;

    private String glastname;

    private String gmiddlename;

    private String gmobile;

    private String gpostalcode;

    private String gregion;

    private String gtitle;

    private String injclasscode;

    private String injdescr;

    private BigDecimal injlength;

    private String landline;

    private String lastname;

    private String medicalinfo;

    private String middlename;

    private String mobile;

    private String ninumber;

    private String ongoing;

    private String postalcode;

    private String region;

    private String remarks;

    private String title;
    private String alternativenumber;
    private String evidencedetails;
    private String details;

    private Date gdob;

    //bi-directional many-to-one association to TblRtaclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "RTACODE")
    private TblRtaclaim tblRtaclaim;

    @Transient
    List<TblRtaPassengerInjury> pasengerTblRtainjuries = new ArrayList<>();

    public TblRtapassenger() {
    }

    public long getPassengercode() {
        return this.passengercode;
    }

    public void setPassengercode(long passengercode) {
        this.passengercode = passengercode;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return this.address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getDriverpassenger() {
        return this.driverpassenger;
    }

    public void setDriverpassenger(String driverpassenger) {
        this.driverpassenger = driverpassenger;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnglishlevel() {
        return this.englishlevel;
    }

    public void setEnglishlevel(String englishlevel) {
        this.englishlevel = englishlevel;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGaddress1() {
        return this.gaddress1;
    }

    public void setGaddress1(String gaddress1) {
        this.gaddress1 = gaddress1;
    }

    public String getGaddress2() {
        return this.gaddress2;
    }

    public void setGaddress2(String gaddress2) {
        this.gaddress2 = gaddress2;
    }

    public String getGaddress3() {
        return this.gaddress3;
    }

    public void setGaddress3(String gaddress3) {
        this.gaddress3 = gaddress3;
    }

    public String getGcity() {
        return this.gcity;
    }

    public void setGcity(String gcity) {
        this.gcity = gcity;
    }

    public String getGemail() {
        return this.gemail;
    }

    public void setGemail(String gemail) {
        this.gemail = gemail;
    }

    public String getGfirstname() {
        return this.gfirstname;
    }

    public void setGfirstname(String gfirstname) {
        this.gfirstname = gfirstname;
    }

    public String getGlandline() {
        return this.glandline;
    }

    public void setGlandline(String glandline) {
        this.glandline = glandline;
    }

    public String getGlastname() {
        return this.glastname;
    }

    public void setGlastname(String glastname) {
        this.glastname = glastname;
    }

    public String getGmiddlename() {
        return this.gmiddlename;
    }

    public void setGmiddlename(String gmiddlename) {
        this.gmiddlename = gmiddlename;
    }

    public String getGmobile() {
        return this.gmobile;
    }

    public void setGmobile(String gmobile) {
        this.gmobile = gmobile;
    }

    public String getGpostalcode() {
        return this.gpostalcode;
    }

    public void setGpostalcode(String gpostalcode) {
        this.gpostalcode = gpostalcode;
    }

    public String getGregion() {
        return this.gregion;
    }

    public void setGregion(String gregion) {
        this.gregion = gregion;
    }

    public String getGtitle() {
        return this.gtitle;
    }

    public void setGtitle(String gtitle) {
        this.gtitle = gtitle;
    }

    public String getInjclasscode() {
        return this.injclasscode;
    }

    public void setInjclasscode(String injclasscode) {
        this.injclasscode = injclasscode;
    }

    public String getInjdescr() {
        return this.injdescr;
    }

    public void setInjdescr(String injdescr) {
        this.injdescr = injdescr;
    }

    public BigDecimal getInjlength() {
        return this.injlength;
    }

    public void setInjlength(BigDecimal injlength) {
        this.injlength = injlength;
    }

    public String getLandline() {
        return this.landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMedicalinfo() {
        return this.medicalinfo;
    }

    public void setMedicalinfo(String medicalinfo) {
        this.medicalinfo = medicalinfo;
    }

    public String getMiddlename() {
        return this.middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNinumber() {
        return this.ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getOngoing() {
        return this.ongoing;
    }

    public void setOngoing(String ongoing) {
        this.ongoing = ongoing;
    }

    public String getPostalcode() {
        return this.postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getRegion() {
        return this.region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TblRtaclaim getTblRtaclaim() {
        return this.tblRtaclaim;
    }

    public void setTblRtaclaim(TblRtaclaim tblRtaclaim) {
        this.tblRtaclaim = tblRtaclaim;
    }

    public String getAlternativenumber() {
        return alternativenumber;
    }

    public void setAlternativenumber(String alternativenumber) {
        this.alternativenumber = alternativenumber;
    }

    public String getEvidencedetails() {
        return evidencedetails;
    }

    public void setEvidencedetails(String evidencedatails) {
        this.evidencedetails = evidencedatails;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String detail) {
        this.details = detail;
    }


    public Date getGdob() {
        return gdob;
    }

    public void setGdob(Date gdob) {
        this.gdob = gdob;
    }

    public List<TblRtaPassengerInjury> getPasengerTblRtainjuries() {
        return pasengerTblRtainjuries;
    }

    public void setPasengerTblRtainjuries(List<TblRtaPassengerInjury> pasengerTblRtainjuries) {
        this.pasengerTblRtainjuries = pasengerTblRtainjuries;
    }
}