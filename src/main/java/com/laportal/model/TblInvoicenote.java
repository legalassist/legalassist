package com.laportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the TBL_INVOICENOTES database table.
 * 
 */
@Entity
@Table(name="TBL_INVOICENOTES")
@NamedQuery(name="TblInvoicenote.findAll", query="SELECT t FROM TblInvoicenote t")
public class TblInvoicenote implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TBL_INVOICENOTES_INVOICENOTESCODE_GENERATOR", sequenceName="SEQ_TBL_INVOICENOTES",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_INVOICENOTES_INVOICENOTESCODE_GENERATOR")
	private long invoicenotescode;


	private Date createdon;

	private String note;

	private String remarks;

	private String usercategorycode;

	private String usercode;

	//bi-directional many-to-one association to TblSolicitorInvoiceHead
	@ManyToOne
	@JoinColumn(name="INVOICEHEADID")
	private TblSolicitorInvoiceHead tblSolicitorInvoiceHead;

	@Transient
	private String userName;

	@Transient
	private boolean self;

	public TblInvoicenote() {
	}

	public long getInvoicenotescode() {
		return this.invoicenotescode;
	}

	public void setInvoicenotescode(long invoicenotescode) {
		this.invoicenotescode = invoicenotescode;
	}

	public Date getCreatedon() {
		return this.createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getUsercategorycode() {
		return this.usercategorycode;
	}

	public void setUsercategorycode(String usercategorycode) {
		this.usercategorycode = usercategorycode;
	}

	public String getUsercode() {
		return this.usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public TblSolicitorInvoiceHead getTblSolicitorInvoiceHead() {
		return this.tblSolicitorInvoiceHead;
	}

	public void setTblSolicitorInvoiceHead(TblSolicitorInvoiceHead tblSolicitorInvoiceHead) {
		this.tblSolicitorInvoiceHead = tblSolicitorInvoiceHead;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isSelf() {
		return self;
	}

	public void setSelf(boolean self) {
		this.self = self;
	}
}