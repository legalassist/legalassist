package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_ROLES database table.
 */
@Entity
@Table(name = "TBL_ROLES")
@NamedQuery(name = "TblRole.findAll", query = "SELECT t FROM TblRole t")
public class TblRole implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
//	@SequenceGenerator(name="TBL_ROLES_ROLECODE_GENERATOR", sequenceName="SEQ_TBL_ROLES",allocationSize = 1)
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TBL_ROLES_ROLECODE_GENERATOR")
    private String rolecode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String descr;

    private String remarks;

    private String rolename;

    private String rolestatus;

    private String usercode;

    //bi-directional many-to-one association to TblCompaign
    @ManyToOne
    @JoinColumn(name = "COMPAIGNCODE")
    private TblCompaign tblCompaign;

    //bi-directional many-to-one association to TblUsercategory
    @ManyToOne
    @JoinColumn(name = "CATEGORYCODE")
    private TblUsercategory tblUsercategory;

    public TblRole() {
    }

    public String getRolecode() {
        return this.rolecode;
    }

    public void setRolecode(String rolecode) {
        this.rolecode = rolecode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getDescr() {
        return this.descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRolename() {
        return this.rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getRolestatus() {
        return this.rolestatus;
    }

    public void setRolestatus(String rolestatus) {
        this.rolestatus = rolestatus;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblCompaign getTblCompaign() {
        return this.tblCompaign;
    }

    public void setTblCompaign(TblCompaign tblCompaign) {
        this.tblCompaign = tblCompaign;
    }

    public TblUsercategory getTblUsercategory() {
        return this.tblUsercategory;
    }

    public void setTblUsercategory(TblUsercategory tblUsercategory) {
        this.tblUsercategory = tblUsercategory;
    }

}