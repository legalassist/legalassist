package com.laportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VIEW_MNCLAIM database table.
 * 
 */
@Entity
@Table(name="VIEW_MNCLAIM")
@NamedQuery(name="ViewMnclaim.findAll", query="SELECT v FROM ViewMnclaim v")
public class ViewMnclaim implements Serializable {
	private static final long serialVersionUID = 1L;

	private String agentadviced;

	private String anynotes;

	private String anyotherlosses;

	@Temporal(TemporalType.DATE)
	private Date calldate;

	private String calltime;

	private String clientaddress;

	private String clientcontactno;

	@Temporal(TemporalType.DATE)
	private Date clientdob;

	private String clientemail;

	private String clientname;

	private String clientninumber;

	private String clientoccupation;

	private String clienttitle;

	private String complaintmade;

	private String complaintresponded;

	@Temporal(TemporalType.DATE)
	private Date createdate;

	private BigDecimal createuser;

	private String emergencysrvcattend;

	private String esign;

	@Temporal(TemporalType.DATE)
	private Date esigndate;

	private String esignstatus;

	private String gdpr;

	private String gpdetails;

	private String hospitalattend;

	private String hospitaldetails;

	private String hotkeyed;

	@Temporal(TemporalType.DATE)
	private Date incidentdatetime;

	private String injuries;

	private BigDecimal introducer;

	@Column(name="INTRODUCER_COMPANY")
	private String introducerCompany;

	@Column(name="INTRODUCER_USER")
	private String introducerUser;

	@Temporal(TemporalType.DATE)
	@Column(name="INVOICE_DATE")
	private Date invoiceDate;

	@Column(name="IS_INTRODUCER_INVOICED")
	private String isIntroducerInvoiced;

	@Column(name="IS_SOLICITOR_INVOICED")
	private String isSolicitorInvoiced;

	private String location;

	@Temporal(TemporalType.DATE)
	private Date medicalattenddate;

	@Id
	private BigDecimal mnclaimcode;

	private String mncode;

	private BigDecimal mntaskcode;

	private String mntaskname;

	private String negligencedescr;

	private String notesdetail;

	private String other;

	private String password;

	private String policeref;

	private String refundedclientinformed;

	private String remarks;

	private String separatecomplaintcontact;

	@Column(name="SOLICITOR_COMPANY")
	private String solicitorCompany;

	@Column(name="SOLICITOR_COMPANYCODE")
	private String solicitorCompanycode;

	@Column(name="SOLICITOR_USER")
	private String solicitorUser;

	@Column(name="SOLICITOR_USERCODE")
	private BigDecimal solicitorUsercode;

	private String status;

	private String stilloffwork;

	private String taskflag;

	private String timeoffwork;

	private String timesincecomplaint;

	private String tpaddress;

	private String tpcompany;

	private String tpcontactno;

	private String tpname;

	@Temporal(TemporalType.DATE)
	private Date updatedate;

	private String witnesscontactdetails;

	private String witnesses;

	private String witnessname;

	public ViewMnclaim() {
	}

	public String getAgentadviced() {
		return this.agentadviced;
	}

	public void setAgentadviced(String agentadviced) {
		this.agentadviced = agentadviced;
	}

	public String getAnynotes() {
		return this.anynotes;
	}

	public void setAnynotes(String anynotes) {
		this.anynotes = anynotes;
	}

	public String getAnyotherlosses() {
		return this.anyotherlosses;
	}

	public void setAnyotherlosses(String anyotherlosses) {
		this.anyotherlosses = anyotherlosses;
	}

	public Date getCalldate() {
		return this.calldate;
	}

	public void setCalldate(Date calldate) {
		this.calldate = calldate;
	}

	public String getCalltime() {
		return this.calltime;
	}

	public void setCalltime(String calltime) {
		this.calltime = calltime;
	}

	public String getClientaddress() {
		return this.clientaddress;
	}

	public void setClientaddress(String clientaddress) {
		this.clientaddress = clientaddress;
	}

	public String getClientcontactno() {
		return this.clientcontactno;
	}

	public void setClientcontactno(String clientcontactno) {
		this.clientcontactno = clientcontactno;
	}

	public Date getClientdob() {
		return this.clientdob;
	}

	public void setClientdob(Date clientdob) {
		this.clientdob = clientdob;
	}

	public String getClientemail() {
		return this.clientemail;
	}

	public void setClientemail(String clientemail) {
		this.clientemail = clientemail;
	}

	public String getClientname() {
		return this.clientname;
	}

	public void setClientname(String clientname) {
		this.clientname = clientname;
	}

	public String getClientninumber() {
		return this.clientninumber;
	}

	public void setClientninumber(String clientninumber) {
		this.clientninumber = clientninumber;
	}

	public String getClientoccupation() {
		return this.clientoccupation;
	}

	public void setClientoccupation(String clientoccupation) {
		this.clientoccupation = clientoccupation;
	}

	public String getClienttitle() {
		return this.clienttitle;
	}

	public void setClienttitle(String clienttitle) {
		this.clienttitle = clienttitle;
	}

	public String getComplaintmade() {
		return this.complaintmade;
	}

	public void setComplaintmade(String complaintmade) {
		this.complaintmade = complaintmade;
	}

	public String getComplaintresponded() {
		return this.complaintresponded;
	}

	public void setComplaintresponded(String complaintresponded) {
		this.complaintresponded = complaintresponded;
	}

	public Date getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public BigDecimal getCreateuser() {
		return this.createuser;
	}

	public void setCreateuser(BigDecimal createuser) {
		this.createuser = createuser;
	}

	public String getEmergencysrvcattend() {
		return this.emergencysrvcattend;
	}

	public void setEmergencysrvcattend(String emergencysrvcattend) {
		this.emergencysrvcattend = emergencysrvcattend;
	}

	public String getEsign() {
		return this.esign;
	}

	public void setEsign(String esign) {
		this.esign = esign;
	}

	public Date getEsigndate() {
		return this.esigndate;
	}

	public void setEsigndate(Date esigndate) {
		this.esigndate = esigndate;
	}

	public String getEsignstatus() {
		return this.esignstatus;
	}

	public void setEsignstatus(String esignstatus) {
		this.esignstatus = esignstatus;
	}

	public String getGdpr() {
		return this.gdpr;
	}

	public void setGdpr(String gdpr) {
		this.gdpr = gdpr;
	}

	public String getGpdetails() {
		return this.gpdetails;
	}

	public void setGpdetails(String gpdetails) {
		this.gpdetails = gpdetails;
	}

	public String getHospitalattend() {
		return this.hospitalattend;
	}

	public void setHospitalattend(String hospitalattend) {
		this.hospitalattend = hospitalattend;
	}

	public String getHospitaldetails() {
		return this.hospitaldetails;
	}

	public void setHospitaldetails(String hospitaldetails) {
		this.hospitaldetails = hospitaldetails;
	}

	public String getHotkeyed() {
		return this.hotkeyed;
	}

	public void setHotkeyed(String hotkeyed) {
		this.hotkeyed = hotkeyed;
	}

	public Date getIncidentdatetime() {
		return this.incidentdatetime;
	}

	public void setIncidentdatetime(Date incidentdatetime) {
		this.incidentdatetime = incidentdatetime;
	}

	public String getInjuries() {
		return this.injuries;
	}

	public void setInjuries(String injuries) {
		this.injuries = injuries;
	}

	public BigDecimal getIntroducer() {
		return this.introducer;
	}

	public void setIntroducer(BigDecimal introducer) {
		this.introducer = introducer;
	}

	public String getIntroducerCompany() {
		return this.introducerCompany;
	}

	public void setIntroducerCompany(String introducerCompany) {
		this.introducerCompany = introducerCompany;
	}

	public String getIntroducerUser() {
		return this.introducerUser;
	}

	public void setIntroducerUser(String introducerUser) {
		this.introducerUser = introducerUser;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getIsIntroducerInvoiced() {
		return this.isIntroducerInvoiced;
	}

	public void setIsIntroducerInvoiced(String isIntroducerInvoiced) {
		this.isIntroducerInvoiced = isIntroducerInvoiced;
	}

	public String getIsSolicitorInvoiced() {
		return this.isSolicitorInvoiced;
	}

	public void setIsSolicitorInvoiced(String isSolicitorInvoiced) {
		this.isSolicitorInvoiced = isSolicitorInvoiced;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getMedicalattenddate() {
		return this.medicalattenddate;
	}

	public void setMedicalattenddate(Date medicalattenddate) {
		this.medicalattenddate = medicalattenddate;
	}

	public BigDecimal getMnclaimcode() {
		return this.mnclaimcode;
	}

	public void setMnclaimcode(BigDecimal mnclaimcode) {
		this.mnclaimcode = mnclaimcode;
	}

	public String getMncode() {
		return this.mncode;
	}

	public void setMncode(String mncode) {
		this.mncode = mncode;
	}

	public BigDecimal getMntaskcode() {
		return this.mntaskcode;
	}

	public void setMntaskcode(BigDecimal mntaskcode) {
		this.mntaskcode = mntaskcode;
	}

	public String getMntaskname() {
		return this.mntaskname;
	}

	public void setMntaskname(String mntaskname) {
		this.mntaskname = mntaskname;
	}

	public String getNegligencedescr() {
		return this.negligencedescr;
	}

	public void setNegligencedescr(String negligencedescr) {
		this.negligencedescr = negligencedescr;
	}

	public String getNotesdetail() {
		return this.notesdetail;
	}

	public void setNotesdetail(String notesdetail) {
		this.notesdetail = notesdetail;
	}

	public String getOther() {
		return this.other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPoliceref() {
		return this.policeref;
	}

	public void setPoliceref(String policeref) {
		this.policeref = policeref;
	}

	public String getRefundedclientinformed() {
		return this.refundedclientinformed;
	}

	public void setRefundedclientinformed(String refundedclientinformed) {
		this.refundedclientinformed = refundedclientinformed;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSeparatecomplaintcontact() {
		return this.separatecomplaintcontact;
	}

	public void setSeparatecomplaintcontact(String separatecomplaintcontact) {
		this.separatecomplaintcontact = separatecomplaintcontact;
	}

	public String getSolicitorCompany() {
		return this.solicitorCompany;
	}

	public void setSolicitorCompany(String solicitorCompany) {
		this.solicitorCompany = solicitorCompany;
	}

	public String getSolicitorCompanycode() {
		return this.solicitorCompanycode;
	}

	public void setSolicitorCompanycode(String solicitorCompanycode) {
		this.solicitorCompanycode = solicitorCompanycode;
	}

	public String getSolicitorUser() {
		return this.solicitorUser;
	}

	public void setSolicitorUser(String solicitorUser) {
		this.solicitorUser = solicitorUser;
	}

	public BigDecimal getSolicitorUsercode() {
		return this.solicitorUsercode;
	}

	public void setSolicitorUsercode(BigDecimal solicitorUsercode) {
		this.solicitorUsercode = solicitorUsercode;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStilloffwork() {
		return this.stilloffwork;
	}

	public void setStilloffwork(String stilloffwork) {
		this.stilloffwork = stilloffwork;
	}

	public String getTaskflag() {
		return this.taskflag;
	}

	public void setTaskflag(String taskflag) {
		this.taskflag = taskflag;
	}

	public String getTimeoffwork() {
		return this.timeoffwork;
	}

	public void setTimeoffwork(String timeoffwork) {
		this.timeoffwork = timeoffwork;
	}

	public String getTimesincecomplaint() {
		return this.timesincecomplaint;
	}

	public void setTimesincecomplaint(String timesincecomplaint) {
		this.timesincecomplaint = timesincecomplaint;
	}

	public String getTpaddress() {
		return this.tpaddress;
	}

	public void setTpaddress(String tpaddress) {
		this.tpaddress = tpaddress;
	}

	public String getTpcompany() {
		return this.tpcompany;
	}

	public void setTpcompany(String tpcompany) {
		this.tpcompany = tpcompany;
	}

	public String getTpcontactno() {
		return this.tpcontactno;
	}

	public void setTpcontactno(String tpcontactno) {
		this.tpcontactno = tpcontactno;
	}

	public String getTpname() {
		return this.tpname;
	}

	public void setTpname(String tpname) {
		this.tpname = tpname;
	}

	public Date getUpdatedate() {
		return this.updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public String getWitnesscontactdetails() {
		return this.witnesscontactdetails;
	}

	public void setWitnesscontactdetails(String witnesscontactdetails) {
		this.witnesscontactdetails = witnesscontactdetails;
	}

	public String getWitnesses() {
		return this.witnesses;
	}

	public void setWitnesses(String witnesses) {
		this.witnesses = witnesses;
	}

	public String getWitnessname() {
		return this.witnessname;
	}

	public void setWitnessname(String witnessname) {
		this.witnessname = witnessname;
	}

}