package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_RTASOLICITOR database table.
 */
@Entity
@Table(name = "TBL_RTASOLICITOR")
@NamedQuery(name = "TblRtasolicitor.findAll", query = "SELECT t FROM TblRtasolicitor t")
public class TblRtasolicitor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_RTASOLICITOR_RTASOLCODE_GENERATOR", sequenceName = "SEQ_RTASOLICITOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_RTASOLICITOR_RTASOLCODE_GENERATOR")
    private long rtasolcode;

    private String companycode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String reference1;

    private String reference2;

    private String remarks;

    private BigDecimal rtacode;

    private String status;

    private String usercode;

    public TblRtasolicitor() {
    }

    public long getRtasolcode() {
        return this.rtasolcode;
    }

    public void setRtasolcode(long rtasolcode) {
        this.rtasolcode = rtasolcode;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getReference1() {
        return this.reference1;
    }

    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getReference2() {
        return this.reference2;
    }

    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getRtacode() {
        return this.rtacode;
    }

    public void setRtacode(BigDecimal rtacode) {
        this.rtacode = rtacode;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

}