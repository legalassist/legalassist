package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.laportal.dto.HdrActionButton;
import com.laportal.dto.HdrCopyLog;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_HDRCLAIM database table.
 */
@Entity
@Table(name = "TBL_HDRCLAIM")
@NamedQuery(name = "TblHdrclaim.findAll", query = "SELECT t FROM TblHdrclaim t")
public class TblHdrclaim implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRCLAIM_HDRCLAIMCODE_GENERATOR", sequenceName = "SEQ_TBL_HDRCLAIM", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRCLAIM_HDRCLAIMCODE_GENERATOR")
    private long hdrclaimcode;

    @Column(name = "APRX_REPORTEDDATE")
    private String aprxReporteddate;


    @Temporal(TemporalType.DATE)
    @Column(name = "DEFECT_LASTREPORTEDON")
    private Date defectLastreportedon;

    @Column(name = "DEFECT_REPORTED")
    private BigDecimal defectReported;

    @Column(name = "HEALTH_AFFECTED")
    private String healthAffected;

    @Column(name = "HEALTH_REL_DETAILS")
    private String healthRelDetails;

    @Column(name = "LL_CONTACTNO")
    private String llContactno;

    @Column(name = "LL_NAME")
    private String llName;

    @Column(name = "LL_RESPOND_DETAIL")
    private String llRespondDetail;

    @Column(name = "LL_RESPONDED")
    private String llResponded;

    @Column(name = "REPORTED_TO_LL")
    private String reportedToLl;

    @Column(name = "REPORTED_TO_LL_BY")
    private String reportedToLlBy;

    private BigDecimal statuscode;

    private String claimcode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date createdate;

    private BigDecimal createuser;

    private String esig;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date esigdate;

    private String remarks;

    //bi-directional many-to-one association to TblHdraffectedper
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdraffectedper> tblHdraffectedpers;

    //bi-directional many-to-one association to TblHdraffectedroom
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdraffectedroom> tblHdraffectedrooms;

    //bi-directional many-to-one association to TblHdrclaimant
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdrclaimant> tblHdrclaimants;

    //bi-directional many-to-one association to TblHdrjointtenancy
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdrjointtenancy> tblHdrjointtenancies;

    //bi-directional many-to-one association to TblHdrtenancy
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdrtenancy> tblHdrtenancies;

    //bi-directional many-to-one association to TblHdrdocument
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdrdocument> tblHdrdocuments;

    //bi-directional many-to-one association to TblHdrlog
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdrlog> tblHdrlogs;

    //bi-directional many-to-one association to TblHdrmessage
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdrmessage> tblHdrmessages;

    //bi-directional many-to-one association to TblHdrnote
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdrnote> tblHdrnotes;

    //bi-directional many-to-one association to TblHdrtask
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdrtask> hdrCaseTasks;


    @Temporal(TemporalType.DATE)
    @Column(name = "DEFECT_FIRSTREPORTEDON")
    private Date defectFirstreportedon;

    //bi-directional many-to-one association to TblHdrsolicitor
    @OneToMany(mappedBy = "tblHdrclaim")
    private List<TblHdrsolicitor> tblHdrsolicitors;

    @Transient
    private List<HdrActionButton> hdrActionButton;

    @Transient
    private TblUser tblUser;

    @Transient
    private String statusDescr;

    @Transient
    private List<HdrActionButton> hdrActionButtonForLA;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date updatedate;

    private BigDecimal advisor;
    private BigDecimal introducer;

    @Transient
    private String advisorname;
    @Transient
    private String introducername;

    @Column(name = "LASTUPDATEUSER")
    private String lastupdateuser;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "CLAWBACK_DATE")
    private Date clawbackDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "SUBMIT_DATE")
    private Date submitDate;

    @Transient
    private BigDecimal introducerInvoiceHeadId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @Transient
    private Date introducerInvoiceDate;


    @Transient
    private BigDecimal solicitorInvoiceHeadId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @Transient
    private Date solicitorInvoiceDate;

    @Transient
    private TblEsignStatus tblEsignStatus;

    @Transient
    private String editFlag;

    @Transient
    private List<HdrCopyLog> hdrCopyLogs = new ArrayList<>();


    public TblHdrclaim() {
    }

    public long getHdrclaimcode() {
        return this.hdrclaimcode;
    }

    public void setHdrclaimcode(long hdrclaimcode) {
        this.hdrclaimcode = hdrclaimcode;
    }

    public String getAprxReporteddate() {
        return this.aprxReporteddate;
    }

    public void setAprxReporteddate(String aprxReporteddate) {
        this.aprxReporteddate = aprxReporteddate;
    }

    public Date getDefectLastreportedon() {
        return this.defectLastreportedon;
    }

    public void setDefectLastreportedon(Date defectLastreportedon) {
        this.defectLastreportedon = defectLastreportedon;
    }

    public BigDecimal getDefectReported() {
        return this.defectReported;
    }

    public void setDefectReported(BigDecimal defectReported) {
        this.defectReported = defectReported;
    }

    public String getHealthAffected() {
        return this.healthAffected;
    }

    public void setHealthAffected(String healthAffected) {
        this.healthAffected = healthAffected;
    }

    public String getHealthRelDetails() {
        return this.healthRelDetails;
    }

    public void setHealthRelDetails(String healthRelDetails) {
        this.healthRelDetails = healthRelDetails;
    }

    public String getLlContactno() {
        return this.llContactno;
    }

    public void setLlContactno(String llContactno) {
        this.llContactno = llContactno;
    }

    public String getLlName() {
        return this.llName;
    }

    public void setLlName(String llName) {
        this.llName = llName;
    }

    public String getLlRespondDetail() {
        return this.llRespondDetail;
    }

    public void setLlRespondDetail(String llRespondDetail) {
        this.llRespondDetail = llRespondDetail;
    }

    public String getLlResponded() {
        return this.llResponded;
    }

    public void setLlResponded(String llResponded) {
        this.llResponded = llResponded;
    }

    public String getReportedToLl() {
        return this.reportedToLl;
    }

    public void setReportedToLl(String reportedToLl) {
        this.reportedToLl = reportedToLl;
    }

    public String getReportedToLlBy() {
        return this.reportedToLlBy;
    }

    public void setReportedToLlBy(String reportedToLlBy) {
        this.reportedToLlBy = reportedToLlBy;
    }

    public List<TblHdraffectedper> getTblHdraffectedpers() {
        return this.tblHdraffectedpers;
    }

    public void setTblHdraffectedpers(List<TblHdraffectedper> tblHdraffectedpers) {
        this.tblHdraffectedpers = tblHdraffectedpers;
    }

    public TblHdraffectedper addTblHdraffectedper(TblHdraffectedper tblHdraffectedper) {
        getTblHdraffectedpers().add(tblHdraffectedper);
        tblHdraffectedper.setTblHdrclaim(this);

        return tblHdraffectedper;
    }

    public TblHdraffectedper removeTblHdraffectedper(TblHdraffectedper tblHdraffectedper) {
        getTblHdraffectedpers().remove(tblHdraffectedper);
        tblHdraffectedper.setTblHdrclaim(null);

        return tblHdraffectedper;
    }

    public List<TblHdraffectedroom> getTblHdraffectedrooms() {
        return this.tblHdraffectedrooms;
    }

    public void setTblHdraffectedrooms(List<TblHdraffectedroom> tblHdraffectedrooms) {
        this.tblHdraffectedrooms = tblHdraffectedrooms;
    }

    public TblHdraffectedroom addTblHdraffectedroom(TblHdraffectedroom tblHdraffectedroom) {
        getTblHdraffectedrooms().add(tblHdraffectedroom);
        tblHdraffectedroom.setTblHdrclaim(this);

        return tblHdraffectedroom;
    }

    public TblHdraffectedroom removeTblHdraffectedroom(TblHdraffectedroom tblHdraffectedroom) {
        getTblHdraffectedrooms().remove(tblHdraffectedroom);
        tblHdraffectedroom.setTblHdrclaim(null);

        return tblHdraffectedroom;
    }

    public List<TblHdrclaimant> getTblHdrclaimants() {
        return this.tblHdrclaimants;
    }

    public void setTblHdrclaimants(List<TblHdrclaimant> tblHdrclaimants) {
        this.tblHdrclaimants = tblHdrclaimants;
    }

    public TblHdrclaimant addTblHdrclaimant(TblHdrclaimant tblHdrclaimant) {
        getTblHdrclaimants().add(tblHdrclaimant);
        tblHdrclaimant.setTblHdrclaim(this);

        return tblHdrclaimant;
    }

    public TblHdrclaimant removeTblHdrclaimant(TblHdrclaimant tblHdrclaimant) {
        getTblHdrclaimants().remove(tblHdrclaimant);
        tblHdrclaimant.setTblHdrclaim(null);

        return tblHdrclaimant;
    }

    public List<TblHdrjointtenancy> getTblHdrjointtenancies() {
        return this.tblHdrjointtenancies;
    }

    public void setTblHdrjointtenancies(List<TblHdrjointtenancy> tblHdrjointtenancies) {
        this.tblHdrjointtenancies = tblHdrjointtenancies;
    }

    public TblHdrjointtenancy addTblHdrjointtenancy(TblHdrjointtenancy tblHdrjointtenancy) {
        getTblHdrjointtenancies().add(tblHdrjointtenancy);
        tblHdrjointtenancy.setTblHdrclaim(this);

        return tblHdrjointtenancy;
    }

    public TblHdrjointtenancy removeTblHdrjointtenancy(TblHdrjointtenancy tblHdrjointtenancy) {
        getTblHdrjointtenancies().remove(tblHdrjointtenancy);
        tblHdrjointtenancy.setTblHdrclaim(null);

        return tblHdrjointtenancy;
    }

    public List<TblHdrtenancy> getTblHdrtenancies() {
        return this.tblHdrtenancies;
    }

    public void setTblHdrtenancies(List<TblHdrtenancy> tblHdrtenancies) {
        this.tblHdrtenancies = tblHdrtenancies;
    }

    public TblHdrtenancy addTblHdrtenancy(TblHdrtenancy tblHdrtenancy) {
        getTblHdrtenancies().add(tblHdrtenancy);
        tblHdrtenancy.setTblHdrclaim(this);

        return tblHdrtenancy;
    }

    public TblHdrtenancy removeTblHdrtenancy(TblHdrtenancy tblHdrtenancy) {
        getTblHdrtenancies().remove(tblHdrtenancy);
        tblHdrtenancy.setTblHdrclaim(null);

        return tblHdrtenancy;
    }

    public BigDecimal getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(BigDecimal statuscode) {
        this.statuscode = statuscode;
    }

    public String getClaimcode() {
        return claimcode;
    }

    public void setClaimcode(String claimcode) {
        this.claimcode = claimcode;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public List<TblHdrdocument> getTblHdrdocuments() {
        return tblHdrdocuments;
    }

    public void setTblHdrdocuments(List<TblHdrdocument> tblHdrdocuments) {
        this.tblHdrdocuments = tblHdrdocuments;
    }

    public List<TblHdrlog> getTblHdrlogs() {
        return tblHdrlogs;
    }

    public void setTblHdrlogs(List<TblHdrlog> tblHdrlogs) {
        this.tblHdrlogs = tblHdrlogs;
    }

    public List<TblHdrmessage> getTblHdrmessages() {
        return tblHdrmessages;
    }

    public void setTblHdrmessages(List<TblHdrmessage> tblHdrmessages) {
        this.tblHdrmessages = tblHdrmessages;
    }

    public List<TblHdrnote> getTblHdrnotes() {
        return tblHdrnotes;
    }

    public void setTblHdrnotes(List<TblHdrnote> tblHdrnotes) {
        this.tblHdrnotes = tblHdrnotes;
    }

    public List<TblHdrtask> getHdrCaseTasks() {
        return hdrCaseTasks;
    }

    public void setHdrCaseTasks(List<TblHdrtask> tblHdrtasks) {
        this.hdrCaseTasks = tblHdrtasks;
    }

    public String getEsig() {
        return esig;
    }

    public void setEsig(String esig) {
        this.esig = esig;
    }

    public Date getEsigdate() {
        return esigdate;
    }

    public void setEsigdate(Date esigdate) {
        this.esigdate = esigdate;
    }

    public Date getDefectFirstreportedon() {
        return defectFirstreportedon;
    }

    public void setDefectFirstreportedon(Date defectFirstreportedon) {
        this.defectFirstreportedon = defectFirstreportedon;
    }

    public List<TblHdrsolicitor> getTblHdrsolicitors() {
        return tblHdrsolicitors;
    }

    public void setTblHdrsolicitors(List<TblHdrsolicitor> tblHdrsolicitors) {
        this.tblHdrsolicitors = tblHdrsolicitors;
    }

    public List<HdrActionButton> getHdrActionButton() {
        return hdrActionButton;
    }

    public void setHdrActionButton(List<HdrActionButton> hdrActionButton) {
        this.hdrActionButton = hdrActionButton;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public TblUser getTblUser() {
        return tblUser;
    }

    public void setTblUser(TblUser tblUser) {
        this.tblUser = tblUser;
    }

    public String getStatusDescr() {
        return statusDescr;
    }

    public void setStatusDescr(String statusDescr) {
        this.statusDescr = statusDescr;
    }

    public List<HdrActionButton> getHdrActionButtonForLA() {
        return hdrActionButtonForLA;
    }

    public void setHdrActionButtonForLA(List<HdrActionButton> hdrActionButtonForLA) {
        this.hdrActionButtonForLA = hdrActionButtonForLA;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public BigDecimal getAdvisor() {
        return advisor;
    }

    public void setAdvisor(BigDecimal advisor) {
        this.advisor = advisor;
    }

    public BigDecimal getIntroducer() {
        return introducer;
    }

    public void setIntroducer(BigDecimal introducer) {
        this.introducer = introducer;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }

    public String getAdvisorname() {
        return advisorname;
    }

    public void setAdvisorname(String advisorname) {
        this.advisorname = advisorname;
    }

    public String getIntroducername() {
        return introducername;
    }

    public void setIntroducername(String introducername) {
        this.introducername = introducername;
    }

    public Date getClawbackDate() {
        return clawbackDate;
    }

    public void setClawbackDate(Date clawbackDate) {
        this.clawbackDate = clawbackDate;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public BigDecimal getIntroducerInvoiceHeadId() {
        return introducerInvoiceHeadId;
    }

    public void setIntroducerInvoiceHeadId(BigDecimal introducerInvoiceHeadId) {
        this.introducerInvoiceHeadId = introducerInvoiceHeadId;
    }

    public Date getIntroducerInvoiceDate() {
        return introducerInvoiceDate;
    }

    public void setIntroducerInvoiceDate(Date introducerInvoiceDate) {
        this.introducerInvoiceDate = introducerInvoiceDate;
    }

    public BigDecimal getSolicitorInvoiceHeadId() {
        return solicitorInvoiceHeadId;
    }

    public void setSolicitorInvoiceHeadId(BigDecimal solicitorInvoiceHeadId) {
        this.solicitorInvoiceHeadId = solicitorInvoiceHeadId;
    }

    public Date getSolicitorInvoiceDate() {
        return solicitorInvoiceDate;
    }

    public void setSolicitorInvoiceDate(Date solicitorInvoiceDate) {
        this.solicitorInvoiceDate = solicitorInvoiceDate;
    }

    public TblEsignStatus getTblEsignStatus() {
        return tblEsignStatus;
    }

    public void setTblEsignStatus(TblEsignStatus tblEsignStatus) {
        this.tblEsignStatus = tblEsignStatus;
    }

    public String getEditFlag() {
        return editFlag;
    }

    public void setEditFlag(String editFlag) {
        this.editFlag = editFlag;
    }

    public List<HdrCopyLog> getHdrCopyLogs() {
        return hdrCopyLogs;
    }

    public void setHdrCopyLogs(List<HdrCopyLog> hdrCopyLogs) {
        this.hdrCopyLogs = hdrCopyLogs;
    }
}