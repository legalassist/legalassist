package com.laportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the VIEW_ELCLAIM database table.
 * 
 */
@Entity
@Table(name="VIEW_ELCLAIM")
@NamedQuery(name="ViewElclaim.findAll", query="SELECT v FROM ViewElclaim v")
public class ViewElclaim implements Serializable {
	private static final long serialVersionUID = 1L;

	private String accbookcopy;

	private String acccircumstances;

	@Temporal(TemporalType.DATE)
	private Date accdatetime;

	private String acclocation;

	private String additionalclaiminfo;

	private BigDecimal advisor;

	private String agencyname;

	private String ambulancecalled;

	private String anyinvestigation;

	private String anyotherlosses;

	private String anypremedicalconds;

	private String anypreviousacc;

	private String anyriskassessments;

	private String anytreatmentreceived;

	private String anywitnesses;

	private String awaresimilaraccident;

	private String changestosystem;

	private String claimantsufferinjuries;

	@Temporal(TemporalType.DATE)
	@Column(name="CLAWBACK_DATE")
	private Date clawbackDate;

	private String clientattendedhospital;

	private String clientemail;

	private String clientinjuries;

	private String clientmobile;

	private String clientpresentcond;

	private String clientseenaccbook;

	private String clientseengp;

	private String clientstillwork;

	private String companysickpay;

	private String contactno;

	@Temporal(TemporalType.DATE)
	private Date createdate;

	private BigDecimal createuser;

	private String dailyactivities;

	@Temporal(TemporalType.DATE)
	private Date dategpattendance;

	@Temporal(TemporalType.DATE)
	private Date datehospitalattendance;

	@Temporal(TemporalType.DATE)
	private Date dob;

	@Id
	private BigDecimal elclaimcode;

	private String elcode;

	private BigDecimal eltaskcode;

	private String eltaskname;

	private String employeraddress;

	private String employercontactno;

	private String employername;

	private String employmentduration;

	private String esig;

	@Temporal(TemporalType.DATE)
	private Date esigdate;

	private String esignstatus;

	private String estimatednetweeklywage;

	private String gpaddress;

	private String gpadvisegiven;

	private String gpname;

	private String hospitaladdress;

	private String hospitaladvisegiven;

	private String hospitalname;

	private String injuriessymptomsstart;

	private BigDecimal introducer;

	@Column(name="INTRODUCER_COMPANY")
	private String introducerCompany;

	@Column(name="INTRODUCER_USER")
	private String introducerUser;

	@Temporal(TemporalType.DATE)
	@Column(name="INVOICE_DATE")
	private Date invoiceDate;

	@Column(name="IS_INTRODUCER_INVOICED")
	private String isIntroducerInvoiced;

	@Column(name="IS_SOLICITOR_INVOICED")
	private String isSolicitorInvoiced;

	private String jobtitle;

	private String lastupdateuser;

	private String lossofearnings;

	private String ninumber;

	private String noworkadvised;

	private String otherlossesdetail;

	private String periodofabsence;

	private String premedicalconddetail;

	private String previousaccdetail;

	private String protectiveequipment;

	private String remarks;

	private String reportedbywhom;

	@Column(name="SOLICITOR_COMPANY")
	private String solicitorCompany;

	@Column(name="SOLICITOR_COMPANYCODE")
	private String solicitorCompanycode;

	@Column(name="SOLICITOR_USER")
	private String solicitorUser;

	@Column(name="SOLICITOR_USERCODE")
	private BigDecimal solicitorUsercode;

	private String status;

	private BigDecimal statuscode;

	@Temporal(TemporalType.DATE)
	@Column(name="SUBMIT_DATE")
	private Date submitDate;

	private String taskflag;

	private String trainingreceived;

	@Temporal(TemporalType.DATE)
	private Date updatedate;

	private String userofprocequip;

	private String witnessdetails;

	private String title;

	private String firstname;

	private String middlename;

	private String lastname;

	private String postalcode;

	private String address1;

	private String address2;

	private String address3;

	private String city;

	private String region;

	public ViewElclaim() {
	}

	public String getAccbookcopy() {
		return this.accbookcopy;
	}

	public void setAccbookcopy(String accbookcopy) {
		this.accbookcopy = accbookcopy;
	}

	public String getAcccircumstances() {
		return this.acccircumstances;
	}

	public void setAcccircumstances(String acccircumstances) {
		this.acccircumstances = acccircumstances;
	}

	public Date getAccdatetime() {
		return this.accdatetime;
	}

	public void setAccdatetime(Date accdatetime) {
		this.accdatetime = accdatetime;
	}

	public String getAcclocation() {
		return this.acclocation;
	}

	public void setAcclocation(String acclocation) {
		this.acclocation = acclocation;
	}

	public String getAdditionalclaiminfo() {
		return this.additionalclaiminfo;
	}

	public void setAdditionalclaiminfo(String additionalclaiminfo) {
		this.additionalclaiminfo = additionalclaiminfo;
	}

	public BigDecimal getAdvisor() {
		return this.advisor;
	}

	public void setAdvisor(BigDecimal advisor) {
		this.advisor = advisor;
	}

	public String getAgencyname() {
		return this.agencyname;
	}

	public void setAgencyname(String agencyname) {
		this.agencyname = agencyname;
	}

	public String getAmbulancecalled() {
		return this.ambulancecalled;
	}

	public void setAmbulancecalled(String ambulancecalled) {
		this.ambulancecalled = ambulancecalled;
	}

	public String getAnyinvestigation() {
		return this.anyinvestigation;
	}

	public void setAnyinvestigation(String anyinvestigation) {
		this.anyinvestigation = anyinvestigation;
	}

	public String getAnyotherlosses() {
		return this.anyotherlosses;
	}

	public void setAnyotherlosses(String anyotherlosses) {
		this.anyotherlosses = anyotherlosses;
	}

	public String getAnypremedicalconds() {
		return this.anypremedicalconds;
	}

	public void setAnypremedicalconds(String anypremedicalconds) {
		this.anypremedicalconds = anypremedicalconds;
	}

	public String getAnypreviousacc() {
		return this.anypreviousacc;
	}

	public void setAnypreviousacc(String anypreviousacc) {
		this.anypreviousacc = anypreviousacc;
	}

	public String getAnyriskassessments() {
		return this.anyriskassessments;
	}

	public void setAnyriskassessments(String anyriskassessments) {
		this.anyriskassessments = anyriskassessments;
	}

	public String getAnytreatmentreceived() {
		return this.anytreatmentreceived;
	}

	public void setAnytreatmentreceived(String anytreatmentreceived) {
		this.anytreatmentreceived = anytreatmentreceived;
	}

	public String getAnywitnesses() {
		return this.anywitnesses;
	}

	public void setAnywitnesses(String anywitnesses) {
		this.anywitnesses = anywitnesses;
	}

	public String getAwaresimilaraccident() {
		return this.awaresimilaraccident;
	}

	public void setAwaresimilaraccident(String awaresimilaraccident) {
		this.awaresimilaraccident = awaresimilaraccident;
	}

	public String getChangestosystem() {
		return this.changestosystem;
	}

	public void setChangestosystem(String changestosystem) {
		this.changestosystem = changestosystem;
	}

	public String getClaimantsufferinjuries() {
		return this.claimantsufferinjuries;
	}

	public void setClaimantsufferinjuries(String claimantsufferinjuries) {
		this.claimantsufferinjuries = claimantsufferinjuries;
	}

	public Date getClawbackDate() {
		return this.clawbackDate;
	}

	public void setClawbackDate(Date clawbackDate) {
		this.clawbackDate = clawbackDate;
	}

	public String getClientattendedhospital() {
		return this.clientattendedhospital;
	}

	public void setClientattendedhospital(String clientattendedhospital) {
		this.clientattendedhospital = clientattendedhospital;
	}

	public String getClientemail() {
		return this.clientemail;
	}

	public void setClientemail(String clientemail) {
		this.clientemail = clientemail;
	}

	public String getClientinjuries() {
		return this.clientinjuries;
	}

	public void setClientinjuries(String clientinjuries) {
		this.clientinjuries = clientinjuries;
	}

	public String getClientmobile() {
		return this.clientmobile;
	}

	public void setClientmobile(String clientmobile) {
		this.clientmobile = clientmobile;
	}


	public String getClientpresentcond() {
		return this.clientpresentcond;
	}

	public void setClientpresentcond(String clientpresentcond) {
		this.clientpresentcond = clientpresentcond;
	}

	public String getClientseenaccbook() {
		return this.clientseenaccbook;
	}

	public void setClientseenaccbook(String clientseenaccbook) {
		this.clientseenaccbook = clientseenaccbook;
	}

	public String getClientseengp() {
		return this.clientseengp;
	}

	public void setClientseengp(String clientseengp) {
		this.clientseengp = clientseengp;
	}

	public String getClientstillwork() {
		return this.clientstillwork;
	}

	public void setClientstillwork(String clientstillwork) {
		this.clientstillwork = clientstillwork;
	}

	public String getCompanysickpay() {
		return this.companysickpay;
	}

	public void setCompanysickpay(String companysickpay) {
		this.companysickpay = companysickpay;
	}

	public String getContactno() {
		return this.contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	public Date getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public BigDecimal getCreateuser() {
		return this.createuser;
	}

	public void setCreateuser(BigDecimal createuser) {
		this.createuser = createuser;
	}

	public String getDailyactivities() {
		return this.dailyactivities;
	}

	public void setDailyactivities(String dailyactivities) {
		this.dailyactivities = dailyactivities;
	}

	public Date getDategpattendance() {
		return this.dategpattendance;
	}

	public void setDategpattendance(Date dategpattendance) {
		this.dategpattendance = dategpattendance;
	}

	public Date getDatehospitalattendance() {
		return this.datehospitalattendance;
	}

	public void setDatehospitalattendance(Date datehospitalattendance) {
		this.datehospitalattendance = datehospitalattendance;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public BigDecimal getElclaimcode() {
		return this.elclaimcode;
	}

	public void setElclaimcode(BigDecimal elclaimcode) {
		this.elclaimcode = elclaimcode;
	}

	public String getElcode() {
		return this.elcode;
	}

	public void setElcode(String elcode) {
		this.elcode = elcode;
	}

	public BigDecimal getEltaskcode() {
		return this.eltaskcode;
	}

	public void setEltaskcode(BigDecimal eltaskcode) {
		this.eltaskcode = eltaskcode;
	}

	public String getEltaskname() {
		return this.eltaskname;
	}

	public void setEltaskname(String eltaskname) {
		this.eltaskname = eltaskname;
	}

	public String getEmployeraddress() {
		return this.employeraddress;
	}

	public void setEmployeraddress(String employeraddress) {
		this.employeraddress = employeraddress;
	}

	public String getEmployercontactno() {
		return this.employercontactno;
	}

	public void setEmployercontactno(String employercontactno) {
		this.employercontactno = employercontactno;
	}

	public String getEmployername() {
		return this.employername;
	}

	public void setEmployername(String employername) {
		this.employername = employername;
	}

	public String getEmploymentduration() {
		return this.employmentduration;
	}

	public void setEmploymentduration(String employmentduration) {
		this.employmentduration = employmentduration;
	}

	public String getEsig() {
		return this.esig;
	}

	public void setEsig(String esig) {
		this.esig = esig;
	}

	public Date getEsigdate() {
		return this.esigdate;
	}

	public void setEsigdate(Date esigdate) {
		this.esigdate = esigdate;
	}

	public String getEsignstatus() {
		return this.esignstatus;
	}

	public void setEsignstatus(String esignstatus) {
		this.esignstatus = esignstatus;
	}

	public String getEstimatednetweeklywage() {
		return this.estimatednetweeklywage;
	}

	public void setEstimatednetweeklywage(String estimatednetweeklywage) {
		this.estimatednetweeklywage = estimatednetweeklywage;
	}

	public String getGpaddress() {
		return this.gpaddress;
	}

	public void setGpaddress(String gpaddress) {
		this.gpaddress = gpaddress;
	}

	public String getGpadvisegiven() {
		return this.gpadvisegiven;
	}

	public void setGpadvisegiven(String gpadvisegiven) {
		this.gpadvisegiven = gpadvisegiven;
	}

	public String getGpname() {
		return this.gpname;
	}

	public void setGpname(String gpname) {
		this.gpname = gpname;
	}

	public String getHospitaladdress() {
		return this.hospitaladdress;
	}

	public void setHospitaladdress(String hospitaladdress) {
		this.hospitaladdress = hospitaladdress;
	}

	public String getHospitaladvisegiven() {
		return this.hospitaladvisegiven;
	}

	public void setHospitaladvisegiven(String hospitaladvisegiven) {
		this.hospitaladvisegiven = hospitaladvisegiven;
	}

	public String getHospitalname() {
		return this.hospitalname;
	}

	public void setHospitalname(String hospitalname) {
		this.hospitalname = hospitalname;
	}

	public String getInjuriessymptomsstart() {
		return this.injuriessymptomsstart;
	}

	public void setInjuriessymptomsstart(String injuriessymptomsstart) {
		this.injuriessymptomsstart = injuriessymptomsstart;
	}

	public BigDecimal getIntroducer() {
		return this.introducer;
	}

	public void setIntroducer(BigDecimal introducer) {
		this.introducer = introducer;
	}

	public String getIntroducerCompany() {
		return this.introducerCompany;
	}

	public void setIntroducerCompany(String introducerCompany) {
		this.introducerCompany = introducerCompany;
	}

	public String getIntroducerUser() {
		return this.introducerUser;
	}

	public void setIntroducerUser(String introducerUser) {
		this.introducerUser = introducerUser;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getIsIntroducerInvoiced() {
		return this.isIntroducerInvoiced;
	}

	public void setIsIntroducerInvoiced(String isIntroducerInvoiced) {
		this.isIntroducerInvoiced = isIntroducerInvoiced;
	}

	public String getIsSolicitorInvoiced() {
		return this.isSolicitorInvoiced;
	}

	public void setIsSolicitorInvoiced(String isSolicitorInvoiced) {
		this.isSolicitorInvoiced = isSolicitorInvoiced;
	}

	public String getJobtitle() {
		return this.jobtitle;
	}

	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}

	public String getLastupdateuser() {
		return this.lastupdateuser;
	}

	public void setLastupdateuser(String lastupdateuser) {
		this.lastupdateuser = lastupdateuser;
	}

	public String getLossofearnings() {
		return this.lossofearnings;
	}

	public void setLossofearnings(String lossofearnings) {
		this.lossofearnings = lossofearnings;
	}

	public String getNinumber() {
		return this.ninumber;
	}

	public void setNinumber(String ninumber) {
		this.ninumber = ninumber;
	}

	public String getNoworkadvised() {
		return this.noworkadvised;
	}

	public void setNoworkadvised(String noworkadvised) {
		this.noworkadvised = noworkadvised;
	}

	public String getOtherlossesdetail() {
		return this.otherlossesdetail;
	}

	public void setOtherlossesdetail(String otherlossesdetail) {
		this.otherlossesdetail = otherlossesdetail;
	}

	public String getPeriodofabsence() {
		return this.periodofabsence;
	}

	public void setPeriodofabsence(String periodofabsence) {
		this.periodofabsence = periodofabsence;
	}

	public String getPremedicalconddetail() {
		return this.premedicalconddetail;
	}

	public void setPremedicalconddetail(String premedicalconddetail) {
		this.premedicalconddetail = premedicalconddetail;
	}

	public String getPreviousaccdetail() {
		return this.previousaccdetail;
	}

	public void setPreviousaccdetail(String previousaccdetail) {
		this.previousaccdetail = previousaccdetail;
	}

	public String getProtectiveequipment() {
		return this.protectiveequipment;
	}

	public void setProtectiveequipment(String protectiveequipment) {
		this.protectiveequipment = protectiveequipment;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getReportedbywhom() {
		return this.reportedbywhom;
	}

	public void setReportedbywhom(String reportedbywhom) {
		this.reportedbywhom = reportedbywhom;
	}

	public String getSolicitorCompany() {
		return this.solicitorCompany;
	}

	public void setSolicitorCompany(String solicitorCompany) {
		this.solicitorCompany = solicitorCompany;
	}

	public String getSolicitorCompanycode() {
		return this.solicitorCompanycode;
	}

	public void setSolicitorCompanycode(String solicitorCompanycode) {
		this.solicitorCompanycode = solicitorCompanycode;
	}

	public String getSolicitorUser() {
		return this.solicitorUser;
	}

	public void setSolicitorUser(String solicitorUser) {
		this.solicitorUser = solicitorUser;
	}

	public BigDecimal getSolicitorUsercode() {
		return this.solicitorUsercode;
	}

	public void setSolicitorUsercode(BigDecimal solicitorUsercode) {
		this.solicitorUsercode = solicitorUsercode;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getStatuscode() {
		return this.statuscode;
	}

	public void setStatuscode(BigDecimal statuscode) {
		this.statuscode = statuscode;
	}

	public Date getSubmitDate() {
		return this.submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public String getTaskflag() {
		return this.taskflag;
	}

	public void setTaskflag(String taskflag) {
		this.taskflag = taskflag;
	}

	public String getTrainingreceived() {
		return this.trainingreceived;
	}

	public void setTrainingreceived(String trainingreceived) {
		this.trainingreceived = trainingreceived;
	}

	public Date getUpdatedate() {
		return this.updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public String getUserofprocequip() {
		return this.userofprocequip;
	}

	public void setUserofprocequip(String userofprocequip) {
		this.userofprocequip = userofprocequip;
	}

	public String getWitnessdetails() {
		return this.witnessdetails;
	}

	public void setWitnessdetails(String witnessdetails) {
		this.witnessdetails = witnessdetails;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
}