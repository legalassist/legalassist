package com.laportal.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_BROADCAST database table.
 */
@Entity
@Table(name = "TBL_BROADCAST")
@NamedQuery(name = "TblBroadcast.findAll", query = "SELECT t FROM TblBroadcast t")
public class TblBroadcast implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_BROADCAST_BROADCASTPK_GENERATOR", sequenceName = "TBL_BROADCAST_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_BROADCAST_BROADCASTPK_GENERATOR")
    @Column(name = "BROADCAST_PK")
    private long broadcastPk;

    @Temporal(TemporalType.DATE)
    private Date createdate;

    private BigDecimal createuser;

    @Lob
    @Column(name = "MESSAGE")
    private String message;

    private String status;

    @Lob
    @Column(name = "TO_USERS")
    private String toUsers;

    @Column(name = "SECHDULE_DATE")
    private String sechduleDate;

    @Column(name = "SECHDULE_TIME")
    private String sechduleTime;

    private String now;

    private String subject;

    public TblBroadcast() {
    }

    public long getBroadcastPk() {
        return this.broadcastPk;
    }

    public void setBroadcastPk(long broadcastPk) {
        this.broadcastPk = broadcastPk;
    }

    public Date getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public BigDecimal getCreateuser() {
        return this.createuser;
    }

    public void setCreateuser(BigDecimal createuser) {
        this.createuser = createuser;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToUsers() {
        return this.toUsers;
    }

    public void setToUsers(String toUsers) {
        this.toUsers = toUsers;
    }

    public String getSechduleDate() {
        return sechduleDate;
    }

    public void setSechduleDate(String sechduleDate) {
        this.sechduleDate = sechduleDate;
    }

    public String getSechduleTime() {
        return sechduleTime;
    }

    public void setSechduleTime(String sechduleTime) {
        this.sechduleTime = sechduleTime;
    }

    public String getNow() {
        return now;
    }

    public void setNow(String now) {
        this.now = now;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}