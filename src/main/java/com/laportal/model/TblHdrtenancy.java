package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TBL_HDRTENANCY database table.
 */
@Entity
@Table(name = "TBL_HDRTENANCY")
@NamedQuery(name = "TblHdrtenancy.findAll", query = "SELECT t FROM TblHdrtenancy t")
public class TblHdrtenancy implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_HDRTENANCY_HDRTENANCYCODE_GENERATOR", sequenceName = "SEQ_TBL_HDRTENANCY", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_HDRTENANCY_HDRTENANCYCODE_GENERATOR")
    private long hdrtenancycode;

    @Column(name = "ARREARS_AMOUNT")
    private BigDecimal arrearsAmount;

    @Column(name = "BENEFITS_DETAIL")
    private String benefitsDetail;

    @Column(name = "CONTRIBUTE_PM")
    private BigDecimal contributePm;

    @Column(name = "CONTRIBUTE_PW")
    private BigDecimal contributePw;

    @Column(name = "IN_ARREARS")
    private String inArrears;

    @Column(name = "IN_PAYMENT_PLAN")
    private String inPaymentPlan;

    @Column(name = "NO_OF_OCCUPANTS")
    private BigDecimal noOfOccupants;

    @Column(name = "PAYMENT_PLAN_PM")
    private BigDecimal paymentPlanPm;

    @Column(name = "PAYMENT_PLAN_PW")
    private BigDecimal paymentPlanPw;

    @Column(name = "PROPERTY_TYPE")
    private String propertyType;

    @Column(name = "RECEIVING_BENEFITS")
    private String receivingBenefits;

    @Column(name = "RENT_CONTRIBUTE")
    private String rentContribute;

    @Column(name = "RENT_PM")
    private BigDecimal rentPm;

    @Column(name = "RENT_PW")
    private BigDecimal rentPw;

    @Temporal(TemporalType.DATE)
    @Column(name = "START_DATE")
    private Date startDate;

    //bi-directional many-to-one association to TblHdrclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "HDR_CLAIMCODE")
    private TblHdrclaim tblHdrclaim;

    @Column(name = "LASTUPDATEUSER")
    private String lastupdateuser;

    public TblHdrtenancy() {
    }

    public long getHdrtenancycode() {
        return this.hdrtenancycode;
    }

    public void setHdrtenancycode(long hdrtenancycode) {
        this.hdrtenancycode = hdrtenancycode;
    }

    public BigDecimal getArrearsAmount() {
        return this.arrearsAmount;
    }

    public void setArrearsAmount(BigDecimal arrearsAmount) {
        this.arrearsAmount = arrearsAmount;
    }

    public String getBenefitsDetail() {
        return this.benefitsDetail;
    }

    public void setBenefitsDetail(String benefitsDetail) {
        this.benefitsDetail = benefitsDetail;
    }

    public BigDecimal getContributePm() {
        return this.contributePm;
    }

    public void setContributePm(BigDecimal contributePm) {
        this.contributePm = contributePm;
    }

    public BigDecimal getContributePw() {
        return this.contributePw;
    }

    public void setContributePw(BigDecimal contributePw) {
        this.contributePw = contributePw;
    }

    public String getInArrears() {
        return this.inArrears;
    }

    public void setInArrears(String inArrears) {
        this.inArrears = inArrears;
    }

    public String getInPaymentPlan() {
        return this.inPaymentPlan;
    }

    public void setInPaymentPlan(String inPaymentPlan) {
        this.inPaymentPlan = inPaymentPlan;
    }

    public BigDecimal getNoOfOccupants() {
        return this.noOfOccupants;
    }

    public void setNoOfOccupants(BigDecimal noOfOccupants) {
        this.noOfOccupants = noOfOccupants;
    }

    public BigDecimal getPaymentPlanPm() {
        return this.paymentPlanPm;
    }

    public void setPaymentPlanPm(BigDecimal paymentPlanPm) {
        this.paymentPlanPm = paymentPlanPm;
    }

    public BigDecimal getPaymentPlanPw() {
        return this.paymentPlanPw;
    }

    public void setPaymentPlanPw(BigDecimal paymentPlanPw) {
        this.paymentPlanPw = paymentPlanPw;
    }

    public String getPropertyType() {
        return this.propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getReceivingBenefits() {
        return this.receivingBenefits;
    }

    public void setReceivingBenefits(String receivingBenefits) {
        this.receivingBenefits = receivingBenefits;
    }

    public String getRentContribute() {
        return this.rentContribute;
    }

    public void setRentContribute(String rentContribute) {
        this.rentContribute = rentContribute;
    }

    public BigDecimal getRentPm() {
        return this.rentPm;
    }

    public void setRentPm(BigDecimal rentPm) {
        this.rentPm = rentPm;
    }

    public BigDecimal getRentPw() {
        return this.rentPw;
    }

    public void setRentPw(BigDecimal rentPw) {
        this.rentPw = rentPw;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public TblHdrclaim getTblHdrclaim() {
        return this.tblHdrclaim;
    }

    public void setTblHdrclaim(TblHdrclaim tblHdrclaim) {
        this.tblHdrclaim = tblHdrclaim;
    }

    public String getLastupdateuser() {
        return lastupdateuser;
    }

    public void setLastupdateuser(String lastupdateuser) {
        this.lastupdateuser = lastupdateuser;
    }
}