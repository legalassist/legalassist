package com.laportal.model;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The persistent class for the VIEW_INVOICE_DETAIL database table.
 */

public class ViewInvoiceDetail implements Serializable {

    private BigDecimal adjust;

    private BigDecimal casecode;

    private BigDecimal charge;

    private String description;

    private String invType;

    private BigDecimal invoiceheadid;

    private Long invoicedetailid;

    private String invoicegeneration;

    private String campaigncode;

    public ViewInvoiceDetail() {
    }

    public Long getInvoicedetailid() {
        return invoicedetailid;
    }

    public void setInvoicedetailid(Long invoicedetailid) {
        this.invoicedetailid = invoicedetailid;
    }

    public String getInvoicegeneration() {
        return invoicegeneration;
    }

    public void setInvoicegeneration(String invoicegeneration) {
        this.invoicegeneration = invoicegeneration;
    }

    public BigDecimal getAdjust() {
        return this.adjust;
    }

    public void setAdjust(BigDecimal adjust) {
        this.adjust = adjust;
    }

    public BigDecimal getCasecode() {
        return this.casecode;
    }

    public void setCasecode(BigDecimal casecode) {
        this.casecode = casecode;
    }

    public BigDecimal getCharge() {
        return this.charge;
    }

    public void setCharge(BigDecimal charge) {
        this.charge = charge;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInvType() {
        return this.invType;
    }

    public void setInvType(String invType) {
        this.invType = invType;
    }

    public BigDecimal getInvoiceheadid() {
        return this.invoiceheadid;
    }

    public void setInvoiceheadid(BigDecimal invoiceheadid) {
        this.invoiceheadid = invoiceheadid;
    }

    public String getCampaigncode() {
        return campaigncode;
    }

    public void setCampaigncode(String campaigncode) {
        this.campaigncode = campaigncode;
    }
}