package com.laportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the TBL_TENANCYSOLICITOR database table.
 */
@Entity
@Table(name = "TBL_TENANCYSOLICITOR")
@NamedQuery(name = "TblTenancysolicitor.findAll", query = "SELECT t FROM TblTenancysolicitor t")
public class TblTenancysolicitor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "TBL_TENANCYSOLICITOR_TENANCYSOLCODE_GENERATOR", sequenceName = "SEQ_TBL_TENANCYSOLICITOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_TENANCYSOLICITOR_TENANCYSOLCODE_GENERATOR")
    private long tenancysolcode;

    private String companycode;

    @Temporal(TemporalType.DATE)
    private Date createdon;

    private String reference1;

    private String reference2;

    private String remarks;

    private String status;

    private String usercode;

    //bi-directional many-to-one association to TblTenancyclaim
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "TENANCYCLAIMCODE")
    private TblTenancyclaim tblTenancyclaim;

    @Transient
    private TblCompanyprofile tblCompanyprofile;

    public TblTenancysolicitor() {
    }

    public long getTenancysolcode() {
        return this.tenancysolcode;
    }

    public void setTenancysolcode(long tenancysolcode) {
        this.tenancysolcode = tenancysolcode;
    }

    public String getCompanycode() {
        return this.companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public Date getCreatedon() {
        return this.createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getReference1() {
        return this.reference1;
    }

    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getReference2() {
        return this.reference2;
    }

    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsercode() {
        return this.usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public TblTenancyclaim getTblTenancyclaim() {
        return this.tblTenancyclaim;
    }

    public void setTblTenancyclaim(TblTenancyclaim tblTenancyclaim) {
        this.tblTenancyclaim = tblTenancyclaim;
    }

    public TblCompanyprofile getTblCompanyprofile() {
        return tblCompanyprofile;
    }

    public void setTblCompanyprofile(TblCompanyprofile tblCompanyprofile) {
        this.tblCompanyprofile = tblCompanyprofile;
    }
}