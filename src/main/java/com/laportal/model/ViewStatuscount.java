package com.laportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the VIEW_STATUSCOUNT database table.
 * 
 */
@Entity
@Table(name="VIEW_STATUSCOUNT")
@NamedQuery(name="ViewStatuscount.findAll", query="SELECT v FROM ViewStatuscount v")
public class ViewStatuscount implements Serializable {
	private static final long serialVersionUID = 1L;

	private String compaigncode;

	private String descr;

	private BigDecimal introducer;

	private BigDecimal seq;

	@Id
	private BigDecimal statuscode;

	private BigDecimal statuscount;

	public ViewStatuscount() {
	}

	public String getCompaigncode() {
		return this.compaigncode;
	}

	public void setCompaigncode(String compaigncode) {
		this.compaigncode = compaigncode;
	}

	public String getDescr() {
		return this.descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public BigDecimal getIntroducer() {
		return this.introducer;
	}

	public void setIntroducer(BigDecimal introducer) {
		this.introducer = introducer;
	}

	public BigDecimal getSeq() {
		return this.seq;
	}

	public void setSeq(BigDecimal seq) {
		this.seq = seq;
	}

	public BigDecimal getStatuscode() {
		return this.statuscode;
	}

	public void setStatuscode(BigDecimal statuscode) {
		this.statuscode = statuscode;
	}

	public BigDecimal getStatuscount() {
		return this.statuscount;
	}

	public void setStatuscount(BigDecimal statuscount) {
		this.statuscount = statuscount;
	}

}