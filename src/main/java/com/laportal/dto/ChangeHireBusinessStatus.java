package com.laportal.dto;

public class ChangeHireBusinessStatus {

    private long hireclaimcode;
    private String companyCode;
    private String note;
    private String userCode;
    private String status;
    private String bussinessStatus;
    private HireBusinessDetailRequest hireBusinessDetailRequest;

    public long getHireclaimcode() {
        return hireclaimcode;
    }

    public void setHireclaimcode(long hireclaimcode) {
        this.hireclaimcode = hireclaimcode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBussinessStatus() {
        return bussinessStatus;
    }

    public void setBussinessStatus(String bussinessStatus) {
        this.bussinessStatus = bussinessStatus;
    }

    public HireBusinessDetailRequest getHireBusinessDetailRequest() {
        return hireBusinessDetailRequest;
    }

    public void setHireBusinessDetailRequest(HireBusinessDetailRequest hireBusinessDetailRequest) {
        this.hireBusinessDetailRequest = hireBusinessDetailRequest;
    }
}
