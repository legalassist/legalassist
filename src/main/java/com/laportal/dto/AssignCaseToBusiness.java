package com.laportal.dto;

public class AssignCaseToBusiness {

    private String message;
    private String note;
    private String companyprofilecode;
    private String hireclaimcode;
    private String userCode;
    private String statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCompanyprofilecode() {
        return companyprofilecode;
    }

    public void setCompanyprofilecode(String companyprofilecode) {
        this.companyprofilecode = companyprofilecode;
    }

    public String getHireclaimcode() {
        return hireclaimcode;
    }

    public void setHireclaimcode(String hireclaimcode) {
        this.hireclaimcode = hireclaimcode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }


    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
