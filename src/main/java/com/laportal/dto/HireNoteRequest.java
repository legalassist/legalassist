package com.laportal.dto;

public class HireNoteRequest {

    private String hireCode;

    private String note;

    private String userCatCode;

    public String getHireCode() {
        return hireCode;
    }

    public void setHireCode(String hireCode) {
        this.hireCode = hireCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserCatCode() {
        return userCatCode;
    }

    public void setUserCatCode(String userCatCode) {
        this.userCatCode = userCatCode;
    }
}
