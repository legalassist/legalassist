package com.laportal.dto;

public class ElNoteRequest {

    private String elCode;

    private String note;

    private String userCatCode;

    public String getElCode() {
        return elCode;
    }

    public void setElCode(String elCode) {
        this.elCode = elCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserCatCode() {
        return userCatCode;
    }

    public void setUserCatCode(String userCatCode) {
        this.userCatCode = userCatCode;
    }
}
