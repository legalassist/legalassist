package com.laportal.dto;

import com.laportal.model.TblRtastatus;

public class RtaActionButton {
    private String apiflag;
    private String buttonname;

    private String buttonvalue;
    private String rejectDialog;
    private TblRtastatus tblRtastatus;

    public String getApiflag() {
        return apiflag;
    }

    public void setApiflag(String apiflag) {
        this.apiflag = apiflag;
    }

    public String getButtonname() {
        return buttonname;
    }

    public void setButtonname(String buttonname) {
        this.buttonname = buttonname;
    }

    public String getButtonvalue() {
        return buttonvalue;
    }

    public void setButtonvalue(String buttonvalue) {
        this.buttonvalue = buttonvalue;
    }

    public TblRtastatus getTblRtastatus() {
        return tblRtastatus;
    }

    public void setTblRtastatus(TblRtastatus tblRtastatus) {
        this.tblRtastatus = tblRtastatus;
    }

    public String getRejectDialog() {
        return rejectDialog;
    }

    public void setRejectDialog(String rejectDialog) {
        this.rejectDialog = rejectDialog;
    }
}
