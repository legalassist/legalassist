package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

public class AddHireDocument {

    private long hireCode;

    private List<HireFileUploadRequest> files = new ArrayList<>();

    public long getHireCode() {
        return hireCode;
    }

    public void setHireCode(long hireCode) {
        this.hireCode = hireCode;
    }

    public List<HireFileUploadRequest> getFiles() {
        return files;
    }

    public void setFiles(List<HireFileUploadRequest> files) {
        this.files = files;
    }

}
