package com.laportal.dto;

import com.laportal.model.TblCompanyprofile;
import com.laportal.model.TblUser;

import java.util.ArrayList;
import java.util.List;

public class LoginResponse {

    private boolean isLogin = false;

    private String message;

    private List<Menu> _nav = new ArrayList<>();

    private String token;

    private boolean isDirectIntroducer = false;

    private TblCompanyprofile tblCompanyprofile = new TblCompanyprofile();

    private String idleLogoutTimeMM;

    private String pwdUpdateFlag;

    private DashboardResponse dashboardResponse = new DashboardResponse();

    private TblUser tblUser;

    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Menu> get_nav() {
        return _nav;
    }

    public void set_nav(List<Menu> _nav) {
        this._nav = _nav;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isDirectIntroducer() {
        return isDirectIntroducer;
    }

    public void setDirectIntroducer(boolean directIntroducer) {
        isDirectIntroducer = directIntroducer;
    }

    public TblCompanyprofile getTblCompanyprofile() {
        return tblCompanyprofile;
    }

    public void setTblCompanyprofile(TblCompanyprofile tblCompanyprofile) {
        this.tblCompanyprofile = tblCompanyprofile;
    }

    public String getIdleLogoutTimeMM() {
        return idleLogoutTimeMM;
    }

    public void setIdleLogoutTimeMM(String idleLogoutTimeMM) {
        this.idleLogoutTimeMM = idleLogoutTimeMM;
    }

    public String getPwdUpdateFlag() {
        return pwdUpdateFlag;
    }

    public void setPwdUpdateFlag(String pwdUpdateFlag) {
        this.pwdUpdateFlag = pwdUpdateFlag;
    }

    public DashboardResponse getDashboardResponse() {
        return dashboardResponse;
    }

    public void setDashboardResponse(DashboardResponse dashboardResponse) {
        this.dashboardResponse = dashboardResponse;
    }

    public TblUser getTblUser() {
        return tblUser;
    }

    public void setTblUser(TblUser tblUser) {
        this.tblUser = tblUser;
    }
}
