package com.laportal.dto;

public class PerformActionOnMnRequest {


    private String mncode;
    private String toStatus;
    private String reason;

    public String getMncode() {
        return mncode;
    }

    public void setMncode(String mncode) {
        this.mncode = mncode;
    }

    public String getToStatus() {
        return toStatus;
    }

    public void setToStatus(String toStatus) {
        this.toStatus = toStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
