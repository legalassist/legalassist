package com.laportal.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.laportal.model.TblRtastatus;
import com.laportal.model.ViewInvoiceDetail;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public class InvoiceDetail {

    private long invoiceheadid;

    private String invoiceToName;

    private String invoiceToPostcode;

    private String invoiceToAddress1;

    private String invoiceToAddress2;

    private String invoiceToCity;

    private String invoiceFromName;

    private String invoiceFromPostcode;

    private String invoiceFromAddress1;

    private String invoiceFromAddress2;

    private String invoiceFromCity;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "UTC")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date invoiceDate;

    private String invoiceNumber;

    private long totalInstructions;

    private String issuedBy;

    private String paymentMethod;

    private String solicitorCode;

    private List<ViewInvoiceDetail> tblSolicitorInvoiceDetail;

    private List<TblRtastatus> tblRtastatuses;

    private long vatAmount;
    private long amount;

    private String compaigntype;

    private String compaingcode;

    private String status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
    private Date duedate;

    private String accountName;

    private String accountNumber;

    private String sortcode;

    private String varegno;


    public String getInvoiceToName() {
        return invoiceToName;
    }

    public void setInvoiceToName(String invoiceToName) {
        this.invoiceToName = invoiceToName;
    }

    public String getInvoiceToPostcode() {
        return invoiceToPostcode;
    }

    public void setInvoiceToPostcode(String invoiceToPostcode) {
        this.invoiceToPostcode = invoiceToPostcode;
    }

    public String getInvoiceToAddress1() {
        return invoiceToAddress1;
    }

    public void setInvoiceToAddress1(String invoiceToAddress1) {
        this.invoiceToAddress1 = invoiceToAddress1;
    }

    public String getInvoiceToAddress2() {
        return invoiceToAddress2;
    }

    public void setInvoiceToAddress2(String invoiceToAddress2) {
        this.invoiceToAddress2 = invoiceToAddress2;
    }

    public String getInvoiceToCity() {
        return invoiceToCity;
    }

    public void setInvoiceToCity(String invoiceToCity) {
        this.invoiceToCity = invoiceToCity;
    }

    public String getInvoiceFromName() {
        return invoiceFromName;
    }

    public void setInvoiceFromName(String invoiceFromName) {
        this.invoiceFromName = invoiceFromName;
    }

    public String getInvoiceFromPostcode() {
        return invoiceFromPostcode;
    }

    public void setInvoiceFromPostcode(String invoiceFromPostcode) {
        this.invoiceFromPostcode = invoiceFromPostcode;
    }

    public String getInvoiceFromAddress1() {
        return invoiceFromAddress1;
    }

    public void setInvoiceFromAddress1(String invoiceFromAddress1) {
        this.invoiceFromAddress1 = invoiceFromAddress1;
    }

    public String getInvoiceFromAddress2() {
        return invoiceFromAddress2;
    }

    public void setInvoiceFromAddress2(String invoiceFromAddress2) {
        this.invoiceFromAddress2 = invoiceFromAddress2;
    }

    public String getInvoiceFromCity() {
        return invoiceFromCity;
    }

    public void setInvoiceFromCity(String invoiceFromCity) {
        this.invoiceFromCity = invoiceFromCity;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public long getTotalInstructions() {
        return totalInstructions;
    }

    public void setTotalInstructions(long totalInstructions) {
        this.totalInstructions = totalInstructions;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public List<ViewInvoiceDetail> getTblSolicitorInvoiceDetail() {
        return tblSolicitorInvoiceDetail;
    }

    public void setTblSolicitorInvoiceDetail(List<ViewInvoiceDetail> tblSolicitorInvoiceDetail) {
        this.tblSolicitorInvoiceDetail = tblSolicitorInvoiceDetail;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public long getInvoiceheadid() {
        return invoiceheadid;
    }

    public void setInvoiceheadid(long invoiceheadid) {
        this.invoiceheadid = invoiceheadid;
    }

    public List<TblRtastatus> getTblRtastatuses() {
        return tblRtastatuses;
    }

    public void setTblRtastatuses(List<TblRtastatus> tblRtastatuses) {
        this.tblRtastatuses = tblRtastatuses;
    }

    public long getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(long vatAmount) {
        this.vatAmount = vatAmount;
    }

    public String getCompaigntype() {
        return compaigntype;
    }

    public void setCompaigntype(String compaigntype) {
        this.compaigntype = compaigntype;
    }

    public String getCompaingcode() {
        return compaingcode;
    }

    public void setCompaingcode(String compaingcode) {
        this.compaingcode = compaingcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDuedate() {
        return duedate;
    }

    public void setDuedate(Date duedate) {
        this.duedate = duedate;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getSortcode() {
        return sortcode;
    }

    public void setSortcode(String sortcode) {
        this.sortcode = sortcode;
    }

    public String getVaregno() {
        return varegno;
    }

    public void setVaregno(String varegno) {
        this.varegno = varegno;
    }
}
