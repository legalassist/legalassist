package com.laportal.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class SaveTblHdraffectedper {

    private String evidenceDetail;

    private String medicalEvidence;

    private Date persondob;

    private String personname;

    private String sufferingFrom;

    public String getEvidenceDetail() {
        return evidenceDetail;
    }

    public void setEvidenceDetail(String evidenceDetail) {
        this.evidenceDetail = evidenceDetail;
    }

    public String getMedicalEvidence() {
        return medicalEvidence;
    }

    public void setMedicalEvidence(String medicalEvidence) {
        this.medicalEvidence = medicalEvidence;
    }

    public Date getPersondob() {
        return persondob;
    }

    public void setPersondob(Date persondob) {
        this.persondob = persondob;
    }

    public String getPersonname() {
        return personname;
    }

    public void setPersonname(String personname) {
        this.personname = personname;
    }

    public String getSufferingFrom() {
        return sufferingFrom;
    }

    public void setSufferingFrom(String sufferingFrom) {
        this.sufferingFrom = sufferingFrom;
    }
}