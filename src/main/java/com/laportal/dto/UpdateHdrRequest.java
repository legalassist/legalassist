package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

public class UpdateHdrRequest {

    private UpdateTblHdrclaim updateTblHdrclaim;

    private UpdateTblHdrclaimant updateTblHdrclaimant;

    private UpdateTblHdrjointtenancy updateTblHdrJointTenancy;

    private UpdateTblHdrtenancy updateTblHdrtenancy;

    private List<UpdateTblHdraffectedper> updateTblHdraffectedperList = new ArrayList<UpdateTblHdraffectedper>();

    private List<UpdateTblHdraffectedroom> updateTblHdraffectedroomList = new ArrayList<UpdateTblHdraffectedroom>();


    public UpdateTblHdrclaim getUpdateTblHdrclaim() {
        return updateTblHdrclaim;
    }

    public void setUpdateTblHdrclaim(UpdateTblHdrclaim updateTblHdrclaim) {
        this.updateTblHdrclaim = updateTblHdrclaim;
    }

    public UpdateTblHdrclaimant getUpdateTblHdrclaimant() {
        return updateTblHdrclaimant;
    }

    public void setUpdateTblHdrclaimant(UpdateTblHdrclaimant updateTblHdrclaimant) {
        this.updateTblHdrclaimant = updateTblHdrclaimant;
    }

    public UpdateTblHdrjointtenancy getUpdateTblHdrJointTenancy() {
        return updateTblHdrJointTenancy;
    }

    public void setUpdateTblHdrJointTenancy(UpdateTblHdrjointtenancy updateTblHdrJointTenancy) {
        this.updateTblHdrJointTenancy = updateTblHdrJointTenancy;
    }

    public UpdateTblHdrtenancy getUpdateTblHdrtenancy() {
        return updateTblHdrtenancy;
    }

    public void setUpdateTblHdrtenancy(UpdateTblHdrtenancy updateTblHdrtenancy) {
        this.updateTblHdrtenancy = updateTblHdrtenancy;
    }

    public List<UpdateTblHdraffectedper> getUpdateTblHdraffectedperList() {
        return updateTblHdraffectedperList;
    }

    public void setUpdateTblHdraffectedperList(List<UpdateTblHdraffectedper> updateTblHdraffectedperList) {
        this.updateTblHdraffectedperList = updateTblHdraffectedperList;
    }

    public List<UpdateTblHdraffectedroom> getUpdateTblHdraffectedroomList() {
        return updateTblHdraffectedroomList;
    }

    public void setUpdateTblHdraffectedroomList(List<UpdateTblHdraffectedroom> updateTblHdraffectedroomList) {
        this.updateTblHdraffectedroomList = updateTblHdraffectedroomList;
    }
}
