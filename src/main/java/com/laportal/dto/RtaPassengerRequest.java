package com.laportal.dto;

import java.util.List;

public class RtaPassengerRequest {

    private String address1;

    private String address2;

    private String address3;

    private String city;

    private String dob;

    private String driverpassenger;

    private String email;

    private String englishlevel;

    private String firstname;

    private String gaddress1;

    private String gaddress2;

    private String gaddress3;

    private String gcity;

    private String gemail;

    private String gfirstname;

    private String glandline;

    private String glastname;

    private String gmiddlename;

    private String gmobile;

    private String gpostalcode;

    private String gregion;

    private String gtitle;

    private List<String> injclasscodes;

    private String injdescription;

    private String injlength;

    private String landline;

    private String lastname;

    private String medicalinfo;

    private String middlename;

    private String mobile;

    private String ninumber;

    private String ongoing;

    private String postalcode;

    private String region;

    private String remarks;

    private String title;

    private String alternativenumber;
    private String medicalevidence;
    private String detail;

    private String gdob;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDriverpassenger() {
        return driverpassenger;
    }

    public void setDriverpassenger(String driverpassenger) {
        this.driverpassenger = driverpassenger;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnglishlevel() {
        return englishlevel;
    }

    public void setEnglishlevel(String englishlevel) {
        this.englishlevel = englishlevel;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGaddress1() {
        return gaddress1;
    }

    public void setGaddress1(String gaddress1) {
        this.gaddress1 = gaddress1;
    }

    public String getGaddress2() {
        return gaddress2;
    }

    public void setGaddress2(String gaddress2) {
        this.gaddress2 = gaddress2;
    }

    public String getGaddress3() {
        return gaddress3;
    }

    public void setGaddress3(String gaddress3) {
        this.gaddress3 = gaddress3;
    }

    public String getGcity() {
        return gcity;
    }

    public void setGcity(String gcity) {
        this.gcity = gcity;
    }

    public String getGemail() {
        return gemail;
    }

    public void setGemail(String gemail) {
        this.gemail = gemail;
    }

    public String getGfirstname() {
        return gfirstname;
    }

    public void setGfirstname(String gfirstname) {
        this.gfirstname = gfirstname;
    }

    public String getGlandline() {
        return glandline;
    }

    public void setGlandline(String glandline) {
        this.glandline = glandline;
    }

    public String getGlastname() {
        return glastname;
    }

    public void setGlastname(String glastname) {
        this.glastname = glastname;
    }

    public String getGmiddlename() {
        return gmiddlename;
    }

    public void setGmiddlename(String gmiddlename) {
        this.gmiddlename = gmiddlename;
    }

    public String getGmobile() {
        return gmobile;
    }

    public void setGmobile(String gmobile) {
        this.gmobile = gmobile;
    }

    public String getGpostalcode() {
        return gpostalcode;
    }

    public void setGpostalcode(String gpostalcode) {
        this.gpostalcode = gpostalcode;
    }

    public String getGregion() {
        return gregion;
    }

    public void setGregion(String gregion) {
        this.gregion = gregion;
    }

    public String getGtitle() {
        return gtitle;
    }

    public void setGtitle(String gtitle) {
        this.gtitle = gtitle;
    }

    public String getInjdescription() {
        return injdescription;
    }

    public void setInjdescription(String injdescription) {
        this.injdescription = injdescription;
    }

    public String getInjlength() {
        return injlength;
    }

    public void setInjlength(String injlength) {
        this.injlength = injlength;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMedicalinfo() {
        return medicalinfo;
    }

    public void setMedicalinfo(String medicalinfo) {
        this.medicalinfo = medicalinfo;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNinumber() {
        return ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getOngoing() {
        return ongoing;
    }

    public void setOngoing(String ongoing) {
        this.ongoing = ongoing;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlternativenumber() {
        return alternativenumber;
    }

    public void setAlternativenumber(String alternativenumber) {
        this.alternativenumber = alternativenumber;
    }

    public String getMedicalevidence() {
        return medicalevidence;
    }

    public void setMedicalevidence(String medicalevidence) {
        this.medicalevidence = medicalevidence;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getGdob() {
        return gdob;
    }

    public void setGdob(String gdob) {
        this.gdob = gdob;
    }

    public List<String> getInjclasscodes() {
        return injclasscodes;
    }

    public void setInjclasscodes(List<String> injclasscodes) {
        this.injclasscodes = injclasscodes;
    }
}
