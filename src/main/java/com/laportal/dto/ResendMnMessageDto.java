package com.laportal.dto;

public class ResendMnMessageDto {


    private String mnMessagecode;

    public String getMnMessagecode() {
        return mnMessagecode;
    }

    public void setMnMessagecode(String mnMessagecode) {
        this.mnMessagecode = mnMessagecode;
    }
}
