package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 4/7/2023
 * Time: 3:56 PM
 * Project : legalassist
 */
public class BroadcastRequest {

    private List<String> toUsers = new ArrayList<>();

    private String other;

    private String subject;

    private String message;

    private String now;

    private String sechduleDate;

    private String sechduleTime;

    public List<String> getToUsers() {
        return toUsers;
    }

    public void setToUsers(List<String> toUsers) {
        this.toUsers = toUsers;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNow() {
        return now;
    }

    public void setNow(String now) {
        this.now = now;
    }

    public String getSechduleDate() {
        return sechduleDate;
    }

    public void setSechduleDate(String sechduleDate) {
        this.sechduleDate = sechduleDate;
    }

    public String getSechduleTime() {
        return sechduleTime;
    }

    public void setSechduleTime(String sechduleTime) {
        this.sechduleTime = sechduleTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
