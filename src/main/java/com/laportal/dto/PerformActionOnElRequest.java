package com.laportal.dto;

public class PerformActionOnElRequest {


    private String elcode;
    private String toStatus;
    private String reason;

    public String getElcode() {
        return elcode;
    }

    public void setElcode(String elcode) {
        this.elcode = elcode;
    }

    public String getToStatus() {
        return toStatus;
    }

    public void setToStatus(String toStatus) {
        this.toStatus = toStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
