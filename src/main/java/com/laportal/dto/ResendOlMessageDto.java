package com.laportal.dto;

public class ResendOlMessageDto {


    private String olmessagecode;

    public String getOlmessagecode() {
        return olmessagecode;
    }

    public void setOlmessagecode(String olmessagecode) {
        this.olmessagecode = olmessagecode;
    }
}
