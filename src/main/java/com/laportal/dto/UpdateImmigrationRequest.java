package com.laportal.dto;

import java.math.BigDecimal;
import java.util.Date;

public class UpdateImmigrationRequest {

    private long immigrationclaimcode;
    private String advice;
    private String clientaddress;
    private String clientcontactno;
    private String clientemail;
    private String clientname;
    private String clienttitle;
    private String contacttime;
    private String esign;
    private Date esigndate;
    private String immigrationcode;
    private Date inquirydatetime;
    private String inquirydescription;
    private String inquirynature;
    private String inquiryreferal;
    private String nationality;
    private String notes;
    private String remarks;
    private BigDecimal status;

    public long getImmigrationclaimcode() {
        return immigrationclaimcode;
    }

    public void setImmigrationclaimcode(long immigrationclaimcode) {
        this.immigrationclaimcode = immigrationclaimcode;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public String getClientaddress() {
        return clientaddress;
    }

    public void setClientaddress(String clientaddress) {
        this.clientaddress = clientaddress;
    }

    public String getClientcontactno() {
        return clientcontactno;
    }

    public void setClientcontactno(String clientcontactno) {
        this.clientcontactno = clientcontactno;
    }

    public String getClientemail() {
        return clientemail;
    }

    public void setClientemail(String clientemail) {
        this.clientemail = clientemail;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public String getClienttitle() {
        return clienttitle;
    }

    public void setClienttitle(String clienttitle) {
        this.clienttitle = clienttitle;
    }

    public String getContacttime() {
        return contacttime;
    }

    public void setContacttime(String contacttime) {
        this.contacttime = contacttime;
    }

    public String getEsign() {
        return esign;
    }

    public void setEsign(String esign) {
        this.esign = esign;
    }

    public Date getEsigndate() {
        return esigndate;
    }

    public void setEsigndate(Date esigndate) {
        this.esigndate = esigndate;
    }

    public String getImmigrationcode() {
        return immigrationcode;
    }

    public void setImmigrationcode(String immigrationcode) {
        this.immigrationcode = immigrationcode;
    }

    public Date getInquirydatetime() {
        return inquirydatetime;
    }

    public void setInquirydatetime(Date inquirydatetime) {
        this.inquirydatetime = inquirydatetime;
    }

    public String getInquirydescription() {
        return inquirydescription;
    }

    public void setInquirydescription(String inquirydescription) {
        this.inquirydescription = inquirydescription;
    }

    public String getInquirynature() {
        return inquirynature;
    }

    public void setInquirynature(String inquirynature) {
        this.inquirynature = inquirynature;
    }

    public String getInquiryreferal() {
        return inquiryreferal;
    }

    public void setInquiryreferal(String inquiryreferal) {
        this.inquiryreferal = inquiryreferal;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getStatus() {
        return status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

}
