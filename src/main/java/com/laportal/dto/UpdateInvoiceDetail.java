package com.laportal.dto;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 5/5/2023
 * Time: 5:45 PM
 * Project : legalassist
 */
public class UpdateInvoiceDetail {
    private String invoiceDetailId;
    private String compaingcode;
    private String charge;
    private String adjust;
    private String invType;

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getCompaingcode() {
        return compaingcode;
    }

    public void setCompaingcode(String compaingcode) {
        this.compaingcode = compaingcode;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getAdjust() {
        return adjust;
    }

    public void setAdjust(String adjust) {
        this.adjust = adjust;
    }

    public String getInvType() {
        return invType;
    }

    public void setInvType(String invType) {
        this.invType = invType;
    }
}
