package com.laportal.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

public class UpdateTblHdrclaim {

    private long hdrclaimcode;

    private String aprxReporteddate;


    private Date defectLastreportedon;

    private BigDecimal defectReported;

    private String healthAffected;

    private String healthRelDetails;

    private String llContactno;

    private String llName;

    private String llRespondDetail;

    private String llResponded;

    private String reportedToLl;

    private String reportedToLlBy;

    private Date defectFirstreportedon;


    public long getHdrclaimcode() {
        return hdrclaimcode;
    }

    public void setHdrclaimcode(long hdrclaimcode) {
        this.hdrclaimcode = hdrclaimcode;
    }

    public String getAprxReporteddate() {
        return aprxReporteddate;
    }

    public void setAprxReporteddate(String aprxReporteddate) {
        this.aprxReporteddate = aprxReporteddate;
    }

    public Date getDefectLastreportedon() {
        return defectLastreportedon;
    }

    public void setDefectLastreportedon(Date defectLastreportedon) {
        this.defectLastreportedon = defectLastreportedon;
    }

    public BigDecimal getDefectReported() {
        return defectReported;
    }

    public void setDefectReported(BigDecimal defectReported) {
        this.defectReported = defectReported;
    }

    public String getHealthAffected() {
        return healthAffected;
    }

    public void setHealthAffected(String healthAffected) {
        this.healthAffected = healthAffected;
    }

    public String getHealthRelDetails() {
        return healthRelDetails;
    }

    public void setHealthRelDetails(String healthRelDetails) {
        this.healthRelDetails = healthRelDetails;
    }

    public String getLlContactno() {
        return llContactno;
    }

    public void setLlContactno(String llContactno) {
        this.llContactno = llContactno;
    }

    public String getLlName() {
        return llName;
    }

    public void setLlName(String llName) {
        this.llName = llName;
    }

    public String getLlRespondDetail() {
        return llRespondDetail;
    }

    public void setLlRespondDetail(String llRespondDetail) {
        this.llRespondDetail = llRespondDetail;
    }

    public String getLlResponded() {
        return llResponded;
    }

    public void setLlResponded(String llResponded) {
        this.llResponded = llResponded;
    }

    public String getReportedToLl() {
        return reportedToLl;
    }

    public void setReportedToLl(String reportedToLl) {
        this.reportedToLl = reportedToLl;
    }

    public String getReportedToLlBy() {
        return reportedToLlBy;
    }

    public void setReportedToLlBy(String reportedToLlBy) {
        this.reportedToLlBy = reportedToLlBy;
    }

    public Date getDefectFirstreportedon() {
        return defectFirstreportedon;
    }

    public void setDefectFirstreportedon(Date defectFirstreportedon) {
        this.defectFirstreportedon = defectFirstreportedon;
    }
}