package com.laportal.dto;

public class PerformActionOnDbRequest {


    private String dbClaimCode;
    private String toStatus;
    private String reason;

    public String getDbClaimCode() {
        return dbClaimCode;
    }

    public void setDbClaimCode(String dbClaimCode) {
        this.dbClaimCode = dbClaimCode;
    }

    public String getToStatus() {
        return toStatus;
    }

    public void setToStatus(String toStatus) {
        this.toStatus = toStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
