package com.laportal.dto;

public class TenancyNoteRequest {

    private String tenancyCode;

    private String note;

    private String userCatCode;

    public String getTenancyCode() {
        return tenancyCode;
    }

    public void setTenancyCode(String tenancyCode) {
        this.tenancyCode = tenancyCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserCatCode() {
        return userCatCode;
    }

    public void setUserCatCode(String userCatCode) {
        this.userCatCode = userCatCode;
    }
}
