package com.laportal.dto;

public class HdrNoteRequest {

    private String hdrCode;

    private String note;

    private String userCatCode;

    public String getHdrCode() {
        return hdrCode;
    }

    public void setHdrCode(String hdrCode) {
        this.hdrCode = hdrCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserCatCode() {
        return userCatCode;
    }

    public void setUserCatCode(String userCatCode) {
        this.userCatCode = userCatCode;
    }
}
