package com.laportal.dto;

public class AssignPcpCasetoSolicitor {

    private String pcpClaimCode;
    private String solicitorCode;
    private String solicitorUserCode;

    public String getPcpClaimCode() {
        return pcpClaimCode;
    }

    public void setPcpClaimCode(String pcpClaimCode) {
        this.pcpClaimCode = pcpClaimCode;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public String getSolicitorUserCode() {
        return solicitorUserCode;
    }

    public void setSolicitorUserCode(String solicitorUserCode) {
        this.solicitorUserCode = solicitorUserCode;
    }
}
