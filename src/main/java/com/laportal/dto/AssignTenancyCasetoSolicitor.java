package com.laportal.dto;

public class AssignTenancyCasetoSolicitor {

    private String tenancyClaimCode;
    private String solicitorCode;
    private String solicitorUserCode;

    public String getTenancyClaimCode() {
        return tenancyClaimCode;
    }

    public void setTenancyClaimCode(String tenancyClaimCode) {
        this.tenancyClaimCode = tenancyClaimCode;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public String getSolicitorUserCode() {
        return solicitorUserCode;
    }

    public void setSolicitorUserCode(String solicitorUserCode) {
        this.solicitorUserCode = solicitorUserCode;
    }
}
