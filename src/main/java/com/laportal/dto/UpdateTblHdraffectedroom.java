package com.laportal.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class UpdateTblHdraffectedroom {

    private long hdraffectedroom;

    private String roomName;

    private String damageList;

    private String disrepairDetail;


    private Date lastReported;

    private String personalPropertydamage;

    private String reportDetails;

    public long getHdraffectedroom() {
        return hdraffectedroom;
    }

    public void setHdraffectedroom(long hdraffectedroom) {
        this.hdraffectedroom = hdraffectedroom;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getDamageList() {
        return damageList;
    }

    public void setDamageList(String damageList) {
        this.damageList = damageList;
    }

    public String getDisrepairDetail() {
        return disrepairDetail;
    }

    public void setDisrepairDetail(String disrepairDetail) {
        this.disrepairDetail = disrepairDetail;
    }

    public Date getLastReported() {
        return lastReported;
    }

    public void setLastReported(Date lastReported) {
        this.lastReported = lastReported;
    }

    public String getPersonalPropertydamage() {
        return personalPropertydamage;
    }

    public void setPersonalPropertydamage(String personalPropertydamage) {
        this.personalPropertydamage = personalPropertydamage;
    }

    public String getReportDetails() {
        return reportDetails;
    }

    public void setReportDetails(String reportDetails) {
        this.reportDetails = reportDetails;
    }
}