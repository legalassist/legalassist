package com.laportal.dto;

public class AssignOlCasetoSolicitor {

    private String olClaimCode;
    private String solicitorCode;
    private String solicitorUserCode;

    public String getOlClaimCode() {
        return olClaimCode;
    }

    public void setOlClaimCode(String olClaimCode) {
        this.olClaimCode = olClaimCode;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public String getSolicitorUserCode() {
        return solicitorUserCode;
    }

    public void setSolicitorUserCode(String solicitorUserCode) {
        this.solicitorUserCode = solicitorUserCode;
    }
}
