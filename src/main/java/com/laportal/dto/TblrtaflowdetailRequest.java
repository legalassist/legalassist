package com.laportal.dto;


public class TblrtaflowdetailRequest {


    private String apiflag;

    private String buttonname;

    private String buttonvalue;

    private String rtaflowcode;
    private String rtastatuscode;
    private String listusercategory;

    private String caseacceptdialog;
    private String caseassigndialog;
    private String caserejectdialog;


    public String getApiflag() {
        return apiflag;
    }

    public void setApiflag(String apiflag) {
        this.apiflag = apiflag;
    }

    public String getButtonname() {
        return buttonname;
    }

    public void setButtonname(String buttonname) {
        this.buttonname = buttonname;
    }

    public String getButtonvalue() {
        return buttonvalue;
    }

    public void setButtonvalue(String buttonvalue) {
        this.buttonvalue = buttonvalue;
    }

    public String getRtaflowcode() {
        return rtaflowcode;
    }

    public void setRtaflowcode(String rtaflowcode) {
        this.rtaflowcode = rtaflowcode;
    }

    public String getRtastatuscode() {
        return rtastatuscode;
    }

    public void setRtastatuscode(String rtastatuscode) {
        this.rtastatuscode = rtastatuscode;
    }

    public String getListusercategory() {
        return listusercategory;
    }

    public void setListusercategory(String listusercategory) {
        this.listusercategory = listusercategory;
    }

    public String getCaseacceptdialog() {
        return caseacceptdialog;
    }

    public void setCaseacceptdialog(String caseacceptdialog) {
        this.caseacceptdialog = caseacceptdialog;
    }

    public String getCaseassigndialog() {
        return caseassigndialog;
    }

    public void setCaseassigndialog(String caseassigndialog) {
        this.caseassigndialog = caseassigndialog;
    }

    public String getCaserejectdialog() {
        return caserejectdialog;
    }

    public void setCaserejectdialog(String caserejectdialog) {
        this.caserejectdialog = caserejectdialog;
    }
}