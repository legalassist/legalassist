package com.laportal.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 2/23/2024
 * Time: 3:11 PM
 * Project : legalassist
 */

@Getter
@Setter
public class AddcompanyCfaRequest {

    private String companyCode;

    private String fileName;

    private String countryType;

    private String ageNature;

    private String compaignCode;

    private String JointTenency;

    private String bike;




}
