package com.laportal.dto;

public class UpdateCompanyCompaignRequest {


    private String companyjobcode;
    private String companycode;
    private String status;
    private String bike;
    private String faultrepairs;
    private String housingFee;
    private String hybrid;
    private String pedestrian;
    private String prestigeroadworthy;
    private String prestigeunroadworthy;
    private String recovery;
    private String repairs;
    private String salvage;
    private String scotishRta;
    private String serious;
    private String solicitorsfees;
    private String standardroadworthy;
    private String standardunroadworthy;
    private String storage;
    private String wiplash;
    private String minor;


    public String getCompanyjobcode() {
        return companyjobcode;
    }

    public void setCompanyjobcode(String companyjobcode) {
        this.companyjobcode = companyjobcode;
    }

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBike() {
        return bike;
    }

    public void setBike(String bike) {
        this.bike = bike;
    }

    public String getFaultrepairs() {
        return faultrepairs;
    }

    public void setFaultrepairs(String faultrepairs) {
        this.faultrepairs = faultrepairs;
    }

    public String getHousingFee() {
        return housingFee;
    }

    public void setHousingFee(String housingFee) {
        this.housingFee = housingFee;
    }

    public String getHybrid() {
        return hybrid;
    }

    public void setHybrid(String hybrid) {
        this.hybrid = hybrid;
    }

    public String getPedestrian() {
        return pedestrian;
    }

    public void setPedestrian(String pedestrian) {
        this.pedestrian = pedestrian;
    }

    public String getPrestigeroadworthy() {
        return prestigeroadworthy;
    }

    public void setPrestigeroadworthy(String prestigeroadworthy) {
        this.prestigeroadworthy = prestigeroadworthy;
    }

    public String getPrestigeunroadworthy() {
        return prestigeunroadworthy;
    }

    public void setPrestigeunroadworthy(String prestigeunroadworthy) {
        this.prestigeunroadworthy = prestigeunroadworthy;
    }

    public String getRecovery() {
        return recovery;
    }

    public void setRecovery(String recovery) {
        this.recovery = recovery;
    }

    public String getRepairs() {
        return repairs;
    }

    public void setRepairs(String repairs) {
        this.repairs = repairs;
    }

    public String getSalvage() {
        return salvage;
    }

    public void setSalvage(String salvage) {
        this.salvage = salvage;
    }

    public String getScotishRta() {
        return scotishRta;
    }

    public void setScotishRta(String scotishRta) {
        this.scotishRta = scotishRta;
    }

    public String getSerious() {
        return serious;
    }

    public void setSerious(String serious) {
        this.serious = serious;
    }

    public String getSolicitorsfees() {
        return solicitorsfees;
    }

    public void setSolicitorsfees(String solicitorsfees) {
        this.solicitorsfees = solicitorsfees;
    }

    public String getStandardroadworthy() {
        return standardroadworthy;
    }

    public void setStandardroadworthy(String standardroadworthy) {
        this.standardroadworthy = standardroadworthy;
    }

    public String getStandardunroadworthy() {
        return standardunroadworthy;
    }

    public void setStandardunroadworthy(String standardunroadworthy) {
        this.standardunroadworthy = standardunroadworthy;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getWiplash() {
        return wiplash;
    }

    public void setWiplash(String wiplash) {
        this.wiplash = wiplash;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }
}
