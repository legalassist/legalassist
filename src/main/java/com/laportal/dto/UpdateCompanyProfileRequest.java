package com.laportal.dto;


public class UpdateCompanyProfileRequest {

    private String companycode;

    private String accountno;

    private String addressline1;

    private String addressline2;

    private String city;

    private String companyregno;

    private LovResponse companystatus;

    private String contactperson;

    private String email;

    private String name;

    private String phone;
    private String phone2;

    private String postcode;

    private String region;

    private String remarks;


    private String varegno;

    private String website;

    private String directIntroducer;

    private String billtoemail;
    private String billtoname;
    private String accountemail;
    private String secondaryaccountemail;
    private String bdmuser;

    private String vat;
    private String jurisdiction;

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getAddressline1() {
        return addressline1;
    }

    public void setAddressline1(String addressline1) {
        this.addressline1 = addressline1;
    }

    public String getAddressline2() {
        return addressline2;
    }

    public void setAddressline2(String addressline2) {
        this.addressline2 = addressline2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompanyregno() {
        return companyregno;
    }

    public void setCompanyregno(String companyregno) {
        this.companyregno = companyregno;
    }

    public LovResponse getCompanystatus() {
        return companystatus;
    }

    public void setCompanystatus(LovResponse companystatus) {
        this.companystatus = companystatus;
    }

    public String getContactperson() {
        return contactperson;
    }

    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }


    public String getVaregno() {
        return varegno;
    }

    public void setVaregno(String varegno) {
        this.varegno = varegno;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }


    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }


    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getDirectIntroducer() {
        return directIntroducer;
    }

    public void setDirectIntroducer(String directIntroducer) {
        this.directIntroducer = directIntroducer;
    }

    public String getBilltoemail() {
        return billtoemail;
    }

    public void setBilltoemail(String billtoemail) {
        this.billtoemail = billtoemail;
    }

    public String getBilltoname() {
        return billtoname;
    }

    public void setBilltoname(String billtoname) {
        this.billtoname = billtoname;
    }

    public String getAccountemail() {
        return accountemail;
    }

    public void setAccountemail(String accountemail) {
        this.accountemail = accountemail;
    }

    public String getSecondaryaccountemail() {
        return secondaryaccountemail;
    }

    public void setSecondaryaccountemail(String secondaryaccountemail) {
        this.secondaryaccountemail = secondaryaccountemail;
    }

    public String getBdmuser() {
        return bdmuser;
    }

    public void setBdmuser(String bdmuser) {
        this.bdmuser = bdmuser;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(String jurisdiction) {
        this.jurisdiction = jurisdiction;
    }
}
