package com.laportal.dto;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 8/7/2023
 * Time: 2:55 PM
 * Project : legalassist
 */
public class CopyHireToHireRequest {
    private long hirecode;

    public long getHirecode() {
        return hirecode;
    }

    public void setHirecode(long hirecode) {
        this.hirecode = hirecode;
    }
}
