package com.laportal.dto;

public class MnNoteRequest {

    private String dbCode;

    private String note;

    private String userCatCode;

    public String getDbCode() {
        return dbCode;
    }

    public void setDbCode(String dbCode) {
        this.dbCode = dbCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserCatCode() {
        return userCatCode;
    }

    public void setUserCatCode(String userCatCode) {
        this.userCatCode = userCatCode;
    }
}
