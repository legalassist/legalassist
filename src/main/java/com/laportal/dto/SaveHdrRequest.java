package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

public class SaveHdrRequest {

    private SaveTblHdrclaim saveTblHdrclaim;

    private SaveTblHdrclaimant saveTblHdrclaimant;

    private SaveTblHdrjointtenancy saveTblHdrjointtenancy;

    private SaveTblHdrtenancy saveTblHdrtenancy;

    private List<SaveTblHdraffectedper> saveTblHdraffectedperList = new ArrayList<SaveTblHdraffectedper>();

    private List<SaveTblHdraffectedroom> saveTblHdraffectedroomList = new ArrayList<SaveTblHdraffectedroom>();


    public SaveTblHdrclaim getSaveTblHdrclaim() {
        return saveTblHdrclaim;
    }

    public void setSaveTblHdrclaim(SaveTblHdrclaim saveTblHdrclaim) {
        this.saveTblHdrclaim = saveTblHdrclaim;
    }

    public SaveTblHdrclaimant getSaveTblHdrclaimant() {
        return saveTblHdrclaimant;
    }

    public void setSaveTblHdrclaimant(SaveTblHdrclaimant saveTblHdrclaimant) {
        this.saveTblHdrclaimant = saveTblHdrclaimant;
    }

    public SaveTblHdrjointtenancy getSaveTblHdrjointtenancy() {
        return saveTblHdrjointtenancy;
    }

    public void setSaveTblHdrjointtenancy(SaveTblHdrjointtenancy saveTblHdrjointtenancy) {
        this.saveTblHdrjointtenancy = saveTblHdrjointtenancy;
    }

    public SaveTblHdrtenancy getSaveTblHdrtenancy() {
        return saveTblHdrtenancy;
    }

    public void setSaveTblHdrtenancy(SaveTblHdrtenancy saveTblHdrtenancy) {
        this.saveTblHdrtenancy = saveTblHdrtenancy;
    }

    public List<SaveTblHdraffectedper> getSaveTblHdraffectedperList() {
        return saveTblHdraffectedperList;
    }

    public void setSaveTblHdraffectedperList(List<SaveTblHdraffectedper> saveTblHdraffectedperList) {
        this.saveTblHdraffectedperList = saveTblHdraffectedperList;
    }

    public List<SaveTblHdraffectedroom> getSaveTblHdraffectedroomList() {
        return saveTblHdraffectedroomList;
    }

    public void setSaveTblHdraffectedroomList(List<SaveTblHdraffectedroom> saveTblHdraffectedroomList) {
        this.saveTblHdraffectedroomList = saveTblHdraffectedroomList;
    }

}
