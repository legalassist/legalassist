package com.laportal.dto;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 1/19/2023
 * Time: 12:31 PM
 * Project : legalassist
 */
public class ManualJobRequest {

    private String campaignCode;
    private String billByDate;
    private List<String> companyCodeList;
    private String billallpendingcases;

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getBillByDate() {
        return billByDate;
    }

    public void setBillByDate(String billByDate) {
        this.billByDate = billByDate;
    }

    public List<String> getCompanyCodeList() {
        return companyCodeList;
    }

    public void setCompanyCodeList(List<String> companyCodeList) {
        this.companyCodeList = companyCodeList;
    }

    public String getBillallpendingcases() {
        return billallpendingcases;
    }

    public void setBillallpendingcases(String billallpendingcases) {
        this.billallpendingcases = billallpendingcases;
    }
}
