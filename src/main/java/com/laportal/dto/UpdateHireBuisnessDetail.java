package com.laportal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 3/1/2024
 * Time: 4:31 PM
 * Project : legalassist
 */

@Getter
@Setter
public class UpdateHireBuisnessDetail {

    private long hirebusinessescode;
    private long hirebusinessdetailcode;
    private Date bookingdate;
    private String bookingmode;
    private Date hireenddate;
    private Date hirestartdate;
    private String service;
}
