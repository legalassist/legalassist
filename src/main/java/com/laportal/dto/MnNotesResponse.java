package com.laportal.dto;

import com.laportal.model.TblMnnote;
import com.laportal.model.TblPlnote;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 2/15/2024
 * Time: 9:22 PM
 * Project : legalassist
 */

@Getter
@Setter
public class MnNotesResponse {

    private List<TblMnnote> tblMnnotes = new ArrayList<>();
    private List<TblMnnote> tblMnnotesLegalOnly = new ArrayList<>();
}
