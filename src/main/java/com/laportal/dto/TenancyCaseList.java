package com.laportal.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TenancyCaseList {

    private BigDecimal tenancyClaimCode;
    private String created;
    private String code;
    private String client;
    private String taskDue;
    private String taskName;
    private String status;
    private String email;
    private String address;
    private Date lastUpdated;
    private String contactNo;
    private String introducer;
    private Date lastNote;

    public BigDecimal getTenancyClaimCode() {
        return tenancyClaimCode;
    }

    public void setTenancyClaimCode(BigDecimal tenancyClaimCode) {
        this.tenancyClaimCode = tenancyClaimCode;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getTaskDue() {
        return taskDue;
    }

    public void setTaskDue(String taskDue) {
        this.taskDue = taskDue;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getIntroducer() {
        return introducer;
    }

    public void setIntroducer(String introducer) {
        this.introducer = introducer;
    }

    public Date getLastNote() {
        return lastNote;
    }

    public void setLastNote(Date lastNote) {
        this.lastNote = lastNote;
    }
}
