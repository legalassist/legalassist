package com.laportal.dto;

public class CurrentTaskRequest {

    private long taskCode;
    private long rtaCode;
    private String current;
    private long hdrCode;
    private long elCode;
    private long olCode;
    private long plCode;
    private long mnCode;

    public long getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(long taskCode) {
        this.taskCode = taskCode;
    }

    public long getRtaCode() {
        return rtaCode;
    }

    public void setRtaCode(long rtaCode) {
        this.rtaCode = rtaCode;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public long getHdrCode() {
        return hdrCode;
    }

    public void setHdrCode(long hdrCode) {
        this.hdrCode = hdrCode;
    }

    public long getElCode() {
        return elCode;
    }

    public void setElCode(long elCode) {
        this.elCode = elCode;
    }

    public long getOlCode() {
        return olCode;
    }

    public void setOlCode(long olCode) {
        this.olCode = olCode;
    }

    public long getPlCode() {
        return plCode;
    }

    public void setPlCode(long plCode) {
        this.plCode = plCode;
    }

    public long getMnCode() {
        return mnCode;
    }

    public void setMnCode(long mnCode) {
        this.mnCode = mnCode;
    }
}
