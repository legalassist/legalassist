package com.laportal.dto;

public class AssignPlCasetoSolicitor {

    private String plClaimCode;
    private String solicitorCode;
    private String solicitorUserCode;

    public String getPlClaimCode() {
        return plClaimCode;
    }

    public void setPlClaimCode(String plClaimCode) {
        this.plClaimCode = plClaimCode;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public String getSolicitorUserCode() {
        return solicitorUserCode;
    }

    public void setSolicitorUserCode(String solicitorUserCode) {
        this.solicitorUserCode = solicitorUserCode;
    }
}
