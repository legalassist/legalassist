package com.laportal.dto;

public class AssignDbCasetoSolicitor {

    private String dbClaimCode;
    private String solicitorCode;
    private String solicitorUserCode;

    public String getDbClaimCode() {
        return dbClaimCode;
    }

    public void setDbClaimCode(String dbClaimCode) {
        this.dbClaimCode = dbClaimCode;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public String getSolicitorUserCode() {
        return solicitorUserCode;
    }

    public void setSolicitorUserCode(String solicitorUserCode) {
        this.solicitorUserCode = solicitorUserCode;
    }
}
