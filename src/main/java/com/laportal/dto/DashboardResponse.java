package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 2/26/2023
 * Time: 2:25 PM
 * Project : legalassist
 */
public class DashboardResponse {

    private List<Nav> nav = new ArrayList<>();

    public List<Nav> getNav() {
        return nav;
    }

    public void setNav(List<Nav> nav) {
        this.nav = nav;
    }
}
