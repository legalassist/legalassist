package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleDamageDetails {
	@JsonProperty("vhThroughAltCompany")
	public String vhThroughAltCompany;
	@JsonProperty("vhCompanyName")
	public String vhCompanyName;
	@JsonProperty("vhCompanyAddress")
	public String vhCompanyAddress;
	@JsonProperty("vhCompanyPhone")
	public String vhCompanyPhone;
	@JsonProperty("vhCompanyNameReferenceNo")
	public String vhCompanyNameReferenceNo;
	@JsonProperty("vhTotalLoss")
	public String vhTotalLoss;
	@JsonProperty("vhPositionWithRepairs")
	public String vhPositionWithRepairs;
	@JsonProperty("vhRequireDefantant")
	public String vhRequireDefantant;
	@JsonProperty("vhLocation")
	public String vhLocation;
	@JsonProperty("vhContactDetails")
	public String vhContactDetails;
}
