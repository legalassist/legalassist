package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehichleDetails2 {
	@JsonProperty("VDC1")
	public String vdc1;
	@JsonProperty("VDC2")
	public String vdc2;
	@JsonProperty("VDC3")
	public String vdc3;
	@JsonProperty("VDC4")
	public String vdc4;
	@JsonProperty("VDC5")
	public String vdc5;
	@JsonProperty("VDC6")
	public String vdc6;
	@JsonProperty("VDC7")
	public String vdc7;
	@JsonProperty("VDC8")
	public String vdc8;
	@JsonProperty("VDTP1")
	public String vdtp1;
	@JsonProperty("VDTP2")
	public String vdtp2;
	@JsonProperty("VDTP3")
	public String vdtp3;
	@JsonProperty("VDTP4")
	public String vdtp4;
	@JsonProperty("VDTP5")
	public String vdtp5;
	@JsonProperty("VDTP6")
	public String vdtp6;
	@JsonProperty("VDTP7")
	public String vdtp7;
	@JsonProperty("VDTP8")
	public String vdtp8;
	@JsonProperty("DamageToVehicle")
	public String damageToVehicle;
	@JsonProperty("ClaimingThroughOwnInsurer")
	public String claimingThroughOwnInsurer;
	@JsonProperty("DCTOC")
	public String dctoc;
	@JsonProperty("VehicleTotalLoss")
	public String vehicleTotalLoss;
	@JsonProperty("CurrentPositionWithRepairs")
	public String currentPositionWithRepairs;
	@JsonProperty("RDOI")
	public String rdoi;
	@JsonProperty("DoneIntervention")
	public String doneIntervention;
	@JsonProperty("ClientPecuniousity")
	public String clientPecuniousity;
	@JsonProperty("InterventionDetails")
	public String interventionDetails;
	@JsonProperty("CarProvided")
	public String carProvided;
	@JsonProperty("CarRequired")
	public String carRequired;
	@JsonProperty("HireStillOnGoing")
	public String hireStillOnGoing;
}
