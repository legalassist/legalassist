package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LitigationDetails {
	@JsonProperty("LitigationTitle")
	public String litigationTitle;
	@JsonProperty("LitigationForename")
	public String litigationForename;
	@JsonProperty("LitigationSurname")
	public String litigationSurname;
	@JsonProperty("LitigationAddress")
	public String litigationAddress;
	@JsonProperty("LitigationPostcode")
	public String litigationPostcode;
	@JsonProperty("LitigationTel")
	public String litigationTel;
	@JsonProperty("LitigationPhoneOther")
	public String litigationPhoneOther;
	@JsonProperty("LitigationPhoneWork")
	public String litigationPhoneWork;
	@JsonProperty("LitigationDOB")
	public String litigationDOB;
	@JsonProperty("LitigationNotes")
	public String litigationNotes;
	@JsonProperty("LitigationNI")
	public String litigationNI;
}
