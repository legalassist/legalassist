package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class HireDetails {
	@JsonProperty("HrVehicleId")
	public String hrVehicleId;
	@JsonProperty("HrVehicleReg")
	public String hrVehicleReg;
	@JsonProperty("HrMake")
	public String hrMake;
	@JsonProperty("HrTempMake")
	public String hrTempMake;
	@JsonProperty("HrModel")
	public String hrModel;
	@JsonProperty("HrABI")
	public String hrABI;
	@JsonProperty("HireToCorresponentID")
	public String hireToCorresponentID;
	@JsonProperty("HrDailyRate")
	public String hrDailyRate;
	@JsonProperty("HrCDW")
	public String hrCDW;
	@JsonProperty("HrVAT1")
	public String hrVAT1;
	@JsonProperty("NonStdDriver")
	public String nonStdDriver;
	@JsonProperty("HrAdmin")
	public String hrAdmin;
	@JsonProperty("HrOther")
	public String hrOther;
	@JsonProperty("HrTaxiEquipmentTransfer")
	public String hrTaxiEquipmentTransfer;
	@JsonProperty("HireInMileage")
	public String hireInMileage;
	@JsonProperty("HireOutMileage")
	public String hireOutMileage;
	@JsonProperty("HrCurrentMileage")
	public String hrCurrentMileage;
	@JsonProperty("HrAddtionalDriver")
	public String hrAddtionalDriver;
	@JsonProperty("DrivertransferFee")
	public String drivertransferFee;
	@JsonProperty("HrCollection")
	public String hrCollection;
	@JsonProperty("TaxiLicencingFee")
	public String taxiLicencingFee;
	@JsonProperty("DeliveryCollection")
	public String deliveryCollection;
	@JsonProperty("HireInTime")
	public String hireInTime;
	@JsonProperty("HireOutTime")
	public String hireOutTime;
	@JsonProperty("HrStartDate")
	public Date hrStartDate;
	@JsonProperty("HrEndDate")
	public Date hrEndDate;
	@JsonProperty("OffHireVehicleLocation")
	public String offHireVehicleLocation;
	@JsonProperty("HireType")
	public String hireType;
	@JsonProperty("CouncilUsage")
	public String councilUsage;
	@JsonProperty("PaddleShift")
	public String paddleShift;
	@JsonProperty("Estate")
	public String estate;
	@JsonProperty("TowBar")
	public String towBar;
	@JsonProperty("BabySeat")
	public String babySeat;
	@JsonProperty("Dual_Control")
	public String dualControl;
	@JsonProperty("Hrtotaldays")
	public String hrtotaldays;
	@JsonProperty("HrInvoiceTotalWithout")
	public String hrInvoiceTotalWithout;
	@JsonProperty("HrInvoiceTotalWith")
	public String hrInvoiceTotalWith;
	@JsonProperty("HrInvoiceTotalVAT")
	public String hrInvoiceTotalVAT;
	@JsonProperty("HrRef")
	public Object hrRef;
}
