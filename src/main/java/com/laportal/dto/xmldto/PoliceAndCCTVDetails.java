package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PoliceAndCCTVDetails {
	@JsonProperty("PoliceCalled")
	public String policeCalled;
	@JsonProperty("PoliceStationName")
	public String policeStationName;
	@JsonProperty("PoliceAddress")
	public String policeAddress;
	@JsonProperty("PolicePostCode")
	public String policePostCode;
	@JsonProperty("PoliceTelMain")
	public String policeTelMain;
	@JsonProperty("PoliceEmailMain")
	public String policeEmailMain;
	@JsonProperty("PoliceFax")
	public String policeFax;
	@JsonProperty("PoliceRef")
	public String policeRef;
	@JsonProperty("PoliceOfficer")
	public String policeOfficer;
	@JsonProperty("PoliceCallerNumber")
	public String policeCallerNumber;
	@JsonProperty("PoliceLogNo")
	public String policeLogNo;
	@JsonProperty("PoliceNotes")
	public String policeNotes;
	@JsonProperty("CCTVAvailable")
	public String cctvAvailable;
	@JsonProperty("CCTVCompanyName")
	public String cctvCompanyName;
	@JsonProperty("CCTVAddress")
	public String cctvAddress;
	@JsonProperty("CCTVPostCode")
	public String cctvPostCode;
	@JsonProperty("CCTVTelMain")
	public String cctvTelMain;
	@JsonProperty("CCTVEmailMain")
	public String cctvEmailMain;
	@JsonProperty("CCTVFax")
	public String cctvFax;
}
