package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeDetails {
	@JsonProperty("UnEmployed")
	public String unEmployed;
	@JsonProperty("EmployeeCompanyName")
	public String employeeCompanyName;
	@JsonProperty("EmployeeDepartment")
	public String employeeDepartment;
	@JsonProperty("EmployeePhone")
	public String employeePhone;
	@JsonProperty("EmployeeAddress1")
	public String employeeAddress1;
	@JsonProperty("EmployeeAddress2")
	public String employeeAddress2;
	@JsonProperty("EmploymenPostCode")
	public String employmenPostCode;

}
