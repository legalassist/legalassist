package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoryDetails {
	@JsonProperty("HistoryDescription")
	public String historyDescription;
	@JsonProperty("HistoryTypeID")
	public int historyTypeID;
	@JsonProperty("HistoryTitle")
	public String historyTitle;
	@JsonProperty("HistoryHireEntry")
	public boolean historyHireEntry;
	@JsonProperty("HistoryStatusID")
	public String historyStatusID;
	@JsonProperty("HistoryDocumentName")
	public String historyDocumentName;
}
