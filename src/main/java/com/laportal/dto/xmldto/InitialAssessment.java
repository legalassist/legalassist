package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InitialAssessment {
	@JsonProperty("VDInitStop")
	public String vdInitStop;
	@JsonProperty("VDInitWriteOff")
	public String vdInitWriteOff;
	@JsonProperty("VDInitDrivable")
	public String vdInitDrivable;
	@JsonProperty("VDInitEstimate")
	public String vdInitEstimate;
	@JsonProperty("VDInitRepair")
	public String vdInitRepair;
	@JsonProperty("VDInitDays")
	public String vdInitDays;
	@JsonProperty("VDInitLocationddl")
	public String vdInitLocationddl;
	@JsonProperty("VDInitLicationText")
	public String vdInitLicationText;
}
