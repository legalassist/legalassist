package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class ClaimantDetails {
	@JsonProperty("ClaimantTitle")
	public String claimantTitle;
	@JsonProperty("ClaimantForename")
	public String claimantForename;
	@JsonProperty("ClaimantSurname")
	public String claimantSurname;
	@JsonProperty("ClaimantAddress")
	public String claimantAddress;
	@JsonProperty("ClaimantAddress2")
	public String claimantAddress2;
	@JsonProperty("ClaimantAddress3")
	public String claimantAddress3;
	@JsonProperty("ClaimantTown")
	public String claimantTown;
	@JsonProperty("ClaimantPostCode")
	public String claimantPostCode;
	@JsonProperty("ClaimantDOB")
	public String claimantDOB;
	@JsonProperty("ClaimantHomeTel")
	public String claimantHomeTel;
	@JsonProperty("ClaimantWorkTel")
	public String claimantWorkTel;
	@JsonProperty("ClaimantEmail")
	public String claimantEmail;
	@JsonProperty("ClaimantWas")
	public String claimantWas;
	@JsonProperty("ClaimingPI")
	public boolean claimingPI;
	@JsonProperty("ClaimantOwner")
	public boolean claimantOwner;
	@JsonProperty("IsClaimantTaxiDriver2")
	public boolean isClaimantTaxiDriver2;
	@JsonProperty("ChildClaim")
	public String childClaim;
	@JsonProperty("ClaimantOccupation")
	public String claimantOccupation;
	@JsonProperty("ClaimantNINumber")
	public String claimantNINumber;
	@JsonProperty("ClaiminForm")
	public String claiminForm;
	@JsonProperty("MIBClaim")
	public String mibClaim;
	@JsonProperty("TotalPassenger")
	public String totalPassenger;
	@JsonProperty("AnyOtherClaim")
	public boolean anyOtherClaim;
	@JsonProperty("HowManyClaim")
	public String howManyClaim;
	@JsonProperty("MainDetailsNote")
	public String mainDetailsNote;
	@JsonProperty("ClaimType")
	public String claimType;
	@JsonProperty("ClaimantReasonNoNINumber")
	public String claimantReasonNoNINumber;
	@JsonProperty("ClaimingHire")
	public String claimingHire;
	@JsonProperty("ClaimingVDClientReparing")
	public String claimingVDClientReparing;
	@JsonProperty("ClaimingVDSourceReparing")
	public String claimingVDSourceReparing;
	@JsonProperty("ClaimingStorage")
	public String claimingStorage;
	@JsonProperty("ClaimingTransportationOrRecovery")
	public String claimingTransportationOrRecovery;
	@JsonProperty("ClaimingInspectionRequired")
	public String claimingInspectionRequired;
	@JsonProperty("ClaimingOther")
	public String claimingOther;
	@JsonProperty("ClaimingLossOfUse")
	public String claimingLossOfUse;
	@JsonProperty("ClaimingLossOfEarnings")
	public String claimingLossOfEarnings;
	@JsonProperty("ClaimingExcess")
	public String claimingExcess;
	@JsonProperty("ClaimingVD")
	public String claimingVD;
	@JsonProperty("Endorsements")
	public String endorsements;
	@JsonProperty("ExpiryDate")
	public String expiryDate;
	@JsonProperty("DateIssued")
	public String dateIssued;
	@JsonProperty("LicenseNo")
	public String licenseNo;
	@JsonProperty("GroupOrCategory")
	public String groupOrCategory;
	@JsonProperty("IndentificationVerified")
	public boolean indentificationVerified;
	@JsonProperty("InterpreterRequired")
	public String interpreterRequired;
	@JsonProperty("SpeakEnglish")
	public String speakEnglish;
	@JsonProperty("ReadEnglish")
	public String readEnglish;
	@JsonProperty("WriteEnglish")
	public String writeEnglish;
	@JsonProperty("FirstLanguage")
	public String firstLanguage;
	@JsonProperty("FirstLanguageSpeak")
	public String firstLanguageSpeak;
	@JsonProperty("FirstLanguageRead")
	public String firstLanguageRead;
	@JsonProperty("FirstLanguageWrite")
	public String firstLanguageWrite;
	@JsonProperty("FirstLanguageOther")
	public String firstLanguageOther;
	@JsonProperty("SecondLanguage")
	public String secondLanguage;
	@JsonProperty("SecondLanguageSpeak")
	public String secondLanguageSpeak;
	@JsonProperty("SecondLanguageRead")
	public String secondLanguageRead;
	@JsonProperty("SecondLanguageWrite")
	public String secondLanguageWrite;
	@JsonProperty("SecondLanguageOther")
	public String secondLanguageOther;
	@JsonProperty("ThirdLanguage")
	public String thirdLanguage;
	@JsonProperty("ThirdLanguageSpeak")
	public String thirdLanguageSpeak;
	@JsonProperty("ThirdLanguageRead")
	public String thirdLanguageRead;
	@JsonProperty("ThirdLanguageWrite")
	public String thirdLanguageWrite;
	@JsonProperty("ThirdLanguageOther")
	public String thirdLanguageOther;
	@JsonProperty("OwnerTitle")
	public String ownerTitle;
	@JsonProperty("OwnerForename")
	public String ownerForename;
	@JsonProperty("OwnerSurname")
	public String ownerSurname;
	@JsonProperty("OwnerAddress1")
	public String ownerAddress1;
	@JsonProperty("OwnerAddress2")
	public String ownerAddress2;
	@JsonProperty("OwnerAddress3")
	public String ownerAddress3;
	@JsonProperty("OwnerPostcode")
	public String ownerPostcode;
	@JsonProperty("OwnerTown")
	public String ownerTown;
	@JsonProperty("OwnerPhone")
	public String ownerPhone;
	@JsonProperty("OwnerWorkPhone")
	public String ownerWorkPhone;
	@JsonProperty("OwnerDOB")
	public String ownerDOB;
	@JsonProperty("OwnerEmail")
	public String ownerEmail;
	@JsonProperty("OwnerNI")
	public String ownerNI;
	@JsonProperty("OwnerOccupation")
	public String ownerOccupation;
	@JsonProperty("CLModel")
	public String clModel;
	@JsonProperty("CLMake")
	public String clMake;
	@JsonProperty("CLColour")
	public String clColour;
	@JsonProperty("CLEngineSize")
	public String clEngineSize;
	@JsonProperty("OccupantsIncludingDriver")
	public String occupantsIncludingDriver;
	@JsonProperty("ClaimantRegNo")
	public String claimantRegNo;
	@JsonProperty("ClientsVD")
	public String clientsVD;
	@JsonProperty("TotalLoss")
	public String totalLoss;
	@JsonProperty("VehicleBranding")
	public String vehicleBranding;
	@JsonProperty("VehicleType")
	public String vehicleType;
	@JsonProperty("LicencingAuthority")
	public String licencingAuthority;
	@JsonProperty("InsurerName")
	public String insurerName;
	@JsonProperty("InsurerAddress")
	public String insurerAddress;
	@JsonProperty("InsurerPostcode")
	public String insurerPostcode;
	@JsonProperty("InsurerEmailMain")
	public String insurerEmailMain;
	@JsonProperty("Reference")
	public String reference;
	@JsonProperty("CLPolicyNumber")
	public String clPolicyNumber;
	@JsonProperty("CoverType")
	public String coverType;
	@JsonProperty("PolicyExcess")
	public String policyExcess;
}
