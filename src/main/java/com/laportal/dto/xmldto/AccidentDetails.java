package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class AccidentDetails {
	@JsonProperty("AccidentDate")
	public String accidentDate;
	@JsonProperty("AccidentTime")
	public String accidentTime;
	@JsonProperty("FaultDetails")
	public String faultDetails;
	@JsonProperty("EveningOrDark")
	public String eveningOrDark;
	@JsonProperty("AccCircsHitFromSideRoad")
	public String accCircsHitFromSideRoad;
	@JsonProperty("AccCircsHitInRear")
	public String accCircsHitInRear;
	@JsonProperty("AccCircsHitWhilstParked")
	public String accCircsHitWhilstParked;
	@JsonProperty("AccCircsAccInCarPark")
	public String accCircsAccInCarPark;
	@JsonProperty("AccCircsOnRoundabout")
	public String accCircsOnRoundabout;
	@JsonProperty("AccCircsConcertinaCollision")
	public String accCircsConcertinaCollision;
	@JsonProperty("AccCircsAccInvChangingLane")
	public String accCircsAccInvChangingLane;
	@JsonProperty("AccCircsHitOther")
	public String accCircsHitOther;
	@JsonProperty("AccidentDescription")
	public String accidentDescription;
	@JsonProperty("ClaimantAtFault")
	public String claimantAtFault;
	@JsonProperty("ProspectOfSuccess")
	public String prospectOfSuccess;
	@JsonProperty("InvolveBusOrCoach")
	public Object involveBusOrCoach;
	@JsonProperty("Location")
	public String location;
	@JsonProperty("Town")
	public String town;
	@JsonProperty("AccidentMarker")
	public String accidentMarker;
	@JsonProperty("ReportedPolice")
	public String reportedPolice;
	@JsonProperty("ComingFrom")
	public String comingFrom;
	@JsonProperty("GoingTo")
	public String goingTo;
	@JsonProperty("SpeedOfClientVehicle")
	public String speedOfClientVehicle;
	@JsonProperty("SpeedOfDefendantsVehicle")
	public String speedOfDefendantsVehicle;
	@JsonProperty("WhoResponsibleAndWhy")
	public String whoResponsibleAndWhy;
	@JsonProperty("WeatherSun")
	public String weatherSun;
	@JsonProperty("WeatherRain")
	public String weatherRain;
	@JsonProperty("WeatherIce")
	public String weatherIce;
	@JsonProperty("WeatherSnow")
	public String weatherSnow;
	@JsonProperty("WeatherFog")
	public String weatherFog;
	@JsonProperty("IsWeatherOther")
	public String isWeatherOther;
	@JsonProperty("WeatherOther")
	public String weatherOther;
	@JsonProperty("RoadDry")
	public String roadDry;
	@JsonProperty("RoadWet")
	public String roadWet;
	@JsonProperty("RoadSnow")
	public String roadSnow;
	@JsonProperty("RoadOil")
	public String roadOil;
	@JsonProperty("RoadIce")
	public String roadIce;
	@JsonProperty("RoadMud")
	public String roadMud;
	@JsonProperty("IsRoadOther")
	public String isRoadOther;
	@JsonProperty("RoadOther")
	public String roadOther;
	@JsonProperty("Visibility")
	public String visibility;
}
