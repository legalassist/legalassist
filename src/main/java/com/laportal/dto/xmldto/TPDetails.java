package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class TPDetails {
	@JsonProperty("TPTitle")
	public String tpTitle;
	@JsonProperty("TPForename")
	public String tpForename;
	@JsonProperty("TPSurname")
	public String tpSurname;
	@JsonProperty("TPAddress")
	public String tpAddress;
	@JsonProperty("TPAddress2")
	public String tpAddress2;
	@JsonProperty("TPTown")
	public String tpTown;
	@JsonProperty("TPPostCode")
	public String tpPostCode;
	@JsonProperty("TPTelMain")
	public String tpTelMain;
	@JsonProperty("TPTelWork")
	public String tpTelWork;
	@JsonProperty("TPEmail")
	public String tpEmail;
	@JsonProperty("TPOwnerTitle")
	public String tpOwnerTitle;
	@JsonProperty("TPOwnerForename")
	public String tpOwnerForename;
	@JsonProperty("TPOwnerSurname")
	public String tpOwnerSurname;
	@JsonProperty("TPOwnerAddress")
	public String tpOwnerAddress;
	@JsonProperty("TPOwnerAddress2")
	public String tpOwnerAddress2;
	@JsonProperty("TPOwnerTown")
	public String tpOwnerTown;
	@JsonProperty("TPOwnerPostcode")
	public String tpOwnerPostcode;
	@JsonProperty("TPOwnerTelMain")
	public String tpOwnerTelMain;
	@JsonProperty("TPOwnerTelWork")
	public String tpOwnerTelWork;
	@JsonProperty("TPPolicyNumber")
	public String tpPolicyNumber;
	@JsonProperty("TPMake")
	public String tpMake;
	@JsonProperty("TPModel")
	public String tpModel;
	@JsonProperty("TPColour")
	public String tpColour;
	@JsonProperty("TPVRN")
	public String tpVRN;
	@JsonProperty("TPDamage")
	public String tpDamage;
	@JsonProperty("TPFuelType")
	public String tpFuelType;
	@JsonProperty("TPVin")
	public String tpVin;
	@JsonProperty("TPTransmission")
	public String tpTransmission;
	@JsonProperty("TPCylinderCapacity")
	public String tpCylinderCapacity;
	@JsonProperty("TPDateOfFirstRegistration")
	public Date tpDateOfFirstRegistration;
	@JsonProperty("TPMOTDetails")
	public String tpMOTDetails;
	@JsonProperty("TPRoadTaxDetails")
	public String tpRoadTaxDetails;
	@JsonProperty("TPNumberOfDoors")
	public String tpNumberOfDoors;
	@JsonProperty("TPCO20Emmision")
	public String tpCO20Emmision;
	@JsonProperty("TPRefNo")
	public String tpRefNo;
	@JsonProperty("TPClaimSolRef")
	public String tpClaimSolRef;
	@JsonProperty("TPClaimRef")
	public String tpClaimRef;
	@JsonProperty("TPNotes")
	public String tpNotes;
	@JsonProperty("TPDescriptionOfTheDriver")
	public String tpDescriptionOfTheDriver;
	@JsonProperty("TPOccupantsIncludingDriver")
	public String tpOccupantsIncludingDriver;
	@JsonProperty("Accidentdate")
	public Date accidentDate;
	@JsonProperty("TPGender")
	public String tpGender;
	@JsonProperty("TPAge")
	public String tpAge;
	@JsonProperty("TPHeight")
	public String tpHeight;
	@JsonProperty("TPBulid")
	public String tpBulid;
	@JsonProperty("TPDistinctFeatures")
	public String tpDistinctFeatures;
	@JsonProperty("TPEthnicity")
	public String tpEthnicity;
	@JsonProperty("TPHairColour")
	public String tpHairColour;
	@JsonProperty("TPInsName")
	public String tpInsName;
	@JsonProperty("TPInsAddress")
	public String tpInsAddress;
	@JsonProperty("TPInsPostcode")
	public String tpInsPostcode;
	@JsonProperty("TPInsEmail")
	public String tpInsEmail;
	@JsonProperty("TPInsTelMain")
	public String tpInsTelMain;
	@JsonProperty("tpsolName")
	public String tpsolName;
	@JsonProperty("tpsolAddress")
	public String tpsolAddress;
	@JsonProperty("tpsolPostcode")
	public String tpsolPostcode;
	@JsonProperty("tpsolEmail")
	public String tpsolEmail;
	@JsonProperty("tpsolTelMain")
	public String tpsolTelMain;
}
