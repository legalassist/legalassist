package com.laportal.dto.xmldto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClaimsCNFexport {

	@JsonProperty("Data")
	public Data data = new Data();

}
