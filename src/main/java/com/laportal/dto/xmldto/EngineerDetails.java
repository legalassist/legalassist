package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
@Getter
@Setter
public class EngineerDetails {
	@JsonProperty("Engineername")
	public String engineername;
	@JsonProperty("EngineerAddress")
	public String engineerAddress;
	@JsonProperty("EngineerPostCode")
	public String engineerPostCode;
	@JsonProperty("EngineerTelMain")
	public String engineerTelMain;
	@JsonProperty("EngineerEmail")
	public String engineerEmail;
	@JsonProperty("EngineerFax")
	public String engineerFax;
	@JsonProperty("InspectionRequired")
	public String inspectionRequired;
	@JsonProperty("InspectionDate")
	public Date inspectionDate;
	@JsonProperty("InspectionTime")
	public Date inspectionTime;
	@JsonProperty("InspectionBy")
	public String inspectionBy;
	@JsonProperty("ActualInspectionDate")
	public Date actualInspectionDate;
	@JsonProperty("VehicleLoaction")
	public String vehicleLoaction;
	@JsonProperty("EngineerNotes")
	public List<String> engineerNotes;
	@JsonProperty("TPInspectionRequired")
	public String tpInspectionRequired;
	@JsonProperty("TPInspectionDate")
	public Date tpInspectionDate;
	@JsonProperty("TPInspectionTime")
	public Date tpInspectionTime;
	@JsonProperty("TPInspectionBy")
	public String tpInspectionBy;
	@JsonProperty("EngineerContactName")
	public String engineerContactName;
	@JsonProperty("EngineerContactReference")
	public String engineerContactReference;
	@JsonProperty("EngineerContactTelMain")
	public String engineerContactTelMain;
	@JsonProperty("EngineerContactEmail")
	public String engineerContactEmail;
}
