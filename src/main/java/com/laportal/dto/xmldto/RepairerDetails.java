package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RepairerDetails {
	@JsonProperty("RepairDetails")
	public String repairDetails;
	@JsonProperty("NewParts")
	public String newParts;
	@JsonProperty("HourlyRate")
	public String hourlyRate;
	@JsonProperty("RepairNotes")
	public String repairNotes;
	@JsonProperty("RepairOthers")
	public String repairOthers;
	@JsonProperty("vatlabour")
	public String vatlabour;
	@JsonProperty("Paint")
	public String paint;
	@JsonProperty("VAtPaint")
	public String vatPaint;
	@JsonProperty("RepairOthervat")
	public String repairOthervat;
	@JsonProperty("vatNewparts")
	public String vatNewparts;
	@JsonProperty("RepairSubTotal")
	public String repairSubTotal;
	@JsonProperty("RepairTotal")
	public String repairTotal;
	@JsonProperty("RepairTotalHours")
	public String repairTotalHours;
	@JsonProperty("TotalLabour")
	public String totalLabour;
	@JsonProperty("RepairVat")
	public String repairVat;
	@JsonProperty("RepairStart")
	public String repairStart;
	@JsonProperty("RepairEnd")
	public String repairEnd;
	@JsonProperty("Repname")
	public String repname;
	@JsonProperty("RepAddress")
	public String repAddress;
	@JsonProperty("RepPostCode")
	public String repPostCode;
	@JsonProperty("RepTelMain")
	public String repTelMain;
	@JsonProperty("RepEmail")
	public String repEmail;
	@JsonProperty("RepFax")
	public String repFax;
}
