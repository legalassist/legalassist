package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class MedicalDetails {
	@JsonProperty("MedDoctorName")
	public String medDoctorName;
	@JsonProperty("MedNotes")
	public Object medNotes;
	@JsonProperty("MedicalAttended")
	public String medicalAttended;
	@JsonProperty("MedicalAppointment")
	public String medicalAppointment;
	@JsonProperty("MedicalTime")
	public String medicalTime;
	@JsonProperty("MedicalAddress")
	public List<Object> medicalAddress;
	@JsonProperty("InterpreterLanguage")
	public String interpreterLanguage;
	@JsonProperty("InterpreteReq")
	public String interpreteReq;
	@JsonProperty("ExpertType")
	public String expertType;
	@JsonProperty("Psychologistdate")
	public String psychologistdate;
	@JsonProperty("PsychologistTime")
	public String psychologistTime;
	@JsonProperty("PsychologistName")
	public String psychologistName;
	@JsonProperty("PsychologistLocation")
	public String psychologistLocation;
	@JsonProperty("MedicalPaid")
	public String medicalPaid;
	@JsonProperty("MedicalPaidDate")
	public String medicalPaidDate;
	@JsonProperty("Medicalname")
	public String medicalname;
	@JsonProperty("MedicalPostCode")
	public String medicalPostCode;
	@JsonProperty("MedicalTelMain")
	public String medicalTelMain;
	@JsonProperty("MedicalEmail")
	public String medicalEmail;
	@JsonProperty("MedicalFax")
	public String medicalFax;
}
