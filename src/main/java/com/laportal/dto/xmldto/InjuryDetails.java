package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InjuryDetails {
	@JsonProperty("SoftTissueInjury")
	public String softTissueInjury;
	@JsonProperty("WhiplashInjury")
	public String whiplashInjury;
	@JsonProperty("BoneInjury")
	public String boneInjury;
	@JsonProperty("OtherInjury")
	public String otherInjury;
	@JsonProperty("WearingSeatbelt")
	public String wearingSeatbelt;
	@JsonProperty("InjuryDescription")
	public String injuryDescription;
	@JsonProperty("InternalNotes")
	public String internalNotes;
	@JsonProperty("TimeOffWork")
	public String timeOffWork;
	@JsonProperty("StillOffWork")
	public String stillOffWork;
	@JsonProperty("HowLongOffWork")
	public String howLongOffWork;
	@JsonProperty("HowLongLast")
	public String howLongLast;
	@JsonProperty("InjuryResolved")
	public String injuryResolved;
	@JsonProperty("SymptomsOnGoing")
	public String symptomsOnGoing;
	@JsonProperty("LossofEarnings")
	public String lossofEarnings;
	@JsonProperty("IsHead")
	public String isHead;
	@JsonProperty("IsArm")
	public String isArm;
	@JsonProperty("IsFinger")
	public String isFinger;
	@JsonProperty("IsHip")
	public String isHip;
	@JsonProperty("IsShin")
	public String isShin;
	@JsonProperty("IsFace")
	public String isFace;
	@JsonProperty("IsUpperBack")
	public String isUpperBack;
	@JsonProperty("IsElbow")
	public String isElbow;
	@JsonProperty("IsGroin")
	public String isGroin;
	@JsonProperty("IsAnkle")
	public String isAnkle;
	@JsonProperty("IsNeck")
	public String isNeck;
	@JsonProperty("IsWrist")
	public String isWrist;
	@JsonProperty("IsLowerBack")
	public String isLowerBack;
	@JsonProperty("IsThigh")
	public String isThigh;
	@JsonProperty("IsFoot")
	public String isFoot;
	@JsonProperty("IsShoulder")
	public String isShoulder;
	@JsonProperty("IsHand")
	public String isHand;
	@JsonProperty("IsChest")
	public String isChest;
	@JsonProperty("IsKnee")
	public String isKnee;
	@JsonProperty("IsOther")
	public String isOther;
	@JsonProperty("EmployeementStatus")
	public String employeementStatus;
	@JsonProperty("EmployerCompanyName")
	public String employerCompanyName;
	@JsonProperty("EmploymentDepartment")
	public String employmentDepartment;
	@JsonProperty("EmployerAddressBlock")
	public String employerAddressBlock;
	@JsonProperty("EmployerAddress2")
	public String employerAddress2;
	@JsonProperty("EmployerPostcode")
	public String employerPostcode;
	@JsonProperty("EmployerTelMain")
	public String employerTelMain;
	@JsonProperty("EmployerFax")
	public String employerFax;
	@JsonProperty("SeekMedicalAttention")
	public String seekMedicalAttention;
	@JsonProperty("WhenSeekMedicalAttention")
	public String whenSeekMedicalAttention;
	@JsonProperty("HospitalAttended")
	public String hospitalAttended;
	@JsonProperty("HospitalDetails")
	public Object hospitalDetails;
	@JsonProperty("HospitalCorrespondentId")
	public String hospitalCorrespondentId;
	@JsonProperty("HospitalAttendedDate")
	public String hospitalAttendedDate;
	@JsonProperty("DetainedOvernight")
	public String detainedOvernight;
	@JsonProperty("DaysDetained")
	public String daysDetained;
	@JsonProperty("HospitalType")
	public String hospitalType;
	@JsonProperty("GPDoctorName")
	public String gpDoctorName;
	@JsonProperty("GPNumberOfTimes")
	public String gpNumberOfTimes;
	@JsonProperty("GPCorrespondentId")
	public String gpCorrespondentId;
	@JsonProperty("GPVisited")
	public String gpVisited;
	@JsonProperty("GPVisitedDate")
	public String gpVisitedDate;
	@JsonProperty("Notes")
	public String notes;
	@JsonProperty("RehabRecommended")
	public String rehabRecommended;
	@JsonProperty("RehabRecommendedDetails")
	public String rehabRecommendedDetails;
	@JsonProperty("RehabNeeds")
	public String rehabNeeds;
	@JsonProperty("RehabNeedsDetails")
	public String rehabNeedsDetails;
}
