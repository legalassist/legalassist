package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class StorageDetails {
	@JsonProperty("IsStorage")
	public String isStorage;
	@JsonProperty("IsRecovery")
	public String isRecovery;
	@JsonProperty("RecoveredTo")
	public String recoveredTo;
	@JsonProperty("RecoveredFrom")
	public String recoveredFrom;
	@JsonProperty("TransportationOrRecovery")
	public String transportationOrRecovery;
	@JsonProperty("RecoveredCompany")
	public String recoveredCompany;
	@JsonProperty("RecoveredNotes")
	public String recoveredNotes;
	@JsonProperty("RecoveredDateIn")
	public Date recoveredDateIn;
	@JsonProperty("RecoveredDateOut")
	public Date recoveredDateOut;
	@JsonProperty("RecoveredAdmin")
	public String recoveredAdmin;
	@JsonProperty("RecoveredOther")
	public String recoveredOther;
	@JsonProperty("Specialist")
	public String specialist;
	@JsonProperty("DailyRate")
	public String dailyRate;
	@JsonProperty("InspectionFunctionFee")
	public String inspectionFunctionFee;
	@JsonProperty("InspectionFunctionFeeUnits")
	public String inspectionFunctionFeeUnits;
	@JsonProperty("RecoveredTotalDays")
	public String recoveredTotalDays;
	@JsonProperty("RecoveryDate")
	public Date recoveryDate;
	@JsonProperty("Recovery1")
	public String recovery1;
	@JsonProperty("RecoveryInvoiceWith")
	public String recoveryInvoiceWith;
	@JsonProperty("RecoveryInvoiceVAT")
	public String recoveryInvoiceVAT;
	@JsonProperty("RecoveryVAT1")
	public String recoveryVAT1;
	@JsonProperty("RecoveryInvoiceWithout")
	public String recoveryInvoiceWithout;
	@JsonProperty("StorageCompanyName")
	public String storageCompanyName;
	@JsonProperty("StorageAddress")
	public String storageAddress;
	@JsonProperty("StoragePostcode")
	public String storagePostcode;
	@JsonProperty("StorageCompanyTelMain")
	public String storageCompanyTelMain;
	@JsonProperty("StorageCompanyEmail")
	public String storageCompanyEmail;
	@JsonProperty("StorageCompanyFax")
	public String storageCompanyFax;
}
