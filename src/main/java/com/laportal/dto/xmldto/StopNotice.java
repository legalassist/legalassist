package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StopNotice {
	@JsonProperty("StopNoticeDone")
	public String stopNoticeDone;
	@JsonProperty("InspectionSite")
	public String inspectionSite;
	@JsonProperty("StopNoticeDocumentName")
	public String stopNoticeDocumentName;
}
