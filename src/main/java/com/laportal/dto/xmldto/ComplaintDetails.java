package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class ComplaintDetails {
	@JsonProperty("ComDateOfComplaint")
	public Date comDateOfComplaint;
	@JsonProperty("ComTimeOfComplaint")
	public String comTimeOfComplaint;
	@JsonProperty("ComCategory")
	public String comCategory;
	@JsonProperty("ComIndividualOrCompany")
	public String comIndividualOrCompany;
	@JsonProperty("ComMadeBy")
	public String comMadeBy;
	@JsonProperty("ComFileHandler")
	public String comFileHandler;
	@JsonProperty("ComComplainDetails")
	public String comComplainDetails;
	@JsonProperty("ComStatus")
	public String comStatus;
	@JsonProperty("ComOpenedBy")
	public String comOpenedBy;
}
