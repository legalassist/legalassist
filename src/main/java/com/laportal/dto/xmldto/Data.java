package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
public class Data {
	@JsonProperty("SolicitorRef")
	public String solicitorRef;
	@JsonProperty("SourceRef")
	public String sourceRef;
	@JsonProperty("ClaimsID")
	public String claimsID;
	@JsonProperty("FileHandler")
	public String fileHandler;
	@JsonProperty("Referrer")
	public String referrer;
	@JsonProperty("CreditControl")
	public String creditControl;

	@JsonProperty("ClaimantDetails")
	public ClaimantDetails claimantDetails = new ClaimantDetails();
	@JsonProperty("HistoryDetails")
	public List<HistoryDetails> historyDetails = new ArrayList<>();
	@JsonProperty("TPDetails")
	public TPDetails tpDetails = new TPDetails();
	@JsonProperty("AccidentDetails")
	public AccidentDetails accidentDetails = new AccidentDetails();
	@JsonProperty("InjuryDetails")
	public InjuryDetails injuryDetails = new InjuryDetails();
	@JsonProperty("WitnessDetails")
	public List<WitnessDetails> witnessDetails = new ArrayList<>();
	@JsonProperty("PoliceAndCCTVDetails")
	public PoliceAndCCTVDetails policeAndCCTVDetails = new PoliceAndCCTVDetails();
	@JsonProperty("EmployeeDetails")
	public EmployeeDetails employeeDetails = new EmployeeDetails();
	@JsonProperty("InsVDetails")
	public InsVDetails insVDetails = new InsVDetails();
	@JsonProperty("HireDetails")
	public List<HireDetails> hireDetails = new ArrayList<>();
	@JsonProperty("HireOutSource")
	public HireOutSource hireOutSource = new HireOutSource();
	@JsonProperty("StorageDetails")
	public StorageDetails storageDetails = new StorageDetails();
	@JsonProperty("EngineerDetails")
	public EngineerDetails engineerDetails = new EngineerDetails();
	@JsonProperty("AgreeAmountDetails")
	public String agreeAmountDetails = new String();
	@JsonProperty("VehichleDetails2")
	public VehichleDetails2 vehichleDetails2 = new VehichleDetails2();
	@JsonProperty("RepairerDetails")
	public RepairerDetails repairerDetails = new RepairerDetails();
	@JsonProperty("MedicalDetails")
	public MedicalDetails medicalDetails = new MedicalDetails();
	@JsonProperty("LitigationDetails")
	public LitigationDetails litigationDetails = new LitigationDetails();
	@JsonProperty("InitialAssessment")
	public InitialAssessment initialAssessment = new InitialAssessment();
	@JsonProperty("VehicleDamageDetails")
	public VehicleDamageDetails vehicleDamageDetails = new VehicleDamageDetails();
	@JsonProperty("SolicitorDetails")
	public SolicitorDetails solicitorDetails = new SolicitorDetails();
	@JsonProperty("LouDetails")
	public String louDetails;
	@JsonProperty("TaxiDetails")
	public TaxiDetails taxiDetails = new TaxiDetails();
	@JsonProperty("ReminderDetails")
	public String reminderDetails;
	@JsonProperty("SignupAgent")
	public SignupAgent signupAgent = new SignupAgent();
	@JsonProperty("AdminDetails")
	public AdminDetails adminDetails = new AdminDetails();
	@JsonProperty("ATEDetails")
	public ATEDetails ateDetails = new ATEDetails();
	@JsonProperty("BusDetails")
	public BusDetails busDetails = new BusDetails();
	@JsonProperty("ExpenseTypeDetails")
	public List<ExpenseTypeDetails> expenseTypeDetails = new ArrayList<>();
	@JsonProperty("StopNotice")
	public StopNotice stopNotice = new StopNotice();
	@JsonProperty("ComplaintDetails")
	public ComplaintDetails complaintDetails = new ComplaintDetails();

}
