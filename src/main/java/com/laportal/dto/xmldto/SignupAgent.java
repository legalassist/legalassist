package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignupAgent {
	@JsonProperty("SignupAgentId")
	public Object signupAgentId;
}
