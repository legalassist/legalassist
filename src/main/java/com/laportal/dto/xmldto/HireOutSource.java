package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class HireOutSource {
	@JsonProperty("HCommission")
	public String hCommission;
	@JsonProperty("HTotal")
	public String hTotal;
	@JsonProperty("HNotes")
	public String hNotes;
	@JsonProperty("HStartTime")
	public Object hStartTime;
	@JsonProperty("HEndTime")
	public Object hEndTime;
	@JsonProperty("HDailyRate")
	public String hDailyRate;
	@JsonProperty("HStartDate")
	public String hStartDate;
	@JsonProperty("HEndDate")
	public Date hEndDate;
	@JsonProperty("HCompanyname")
	public String hCompanyname;
	@JsonProperty("HCompanyAddress")
	public String hCompanyAddress;
	@JsonProperty("HCompanyPostCode")
	public String hCompanyPostCode;
	@JsonProperty("HCompanyTelMain")
	public String hCompanyTelMain;
	@JsonProperty("HCompanyEmail")
	public String hCompanyEmail;
	@JsonProperty("HCompanyFax")
	public String hCompanyFax;
}
