package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class InsVDetails {
	@JsonProperty("InsLEI")
	public String insLEI;
	@JsonProperty("InsFuelType")
	public String insFuelType;
	@JsonProperty("InsVIN")
	public String insVIN;
	@JsonProperty("InsTransmission")
	public String insTransmission;
	@JsonProperty("InsCylinderCapacity")
	public String insCylinderCapacity;
	@JsonProperty("InsDFR")
	public Date insDFR;
	@JsonProperty("InsMOTDetails")
	public String insMOTDetails;
	@JsonProperty("InsRoadTaxDetails")
	public String insRoadTaxDetails;
	@JsonProperty("InsNumberOfDoors")
	public String insNumberOfDoors;
	@JsonProperty("InsCO20Emmision")
	public String insCO20Emmision;
	@JsonProperty("InsApproxValue")
	public String insApproxValue;
	@JsonProperty("InsExcessAmount")
	public String insExcessAmount;
	@JsonProperty("InsVehicleFinanced")
	public String insVehicleFinanced;
	@JsonProperty("InsAnyModification")
	public String insAnyModification;
	@JsonProperty("InsRampFitted")
	public String insRampFitted;
	@JsonProperty("InsCorName")
	public String insCorName;
	@JsonProperty("InsCorAddress")
	public String insCorAddress;
	@JsonProperty("InsCorPostCode")
	public String insCorPostCode;
	@JsonProperty("InsCorTelMain")
	public String insCorTelMain;
	@JsonProperty("InsCorEmail")
	public String insCorEmail;
	@JsonProperty("InsCorFax")
	public String insCorFax;
}
