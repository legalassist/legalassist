package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WitnessDetails {
	@JsonProperty("WitnessTitle")
	public String witnessTitle;
	@JsonProperty("WitnessForename")
	public String witnessForename;
	@JsonProperty("WitnessSurname")
	public String witnessSurname;
	@JsonProperty("WitnessAddress")
	public String witnessAddress;
	@JsonProperty("WitnessAddress2")
	public String witnessAddress2;
	@JsonProperty("WitnessTown")
	public String witnessTown;
	@JsonProperty("WitnessPostCode")
	public String witnessPostCode;
	@JsonProperty("WitnessTelMain")
	public String witnessTelMain;
	@JsonProperty("WitnessTelMobile")
	public String witnessTelMobile;
	@JsonProperty("WitnessEmailMain")
	public String witnessEmailMain;
	@JsonProperty("WitnessRegNo")
	public String witnessRegNo;
	@JsonProperty("WitnessMake")
	public String witnessMake;
	@JsonProperty("WitnessModel")
	public String witnessModel;
	@JsonProperty("WitnessColour")
	public String witnessColour;
	@JsonProperty("WitnessInsPolicyNo")
	public String witnessInsPolicyNo;
	@JsonProperty("WitnessInsCompanyName")
	public String witnessInsCompanyName;
	@JsonProperty("WitnessInsAddress")
	public String witnessInsAddress;
	@JsonProperty("WitnessInsAddress2")
	public String witnessInsAddress2;
	@JsonProperty("WitnessInsTown")
	public String witnessInsTown;
	@JsonProperty("WitnessInsPostCode")
	public String witnessInsPostCode;
	@JsonProperty("WitnessNotes")
	public String witnessNotes;
	@JsonProperty("WitnessIsWitness")
	public boolean witnessIsWitness;
	@JsonProperty("WitnessIndependant")
	public boolean witnessIndependant;
}
