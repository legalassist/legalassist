package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ATEDetails {
	@JsonProperty("ATEName")
	public String ateName;
	@JsonProperty("ATEAddress")
	public String ateAddress;
	@JsonProperty("ATEPostCode")
	public String atePostCode;
	@JsonProperty("ATETel")
	public String ateTel;
	@JsonProperty("ATEFax")
	public String ateFax;
	@JsonProperty("ATEEmail")
	public String ateEmail;
	@JsonProperty("ATEPaid")
	public String atePaid;
	@JsonProperty("ATEPaidDate")
	public String atePaidDate;
}
