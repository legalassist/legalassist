package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class ExpenseTypeDetails {
	@JsonProperty("ExpenseType")
	public String expenseType;
	@JsonProperty("ExpenseAmount")
	public String expenseAmount;
	@JsonProperty("ExpenseDate")
	public Date expenseDate;
	@JsonProperty("ExpenseNotes")
	public String expenseNotes;
}
