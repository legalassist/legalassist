package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminDetails {
	@JsonProperty("CommisionPaid")
	public String commisionPaid;
	@JsonProperty("ClawBack")
	public String clawBack;
	@JsonProperty("InvoiceSent")
	public String invoiceSent;
	@JsonProperty("PaymentRecieved")
	public String paymentRecieved;
	@JsonProperty("AdminOnly")
	public String adminOnly;
	@JsonProperty("AdminNotes")
	public String adminNotes;
}
