package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusDetails {
	@JsonProperty("DriverDescription")
	public String driverDescription;
	@JsonProperty("VehicleDescription")
	public String vehicleDescription;
	@JsonProperty("IsEvidence")
	public String isEvidence;
	@JsonProperty("Evidence")
	public String evidence;
}
