package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class TaxiDetails {
	@JsonProperty("TaxiOtherInfo")
	public String taxiOtherInfo;
	@JsonProperty("TaxiLicenceStart")
	public Date taxiLicenceStart;
	@JsonProperty("TaxiLienceEnd")
	public Date taxiLienceEnd;
	@JsonProperty("TaxiBadgeNo")
	public String taxiBadgeNo;
	@JsonProperty("TaxiLicenceAuth")
	public String taxiLicenceAuth;
	@JsonProperty("TaxiPlateNo")
	public String taxiPlateNo;
	@JsonProperty("TaxiSelfEmployed")
	public String taxiSelfEmployed;
	@JsonProperty("TaxiDss")
	public String taxiDss;
}