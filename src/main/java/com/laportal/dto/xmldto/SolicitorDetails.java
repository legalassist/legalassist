package com.laportal.dto.xmldto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SolicitorDetails {
	@JsonProperty("InjSolRef")
	public String injSolRef;
	@JsonProperty("PortalRef")
	public String portalRef;
	@JsonProperty("DealingDirect")
	public String dealingDirect;
	@JsonProperty("SolicitorInstructed")
	public String solicitorInstructed;
	@JsonProperty("SolicitorCorrespondentID")
	public String solicitorCorrespondentID;
	@JsonProperty("SolFileHandlerID")
	public String solFileHandlerID;
}
