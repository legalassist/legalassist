package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

public class AddNewPassengersRtaCase {

    private String rtacode;
    private List<RtaPassengerRequest> passengers = new ArrayList<>();

    public String getRtacode() {
        return rtacode;
    }

    public void setRtacode(String rtacode) {
        this.rtacode = rtacode;
    }

    public List<RtaPassengerRequest> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<RtaPassengerRequest> passengers) {
        this.passengers = passengers;
    }
}
