package com.laportal.dto;

public class ResendHdrMessageDto {


    private String hdrmessagecode;

    public String getHdrmessagecode() {
        return hdrmessagecode;
    }

    public void setHdrmessagecode(String hdrmessagecode) {
        this.hdrmessagecode = hdrmessagecode;
    }
}
