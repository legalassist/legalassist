package com.laportal.dto;

import java.util.Date;

public class UpdateTblHdrjointtenancy {

    private long hdrjointtenancycode;

    private String taddress1;

    private String taddress2;

    private String taddress3;

    private String tcity;


    private Date tDob;

    private String tEmail;

    private String tFname;

    private String tLandline;

    private String tMname;

    private String tMobileno;

    private String tNinumber;

    private String tpostalcode;

    private String tregion;

    private String tSname;

    private String tTitle;

    public String getTaddress1() {
        return taddress1;
    }

    public void setTaddress1(String taddress1) {
        this.taddress1 = taddress1;
    }

    public String getTaddress2() {
        return taddress2;
    }

    public void setTaddress2(String taddress2) {
        this.taddress2 = taddress2;
    }

    public String getTaddress3() {
        return taddress3;
    }

    public void setTaddress3(String taddress3) {
        this.taddress3 = taddress3;
    }

    public String getTcity() {
        return tcity;
    }

    public void setTcity(String tcity) {
        this.tcity = tcity;
    }

    public Date gettDob() {
        return tDob;
    }

    public void settDob(Date tDob) {
        this.tDob = tDob;
    }

    public String gettEmail() {
        return tEmail;
    }

    public void settEmail(String tEmail) {
        this.tEmail = tEmail;
    }

    public String gettFname() {
        return tFname;
    }

    public void settFname(String tFname) {
        this.tFname = tFname;
    }

    public String gettLandline() {
        return tLandline;
    }

    public void settLandline(String tLandline) {
        this.tLandline = tLandline;
    }

    public String gettMname() {
        return tMname;
    }

    public void settMname(String tMname) {
        this.tMname = tMname;
    }

    public String gettMobileno() {
        return tMobileno;
    }

    public void settMobileno(String tMobileno) {
        this.tMobileno = tMobileno;
    }

    public String gettNinumber() {
        return tNinumber;
    }

    public void settNinumber(String tNinumber) {
        this.tNinumber = tNinumber;
    }

    public String getTpostalcode() {
        return tpostalcode;
    }

    public void setTpostalcode(String tpostalcode) {
        this.tpostalcode = tpostalcode;
    }

    public String getTregion() {
        return tregion;
    }

    public void setTregion(String tregion) {
        this.tregion = tregion;
    }

    public String gettSname() {
        return tSname;
    }

    public void settSname(String tSname) {
        this.tSname = tSname;
    }

    public String gettTitle() {
        return tTitle;
    }

    public void settTitle(String tTitle) {
        this.tTitle = tTitle;
    }

    public long getHdrjointtenancycode() {
        return hdrjointtenancycode;
    }

    public void setHdrjointtenancycode(long hdrjointtenancycode) {
        this.hdrjointtenancycode = hdrjointtenancycode;
    }
}