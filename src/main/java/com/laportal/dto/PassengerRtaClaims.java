package com.laportal.dto;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 10/25/2022
 * Time: 10:40 PM
 * Project : legalassist
 */
public class PassengerRtaClaims {

    private String rtacode;
    private String name;
    private String rtanumber;
    private String status;

    public String getRtacode() {
        return rtacode;
    }

    public void setRtacode(String rtacode) {
        this.rtacode = rtacode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRtanumber() {
        return rtanumber;
    }

    public void setRtanumber(String rtanumber) {
        this.rtanumber = rtanumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
