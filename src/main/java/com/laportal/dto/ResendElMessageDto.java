package com.laportal.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ResendElMessageDto {


    @NotNull
    @NotEmpty
    private String elmessagecode;

    public String getElmessagecode() {
        return elmessagecode;
    }

    public void setElmessagecode(String elmessagecode) {
        this.elmessagecode = elmessagecode;
    }
}
