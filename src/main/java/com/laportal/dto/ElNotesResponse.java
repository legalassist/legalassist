package com.laportal.dto;

import com.laportal.model.TblElnote;
import com.laportal.model.TblRtanote;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 1/25/2024
 * Time: 5:06 PM
 * Project : legalassist
 */
@Getter
@Setter
public class ElNotesResponse {

    private List<TblElnote> tblElnotes = new ArrayList<>();
    private List<TblElnote> tblElnotesLegalOnly = new ArrayList<>();
}
