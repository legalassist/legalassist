package com.laportal.dto;

public class AssignElCasetoSolicitor {

    private String elClaimCode;
    private String solicitorCode;
    private String solicitorUserCode;

    public String getElClaimCode() {
        return elClaimCode;
    }

    public void setElClaimCode(String elClaimCode) {
        this.elClaimCode = elClaimCode;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public String getSolicitorUserCode() {
        return solicitorUserCode;
    }

    public void setSolicitorUserCode(String solicitorUserCode) {
        this.solicitorUserCode = solicitorUserCode;
    }
}
