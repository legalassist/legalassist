package com.laportal.dto;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 5/26/2023
 * Time: 4:18 PM
 * Project : legalassist
 */
public class ManualInvoiceLineRequest {

    private Long adjust;

    private Long amount;

    private Long charge;

    private String description;

    private long invoiceheadid;

    public Long getAdjust() {
        return adjust;
    }

    public void setAdjust(Long adjust) {
        this.adjust = adjust;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getCharge() {
        return charge;
    }

    public void setCharge(Long charge) {
        this.charge = charge;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getInvoiceheadid() {
        return invoiceheadid;
    }

    public void setInvoiceheadid(long invoiceheadid) {
        this.invoiceheadid = invoiceheadid;
    }
}
