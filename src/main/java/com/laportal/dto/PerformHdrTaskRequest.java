package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

public class PerformHdrTaskRequest {

    private long taskCode;
    private long hdrClaimCode;
    private List<RtaFileUploadRequest> files = new ArrayList<>();

    public long getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(long taskCode) {
        this.taskCode = taskCode;
    }

    public long getHdrClaimCode() {
        return hdrClaimCode;
    }

    public void setHdrClaimCode(long hdrClaimCode) {
        this.hdrClaimCode = hdrClaimCode;
    }

    public List<RtaFileUploadRequest> getFiles() {
        return files;
    }

    public void setFiles(List<RtaFileUploadRequest> files) {
        this.files = files;
    }
}
