package com.laportal.dto;

public class UpdateRtaFileUploadRequest {

    private long rtadoccode;

    private String fileName;

    private String fileBase64;

    private String filedescr;

    public long getRtadoccode() {
        return rtadoccode;
    }

    public void setRtadoccode(long rtadoccode) {
        this.rtadoccode = rtadoccode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileBase64() {
        return fileBase64;
    }

    public void setFileBase64(String fileBase64) {
        this.fileBase64 = fileBase64;
    }

    public String getFiledescr() {
        return filedescr;
    }

    public void setFiledescr(String filedescr) {
        this.filedescr = filedescr;
    }
}
