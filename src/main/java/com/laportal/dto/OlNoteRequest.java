package com.laportal.dto;

public class OlNoteRequest {

    private String olCode;

    private String note;

    private String userCatCode;

    public String getOlCode() {
        return olCode;
    }

    public void setOlCode(String olCode) {
        this.olCode = olCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserCatCode() {
        return userCatCode;
    }

    public void setUserCatCode(String userCatCode) {
        this.userCatCode = userCatCode;
    }
}
