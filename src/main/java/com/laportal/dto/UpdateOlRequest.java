package com.laportal.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


@Getter
@Setter
public class UpdateOlRequest {

    private long olclaimcode;
    private Date accdatetime;
    private String accdescription;
    private String acclocation;
    private String accreportedto;
    private String describeinjuries;
    private Date dob;
    private String email;
    private String injuriesduration;
    private String landline;
    private String medicalattention;
    private String mobile;
    private String ninumber;
    private String occupation;
    private String password;
    private String remarks;
    private BigDecimal status;
    private String witnesses;
    private String title;
    private String firstname;
    private String middlename;
    private String lastname;
    private String postalcode;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String region;
    private String acctime;
    private String anywitnesses;
}
