package com.laportal.dto;

import java.math.BigDecimal;
import java.util.Date;

public class SavePcpRequest {

    private String addressOfClient;
    private String brokeraddress;
    private String brokername;
    private BigDecimal cashDeposit;
    private BigDecimal chargeForCredit;
    private String contactNo;
    private Date contractDate;
    private String contractualApr;
    private BigDecimal contractualMonthlyPayment;
    private String dealeraddress;
    private String dealername;
    private Date dob;
    private String emailAddress;
    private String esig;
    private Date esigdate;
    private BigDecimal fcaNumber;
    private String fullName;
    private String lenderaddress;
    private String lendername;
    private BigDecimal lowestAdvertisedRate;
    private String makemodel;
    private String niNumber;
    private String optionToPurchaseFee;
    private String partExchange;
    private Date purchaseDate;
    private BigDecimal purchasePrice;
    private String registration;
    private String remarks;
    private BigDecimal setupFees;
    private BigDecimal totalPayable;
    private String contractualTerm;
    private String endOfAgreement;

    public String getAddressOfClient() {
        return addressOfClient;
    }

    public void setAddressOfClient(String addressOfClient) {
        this.addressOfClient = addressOfClient;
    }

    public String getBrokeraddress() {
        return brokeraddress;
    }

    public void setBrokeraddress(String brokeraddress) {
        this.brokeraddress = brokeraddress;
    }

    public String getBrokername() {
        return brokername;
    }

    public void setBrokername(String brokername) {
        this.brokername = brokername;
    }

    public BigDecimal getCashDeposit() {
        return cashDeposit;
    }

    public void setCashDeposit(BigDecimal cashDeposit) {
        this.cashDeposit = cashDeposit;
    }

    public BigDecimal getChargeForCredit() {
        return chargeForCredit;
    }

    public void setChargeForCredit(BigDecimal chargeForCredit) {
        this.chargeForCredit = chargeForCredit;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public String getContractualApr() {
        return contractualApr;
    }

    public void setContractualApr(String contractualApr) {
        this.contractualApr = contractualApr;
    }

    public BigDecimal getContractualMonthlyPayment() {
        return contractualMonthlyPayment;
    }

    public void setContractualMonthlyPayment(BigDecimal contractualMonthlyPayment) {
        this.contractualMonthlyPayment = contractualMonthlyPayment;
    }

    public String getDealeraddress() {
        return dealeraddress;
    }

    public void setDealeraddress(String dealeraddress) {
        this.dealeraddress = dealeraddress;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEsig() {
        return esig;
    }

    public void setEsig(String esig) {
        this.esig = esig;
    }

    public Date getEsigdate() {
        return esigdate;
    }

    public void setEsigdate(Date esigdate) {
        this.esigdate = esigdate;
    }

    public BigDecimal getFcaNumber() {
        return fcaNumber;
    }

    public void setFcaNumber(BigDecimal fcaNumber) {
        this.fcaNumber = fcaNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLenderaddress() {
        return lenderaddress;
    }

    public void setLenderaddress(String lenderaddress) {
        this.lenderaddress = lenderaddress;
    }

    public String getLendername() {
        return lendername;
    }

    public void setLendername(String lendername) {
        this.lendername = lendername;
    }

    public BigDecimal getLowestAdvertisedRate() {
        return lowestAdvertisedRate;
    }

    public void setLowestAdvertisedRate(BigDecimal lowestAdvertisedRate) {
        this.lowestAdvertisedRate = lowestAdvertisedRate;
    }

    public String getMakemodel() {
        return makemodel;
    }

    public void setMakemodel(String makemodel) {
        this.makemodel = makemodel;
    }

    public String getNiNumber() {
        return niNumber;
    }

    public void setNiNumber(String niNumber) {
        this.niNumber = niNumber;
    }

    public String getOptionToPurchaseFee() {
        return optionToPurchaseFee;
    }

    public void setOptionToPurchaseFee(String optionToPurchaseFee) {
        this.optionToPurchaseFee = optionToPurchaseFee;
    }

    public String getPartExchange() {
        return partExchange;
    }

    public void setPartExchange(String partExchange) {
        this.partExchange = partExchange;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public BigDecimal getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(BigDecimal purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getSetupFees() {
        return setupFees;
    }

    public void setSetupFees(BigDecimal setupFees) {
        this.setupFees = setupFees;
    }

    public BigDecimal getTotalPayable() {
        return totalPayable;
    }

    public void setTotalPayable(BigDecimal totalPayable) {
        this.totalPayable = totalPayable;
    }

    public String getContractualTerm() {
        return contractualTerm;
    }

    public void setContractualTerm(String contractualTerm) {
        this.contractualTerm = contractualTerm;
    }

    public String getEndOfAgreement() {
        return endOfAgreement;
    }

    public void setEndOfAgreement(String endOfAgreement) {
        this.endOfAgreement = endOfAgreement;
    }
}
