package com.laportal.dto;

public class RtaFileResponse {


    private String fileName;

    private String fileBase64;

    private String filedescr;

    private String fileType;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileBase64() {
        return fileBase64;
    }

    public void setFileBase64(String fileBase64) {
        this.fileBase64 = fileBase64;
    }

    public String getFiledescr() {
        return filedescr;
    }

    public void setFiledescr(String filedescr) {
        this.filedescr = filedescr;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
