package com.laportal.dto;

import java.util.Date;

public class SaveTblHdrclaimant {

    private String caddress1;

    private String caddress2;

    private String caddress3;

    private String ccity;

    private String ccontacttime;

    private Date cdob;

    private String cemail;

    private String cfname;

    private String clandline;

    private String cmname;

    private String cmobileno;

    private String cninumber;

    private String cpostcode;

    private String cregion;

    private String csname;

    private String ctitle;

    private String isJointTenancy;

    private String cpostalcode;

    public String getCaddress1() {
        return caddress1;
    }

    public void setCaddress1(String caddress1) {
        this.caddress1 = caddress1;
    }

    public String getCaddress2() {
        return caddress2;
    }

    public void setCaddress2(String caddress2) {
        this.caddress2 = caddress2;
    }

    public String getCaddress3() {
        return caddress3;
    }

    public void setCaddress3(String caddress3) {
        this.caddress3 = caddress3;
    }

    public String getCcity() {
        return ccity;
    }

    public void setCcity(String ccity) {
        this.ccity = ccity;
    }

    public String getCcontacttime() {
        return ccontacttime;
    }

    public void setCcontacttime(String ccontacttime) {
        this.ccontacttime = ccontacttime;
    }

    public Date getCdob() {
        return cdob;
    }

    public void setCdob(Date cdob) {
        this.cdob = cdob;
    }

    public String getCemail() {
        return cemail;
    }

    public void setCemail(String cemail) {
        this.cemail = cemail;
    }

    public String getCfname() {
        return cfname;
    }

    public void setCfname(String cfname) {
        this.cfname = cfname;
    }

    public String getClandline() {
        return clandline;
    }

    public void setClandline(String clandline) {
        this.clandline = clandline;
    }

    public String getCmname() {
        return cmname;
    }

    public void setCmname(String cmname) {
        this.cmname = cmname;
    }

    public String getCmobileno() {
        return cmobileno;
    }

    public void setCmobileno(String cmobileno) {
        this.cmobileno = cmobileno;
    }

    public String getCninumber() {
        return cninumber;
    }

    public void setCninumber(String cninumber) {
        this.cninumber = cninumber;
    }

    public String getCpostcode() {
        return cpostcode;
    }

    public void setCpostcode(String cpostcode) {
        this.cpostcode = cpostcode;
    }

    public String getCregion() {
        return cregion;
    }

    public void setCregion(String cregion) {
        this.cregion = cregion;
    }

    public String getCsname() {
        return csname;
    }

    public void setCsname(String csname) {
        this.csname = csname;
    }

    public String getCtitle() {
        return ctitle;
    }

    public void setCtitle(String ctitle) {
        this.ctitle = ctitle;
    }

    public String getIsJointTenancy() {
        return isJointTenancy;
    }

    public void setIsJointTenancy(String isJointTenancy) {
        this.isJointTenancy = isJointTenancy;
    }

    public String getCpostalcode() {
        return cpostalcode;
    }

    public void setCpostalcode(String cpostalcode) {
        this.cpostalcode = cpostalcode;
    }
}