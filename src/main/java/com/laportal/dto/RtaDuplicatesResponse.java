package com.laportal.dto;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 5/15/2023
 * Time: 11:52 PM
 * Project : legalassist
 */
public class RtaDuplicatesResponse {
    private String duplicateType;
    private String caseNumber;
    private String fullName;
    private String rtacode;
    private String hirecode;
    private String hdrcode;

    public String getDuplicateType() {
        return duplicateType;
    }

    public void setDuplicateType(String duplicateType) {
        this.duplicateType = duplicateType;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRtacode() {
        return rtacode;
    }

    public void setRtacode(String rtacode) {
        this.rtacode = rtacode;
    }

    public String getHirecode() {
        return hirecode;
    }

    public void setHirecode(String hirecode) {
        this.hirecode = hirecode;
    }

    public String getHdrcode() {
        return hdrcode;
    }

    public void setHdrcode(String hdrcode) {
        this.hdrcode = hdrcode;
    }
}
