package com.laportal.dto;

public class PerformActionOnHdrRequest {


    private String hdrClaimCode;
    private String toStatus;
    private String reason;


    public String getHdrClaimCode() {
        return hdrClaimCode;
    }

    public void setHdrClaimCode(String hdrClaimCode) {
        this.hdrClaimCode = hdrClaimCode;
    }

    public String getToStatus() {
        return toStatus;
    }

    public void setToStatus(String toStatus) {
        this.toStatus = toStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
