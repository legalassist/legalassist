package com.laportal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
public class SaveMnRequest {

    private String anynotes;
    private String anyotherlosses;
    private Date calldate;
    private String calltime;
    private String clientcontactno;
    private Date clientdob;
    private String clientemail;
    private String clientninumber;
    private String clientoccupation;
    private String complaintmade;
    private String complaintresponded;
    private String emergencysrvcattend;
    private String gpdetails;
    private String hospitalattend;
    private String hospitaldetails;
    private Date incidentdatetime;
    private String injuries;
    private String location;
    private Date medicalattenddate;
    private String negligencedescr;
    private String notesdetail;
    private String other;
    private String policeref;
    private String remarks;
    private String separatecomplaintcontact;
    private String stilloffwork;
    private String timeoffwork;
    private String timesincecomplaint;
    private String tpaddress;
    private String tpcompany;
    private String tpcontactno;
    private String tpname;
    private String witnesscontactdetails;
    private String witnesses;
    private String witnessname;
    private String hotkeyed;
    private String refundedclientinformed;
    private String agentadviced;
    private String gdpr;

    private Long advisor;
    private Long introducer;
    private String title;
    private String firstname;
    private String middlename;
    private String lastname;
    private String postalcode;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String region;
    private String acctime;


}
