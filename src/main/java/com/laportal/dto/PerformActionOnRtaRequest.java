package com.laportal.dto;

public class PerformActionOnRtaRequest {


    private String rtacode;
    private String toStatus;
    private String reason;


    public String getRtacode() {
        return rtacode;
    }

    public void setRtacode(String rtacode) {
        this.rtacode = rtacode;
    }

    public String getToStatus() {
        return toStatus;
    }

    public void setToStatus(String toStatus) {
        this.toStatus = toStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
