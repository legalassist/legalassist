package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 2/26/2023
 * Time: 4:15 PM
 * Project : legalassist
 */
public class ManualinvoiceRequest {
    private String customername;
    private String customeraddress;
    private String duedate;
    private String tax;
    private String grandtotal;

    private List<ManualinvoiceRequestDetail> manualinvoicedetails = new ArrayList<>();

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomeraddress() {
        return customeraddress;
    }

    public void setCustomeraddress(String customeraddress) {
        this.customeraddress = customeraddress;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public List<ManualinvoiceRequestDetail> getManualinvoicedetails() {
        return manualinvoicedetails;
    }

    public void setManualinvoicedetails(List<ManualinvoiceRequestDetail> manualinvoicedetails) {
        this.manualinvoicedetails = manualinvoicedetails;
    }

    public String getGrandtotal() {
        return grandtotal;
    }

    public void setGrandtotal(String grandtotal) {
        this.grandtotal = grandtotal;
    }
}
