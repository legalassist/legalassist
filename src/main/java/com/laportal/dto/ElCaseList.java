package com.laportal.dto;

import java.math.BigDecimal;
import java.util.Date;

public class ElCaseList {

    private BigDecimal elClaimCode;
    private Date created;
    private String code;
    private String client;
    private String currentTask;
    private String status;
    private Date lastUpdated;
    private String introducer;
    private Date lastNote;

    public BigDecimal getElClaimCode() {
        return elClaimCode;
    }

    public void setElClaimCode(BigDecimal elClaimCode) {
        this.elClaimCode = elClaimCode;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }


    public String getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(String currentTask) {
        this.currentTask = currentTask;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    public String getIntroducer() {
        return introducer;
    }

    public void setIntroducer(String introducer) {
        this.introducer = introducer;
    }

    public Date getLastNote() {
        return lastNote;
    }

    public void setLastNote(Date lastNote) {
        this.lastNote = lastNote;
    }
}
