package com.laportal.dto;

import java.util.List;

public class Menu {

    private String label;
    private String icon;
    private String to;
    private String isAddnewcase;
    private String isList;

    private List<Menu> items = null;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public List<Menu> getItems() {
        return items;
    }

    public void setItems(List<Menu> items) {
        this.items = items;
    }

    public String getIsAddnewcase() {
        return isAddnewcase;
    }

    public void setIsAddnewcase(String isAddnewcase) {
        this.isAddnewcase = isAddnewcase;
    }

    public String getIsList() {
        return isList;
    }

    public void setIsList(String isList) {
        this.isList = isList;
    }
}
