package com.laportal.dto;

import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

public class UploadHdrDocs {

    private String hdrClaimCode;

    private List<MultipartFile> multipartFiles = new ArrayList<>();

    public String getHdrClaimCode() {
        return hdrClaimCode;
    }

    public void setHdrClaimCode(String hdrClaimCode) {
        this.hdrClaimCode = hdrClaimCode;
    }

    public List<MultipartFile> getMultipartFiles() {
        return multipartFiles;
    }

    public void setMultipartFiles(List<MultipartFile> multipartFiles) {
        this.multipartFiles = multipartFiles;
    }
}
