package com.laportal.dto;

import com.laportal.model.TblHdrnote;
import com.laportal.model.TblHirenote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 12/7/2023
 * Time: 5:40 PM
 * Project : legalassist
 */
public class HireNotesResponse {

    private List<TblHirenote> tblHirenotes = new ArrayList<>();
    private List<TblHirenote> tblHirenotesLegalOnly = new ArrayList<>();

    public List<TblHirenote> getTblHirenotes() {
        return tblHirenotes;
    }

    public void setTblHirenotes(List<TblHirenote> tblHirenotes) {
        this.tblHirenotes = tblHirenotes;
    }

    public List<TblHirenote> getTblHirenotesLegalOnly() {
        return tblHirenotesLegalOnly;
    }

    public void setTblHirenotesLegalOnly(List<TblHirenote> tblHirenotesLegalOnly) {
        this.tblHirenotesLegalOnly = tblHirenotesLegalOnly;
    }
}
