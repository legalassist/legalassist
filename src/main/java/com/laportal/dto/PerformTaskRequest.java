package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

public class PerformTaskRequest {

    private long taskCode;
    private long rtaCode;
    private List<RtaFileUploadRequest> files = new ArrayList<>();

    public long getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(long taskCode) {
        this.taskCode = taskCode;
    }

    public long getRtaCode() {
        return rtaCode;
    }

    public void setRtaCode(long rtaCode) {
        this.rtaCode = rtaCode;
    }

    public List<RtaFileUploadRequest> getFiles() {
        return files;
    }

    public void setFiles(List<RtaFileUploadRequest> files) {
        this.files = files;
    }
}
