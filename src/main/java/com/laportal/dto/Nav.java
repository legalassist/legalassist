package com.laportal.dto;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 2/26/2023
 * Time: 2:25 PM
 * Project : legalassist
 */

public class Nav {
    private String label;
    private String isAddnewcase;
    private String addNewCaseLink;
    private String addNewCaseLabel;
    private String isList;
    private String listLabel;
    private String listLink;
    private String bottomAreaShowLabel;
    private String bottomAreaShowLink;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIsAddnewcase() {
        return isAddnewcase;
    }

    public void setIsAddnewcase(String isAddnewcase) {
        this.isAddnewcase = isAddnewcase;
    }

    public String getAddNewCaseLink() {
        return addNewCaseLink;
    }

    public void setAddNewCaseLink(String addNewCaseLink) {
        this.addNewCaseLink = addNewCaseLink;
    }

    public String getAddNewCaseLabel() {
        return addNewCaseLabel;
    }

    public void setAddNewCaseLabel(String addNewCaseLabel) {
        this.addNewCaseLabel = addNewCaseLabel;
    }

    public String getIsList() {
        return isList;
    }

    public void setIsList(String isList) {
        this.isList = isList;
    }

    public String getListLabel() {
        return listLabel;
    }

    public void setListLabel(String listLabel) {
        this.listLabel = listLabel;
    }

    public String getListLink() {
        return listLink;
    }

    public void setListLink(String listLink) {
        this.listLink = listLink;
    }

    public String getBottomAreaShowLabel() {
        return bottomAreaShowLabel;
    }

    public void setBottomAreaShowLabel(String bottomAreaShowLabel) {
        this.bottomAreaShowLabel = bottomAreaShowLabel;
    }

    public String getBottomAreaShowLink() {
        return bottomAreaShowLink;
    }

    public void setBottomAreaShowLink(String bottomAreaShowLink) {
        this.bottomAreaShowLink = bottomAreaShowLink;
    }
}