package com.laportal.dto;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 1/19/2023
 * Time: 1:00 PM
 * Project : legalassist
 */
public class RemoveInvoiceRequest {
    private String invoiceDetailId;
    private String compaingcode;
    private String invType;

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getCompaingcode() {
        return compaingcode;
    }

    public void setCompaingcode(String compaingcode) {
        this.compaingcode = compaingcode;
    }

    public String getInvType() {
        return invType;
    }

    public void setInvType(String invType) {
        this.invType = invType;
    }
}
