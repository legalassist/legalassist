package com.laportal.dto;

import java.math.BigDecimal;
import java.util.Date;

public class SaveTblHdrtenancy {

    private BigDecimal arrearsAmount;

    private String benefitsDetail;

    private BigDecimal contributePm;

    private BigDecimal contributePw;

    private String inArrears;

    private String inPaymentPlan;

    private BigDecimal noOfOccupants;

    private BigDecimal paymentPlanPm;

    private BigDecimal paymentPlanPw;

    private String propertyType;

    private String receivingBenefits;

    private String rentContribute;

    private BigDecimal rentPm;

    private BigDecimal rentPw;

    private Date startDate;

    public BigDecimal getArrearsAmount() {
        return arrearsAmount;
    }

    public void setArrearsAmount(BigDecimal arrearsAmount) {
        this.arrearsAmount = arrearsAmount;
    }

    public String getBenefitsDetail() {
        return benefitsDetail;
    }

    public void setBenefitsDetail(String benefitsDetail) {
        this.benefitsDetail = benefitsDetail;
    }

    public BigDecimal getContributePm() {
        return contributePm;
    }

    public void setContributePm(BigDecimal contributePm) {
        this.contributePm = contributePm;
    }

    public BigDecimal getContributePw() {
        return contributePw;
    }

    public void setContributePw(BigDecimal contributePw) {
        this.contributePw = contributePw;
    }

    public String getInArrears() {
        return inArrears;
    }

    public void setInArrears(String inArrears) {
        this.inArrears = inArrears;
    }

    public String getInPaymentPlan() {
        return inPaymentPlan;
    }

    public void setInPaymentPlan(String inPaymentPlan) {
        this.inPaymentPlan = inPaymentPlan;
    }

    public BigDecimal getNoOfOccupants() {
        return noOfOccupants;
    }

    public void setNoOfOccupants(BigDecimal noOfOccupants) {
        this.noOfOccupants = noOfOccupants;
    }

    public BigDecimal getPaymentPlanPm() {
        return paymentPlanPm;
    }

    public void setPaymentPlanPm(BigDecimal paymentPlanPm) {
        this.paymentPlanPm = paymentPlanPm;
    }

    public BigDecimal getPaymentPlanPw() {
        return paymentPlanPw;
    }

    public void setPaymentPlanPw(BigDecimal paymentPlanPw) {
        this.paymentPlanPw = paymentPlanPw;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getReceivingBenefits() {
        return receivingBenefits;
    }

    public void setReceivingBenefits(String receivingBenefits) {
        this.receivingBenefits = receivingBenefits;
    }

    public String getRentContribute() {
        return rentContribute;
    }

    public void setRentContribute(String rentContribute) {
        this.rentContribute = rentContribute;
    }

    public BigDecimal getRentPm() {
        return rentPm;
    }

    public void setRentPm(BigDecimal rentPm) {
        this.rentPm = rentPm;
    }

    public BigDecimal getRentPw() {
        return rentPw;
    }

    public void setRentPw(BigDecimal rentPw) {
        this.rentPw = rentPw;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}