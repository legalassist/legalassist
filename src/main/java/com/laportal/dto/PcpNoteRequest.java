package com.laportal.dto;

public class PcpNoteRequest {

    private String pcpCode;

    private String note;

    private String userCatCode;

    public String getPcpCode() {
        return pcpCode;
    }

    public void setPcpCode(String pcpCode) {
        this.pcpCode = pcpCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserCatCode() {
        return userCatCode;
    }

    public void setUserCatCode(String userCatCode) {
        this.userCatCode = userCatCode;
    }
}
