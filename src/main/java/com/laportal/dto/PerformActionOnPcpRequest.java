package com.laportal.dto;

public class PerformActionOnPcpRequest {


    private String pcpClaimCode;
    private String toStatus;
    private String reason;

    public String getPcpClaimCode() {
        return pcpClaimCode;
    }

    public void setPcpClaimCode(String pcpClaimCode) {
        this.pcpClaimCode = pcpClaimCode;
    }

    public String getToStatus() {
        return toStatus;
    }

    public void setToStatus(String toStatus) {
        this.toStatus = toStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
