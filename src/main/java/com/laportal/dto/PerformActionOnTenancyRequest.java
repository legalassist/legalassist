package com.laportal.dto;

public class PerformActionOnTenancyRequest {


    private String tenancyClaimCode;
    private String toStatus;
    private String reason;

    public String getTenancyClaimCode() {
        return tenancyClaimCode;
    }

    public void setTenancyClaimCode(String tenancyClaimCode) {
        this.tenancyClaimCode = tenancyClaimCode;
    }

    public String getToStatus() {
        return toStatus;
    }

    public void setToStatus(String toStatus) {
        this.toStatus = toStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
