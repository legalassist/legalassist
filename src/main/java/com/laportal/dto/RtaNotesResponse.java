package com.laportal.dto;

import com.laportal.model.TblInvoicenote;
import com.laportal.model.TblRtanote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 8/4/2023
 * Time: 8:22 PM
 * Project : legalassist
 */
public class RtaNotesResponse {

    private List<TblRtanote> tblRtanotes = new ArrayList<>();
    private List<TblRtanote> tblRtanotesLegalOnly = new ArrayList<>();
    private List<TblInvoicenote> tblInvoicenotes = new ArrayList<>();

    public List<TblRtanote> getTblRtanotes() {
        return tblRtanotes;
    }

    public void setTblRtanotes(List<TblRtanote> tblRtanotes) {
        this.tblRtanotes = tblRtanotes;
    }

    public List<TblRtanote> getTblRtanotesLegalOnly() {
        return tblRtanotesLegalOnly;
    }

    public void setTblRtanotesLegalOnly(List<TblRtanote> tblRtanotesLegalOnly) {
        this.tblRtanotesLegalOnly = tblRtanotesLegalOnly;
    }

    public List<TblInvoicenote> getTblInvoicenotes() {
        return tblInvoicenotes;
    }

    public void setTblInvoicenotes(List<TblInvoicenote> tblInvoicenotes) {
        this.tblInvoicenotes = tblInvoicenotes;
    }
}
