package com.laportal.dto;

public class PerformActionOnOlRequest {


    private String olcode;
    private String toStatus;
    private String reason;

    public String getOlcode() {
        return olcode;
    }

    public void setOlcode(String olcode) {
        this.olcode = olcode;
    }

    public String getToStatus() {
        return toStatus;
    }

    public void setToStatus(String toStatus) {
        this.toStatus = toStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
