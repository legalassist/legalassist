package com.laportal.dto;

import com.laportal.model.TblHdrnote;
import com.laportal.model.TblRtanote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 8/4/2023
 * Time: 8:22 PM
 * Project : legalassist
 */
public class HdrNotesResponse {

    private List<TblHdrnote> tblHdrnotes = new ArrayList<>();
    private List<TblHdrnote> tblhdrnotesLegalOnly = new ArrayList<>();

    public List<TblHdrnote> getTblHdrnotes() {
        return tblHdrnotes;
    }

    public void setTblHdrnotes(List<TblHdrnote> tblHdrnotes) {
        this.tblHdrnotes = tblHdrnotes;
    }

    public List<TblHdrnote> getTblhdrnotesLegalOnly() {
        return tblhdrnotesLegalOnly;
    }

    public void setTblhdrnotesLegalOnly(List<TblHdrnote> tblhdrnotesLegalOnly) {
        this.tblhdrnotesLegalOnly = tblhdrnotesLegalOnly;
    }
}
