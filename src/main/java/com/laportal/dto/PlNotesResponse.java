package com.laportal.dto;

import com.laportal.model.TblOlnote;
import com.laportal.model.TblPlnote;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 2/12/2024
 * Time: 7:14 PM
 * Project : legalassist
 */

@Getter
@Setter
public class PlNotesResponse {

    private List<TblPlnote> tblPlnotes = new ArrayList<>();
    private List<TblPlnote> tblPlnotesLegalOnly = new ArrayList<>();
}
