package com.laportal.dto;

public class ResendTenancyMessageDto {


    private String tenancymessagecode;

    public String getTenancymessagecode() {
        return tenancymessagecode;
    }

    public void setTenancymessagecode(String tenancymessagecode) {
        this.tenancymessagecode = tenancymessagecode;
    }
}
