package com.laportal.dto;

public class ResendPcpMessageDto {


    private String pcpmessagecode;

    public String getPcpmessagecode() {
        return pcpmessagecode;
    }

    public void setPcpmessagecode(String pcpmessagecode) {
        this.pcpmessagecode = pcpmessagecode;
    }
}
