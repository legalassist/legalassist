package com.laportal.dto;

import java.util.Date;

public class SavePlRequest {

    private Date accdatetime;
    private String acctime;
    private String accdescription;
    private String acclocation;
    private String accreportedto;
    private String describeinjuries;
    private Date dob;
    private String email;
    private String injuriesduration;
    private String landline;
    private String medicalattention;
    private String mobile;
    private String ninumber;
    private String occupation;
    private String password;
    private String remarks;
    private String witnesses;
    private String title;
    private String firstname;
    private String middlename;
    private String lastname;
    private String postalcode;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String region;
    private Long advisor;
    private Long introducer;

    public Date getAccdatetime() {
        return accdatetime;
    }

    public void setAccdatetime(Date accdatetime) {
        this.accdatetime = accdatetime;
    }

    public String getAccdescription() {
        return accdescription;
    }

    public void setAccdescription(String accdescription) {
        this.accdescription = accdescription;
    }

    public String getAcclocation() {
        return acclocation;
    }

    public void setAcclocation(String acclocation) {
        this.acclocation = acclocation;
    }

    public String getAccreportedto() {
        return accreportedto;
    }

    public void setAccreportedto(String accreportedto) {
        this.accreportedto = accreportedto;
    }

    public String getDescribeinjuries() {
        return describeinjuries;
    }

    public void setDescribeinjuries(String describeinjuries) {
        this.describeinjuries = describeinjuries;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInjuriesduration() {
        return injuriesduration;
    }

    public void setInjuriesduration(String injuriesduration) {
        this.injuriesduration = injuriesduration;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getMedicalattention() {
        return medicalattention;
    }

    public void setMedicalattention(String medicalattention) {
        this.medicalattention = medicalattention;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNinumber() {
        return ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getWitnesses() {
        return witnesses;
    }

    public void setWitnesses(String witnesses) {
        this.witnesses = witnesses;
    }

    public String getAcctime() {
        return acctime;
    }

    public void setAcctime(String acctime) {
        this.acctime = acctime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Long getAdvisor() {
        return advisor;
    }

    public void setAdvisor(Long advisor) {
        this.advisor = advisor;
    }

    public Long getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Long introducer) {
        this.introducer = introducer;
    }
}
