package com.laportal.dto;

public class ResendPlMessageDto {


    private String plmessagecode;

    public String getPlmessagecode() {
        return plmessagecode;
    }

    public void setPlmessagecode(String plmessagecode) {
        this.plmessagecode = plmessagecode;
    }
}
