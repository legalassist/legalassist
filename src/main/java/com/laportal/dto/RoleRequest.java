package com.laportal.dto;

public class RoleRequest {

    private String descr;

    private String rolename;

    private String rolestatus;

    private String compaignCode;

    private String usercategoryCode;

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getRolestatus() {
        return rolestatus;
    }

    public void setRolestatus(String rolestatus) {
        this.rolestatus = rolestatus;
    }

    public String getCompaignCode() {
        return compaignCode;
    }

    public void setCompaignCode(String compaignCode) {
        this.compaignCode = compaignCode;
    }

    public String getUsercategoryCode() {
        return usercategoryCode;
    }

    public void setUsercategoryCode(String usercategoryCode) {
        this.usercategoryCode = usercategoryCode;
    }
}
