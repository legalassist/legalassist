package com.laportal.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 2/6/2024
 * Time: 2:08 PM
 * Project : legalassist
 */


@Getter
@Setter
public class CompanyProfileResponse {

   private String name;
    private String addressline1;
    private String updatedOn;
    private String companystatus;
    private String companyId;
    private String categoryname;

}
