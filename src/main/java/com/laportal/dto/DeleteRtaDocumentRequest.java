package com.laportal.dto;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 5/15/2023
 * Time: 10:26 PM
 * Project : legalassist
 */
public class DeleteRtaDocumentRequest {
    private long doccode;

    public long getDoccode() {
        return doccode;
    }

    public void setDoccode(long doccode) {
        this.doccode = doccode;
    }

}
