package com.laportal.dto;

public class ResendRtaMessageDto {


    private String rtamessagecode;

    public String getRtamessagecode() {
        return rtamessagecode;
    }

    public void setRtamessagecode(String rtamessagecode) {
        this.rtamessagecode = rtamessagecode;
    }
}
