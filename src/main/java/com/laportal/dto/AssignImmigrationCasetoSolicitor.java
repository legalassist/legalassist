package com.laportal.dto;

public class AssignImmigrationCasetoSolicitor {

    private String immigrationClaimCode;
    private String solicitorCode;
    private String solicitorUserCode;

    public String getImmigrationClaimCode() {
        return immigrationClaimCode;
    }

    public void setImmigrationClaimCode(String immigrationClaimCode) {
        this.immigrationClaimCode = immigrationClaimCode;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public String getSolicitorUserCode() {
        return solicitorUserCode;
    }

    public void setSolicitorUserCode(String solicitorUserCode) {
        this.solicitorUserCode = solicitorUserCode;
    }
}
