package com.laportal.dto;


import java.util.ArrayList;
import java.util.List;

public class UpdateCompanyUserRequest {


    private String companycode;
    private String usercode;
    private String loginid;
    private String username;
    private String status;
    private List<String> rolecodes = new ArrayList<>();

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getRolecodes() {
        return rolecodes;
    }

    public void setRolecodes(List<String> rolecodes) {
        this.rolecodes = rolecodes;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
