package com.laportal.dto;

public class HireBusinessDetailRequest {

    private String bookingdate;
    private String bookingmode;
    private String createdby;
    private String hireenddate;
    private String hirestartdate;
    private String outsourced;
    private String outsourcedto;
    private String reason;
    private String service;

    public String getBookingdate() {
        return bookingdate;
    }

    public void setBookingdate(String bookingdate) {
        this.bookingdate = bookingdate;
    }

    public String getBookingmode() {
        return bookingmode;
    }

    public void setBookingmode(String bookingmode) {
        this.bookingmode = bookingmode;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getHireenddate() {
        return hireenddate;
    }

    public void setHireenddate(String hireenddate) {
        this.hireenddate = hireenddate;
    }

    public String getHirestartdate() {
        return hirestartdate;
    }

    public void setHirestartdate(String hirestartdate) {
        this.hirestartdate = hirestartdate;
    }

    public String getOutsourced() {
        return outsourced;
    }

    public void setOutsourced(String outsourced) {
        this.outsourced = outsourced;
    }

    public String getOutsourcedto() {
        return outsourcedto;
    }

    public void setOutsourcedto(String outsourcedto) {
        this.outsourcedto = outsourcedto;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }
}
