package com.laportal.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 2/29/2024
 * Time: 3:17 PM
 * Project : legalassist
 */

@Getter
@Setter
public class HdrCopyLog {

    private String hdrcode;
    private String hdrClaimCode;
    private String statusDescr;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss" ,timezone = "UTC")
    private Date createDate;
    private String solicitorName;

}
