package com.laportal.dto;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 8/3/2023
 * Time: 11:14 PM
 * Project : legalassist
 */
public class CopyRtaToRtaRequest {

    private String rtacode;
    private String hdrcode;

    public String getRtacode() {
        return rtacode;
    }

    public void setRtacode(String rtacode) {
        this.rtacode = rtacode;
    }

    public String getHdrcode() {
        return hdrcode;
    }

    public void setHdrcode(String hdrcode) {
        this.hdrcode = hdrcode;
    }
}
