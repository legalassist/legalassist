package com.laportal.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 1/10/2024
 * Time: 3:05 PM
 * Project : legalassist
 */

@Getter
@Setter
public class ThirdPartyNoteRequest {
    @NotEmpty
    @NotNull
    private String clientSecret;

    @NotNull
    @NotEmpty
    private String claimType;


    @NotNull
    @NotEmpty
    private String claimRefNo;


    @NotNull
    @NotEmpty
    private String claimNote;


    @NotNull
    @NotEmpty
    private String claimNoteDate;

    private String reserved1;
    private String reserved2;
    private String reserved3;
    private String reserved4;
    private String reserved5;
    private String reserved6;


}
