package com.laportal.dto;

public class ChangeHireBusinessUser {

    private String hireclaimcode;
    private String userCode;
    private String companyCode;

    public String getHireclaimcode() {
        return hireclaimcode;
    }

    public void setHireclaimcode(String hireclaimcode) {
        this.hireclaimcode = hireclaimcode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
}
