package com.laportal.dto;

public class ChangeBusinessStatus {

    private long hireclaimcode;
    private String status;
    private String companyCode;

    public long getHireclaimcode() {
        return hireclaimcode;
    }

    public void setHireclaimcode(long hireclaimcode) {
        this.hireclaimcode = hireclaimcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
}
