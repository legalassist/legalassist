package com.laportal.dto;

import com.laportal.model.TblRtastatus;

public class HireActionButton {


    private String buttonname;
    private String buttonvalue;
    private TblRtastatus tblRtastatus;

    private String caseacceptdialog;
    private String caseassigndialog;
    private String caserejectdialog;
    private String casecanceldialog;
    private String apiflag;


    public String getButtonname() {
        return buttonname;
    }

    public void setButtonname(String buttonname) {
        this.buttonname = buttonname;
    }

    public String getButtonvalue() {
        return buttonvalue;
    }

    public void setButtonvalue(String buttonvalue) {
        this.buttonvalue = buttonvalue;
    }

    public TblRtastatus getTblRtastatus() {
        return tblRtastatus;
    }

    public void setTblRtastatus(TblRtastatus tblRtastatus) {
        this.tblRtastatus = tblRtastatus;
    }

    public String getCaseacceptdialog() {
        return caseacceptdialog;
    }

    public void setCaseacceptdialog(String caseacceptdialog) {
        this.caseacceptdialog = caseacceptdialog;
    }

    public String getCaseassigndialog() {
        return caseassigndialog;
    }

    public void setCaseassigndialog(String caseassigndialog) {
        this.caseassigndialog = caseassigndialog;
    }

    public String getCaserejectdialog() {
        return caserejectdialog;
    }

    public void setCaserejectdialog(String caserejectdialog) {
        this.caserejectdialog = caserejectdialog;
    }

    public String getApiflag() {
        return apiflag;
    }

    public void setApiflag(String apiflag) {
        this.apiflag = apiflag;
    }

    public String getCasecanceldialog() {
        return casecanceldialog;
    }

    public void setCasecanceldialog(String casecanceldialog) {
        this.casecanceldialog = casecanceldialog;
    }
}
