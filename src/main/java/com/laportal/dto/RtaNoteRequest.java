package com.laportal.dto;

public class RtaNoteRequest {

    private String rtaCode;

    private String note;

    private String userCatCode;

    private String invoiceHeadId;


    public String getRtaCode() {
        return rtaCode;
    }

    public void setRtaCode(String rtaCode) {
        this.rtaCode = rtaCode;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUserCatCode() {
        return userCatCode;
    }

    public void setUserCatCode(String userCatCode) {
        this.userCatCode = userCatCode;
    }

    public String getInvoiceHeadId() {
        return invoiceHeadId;
    }

    public void setInvoiceHeadId(String invoiceHeadId) {
        this.invoiceHeadId = invoiceHeadId;
    }
}
