package com.laportal.dto;

public class CopyCaseRequest {

    private String rtacode;
    private String hireCode;

    public String getRtacode() {
        return rtacode;
    }

    public void setRtacode(String rtacode) {
        this.rtacode = rtacode;
    }

    public String getHireCode() {
        return hireCode;
    }

    public void setHireCode(String hireCode) {
        this.hireCode = hireCode;
    }
}
