package com.laportal.dto;

import java.util.Date;

public class SaveTenancyRequest {

    private String amountOfDepositReturned;
    private String arrears;
    private String claimHousingBenefitsLiving;
    private String contact;
    private String currentAddress;
    private Date dateMovedOn;
    private String depositPaid;
    private Date depositPaidDate;
    private String depositPaidMethod;
    private String depositProtected;
    private Date dob;
    private String email;
    private String fullName;
    private String haveLandlordContact;
    private String landlordAddress;
    private String landlordContact;
    private String landlordDamageClaim;
    private String landlordEmailAddress;
    private String landlordLiveInProperty;
    private String nameOfAgency;
    private String noOfTenancyAgrmtSigned;
    private String onlyTenantRegistered;
    private String paidToLandlordOrAgent;
    private String paymentPlan;
    private String receivingBenefits;
    private String rentAmount;
    private String rentArrears;
    private String rentArrearsDetail;
    private String stillLivingInProperty;
    private Date termDate;
    private String thinkReason;
    private String withheldReason;
    private String tenancyCode;

    public String getAmountOfDepositReturned() {
        return amountOfDepositReturned;
    }

    public void setAmountOfDepositReturned(String amountOfDepositReturned) {
        this.amountOfDepositReturned = amountOfDepositReturned;
    }

    public String getArrears() {
        return arrears;
    }

    public void setArrears(String arrears) {
        this.arrears = arrears;
    }

    public String getClaimHousingBenefitsLiving() {
        return claimHousingBenefitsLiving;
    }

    public void setClaimHousingBenefitsLiving(String claimHousingBenefitsLiving) {
        this.claimHousingBenefitsLiving = claimHousingBenefitsLiving;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public Date getDateMovedOn() {
        return dateMovedOn;
    }

    public void setDateMovedOn(Date dateMovedOn) {
        this.dateMovedOn = dateMovedOn;
    }

    public String getDepositPaid() {
        return depositPaid;
    }

    public void setDepositPaid(String depositPaid) {
        this.depositPaid = depositPaid;
    }

    public Date getDepositPaidDate() {
        return depositPaidDate;
    }

    public void setDepositPaidDate(Date depositPaidDate) {
        this.depositPaidDate = depositPaidDate;
    }

    public String getDepositPaidMethod() {
        return depositPaidMethod;
    }

    public void setDepositPaidMethod(String depositPaidMethod) {
        this.depositPaidMethod = depositPaidMethod;
    }

    public String getDepositProtected() {
        return depositProtected;
    }

    public void setDepositProtected(String depositProtected) {
        this.depositProtected = depositProtected;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getHaveLandlordContact() {
        return haveLandlordContact;
    }

    public void setHaveLandlordContact(String haveLandlordContact) {
        this.haveLandlordContact = haveLandlordContact;
    }

    public String getLandlordAddress() {
        return landlordAddress;
    }

    public void setLandlordAddress(String landlordAddress) {
        this.landlordAddress = landlordAddress;
    }

    public String getLandlordContact() {
        return landlordContact;
    }

    public void setLandlordContact(String landlordContact) {
        this.landlordContact = landlordContact;
    }

    public String getLandlordDamageClaim() {
        return landlordDamageClaim;
    }

    public void setLandlordDamageClaim(String landlordDamageClaim) {
        this.landlordDamageClaim = landlordDamageClaim;
    }

    public String getLandlordEmailAddress() {
        return landlordEmailAddress;
    }

    public void setLandlordEmailAddress(String landlordEmailAddress) {
        this.landlordEmailAddress = landlordEmailAddress;
    }

    public String getLandlordLiveInProperty() {
        return landlordLiveInProperty;
    }

    public void setLandlordLiveInProperty(String landlordLiveInProperty) {
        this.landlordLiveInProperty = landlordLiveInProperty;
    }

    public String getNameOfAgency() {
        return nameOfAgency;
    }

    public void setNameOfAgency(String nameOfAgency) {
        this.nameOfAgency = nameOfAgency;
    }

    public String getNoOfTenancyAgrmtSigned() {
        return noOfTenancyAgrmtSigned;
    }

    public void setNoOfTenancyAgrmtSigned(String noOfTenancyAgrmtSigned) {
        this.noOfTenancyAgrmtSigned = noOfTenancyAgrmtSigned;
    }

    public String getOnlyTenantRegistered() {
        return onlyTenantRegistered;
    }

    public void setOnlyTenantRegistered(String onlyTenantRegistered) {
        this.onlyTenantRegistered = onlyTenantRegistered;
    }

    public String getPaidToLandlordOrAgent() {
        return paidToLandlordOrAgent;
    }

    public void setPaidToLandlordOrAgent(String paidToLandlordOrAgent) {
        this.paidToLandlordOrAgent = paidToLandlordOrAgent;
    }

    public String getPaymentPlan() {
        return paymentPlan;
    }

    public void setPaymentPlan(String paymentPlan) {
        this.paymentPlan = paymentPlan;
    }

    public String getReceivingBenefits() {
        return receivingBenefits;
    }

    public void setReceivingBenefits(String receivingBenefits) {
        this.receivingBenefits = receivingBenefits;
    }

    public String getRentAmount() {
        return rentAmount;
    }

    public void setRentAmount(String rentAmount) {
        this.rentAmount = rentAmount;
    }

    public String getRentArrears() {
        return rentArrears;
    }

    public void setRentArrears(String rentArrears) {
        this.rentArrears = rentArrears;
    }

    public String getRentArrearsDetail() {
        return rentArrearsDetail;
    }

    public void setRentArrearsDetail(String rentArrearsDetail) {
        this.rentArrearsDetail = rentArrearsDetail;
    }

    public String getStillLivingInProperty() {
        return stillLivingInProperty;
    }

    public void setStillLivingInProperty(String stillLivingInProperty) {
        this.stillLivingInProperty = stillLivingInProperty;
    }

    public Date getTermDate() {
        return termDate;
    }

    public void setTermDate(Date termDate) {
        this.termDate = termDate;
    }

    public String getThinkReason() {
        return thinkReason;
    }

    public void setThinkReason(String thinkReason) {
        this.thinkReason = thinkReason;
    }

    public String getWithheldReason() {
        return withheldReason;
    }

    public void setWithheldReason(String withheldReason) {
        this.withheldReason = withheldReason;
    }

    public String getTenancyCode() {
        return tenancyCode;
    }

    public void setTenancyCode(String tenancyCode) {
        this.tenancyCode = tenancyCode;
    }
}
