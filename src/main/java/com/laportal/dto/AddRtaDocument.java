package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

public class AddRtaDocument {

    private long rtaCode;

    private List<RtaFileUploadRequest> files = new ArrayList<>();

    public long getRtaCode() {
        return rtaCode;
    }

    public void setRtaCode(long rtaCode) {
        this.rtaCode = rtaCode;
    }

    public List<RtaFileUploadRequest> getFiles() {
        return files;
    }

    public void setFiles(List<RtaFileUploadRequest> files) {
        this.files = files;
    }

}
