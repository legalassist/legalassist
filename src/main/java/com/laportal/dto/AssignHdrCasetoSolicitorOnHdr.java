package com.laportal.dto;

public class AssignHdrCasetoSolicitorOnHdr {

    private String hdrClaimCode;
    private String solicitorCode;
    private String solicitorUserCode;

    public String getHdrClaimCode() {
        return hdrClaimCode;
    }

    public void setHdrClaimCode(String hdrClaimCode) {
        this.hdrClaimCode = hdrClaimCode;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public String getSolicitorUserCode() {
        return solicitorUserCode;
    }

    public void setSolicitorUserCode(String solicitorUserCode) {
        this.solicitorUserCode = solicitorUserCode;
    }
}
