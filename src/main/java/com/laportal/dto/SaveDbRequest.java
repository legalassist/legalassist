package com.laportal.dto;

import java.util.Date;

public class SaveDbRequest {

    private String anyFfmSufferedDb;
    private String claimantAddress;
    private String claimantContactno;
    private Date claimantDob;
    private String claimantEmail;
    private String claimantName;
    private String complaintDefendent;
    private String consentPiShared;
    private Date dateOfBreach;
    private String dateOfKnowledge;
    private String dbByCompBusns;
    private String dbPreexistingCond;
    private String dbRectified;
    private String defendentAddress;
    private String defendentContactno;
    private String defendentEmail;
    private String defendentHandleDb;
    private String defendentName;
    private String defendentReqConsent;
    private String evidenceOfDb;
    private String frstFoundAbtDb;
    private String hasDbAffectedYou;
    private String howFeelYourDb;
    private String howKnowAbtDb;
    private String howKnowDefendent;
    private String instructedPrvntDb;
    private String litigationFriend;
    private String otherprblmWithDefendent;
    private String piDisclosedInDb;
    private String piShared;
    private String recurringDbOfPi;
    private String remarks;
    private String soughtMedicalAvdnce;

    public String getAnyFfmSufferedDb() {
        return anyFfmSufferedDb;
    }

    public void setAnyFfmSufferedDb(String anyFfmSufferedDb) {
        this.anyFfmSufferedDb = anyFfmSufferedDb;
    }

    public String getClaimantAddress() {
        return claimantAddress;
    }

    public void setClaimantAddress(String claimantAddress) {
        this.claimantAddress = claimantAddress;
    }

    public String getClaimantContactno() {
        return claimantContactno;
    }

    public void setClaimantContactno(String claimantContactno) {
        this.claimantContactno = claimantContactno;
    }

    public Date getClaimantDob() {
        return claimantDob;
    }

    public void setClaimantDob(Date claimantDob) {
        this.claimantDob = claimantDob;
    }

    public String getClaimantEmail() {
        return claimantEmail;
    }

    public void setClaimantEmail(String claimantEmail) {
        this.claimantEmail = claimantEmail;
    }

    public String getClaimantName() {
        return claimantName;
    }

    public void setClaimantName(String claimantName) {
        this.claimantName = claimantName;
    }

    public String getComplaintDefendent() {
        return complaintDefendent;
    }

    public void setComplaintDefendent(String complaintDefendent) {
        this.complaintDefendent = complaintDefendent;
    }

    public String getConsentPiShared() {
        return consentPiShared;
    }

    public void setConsentPiShared(String consentPiShared) {
        this.consentPiShared = consentPiShared;
    }

    public Date getDateOfBreach() {
        return dateOfBreach;
    }

    public void setDateOfBreach(Date dateOfBreach) {
        this.dateOfBreach = dateOfBreach;
    }

    public String getDateOfKnowledge() {
        return dateOfKnowledge;
    }

    public void setDateOfKnowledge(String dateOfKnowledge) {
        this.dateOfKnowledge = dateOfKnowledge;
    }

    public String getDbByCompBusns() {
        return dbByCompBusns;
    }

    public void setDbByCompBusns(String dbByCompBusns) {
        this.dbByCompBusns = dbByCompBusns;
    }

    public String getDbPreexistingCond() {
        return dbPreexistingCond;
    }

    public void setDbPreexistingCond(String dbPreexistingCond) {
        this.dbPreexistingCond = dbPreexistingCond;
    }

    public String getDbRectified() {
        return dbRectified;
    }

    public void setDbRectified(String dbRectified) {
        this.dbRectified = dbRectified;
    }

    public String getDefendentAddress() {
        return defendentAddress;
    }

    public void setDefendentAddress(String defendentAddress) {
        this.defendentAddress = defendentAddress;
    }

    public String getDefendentContactno() {
        return defendentContactno;
    }

    public void setDefendentContactno(String defendentContactno) {
        this.defendentContactno = defendentContactno;
    }

    public String getDefendentEmail() {
        return defendentEmail;
    }

    public void setDefendentEmail(String defendentEmail) {
        this.defendentEmail = defendentEmail;
    }

    public String getDefendentHandleDb() {
        return defendentHandleDb;
    }

    public void setDefendentHandleDb(String defendentHandleDb) {
        this.defendentHandleDb = defendentHandleDb;
    }

    public String getDefendentName() {
        return defendentName;
    }

    public void setDefendentName(String defendentName) {
        this.defendentName = defendentName;
    }

    public String getDefendentReqConsent() {
        return defendentReqConsent;
    }

    public void setDefendentReqConsent(String defendentReqConsent) {
        this.defendentReqConsent = defendentReqConsent;
    }

    public String getEvidenceOfDb() {
        return evidenceOfDb;
    }

    public void setEvidenceOfDb(String evidenceOfDb) {
        this.evidenceOfDb = evidenceOfDb;
    }

    public String getFrstFoundAbtDb() {
        return frstFoundAbtDb;
    }

    public void setFrstFoundAbtDb(String frstFoundAbtDb) {
        this.frstFoundAbtDb = frstFoundAbtDb;
    }

    public String getHasDbAffectedYou() {
        return hasDbAffectedYou;
    }

    public void setHasDbAffectedYou(String hasDbAffectedYou) {
        this.hasDbAffectedYou = hasDbAffectedYou;
    }

    public String getHowFeelYourDb() {
        return howFeelYourDb;
    }

    public void setHowFeelYourDb(String howFeelYourDb) {
        this.howFeelYourDb = howFeelYourDb;
    }

    public String getHowKnowAbtDb() {
        return howKnowAbtDb;
    }

    public void setHowKnowAbtDb(String howKnowAbtDb) {
        this.howKnowAbtDb = howKnowAbtDb;
    }

    public String getHowKnowDefendent() {
        return howKnowDefendent;
    }

    public void setHowKnowDefendent(String howKnowDefendent) {
        this.howKnowDefendent = howKnowDefendent;
    }

    public String getInstructedPrvntDb() {
        return instructedPrvntDb;
    }

    public void setInstructedPrvntDb(String instructedPrvntDb) {
        this.instructedPrvntDb = instructedPrvntDb;
    }

    public String getLitigationFriend() {
        return litigationFriend;
    }

    public void setLitigationFriend(String litigationFriend) {
        this.litigationFriend = litigationFriend;
    }

    public String getOtherprblmWithDefendent() {
        return otherprblmWithDefendent;
    }

    public void setOtherprblmWithDefendent(String otherprblmWithDefendent) {
        this.otherprblmWithDefendent = otherprblmWithDefendent;
    }

    public String getPiDisclosedInDb() {
        return piDisclosedInDb;
    }

    public void setPiDisclosedInDb(String piDisclosedInDb) {
        this.piDisclosedInDb = piDisclosedInDb;
    }

    public String getPiShared() {
        return piShared;
    }

    public void setPiShared(String piShared) {
        this.piShared = piShared;
    }

    public String getRecurringDbOfPi() {
        return recurringDbOfPi;
    }

    public void setRecurringDbOfPi(String recurringDbOfPi) {
        this.recurringDbOfPi = recurringDbOfPi;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSoughtMedicalAvdnce() {
        return soughtMedicalAvdnce;
    }

    public void setSoughtMedicalAvdnce(String soughtMedicalAvdnce) {
        this.soughtMedicalAvdnce = soughtMedicalAvdnce;
    }
}
