package com.laportal.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 1/15/2024
 * Time: 2:11 PM
 * Project : legalassist
 */
@Getter
@Setter
public class SendCaseDataRequest {

    private Long code;


}
