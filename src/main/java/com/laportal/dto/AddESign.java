package com.laportal.dto;

public class AddESign {

    private long rtaCode;

    private String eSign;

    public long getRtaCode() {
        return rtaCode;
    }

    public void setRtaCode(long rtaCode) {
        this.rtaCode = rtaCode;
    }

    public String geteSign() {
        return eSign;
    }

    public void seteSign(String eSign) {
        this.eSign = eSign;
    }

}
