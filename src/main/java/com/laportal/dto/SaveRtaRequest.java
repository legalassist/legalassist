package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

public class SaveRtaRequest {

    private String accdate;

    private String acctime;

    private String address1;

    private String address2;

    private String address3;

    private String city;

    private String contactdue;

    private String description;

    private String dob;

    private String driverpassenger;

    private String email;

    private String englishlevel;

    private String firstname;

    private String greencardno;

    private String injdescription;

    private String injlength;

    private String insurer;

    private String landline;

    private String lastname;

    private String location;

    private String makemodel;

    private String medicalinfo;

    private String middlename;

    private String mobile;

    private String ninumber;

    private String ongoing;

    private String partyaddress;

    private String partycontactno;

    private String partyinsurer;

    private String partymakemodel;

    private String partyname;

    private String partypolicyno;

    private String partyrefno;

    private String partyregno;

    private String passengerinfo;

    private String policyno;

    private String postalcode;

    private String rdweathercond;

    private String refno;

    private String region;

    private String registerationno;

    private String reportedtopolice;

    private String scotland;

    private String title;

    private String vehiclecondition;

    private String circumcode;

    private List<String> injclasscodes;

    private String gaddress1;

    private String gaddress2;

    private String gaddress3;

    private String gcity;

    private String gemail;

    private String gfirstname;

    private String glandline;

    private String glastname;

    private String gmiddlename;

    private String gmobile;

    private String gpostalcode;

    private String gregion;

    private String gtitle;

    private String password;
    private String translatordetail;
    private String alternativenumber;
    private String medicalevidence;
    private String reportedon;
    private String gdob;
    private String yearofmanufacture;
    private String otherlanguages;

    private List<RtaPassengerRequest> passengers = new ArrayList<>();

    private List<RtaFileUploadRequest> files = new ArrayList<>();


    private Long advisor;
    private Long introducer;
    private String referencenumber;

    private String airbagopened;
    private String vehicleType;

    private String vdImages;
    private String injurySustained;

    public String getAccdate() {
        return accdate;
    }

    public void setAccdate(String accdate) {
        this.accdate = accdate;
    }

    public String getAcctime() {
        return acctime;
    }

    public void setAcctime(String acctime) {
        this.acctime = acctime;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContactdue() {
        return contactdue;
    }

    public void setContactdue(String contactdue) {
        this.contactdue = contactdue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDriverpassenger() {
        return driverpassenger;
    }

    public void setDriverpassenger(String driverpassenger) {
        this.driverpassenger = driverpassenger;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnglishlevel() {
        return englishlevel;
    }

    public void setEnglishlevel(String englishlevel) {
        this.englishlevel = englishlevel;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGreencardno() {
        return greencardno;
    }

    public void setGreencardno(String greencardno) {
        this.greencardno = greencardno;
    }

    public String getInjdescription() {
        return injdescription;
    }

    public void setInjdescription(String injdescription) {
        this.injdescription = injdescription;
    }

    public String getInjlength() {
        return injlength;
    }

    public void setInjlength(String injlength) {
        this.injlength = injlength;
    }

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMakemodel() {
        return makemodel;
    }

    public void setMakemodel(String makemodel) {
        this.makemodel = makemodel;
    }

    public String getMedicalinfo() {
        return medicalinfo;
    }

    public void setMedicalinfo(String medicalinfo) {
        this.medicalinfo = medicalinfo;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNinumber() {
        return ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getOngoing() {
        return ongoing;
    }

    public void setOngoing(String ongoing) {
        this.ongoing = ongoing;
    }

    public String getPartyaddress() {
        return partyaddress;
    }

    public void setPartyaddress(String partyaddress) {
        this.partyaddress = partyaddress;
    }

    public String getPartycontactno() {
        return partycontactno;
    }

    public void setPartycontactno(String partycontactno) {
        this.partycontactno = partycontactno;
    }

    public String getPartyinsurer() {
        return partyinsurer;
    }

    public void setPartyinsurer(String partyinsurer) {
        this.partyinsurer = partyinsurer;
    }

    public String getPartymakemodel() {
        return partymakemodel;
    }

    public void setPartymakemodel(String partymakemodel) {
        this.partymakemodel = partymakemodel;
    }

    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getPartypolicyno() {
        return partypolicyno;
    }

    public void setPartypolicyno(String partypolicyno) {
        this.partypolicyno = partypolicyno;
    }

    public String getPartyrefno() {
        return partyrefno;
    }

    public void setPartyrefno(String partyrefno) {
        this.partyrefno = partyrefno;
    }

    public String getPartyregno() {
        return partyregno;
    }

    public void setPartyregno(String partyregno) {
        this.partyregno = partyregno;
    }

    public String getPassengerinfo() {
        return passengerinfo;
    }

    public void setPassengerinfo(String passengerinfo) {
        this.passengerinfo = passengerinfo;
    }

    public String getPolicyno() {
        return policyno;
    }

    public void setPolicyno(String policyno) {
        this.policyno = policyno;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getRdweathercond() {
        return rdweathercond;
    }

    public void setRdweathercond(String rdweathercond) {
        this.rdweathercond = rdweathercond;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegisterationno() {
        return registerationno;
    }

    public void setRegisterationno(String registerationno) {
        this.registerationno = registerationno;
    }

    public String getReportedtopolice() {
        return reportedtopolice;
    }

    public void setReportedtopolice(String reportedtopolice) {
        this.reportedtopolice = reportedtopolice;
    }

    public String getScotland() {
        return scotland;
    }

    public void setScotland(String scotland) {
        this.scotland = scotland;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVehiclecondition() {
        return vehiclecondition;
    }

    public void setVehiclecondition(String vehiclecondition) {
        this.vehiclecondition = vehiclecondition;
    }

    public String getCircumcode() {
        return circumcode;
    }

    public void setCircumcode(String circumcode) {
        this.circumcode = circumcode;
    }

    public List<String> getInjclasscodes() {
        return injclasscodes;
    }

    public void setInjclasscodes(List<String> injclasscodes) {
        this.injclasscodes = injclasscodes;
    }

    public List<RtaPassengerRequest> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<RtaPassengerRequest> passengers) {
        this.passengers = passengers;
    }

    public List<RtaFileUploadRequest> getFiles() {
        return files;
    }

    public void setFiles(List<RtaFileUploadRequest> files) {
        this.files = files;
    }

    public String getGaddress1() {
        return gaddress1;
    }

    public void setGaddress1(String gaddress1) {
        this.gaddress1 = gaddress1;
    }

    public String getGaddress2() {
        return gaddress2;
    }

    public void setGaddress2(String gaddress2) {
        this.gaddress2 = gaddress2;
    }

    public String getGaddress3() {
        return gaddress3;
    }

    public void setGaddress3(String gaddress3) {
        this.gaddress3 = gaddress3;
    }

    public String getGcity() {
        return gcity;
    }

    public void setGcity(String gcity) {
        this.gcity = gcity;
    }

    public String getGemail() {
        return gemail;
    }

    public void setGemail(String gemail) {
        this.gemail = gemail;
    }

    public String getGfirstname() {
        return gfirstname;
    }

    public void setGfirstname(String gfirstname) {
        this.gfirstname = gfirstname;
    }

    public String getGlandline() {
        return glandline;
    }

    public void setGlandline(String glandline) {
        this.glandline = glandline;
    }

    public String getGlastname() {
        return glastname;
    }

    public void setGlastname(String glastname) {
        this.glastname = glastname;
    }

    public String getGmiddlename() {
        return gmiddlename;
    }

    public void setGmiddlename(String gmiddlename) {
        this.gmiddlename = gmiddlename;
    }

    public String getGmobile() {
        return gmobile;
    }

    public void setGmobile(String gmobile) {
        this.gmobile = gmobile;
    }

    public String getGpostalcode() {
        return gpostalcode;
    }

    public void setGpostalcode(String gpostalcode) {
        this.gpostalcode = gpostalcode;
    }

    public String getGregion() {
        return gregion;
    }

    public void setGregion(String gregion) {
        this.gregion = gregion;
    }

    public String getGtitle() {
        return gtitle;
    }

    public void setGtitle(String gtitle) {
        this.gtitle = gtitle;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTranslatordetail() {
        return translatordetail;
    }

    public void setTranslatordetail(String translatordetail) {
        this.translatordetail = translatordetail;
    }

    public String getAlternativenumber() {
        return alternativenumber;
    }

    public void setAlternativenumber(String alternativenumber) {
        this.alternativenumber = alternativenumber;
    }

    public String getMedicalevidence() {
        return medicalevidence;
    }

    public void setMedicalevidence(String medicalevidence) {
        this.medicalevidence = medicalevidence;
    }

    public String getReportedon() {
        return reportedon;
    }

    public void setReportedon(String reportedon) {
        this.reportedon = reportedon;
    }

    public String getGdob() {
        return gdob;
    }

    public void setGdob(String gdob) {
        this.gdob = gdob;
    }

    public String getYearofmanufacture() {
        return yearofmanufacture;
    }

    public void setYearofmanufacture(String yearofmanufacture) {
        this.yearofmanufacture = yearofmanufacture;
    }

    public String getOtherlanguages() {
        return otherlanguages;
    }

    public void setOtherlanguages(String otherlanguages) {
        this.otherlanguages = otherlanguages;
    }

    public Long getAdvisor() {
        return advisor;
    }

    public void setAdvisor(Long advisor) {
        this.advisor = advisor;
    }

    public Long getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Long introducer) {
        this.introducer = introducer;
    }

    public String getReferencenumber() {
        return referencenumber;
    }

    public void setReferencenumber(String referencenumber) {
        this.referencenumber = referencenumber;
    }

    public String getAirbagopened() {
        return airbagopened;
    }

    public void setAirbagopened(String airbagopened) {
        this.airbagopened = airbagopened;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVdImages() {
        return vdImages;
    }

    public void setVdImages(String vdImages) {
        this.vdImages = vdImages;
    }

    public String getInjurySustained() {
        return injurySustained;
    }

    public void setInjurySustained(String injurySustained) {
        this.injurySustained = injurySustained;
    }
}
