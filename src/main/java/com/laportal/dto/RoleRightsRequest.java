package com.laportal.dto;

import java.util.ArrayList;
import java.util.List;

public class RoleRightsRequest {


    private String roleCode;

    private List<String> pagesvalue = new ArrayList<>();

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public List<String> getPagesvalue() {
        return pagesvalue;
    }

    public void setPagesvalue(List<String> pagesvalue) {
        this.pagesvalue = pagesvalue;
    }
}
