package com.laportal.dto;

public class PerformHotKeyOnRtaRequest {

    private String elcode;
    private String rtacode;
    private String olcode;
    private String plcode;
    private String mncode;
    private String toStatus;
    private String solicitorCode;
    private String solicitorUserCode;


    public String getElcode() {
        return elcode;
    }

    public void setElcode(String elcode) {
        this.elcode = elcode;
    }

    public String getToStatus() {
        return toStatus;
    }

    public void setToStatus(String toStatus) {
        this.toStatus = toStatus;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public String getSolicitorUserCode() {
        return solicitorUserCode;
    }

    public void setSolicitorUserCode(String solicitorUserCode) {
        this.solicitorUserCode = solicitorUserCode;
    }

    public String getRtacode() {
        return rtacode;
    }

    public void setRtacode(String rtacode) {
        this.rtacode = rtacode;
    }

    public String getOlcode() {
        return olcode;
    }

    public void setOlcode(String olcode) {
        this.olcode = olcode;
    }

    public String getPlcode() {
        return plcode;
    }

    public void setPlcode(String plcode) {
        this.plcode = plcode;
    }

    public String getMncode() {
        return mncode;
    }

    public void setMncode(String mncode) {
        this.mncode = mncode;
    }
}
