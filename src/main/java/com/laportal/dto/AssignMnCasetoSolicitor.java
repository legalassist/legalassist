package com.laportal.dto;

public class AssignMnCasetoSolicitor {

    private String mnClaimCode;
    private String solicitorCode;
    private String solicitorUserCode;

    public String getMnClaimCode() {
        return mnClaimCode;
    }

    public void setMnClaimCode(String mnClaimCode) {
        this.mnClaimCode = mnClaimCode;
    }

    public String getSolicitorCode() {
        return solicitorCode;
    }

    public void setSolicitorCode(String solicitorCode) {
        this.solicitorCode = solicitorCode;
    }

    public String getSolicitorUserCode() {
        return solicitorUserCode;
    }

    public void setSolicitorUserCode(String solicitorUserCode) {
        this.solicitorUserCode = solicitorUserCode;
    }
}
