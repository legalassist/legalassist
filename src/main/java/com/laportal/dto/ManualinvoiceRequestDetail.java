package com.laportal.dto;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 2/26/2023
 * Time: 4:15 PM
 * Project : legalassist
 */
public class ManualinvoiceRequestDetail {

    private String description;

    private String amount;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
