package com.laportal.dto;

import java.util.Date;

public class SaveElRequest {

    private String accbookcopy;
    private String acccircumstances;
    private Date accdatetime;
    private String acclocation;
    private String additionalclaiminfo;
    private String agencyname;
    private String ambulancecalled;
    private String anyinvestigation;
    private String anyotherlosses;
    private String anypremedicalconds;
    private String anypreviousacc;
    private String anyriskassessments;
    private String anytreatmentreceived;
    private String anywitnesses;
    private String awaresimilaraccident;
    private String changestosystem;
    private String claimantsufferinjuries;
    private String clientattendedhospital;
    private String clientinjuries;
    private String clientpresentcond;
    private String clientseenaccbook;
    private String clientseengp;
    private String clientstillwork;
    private String companysickpay;
    private String mobile;
    private String dailyactivities;
    private Date dategpattendance;
    private Date datehospitalattendance;
    private Date dob;
    private String employeraddress;
    private String employercontactno;
    private String employername;
    private String employmentduration;
    private String estimatednetweeklywage;
    private String gpaddress;
    private String gpadvisegiven;
    private String gpname;
    private String hospitaladdress;
    private String hospitaladvisegiven;
    private String hospitalname;
    private String injuriessymptomsstart;
    private String jobtitle;
    private String lossofearnings;
    private String ninumber;
    private String noworkadvised;
    private String otherlossesdetail;
    private String periodofabsence;
    private String premedicalconddetail;
    private String previousaccdetail;
    private String protectiveequipment;
    private String remarks;
    private String reportedbywhom;
    private String trainingreceived;
    private String userofprocequip;
    private String witnessdetails;
    private Long advisor;
    private Long introducer;
    private String title;
    private String firstname;
    private String middlename;
    private String lastname;
    private String postalcode;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String region;
    private String acctime;
    private String wasclientwearing;
    private String email;

    public String getAccbookcopy() {
        return accbookcopy;
    }

    public void setAccbookcopy(String accbookcopy) {
        this.accbookcopy = accbookcopy;
    }

    public String getAcccircumstances() {
        return acccircumstances;
    }

    public void setAcccircumstances(String acccircumstances) {
        this.acccircumstances = acccircumstances;
    }

    public Date getAccdatetime() {
        return accdatetime;
    }

    public void setAccdatetime(Date accdatetime) {
        this.accdatetime = accdatetime;
    }

    public String getAcclocation() {
        return acclocation;
    }

    public void setAcclocation(String acclocation) {
        this.acclocation = acclocation;
    }

    public String getAdditionalclaiminfo() {
        return additionalclaiminfo;
    }

    public void setAdditionalclaiminfo(String additionalclaiminfo) {
        this.additionalclaiminfo = additionalclaiminfo;
    }


    public String getAgencyname() {
        return agencyname;
    }

    public void setAgencyname(String agencyname) {
        this.agencyname = agencyname;
    }

    public String getAmbulancecalled() {
        return ambulancecalled;
    }

    public void setAmbulancecalled(String ambulancecalled) {
        this.ambulancecalled = ambulancecalled;
    }

    public String getAnyinvestigation() {
        return anyinvestigation;
    }

    public void setAnyinvestigation(String anyinvestigation) {
        this.anyinvestigation = anyinvestigation;
    }

    public String getAnyotherlosses() {
        return anyotherlosses;
    }

    public void setAnyotherlosses(String anyotherlosses) {
        this.anyotherlosses = anyotherlosses;
    }

    public String getAnypremedicalconds() {
        return anypremedicalconds;
    }

    public void setAnypremedicalconds(String anypremedicalconds) {
        this.anypremedicalconds = anypremedicalconds;
    }

    public String getAnypreviousacc() {
        return anypreviousacc;
    }

    public void setAnypreviousacc(String anypreviousacc) {
        this.anypreviousacc = anypreviousacc;
    }

    public String getAnyriskassessments() {
        return anyriskassessments;
    }

    public void setAnyriskassessments(String anyriskassessments) {
        this.anyriskassessments = anyriskassessments;
    }

    public String getAnytreatmentreceived() {
        return anytreatmentreceived;
    }

    public void setAnytreatmentreceived(String anytreatmentreceived) {
        this.anytreatmentreceived = anytreatmentreceived;
    }

    public String getAnywitnesses() {
        return anywitnesses;
    }

    public void setAnywitnesses(String anywitnesses) {
        this.anywitnesses = anywitnesses;
    }

    public String getAwaresimilaraccident() {
        return awaresimilaraccident;
    }

    public void setAwaresimilaraccident(String awaresimilaraccident) {
        this.awaresimilaraccident = awaresimilaraccident;
    }

    public String getChangestosystem() {
        return changestosystem;
    }

    public void setChangestosystem(String changestosystem) {
        this.changestosystem = changestosystem;
    }

    public String getClaimantsufferinjuries() {
        return claimantsufferinjuries;
    }

    public void setClaimantsufferinjuries(String claimantsufferinjuries) {
        this.claimantsufferinjuries = claimantsufferinjuries;
    }

    public String getClientattendedhospital() {
        return clientattendedhospital;
    }

    public void setClientattendedhospital(String clientattendedhospital) {
        this.clientattendedhospital = clientattendedhospital;
    }

    public String getClientinjuries() {
        return clientinjuries;
    }

    public void setClientinjuries(String clientinjuries) {
        this.clientinjuries = clientinjuries;
    }


    public String getClientpresentcond() {
        return clientpresentcond;
    }

    public void setClientpresentcond(String clientpresentcond) {
        this.clientpresentcond = clientpresentcond;
    }

    public String getClientseenaccbook() {
        return clientseenaccbook;
    }

    public void setClientseenaccbook(String clientseenaccbook) {
        this.clientseenaccbook = clientseenaccbook;
    }

    public String getClientseengp() {
        return clientseengp;
    }

    public void setClientseengp(String clientseengp) {
        this.clientseengp = clientseengp;
    }

    public String getClientstillwork() {
        return clientstillwork;
    }

    public void setClientstillwork(String clientstillwork) {
        this.clientstillwork = clientstillwork;
    }

    public String getCompanysickpay() {
        return companysickpay;
    }

    public void setCompanysickpay(String companysickpay) {
        this.companysickpay = companysickpay;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDailyactivities() {
        return dailyactivities;
    }

    public void setDailyactivities(String dailyactivities) {
        this.dailyactivities = dailyactivities;
    }

    public Date getDategpattendance() {
        return dategpattendance;
    }

    public void setDategpattendance(Date dategpattendance) {
        this.dategpattendance = dategpattendance;
    }

    public Date getDatehospitalattendance() {
        return datehospitalattendance;
    }

    public void setDatehospitalattendance(Date datehospitalattendance) {
        this.datehospitalattendance = datehospitalattendance;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmployeraddress() {
        return employeraddress;
    }

    public void setEmployeraddress(String employeraddress) {
        this.employeraddress = employeraddress;
    }

    public String getEmployercontactno() {
        return employercontactno;
    }

    public void setEmployercontactno(String employercontactno) {
        this.employercontactno = employercontactno;
    }

    public String getEmployername() {
        return employername;
    }

    public void setEmployername(String employername) {
        this.employername = employername;
    }

    public String getEmploymentduration() {
        return employmentduration;
    }

    public void setEmploymentduration(String employmentduration) {
        this.employmentduration = employmentduration;
    }

    public String getEstimatednetweeklywage() {
        return estimatednetweeklywage;
    }

    public void setEstimatednetweeklywage(String estimatednetweeklywage) {
        this.estimatednetweeklywage = estimatednetweeklywage;
    }

    public String getGpaddress() {
        return gpaddress;
    }

    public void setGpaddress(String gpaddress) {
        this.gpaddress = gpaddress;
    }

    public String getGpadvisegiven() {
        return gpadvisegiven;
    }

    public void setGpadvisegiven(String gpadvisegiven) {
        this.gpadvisegiven = gpadvisegiven;
    }

    public String getGpname() {
        return gpname;
    }

    public void setGpname(String gpname) {
        this.gpname = gpname;
    }

    public String getHospitaladdress() {
        return hospitaladdress;
    }

    public void setHospitaladdress(String hospitaladdress) {
        this.hospitaladdress = hospitaladdress;
    }

    public String getHospitaladvisegiven() {
        return hospitaladvisegiven;
    }

    public void setHospitaladvisegiven(String hospitaladvisegiven) {
        this.hospitaladvisegiven = hospitaladvisegiven;
    }

    public String getHospitalname() {
        return hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname;
    }

    public String getInjuriessymptomsstart() {
        return injuriessymptomsstart;
    }

    public void setInjuriessymptomsstart(String injuriessymptomsstart) {
        this.injuriessymptomsstart = injuriessymptomsstart;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public String getLossofearnings() {
        return lossofearnings;
    }

    public void setLossofearnings(String lossofearnings) {
        this.lossofearnings = lossofearnings;
    }

    public String getNinumber() {
        return ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getNoworkadvised() {
        return noworkadvised;
    }

    public void setNoworkadvised(String noworkadvised) {
        this.noworkadvised = noworkadvised;
    }

    public String getOtherlossesdetail() {
        return otherlossesdetail;
    }

    public void setOtherlossesdetail(String otherlossesdetail) {
        this.otherlossesdetail = otherlossesdetail;
    }

    public String getPeriodofabsence() {
        return periodofabsence;
    }

    public void setPeriodofabsence(String periodofabsence) {
        this.periodofabsence = periodofabsence;
    }

    public String getPremedicalconddetail() {
        return premedicalconddetail;
    }

    public void setPremedicalconddetail(String premedicalconddetail) {
        this.premedicalconddetail = premedicalconddetail;
    }

    public String getPreviousaccdetail() {
        return previousaccdetail;
    }

    public void setPreviousaccdetail(String previousaccdetail) {
        this.previousaccdetail = previousaccdetail;
    }

    public String getProtectiveequipment() {
        return protectiveequipment;
    }

    public void setProtectiveequipment(String protectiveequipment) {
        this.protectiveequipment = protectiveequipment;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getReportedbywhom() {
        return reportedbywhom;
    }

    public void setReportedbywhom(String reportedbywhom) {
        this.reportedbywhom = reportedbywhom;
    }

    public String getTrainingreceived() {
        return trainingreceived;
    }

    public void setTrainingreceived(String trainingreceived) {
        this.trainingreceived = trainingreceived;
    }

    public String getUserofprocequip() {
        return userofprocequip;
    }

    public void setUserofprocequip(String userofprocequip) {
        this.userofprocequip = userofprocequip;
    }

    public String getWitnessdetails() {
        return witnessdetails;
    }

    public void setWitnessdetails(String witnessdetails) {
        this.witnessdetails = witnessdetails;
    }

    public Long getAdvisor() {
        return advisor;
    }

    public void setAdvisor(Long advisor) {
        this.advisor = advisor;
    }

    public Long getIntroducer() {
        return introducer;
    }

    public void setIntroducer(Long introducer) {
        this.introducer = introducer;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAcctime() {
        return acctime;
    }

    public void setAcctime(String acctime) {
        this.acctime = acctime;
    }

    public String getWasclientwearing() {
        return wasclientwearing;
    }

    public void setWasclientwearing(String wasclientwearing) {
        this.wasclientwearing = wasclientwearing;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
