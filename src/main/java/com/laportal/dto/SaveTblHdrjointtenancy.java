package com.laportal.dto;

import java.util.Date;

public class SaveTblHdrjointtenancy {

    private String taddress1;

    private String taddress2;

    private String taddress3;

    private String tcity;

    private Date tdob;

    private String temail;

    private String tfname;

    private String tlandline;

    private String tmname;

    private String tmobileno;

    private String tninumber;

    private String tpostcode;

    private String tregion;

    private String tsname;

    private String ttitle;

    private String tpostalcode;

    public String getTaddress1() {
        return taddress1;
    }

    public void setTaddress1(String taddress1) {
        this.taddress1 = taddress1;
    }

    public String getTaddress2() {
        return taddress2;
    }

    public void setTaddress2(String taddress2) {
        this.taddress2 = taddress2;
    }

    public String getTaddress3() {
        return taddress3;
    }

    public void setTaddress3(String taddress3) {
        this.taddress3 = taddress3;
    }

    public String getTcity() {
        return tcity;
    }

    public void setTcity(String tcity) {
        this.tcity = tcity;
    }

    public Date getTdob() {
        return tdob;
    }

    public void setTdob(Date tdob) {
        this.tdob = tdob;
    }

    public String getTemail() {
        return temail;
    }

    public void setTemail(String temail) {
        this.temail = temail;
    }

    public String getTfname() {
        return tfname;
    }

    public void setTfname(String tfname) {
        this.tfname = tfname;
    }

    public String getTlandline() {
        return tlandline;
    }

    public void setTlandline(String tlandline) {
        this.tlandline = tlandline;
    }

    public String getTmname() {
        return tmname;
    }

    public void setTmname(String tmname) {
        this.tmname = tmname;
    }

    public String getTmobileno() {
        return tmobileno;
    }

    public void setTmobileno(String tmobileno) {
        this.tmobileno = tmobileno;
    }

    public String getTninumber() {
        return tninumber;
    }

    public void setTninumber(String tninumber) {
        this.tninumber = tninumber;
    }

    public String getTpostcode() {
        return tpostcode;
    }

    public void setTpostcode(String tpostcode) {
        this.tpostcode = tpostcode;
    }

    public String getTregion() {
        return tregion;
    }

    public void setTregion(String tregion) {
        this.tregion = tregion;
    }

    public String getTsname() {
        return tsname;
    }

    public void setTsname(String tsname) {
        this.tsname = tsname;
    }

    public String getTtitle() {
        return ttitle;
    }

    public void setTtitle(String ttitle) {
        this.ttitle = ttitle;
    }

    public String getTpostalcode() {
        return tpostalcode;
    }

    public void setTpostalcode(String tpostalcode) {
        this.tpostalcode = tpostalcode;
    }
}