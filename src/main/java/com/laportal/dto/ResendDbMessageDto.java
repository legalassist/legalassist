package com.laportal.dto;

public class ResendDbMessageDto {


    private String dbmessagecode;

    public String getDbmessagecode() {
        return dbmessagecode;
    }

    public void setDbmessagecode(String dbmessagecode) {
        this.dbmessagecode = dbmessagecode;
    }
}
