package com.laportal.dto;

public class ResendImmigrationMessageDto {


    private String dbmessagecode;

    public String getDbmessagecode() {
        return dbmessagecode;
    }

    public void setDbmessagecode(String dbmessagecode) {
        this.dbmessagecode = dbmessagecode;
    }
}
