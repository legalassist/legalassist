package com.laportal.dto;

import java.math.BigDecimal;

public class DbStatusCountList {

    private BigDecimal statusCount;
    private String statusName;
    private BigDecimal statusCode;

    public BigDecimal getStatusCount() {
        return statusCount;
    }

    public void setStatusCount(BigDecimal statusCount) {
        this.statusCount = statusCount;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public BigDecimal getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(BigDecimal statusCode) {
        this.statusCode = statusCode;
    }
}
