package com.laportal.dto;


public class TblRtaflowRequest {

    private String companycode;
    private String compaingcode;
    private String statuscode;
    private String taskflag;
    private String editflag;
    private String usercategory;

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getCompaingcode() {
        return compaingcode;
    }

    public void setCompaingcode(String compaingcode) {
        this.compaingcode = compaingcode;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getTaskflag() {
        return taskflag;
    }

    public void setTaskflag(String taskflag) {
        this.taskflag = taskflag;
    }

    public String getEditflag() {
        return editflag;
    }

    public void setEditflag(String editflag) {
        this.editflag = editflag;
    }

    public String getUsercategory() {
        return usercategory;
    }

    public void setUsercategory(String usercategory) {
        this.usercategory = usercategory;
    }
}
