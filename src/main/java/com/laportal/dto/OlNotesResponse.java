package com.laportal.dto;

import com.laportal.model.TblElnote;
import com.laportal.model.TblOlnote;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: Murtaza Malik
 * Date: 2/7/2024
 * Time: 6:16 PM
 * Project : legalassist
 */

@Getter
@Setter
public class OlNotesResponse {

    private List<TblOlnote> tblOlnotes = new ArrayList<>();
    private List<TblOlnote> tblOlnotesLegalOnly = new ArrayList<>();
}
