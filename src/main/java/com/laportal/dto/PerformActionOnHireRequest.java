package com.laportal.dto;

public class PerformActionOnHireRequest {

    private String hireclaimcode;
    private String toStatus;
    private String reason;

    public String getHireclaimcode() {
        return hireclaimcode;
    }

    public void setHireclaimcode(String hireclaimcode) {
        this.hireclaimcode = hireclaimcode;
    }

    public String getToStatus() {
        return toStatus;
    }

    public void setToStatus(String toStatus) {
        this.toStatus = toStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}


