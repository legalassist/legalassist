package com.laportal.dto;


import java.util.ArrayList;
import java.util.List;

public class CompanyUserRequest {


    private String companycode;
    private String loginid;
    private String username;
    private String status;
    private String pwd;
    private List<String> rolecodes = new ArrayList<>();

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getRolecodes() {
        return rolecodes;
    }

    public void setRolecodes(List<String> rolecodes) {
        this.rolecodes = rolecodes;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
