package com.laportal.dto;


public class ModulePageRequest {

    private String compaigncode;

    private String companycode;

    private String pagedescr;

    private String pageicon;

    private String pagename;

    private String pagepath;

    private String pagestatus;

    private String moduleCode;

    private String islist;

    private String isnewcase;

    public String getCompaigncode() {
        return compaigncode;
    }

    public void setCompaigncode(String compaigncode) {
        this.compaigncode = compaigncode;
    }

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getPagedescr() {
        return pagedescr;
    }

    public void setPagedescr(String pagedescr) {
        this.pagedescr = pagedescr;
    }

    public String getPageicon() {
        return pageicon;
    }

    public void setPageicon(String pageicon) {
        this.pageicon = pageicon;
    }

    public String getPagename() {
        return pagename;
    }

    public void setPagename(String pagename) {
        this.pagename = pagename;
    }

    public String getPagepath() {
        return pagepath;
    }

    public void setPagepath(String pagepath) {
        this.pagepath = pagepath;
    }

    public String getPagestatus() {
        return pagestatus;
    }

    public void setPagestatus(String pagestatus) {
        this.pagestatus = pagestatus;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getIslist() {
        return islist;
    }

    public void setIslist(String islist) {
        this.islist = islist;
    }

    public String getIsnewcase() {
        return isnewcase;
    }

    public void setIsnewcase(String isnewcase) {
        this.isnewcase = isnewcase;
    }
}
