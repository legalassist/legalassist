package com.laportal.dto;

import java.math.BigDecimal;
import java.util.Date;

public class UpdateHireRequest {

    private long hirecode;

    private String accdate;

    private String acctime;

    private String address1;

    private String address2;

    private String address3;

    private String circumcode;

    private String city;

    private String claimrefno;

    private String contactdue;

    private String description;

    private String dob;

    private String driverpassenger;

    private String email;

    private String firstname;

    private String greencardno;

    private String hirenumber;

    private String injclasscode;

    private String injdescription;

    private BigDecimal injlength;

    private String instorage;

    private String insurer;

    private String landline;

    private String lastname;

    private String location;

    private String makemodel;

    private String medicalinfo;

    private String middlename;

    private String mobile;

    private String ninumber;

    private String ongoing;

    private String partyaddress;

    private String partycontactno;

    private String partyinsurer;

    private String partymakemodel;

    private String partyname;

    private String partypolicyno;

    private String partyrefno;

    private String partyregno;

    private String passengerinfo;

    private String policycover;

    private String policyholder;

    private String policyno;

    private String postalcode;

    private String rdweathercond;

    private String recovered;

    private String recoveredby;

    private String refno;

    private String region;

    private String registerationno;

    private String reportedtopolice;

    private String scotland;

    private String storage;

    private String title;

    private String usercode;

    private String vehiclecondition;

    private Date createdon;

    private Date lastupdated;

    private UpdateHireBuisnessDetail updateHireBuisnessDetail;

    public String getAccdate() {
        return accdate;
    }

    public void setAccdate(String accdate) {
        this.accdate = accdate;
    }

    public String getAcctime() {
        return acctime;
    }

    public void setAcctime(String acctime) {
        this.acctime = acctime;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCircumcode() {
        return circumcode;
    }

    public void setCircumcode(String circumcode) {
        this.circumcode = circumcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContactdue() {
        return contactdue;
    }

    public void setContactdue(String contactdue) {
        this.contactdue = contactdue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDriverpassenger() {
        return driverpassenger;
    }

    public void setDriverpassenger(String driverpassenger) {
        this.driverpassenger = driverpassenger;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGreencardno() {
        return greencardno;
    }

    public void setGreencardno(String greencardno) {
        this.greencardno = greencardno;
    }

    public String getHirenumber() {
        return hirenumber;
    }

    public void setHirenumber(String hirenumber) {
        this.hirenumber = hirenumber;
    }

    public String getInjclasscode() {
        return injclasscode;
    }

    public void setInjclasscode(String injclasscode) {
        this.injclasscode = injclasscode;
    }

    public String getInjdescription() {
        return injdescription;
    }

    public void setInjdescription(String injdescription) {
        this.injdescription = injdescription;
    }

    public String getInstorage() {
        return instorage;
    }

    public void setInstorage(String instorage) {
        this.instorage = instorage;
    }

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMakemodel() {
        return makemodel;
    }

    public void setMakemodel(String makemodel) {
        this.makemodel = makemodel;
    }

    public String getMedicalinfo() {
        return medicalinfo;
    }

    public void setMedicalinfo(String medicalinfo) {
        this.medicalinfo = medicalinfo;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNinumber() {
        return ninumber;
    }

    public void setNinumber(String ninumber) {
        this.ninumber = ninumber;
    }

    public String getOngoing() {
        return ongoing;
    }

    public void setOngoing(String ongoing) {
        this.ongoing = ongoing;
    }

    public String getPartyaddress() {
        return partyaddress;
    }

    public void setPartyaddress(String partyaddress) {
        this.partyaddress = partyaddress;
    }

    public String getPartycontactno() {
        return partycontactno;
    }

    public void setPartycontactno(String partycontactno) {
        this.partycontactno = partycontactno;
    }

    public String getPartyinsurer() {
        return partyinsurer;
    }

    public void setPartyinsurer(String partyinsurer) {
        this.partyinsurer = partyinsurer;
    }

    public String getPartymakemodel() {
        return partymakemodel;
    }

    public void setPartymakemodel(String partymakemodel) {
        this.partymakemodel = partymakemodel;
    }

    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getPartypolicyno() {
        return partypolicyno;
    }

    public void setPartypolicyno(String partypolicyno) {
        this.partypolicyno = partypolicyno;
    }

    public String getPartyrefno() {
        return partyrefno;
    }

    public void setPartyrefno(String partyrefno) {
        this.partyrefno = partyrefno;
    }

    public String getPartyregno() {
        return partyregno;
    }

    public void setPartyregno(String partyregno) {
        this.partyregno = partyregno;
    }

    public String getPassengerinfo() {
        return passengerinfo;
    }

    public void setPassengerinfo(String passengerinfo) {
        this.passengerinfo = passengerinfo;
    }

    public String getPolicycover() {
        return policycover;
    }

    public void setPolicycover(String policycover) {
        this.policycover = policycover;
    }

    public String getPolicyholder() {
        return policyholder;
    }

    public void setPolicyholder(String policyholder) {
        this.policyholder = policyholder;
    }

    public String getPolicyno() {
        return policyno;
    }

    public void setPolicyno(String policyno) {
        this.policyno = policyno;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getRdweathercond() {
        return rdweathercond;
    }

    public void setRdweathercond(String rdweathercond) {
        this.rdweathercond = rdweathercond;
    }

    public String getRecovered() {
        return recovered;
    }

    public void setRecovered(String recovered) {
        this.recovered = recovered;
    }

    public String getRecoveredby() {
        return recoveredby;
    }

    public void setRecoveredby(String recoveredby) {
        this.recoveredby = recoveredby;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegisterationno() {
        return registerationno;
    }

    public void setRegisterationno(String registerationno) {
        this.registerationno = registerationno;
    }

    public String getReportedtopolice() {
        return reportedtopolice;
    }

    public void setReportedtopolice(String reportedtopolice) {
        this.reportedtopolice = reportedtopolice;
    }

    public String getScotland() {
        return scotland;
    }

    public void setScotland(String scotland) {
        this.scotland = scotland;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getVehiclecondition() {
        return vehiclecondition;
    }

    public void setVehiclecondition(String vehiclecondition) {
        this.vehiclecondition = vehiclecondition;
    }

    public BigDecimal getInjlength() {
        return injlength;
    }

    public void setInjlength(BigDecimal injlength) {
        this.injlength = injlength;
    }

    public String getClaimrefno() {
        return claimrefno;
    }

    public void setClaimrefno(String claimrefno) {
        this.claimrefno = claimrefno;
    }

    public long getHirecode() {
        return hirecode;
    }

    public void setHirecode(long hirecode) {
        this.hirecode = hirecode;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public Date getLastupdated() {
        return lastupdated;
    }

    public void setLastupdated(Date lastupdated) {
        this.lastupdated = lastupdated;
    }

    public UpdateHireBuisnessDetail getUpdateHireBuisnessDetail() {
        return updateHireBuisnessDetail;
    }

    public void setUpdateHireBuisnessDetail(UpdateHireBuisnessDetail updateHireBuisnessDetail) {
        this.updateHireBuisnessDetail = updateHireBuisnessDetail;
    }
}
