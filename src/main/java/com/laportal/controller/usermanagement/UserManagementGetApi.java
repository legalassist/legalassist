package com.laportal.controller.usermanagement;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.CompanyProfileResponse;
import com.laportal.model.*;
import com.laportal.service.user.UsersService;
import com.laportal.util.JWTSecurity;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

@RestController
public class UserManagementGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(UserManagementGetApi.class);
    @SuppressWarnings("unused")
    @Autowired
    private Environment env;
    @Autowired
    private UsersService usersService;

    @SuppressWarnings("resource")
    @RequestMapping(value = "/testService", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> testService(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == testService(); ");
//
//            SimpleMailMessage msg = new SimpleMailMessage();
//            msg.setTo("malikmurtaza@hotmail.com", "murtazamalik47@yahoo.com");
//
//            msg.setSubject("Testing from Spring Boot");
//            msg.setText("Hello World \n Spring Boot Email");
//
//            javaMailSender.send(msg);
            File file = new File("E:\\testupload\\A\\");
            if (!file.exists()) {
                if (file.mkdir()) {
                    System.out.println("Directory is created!");
                    String S = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAC2AOUDASIAAhEBAxEB/8QAHQABAAIDAQEBAQAAAAAAAAAAAAYHBAUIAQMCCf/EAFsQAAEDAwEFAQcLDgsECwAAAAEAAgMEBREGBxIhMUFRExciMmFx0ggUFSNSVYGRlLHRFhgkM0JDYnKSk6GywdMlNTZTVFZ0goOj8EaVs8I0N0RFY2V1hKKkw//EABwBAQEAAgMBAQAAAAAAAAAAAAABAgYDBQcIBP/EAD4RAAIBAgMFAgkLAwUAAAAAAAABAgMRBAUhBhIxQVEXYRMiU3GBkZKh0gcVIzJCUnLC0eHwFBZiQ1SCorH/2gAMAwEAAhEDEQA/AP6eoiIAiIgCIiAIiIAiIgC1mo9QUGmLRNd7i/EcQw1gPhSPPJrfKf0DJ5BZ1VVU9DTS1lZMyGCBhkkkecNa0DJJPYudNf61qdZXcysLo6CmJbSQkYIHu3fhO7OgwO1attXtJT2ewm9HWrLSK/M+5e96eaN2MW6661Xda+avfe66nMri4Q09S9kcY6NABxwHDJWMdV6pPPUl1+WSektUi+fKma42rN1Klabb1erOM2n1Vaoxj6pLtj+2yekrN2SWPUdye3VN8vVyfRtyKSCSrkcJjyL3Ani0dO0+QDMQ2baEk1dc/XNaxzbXSOHd3cu6u5iJp/WPQcOoXQkUUcETIYY2xxxtDWNaMBoHIAdAvS9g8ixWNms0xs5Omn4ibfjNfa/CuXV9y1yiuZ+kRF7CZhERAEREAREQBERAEREAREQBERAEREAREQBERAERVpta1+bTTu0zZp8Vs7fsmVp4wRkeKD0c4fEPOCuszfNcPkuEljMS/FXLm3yS73+/AjdiM7V9oHs3UO05ZpwbfTu9vlYeFRIOOM+4H6T5hmuEAAGAi+as3zbEZzi54vEu7lw6Jcku5fvxONu4W50ppev1beI7VRZYD4U827lsMY+6PlPQdTjsWvt1urbvXw2y3U5mqah25GwHGTjJyewDr0XSGitIUWjrO2hg3ZKmTD6mfGDI/wBEcgP2krvtj9mJbQYrfqr6CH1n1/xXn59F51epXNnZrPQWG2wWm2Q9zp6du60ZyT2knqSeJKzERfQ9OnCjBU6atFKyS4JI5AonrXabpjQlVQUV5neZq6QDciG8YYs4Mrx0aD8J44BwV9NoevbZs/sTrnV4mqpcx0dLvYdNJ+xo5k/tIB5Fvd7ueo7tU3q8VJnq6p++9x4AdjR2NHIDkAAFmRs7ghmiqImTwStkikaHsew5a5pGQQeoX6XPuwfal6wki0PqGp+xpXbtumeftTz95J9yT4vYTjkRjoJChERAEREAREQBERAEREAREQBERAEREARFrdQ3+36ZtUt2uUmI48BrR40jjyaPL8wyeQXHWrU8PTlVqu0Yq7b4JIGm2h64p9HWr2lzH3KqBbSxHjjtkcPcjPwnh2kc7VE89VPJVVMrpZZnmSR7zlznE5JJ6klZt/v1w1LdZrvcngzTHg0co29GtHYB8ZJK1y+ctq9pam0OLvHSlHSEfzPvfu0XU427heta5zg1rSSTgADiSvFbuyHQBHc9W3qnxnDqCJ4/zSP1fj7CuuyLJcRn2MjhaC/E+UVzb/mrsglckWzHQLdK0PslcYwbpVtG+D94ZzDB5e3y8OmTOURfSmW5dh8qw0MJhY2hH+NvvfM5ErBfGtqHUdHPVsppah0MbpBDEMvkIGd1oPU8gvsi/cDi3W2r7xrW/T3m7uc1xJjhgGd2njB4MaPnPU5J6BaFX5t02Smbu+udNU2XgGS400beLuHGZoHXHjDr43Q5oPmhieglp3mkgg9DggrprYntUGq6IaavtQPZekZ7VI48aqIDn+OOvbz7VzIsigr6211kFxt9S+Cop5BLFKw4cxwOc/SOxAjuhFC9l20aj2gWQSvLIrpSgNrIAevR7R7l36Dw7CZohkEREAREQBERAEREAREQBERAEREB8quqpqGmlrKydkMEDDJJI84a1oGSSVzpr/Ws+srv3ZgdHQUxLKSJ3A46vd+E7s6DA6Fb7avr83yqdp20T5t1O726Rh4VEg44z7lv6Tx6DNcrw/bzav5wqPLcHL6KL8Zr7Uly/Cve/MjCTCIt/ozSNbrC7tt9MTFAwb9TPjIjjz+seQHnPavPMJha2OrRw+HjvSk7JLmzE3my/QLtT14utyiPsXSv4gjhO8cdweT3XxdeF+gBoDWgADgAFj263UdpoYbbb4Gw09OwMYxvQftPUlZC+kdmtn6Oz2DVCOs5azl1f6LkvTxbORKwREWxFCIiALm3bdsnOnKiTVunKX+Cp371VDG3hSyE+MB0jJ/JPDkQF0kvxUU8FVBJS1ULJYZmGOSN7Q5r2kYIIPMEIRq5wkisPa7suqNBXT19bWPksla8ineeJp3czC4+bxT1HPiMqvEIbjSmqLro+9099tEobNCcPYfFlYfGY4dhGPLyPRdfaP1Za9aWKC+Wp/gSDdkjJy6KQc2H/XEYK4pUx2Z7Q67Z/fBVDelt1SWsrIAfGZnxgPdN6fCORyATOv0WNbLnQ3m3wXS2VLKilqWCSKRh4Oaf9cuiyUMgiIgCIiAIiIAiIgCIiAKstrmv/YuB+l7RNisqGfZUrT9pjP3I/CI59g84IkW0PW8Oj7T7QWvuVUC2mjIzu9sjh2DPLqeHaRztUVE1VPJVVMzpZpnmSSRxy5zickk9SSvNNvNq/m+m8swcvpZLxmvsp8vO/cteaMZPkfMAAYCIv0xrnubGxpc5x3WgDJJ7AvEDAybTaa++XKC1W2Ay1E7g1jeg7S7sAHEnoAV0lpDStDpCzR2uk8OQ+HUTEYMsmOLvIOgHQLTbNdBx6St3ryuY110q2+2kcRE08e5g/pJ6nzBTRe97EbKrJqP9Zil9PNcPurp53z9XW+cVYIi8c5rGl73BrWjJJOAAt/Mj1FEJtr2zWBxa/V9CSOB3N5/zArGfts2Xs56qiP4tNM75mICcIq/k287LY/8AaKR34tDUH/kWPJ6oPZkzxLnWSfi0Mo+cBAWQiq9/qjNnbPF9lH/i0mPncF8X+qT0C3xaG9P81NH+2QILllXi0W6/Wyps92pWVFJVxmOWN3UH5iOYI4ggFcj7SNn1y2fXx1BPvzUFQS+iqsfbWZ4td2PHAH4DwB4XNJ6prRTftdjvr/8AChH/AOij+s9uOgtaWGosNx0peHxyjeilJha+GQDwZGneOCM48oyMEEgiMo1EGcceaIYlobGNqbtG3AWK9zk2Wsf4xPCmkPDfH4J+6Hw9uen2PZIxskbg5rgC1wOQR2hcIK9dhG1XcMOhdRVPgkhltqJHcj/Mk/q/F2IVMvtERDIIiIAiIgCIiALV6k1Fb9L2ma7XF3gR+CyMHwpXnkxvlP6Bk9FnVlZS2+llrq2dkMEDDJJI44DWjmVznrzWdVrK7uqPDjoacllLA7hhvV57HHn8Q6EnVdrNpaez2EvHWrPSK/M+5e96dbRuxqtQX64alus13uTw6abADR4sbRya0dgHxkla5EXzlWrVMTUdWq7ybu2+Lb5nGFcWyLQHrdkerrzB7a8b1DE4eI0/fSO0jl2Dj1GI5st2f/VLWC9XaH+C6V+Gsd/2iQHxfxR912nh24vkAAYC9V2A2U8I1m+Njp9hPn/k/wAvXj0vlFcwiIvYjMKhtvW1QkzaD09U8PFudRG7/Iaf1/yfdBXlcKaWtop6SGslpHzMLBPDjfjz903IIz2ZBVUP9TNo973PN/vu845J7pCcn82hGc2oukfrZNGe/wBffzkH7pfr62XRPv5fvzsH7pCWObEXSv1s+hut4vp/x4f3S9HqaNC9btfj/jw/ukFjmlF0v9bToT30vvyiL90vfratB9bnfT/7iL92gsczoumR6mvQI/7wvh89TH+7Qepr0B1rr27z1Mf7I0FjmZF04PU3bPhxNTeD56lnoKM7Q/U+W+12CS7aKkrp6qkBkmppniQyx44lmGg7w5445AIAzhBYoletc5h3mEggggg4IPavOGBxyCiEOndiu1Rur6Eadvk49maNngPceNXEPuvK8cN7t59oFprhe33CttNdBc7bUyU9VTPbLFKw8Wuzz+cEdQutdmG0ai2g2QTnchudKAyspweTuj29dx2OHZxHlIyTJmiIhQiIgCIqv2ubQDb4n6Vss+KqZv2ZK0/aYz9wD7o9ewefI6vOM2w+SYSWMxL0XBc2+SXe/wB+BG7Eb2rbQPZ+qNgtE2bbTPzLIw5FRIOIwfcjp2nj0BVdpyRfNebZriM5xc8XiXeUvUlyS7l/NTjbuFItD6NrNZXcUbC+Ojiw+qnH3tueDR+EeQHQeQEKOqYaZ2nXrSlrZarXbbZ3MPc975I3mSRxOSXEOGcDAHmC5MkWX/1sZZm2qS1dldvu7r8304alXE6BoaKkttHDQUMDYaeBgZGxo4NAX3VGjbnqwHwrdaSPJFIP+dejbrqkO8K12sjyMk9Ne2R+UHIYpRU2lw+qzLeReKKjxt01P1tVrP8Adk9NejbrqXrabZ+TJ6SyXyg5E/8AUfssu8i70VJd/bUXvNbvgL/pXvf31B7y278p/wBKvaDkPlX7Mv0G8i7EVKd/i/dbFQflvXvf4vvvDQfnHq9oGQ+Vfsy/QbyLqRUsNvF662Ci/OvXvf5vHXT1H+ef9Cv9/wCQ+Wfsy/Qm8i6EVM9/q6f1bpflLvRXvf7uX9Wqb5S70U/v/IPLP2J/CXeRcqKm+/3cP6sU/wArPoqwNDa3ota219TFEKerp3BtRTl29u55OB4Zae3HMEdF2WW7VZTm9dYbCVrzaulZq9uNrpeoJpkkREWwlOc9u+y0WSpk1pYKbFvqX/Z0LBwp5XH7YB0Y44z2O/G4U2u7Kmmp62mlo6uBk0E7HRyxyNDmvYRgtIPMELkzavs4qdn98+xw+W0VrnPopic7nbC4+6Hb900ducDFogy3GlNU3XRt8p7/AGiQCaHg9h4Mmj+6jcOwj4iAea06IQ7X0hqy1a0sUF+tEhMcvgyRu8eGQeMx3lH6QQRwK3K4+2abQ6/Z7fBVs357dUkMraYHx254PA6OHTt5cjkdb2u50F6t1PdbXVMqKSqjEkUrDkOaf9cuhQyTMpERCkQ2j66i0fa+50r2PulU0injPHcHIyOHYOg6nyZI55llknlfUTyOkkleXve45LnE5JJ6kldOXPRml7zVurrpZaepqHgAySAl2AMAZzwCwzs10KTn6mqT4N4ftXm21Oyeb7R4pT8LBUo/Vj43renF8/QjFq5zYi6R72Wgzz03TflP+led7HQeMfU3T/lP+lav2YZn5Wn/ANvhJus5vRdId7DQY/2cg/Lf6Sd7DQeMfU7D+ck9JTswzTytP1y+EbrOb0XRx2XaCIwdPRfnZPSXh2WaCPPT0f56X0k7MM08rT9cvhG6znJF0WdlGgDzsA+UzemvO9NoD3h/+1N6ax7Mc28pT9cvhG6znVF0V3ptAe8R+VTemvDsl0Aedjd8qm9NTsxzfylP1y+EbrOdkXQ3eh0B7zS/LZ/TTvQaA955flc3pKdmWceUp+uXwDdZzyi6F7z+gfemb5XL6S/Ped0D71z/ACyX0lOzPOPv0/al8I3Wc+Iugu85oP3uqPlcn0rzvNaE/oFT8rk+lTszzf71P2pfCN1nPy22l9R12lLzDeaLwizwZYicNljPjNP+uBAPRXV3mdCf0Kq+VyfSvO8xob+i1fyly5cP8nme4SrGvRnTU4u6alLRr/iN1kts93ob7bae7W2bulPUM3mnkR2gjoQcgjoQsxaXTGkrVpKCaltD6kQzvEhjllL2tdjGRnlkYz5gt0vasG8RKhB4pJVLeNbVX7u4zC1OqdM2rV9jqbDeId+CobwcPHiePFe09HA8R8XIlbZF+kHGOqNA6l0reqiy1lsqqgwnMU8ED3RzRnxXggdnMdOR4jK1PsLePeiv+Sv9FdyIhLHDosV7PKy3H5LJ6Ks3YvrTUGi7kLFebVcjY66QZc6ll+xJTykHg+IeAcB2b3PO90qiCwBBGQeCIiFNbqS+0+mrLU3uqglmipQ0uZHjedlwaMZ4cyvjpPU1Lq2zMvNJTTQRvkfHuS43gWnB5Ehazapn6gbrg48GL/isWBsW/kNF/ap/1lrksxxC2hjl9/o3S37WX1t63Hjw5EvrYnSIi2MoREQEW1ptAt+iZaSKtoKqpNY17mmHdw3dxnO8R2hPqvv5DS3Z5eSHDPGWnBHn9sUI28fxjYcc9yo/WjVwN8UeZanhcVjcwzbGYTwzhCl4Pdso/ajd3bTIRY6v1DgEbO7zx/8AGp+H+YttY7rX3WKV9fYKu1ujcA1lQ+NxeO0bjitmi76hha9Ke9OvKS6NQS90U/eUIiL9wNLqXWFg0lTtmvNaI3SAmKFg3pJMdjR08pwPKtNSa31Rd4hVWTZ5WyUzvEkrKuOmLh27pyceUZUH0rEzXe1SuuV1HdoKMyzRRv4tLY3iOJuOwZ3vKc9pV1rU8rxmN2g8JiadTwVFScY7qi5StpvNyUku5JenrE7kOqtbaktLDU3zZ/XRUrBmSWkqo6ktHU7owceUrdac1XYtV0pqrLWiYMx3SNwLZIyeQc0/PyPQrbqH2/ZzTWjWEmqrXc5KaObe7pRMiHc3Bw8IZzy3vC5cCuwnDM8HXpunLw1Nu0lLdUor7ya3U0uatfoUmCIi7wBEUY2iaqGk9NT1kMjW1lR7RSg/zhHjf3QC74PKvzY3F0sBh54qu7Rgm36P5oDS3fbRp603WptnsfW1IpZe5Pnh3CwkcDjLgeByPOCp9T1ENXBHVU0rZIpmCSN7TkOaRkEfAq60fs4ids8qbdcW7lbe2Coc97cuhIGYR/d4OI7XOC92O3+d1HV6MumWVtoe4MY48e572C3y7ruHmc1aplOa5lTxVKlmtksRFyhZW3ZJ33H18Vp3et00RX5ljoiLdChRS57Q7fa9X02j5bdVPnqXRNEzd3cBk5Z45/QpWqa1eSNtdqIP3yj/AFitd2kzHEZbQozw7s5VYReiejbvxI3YuVERbEUIiICJ7Vv5AXXzRf8AFYsDYr/IaL+1T/rLZbUIZZ9CXWKGN73lsZDWNJJxK08gojsy1nYtN6Xbary+rgqG1Er931nM4brjkcWtK0bG4mjgtqoVsTNQi6DV27K+/wBWTmWuiinfQ0b/AE2s4/8Al9R6CybXr/TF5uEdroKqpfUS53Q6imY3gCfGc0AcAeq2aGdZbVkoQxEG27JKSu30WpbkiREXZgp3b00OuNjaTwMc4P5UanEWzTS7IwweyeMcc3Oo48Px1DNudPPNcLG+Kmlka1k++Y2F2PCj7FMm7TtHBozW1YI4EG31GR/8F53h45Ys+zCWZbnGnu7+79zW1/Rcx0vqfvvb6ZxgOufLH8Z1Hprf2y3U9poYbdSmUxQDdaZZC9x454udxPPqo8dp2jgN717V88fxfUegtpYtU2bUhqG2maaQ0273TulPJFjezjG+0Z5HktowNXJY1lHAyp77VvFcbtceXmuVW5G2REXeFKX2Oj1nru9UEpAkbDM3jzJbMAf2q5pmyOhe2GQRyFpDHluQ12OBx18yrDVukdRae1c3XukKU1ge7fqaQeMCRh+Bzc1w7OIcc8ekit+1TR9TEBcbgbVUge2U9ax0bmHqMkYPxrR9nK9LIqVTKsfLwbjKW65OylFu6cZaJvqr3XMxWmh+xY9ou6AdeUWep9h2/vFEbvrPW+nNZ0Omau90lcyeam7o9tE2PDJH7pbwJ446+VS2r2n6Va0xWeqkvFW4YipqGN0jnu6DexgfCVWFxtWrXa6tN91HbHRTXSugmEceXiJjZGNDSW8G4bjnxxnK6zP8XSw9On80Vpzlvx3pKpUnGMb21e843basuLK+4vxERellCpHUd8pdY7RIm1NNXVljs8hjcykpnzb+OJcWtB8Fz2huerR5VYW0zUVRp/TMwt7ZXV1bmngMTC4syPCfw5YGcfhFq+Wy3Sh0xpqN1VHu11wxUVAI4sGPAj/ujn+EXLTs8hUzrH08pou0IWqVHa60fiQaur3erXRJkeuhkd8S15x7C6gz/wClTfQq31ffG2nWtHrmxWu5wNO6KptVRyQMkdjBaC4YJczh525V4rV6nsNPqWxVdlqSAKhmGPx4jxxa74CAV+jO8mx2ZYRwVZb8GpwajZqUdVrvO1+D05hpmdRVlNcaOGvo5RLBURtljeOTmkZBX2VZbHLvcaVtZoy8U00UtE50lPvsIAG9iRgJ54ccjtDj2KzV2uTZks2wUMVazf1l0ktGvQ/cE7hU1q7/AK7LXn+co/nKuVUvrvulJtboblJTVDqenNJLI+OF78BriTjdHHgOS6LbVqOFw85cFWpv/wBEi6EUUO1DRoODXVf+76j0F530dG/02s/3fUegu8+fcr/3MPbj+ouiWIsGy3u3agovZC2SSvgLyzMkL4jkc/BeAf0IuxpVYVoKpTacXwa1TKZyLAv15pNPWeqvNbvGKljL9xvjSO5NY3tc5xDQOpIUStM+pdL3q2O1Rc5aun1I0xTB7sx0Fw4vZFGekbmlzBnPhRs6uXICeIiIAiIgCKG2qepZPXHW09wgqvXcohcx8sdG2m3j3LcfHhni4zvnf3s54YW7sUAbNUVVDqI3G2zBohic8TdweM72JclzgeHB2SCDxwcADboiIAiLQ6wdfW0tF7DtnMHrxnsiabHrgUuDvdzz13t3OPC3c7vHCA3yFrXcwD51F6g2augFJa9TVdsuL/8Aozp6iXugfjhmGY+GO1pHxc1JoxII2iVzXPAG85rcAnqQMnHxoD9AAcAMIiIAiKB7O7vcrjqrXFJXXCeoiobqyKmZI8ubCzcPgt6AZ6BATxERAERRrWT78ya0+x7as2o1LhdjRDNSItw7m7jwt3fxvbnh45dUBJUUUnFquG5TWDVk9uugc0xNnqJJHENIy19PM4FwIyDwB45BB4qVoAiIgCLx7BIx0bi4BwIO64g/ARxCgmso5rbqLSFFQ3S5QwXK5yU9WwV0x7rGKeR4BJcSPCaOIwgJ4ix6Ohhog8QyVDt/Ge7VEkvLs33HHwIgNLqTTVTqa72qOtdCbJQPdVzwFx36ioHCEEYxuNy5/jcXBvDhlfTUujrbqGy1FrO/DK5u/TVAe4up528Y5G8ebXAH4MIiA2dnNzdaqQ3n1v6/ELRUmnJMRkA8ItyAcE5xkLLREAREQEcpKvUViidS32anuY7q7uFWw9ylkYTlokjDN0EA4y04OM4HJfm02CvOrKrV1W6lpW1FG2kbS0rnOEuH7wmleQ3eePFA3eAJ4nPAiAkqIiALV3yn1A91HVWCup43U0xdUUtQ3EdVGWkFpeAXRkHDg4A8sEEHgRAam9S1+qbXWafp7Xb3PqYnxSGrlc6OPPDfDQzLyDxAy3kOI5rfWa3utFoorU+slq3UdPHAZ5jl8u60Ded5TjJREIjMREQoUN0PpS56f1Fq26V01M+G9XBtTTCJ7i5rQDkPBaADx6EoiAmSIiALT3un1KK6huFir6fuFP3RtXQVA3WVLXAbrhKGucxzSMjAIOSD0IIgNNqanr9c2St05S0VBEKhncpKipkc805P3yNgb4T282+E3BwcqWUkBpaWGlM8kxhjbGZJDl78DG849SeZREIj6oiIUKN6o07X3m/aYuVJJTtis1fJVTiRxDnMdA9gDAAQTl45kIiAkiIiA//Z";
                    byte[] imageByte = Base64.decodeBase64(S);
                    String directory = file.getAbsolutePath() + "/sample.jpg";
                    new FileOutputStream(directory).write(imageByte);
                } else {
                    System.out.println("Failed to create directory!");
                }
            }
            System.out.println("Directory is created!");
            String S = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAC2AOUDASIAAhEBAxEB/8QAHQABAAIDAQEBAQAAAAAAAAAAAAYHBAUIAQMCCf/EAFsQAAEDAwEFAQcLDgsECwAAAAEAAgMEBREGBxIhMUFRExciMmFx0ggUFSNSVYGRlLHRFhgkM0JDYnKSk6GywdMlNTZTVFZ0goOj8EaVs8I0N0RFY2V1hKKkw//EABwBAQEAAgMBAQAAAAAAAAAAAAABAgYDBQcIBP/EAD4RAAIBAgMFAgkLAwUAAAAAAAABAgMRBAUhBhIxQVEXYRMiU3GBkZKh0gcVIzJCUnLC0eHwFBZiQ1SCorH/2gAMAwEAAhEDEQA/AP6eoiIAiIgCIiAIiIAiIgC1mo9QUGmLRNd7i/EcQw1gPhSPPJrfKf0DJ5BZ1VVU9DTS1lZMyGCBhkkkecNa0DJJPYudNf61qdZXcysLo6CmJbSQkYIHu3fhO7OgwO1attXtJT2ewm9HWrLSK/M+5e96eaN2MW6661Xda+avfe66nMri4Q09S9kcY6NABxwHDJWMdV6pPPUl1+WSektUi+fKma42rN1Klabb1erOM2n1Vaoxj6pLtj+2yekrN2SWPUdye3VN8vVyfRtyKSCSrkcJjyL3Ani0dO0+QDMQ2baEk1dc/XNaxzbXSOHd3cu6u5iJp/WPQcOoXQkUUcETIYY2xxxtDWNaMBoHIAdAvS9g8ixWNms0xs5Omn4ibfjNfa/CuXV9y1yiuZ+kRF7CZhERAEREAREQBERAEREAREQBERAEREAREQBERAERVpta1+bTTu0zZp8Vs7fsmVp4wRkeKD0c4fEPOCuszfNcPkuEljMS/FXLm3yS73+/AjdiM7V9oHs3UO05ZpwbfTu9vlYeFRIOOM+4H6T5hmuEAAGAi+as3zbEZzi54vEu7lw6Jcku5fvxONu4W50ppev1beI7VRZYD4U827lsMY+6PlPQdTjsWvt1urbvXw2y3U5mqah25GwHGTjJyewDr0XSGitIUWjrO2hg3ZKmTD6mfGDI/wBEcgP2krvtj9mJbQYrfqr6CH1n1/xXn59F51epXNnZrPQWG2wWm2Q9zp6du60ZyT2knqSeJKzERfQ9OnCjBU6atFKyS4JI5AonrXabpjQlVQUV5neZq6QDciG8YYs4Mrx0aD8J44BwV9NoevbZs/sTrnV4mqpcx0dLvYdNJ+xo5k/tIB5Fvd7ueo7tU3q8VJnq6p++9x4AdjR2NHIDkAAFmRs7ghmiqImTwStkikaHsew5a5pGQQeoX6XPuwfal6wki0PqGp+xpXbtumeftTz95J9yT4vYTjkRjoJChERAEREAREQBERAEREAREQBERAEREARFrdQ3+36ZtUt2uUmI48BrR40jjyaPL8wyeQXHWrU8PTlVqu0Yq7b4JIGm2h64p9HWr2lzH3KqBbSxHjjtkcPcjPwnh2kc7VE89VPJVVMrpZZnmSR7zlznE5JJ6klZt/v1w1LdZrvcngzTHg0co29GtHYB8ZJK1y+ctq9pam0OLvHSlHSEfzPvfu0XU427heta5zg1rSSTgADiSvFbuyHQBHc9W3qnxnDqCJ4/zSP1fj7CuuyLJcRn2MjhaC/E+UVzb/mrsglckWzHQLdK0PslcYwbpVtG+D94ZzDB5e3y8OmTOURfSmW5dh8qw0MJhY2hH+NvvfM5ErBfGtqHUdHPVsppah0MbpBDEMvkIGd1oPU8gvsi/cDi3W2r7xrW/T3m7uc1xJjhgGd2njB4MaPnPU5J6BaFX5t02Smbu+udNU2XgGS400beLuHGZoHXHjDr43Q5oPmhieglp3mkgg9DggrprYntUGq6IaavtQPZekZ7VI48aqIDn+OOvbz7VzIsigr6211kFxt9S+Cop5BLFKw4cxwOc/SOxAjuhFC9l20aj2gWQSvLIrpSgNrIAevR7R7l36Dw7CZohkEREAREQBERAEREAREQBERAEREB8quqpqGmlrKydkMEDDJJI84a1oGSSVzpr/Ws+srv3ZgdHQUxLKSJ3A46vd+E7s6DA6Fb7avr83yqdp20T5t1O726Rh4VEg44z7lv6Tx6DNcrw/bzav5wqPLcHL6KL8Zr7Uly/Cve/MjCTCIt/ozSNbrC7tt9MTFAwb9TPjIjjz+seQHnPavPMJha2OrRw+HjvSk7JLmzE3my/QLtT14utyiPsXSv4gjhO8cdweT3XxdeF+gBoDWgADgAFj263UdpoYbbb4Gw09OwMYxvQftPUlZC+kdmtn6Oz2DVCOs5azl1f6LkvTxbORKwREWxFCIiALm3bdsnOnKiTVunKX+Cp371VDG3hSyE+MB0jJ/JPDkQF0kvxUU8FVBJS1ULJYZmGOSN7Q5r2kYIIPMEIRq5wkisPa7suqNBXT19bWPksla8ineeJp3czC4+bxT1HPiMqvEIbjSmqLro+9099tEobNCcPYfFlYfGY4dhGPLyPRdfaP1Za9aWKC+Wp/gSDdkjJy6KQc2H/XEYK4pUx2Z7Q67Z/fBVDelt1SWsrIAfGZnxgPdN6fCORyATOv0WNbLnQ3m3wXS2VLKilqWCSKRh4Oaf9cuiyUMgiIgCIiAIiIAiIgCIiAKstrmv/YuB+l7RNisqGfZUrT9pjP3I/CI59g84IkW0PW8Oj7T7QWvuVUC2mjIzu9sjh2DPLqeHaRztUVE1VPJVVMzpZpnmSSRxy5zickk9SSvNNvNq/m+m8swcvpZLxmvsp8vO/cteaMZPkfMAAYCIv0xrnubGxpc5x3WgDJJ7AvEDAybTaa++XKC1W2Ay1E7g1jeg7S7sAHEnoAV0lpDStDpCzR2uk8OQ+HUTEYMsmOLvIOgHQLTbNdBx6St3ryuY110q2+2kcRE08e5g/pJ6nzBTRe97EbKrJqP9Zil9PNcPurp53z9XW+cVYIi8c5rGl73BrWjJJOAAt/Mj1FEJtr2zWBxa/V9CSOB3N5/zArGfts2Xs56qiP4tNM75mICcIq/k287LY/8AaKR34tDUH/kWPJ6oPZkzxLnWSfi0Mo+cBAWQiq9/qjNnbPF9lH/i0mPncF8X+qT0C3xaG9P81NH+2QILllXi0W6/Wyps92pWVFJVxmOWN3UH5iOYI4ggFcj7SNn1y2fXx1BPvzUFQS+iqsfbWZ4td2PHAH4DwB4XNJ6prRTftdjvr/8AChH/AOij+s9uOgtaWGosNx0peHxyjeilJha+GQDwZGneOCM48oyMEEgiMo1EGcceaIYlobGNqbtG3AWK9zk2Wsf4xPCmkPDfH4J+6Hw9uen2PZIxskbg5rgC1wOQR2hcIK9dhG1XcMOhdRVPgkhltqJHcj/Mk/q/F2IVMvtERDIIiIAiIgCIiALV6k1Fb9L2ma7XF3gR+CyMHwpXnkxvlP6Bk9FnVlZS2+llrq2dkMEDDJJI44DWjmVznrzWdVrK7uqPDjoacllLA7hhvV57HHn8Q6EnVdrNpaez2EvHWrPSK/M+5e96dbRuxqtQX64alus13uTw6abADR4sbRya0dgHxkla5EXzlWrVMTUdWq7ybu2+Lb5nGFcWyLQHrdkerrzB7a8b1DE4eI0/fSO0jl2Dj1GI5st2f/VLWC9XaH+C6V+Gsd/2iQHxfxR912nh24vkAAYC9V2A2U8I1m+Njp9hPn/k/wAvXj0vlFcwiIvYjMKhtvW1QkzaD09U8PFudRG7/Iaf1/yfdBXlcKaWtop6SGslpHzMLBPDjfjz903IIz2ZBVUP9TNo973PN/vu845J7pCcn82hGc2oukfrZNGe/wBffzkH7pfr62XRPv5fvzsH7pCWObEXSv1s+hut4vp/x4f3S9HqaNC9btfj/jw/ukFjmlF0v9bToT30vvyiL90vfratB9bnfT/7iL92gsczoumR6mvQI/7wvh89TH+7Qepr0B1rr27z1Mf7I0FjmZF04PU3bPhxNTeD56lnoKM7Q/U+W+12CS7aKkrp6qkBkmppniQyx44lmGg7w5445AIAzhBYoletc5h3mEggggg4IPavOGBxyCiEOndiu1Rur6Eadvk49maNngPceNXEPuvK8cN7t59oFprhe33CttNdBc7bUyU9VTPbLFKw8Wuzz+cEdQutdmG0ai2g2QTnchudKAyspweTuj29dx2OHZxHlIyTJmiIhQiIgCIqv2ubQDb4n6Vss+KqZv2ZK0/aYz9wD7o9ewefI6vOM2w+SYSWMxL0XBc2+SXe/wB+BG7Eb2rbQPZ+qNgtE2bbTPzLIw5FRIOIwfcjp2nj0BVdpyRfNebZriM5xc8XiXeUvUlyS7l/NTjbuFItD6NrNZXcUbC+Ojiw+qnH3tueDR+EeQHQeQEKOqYaZ2nXrSlrZarXbbZ3MPc975I3mSRxOSXEOGcDAHmC5MkWX/1sZZm2qS1dldvu7r8304alXE6BoaKkttHDQUMDYaeBgZGxo4NAX3VGjbnqwHwrdaSPJFIP+dejbrqkO8K12sjyMk9Ne2R+UHIYpRU2lw+qzLeReKKjxt01P1tVrP8Adk9NejbrqXrabZ+TJ6SyXyg5E/8AUfssu8i70VJd/bUXvNbvgL/pXvf31B7y278p/wBKvaDkPlX7Mv0G8i7EVKd/i/dbFQflvXvf4vvvDQfnHq9oGQ+Vfsy/QbyLqRUsNvF662Ci/OvXvf5vHXT1H+ef9Cv9/wCQ+Wfsy/Qm8i6EVM9/q6f1bpflLvRXvf7uX9Wqb5S70U/v/IPLP2J/CXeRcqKm+/3cP6sU/wArPoqwNDa3ota219TFEKerp3BtRTl29u55OB4Zae3HMEdF2WW7VZTm9dYbCVrzaulZq9uNrpeoJpkkREWwlOc9u+y0WSpk1pYKbFvqX/Z0LBwp5XH7YB0Y44z2O/G4U2u7Kmmp62mlo6uBk0E7HRyxyNDmvYRgtIPMELkzavs4qdn98+xw+W0VrnPopic7nbC4+6Hb900ducDFogy3GlNU3XRt8p7/AGiQCaHg9h4Mmj+6jcOwj4iAea06IQ7X0hqy1a0sUF+tEhMcvgyRu8eGQeMx3lH6QQRwK3K4+2abQ6/Z7fBVs357dUkMraYHx254PA6OHTt5cjkdb2u50F6t1PdbXVMqKSqjEkUrDkOaf9cuhQyTMpERCkQ2j66i0fa+50r2PulU0injPHcHIyOHYOg6nyZI55llknlfUTyOkkleXve45LnE5JJ6kldOXPRml7zVurrpZaepqHgAySAl2AMAZzwCwzs10KTn6mqT4N4ftXm21Oyeb7R4pT8LBUo/Vj43renF8/QjFq5zYi6R72Wgzz03TflP+led7HQeMfU3T/lP+lav2YZn5Wn/ANvhJus5vRdId7DQY/2cg/Lf6Sd7DQeMfU7D+ck9JTswzTytP1y+EbrOb0XRx2XaCIwdPRfnZPSXh2WaCPPT0f56X0k7MM08rT9cvhG6znJF0WdlGgDzsA+UzemvO9NoD3h/+1N6ax7Mc28pT9cvhG6znVF0V3ptAe8R+VTemvDsl0Aedjd8qm9NTsxzfylP1y+EbrOdkXQ3eh0B7zS/LZ/TTvQaA955flc3pKdmWceUp+uXwDdZzyi6F7z+gfemb5XL6S/Ped0D71z/ACyX0lOzPOPv0/al8I3Wc+Iugu85oP3uqPlcn0rzvNaE/oFT8rk+lTszzf71P2pfCN1nPy22l9R12lLzDeaLwizwZYicNljPjNP+uBAPRXV3mdCf0Kq+VyfSvO8xob+i1fyly5cP8nme4SrGvRnTU4u6alLRr/iN1kts93ob7bae7W2bulPUM3mnkR2gjoQcgjoQsxaXTGkrVpKCaltD6kQzvEhjllL2tdjGRnlkYz5gt0vasG8RKhB4pJVLeNbVX7u4zC1OqdM2rV9jqbDeId+CobwcPHiePFe09HA8R8XIlbZF+kHGOqNA6l0reqiy1lsqqgwnMU8ED3RzRnxXggdnMdOR4jK1PsLePeiv+Sv9FdyIhLHDosV7PKy3H5LJ6Ks3YvrTUGi7kLFebVcjY66QZc6ll+xJTykHg+IeAcB2b3PO90qiCwBBGQeCIiFNbqS+0+mrLU3uqglmipQ0uZHjedlwaMZ4cyvjpPU1Lq2zMvNJTTQRvkfHuS43gWnB5Ehazapn6gbrg48GL/isWBsW/kNF/ap/1lrksxxC2hjl9/o3S37WX1t63Hjw5EvrYnSIi2MoREQEW1ptAt+iZaSKtoKqpNY17mmHdw3dxnO8R2hPqvv5DS3Z5eSHDPGWnBHn9sUI28fxjYcc9yo/WjVwN8UeZanhcVjcwzbGYTwzhCl4Pdso/ajd3bTIRY6v1DgEbO7zx/8AGp+H+YttY7rX3WKV9fYKu1ujcA1lQ+NxeO0bjitmi76hha9Ke9OvKS6NQS90U/eUIiL9wNLqXWFg0lTtmvNaI3SAmKFg3pJMdjR08pwPKtNSa31Rd4hVWTZ5WyUzvEkrKuOmLh27pyceUZUH0rEzXe1SuuV1HdoKMyzRRv4tLY3iOJuOwZ3vKc9pV1rU8rxmN2g8JiadTwVFScY7qi5StpvNyUku5JenrE7kOqtbaktLDU3zZ/XRUrBmSWkqo6ktHU7owceUrdac1XYtV0pqrLWiYMx3SNwLZIyeQc0/PyPQrbqH2/ZzTWjWEmqrXc5KaObe7pRMiHc3Bw8IZzy3vC5cCuwnDM8HXpunLw1Nu0lLdUor7ya3U0uatfoUmCIi7wBEUY2iaqGk9NT1kMjW1lR7RSg/zhHjf3QC74PKvzY3F0sBh54qu7Rgm36P5oDS3fbRp603WptnsfW1IpZe5Pnh3CwkcDjLgeByPOCp9T1ENXBHVU0rZIpmCSN7TkOaRkEfAq60fs4ids8qbdcW7lbe2Coc97cuhIGYR/d4OI7XOC92O3+d1HV6MumWVtoe4MY48e572C3y7ruHmc1aplOa5lTxVKlmtksRFyhZW3ZJ33H18Vp3et00RX5ljoiLdChRS57Q7fa9X02j5bdVPnqXRNEzd3cBk5Z45/QpWqa1eSNtdqIP3yj/AFitd2kzHEZbQozw7s5VYReiejbvxI3YuVERbEUIiICJ7Vv5AXXzRf8AFYsDYr/IaL+1T/rLZbUIZZ9CXWKGN73lsZDWNJJxK08gojsy1nYtN6Xbary+rgqG1Er931nM4brjkcWtK0bG4mjgtqoVsTNQi6DV27K+/wBWTmWuiinfQ0b/AE2s4/8Al9R6CybXr/TF5uEdroKqpfUS53Q6imY3gCfGc0AcAeq2aGdZbVkoQxEG27JKSu30WpbkiREXZgp3b00OuNjaTwMc4P5UanEWzTS7IwweyeMcc3Oo48Px1DNudPPNcLG+Kmlka1k++Y2F2PCj7FMm7TtHBozW1YI4EG31GR/8F53h45Ys+zCWZbnGnu7+79zW1/Rcx0vqfvvb6ZxgOufLH8Z1Hprf2y3U9poYbdSmUxQDdaZZC9x454udxPPqo8dp2jgN717V88fxfUegtpYtU2bUhqG2maaQ0273TulPJFjezjG+0Z5HktowNXJY1lHAyp77VvFcbtceXmuVW5G2REXeFKX2Oj1nru9UEpAkbDM3jzJbMAf2q5pmyOhe2GQRyFpDHluQ12OBx18yrDVukdRae1c3XukKU1ge7fqaQeMCRh+Bzc1w7OIcc8ekit+1TR9TEBcbgbVUge2U9ax0bmHqMkYPxrR9nK9LIqVTKsfLwbjKW65OylFu6cZaJvqr3XMxWmh+xY9ou6AdeUWep9h2/vFEbvrPW+nNZ0Omau90lcyeam7o9tE2PDJH7pbwJ446+VS2r2n6Va0xWeqkvFW4YipqGN0jnu6DexgfCVWFxtWrXa6tN91HbHRTXSugmEceXiJjZGNDSW8G4bjnxxnK6zP8XSw9On80Vpzlvx3pKpUnGMb21e843basuLK+4vxERellCpHUd8pdY7RIm1NNXVljs8hjcykpnzb+OJcWtB8Fz2huerR5VYW0zUVRp/TMwt7ZXV1bmngMTC4syPCfw5YGcfhFq+Wy3Sh0xpqN1VHu11wxUVAI4sGPAj/ujn+EXLTs8hUzrH08pou0IWqVHa60fiQaur3erXRJkeuhkd8S15x7C6gz/wClTfQq31ffG2nWtHrmxWu5wNO6KptVRyQMkdjBaC4YJczh525V4rV6nsNPqWxVdlqSAKhmGPx4jxxa74CAV+jO8mx2ZYRwVZb8GpwajZqUdVrvO1+D05hpmdRVlNcaOGvo5RLBURtljeOTmkZBX2VZbHLvcaVtZoy8U00UtE50lPvsIAG9iRgJ54ccjtDj2KzV2uTZks2wUMVazf1l0ktGvQ/cE7hU1q7/AK7LXn+co/nKuVUvrvulJtboblJTVDqenNJLI+OF78BriTjdHHgOS6LbVqOFw85cFWpv/wBEi6EUUO1DRoODXVf+76j0F530dG/02s/3fUegu8+fcr/3MPbj+ouiWIsGy3u3agovZC2SSvgLyzMkL4jkc/BeAf0IuxpVYVoKpTacXwa1TKZyLAv15pNPWeqvNbvGKljL9xvjSO5NY3tc5xDQOpIUStM+pdL3q2O1Rc5aun1I0xTB7sx0Fw4vZFGekbmlzBnPhRs6uXICeIiIAiIgCKG2qepZPXHW09wgqvXcohcx8sdG2m3j3LcfHhni4zvnf3s54YW7sUAbNUVVDqI3G2zBohic8TdweM72JclzgeHB2SCDxwcADboiIAiLQ6wdfW0tF7DtnMHrxnsiabHrgUuDvdzz13t3OPC3c7vHCA3yFrXcwD51F6g2augFJa9TVdsuL/8Aozp6iXugfjhmGY+GO1pHxc1JoxII2iVzXPAG85rcAnqQMnHxoD9AAcAMIiIAiKB7O7vcrjqrXFJXXCeoiobqyKmZI8ubCzcPgt6AZ6BATxERAERRrWT78ya0+x7as2o1LhdjRDNSItw7m7jwt3fxvbnh45dUBJUUUnFquG5TWDVk9uugc0xNnqJJHENIy19PM4FwIyDwB45BB4qVoAiIgCLx7BIx0bi4BwIO64g/ARxCgmso5rbqLSFFQ3S5QwXK5yU9WwV0x7rGKeR4BJcSPCaOIwgJ4ix6Ohhog8QyVDt/Ge7VEkvLs33HHwIgNLqTTVTqa72qOtdCbJQPdVzwFx36ioHCEEYxuNy5/jcXBvDhlfTUujrbqGy1FrO/DK5u/TVAe4up528Y5G8ebXAH4MIiA2dnNzdaqQ3n1v6/ELRUmnJMRkA8ItyAcE5xkLLREAREQEcpKvUViidS32anuY7q7uFWw9ylkYTlokjDN0EA4y04OM4HJfm02CvOrKrV1W6lpW1FG2kbS0rnOEuH7wmleQ3eePFA3eAJ4nPAiAkqIiALV3yn1A91HVWCup43U0xdUUtQ3EdVGWkFpeAXRkHDg4A8sEEHgRAam9S1+qbXWafp7Xb3PqYnxSGrlc6OPPDfDQzLyDxAy3kOI5rfWa3utFoorU+slq3UdPHAZ5jl8u60Ded5TjJREIjMREQoUN0PpS56f1Fq26V01M+G9XBtTTCJ7i5rQDkPBaADx6EoiAmSIiALT3un1KK6huFir6fuFP3RtXQVA3WVLXAbrhKGucxzSMjAIOSD0IIgNNqanr9c2St05S0VBEKhncpKipkc805P3yNgb4T282+E3BwcqWUkBpaWGlM8kxhjbGZJDl78DG849SeZREIj6oiIUKN6o07X3m/aYuVJJTtis1fJVTiRxDnMdA9gDAAQTl45kIiAkiIiA//Z";
            byte[] imageByte = Base64.decodeBase64(S);
            String directory = file.getAbsolutePath() + "/sample.jpg";
            new FileOutputStream(directory).write(imageByte);
            // This will decode the String which is encoded by using Base64 class
            LOG.info("\n EXITING THIS METHOD == testService(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Record Found", null);
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == testService();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == testService(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllModules", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllModules(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getAllModules(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblModule> tblModules = usersService.getAllModules();
            if (tblModules != null && tblModules.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getAllModules(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblModules);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAllModules(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getAllModules();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllModules(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getMenuPages/{moduleCode}/{companyCode}/{compaignCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getMenuPages(@PathVariable String moduleCode,
                                                                @PathVariable String companyCode, @PathVariable String compaignCode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getMenuPages(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblPage> tblPages = usersService.getMenuPages(moduleCode, companyCode, compaignCode);
            if (tblPages != null && tblPages.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getMenuPages(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblPages);
            } else {
                LOG.info("\n EXITING THIS METHOD == getMenuPages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getMenuPages();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getMenuPages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getMenuPages/{moduleCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getMenuPages(@PathVariable String moduleCode,
                                                                HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getMenuPages(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblPage> tblPages = usersService.getMenuPages(moduleCode, null, null);
            if (tblPages != null && tblPages.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getMenuPages(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblPages);
            } else {
                LOG.info("\n EXITING THIS METHOD == getMenuPages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getMenuPages();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getMenuPages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getRoles/{categoryCode}/{compaignCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getRoles(@PathVariable String categoryCode,
                                                            @PathVariable String compaignCode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getRoles(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblRole> tblRoles = usersService.getRoles(categoryCode, compaignCode);
            if (tblRoles != null && tblRoles.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getRoles(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblRoles);
            } else {
                LOG.info("\n EXITING THIS METHOD == getRoles(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getRoles();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getRoles(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getRoles", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getRoles(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getRoles(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblRole> tblRoles = usersService.getRoles(null, null);
            if (tblRoles != null && tblRoles.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getRoles(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblRoles);
            } else {
                LOG.info("\n EXITING THIS METHOD == getRoles(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getRoles();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getRoles(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getRolesRights/{roleCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getRolesRights(@PathVariable String roleCode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getRolesRights(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblRolepage> rolesRights = usersService.getRolesRights(roleCode);
            if (rolesRights != null && rolesRights.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getRolesRights(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", rolesRights);
            } else {
                LOG.info("\n EXITING THIS METHOD == getRolesRights(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getRolesRights();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getRolesRights(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllCompaniesProfile", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllCompaniesProfile(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getAllCompaniesProfile(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblCompanyprofile> tblCompanyprofiles = usersService.getAllCompaniesProfile();
            if (tblCompanyprofiles != null && tblCompanyprofiles.size() > 0) {
//                List<CompanyProfileResponse> companyProfileResponses = new ArrayList<>();
//                for(TblCompanyprofile tblCompanyprofile:tblCompanyprofiles){
//                    CompanyProfileResponse companyProfileResponse = new CompanyProfileResponse();
//
//                    companyProfileResponse.setName(tblCompanyprofile.getName());
//                    companyProfileResponse.setAddressline1(tblCompanyprofile.getAddressline1());
//                    companyProfileResponse.setCompanystatus(tblCompanyprofile.getCompanystatus());
//                    companyProfileResponse.setCompanyId(tblCompanyprofile.getCompanycode());
//                    companyProfileResponse.setCategoryname(tblCompanyprofile.getTblUsercategory().getCategoryname());
////                    companyProfileResponse.setUpdatedOn(tblCompanyprofile.get);
//
//                    companyProfileResponses.add(companyProfileResponse);
//                }
                LOG.info("\n EXITING THIS METHOD == getAllCompaniesProfile(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblCompanyprofiles);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAllCompaniesProfile(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getAllCompaniesProfile();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllCompaniesProfile(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getCompaniesProfile/{companyCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getCompaniesProfile(@PathVariable String companyCode,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getCompaniesProfile(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getCompaniesProfile(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = usersService.getCompaniesProfile(companyCode);
            if (tblCompanyprofile != null) {

                LOG.info("\n EXITING THIS METHOD == getCompaniesProfile(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblCompanyprofile);
            } else {
                LOG.info("\n EXITING THIS METHOD == getCompaniesProfile(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getCompaniesProfile();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getCompaniesProfile(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getCompaniesJobs/{companyCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getCompaniesJobs(@PathVariable String companyCode,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getCompaniesJobs(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getCompaniesJobs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblCompanyjob> tblCompanyjobs = usersService.getcompanyJobs(companyCode);
            if (tblCompanyjobs != null) {

                LOG.info("\n EXITING THIS METHOD == getCompaniesJobs(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblCompanyjobs);
            } else {
                LOG.info("\n EXITING THIS METHOD == getCompaniesJobs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getCompaniesJobs();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getCompaniesJobs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getCompaniesUsers/{companyCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getCompaniesUsers(@PathVariable String companyCode,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getCompaniesUsers(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getCompaniesUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblUser> tblUsers = usersService.getUsers(companyCode);
            if (tblUsers != null) {

                LOG.info("\n EXITING THIS METHOD == getCompaniesUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblUsers);
            } else {
                LOG.info("\n EXITING THIS METHOD == getCompaniesUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getCompaniesUsers();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getCompaniesUsers(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getCompaniesDocs/{companyCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getCompaniesDocs(@PathVariable String companyCode,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getCompaniesDocs(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getCompaniesDocs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblCompanydoc> tblCompanydocs = usersService.getCompanyDocs(companyCode);
            if (tblCompanydocs != null) {

                LOG.info("\n EXITING THIS METHOD == getCompaniesDocs(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblCompanydocs);
            } else {
                LOG.info("\n EXITING THIS METHOD == getCompaniesDocs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getCompaniesDocs();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getCompaniesDocs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllUsers", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllUsers(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getAllUsers(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAllUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblUser> allUsers = usersService.getAllUsers();
            if (allUsers != null) {
                LOG.info("\n EXITING THIS METHOD == getAllUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", allUsers);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAllUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getAllUsers();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllUsers(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/refreshToken", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> refreshToken(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == refreshToken(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == refreshToken(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblLogintoken tblLogintoken = new TblLogintoken();
            Map<String, Object> claims = new HashMap<>();
            claims.put("userName", tblUser.getLoginid());
            claims.put("companyCode", tblUser.getCompanycode());
            claims.put("userCode", tblUser.getUsercode());
            JWTSecurity jwtSecurity = new JWTSecurity();
            String token = "Bearer " + jwtSecurity.createJWTWithClaims(tblUser.getUsercode(), claims,env);
            Calendar calendar = Calendar.getInstance();
            tblLogintoken.setEffectivefrom(calendar.getTime());
            calendar.add(Calendar.MINUTE, Integer.parseInt(getDataFromProperties("loginTokenTime")));
            tblLogintoken.setEffectiveto(calendar.getTime());
            tblLogintoken.setToken(token);
            tblLogintoken.setUsercode(tblUser.getUsercode());
            tblLogintoken.setLogindate(new Date());
            tblLogintoken = usersService.saveToken(tblLogintoken);
            if (tblLogintoken != null) {
                LOG.info("\n EXITING THIS METHOD == refreshToken(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblLogintoken.getToken());
            } else {
                LOG.info("\n EXITING THIS METHOD == refreshToken(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == refreshToken();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == refreshToken(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getIntoducers", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getIntoducers(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getIntoducers(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getIntoducers(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = usersService.getCompaniesProfile(tblUser.getCompanycode());
            List<TblCompanyprofile> tblCompanyprofiles = new ArrayList<>();
            if (tblCompanyprofile != null) {
                if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                    tblCompanyprofiles = usersService.getAllIntroducersCompaniesProfile();
                    LOG.info("\n EXITING THIS METHOD == getIntoducers(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblCompanyprofiles);
                } else {
                    tblCompanyprofiles.add(tblCompanyprofile);
                    LOG.info("\n EXITING THIS METHOD == getIntoducers(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblCompanyprofiles);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getIntoducers(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getIntoducers();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getIntoducers(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAdvisor/{companycode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAdvisor(@PathVariable String companycode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getAdvisor(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAdvisor(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblUser> tblUsers = usersService.getUsers(companycode);
            if (tblUsers != null) {
                LOG.info("\n EXITING THIS METHOD == getAdvisor(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblUsers);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAdvisor(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getAdvisor();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAdvisor(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllBroadCastMessages", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllBroadCastMessages(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getAllBroadCastMessages(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAllBroadCastMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblBroadcast> tblBroadcastList = usersService.getAllBroadCastMessages();
            if (tblBroadcastList != null && tblBroadcastList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getAllBroadCastMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblBroadcastList);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAllBroadCastMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getAllBroadCastMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllBroadCastMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getBroadCastMessage/{broadCastMsgId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getBroadCastMessage(@PathVariable String broadCastMsgId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementGetApi \n METHOD == getBroadCastMessage(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getBroadCastMessage(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblBroadcast tblBroadcast = usersService.getBroadCastMessage(Long.valueOf(broadCastMsgId));
            if (tblBroadcast != null) {
                LOG.info("\n EXITING THIS METHOD == getBroadCastMessage(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblBroadcast);
            } else {
                LOG.info("\n EXITING THIS METHOD == getBroadCastMessage(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == getBroadCastMessage();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getBroadCastMessage(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
