package com.laportal.controller.usermanagement;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.rta.RtaService;
import com.laportal.service.user.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class UserManagementPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(UserManagementGetApi.class);

    @Autowired
    private UsersService usersService;

    @Autowired
    private RtaService rtaService;

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> userLogin(@RequestBody LoginRequest loginRequest,
                                                             HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == userLogin(); ");

            LoginResponse loginResponse = usersService.userLogin(loginRequest);
            if (loginResponse.isLogin()) {
                LOG.info("\n EXITING THIS METHOD == login(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, loginResponse.getMessage(), loginResponse);
            } else {
                LOG.info("\n EXITING THIS METHOD == login(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, loginResponse.getMessage(), loginResponse);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == userLogin();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == userLogin(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/saveCompanyProfile", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> saveCompanyProfile(
            @RequestBody CompanyProfileRequest companyProfileRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == saveCompanyProfile(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = new TblCompanyprofile();

            tblCompanyprofile.setAccountno(companyProfileRequest.getAccountno());
            tblCompanyprofile.setAddressline1(companyProfileRequest.getAddressline1());
            tblCompanyprofile.setAddressline2(companyProfileRequest.getAddressline2());
            tblCompanyprofile.setCity(companyProfileRequest.getCity());
            tblCompanyprofile.setCompanyregno(companyProfileRequest.getCompanyregno());
            tblCompanyprofile.setCompanystatus(companyProfileRequest.getCompanystatus());
            tblCompanyprofile.setContactperson(companyProfileRequest.getContactperson());
            tblCompanyprofile.setEmail(companyProfileRequest.getEmail());
            tblCompanyprofile.setName(companyProfileRequest.getName());
            tblCompanyprofile.setPhone(companyProfileRequest.getPhone());
            tblCompanyprofile.setPhone2(companyProfileRequest.getPhone2());
            tblCompanyprofile.setPostcode(companyProfileRequest.getPostcode());
            tblCompanyprofile.setRegion(companyProfileRequest.getRegion());
            tblCompanyprofile.setTag(companyProfileRequest.getTag());
            tblCompanyprofile.setVaregno(companyProfileRequest.getVaregno());
            tblCompanyprofile.setVat(companyProfileRequest.getVat());
            tblCompanyprofile.setWebsite(companyProfileRequest.getWebsite());
            tblCompanyprofile.setDirectintroducer(companyProfileRequest.getDirectIntroducer());
            tblCompanyprofile.setBilltoemail(companyProfileRequest.getBilltoemail());
            tblCompanyprofile.setBilltoname(companyProfileRequest.getBilltoname());
            tblCompanyprofile.setAccountemail(companyProfileRequest.getAccountemail());
            tblCompanyprofile.setSecondaryaccountemail(companyProfileRequest.getSecondaryaccountemail());
            tblCompanyprofile.setJurisdiction(companyProfileRequest.getJurisdiction());
            tblCompanyprofile.setAirbagCase(companyProfileRequest.getAirbagCase());

            TblUsercategory tblUsercategory = new TblUsercategory();
            tblUsercategory.setCategorycode(companyProfileRequest.getUserCategoryCode());
            tblCompanyprofile.setTblUsercategory(tblUsercategory);

            tblCompanyprofile.setCreatedon(new Date());
            tblCompanyprofile.setUsercode(tblUser.getUsercode());

            TblUser bdmuser = new TblUser();

            if (companyProfileRequest.getBdmuser() == null || companyProfileRequest.getBdmuser().isEmpty()) {
                tblCompanyprofile.setBdmuser(null);
            } else {
                bdmuser.setUsercode(companyProfileRequest.getBdmuser());
                tblCompanyprofile.setBdmuser(bdmuser);
            }


            tblCompanyprofile = usersService.saveCompanyProfile(tblCompanyprofile);

            if (tblCompanyprofile.getCompanycode() != null && !tblCompanyprofile.getCompanycode().isEmpty()) {
                LOG.info("\n EXITING THIS METHOD == saveCompanyProfile(); \n\n\n");
                HashMap<String,Object> map = new HashMap<>();
                map.put("companyCode",tblCompanyprofile.getCompanycode());
                return getResponseFormat(HttpStatus.OK, "Save SuccessFull", map);
            } else {
                LOG.info("\n EXITING THIS METHOD == saveCompanyProfile(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == EventApi \n METHOD == saveCompanyProfile();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == saveCompanyProfile(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/saveCompanyCompaign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> saveCompanyCompaign(
            @RequestBody CompanyCompaignRequest companyCompaignRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == saveCompanyCompaigns(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (companyCompaignRequest.getCompanycode() != null && !companyCompaignRequest.getCompanycode().isEmpty()) {

                TblCompanyjob tblCompanyjob = new TblCompanyjob();


                tblCompanyjob.setCompanycode(companyCompaignRequest.getCompanycode());
                tblCompanyjob.setStandardroadworthy(companyCompaignRequest.getStandardroadworthy() == null || companyCompaignRequest.getStandardroadworthy().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getStandardroadworthy()));
                tblCompanyjob.setStandardunroadworthy(companyCompaignRequest.getStandardunroadworthy() == null || companyCompaignRequest.getStandardunroadworthy().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getStandardunroadworthy()));
                tblCompanyjob.setPrestigeroadworthy(companyCompaignRequest.getPrestigeroadworthy() == null || companyCompaignRequest.getPrestigeroadworthy().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getPrestigeroadworthy()));
                tblCompanyjob.setPrestigeunroadworthy(companyCompaignRequest.getPrestigeunroadworthy() == null || companyCompaignRequest.getPrestigeunroadworthy().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getPrestigeunroadworthy()));
                tblCompanyjob.setRecovery(companyCompaignRequest.getRecovery() == null || companyCompaignRequest.getRecovery().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getRecovery()));
                tblCompanyjob.setSalvage(companyCompaignRequest.getSalvage() == null || companyCompaignRequest.getSalvage().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getSalvage()));
                tblCompanyjob.setRepairs(companyCompaignRequest.getRepairs() == null || companyCompaignRequest.getRepairs().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getRepairs()));
                tblCompanyjob.setFaultrepairs(companyCompaignRequest.getFaultrepairs() == null || companyCompaignRequest.getFaultrepairs().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getFaultrepairs()));
                tblCompanyjob.setSolicitorsfees(companyCompaignRequest.getSolicitorsfees() == null || companyCompaignRequest.getSolicitorsfees().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getSolicitorsfees()));
                tblCompanyjob.setHousingFee(companyCompaignRequest.getHousingFee() == null || companyCompaignRequest.getHousingFee().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getHousingFee()));
                tblCompanyjob.setBike(companyCompaignRequest.getBike() == null || companyCompaignRequest.getBike().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getBike()));
                tblCompanyjob.setStorage(companyCompaignRequest.getStorage() == null || companyCompaignRequest.getStorage().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getStorage()));
                tblCompanyjob.setWiplash(companyCompaignRequest.getWiplash() == null || companyCompaignRequest.getWiplash().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getWiplash()));
                tblCompanyjob.setHybrid(companyCompaignRequest.getHybrid() == null || companyCompaignRequest.getHybrid().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getHybrid()));
                tblCompanyjob.setMinor(companyCompaignRequest.getMinor() == null || companyCompaignRequest.getMinor().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getMinor()));
                tblCompanyjob.setScotishRta(companyCompaignRequest.getScotishRta() == null || companyCompaignRequest.getScotishRta().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getScotishRta()));
                tblCompanyjob.setPedestrian(companyCompaignRequest.getPedestrian() == null || companyCompaignRequest.getPedestrian().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getPedestrian()));
                tblCompanyjob.setRecovery(companyCompaignRequest.getRecovery() == null || companyCompaignRequest.getRecovery().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getRecovery()));
                tblCompanyjob.setSerious(companyCompaignRequest.getSerious() == null || companyCompaignRequest.getSerious().isEmpty() ? new BigDecimal(0) : new BigDecimal(companyCompaignRequest.getSerious()));


                tblCompanyjob.setStatus(companyCompaignRequest.getStatus());

                TblCompaign tblCompaign = new TblCompaign();
                tblCompaign.setCompaigncode(companyCompaignRequest.getCompaigncode());

                tblCompanyjob.setTblCompaign(tblCompaign);

                tblCompanyjob.setCreatedon(new Date());
                tblCompanyjob.setUsercode(tblUser.getUsercode());

                tblCompanyjob = usersService.saveCompanyCompaign(tblCompanyjob);

                if (tblCompanyjob != null && !tblCompanyjob.getCompanyjobcode().isEmpty()) {
                    List<TblCompanyjob> tblCompanyjobs = usersService
                            .getcompanyJobs(companyCompaignRequest.getCompanycode());

                    LOG.info("\n EXITING THIS METHOD == saveCompanyCompaigns(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Save SuccessFull", tblCompanyjobs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == saveCompanyCompaigns(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == saveCompanyCompaigns(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against Request", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == saveCompanyCompaigns();  ERROR ----- "
                    + e.getLocalizedMessage());
            e.printStackTrace();
            LOG.info("\n EXITING THIS METHOD == saveCompanyCompaigns(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/saveCompanyUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> saveCompanyUser(
            @RequestBody CompanyUserRequest companyCompaignRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == saveCompanyUser(); ");
            TblUser tblUser1 = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser1 == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (companyCompaignRequest.getCompanycode() != null && !companyCompaignRequest.getCompanycode().isEmpty()) {
                TblUser tblUser = new TblUser();

                tblUser.setCompanycode(companyCompaignRequest.getCompanycode());
                tblUser.setLoginid(companyCompaignRequest.getLoginid());
                tblUser.setStatus(companyCompaignRequest.getStatus());
                tblUser.setPassword(generatePassword());
                tblUser.setUsername(companyCompaignRequest.getUsername());
                tblUser.setPwdupdateflag("N");
                tblUser.setCreatedon(new Date());

                tblUser = usersService.saveCompanyUser(tblUser, companyCompaignRequest.getRolecodes());
                if (tblUser != null && !tblUser.getUsercode().isEmpty()) {
                        TblEmailTemplate tblEmailTemplate = rtaService.findByEmailType("new-user");
                        String userBody = tblEmailTemplate.getEmailtemplate();
                        userBody = userBody.replace("[USER_NAME]", tblUser.getLoginid());
                        userBody = userBody.replace("[PASSWORD]", tblUser.getPassword());
                        usersService.saveEmail(tblUser.getUsername(), userBody, "Legal Assist User Credentials");

                        List<TblUser> tblUsers = usersService.getUsers(companyCompaignRequest.getCompanycode());
                        LOG.info("\n EXITING THIS METHOD == saveCompanyUser(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Save SuccessFull", tblUsers);

                } else {
                    LOG.info("\n EXITING THIS METHOD == saveCompanyUser(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " Error While Saving User", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == saveCompanyUser(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == saveCompanyUser();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == saveCompanyUser(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/saveModule", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> saveModule(@RequestBody ModuleRequest moduleRequest,
                                                              HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == saveModule(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblModule tblModule = new TblModule();

            tblModule.setDescr(moduleRequest.getDescr());
            tblModule.setModulename(moduleRequest.getModulename());
            tblModule.setModulestatus(moduleRequest.getModulestatus());

            tblModule.setCreatedon(new Date());
            tblModule.setUsercode("1");

            tblModule = usersService.saveModule(tblModule);

            if (tblModule.getModulecode() != null && !tblModule.getModulecode().isEmpty()) {
                List<TblModule> tblModules = usersService.getAllModules();
                LOG.info("\n EXITING THIS METHOD == saveModule(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Save SuccessFull", tblModules);
            } else {
                LOG.info("\n EXITING THIS METHOD == saveModule(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == saveModule();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == saveModule(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/saveModulePage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> saveModulePage(@RequestBody ModulePageRequest modulePageRequest,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == saveModulePage(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblPage tblPage = new TblPage();

            tblPage.setCompaigncode(modulePageRequest.getCompaigncode());
            tblPage.setCompanycode(modulePageRequest.getCompanycode());
            tblPage.setPagedescr(modulePageRequest.getPagedescr());
            tblPage.setPageicon(modulePageRequest.getPageicon());
            tblPage.setPagename(modulePageRequest.getPagename());
            tblPage.setPagepath(modulePageRequest.getPagepath());
            tblPage.setPagestatus(modulePageRequest.getPagestatus());
            tblPage.setIslist(modulePageRequest.getIslist());
            tblPage.setIsnewcase(modulePageRequest.getIsnewcase());

            TblModule tblModule = new TblModule();
            tblModule.setModulecode(modulePageRequest.getModuleCode());
            tblPage.setTblModule(tblModule);

            tblPage.setCreatedon(new Date());
            tblPage.setUsercode("1");

            tblPage = usersService.saveModulePage(tblPage);

            if (tblPage.getPagecode() != null && !tblPage.getPagecode().isEmpty()) {
                List<TblPage> tblPages = usersService.getMenuPages(tblPage.getTblModule().getModulecode(), null, null);
                LOG.info("\n EXITING THIS METHOD == saveModulePage(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Save SuccessFull", tblPages);
            } else {
                LOG.info("\n EXITING THIS METHOD == saveModulePage(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == saveModulePage();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == saveModulePage(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/saveRole", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> saveRole(@RequestBody RoleRequest roleRequest,
                                                            HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == saveRole(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblRole tblRole = new TblRole();

            tblRole.setDescr(roleRequest.getDescr());
            tblRole.setRolename(roleRequest.getRolename());
            tblRole.setRolestatus(roleRequest.getRolestatus());

            TblCompaign tblCompaign = new TblCompaign();
            TblUsercategory tblUsercategory = new TblUsercategory();

            tblCompaign.setCompaigncode(roleRequest.getCompaignCode());
            tblUsercategory.setCategorycode(roleRequest.getUsercategoryCode());

            tblRole.setTblCompaign(null);
            tblRole.setTblUsercategory(null);

            tblRole.setCreatedon(new Date());
            tblRole.setUsercode("1");

            tblRole = usersService.saveRole(tblRole);

            if (tblRole.getRolecode() != null && !tblRole.getRolecode().isEmpty()) {
                List<TblRole> tblRoles = usersService.getRoles(null, null);
                LOG.info("\n EXITING THIS METHOD == saveRole(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Save SuccessFull", tblRoles);
            } else {
                LOG.info("\n EXITING THIS METHOD == saveRole(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == saveRole();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == saveRole(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/saveRoleRights", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> saveRoleRights(@RequestBody RoleRightsRequest roleRightsRequest,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == saveRoleRights(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblRolepage> tblRolepages = new ArrayList<>();
            TblRolepage tblRolepage = null;
            TblRole tblRole = null;
            TblPage tblPage = null;

            for (String pageCode : roleRightsRequest.getPagesvalue()) {
                tblRolepage = new TblRolepage();

                tblRole = new TblRole();
                tblPage = new TblPage();

                tblRole.setRolecode(roleRightsRequest.getRoleCode());
                tblPage.setPagecode(pageCode);

                tblRolepage.setTblRole(tblRole);
                tblRolepage.setTblPage(tblPage);

                tblRolepage.setStatus("Y");
                tblRolepage.setCreatedon(new Date());
                tblRolepage.setUsercode("1");

                tblRolepages.add(tblRolepage);
            }

            tblRolepages = usersService.saveRoleRights(tblRolepages);

            if (tblRolepages != null) {
                List<TblRolepage> rolesRights = usersService.getRolesRights(roleRightsRequest.getRoleCode());
                LOG.info("\n EXITING THIS METHOD == saveRoleRights(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Save SuccessFull", rolesRights);
            } else {
                LOG.info("\n EXITING THIS METHOD == saveRoleRights(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == saveRoleRights();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == saveRoleRights(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/updateCompanyProfile", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateCompanyProfile(
            @RequestBody UpdateCompanyProfileRequest updateCompanyProfileRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == updateCompanyProfile(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            int update = usersService.updateCompanyProfile(updateCompanyProfileRequest);

            if (update > 0) {
                LOG.info("\n EXITING THIS METHOD == updateCompanyProfile(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Updated SuccessFully", null);
            } else {
                LOG.info("\n EXITING THIS METHOD == updateCompanyProfile(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Record Not Updated", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == updateCompanyProfile();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == updateCompanyProfile(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/updateCompanyCompaign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateCompanyJobs(
            @RequestBody UpdateCompanyCompaignRequest updateCompanyCompaignRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == updateCompanyJobs(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyjob update = usersService.updateCompanyJobs(updateCompanyCompaignRequest);

            if (update != null) {
                List<TblCompanyjob> tblCompanyjobs = usersService
                        .getcompanyJobs(updateCompanyCompaignRequest.getCompanycode());

                LOG.info("\n EXITING THIS METHOD == updateCompanyJobs(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Updated SuccessFully", tblCompanyjobs);
            } else {
                LOG.info("\n EXITING THIS METHOD == updateCompanyJobs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Record Not Updated", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == updateCompanyJobs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == updateCompanyJobs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/updateCompanyUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateCompanyUser(
            @RequestBody UpdateCompanyUserRequest updateCompanyUserRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == updateCompanyUser(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            int update = usersService.updateCompanyUser(updateCompanyUserRequest);

            if (update > 0) {
                List<TblUser> tblUsers = usersService.getUsers(updateCompanyUserRequest.getCompanycode());

                LOG.info("\n EXITING THIS METHOD == updateCompanyUser(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Updated SuccessFully", tblUsers);
            } else {
                LOG.info("\n EXITING THIS METHOD == updateCompanyUser(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Record Not Updated", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == updateCompanyUser();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == updateCompanyUser(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/changePassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> changePassword(
            @RequestBody ChangePasswordRequest changePasswordRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == changePassword(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == changePassword(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            changePasswordRequest.setUserId(tblUser.getUsercode());
            int update = usersService.updatepassword(changePasswordRequest);

            if (update > 0) {
                LOG.info("\n EXITING THIS METHOD == changePassword(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Updated SuccessFully", "Password Updated SuccessFully");
            } else {
                LOG.info("\n EXITING THIS METHOD == changePassword(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Record Not Updated", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == changePassword();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == changePassword(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/changeUserPassword", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> changeUserPassword(
            @RequestBody ChangePasswordRequest changePasswordRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == changeUserPassword(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == changeUserPassword(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            changePasswordRequest.setPassword(generatePassword());
            int update = usersService.updatepassword(changePasswordRequest);

            if (update > 0) {
                TblUser tblUser1 = rtaService.findByUserId(changePasswordRequest.getUserId());
                TblEmailTemplate tblEmailTemplate = rtaService.findByEmailType("new-user");
                String userBody = tblEmailTemplate.getEmailtemplate();


                userBody = userBody.replace("[USER_NAME]", tblUser1.getLoginid());
                userBody = userBody.replace("[PASSWORD]", tblUser1.getPassword());

                usersService.saveEmail(tblUser1.getUsername(), userBody, "Legal Assist User Credentials");
                LOG.info("\n EXITING THIS METHOD == changeUserPassword(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Updated SuccessFully", "Password Updated SuccessFully");
            } else {
                LOG.info("\n EXITING THIS METHOD == changeUserPassword(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Record Not Updated", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == changeUserPassword();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == changeUserPassword(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/changepwd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> changepwd(@RequestBody ChangePwdRequest changePwdRequest,
                                                             HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == userLogin(); ");
            String changepwd = "";
            changepwd = usersService.changepwd(changePwdRequest);
            if (!changepwd.equals("")) {
                LOG.info("\n EXITING THIS METHOD == login(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, changepwd, changepwd);
            } else {
                LOG.info("\n EXITING THIS METHOD == login(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, changepwd, changepwd);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == userLogin();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == userLogin(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/userManagement/createBroadCastMessage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> createBroadCastMessage(@RequestBody BroadcastRequest broadcastRequest,
                                                                          HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == createBroadCastMessage(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblBroadcast tblBroadcast = new TblBroadcast();

            String userEmails = "";


            for (String usercode : broadcastRequest.getToUsers()) {
                TblUser user = usersService.getUserById(usercode);
                if (user != null) {
                    userEmails = userEmails + user.getUsername() + ",";
                }
            }
            userEmails.substring(0, userEmails.length() - 1);

            tblBroadcast.setToUsers(userEmails);
            tblBroadcast.setMessage(broadcastRequest.getMessage());
            tblBroadcast.setNow(broadcastRequest.getNow());
            if (broadcastRequest.getNow().equals("N")) {
                tblBroadcast.setSechduleDate(broadcastRequest.getSechduleDate());
                tblBroadcast.setSechduleTime(broadcastRequest.getSechduleTime());
            }
            tblBroadcast.setCreateuser(new BigDecimal(tblUser.getUsercode()));
            tblBroadcast.setStatus("N");
            tblBroadcast.setCreatedate(new Date());

            tblBroadcast = usersService.createBroadCastMessage(tblBroadcast);

            if (broadcastRequest.getNow().equals("Y")) {

                String[] toEmails = tblBroadcast.getToUsers().split(",");
                List<String> toEmailAddresses = new ArrayList<String>();
                toEmailAddresses.addAll(Arrays.asList(toEmails));

                sendEmail(toEmailAddresses, null, tblBroadcast.getSubject(), tblBroadcast.getMessage(),
                        null, null);
                tblBroadcast.setStatus("Y");
                tblBroadcast = usersService.createBroadCastMessage(tblBroadcast);

            }

            if (tblBroadcast != null) {
                LOG.info("\n EXITING THIS METHOD == createBroadCastMessage(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "BroadCast Message Save/Send SuccessFully", "");
            } else {
                LOG.info("\n EXITING THIS METHOD == createBroadCastMessage(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Save/Send BroadCast Message", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == createBroadCastMessage();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == createBroadCastMessage(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/userManagement/addcompanyCfa", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addcompanyCfa(
            @RequestBody AddcompanyCfaRequest addcompanyCfaRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == UserManagementPostApi \n METHOD == addcompanyCfa(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addcompanyCfa(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }


            TblCompanydoc tblCompanydoc = usersService.addcompanyCfa(addcompanyCfaRequest);

            if (tblCompanydoc !=null &&tblCompanydoc.getCompanydocscode()>0) {


                LOG.info("\n EXITING THIS METHOD == addcompanyCfa(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Saved SuccessFully", tblCompanydoc);
            } else {
                LOG.info("\n EXITING THIS METHOD == addcompanyCfa(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Record Not Updated", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == EventApi \n METHOD == addcompanyCfa();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addcompanyCfa(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
