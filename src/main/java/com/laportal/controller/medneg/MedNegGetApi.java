package com.laportal.controller.medneg;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.MNCaseList;
import com.laportal.dto.MnNotesResponse;
import com.laportal.dto.MnStatusCountList;
import com.laportal.dto.RtaAuditLogResponse;
import com.laportal.model.*;
import com.laportal.service.medneg.MedNegService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/medneg")
public class MedNegGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(MedNegGetApi.class);

    @Autowired
    private MedNegService medNegService;

    @RequestMapping(value = "/getMnCases", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getMnCases(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MedNegGetApi \n METHOD == getMnCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getMnCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = medNegService.isMnCaseAllowed(tblUser.getCompanycode(), "11");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getMnCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            TblCompanyprofile tblCompanyprofile = medNegService.getCompanyProfile(tblUser.getCompanycode());
            List<MNCaseList> viewHdrCaseslist = medNegService.getAuthMnCasesUserAndCategoryWise(tblUser.getUsercode(), tblCompanyprofile.getTblUsercategory().getCategorycode());
            if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getMnCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
            } else {
                LOG.info("\n EXITING THIS METHOD == getMnCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == MedNegGetApi \n METHOD == getMnCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getMnCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getMNCaseById/{mNCaseId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrCaseById(@PathVariable long mNCaseId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MedNegGetApi \n METHOD == getMNCaseById(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getMNCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = medNegService.isMnCaseAllowed(tblUser.getCompanycode(), "11");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getMNCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }
            TblMnclaim tblMnclaim = medNegService.findMnCaseById(mNCaseId, tblUser);

            LOG.info("\n EXITING THIS METHOD == getMNCaseById(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Record Found", tblMnclaim);
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == MedNegGetApi \n METHOD == getMNCaseById();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getMNCaseById(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllMnStatusCounts", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllMnStatusCounts(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MedNegGetApi \n METHOD == getAllMnStatusCounts(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAllMnStatusCounts(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = medNegService.isMnCaseAllowed(tblUser.getCompanycode(), "11");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getAllMnStatusCounts(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }
            TblCompanyprofile tblCompanyprofile = medNegService.getCompanyProfile(tblUser.getCompanycode());
            List<MnStatusCountList> DbStatusCountsForIntroducers = medNegService.getAllMnStatusCounts(tblCompanyprofile.getCompanycode(),"11");

            LOG.info("\n EXITING THIS METHOD == getAllMnStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Record Found", DbStatusCountsForIntroducers);

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == MedNegGetApi \n METHOD == getAllMnStatusCounts();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllMnStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getMnCasesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getMnCasesByStatus(@PathVariable long statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MedNegGetApi \n METHOD == getMnCasesByStatus(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getMnCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = medNegService.isMnCaseAllowed(tblUser.getCompanycode(), "11");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getMnCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }
            TblCompanyprofile tblCompanyprofile = medNegService.getCompanyProfile(tblUser.getCompanycode());
            List<MNCaseList> mnCaseLists = medNegService.getMnCasesByStatus(tblUser.getUsercode(), String.valueOf(statusId), tblCompanyprofile.getTblUsercategory().getCategorycode());

            if (mnCaseLists != null && mnCaseLists.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getMnCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", mnCaseLists);
            } else {
                LOG.info("\n EXITING THIS METHOD == getMnCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == MedNegGetApi \n METHOD == getMnCasesByStatus();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getMnCasesByStatus(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getMnCaseLogs/{Mncode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getDbCaseLogs(@PathVariable String Mncode,
                                                                 HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MedNegGetApi \n METHOD == getMnCaseLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getMnCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblMnlog> tblMnlogs = medNegService.getMnCaseLogs(Mncode);

                if (tblMnlogs != null && tblMnlogs.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getMnCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblMnlogs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getMnCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getMnCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getMnCaseLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getMnCaseLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getMnCaseNotes/{mNcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getMnCaseNotes(@PathVariable String mNcode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MedNegGetApi \n METHOD == getMnCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getMnCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblMnnote> tblMnnotes = medNegService.getMnCaseNotes(mNcode, tblUser);
                List<TblMnnote> tblMnnotesLegalInternal = medNegService.getAuthMnCaseNotesOfLegalInternal(mNcode, tblUser);

                if (tblMnnotes != null && tblMnnotes.size() > 0) {
                    MnNotesResponse mnNotesResponse = new MnNotesResponse();
                    mnNotesResponse.setTblMnnotes(tblMnnotes);
                    mnNotesResponse.setTblMnnotesLegalOnly(tblMnnotesLegalInternal);

                    LOG.info("\n EXITING THIS METHOD == getMnCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", mnNotesResponse);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getMnCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getMnCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getMnCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getMnCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getMnCaseMessages/{mNcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getDbCaseMessages(@PathVariable String mNcode,
                                                                     HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MedNegGetApi \n METHOD == getMnCaseMessages(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getMnCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblMnmessage> tblMnmessages = medNegService.getMnCaseMessages(mNcode);

                if (tblMnmessages != null && tblMnmessages.size() > 0) {
                    for (TblMnmessage tblDbmessage : tblMnmessages) {
                        tblDbmessage.setTblMnclaim(null);
                    }

                    LOG.info("\n EXITING THIS METHOD == getMnCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblMnmessages);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getMnCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getMnCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getMnCaseMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getMnCaseMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getMnAuditLogs/{plCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getMnAuditLogs(@PathVariable String plCode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getMnAuditLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getMnAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<RtaAuditLogResponse> elAuditLogResponses = medNegService.getMnAuditLogs(Long.valueOf(plCode));

                if (elAuditLogResponses != null) {
                    LOG.info("\n EXITING THIS METHOD == getMnAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", elAuditLogResponses);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getMnAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getMnAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == ElGetApi \n METHOD == getMnAuditLogs();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getMnAuditLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

}
