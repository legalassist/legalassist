package com.laportal.controller.medneg;


import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.medneg.MedNegService;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

@RestController
@RequestMapping("/medneg")
public class MedNegPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(MedNegPostApi.class);

    @Autowired
    private MedNegService mnService;

    @RequestMapping(value = "/addNewMnCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewMnCase(@RequestBody SaveMnRequest saveMnRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MedNegPostApi \n METHOD == addNewHdrCase(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = mnService.isMnCaseAllowed(tblUser.getCompanycode(), "11");

            if (allowed) {
                TblMnclaim tblMnclaim = new TblMnclaim();

                tblMnclaim.setTitle(saveMnRequest.getTitle());
                tblMnclaim.setFirstname(saveMnRequest.getFirstname());
                tblMnclaim.setMiddlename(saveMnRequest.getMiddlename());
                tblMnclaim.setLastname(saveMnRequest.getLastname());
                tblMnclaim.setPostalcode(saveMnRequest.getPostalcode());
                tblMnclaim.setAddress1(saveMnRequest.getAddress1());
                tblMnclaim.setAddress2(saveMnRequest.getAddress2());
                tblMnclaim.setAddress3(saveMnRequest.getAddress3());
                tblMnclaim.setCity(saveMnRequest.getCity());
                tblMnclaim.setRegion(saveMnRequest.getRegion());

                tblMnclaim.setAnynotes(saveMnRequest.getAnynotes());
                tblMnclaim.setAnyotherlosses(saveMnRequest.getAnyotherlosses());
                tblMnclaim.setCalldate(saveMnRequest.getCalldate());
                tblMnclaim.setCalltime(saveMnRequest.getCalltime());
                tblMnclaim.setClientcontactno(saveMnRequest.getClientcontactno());
                tblMnclaim.setClientdob(saveMnRequest.getClientdob());
                tblMnclaim.setClientemail(saveMnRequest.getClientemail());
                tblMnclaim.setClientninumber(saveMnRequest.getClientninumber());
                tblMnclaim.setClientoccupation(saveMnRequest.getClientoccupation());
                tblMnclaim.setComplaintmade(saveMnRequest.getComplaintmade());
                tblMnclaim.setComplaintresponded(saveMnRequest.getComplaintresponded());
                tblMnclaim.setEmergencysrvcattend(saveMnRequest.getEmergencysrvcattend());
                tblMnclaim.setGpdetails(saveMnRequest.getGpdetails());
                tblMnclaim.setHospitalattend(saveMnRequest.getHospitalattend());
                tblMnclaim.setHospitaldetails(saveMnRequest.getHospitaldetails());
                tblMnclaim.setIncidentdatetime(saveMnRequest.getIncidentdatetime());
                tblMnclaim.setInjuries(saveMnRequest.getInjuries());
                tblMnclaim.setLocation(saveMnRequest.getLocation());
                tblMnclaim.setMedicalattenddate(saveMnRequest.getMedicalattenddate());
                tblMnclaim.setNegligencedescr(saveMnRequest.getNegligencedescr());
                tblMnclaim.setNotesdetail(saveMnRequest.getNotesdetail());
                tblMnclaim.setOther(saveMnRequest.getOther());
                tblMnclaim.setPoliceref(saveMnRequest.getPoliceref());
                tblMnclaim.setRemarks(saveMnRequest.getRemarks());
                tblMnclaim.setSeparatecomplaintcontact(saveMnRequest.getSeparatecomplaintcontact());
                tblMnclaim.setStilloffwork(saveMnRequest.getStilloffwork());
                tblMnclaim.setTimeoffwork(saveMnRequest.getTimeoffwork());
                tblMnclaim.setTimesincecomplaint(saveMnRequest.getTimesincecomplaint());
                tblMnclaim.setTpaddress(saveMnRequest.getTpaddress());
                tblMnclaim.setTpcompany(saveMnRequest.getTpcompany());
                tblMnclaim.setTpcontactno(saveMnRequest.getTpcontactno());
                tblMnclaim.setTpname(saveMnRequest.getTpname());
                tblMnclaim.setWitnesscontactdetails(saveMnRequest.getWitnesscontactdetails());
                tblMnclaim.setWitnesses(saveMnRequest.getWitnesses());
                tblMnclaim.setWitnessname(saveMnRequest.getWitnessname());
                tblMnclaim.setUpdatedate(new Date());
                tblMnclaim.setHotkeyed(saveMnRequest.getHotkeyed());
                tblMnclaim.setRefundedclientinformed(saveMnRequest.getRefundedclientinformed());
                tblMnclaim.setAgentadviced(saveMnRequest.getAgentadviced());
                tblMnclaim.setGdpr(saveMnRequest.getGdpr());
                tblMnclaim.setAcctime(saveMnRequest.getAcctime());


                tblMnclaim.setStatus(new BigDecimal(779));
                tblMnclaim.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                tblMnclaim.setCreatedate(new Date());
                tblMnclaim.setIntroducer(saveMnRequest.getIntroducer());
                tblMnclaim.setAdvisor(saveMnRequest.getAdvisor());
                tblMnclaim.setEsign("N");

                TblCompanyprofile tblCompanyprofile = mnService.getCompanyProfile(tblUser.getCompanycode());
                tblMnclaim.setMncode(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());

                tblMnclaim = mnService.saveMnRequest(tblMnclaim);

                if (tblMnclaim != null) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = mnService.saveCompanyProfile(tblCompanyprofile);

                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblMnclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == MedNegPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/updateMnCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateMnCase(@RequestBody UpdateMnRequest updateMnRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MedNegPostApi \n METHOD == updateHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = mnService.isMnCaseAllowed(tblUser.getCompanycode(), "11");

            if (allowed) {
                TblMnclaim tblMnclaim = mnService.findMnCaseByIdWithoutUser(updateMnRequest.getMnclaimcode());

                tblMnclaim.setTitle(updateMnRequest.getTitle());
                tblMnclaim.setFirstname(updateMnRequest.getFirstname());
                tblMnclaim.setMiddlename(updateMnRequest.getMiddlename());
                tblMnclaim.setLastname(updateMnRequest.getLastname());
                tblMnclaim.setPostalcode(updateMnRequest.getPostalcode());
                tblMnclaim.setAddress1(updateMnRequest.getAddress1());
                tblMnclaim.setAddress2(updateMnRequest.getAddress2());
                tblMnclaim.setAddress3(updateMnRequest.getAddress3());
                tblMnclaim.setCity(updateMnRequest.getCity());
                tblMnclaim.setRegion(updateMnRequest.getRegion());

                tblMnclaim.setAnynotes(updateMnRequest.getAnynotes());
                tblMnclaim.setAnyotherlosses(updateMnRequest.getAnyotherlosses());
                tblMnclaim.setCalldate(updateMnRequest.getCalldate());
                tblMnclaim.setCalltime(updateMnRequest.getCalltime());
                tblMnclaim.setClientcontactno(updateMnRequest.getClientcontactno());
                tblMnclaim.setClientdob(updateMnRequest.getClientdob());
                tblMnclaim.setClientemail(updateMnRequest.getClientemail());
                tblMnclaim.setClientninumber(updateMnRequest.getClientninumber());
                tblMnclaim.setClientoccupation(updateMnRequest.getClientoccupation());
                tblMnclaim.setComplaintmade(updateMnRequest.getComplaintmade());
                tblMnclaim.setComplaintresponded(updateMnRequest.getComplaintresponded());
                tblMnclaim.setEmergencysrvcattend(updateMnRequest.getEmergencysrvcattend());
                tblMnclaim.setGpdetails(updateMnRequest.getGpdetails());
                tblMnclaim.setHospitalattend(updateMnRequest.getHospitalattend());
                tblMnclaim.setHospitaldetails(updateMnRequest.getHospitaldetails());
                tblMnclaim.setIncidentdatetime(updateMnRequest.getIncidentdatetime());
                tblMnclaim.setInjuries(updateMnRequest.getInjuries());
                tblMnclaim.setLocation(updateMnRequest.getLocation());
                tblMnclaim.setMedicalattenddate(updateMnRequest.getMedicalattenddate());
                tblMnclaim.setNegligencedescr(updateMnRequest.getNegligencedescr());
                tblMnclaim.setNotesdetail(updateMnRequest.getNotesdetail());
                tblMnclaim.setOther(updateMnRequest.getOther());
                tblMnclaim.setPoliceref(updateMnRequest.getPoliceref());
                tblMnclaim.setRemarks(updateMnRequest.getRemarks());
                tblMnclaim.setSeparatecomplaintcontact(updateMnRequest.getSeparatecomplaintcontact());
                tblMnclaim.setStilloffwork(updateMnRequest.getStilloffwork());
                tblMnclaim.setTimeoffwork(updateMnRequest.getTimeoffwork());
                tblMnclaim.setTimesincecomplaint(updateMnRequest.getTimesincecomplaint());
                tblMnclaim.setTpaddress(updateMnRequest.getTpaddress());
                tblMnclaim.setTpcompany(updateMnRequest.getTpcompany());
                tblMnclaim.setTpcontactno(updateMnRequest.getTpcontactno());
                tblMnclaim.setTpname(updateMnRequest.getTpname());
                tblMnclaim.setWitnesscontactdetails(updateMnRequest.getWitnesscontactdetails());
                tblMnclaim.setWitnesses(updateMnRequest.getWitnesses());
                tblMnclaim.setWitnessname(updateMnRequest.getWitnessname());
                tblMnclaim.setUpdatedate(new Date());
                tblMnclaim.setHotkeyed(updateMnRequest.getHotkeyed());
                tblMnclaim.setRefundedclientinformed(updateMnRequest.getRefundedclientinformed());
                tblMnclaim.setAgentadviced(updateMnRequest.getAgentadviced());
                tblMnclaim.setGdpr(updateMnRequest.getGdpr());

                tblMnclaim.setUpdatedate(new Date());
                tblMnclaim.setLastupdateuser(tblUser.getUsercode());
                tblMnclaim.setAcctime(updateMnRequest.getAcctime());

                tblMnclaim = mnService.updateMnCase(tblMnclaim);

                if (tblMnclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblMnclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == MedNegPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addMnNotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addMnNotes(@RequestBody MnNoteRequest mnNoteRequest,
                                                              HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MedNegPostApi \n METHOD == addDbNotes(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = mnService.isMnCaseAllowed(tblUser.getCompanycode(), "11");

            if (allowed) {
                if (!mnNoteRequest.getDbCode().isEmpty()) {
                    TblMnnote tblDbnote = new TblMnnote();
                    TblMnclaim tblMnclaim = new TblMnclaim();

                    tblMnclaim.setMnclaimcode(Long.valueOf(mnNoteRequest.getDbCode()));
                    tblDbnote.setTblMnclaim(tblMnclaim);
                    tblDbnote.setNote(mnNoteRequest.getNote());
                    tblDbnote.setUsercategorycode(mnNoteRequest.getUserCatCode());
                    tblDbnote.setCreatedon(new Date());
                    tblDbnote.setUsercode(tblUser.getUsercode());

                    tblDbnote = mnService.addTblMnNote(tblDbnote);

                    if (tblDbnote != null && tblDbnote.getMnnotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblDbnote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == MedNegPostApi \n METHOD == addHdrNotes();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/resendMnEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> resendMnEmail(@RequestBody ResendMnMessageDto resendMnMessageDto,
                                                                 HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == resendRtaEmail(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }


            TblEmail tblEmail = mnService.resendEmail(resendMnMessageDto.getMnMessagecode());
            if (tblEmail != null && tblEmail.getEmailcode() > 0) {

                LOG.info("\n EXITING THIS METHOD == resendElEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Success", tblEmail);
            } else {
                LOG.info("\n EXITING THIS METHOD == resendElEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While resending Email", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == resendRtaEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

//    @RequestMapping(value = "/assigncasetosolicitorbyLA", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<HashMap<String, Object>> assigncasetosolicitorbyLA(
//            @RequestBody AssignMnCasetoSolicitor assignMnCasetoSolicitor, HttpServletRequest request)
//            throws ParseException {
//        try {
//            LOG.info("\n\n\nINSIDE \n CLASS == MedNegPostApi \n METHOD == performActionOnHdrByLegalAssist(); ");
//
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
//            if (tblUser == null) {
//                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
//                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
//            }
//
//            if (tblUser != null) {
//                TblMnclaim tblMnclaim = mnService.assignCaseToSolicitor(assignMnCasetoSolicitor, tblUser);
//                if (tblMnclaim != null) {
//
//                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
//                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblMnclaim);
//                } else {
//                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
//                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
//                }
//
//
//            } else {
//                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
//                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
//            }
//
//        } catch (Exception e) {
//            LOG.error("\n CLASS == MedNegPostApi \n METHOD == performActionOnHdrByLegalAssist();  ERROR ----- "
//                    + e.getLocalizedMessage());
//            LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
//            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
//        }
//    }

    @RequestMapping(value = "/performActionOnMnFromDirectIntro", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnElFromDirectIntro(
            @RequestBody PerformHotKeyOnRtaRequest performHotKeyOnElRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnElFromDirectIntro(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnElFromDirectIntro(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblMnclaim tblMnclaim = mnService.performActionOnMnFromDirectIntro(performHotKeyOnElRequest,
                        tblUser);
                if (tblMnclaim != null) {

                    LOG.info("\n EXITING THIS METHOD == performActionOnElFromDirectIntro(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblMnclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnElFromDirectIntro(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnElFromDirectIntro(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnElFromDirectIntro();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnElFromDirectIntro(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnMn", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnMn(
            @RequestBody PerformActionOnMnRequest performActionOnMnRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnHdr(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblMnclaim tblMnclaim = mnService.findMnCaseByIdWithoutUser(Long.parseLong(performActionOnMnRequest.getMncode()));


            if (performActionOnMnRequest.getToStatus().equals("785")) {
                List<TblMntask> tblELtask = mnService
                        .getMnTaskAgainstELCodeAndStatus(performActionOnMnRequest.getMncode(), "N");
                List<TblMntask> tblELtask1 = mnService
                        .getMnTaskAgainstELCodeAndStatus(performActionOnMnRequest.getMncode(), "P");

                if (tblELtask != null && tblELtask.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == performActionOnMn(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Some Task Are Not Performed", tblMnclaim);
                } else if (tblELtask1 != null && tblELtask1.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == performActionOnMn(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Some Task Are In Pending State", tblMnclaim);
                } else {

                }
            }


            if (tblUser != null) {
                if ((performActionOnMnRequest.getToStatus().equals("789")) ||
                        (performActionOnMnRequest.getToStatus().equals("829")) ||
                        (performActionOnMnRequest.getToStatus().equals("830")) ||
                        (performActionOnMnRequest.getToStatus().equals("831"))) {
                    // reject status
                    tblMnclaim = mnService.performRejectCancelActionOnMn(performActionOnMnRequest.getMncode(),
                            performActionOnMnRequest.getToStatus(), performActionOnMnRequest.getReason(), tblUser);

                } else if (performActionOnMnRequest.getToStatus().equals("792")) {
                    //cancel status
                    tblMnclaim = mnService.performRejectCancelActionOnMn(performActionOnMnRequest.getMncode(),
                            performActionOnMnRequest.getToStatus(), performActionOnMnRequest.getReason(), tblUser);

                } else if (performActionOnMnRequest.getToStatus().equals("802")) {
                    // revert status
                    tblMnclaim = mnService.performRevertActionOnMn(performActionOnMnRequest.getMncode(),
                            performActionOnMnRequest.getToStatus(), performActionOnMnRequest.getReason(), tblUser);

                } else {
                    tblMnclaim = mnService.performActionOnMn(performActionOnMnRequest.getMncode(),
                            performActionOnMnRequest.getToStatus(), tblUser);
                }

                if (tblMnclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == performActionOnMn(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblMnclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == MedNegPostApi \n METHOD == addESign(); ");
        try {

            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("code")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }

            TblMnclaim tblMnclaim = mnService.findMnCaseByIdWithoutUser(addESign.getRtaCode());
            if (tblMnclaim != null && tblMnclaim.getMnclaimcode() > 0) {

                if (tblMnclaim.getEsign().equals("Y")) {
                    List<TblMndocument> tblMndocuments = mnService.getAuthMnCasedocuments(tblMnclaim.getMnclaimcode());
                    for (TblMndocument tblMndocument : tblMndocuments) {
                        if (tblMndocument.getDoctype().equalsIgnoreCase("Esign")) {
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("signedDocument", tblMndocument.getDocbase64());
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.OK, "You Have Already Signed The Document",
                                    map);
                        }
                    }

                }
                // solicitor company
                TblCompanyprofile tblCompanyprofile = mnService
                        .getCompanyProfileAgainstMnCode(tblMnclaim.getMnclaimcode());
                if (tblCompanyprofile != null) {

                    TblCompanydoc tblCompanydoc = mnService.getCompanyDocs(tblCompanyprofile.getCompanycode());
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                    byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                    ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                    BufferedImage image1 = ImageIO.read(bis);
                    bis.close();


                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();

                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            while (keys.hasNext()) {
                                String key = keys.next();
                                if (!key.equals("code") && !key.equals("eSign")) {
                                    if (fieldName.equals(key)) {
                                        textField.setValue(jsonObject.getString(key));
                                    }
                                }
                            }

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = mnService.getMnDbColumnValue(fieldName.replace("DB_", ""),
                                        tblMnclaim.getMnclaimcode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            }
                            if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    for (int a = 0; a < document.getNumberOfPages(); a++) {
                        PDPage p = document.getPage(a);
                        PDResources resources = p.getResources();
                        for (COSName xObjectName : resources.getXObjectNames()) {
                            PDXObject xObject = resources.getXObject(xObjectName);
                            if (xObject instanceof PDImageXObject) {
                                PDImageXObject original_img = ((PDImageXObject) xObject);
                                PDImageXObject pdImageXObject = LosslessFactory.createFromImage(document, image1);
                                resources.put(xObjectName, pdImageXObject);
                            }
                        }
                    }

                    LOG.info("\n Esign Document Prepared SuccessFully\n");
                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Mn\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Mn\\";

                    File theDir = new File(UPLOADED_FOLDER + tblMnclaim.getMnclaimcode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblMnclaim.getMncode() + "_Signed.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblMnclaim.getMncode() + "_Signed.pdf");
                    }

                    tblMnclaim.setEsign("Y");
                    tblMnclaim.setEsigndate(new Date());
                    tblMnclaim = mnService.updateMnCase(tblMnclaim);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    LOG.info("\n Esign Saved on folder SuccessFully SuccessFully\n");
                    File file = new File(theDir.getPath() + "\\" + tblMnclaim.getMncode() + "_Signed.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    TblTask tblMntaskdocument = new TblTask();
                    tblMntaskdocument.setTaskcode(35);

                    TblMndocument tblMndocument = new TblMndocument();
                    tblMndocument.setTblMnclaim(tblMnclaim);
                    tblMndocument.setDocbase64(encoded);
                    tblMndocument.setDocname("Esign");
                    tblMndocument.setDoctype("Esign");
                    tblMndocument.setCreateddate(new Date());
                    tblMndocument.setCreateuser("1");
                    tblMndocument.setDocurl(
                            UPLOADED_SERVER + tblMnclaim.getMnclaimcode() + "\\" + tblMnclaim.getMncode() + "_Signed.pdf");
                    tblMndocument.setTblTask(tblMntaskdocument);

                    tblMndocument = mnService.addMnSingleDocumentSingle(tblMndocument);
                    LOG.info("\n saving doc in database\n");
                    if (tblMndocument != null && tblMndocument.getMndocumentscode() > 0) {
//                    Locale.setDefault(backup);

                        TblMntask tblMntask = new TblMntask();
                        tblMntask.setTaskcode(new BigDecimal(35));
                        tblMntask.setTblMnclaim(tblMnclaim);
                        tblMntask.setStatus("C");
                        tblMntask.setRemarks("Completed");
                        tblMntask.setCompletedon(new Date());

                        int tblMntask1 = mnService.updateTblMntask(tblMntask);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("doc", encoded);
                        TblUser legalUser = mnService.findByUserId("2");

                        // cleint Email
                        TblEmailTemplate tblEmailTemplate = mnService.findByEmailType("esign-clnt-complete");
                        String clientbody = tblEmailTemplate.getEmailtemplate();

                        clientbody = clientbody.replace("[LEGAL_CLIENT_NAME]", tblMnclaim.getFirstname() +(tblMnclaim.getMiddlename() != null?tblMnclaim.getMiddlename():"") +" "+tblMnclaim.getLastname());
//                                tblMnclaim.getFirstname() + " "
////                                        + (tblMnclaim.getMiddlename() == null ? "" : tblMnclaim.getMiddlename() + " ")
////                                        + tblMnclaim.getLastname());
                        clientbody = clientbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());

                        mnService.saveEmail(tblMnclaim.getClientemail(), clientbody, "No Win No Fee Agreement ", tblMnclaim, legalUser, file.getPath());
                        LOG.info("\n Email Sent To client\n");
                        // Introducer Email
                        tblEmailTemplate = mnService.findByEmailType("esign-Int-sol-complete");
                        String Introducerbody = tblEmailTemplate.getEmailtemplate();
                        TblUser introducerUser = mnService.findByUserId(String.valueOf(tblMnclaim.getAdvisor()));

                        Introducerbody = Introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
                        Introducerbody = Introducerbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
                        Introducerbody = Introducerbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() +(tblMnclaim.getMiddlename() != null?tblMnclaim.getMiddlename():"") +" "+tblMnclaim.getLastname());
//                                tblMnclaim.getFirstname() + " "
//                                        + (tblMnclaim.getMiddlename() == null ? "" : tblMnclaim.getMiddlename() + " ")
//                                        + tblMnclaim.getLastname());
                        Introducerbody = Introducerbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        Introducerbody = Introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblMnclaim.getMnclaimcode());

                        mnService.saveEmail(introducerUser.getUsername(), Introducerbody, "Esign", tblMnclaim, introducerUser);
                        LOG.info("\n Email Sent to intro\n");
                        // PI department Email
                        String LegalPideptbody = tblEmailTemplate.getEmailtemplate();

                        LegalPideptbody = LegalPideptbody.replace("[USER_NAME]", legalUser.getLoginid());
                        LegalPideptbody = LegalPideptbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
                        LegalPideptbody = LegalPideptbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        LegalPideptbody = LegalPideptbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() +(tblMnclaim.getMiddlename() != null?tblMnclaim.getMiddlename():"") +" "+tblMnclaim.getLastname());
//                                tblMnclaim.getFirstname() + " "
//                                        + (tblMnclaim.getMiddlename() == null ? "" : tblMnclaim.getMiddlename() + " ")
//                                        + tblMnclaim.getLastname());
                        LegalPideptbody = LegalPideptbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblMnclaim.getMnclaimcode());

                        mnService.saveEmail(legalUser.getUsername(), LegalPideptbody, "Esign", tblMnclaim, legalUser);


                        saveEsignSubmitStatus(String.valueOf(tblMnclaim.getMnclaimcode()), "9", request);

                        LOG.info("\n Email Sent to Dept\n");
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "You Have SuccessFully Signed The CFA, A Copy Of That CFA Has Been Sent To The Provided Email.", map);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST,
                                "Error While Saving The Document..Esign Compeleted", null);
                    }

                } else {
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No document template found against provided RTA",
                            null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }
            
            
        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addESign(); ");
        try {


            TblMnclaim tblMnclaim = mnService.findMnCaseByIdWithoutUser(Long.valueOf(eSignFieldsRequest.getCode()));

            if (tblMnclaim != null && tblMnclaim.getMnclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = mnService
                        .getCompanyProfileAgainstMnCode(tblMnclaim.getMnclaimcode());
                if (!tblMnclaim.getEsign().equals("Y")) {

                    TblCompanydoc tblCompanydoc = mnService.getCompanyDocs(tblCompanyprofile.getCompanycode());
                    if (tblCompanydoc == null) {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "No CFA document found", null);
                    }
                    HashMap<String, String> fields = new HashMap<>();
                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();


                    /////////////////////// FILL the CFA With Client DATA ////////////////////////


                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = mnService.getMnDbColumnValue(fieldName.replace("DB_", ""),
                                        tblMnclaim.getMnclaimcode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            } else if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            } else {
                                fields.put(fieldName, fieldName);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    /////////////////////// END OF FILL the CFA With Client DATA /////////////////

                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Mn\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Mn\\";

                    File theDir = new File(UPLOADED_FOLDER + tblMnclaim.getMnclaimcode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblMnclaim.getMncode() + "_view.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblMnclaim.getMncode() + "_view.pdf");
                    }

                    File file = new File(theDir.getPath() + "\\" + tblMnclaim.getMncode() + "_view.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    fields.put("doc", encoded);
                    fields.put("isEsign", "N");

                    saveEsignOpenStatus(String.valueOf(tblMnclaim.getMnclaimcode()), "11", request);

                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", fields);
                } else {

                    List<TblMndocument> tblMndocuments = mnService.getAuthMnCasedocuments(tblMnclaim.getMnclaimcode());
                    if (tblMndocuments != null && tblMndocuments.size() > 0) {
                        for (TblMndocument tblMndocument : tblMndocuments) {
                            if (tblMndocument.getTblTask() != null && tblMndocument.getTblTask().getTaskcode() == 35) {
                                HashMap<String, String> fields = new HashMap<>();
                                fields.put("doc", tblMndocument.getDocbase64());
                                fields.put("isEsign", "Y");
                                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                                return getResponseFormat(HttpStatus.OK, "You have Already Signed The Document.", fields);
                            }
                        }
                    }

                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Signed Document Found Please Contact Our HelpLine : please call us at 01615374448", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No EL document found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == MedNegPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("mnClaimCode") long mnClaimCode, @RequestParam("taskCode") long taskCode,
                                                               @RequestParam(value = "multipartFiles", required = false) List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblMnclaim tblMnclaim = mnService.findMnCaseByIdWithoutUser(mnClaimCode);
            TblMnsolicitor tblMnsolicitor = mnService.getMnSolicitorsOfMnClaim(mnClaimCode);
            TblCompanyprofile solicitorCompany = mnService.getCompanyProfile(tblMnsolicitor.getCompanycode());
            if (tblMnclaim != null && tblMnclaim.getMnclaimcode() > 0) {
                TblTask tblTask = mnService.getTaskAgainstCode(taskCode);
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblMntask tblMntask = new TblMntask();
                    tblMntask.setMntaskcode(taskCode);
                    tblMntask.setTblMnclaim(tblMnclaim);
                    if (tblTask.getTaskcode() == 35) {
                        TblCompanydoc tblCompanydoc = mnService.getCompanyDocs(solicitorCompany.getCompanycode());
                        if (tblCompanydoc == null) {
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "There is No CFA Attach To the Solicitor", null);
                        }
                        String emailUrl = getDataFromProperties("esign.baseurl") + "Mn=" + tblMnclaim.getMnclaimcode();
                        TblEmailTemplate tblEmailTemplate = getEmailTemplateByType("esign");

                        String body = tblEmailTemplate.getEmailtemplate();
                        body = body.replace("[LEGAL_CLIENT_NAME]", tblMnclaim.getFirstname() +(tblMnclaim.getMiddlename() != null?tblMnclaim.getMiddlename():"") +" "+tblMnclaim.getLastname());
                        body = body.replace("[SOLICITOR_COMPANY_NAME]", solicitorCompany.getName());
                        body = body.replace("[ESIGN_URL]", emailUrl);
                        if (tblMnclaim.getClientemail() != null) {
//                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
//                            return getResponseFormat(HttpStatus.BAD_REQUEST, "No Client Email Address Found", null);
                            LOG.info("\n Email Sendpos");
                            mnService.saveEmail(tblMnclaim.getClientemail(), body, "No Win No Fee Agreement", tblMnclaim, tblUser);
                        }
                        LOG.info("\n Sending SMS ");
                        if (tblMnclaim.getClientcontactno() != null && !tblMnclaim.getClientcontactno().isEmpty()) {
                            LOG.info("\n SMS SEND");
                            TblEmailTemplate tblEmailTemplateSMS = getEmailTemplateByType("esign-SMS");
                            String message = tblEmailTemplateSMS.getEmailtemplate();
                            message = message.replace("[LEGAL_CLIENT_NAME]", tblMnclaim.getFirstname() +(tblMnclaim.getMiddlename() != null?tblMnclaim.getMiddlename():"") +" "+tblMnclaim.getLastname());
                            message = message.replace("[SOLICITOR_COMPANY_NAME]", solicitorCompany.getName());
                            message = message.replace("[ESIGN_URL]", emailUrl);


                            sendSMS(tblMnclaim.getClientcontactno(), message);
                            tblMntask.setRemarks("Esign/SMS Email Sent Successfully to Client");
                        } else {
                            tblMntask.setRemarks("Esign Email Sent Successfully to Client");
                        }

                        saveEmailSmsEsignStatus(String.valueOf(tblMnclaim.getMnclaimcode()), "9", tblUser.getUsercode());

                        tblMntask.setStatus("P");
                    } else {

                        String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Mn\\";
                        String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Mn\\";
                        List<TblMndocument> tblMndocuments = new ArrayList<TblMndocument>();

                        File theDir = new File(UPLOADED_FOLDER + tblMnclaim.getMnclaimcode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }

                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);
                            TblMndocument tblMndocument = new TblMndocument();
                            tblMndocument.setDocurl(UPLOADED_SERVER + tblMnclaim.getMnclaimcode() + "\\"
                                    + multipartFile.getOriginalFilename());
                            tblMndocument.setDocumentPath(tblMndocument.getDocurl());
                            tblMndocument.setDoctype(multipartFile.getContentType());
                            tblMndocument.setTblMnclaim(tblMnclaim);
                            tblMndocument.setTblTask(tblTask);
                            tblMndocument.setCreateuser(tblUser.getUsercode());
                            tblMndocument.setCreateddate(new Date());

                            tblMndocuments.add(tblMndocument);
                        }
                        mnService.saveTblMndocuments(tblMndocuments);
                        tblMntask.setStatus("C");
                        tblMntask.setRemarks("Completed");

                    }
                    tblMntask.setCompletedon(new Date());
                    tblMntask.setCompletedby(tblUser.getUsercode());
                    int tblRtatask1 = mnService.updateTblMntask(tblMntask);
                    if (tblRtatask1 > 0) {

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblRtatask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }



    @RequestMapping(value = "/addMnDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> addMnDocument(@RequestParam("elClaimCode") String elClaimCode,
                                                                 @RequestParam("multipartFiles") List<MultipartFile> multipartFiles, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == MnPostApi \n METHOD == addMnDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addMnDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblMnclaim tblMnclaim = mnService.findMnCaseByIdWithoutUser(Long.valueOf(elClaimCode));
            if (tblMnclaim != null && tblMnclaim.getMnclaimcode() > 0) {
//                String UPLOADED_FOLDER = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0_Tomcat9-la\\webapps\\Mn\\";
//                String UPLOADED_SERVER = "http:\\115.186.147.30:8080\\Mn\\";
                String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Mn\\";
                String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Mn\\";
                List<TblMndocument> tblMndocuments = new ArrayList<TblMndocument>();

                File theDir = new File(UPLOADED_FOLDER + tblMnclaim.getMnclaimcode());
                if (!theDir.exists()) {
                    theDir.mkdirs();
                }

                for (MultipartFile multipartFile : multipartFiles) {
                    byte[] bytes = multipartFile.getBytes();
                    Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                    Files.write(path, bytes);
                    TblMndocument tblMndocument = new TblMndocument();
                    tblMndocument.setDocurl(
                            UPLOADED_SERVER + tblMnclaim.getMnclaimcode() + "\\" + multipartFile.getOriginalFilename());
                    tblMndocument.setDocumentPath(tblMndocument.getDocurl());
                    tblMndocument.setDoctype(multipartFile.getContentType());
                    tblMndocument.setTblMnclaim(tblMnclaim);
                    tblMndocument.setCreateuser(tblUser.getUsercode());
                    tblMndocument.setCreateddate(new Date());

                    tblMndocuments.add(tblMndocument);
                }

                tblMndocuments = mnService.saveTblMndocuments(tblMndocuments);

                // Legal internal Email
                TblEmailTemplate tblEmailTemplate = mnService.findByEmailType("document-add");
                String legalbody = tblEmailTemplate.getEmailtemplate();
                TblUser legalUser = mnService.findByUserId("2");

                legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
                legalbody = legalbody.replace("[CASE_NUMBER]", tblMnclaim.getMncode());
                legalbody = legalbody.replace("[CLIENT_NAME]", tblMnclaim.getFirstname() +(tblMnclaim.getMiddlename() != null?tblMnclaim.getMiddlename():"") +" "+tblMnclaim.getLastname());
//                        tblMnclaim.getFirstname() + " "
//                                + (tblMnclaim.getMiddlename() == null ? "" : tblMnclaim.getMiddlename() + " ")
//                                + tblMnclaim.getLastname());
                legalbody = legalbody.replace("[COUNT]", String.valueOf(tblMndocuments.size()));
                legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.Mn") + tblMnclaim.getMnclaimcode());

                mnService.saveEmail(legalUser.getUsername(), legalbody,
                        tblMnclaim.getMncode() + " | Processing Note", tblMnclaim, legalUser);

                LOG.info("\n EXITING THIS METHOD == addMnDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Document Saved Successfully", null);
            } else {
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Find", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == MnPostApi \n METHOD == addMnDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addMnDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/setCurrentTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> setCurrentTask(@RequestBody CurrentTaskRequest currentTaskRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == setCurrentTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            int tblRtatask1 = mnService.updateCurrentTask(currentTaskRequest.getTaskCode(),
                    currentTaskRequest.getMnCode(), currentTaskRequest.getCurrent());
            if (tblRtatask1 > 0) {
                List<TblMntask> tblMntaskList = mnService
                        .findTblMntaskByMnClaimCode(currentTaskRequest.getMnCode());

                if (tblMntaskList != null && tblMntaskList.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Current Task Updated SuccessFully", tblMntaskList);
                } else {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Updating Task", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == setCurrentTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/deleteMnDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> deleteRtaDocument(
            @RequestBody DeleteRtaDocumentRequest deleteRtaDocumentRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == deleteRtaDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = mnService.getCompanyProfile(tblUser.getCompanycode());
            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {

                mnService.deleteMnDocument(deleteRtaDocumentRequest.getDoccode());
                return getResponseFormat(HttpStatus.OK, "Document Deleted", null);
            } else {
                LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Action", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaPostApi \n METHOD == deleteRtaDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
