package com.laportal.controller.el;


import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.el.ElService;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/El")
public class ElPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(ElPostApi.class);

    @Autowired
    private ElService elService;

    @RequestMapping(value = "/addNewElCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewHdrCase(@RequestBody SaveElRequest saveElRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addNewHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = elService.isELCaseAllowed(tblUser.getCompanycode(), "9");

            if (allowed) {
                TblElclaim tblElclaim = new TblElclaim();

                tblElclaim.setTitle(saveElRequest.getTitle());
                tblElclaim.setFirstname(saveElRequest.getFirstname());
                tblElclaim.setMiddlename(saveElRequest.getMiddlename());
                tblElclaim.setLastname(saveElRequest.getLastname());
                tblElclaim.setPostalcode(saveElRequest.getPostalcode());
                tblElclaim.setAddress1(saveElRequest.getAddress1());
                tblElclaim.setAddress2(saveElRequest.getAddress2());
                tblElclaim.setAddress3(saveElRequest.getAddress3());
                tblElclaim.setCity(saveElRequest.getCity());
                tblElclaim.setRegion(saveElRequest.getRegion());

                tblElclaim.setAccbookcopy(saveElRequest.getAccbookcopy());
                tblElclaim.setAcccircumstances(saveElRequest.getAcccircumstances());
                tblElclaim.setAccdatetime(saveElRequest.getAccdatetime());
                tblElclaim.setAcclocation(saveElRequest.getAcclocation());
                tblElclaim.setAdditionalclaiminfo(saveElRequest.getAdditionalclaiminfo());
                tblElclaim.setAgencyname(saveElRequest.getAgencyname());
                tblElclaim.setAmbulancecalled(saveElRequest.getAmbulancecalled());
                tblElclaim.setAnyinvestigation(saveElRequest.getAnyinvestigation());
                tblElclaim.setAnyotherlosses(saveElRequest.getAnyotherlosses());
                tblElclaim.setAnypremedicalconds(saveElRequest.getAnypremedicalconds());
                tblElclaim.setAnypreviousacc(saveElRequest.getAnypreviousacc());
                tblElclaim.setAnyriskassessments(saveElRequest.getAnyriskassessments());
                tblElclaim.setAnytreatmentreceived(saveElRequest.getAnytreatmentreceived());
                tblElclaim.setAnywitnesses(saveElRequest.getAnywitnesses());
                tblElclaim.setAwaresimilaraccident(saveElRequest.getAwaresimilaraccident());
                tblElclaim.setChangestosystem(saveElRequest.getChangestosystem());
                tblElclaim.setClaimantsufferinjuries(saveElRequest.getClaimantsufferinjuries());
                tblElclaim.setClientattendedhospital(saveElRequest.getClientattendedhospital());
                tblElclaim.setClientinjuries(saveElRequest.getClientinjuries());
                tblElclaim.setClientpresentcond(saveElRequest.getClientpresentcond());
                tblElclaim.setClientseenaccbook(saveElRequest.getClientseenaccbook());
                tblElclaim.setClientseengp(saveElRequest.getClientseengp());
                tblElclaim.setClientstillwork(saveElRequest.getClientstillwork());
                tblElclaim.setCompanysickpay(saveElRequest.getCompanysickpay());
                tblElclaim.setContactno(saveElRequest.getMobile());
                tblElclaim.setDailyactivities(saveElRequest.getDailyactivities());
                tblElclaim.setDategpattendance(saveElRequest.getDategpattendance());
                tblElclaim.setDatehospitalattendance(saveElRequest.getDatehospitalattendance());
                tblElclaim.setEmployeraddress(saveElRequest.getEmployeraddress());
                tblElclaim.setEmployercontactno(saveElRequest.getEmployercontactno());
                tblElclaim.setEmployername(saveElRequest.getEmployername());
                tblElclaim.setEmploymentduration(saveElRequest.getEmploymentduration());
                tblElclaim.setEstimatednetweeklywage(saveElRequest.getEstimatednetweeklywage());
                tblElclaim.setGpaddress(saveElRequest.getGpaddress());
                tblElclaim.setGpadvisegiven(saveElRequest.getGpadvisegiven());
                tblElclaim.setGpname(saveElRequest.getGpname());
                tblElclaim.setHospitaladdress(saveElRequest.getHospitaladdress());
                tblElclaim.setHospitaladvisegiven(saveElRequest.getHospitaladvisegiven());
                tblElclaim.setHospitalname(saveElRequest.getHospitalname());
                tblElclaim.setInjuriessymptomsstart(saveElRequest.getInjuriessymptomsstart());
                tblElclaim.setJobtitle(saveElRequest.getJobtitle());
                tblElclaim.setLossofearnings(saveElRequest.getLossofearnings());
                tblElclaim.setNinumber(saveElRequest.getNinumber());
                tblElclaim.setNoworkadvised(saveElRequest.getNoworkadvised());
                tblElclaim.setOtherlossesdetail(saveElRequest.getOtherlossesdetail());
                tblElclaim.setPeriodofabsence(saveElRequest.getPeriodofabsence());
                tblElclaim.setPremedicalconddetail(saveElRequest.getPremedicalconddetail());
                tblElclaim.setPreviousaccdetail(saveElRequest.getPreviousaccdetail());
                tblElclaim.setProtectiveequipment(saveElRequest.getProtectiveequipment());
                tblElclaim.setReportedbywhom(saveElRequest.getReportedbywhom());
                tblElclaim.setTrainingreceived(saveElRequest.getTrainingreceived());
                tblElclaim.setUserofprocequip(saveElRequest.getUserofprocequip());
                tblElclaim.setWitnessdetails(saveElRequest.getWitnessdetails());
                tblElclaim.setDob(saveElRequest.getDob());
                tblElclaim.setRemarks(saveElRequest.getRemarks());
                tblElclaim.setMobile(saveElRequest.getMobile());
                tblElclaim.setAcctime(saveElRequest.getAcctime());
                tblElclaim.setWasclientwearing(saveElRequest.getWasclientwearing());
                tblElclaim.setEmail(saveElRequest.getEmail());


                tblElclaim.setStatus(new BigDecimal(559));
                tblElclaim.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                tblElclaim.setCreatedate(new Date());
                tblElclaim.setIntroducer(saveElRequest.getIntroducer());
                tblElclaim.setAdvisor(saveElRequest.getAdvisor());
                tblElclaim.setEsig("N");

                TblCompanyprofile tblCompanyprofile = elService.getCompanyProfile(tblUser.getCompanycode());
                tblElclaim.setElcode(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());

                tblElclaim = elService.saveElRequest(tblElclaim);

                if (tblElclaim != null) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = elService.saveCompanyProfile(tblCompanyprofile);

                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblElclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/updateElCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateElCase(@RequestBody UpdateElRequest updateElRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == updateHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = elService.isELCaseAllowed(tblUser.getCompanycode(), "9");

            if (allowed) {
                TblElclaim tblElclaim = elService.findElCaseByIdWithoutUser(updateElRequest.getElclaimcode());

                tblElclaim.setTitle(updateElRequest.getTitle());
                tblElclaim.setFirstname(updateElRequest.getFirstname());
                tblElclaim.setMiddlename(updateElRequest.getMiddlename());
                tblElclaim.setLastname(updateElRequest.getLastname());
                tblElclaim.setPostalcode(updateElRequest.getPostalcode());
                tblElclaim.setAddress1(updateElRequest.getAddress1());
                tblElclaim.setAddress2(updateElRequest.getAddress2());
                tblElclaim.setAddress3(updateElRequest.getAddress3());
                tblElclaim.setCity(updateElRequest.getCity());
                tblElclaim.setRegion(updateElRequest.getRegion());

                tblElclaim.setElclaimcode(updateElRequest.getElclaimcode());
                tblElclaim.setAccbookcopy(updateElRequest.getAccbookcopy());
                tblElclaim.setAcccircumstances(updateElRequest.getAcccircumstances());
                tblElclaim.setAccdatetime(updateElRequest.getAccdatetime());
                tblElclaim.setAcclocation(updateElRequest.getAcclocation());
                tblElclaim.setAdditionalclaiminfo(updateElRequest.getAdditionalclaiminfo());
                tblElclaim.setAgencyname(updateElRequest.getAgencyname());
                tblElclaim.setAmbulancecalled(updateElRequest.getAmbulancecalled());
                tblElclaim.setAnyinvestigation(updateElRequest.getAnyinvestigation());
                tblElclaim.setAnyotherlosses(updateElRequest.getAnyotherlosses());
                tblElclaim.setAnypremedicalconds(updateElRequest.getAnypremedicalconds());
                tblElclaim.setAnypreviousacc(updateElRequest.getAnypreviousacc());
                tblElclaim.setAnyriskassessments(updateElRequest.getAnyriskassessments());
                tblElclaim.setAnytreatmentreceived(updateElRequest.getAnytreatmentreceived());
                tblElclaim.setAnywitnesses(updateElRequest.getAnywitnesses());
                tblElclaim.setAwaresimilaraccident(updateElRequest.getAwaresimilaraccident());
                tblElclaim.setChangestosystem(updateElRequest.getChangestosystem());
                tblElclaim.setClaimantsufferinjuries(updateElRequest.getClaimantsufferinjuries());
                tblElclaim.setClientattendedhospital(updateElRequest.getClientattendedhospital());
                tblElclaim.setClientinjuries(updateElRequest.getClientinjuries());
                tblElclaim.setClientpresentcond(updateElRequest.getClientpresentcond());
                tblElclaim.setClientseenaccbook(updateElRequest.getClientseenaccbook());
                tblElclaim.setClientseengp(updateElRequest.getClientseengp());
                tblElclaim.setClientstillwork(updateElRequest.getClientstillwork());
                tblElclaim.setCompanysickpay(updateElRequest.getCompanysickpay());
                tblElclaim.setMobile(updateElRequest.getMobile());
                tblElclaim.setDailyactivities(updateElRequest.getDailyactivities());
                tblElclaim.setDategpattendance(updateElRequest.getDategpattendance());
                tblElclaim.setDatehospitalattendance(updateElRequest.getDatehospitalattendance());
                tblElclaim.setEmployeraddress(updateElRequest.getEmployeraddress());
                tblElclaim.setEmployercontactno(updateElRequest.getEmployercontactno());
                tblElclaim.setEmployername(updateElRequest.getEmployername());
                tblElclaim.setEmploymentduration(updateElRequest.getEmploymentduration());
                tblElclaim.setEstimatednetweeklywage(updateElRequest.getEstimatednetweeklywage());
                tblElclaim.setGpaddress(updateElRequest.getGpaddress());
                tblElclaim.setGpadvisegiven(updateElRequest.getGpadvisegiven());
                tblElclaim.setGpname(updateElRequest.getGpname());
                tblElclaim.setHospitaladdress(updateElRequest.getHospitaladdress());
                tblElclaim.setHospitaladvisegiven(updateElRequest.getHospitaladvisegiven());
                tblElclaim.setHospitalname(updateElRequest.getHospitalname());
                tblElclaim.setInjuriessymptomsstart(updateElRequest.getInjuriessymptomsstart());
                tblElclaim.setJobtitle(updateElRequest.getJobtitle());
                tblElclaim.setLossofearnings(updateElRequest.getLossofearnings());
                tblElclaim.setNinumber(updateElRequest.getNinumber());
                tblElclaim.setNoworkadvised(updateElRequest.getNoworkadvised());
                tblElclaim.setOtherlossesdetail(updateElRequest.getOtherlossesdetail());
                tblElclaim.setPeriodofabsence(updateElRequest.getPeriodofabsence());
                tblElclaim.setPremedicalconddetail(updateElRequest.getPremedicalconddetail());
                tblElclaim.setPreviousaccdetail(updateElRequest.getPreviousaccdetail());
                tblElclaim.setProtectiveequipment(updateElRequest.getProtectiveequipment());
                tblElclaim.setReportedbywhom(updateElRequest.getReportedbywhom());
                tblElclaim.setTrainingreceived(updateElRequest.getTrainingreceived());
                tblElclaim.setUserofprocequip(updateElRequest.getUserofprocequip());
                tblElclaim.setWitnessdetails(updateElRequest.getWitnessdetails());
                tblElclaim.setDob(updateElRequest.getDob());
                tblElclaim.setRemarks(updateElRequest.getRemarks());
                tblElclaim.setUpdatedate(new Date());
                tblElclaim.setLastupdateuser(tblUser.getUsercode());
                tblElclaim.setAcctime(updateElRequest.getAcctime());
                tblElclaim.setWasclientwearing(updateElRequest.getWasclientwearing());
                tblElclaim.setEmail(updateElRequest.getEmail());

                tblElclaim = elService.updateElCase(tblElclaim);

                if (tblElclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblElclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addElNotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addElNotes(@RequestBody ElNoteRequest ElNoteRequest,
                                                              HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addElNotes(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addElNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = elService.isELCaseAllowed(tblUser.getCompanycode(), "9");

            if (allowed) {
                if (!ElNoteRequest.getElCode().isEmpty()) {
                    TblElnote tblElnote = new TblElnote();
                    TblElclaim tblElclaim = new TblElclaim();

                    tblElclaim.setElclaimcode(Long.valueOf(ElNoteRequest.getElCode()));
                    tblElnote.setTblElclaim(tblElclaim);
                    tblElnote.setNote(ElNoteRequest.getNote());
                    tblElnote.setUsercategorycode(ElNoteRequest.getUserCatCode());
                    tblElnote.setCreatedon(new Date());
                    tblElnote.setUsercode(tblUser.getUsercode());

                    tblElnote = elService.addTblElNote(tblElnote);

                    if (tblElnote != null && tblElnote.getElnotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addElNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblElnote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addElNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addElNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addElNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addElNotes();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addElNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/resendElEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> resendElEmail(@Valid @RequestBody ResendElMessageDto resendElMessageDto,
                                                                 HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == resendElEmail(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == resendElEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblEmail tblEmail = elService.resendEmail(resendElMessageDto.getElmessagecode());
            if (tblEmail != null && tblEmail.getEmailcode() > 0) {

                LOG.info("\n EXITING THIS METHOD == resendElEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Success", tblEmail);
            } else {
                LOG.info("\n EXITING THIS METHOD == resendElEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While resending Email", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == resendElEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == resendElEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/assigncasetosolicitorbyLA", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> assigncasetosolicitorbyLA(
            @RequestBody AssignElCasetoSolicitor assignElCasetoSolicitor, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblElclaim tblElclaim = elService.assignCaseToSolicitor(assignElCasetoSolicitor, tblUser);
                if (tblElclaim != null) {
                    tblElclaim = elService.findElCaseById(Long.valueOf(assignElCasetoSolicitor.getElClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblElclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }


            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnElFromDirectIntro", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnElFromDirectIntro(
            @RequestBody PerformHotKeyOnRtaRequest performHotKeyOnElRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnElFromDirectIntro(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnElFromDirectIntro(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblElclaim tblElclaim = elService.performActionOnElFromDirectIntro(performHotKeyOnElRequest,
                        tblUser);
                if (tblElclaim != null) {

                    LOG.info("\n EXITING THIS METHOD == performActionOnElFromDirectIntro(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblElclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnElFromDirectIntro(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnElFromDirectIntro(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnElFromDirectIntro();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnElFromDirectIntro(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnEl", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnEl(
            @RequestBody PerformActionOnElRequest performActionOnElRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnEl(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblElclaim tblElclaim = elService.findElCaseByIdWithoutUser(Long.parseLong(performActionOnElRequest.getElcode()));


            if (performActionOnElRequest.getToStatus().equals("565")) {
                List<TblEltask> tblELtask = elService
                        .getELTaskAgainstELCodeAndStatus(performActionOnElRequest.getElcode(), "N");
                List<TblEltask> tblELtask1 = elService
                        .getELTaskAgainstELCodeAndStatus(performActionOnElRequest.getElcode(), "P");

                if (tblELtask != null && tblELtask.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Some Task Are Not Performed", tblElclaim);
                } else if (tblELtask1 != null && tblELtask1.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Some Task Are In Pending State", tblElclaim);
                } else {

                }
            }


            if (tblUser != null) {
                if ((performActionOnElRequest.getToStatus().equals("569")) ||
                        (performActionOnElRequest.getToStatus().equals("610")) ||
                        (performActionOnElRequest.getToStatus().equals("611")) ||
                        (performActionOnElRequest.getToStatus().equals("609"))) {
                    // reject status
                    tblElclaim = elService.performRejectCancelActionOnEl(performActionOnElRequest.getElcode(),
                            performActionOnElRequest.getToStatus(), performActionOnElRequest.getReason(), tblUser);

                } else if (performActionOnElRequest.getToStatus().equals("572")) {
                    //cancel status
                    tblElclaim = elService.performRejectCancelActionOnEl(performActionOnElRequest.getElcode(),
                            performActionOnElRequest.getToStatus(), performActionOnElRequest.getReason(), tblUser);

                } else if (performActionOnElRequest.getToStatus().equals("582")) {
                    // revert status
                    tblElclaim = elService.performRevertActionOnEL(performActionOnElRequest.getElcode(),
                            performActionOnElRequest.getToStatus(), performActionOnElRequest.getReason(), tblUser);

                } else {
                    tblElclaim = elService.performActionOnEl(performActionOnElRequest.getElcode(),
                            performActionOnElRequest.getToStatus(), tblUser);
                }

                if (tblElclaim != null) {
                    TblElclaim Elclaim = elService
                            .findElCaseById(Long.valueOf(performActionOnElRequest.getElcode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", Elclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnEl();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addESign(); ");
        try {

            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("code")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }

            TblElclaim tblElclaim = elService.findElCaseByIdWithoutUser(addESign.getRtaCode());
            if (tblElclaim != null && tblElclaim.getElclaimcode() > 0) {

                if (tblElclaim.getEsig().equals("Y")) {
                    List<TblEldocument> tblEldocuments = elService.getAuthElCasedocuments(tblElclaim.getElclaimcode());
                    for (TblEldocument tblEldocument : tblEldocuments) {
                        if (tblEldocument.getDoctype().equalsIgnoreCase("Esign")) {
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("signedDocument", tblEldocument.getDocbase64());
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.OK, "You Have Already Signed The Document",
                                    map);
                        }
                    }

                }
                // solicitor company
                TblCompanyprofile tblCompanyprofile = elService
                        .getCompanyProfileAgainstElCode(tblElclaim.getElclaimcode());
                if (tblCompanyprofile != null) {

                    TblCompanydoc tblCompanydoc = elService.getCompanyDocs(tblCompanyprofile.getCompanycode());
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                    byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                    ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                    BufferedImage image1 = ImageIO.read(bis);
                    bis.close();


                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();

                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            while (keys.hasNext()) {
                                String key = keys.next();
                                if (!key.equals("code") && !key.equals("eSign")) {
                                    if (fieldName.equals(key)) {
                                        textField.setValue(jsonObject.getString(key));
                                    }
                                }
                            }

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = elService.getElDbColumnValue(fieldName.replace("DB_", ""),
                                        tblElclaim.getElclaimcode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            }
                            if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    for (int a = 0; a < document.getNumberOfPages(); a++) {
                        PDPage p = document.getPage(a);
                        PDResources resources = p.getResources();
                        for (COSName xObjectName : resources.getXObjectNames()) {
                            PDXObject xObject = resources.getXObject(xObjectName);
                            if (xObject instanceof PDImageXObject) {
                                PDImageXObject original_img = ((PDImageXObject) xObject);
                                PDImageXObject pdImageXObject = LosslessFactory.createFromImage(document, image1);
                                resources.put(xObjectName, pdImageXObject);
                            }
                        }
                    }

                    LOG.info("\n Esign Document Prepared SuccessFully\n");
                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\El\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\El\\";

                    File theDir = new File(UPLOADED_FOLDER + tblElclaim.getElclaimcode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblElclaim.getElcode() + "_Signed.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblElclaim.getElcode() + "_Signed.pdf");
                    }

                    tblElclaim.setEsig("Y");
                    tblElclaim.setEsigdate(new Date());
                    tblElclaim = elService.updateElCase(tblElclaim);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    LOG.info("\n Esign Saved on folder SuccessFully SuccessFully\n");
                    File file = new File(theDir.getPath() + "\\" + tblElclaim.getElcode() + "_Signed.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    TblTask tblEltaskdocument = new TblTask();
                    tblEltaskdocument.setTaskcode(20);

                    TblEldocument tblEldocument = new TblEldocument();
                    tblEldocument.setTblElclaim(tblElclaim);
                    tblEldocument.setDocbase64(encoded);
                    tblEldocument.setDocname("Esign");
                    tblEldocument.setDoctype("Esign");
                    tblEldocument.setCreateddate(new Date());
                    tblEldocument.setCreateuser("1");
                    tblEldocument.setDocurl(
                            UPLOADED_SERVER + tblElclaim.getElclaimcode() + "\\" + tblElclaim.getElcode() + "_Signed.pdf");
                    tblEldocument.setTblTask(tblEltaskdocument);

                    tblEldocument = elService.addElSingleDocumentSingle(tblEldocument);
                    LOG.info("\n saving doc in database\n");
                    if (tblEldocument != null && tblEldocument.getEldocumentscode() > 0) {
//                    Locale.setDefault(backup);

                        TblEltask tblEltask = new TblEltask();
                        tblEltask.setEltaskcode(20);
                        tblEltask.setTblElclaim(tblElclaim);
                        tblEltask.setStatus("C");
                        tblEltask.setRemarks("Completed");
                        tblEltask.setCompletedon(new Date());

                        int tblRtatask1 = elService.updateTblElTask(tblEltask);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("doc", encoded);
                        TblUser legalUser = elService.findByUserId("2");

                        // cleint Email
                        TblEmailTemplate tblEmailTemplate = elService.findByEmailType("esign-clnt-complete");
                        String clientbody = tblEmailTemplate.getEmailtemplate();

                        clientbody = clientbody.replace("[LEGAL_CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                                tblElclaim.getFirstname() + " "
////                                        + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
////                                        + tblElclaim.getLastname());
                        clientbody = clientbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());

                        elService.saveEmail(tblElclaim.getEmail(), clientbody, "No Win No Fee Agreement ", tblElclaim, legalUser, file.getPath());
                        LOG.info("\n Email Sent To client\n");
                        // Introducer Email
                        tblEmailTemplate = elService.findByEmailType("esign-Int-sol-complete");
                        String Introducerbody = tblEmailTemplate.getEmailtemplate();
                        TblUser introducerUser = elService.findByUserId(String.valueOf(tblElclaim.getAdvisor()));

                        Introducerbody = Introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
                        Introducerbody = Introducerbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
                        Introducerbody = Introducerbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                                tblElclaim.getFirstname() + " "
//                                        + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                                        + tblElclaim.getLastname());
                        Introducerbody = Introducerbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        Introducerbody = Introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblElclaim.getElclaimcode());

                        elService.saveEmail(introducerUser.getUsername(), Introducerbody, "Esign", tblElclaim, introducerUser);
                        LOG.info("\n Email Sent to intro\n");
                        // PI department Email
                        String LegalPideptbody = tblEmailTemplate.getEmailtemplate();

                        LegalPideptbody = LegalPideptbody.replace("[USER_NAME]", legalUser.getLoginid());
                        LegalPideptbody = LegalPideptbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
                        LegalPideptbody = LegalPideptbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        LegalPideptbody = LegalPideptbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                                tblElclaim.getFirstname() + " "
//                                        + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                                        + tblElclaim.getLastname());
                        LegalPideptbody = LegalPideptbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblElclaim.getElclaimcode());

                        elService.saveEmail(legalUser.getUsername(), LegalPideptbody, "Esign", tblElclaim, legalUser);


                        saveEsignSubmitStatus(String.valueOf(tblElclaim.getElclaimcode()), "9", request);

                        LOG.info("\n Email Sent to Dept\n");
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "You Have SuccessFully Signed The CFA, A Copy Of That CFA Has Been Sent To The Provided Email.", map);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST,
                                "Error While Saving The Document..Esign Compeleted", null);
                    }

                } else {
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No document template found against provided RTA",
                            null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }


        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addESign(); ");
        try {

            TblElclaim tblElclaim = elService.findElCaseByIdWithoutUser(Long.valueOf(eSignFieldsRequest.getCode()));

            if (tblElclaim != null && tblElclaim.getElclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = elService
                        .getCompanyProfileAgainstElCode(tblElclaim.getElclaimcode());
                if (!tblElclaim.getEsig().equals("Y")) {

                    TblCompanydoc tblCompanydoc = elService.getCompanyDocs(tblCompanyprofile.getCompanycode());
                    if (tblCompanydoc == null) {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "No CFA document found", null);
                    }
                    HashMap<String, String> fields = new HashMap<>();
                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();


                    /////////////////////// FILL the CFA With Client DATA ////////////////////////


                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = elService.getElDbColumnValue(fieldName.replace("DB_", ""),
                                        tblElclaim.getElclaimcode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            } else if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            } else {
                                fields.put(fieldName, fieldName);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    /////////////////////// END OF FILL the CFA With Client DATA /////////////////

                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\El\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\El\\";

                    File theDir = new File(UPLOADED_FOLDER + tblElclaim.getElclaimcode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblElclaim.getElcode() + "_view.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblElclaim.getElcode() + "_view.pdf");
                    }

                    File file = new File(theDir.getPath() + "\\" + tblElclaim.getElcode() + "_view.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    fields.put("doc", encoded);
                    fields.put("isEsign", "N");

                    saveEsignOpenStatus(String.valueOf(tblElclaim.getElclaimcode()), "9", request);

                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", fields);
                } else {

                    List<TblEldocument> tblEldocuments = elService.getAuthElCasedocuments(tblElclaim.getElclaimcode());
                    if (tblEldocuments != null && tblEldocuments.size() > 0) {
                        for (TblEldocument tblEldocument : tblEldocuments) {
                            if (tblEldocument.getTblTask() != null && tblEldocument.getTblTask().getTaskcode() == 20) {
                                HashMap<String, String> fields = new HashMap<>();
                                fields.put("doc", tblEldocument.getDocbase64());
                                fields.put("isEsign", "Y");
                                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                                return getResponseFormat(HttpStatus.OK, "You have Already Signed The Document.", fields);
                            }
                        }
                    }

                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Signed Document Found Please Contact Our HelpLine : please call us at 01615374448", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No EL document found", null);
            }


        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("elClaimCode") long elClaimCode,
                                                               @RequestParam("taskCode") long taskCode,
                                                               @RequestParam(value = "multipartFiles", required = false) List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblElclaim tblElclaim = elService.findElCaseByIdWithoutUser(elClaimCode);
            TblElsolicitor tblElsolicitor = elService.getELSolicitorsOfElClaim(elClaimCode);
            TblCompanyprofile solicitorCompany = elService.getCompanyProfile(tblElsolicitor.getCompanycode());
            if (tblElclaim != null && tblElclaim.getElclaimcode() > 0) {
                TblTask tblTask = elService.getTaskAgainstCode(taskCode);
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblEltask tblEltask = new TblEltask();
                    tblEltask.setEltaskcode(taskCode);
                    tblEltask.setTblElclaim(tblElclaim);
                    if (tblTask.getTaskcode() == 20) {
                        TblCompanydoc tblCompanydoc = elService.getCompanyDocs(solicitorCompany.getCompanycode());
                        if (tblCompanydoc == null) {
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "There is No CFA Attach To the Solicitor", null);
                        }
                        String emailUrl = getDataFromProperties("esign.baseurl") + "El=" + tblElclaim.getElclaimcode();
                        TblEmailTemplate tblEmailTemplate = getEmailTemplateByType("esign");

                        String body = tblEmailTemplate.getEmailtemplate();
                        body = body.replace("[LEGAL_CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
                        body = body.replace("[SOLICITOR_COMPANY_NAME]", solicitorCompany.getName());
                        body = body.replace("[ESIGN_URL]", emailUrl);
                        if (tblElclaim.getEmail() != null) {
//                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
//                            return getResponseFormat(HttpStatus.BAD_REQUEST, "No Client Email Address Found", null);
                            LOG.info("\n Email Sendpos");
                            elService.saveEmail(tblElclaim.getEmail(), body, "No Win No Fee Agreement", tblElclaim, tblUser);
                        }
                        LOG.info("\n Sending SMS ");
                        if (tblElclaim.getMobile() != null && !tblElclaim.getMobile().isEmpty()) {
                            LOG.info("\n SMS SEND");
                            TblEmailTemplate tblEmailTemplateSMS = getEmailTemplateByType("esign-SMS");
                            String message = tblEmailTemplateSMS.getEmailtemplate();
                            message = message.replace("[LEGAL_CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
                            message = message.replace("[SOLICITOR_COMPANY_NAME]", solicitorCompany.getName());
                            message = message.replace("[ESIGN_URL]", emailUrl);


                            sendSMS(tblElclaim.getMobile(), message);
                            tblEltask.setRemarks("Esign/SMS Email Sent Successfully to Client");
                        } else {
                            tblEltask.setRemarks("Esign Email Sent Successfully to Client");
                        }

                        saveEmailSmsEsignStatus(String.valueOf(tblElclaim.getElclaimcode()), "9", tblUser.getUsercode());

                        tblEltask.setStatus("P");
                    } else {

                        String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\El\\";
                        String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\El\\";
                        List<TblEldocument> tblEldocuments = new ArrayList<TblEldocument>();

                        File theDir = new File(UPLOADED_FOLDER + tblElclaim.getElclaimcode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }

                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);
                            TblEldocument tblEldocument = new TblEldocument();
                            tblEldocument.setDocurl(UPLOADED_SERVER + tblElclaim.getElclaimcode() + "\\"
                                    + multipartFile.getOriginalFilename());
                            tblEldocument.setDocumentPath(tblEldocument.getDocurl());
                            tblEldocument.setDoctype(multipartFile.getContentType());
                            tblEldocument.setTblElclaim(tblElclaim);
                            tblEldocument.setTblTask(tblTask);
                            tblEldocument.setCreateuser(tblUser.getUsercode());
                            tblEldocument.setCreateddate(new Date());

                            tblEldocuments.add(tblEldocument);
                        }
                        elService.addElDocument(tblEldocuments);
                        tblEltask.setStatus("C");
                        tblEltask.setRemarks("Completed");

                    }
                    tblEltask.setCompletedon(new Date());
                    tblEltask.setCompletedby(tblUser.getUsercode());
                    int tblRtatask1 = elService.updateTblElTask(tblEltask);
                    if (tblRtatask1 > 0) {

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblRtatask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }


        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addElDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> addElDocument(@RequestParam("elClaimCode") String elClaimCode,
                                                                 @RequestParam("multipartFiles") List<MultipartFile> multipartFiles, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElPostApi \n METHOD == addElDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addElDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblElclaim tblElclaim = elService.findElCaseByIdWithoutUser(Long.valueOf(elClaimCode));
            if (tblElclaim != null && tblElclaim.getElclaimcode() > 0) {
//                String UPLOADED_FOLDER = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0_Tomcat9-la\\webapps\\El\\";
//                String UPLOADED_SERVER = "http:\\115.186.147.30:8080\\El\\";
                String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\El\\";
                String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\El\\";
                List<TblEldocument> tblEldocuments = new ArrayList<TblEldocument>();

                File theDir = new File(UPLOADED_FOLDER + tblElclaim.getElclaimcode());
                if (!theDir.exists()) {
                    theDir.mkdirs();
                }

                for (MultipartFile multipartFile : multipartFiles) {
                    byte[] bytes = multipartFile.getBytes();
                    Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                    Files.write(path, bytes);
                    TblEldocument tblEldocument = new TblEldocument();
                    tblEldocument.setDocurl(
                            UPLOADED_SERVER + tblElclaim.getElclaimcode() + "\\" + multipartFile.getOriginalFilename());
                    tblEldocument.setDocumentPath(tblEldocument.getDocurl());
                    tblEldocument.setDoctype(multipartFile.getContentType());
                    tblEldocument.setTblElclaim(tblElclaim);
                    tblEldocument.setCreateuser(tblUser.getUsercode());
                    tblEldocument.setCreateddate(new Date());

                    tblEldocuments.add(tblEldocument);
                }

                tblEldocuments = elService.addElDocument(tblEldocuments);

                // Legal internal Email
                TblEmailTemplate tblEmailTemplate = elService.findByEmailType("document-add");
                String legalbody = tblEmailTemplate.getEmailtemplate();
                TblUser legalUser = elService.findByUserId("2");

                legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
                legalbody = legalbody.replace("[CASE_NUMBER]", tblElclaim.getElcode());
                legalbody = legalbody.replace("[CLIENT_NAME]", tblElclaim.getFirstname() +(tblElclaim.getMiddlename() != null?tblElclaim.getMiddlename():"") +" "+tblElclaim.getLastname());
//                        tblElclaim.getFirstname() + " "
//                                + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                                + tblElclaim.getLastname());
                legalbody = legalbody.replace("[COUNT]", String.valueOf(tblEldocuments.size()));
                legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.El") + tblElclaim.getElclaimcode());

                elService.saveEmail(legalUser.getUsername(), legalbody,
                        tblElclaim.getElcode() + " | Processing Note", tblElclaim, legalUser);

                LOG.info("\n EXITING THIS METHOD == addElDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Document Saved Successfully", null);
            } else {
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Find", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == ElPostApi \n METHOD == addElDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addElDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/setCurrentTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> setCurrentTask(@RequestBody CurrentTaskRequest currentTaskRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == setCurrentTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            int tblRtatask1 = elService.updateCurrentTask(currentTaskRequest.getTaskCode(),
                    currentTaskRequest.getElCode(), currentTaskRequest.getCurrent());
            if (tblRtatask1 > 0) {
                List<TblEltask> tblEltaskList = elService
                        .getAuthElCaseTasks(String.valueOf(currentTaskRequest.getElCode()));

                if (tblEltaskList != null && tblEltaskList.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Current Task Updated SuccessFully", tblEltaskList);
                } else {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Updating Task", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == setCurrentTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/deleteElDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> deleteRtaDocument(
            @RequestBody DeleteRtaDocumentRequest deleteRtaDocumentRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == deleteRtaDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = elService.getCompanyProfile(tblUser.getCompanycode());
            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {

                elService.deleteElDocument(deleteRtaDocumentRequest.getDoccode());
                return getResponseFormat(HttpStatus.OK, "Document Deleted", null);
            } else {
                LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Action", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaPostApi \n METHOD == deleteRtaDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
