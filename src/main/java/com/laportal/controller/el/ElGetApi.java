package com.laportal.controller.el;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.ElCaseList;
import com.laportal.dto.ElNotesResponse;
import com.laportal.dto.ElStatusCountList;
import com.laportal.dto.RtaAuditLogResponse;
import com.laportal.model.*;
import com.laportal.service.el.ElService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/El")
public class ElGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(ElGetApi.class);

    @Autowired
    private ElService elService;

    @RequestMapping(value = "/getElCases", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getElCases(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getElCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = elService.isELCaseAllowed(tblUser.getCompanycode(), "9");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            TblCompanyprofile tblCompanyprofile = elService.getCompanyProfile(tblUser.getCompanycode());
            List<ElCaseList> elCaseLists = elService.getAuthElCasesUserAndCategoryWise(tblUser.getUsercode(), tblCompanyprofile.getTblUsercategory().getCategorycode());

            if (elCaseLists != null && elCaseLists.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", elCaseLists);
            } else {
                LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == ElGetApi \n METHOD == getElCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getElCaseById/{ElCaseId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrCaseById(@PathVariable long ElCaseId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getElCaseById(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getElCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = elService.isELCaseAllowed(tblUser.getCompanycode(), "9");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            TblElclaim tblElclaim = elService.findElCaseByIdForView(ElCaseId, tblUser);
            if (tblElclaim != null) {

                LOG.info("\n EXITING THIS METHOD == getElCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblElclaim);
            } else {
                LOG.info("\n EXITING THIS METHOD == getElCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Error While Fetching Case Info. Some Info is missing", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == ElGetApi \n METHOD == getElCaseById();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getElCaseById(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllElStatusCounts", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllElStatusCounts(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getAllElStatusCounts(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAllElStatusCounts(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = elService.isELCaseAllowed(tblUser.getCompanycode(), "9");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            TblCompanyprofile tblCompanyprofile = elService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<ElStatusCountList> elStatusCountLists = elService.getAllElStatusCountsForIntroducers(tblCompanyprofile.getCompanycode());
                if (elStatusCountLists != null) {
                    LOG.info("\n EXITING THIS METHOD == getAllElStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", elStatusCountLists);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAllElStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Cases Found Against User", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<ElStatusCountList> elStatusCountLists = elService.getAllElStatusCountsForSolicitor(tblCompanyprofile.getCompanycode());

                if (elStatusCountLists != null) {
                    LOG.info("\n EXITING THIS METHOD == getAllElStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", elStatusCountLists);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAllElStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Cases Found Against User", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<ElStatusCountList> elStatusCountLists = elService.getAllElStatusCounts();

                if (elStatusCountLists != null) {
                    LOG.info("\n EXITING THIS METHOD == getAllElStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", elStatusCountLists);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAllElStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Cases Found Against User", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAllElStatusCounts(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == ElGetApi \n METHOD == getAllElStatusCounts();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllElStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getElCasesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getElCasesByStatus(@PathVariable long statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getElCasesByStatus(); ");


            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getElCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = elService.getCompanyProfile(tblUser.getCompanycode());
            boolean allowed = elService.isELCaseAllowed(tblUser.getCompanycode(), "9");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }


            List<ElCaseList> elCaseLists = elService.getAuthElCasesUserAndCategoryWiseAndStatusWise(tblUser.getUsercode(), String.valueOf(statusId), tblCompanyprofile.getTblUsercategory().getCategorycode());
            if (elCaseLists != null && elCaseLists.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", elCaseLists);
            } else {
                LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }


        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == ElGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAuthElCaseLogs/{Elcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getElCaseLogs(@PathVariable String Elcode,
                                                                 HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getElCaseLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getElCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblEllog> ElCaseLogs = elService.getElCaseLogs(Elcode);

                if (ElCaseLogs != null && ElCaseLogs.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getElCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", ElCaseLogs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getElCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getElCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == ElGetApi \n METHOD == getElCaseLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getElCaseLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getElCaseNotes/{Elcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getElCaseNotes(@PathVariable String Elcode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getAuthRtaCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = elService.isELCaseAllowed(tblUser.getCompanycode(), "9");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            if (tblUser != null) {
                List<TblElnote> tblElnotes = elService.getElCaseNotes(Elcode, tblUser);
                List<TblElnote> tblElnotesLegalInternal = elService.getAuthElCaseNotesOfLegalInternal(Elcode, tblUser);

                if (tblElnotes != null && tblElnotes.size() > 0) {
                    ElNotesResponse elNotesResponse = new ElNotesResponse();
                    elNotesResponse.setTblElnotes(tblElnotes);
                    elNotesResponse.setTblElnotesLegalOnly(tblElnotesLegalInternal);

                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", elNotesResponse);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == ElGetApi \n METHOD == getElCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getElCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getElCaseMessages/{Elcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getElCaseMessages(@PathVariable String Elcode,
                                                                     HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getElCaseMessages(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblElmessage> tblElmessages = elService.getElCaseMessages(Elcode);

                if (tblElmessages != null && tblElmessages.size() > 0) {
                    for (TblElmessage tblElmessage : tblElmessages) {
                        tblElmessage.setTblElclaim(null);
                    }

                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblElmessages);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == ElGetApi \n METHOD == getHdrCaseMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getElCaseTasks/{elcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getElCaseTasks(@PathVariable String elcode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getElCaseTasks(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getElCaseTasks(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblEltask> tblEltasks = elService.getAuthElCaseTasks(elcode);

                if (tblEltasks != null && tblEltasks.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getElCaseTasks(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblEltasks);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getElCaseTasks(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getElCaseTasks(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == ElGetApi \n METHOD == getElCaseTasks();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getElCaseTasks(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getElCasedocuments/{elCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getElCasedocuments(@PathVariable String elCode,
                                                                      HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getElCasedocuments(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getElCasedocuments(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = elService.isELCaseAllowed(tblUser.getCompanycode(), "9");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getElCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            if (tblUser != null) {
                List<TblEldocument> tblEldocuments = elService.getAuthElCasedocuments(Long.parseLong(elCode));

                if (tblEldocuments != null) {
                    LOG.info("\n EXITING THIS METHOD == getElCasedocuments(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblEldocuments);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getElCasedocuments(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getElCasedocuments(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == ElGetApi \n METHOD == getElCasedocuments();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getElCasedocuments(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }


    @RequestMapping(value = "/getElAuditLogs/{elCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getElAuditLogs(@PathVariable String elCode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getElCasedocuments(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getElCasedocuments(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<RtaAuditLogResponse> elAuditLogResponses = elService.getElAuditLogs(Long.valueOf(elCode));

                if (elAuditLogResponses != null) {
                    LOG.info("\n EXITING THIS METHOD == getElCasedocuments(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", elAuditLogResponses);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getElCasedocuments(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getElCasedocuments(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == ElGetApi \n METHOD == getElCasedocuments();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getElCasedocuments(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

}
