package com.laportal.controller.db;


import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.db.DbService;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.exporting.PdfImageInfo;
import com.spire.pdf.fields.PdfField;
import com.spire.pdf.graphics.PdfImage;
import com.spire.pdf.widget.PdfFormFieldWidgetCollection;
import com.spire.pdf.widget.PdfFormWidget;
import com.spire.pdf.widget.PdfPageCollection;
import com.spire.pdf.widget.PdfTextBoxFieldWidget;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.geom.Dimension2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

@RestController
@RequestMapping("/db")
public class DbPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(DbPostApi.class);

    @Autowired
    private DbService dbService;

    @RequestMapping(value = "/addNewDbCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewHdrCase(@RequestBody SaveDbRequest saveDbRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addNewHdrCase(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = dbService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                TblDbclaim tblDbclaim = new TblDbclaim();

                tblDbclaim.setAnyFfmSufferedDb(saveDbRequest.getAnyFfmSufferedDb());
                tblDbclaim.setClaimantAddress(saveDbRequest.getClaimantAddress());
                tblDbclaim.setClaimantContactno(saveDbRequest.getClaimantContactno());
                tblDbclaim.setClaimantDob(saveDbRequest.getClaimantDob());
                tblDbclaim.setClaimantEmail(saveDbRequest.getClaimantEmail());
                tblDbclaim.setClaimantName(saveDbRequest.getClaimantName());
                tblDbclaim.setComplaintDefendent(saveDbRequest.getComplaintDefendent());
                tblDbclaim.setConsentPiShared(saveDbRequest.getConsentPiShared());
                tblDbclaim.setDateOfBreach(saveDbRequest.getDateOfBreach());
                tblDbclaim.setDateOfKnowledge(saveDbRequest.getDateOfKnowledge());
                tblDbclaim.setDbByCompBusns(saveDbRequest.getDbByCompBusns());
                tblDbclaim.setDbPreexistingCond(saveDbRequest.getDbPreexistingCond());
                tblDbclaim.setDbRectified(saveDbRequest.getDbRectified());
                tblDbclaim.setDefendentAddress(saveDbRequest.getDefendentAddress());
                tblDbclaim.setDefendentContactno(saveDbRequest.getDefendentContactno());
                tblDbclaim.setDefendentEmail(saveDbRequest.getDefendentEmail());
                tblDbclaim.setDefendentHandleDb(saveDbRequest.getDefendentHandleDb());
                tblDbclaim.setDefendentName(saveDbRequest.getDefendentName());
                tblDbclaim.setDefendentReqConsent(saveDbRequest.getDefendentReqConsent());
                tblDbclaim.setEvidenceOfDb(saveDbRequest.getEvidenceOfDb());
                tblDbclaim.setFrstFoundAbtDb(saveDbRequest.getFrstFoundAbtDb());
                tblDbclaim.setHasDbAffectedYou(saveDbRequest.getHasDbAffectedYou());
                tblDbclaim.setHowFeelYourDb(saveDbRequest.getHowFeelYourDb());
                tblDbclaim.setHowKnowAbtDb(saveDbRequest.getHowKnowAbtDb());
                tblDbclaim.setHowKnowDefendent(saveDbRequest.getHowKnowDefendent());
                tblDbclaim.setInstructedPrvntDb(saveDbRequest.getInstructedPrvntDb());
                tblDbclaim.setLitigationFriend(saveDbRequest.getLitigationFriend());
                tblDbclaim.setOtherprblmWithDefendent(saveDbRequest.getOtherprblmWithDefendent());
                tblDbclaim.setPiDisclosedInDb(saveDbRequest.getPiDisclosedInDb());
                tblDbclaim.setPiShared(saveDbRequest.getPiShared());
                tblDbclaim.setRecurringDbOfPi(saveDbRequest.getRecurringDbOfPi());
                tblDbclaim.setRemarks(saveDbRequest.getRemarks());
                tblDbclaim.setSoughtMedicalAvdnce(saveDbRequest.getSoughtMedicalAvdnce());
                tblDbclaim.setStatus(new BigDecimal(39));
                tblDbclaim.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                tblDbclaim.setCreatedate(new Date());

                TblCompanyprofile tblCompanyprofile = dbService.getCompanyProfile(tblUser.getCompanycode());
                tblDbclaim.setDbcode(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());

                tblDbclaim = dbService.saveDbRequest(tblDbclaim);

                if (tblDbclaim != null) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = dbService.saveCompanyProfile(tblCompanyprofile);

                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblDbclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/updateDbCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateDbCase(@RequestBody UpdateDbRequest updateDbRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == updateHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = dbService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                TblDbclaim tblDbclaim = dbService.findDbCaseByIdWithoutUser(updateDbRequest.getDbclaimcode());

                tblDbclaim.setDbcode(updateDbRequest.getDbcode());
                tblDbclaim.setAnyFfmSufferedDb(updateDbRequest.getAnyFfmSufferedDb());
                tblDbclaim.setClaimantAddress(updateDbRequest.getClaimantAddress());
                tblDbclaim.setClaimantContactno(updateDbRequest.getClaimantContactno());
                tblDbclaim.setClaimantDob(updateDbRequest.getClaimantDob());
                tblDbclaim.setClaimantEmail(updateDbRequest.getClaimantEmail());
                tblDbclaim.setClaimantName(updateDbRequest.getClaimantName());
                tblDbclaim.setComplaintDefendent(updateDbRequest.getComplaintDefendent());
                tblDbclaim.setConsentPiShared(updateDbRequest.getConsentPiShared());
                tblDbclaim.setDateOfBreach(updateDbRequest.getDateOfBreach());
                tblDbclaim.setDateOfKnowledge(updateDbRequest.getDateOfKnowledge());
                tblDbclaim.setDbByCompBusns(updateDbRequest.getDbByCompBusns());
                tblDbclaim.setDbPreexistingCond(updateDbRequest.getDbPreexistingCond());
                tblDbclaim.setDbRectified(updateDbRequest.getDbRectified());
                tblDbclaim.setDefendentAddress(updateDbRequest.getDefendentAddress());
                tblDbclaim.setDefendentContactno(updateDbRequest.getDefendentContactno());
                tblDbclaim.setDefendentEmail(updateDbRequest.getDefendentEmail());
                tblDbclaim.setDefendentHandleDb(updateDbRequest.getDefendentHandleDb());
                tblDbclaim.setDefendentName(updateDbRequest.getDefendentName());
                tblDbclaim.setDefendentReqConsent(updateDbRequest.getDefendentReqConsent());
                tblDbclaim.setEvidenceOfDb(updateDbRequest.getEvidenceOfDb());
                tblDbclaim.setFrstFoundAbtDb(updateDbRequest.getFrstFoundAbtDb());
                tblDbclaim.setHasDbAffectedYou(updateDbRequest.getHasDbAffectedYou());
                tblDbclaim.setHowFeelYourDb(updateDbRequest.getHowFeelYourDb());
                tblDbclaim.setHowKnowAbtDb(updateDbRequest.getHowKnowAbtDb());
                tblDbclaim.setHowKnowDefendent(updateDbRequest.getHowKnowDefendent());
                tblDbclaim.setInstructedPrvntDb(updateDbRequest.getInstructedPrvntDb());
                tblDbclaim.setLitigationFriend(updateDbRequest.getLitigationFriend());
                tblDbclaim.setOtherprblmWithDefendent(updateDbRequest.getOtherprblmWithDefendent());
                tblDbclaim.setPiDisclosedInDb(updateDbRequest.getPiDisclosedInDb());
                tblDbclaim.setPiShared(updateDbRequest.getPiShared());
                tblDbclaim.setRecurringDbOfPi(updateDbRequest.getRecurringDbOfPi());
                tblDbclaim.setRemarks(updateDbRequest.getRemarks());
                tblDbclaim.setSoughtMedicalAvdnce(updateDbRequest.getSoughtMedicalAvdnce());
                tblDbclaim.setUpdatedate(new Date());

                tblDbclaim = dbService.saveDbRequest(tblDbclaim);

                if (tblDbclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblDbclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addDbNotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addDbNotes(@RequestBody DbNoteRequest DbNoteRequest,
                                                              HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addDbNotes(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = dbService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                if (!DbNoteRequest.getDbCode().isEmpty()) {
                    TblDbnote tblDbnote = new TblDbnote();
                    TblDbclaim tblDbclaim = new TblDbclaim();

                    tblDbclaim.setDbclaimcode(Long.valueOf(DbNoteRequest.getDbCode()));
                    tblDbnote.setTblDbclaim(tblDbclaim);
                    tblDbnote.setNote(DbNoteRequest.getNote());
                    tblDbnote.setUsercategorycode(DbNoteRequest.getUserCatCode());
                    tblDbnote.setCreatedon(new Date());
                    tblDbnote.setUsercode(tblUser.getUsercode());

                    tblDbnote = dbService.addTblDbNote(tblDbnote);

                    if (tblDbnote != null && tblDbnote.getDbnotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblDbnote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addHdrNotes();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/resendDbEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> resendDbEmail(@RequestBody ResendDbMessageDto resendDbMessageDto,
                                                                 HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == resendRtaEmail(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblDbmessage tblDbmessage = dbService.getTblDbMessageById(resendDbMessageDto.getDbmessagecode());
            if (tblDbmessage != null && tblDbmessage.getDbmessagecode() > 0) {


                TblEmail tblEmail = new TblEmail();
                tblEmail.setSenflag(new BigDecimal(0));
                tblEmail.setEmailaddress(tblDbmessage.getSentto());
                tblEmail.setEmailbody(tblDbmessage.getMessage());
                tblEmail.setEmailsubject("Db LegalAssist");
                tblEmail.setCreatedon(new Date());


                tblEmail = dbService.saveTblEmail(tblEmail);
                if (tblEmail != null && tblEmail.getEmailcode() > 0) {

                    LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", tblEmail);
                } else {
                    LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While resending Email", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Email Found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == resendRtaEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/assigncasetosolicitorbyLA", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> assigncasetosolicitorbyLA(
            @RequestBody AssignDbCasetoSolicitor assignDbCasetoSolicitor, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblDbclaim tblDbclaim = dbService.assignCaseToSolicitor(assignDbCasetoSolicitor, tblUser);
                if (tblDbclaim != null) {
                    tblDbclaim = dbService.findDbCaseById(Long.valueOf(assignDbCasetoSolicitor.getDbClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblDbclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }


            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnDb", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnDb(
            @RequestBody PerformActionOnDbRequest performActionOnDbRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnHdr(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblDbclaim tblDbclaim = null;
                if (performActionOnDbRequest.getToStatus().equals("59")) {
                    // reject status
                    tblDbclaim = dbService.performRejectCancelActionOnDb(performActionOnDbRequest.getDbClaimCode(),
                            performActionOnDbRequest.getToStatus(), performActionOnDbRequest.getReason(), tblUser);

                } else if (performActionOnDbRequest.getToStatus().equals("60")) {
                    //cancel status
                    tblDbclaim = dbService.performRejectCancelActionOnDb(performActionOnDbRequest.getDbClaimCode(),
                            performActionOnDbRequest.getToStatus(), performActionOnDbRequest.getReason(), tblUser);

                } else {
                    tblDbclaim = dbService.performActionOnDb(performActionOnDbRequest.getDbClaimCode(),
                            performActionOnDbRequest.getToStatus(), tblUser);
                }

                if (tblDbclaim != null) {
                    TblDbclaim Dbclaim = dbService
                            .findDbCaseById(Long.valueOf(performActionOnDbRequest.getDbClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", Dbclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addESign(); ");
        try {
            Locale backup = Locale.getDefault();
            Locale.setDefault(Locale.ENGLISH);
            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("DbClaimCode")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }

            TblDbclaim tblDbclaim = dbService.findDbCaseByIdWithoutUser(addESign.getRtaCode());
            if (tblDbclaim != null && tblDbclaim.getDbclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = dbService.getCompanyProfileAgainstDbCode(tblDbclaim.getDbclaimcode());
                PdfDocument doc = new PdfDocument();

                String path = "";
                TblCompanydoc tblCompanydoc = new TblCompanydoc();
                tblCompanydoc = dbService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "E");

                doc.loadFromFile(tblCompanydoc.getPath());

                byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                BufferedImage image1 = ImageIO.read(bis);
                bis.close();

                PdfImage image = PdfImage.fromImage(image1);

                // Get the first worksheet
                PdfPageCollection page = doc.getPages();
                for (int j = 0; j < page.getCount(); j++) {
                    // Get the image information of the page
                    PdfImageInfo[] imageInfo = page.get(j).getImagesInfo();

                    // Loop through the image information
                    for (int i = 0; i < imageInfo.length; i++) {

                        // Get the bounds property of a specific image
                        Rectangle2D rect = imageInfo[i].getBounds();
                        // Get the x and y coordinates
                        System.out.println(String.format("The coordinate of image %d:（%f, %f）", i + 1, rect.getX(),
                                rect.getY()));

                        page.get(j).getCanvas().drawImage(image, rect.getX(), rect.getY(), rect.getWidth(),
                                rect.getHeight());
                    }
                }

                // get the form fields from the document
                PdfFormWidget form = (PdfFormWidget) doc.getForm();

                // get the form widget collection
                PdfFormFieldWidgetCollection formWidgetCollection = form.getFieldsWidget();

                // loop through the widget collection and fill each field with value
                for (int i = 0; i < formWidgetCollection.getCount(); i++) {
                    PdfField field = formWidgetCollection.get(i);
                    if (field instanceof PdfTextBoxFieldWidget) {
                        while (keys.hasNext()) {
                            String key = keys.next();
                            if (!key.equals("rtaCode") && !key.equals("eSign")) {
                                if (field.getName().equals(key)) {
                                    PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;
                                    textBoxField.setText(jsonObject.getString(key));
                                }
                            }
                        }

                        if (field.getName().startsWith("DB_")) {

                            PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;
                            String keyValue = dbService.getDbDbColumnValue(field.getName().replace("DB_", ""),
                                    tblDbclaim.getDbclaimcode());
                            if (keyValue.isEmpty()) {
                                Dimension2D dimension2D = new Dimension();
                                dimension2D.setSize(0, 0);
                                textBoxField.setSize(dimension2D);
                            } else {
                                textBoxField.setText(keyValue);
                            }


                        }
                        if (field.getName().startsWith("DATE")) {
                            PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;

                            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

                            Date date = new Date();
                            String s = formatter.format(date);
                            textBoxField.setText(s);
                        }
                        field.setFlatten(true);
                        field.setReadOnly(true);
                    }
                }
                String s = "C:\\Esigns\\" + tblDbclaim.getDbcode() + ".pdf";
                doc.saveToFile(s);
                File file = new File(s);
//                String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));
                String UPLOADED_FOLDER = "E://temp//";

                TblDbdocument tblDbdocument = new TblDbdocument();
                tblDbdocument.setTblDbclaim(tblDbclaim);
                tblDbdocument.setDocumentType("E-Sign");
                tblDbdocument.setDocumentPath(UPLOADED_FOLDER + tblDbclaim.getDbcode() + ".pdf");

                byte[] bytes = s.getBytes();
                Path path1 = Paths.get(UPLOADED_FOLDER + tblDbclaim.getDbcode() + ".pdf");
                Files.write(path1, bytes);

                tblDbdocument = dbService.saveTblDbDocument(tblDbdocument);
                if (tblDbdocument != null && tblDbdocument.getDbdocumentscode() > 0) {
                    Locale.setDefault(backup);
                    tblDbclaim.setEsig("Y");
                    tblDbclaim = dbService.saveDbRequest(tblDbclaim);
                    TblDbtask tblDbtask = dbService.getDbTaskByDbCodeAndTaskCode(tblDbclaim.getDbclaimcode(), 1);

                    tblDbtask.setStatus("C");
                    tblDbtask.setRemarks("Completed");

                    TblDbtask tblDbtask1 = dbService.updateTblDbTask(tblDbtask);
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", "Esign Completed");
                } else {
                    Locale.setDefault(backup);
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Saving The Document..Esign Compeleted",
                            null);
                }
            } else {
                Locale.setDefault(backup);
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addESign(); ");
        try {

            TblDbclaim tblDbclaim = dbService.findDbCaseByIdWithoutUser(Long.valueOf(eSignFieldsRequest.getCode()));
            if (tblDbclaim != null && tblDbclaim.getDbclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = dbService
                        .getCompanyProfileAgainstDbCode(tblDbclaim.getDbclaimcode());

                String path = "";
                TblCompanydoc tblCompanydoc = new TblCompanydoc();
                tblCompanydoc = dbService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "E");

                HashMap<String, String> fields = new HashMap<>();

                PdfDocument doc = new PdfDocument();
                doc.loadFromFile(tblCompanydoc.getPath());

                PdfFormWidget form = (PdfFormWidget) doc.getForm();
                PdfFormFieldWidgetCollection formWidgetCollection = form.getFieldsWidget();
                for (int i = 0; i < formWidgetCollection.getCount(); i++) {
                    PdfField field = formWidgetCollection.get(i);
                    if (!field.getName().startsWith("DB_") && !field.getName().equals("DATE")) {
                        fields.put(field.getName(), field.getName());
                    }
                }

                String viewDocPath = tblCompanydoc.getPath().replace("Esigns", "ViewEsigns");
                File file = new File(viewDocPath);
                String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                fields.put("doc", encoded);

                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Success", fields);
            } else {
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No HDR document found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("hdrClaimCode") long hdrClaimCode, @RequestParam("taskCode") long taskCode, @RequestParam("multipartFiles") List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblDbclaim tblDbclaim = dbService.findDbCaseById(hdrClaimCode, tblUser);
            if (tblDbclaim != null && tblDbclaim.getDbclaimcode() > 0) {
                TblTask tblTask = dbService.getTaskAgainstCode(taskCode);
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblDbtask tblDbtask = dbService.getDbTaskByDbCodeAndTaskCode(tblDbclaim.getDbclaimcode(), taskCode);

                    if (tblTask.getTaskcode() == 1) {
                        if (tblDbclaim.getClaimantEmail() == null) {
                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "No Client Email Address Found", null);
                        }
                        String emailUrl = "115.186.147.30:8888/esign?hdr=" + tblDbclaim.getDbclaimcode();

                        TblEmail tblEmail = new TblEmail();
                        tblEmail.setSenflag(new BigDecimal(0));
                        tblEmail.setEmailaddress(tblDbclaim.getClaimantEmail());
                        tblEmail.setEmailbody("Email sent to perform esign on mentioned hdr claim number: " + tblDbclaim.getDbcode()
                                + "\n Click on the below link to perform action \n" + emailUrl);
                        tblEmail.setEmailsubject("Db CASE" + tblDbclaim.getDbcode());
                        tblEmail.setCreatedon(new Date());

                        tblEmail = dbService.saveTblEmail(tblEmail);

                        tblDbtask.setStatus("P");
                        tblDbtask.setRemarks("Email Sent Successfully to Client");
                    } else {
                        String UPLOADED_FOLDER = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\";
                        String UPLOADED_SERVER = "http:\\115.186.147.30:8888\\";
                        List<TblDbdocument> tblDbdocuments = new ArrayList<>();

                        File theDir = new File(UPLOADED_FOLDER + tblDbclaim.getDbcode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }

                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);

                            TblDbdocument tblDbdocument = new TblDbdocument();
                            tblDbdocument.setDocumentPath(UPLOADED_SERVER + tblDbclaim.getDbcode() + "\\" + multipartFile.getOriginalFilename());
                            tblDbdocument.setDocumentType(multipartFile.getContentType());
                            tblDbdocument.setTblDbclaim(tblDbclaim);

                            tblDbdocuments.add(tblDbdocument);
                        }

                        tblDbdocuments = dbService.saveTblDbDocuments(tblDbdocuments);
                        tblDbtask.setStatus("C");
                        tblDbtask.setRemarks("Completed");
                    }
                    TblDbtask tblHdrtask1 = dbService.updateTblDbTask(tblDbtask);
                    if (tblHdrtask1 != null && tblHdrtask1.getDbtaskcode() > 0) {

                        List<TblDbtask> tblDbtasks = dbService.findTblDbTaskByDbClaimCode(tblDbclaim.getDbclaimcode());
                        int count = 0;
                        for (TblDbtask Dbtask : tblDbtasks) {
                            if (Dbtask.getStatus().equals("C")) {
                                count++;
                            }
                        }

                        if (count == tblDbtasks.size()) {
                            tblDbclaim.setStatus(new BigDecimal(61));

                            tblDbclaim = dbService.saveDbRequest(tblDbclaim);
                        }

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblHdrtask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
