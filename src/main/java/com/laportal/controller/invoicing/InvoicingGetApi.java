package com.laportal.controller.invoicing;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.InvoiceDetail;
import com.laportal.dto.InvoicingStatusCountList;
import com.laportal.dto.RtaNotesResponse;
import com.laportal.model.*;
import com.laportal.service.invoicing.InvoicingService;
import com.laportal.service.user.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/invoicing")
public class InvoicingGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(InvoicingGetApi.class);
    @Autowired
    private InvoicingService invoicingService;
    @Autowired
    private UsersService usersService;

    @RequestMapping(value = "/getInvoices", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getInvoices(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingGetApi \n METHOD == getInvoices(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblSolicitorInvoiceHead> tblSolicitorInvoiceList = invoicingService.getInvoices();
            if (tblSolicitorInvoiceList != null && tblSolicitorInvoiceList.size() > 0) {
                for (TblSolicitorInvoiceHead tblSolicitorInvoiceHead : tblSolicitorInvoiceList) {
                    tblSolicitorInvoiceHead.setCompaingcode("ALL");
                    tblSolicitorInvoiceHead.setTblSolicitorInvoiceDetails(null);
                    tblSolicitorInvoiceHead.setDuedate(addDaysToSpecificDate(tblSolicitorInvoiceHead.getCreatedate(),2));
                }
                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblSolicitorInvoiceList);
            } else {
                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingGetApi \n METHOD == getInvoices();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllInvoicesStatusCount", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllInvoicesStatusCount(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingGetApi \n METHOD == getAllInvoicesStatusCount(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAllInvoicesStatusCount(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<InvoicingStatusCountList> invoicingStatusCountLists = invoicingService.getAllInvoicesStatusCount();
            if (invoicingStatusCountLists != null) {
                LOG.info("\n EXITING THIS METHOD == getAllInvoicesStatusCount(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", invoicingStatusCountLists);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAllInvoicesStatusCount(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == InvoicingGetApi \n METHOD == getAllInvoicesStatusCount();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllInvoicesStatusCount(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getInvoicesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getInvoicesByStatus(@PathVariable long statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingGetApi \n METHOD == getInvoicesByStatus(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getInvoicesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblSolicitorInvoiceHead> tblSolicitorInvoiceList = invoicingService.getInvoicesByStatus(statusId);
            if (tblSolicitorInvoiceList != null && tblSolicitorInvoiceList.size() > 0) {
                for (TblSolicitorInvoiceHead tblSolicitorInvoiceHead : tblSolicitorInvoiceList) {
                    tblSolicitorInvoiceHead.setTblSolicitorInvoiceDetails(null);
                    tblSolicitorInvoiceHead.setDuedate(addDaysToSpecificDate(tblSolicitorInvoiceHead.getCreatedate(),2));
                }
                LOG.info("\n EXITING THIS METHOD == getInvoicesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblSolicitorInvoiceList);
            } else {
                LOG.info("\n EXITING THIS METHOD == getInvoicesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == InvoicingGetApi \n METHOD == getInvoicesByStatus();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getInvoicesByStatus(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getInvoiceDetail/{invoiceId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getInvoices(@PathVariable long invoiceId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingGetApi \n METHOD == getInvoices(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<ViewInvoiceDetail> tblSolicitorInvoiceDetail = invoicingService.getInvoiceDetailById(invoiceId);
            if (tblSolicitorInvoiceDetail != null && tblSolicitorInvoiceDetail.size() > 0) {
                InvoiceDetail invoiceDetail = new InvoiceDetail();

                TblSolicitorInvoiceHead tblSolicitorInvoiceHead = invoicingService.getInvoiceById(invoiceId);
                TblCompanyprofile tblCompanyprofile = invoicingService.getCompanyProfileById(tblSolicitorInvoiceHead.getCompanycode());
                TblCompanyprofile tblCompanyprofileLA = null;
                if(tblCompanyprofile.getCompanycode().equalsIgnoreCase("210") ||
                        tblCompanyprofile.getCompanycode().equalsIgnoreCase("625")){
                    tblCompanyprofileLA  = invoicingService.getCompanyProfileById("9");
                }else{
                    tblCompanyprofileLA = invoicingService.getCompanyProfileById("10");
                }


                TblUser tblUser1 = null;
                if (tblSolicitorInvoiceHead.getCreateuser() != null) {
                    tblUser1 = invoicingService.getUserById(tblSolicitorInvoiceHead.getCreateuser().toString());
                }


                invoiceDetail.setInvoiceToName(tblCompanyprofile.getName());
                invoiceDetail.setInvoiceToPostcode(tblCompanyprofile.getPostcode());
                invoiceDetail.setInvoiceToAddress1(tblCompanyprofile.getAddressline1());
                invoiceDetail.setInvoiceToAddress2(tblCompanyprofile.getAddressline2());
                invoiceDetail.setInvoiceToCity(tblCompanyprofile.getCity());

                invoiceDetail.setInvoiceFromName(tblCompanyprofileLA.getName());
                invoiceDetail.setInvoiceFromPostcode(tblCompanyprofileLA.getPostcode());
                invoiceDetail.setInvoiceFromAddress1(tblCompanyprofileLA.getAddressline1());
                invoiceDetail.setInvoiceFromAddress2(tblCompanyprofileLA.getAddressline2());
                invoiceDetail.setInvoiceFromCity(tblCompanyprofileLA.getCity());
                invoiceDetail.setAccountName(tblCompanyprofileLA.getAccountName());
                invoiceDetail.setAccountNumber(tblCompanyprofileLA.getAccountNumber());
                invoiceDetail.setSortcode(tblCompanyprofileLA.getSortcode());
                invoiceDetail.setVaregno(tblCompanyprofileLA.getVaregno());

                invoiceDetail.setInvoiceheadid(tblSolicitorInvoiceHead.getInvoiceheadid());
                invoiceDetail.setSolicitorCode(tblSolicitorInvoiceHead.getCompanycode());
                invoiceDetail.setInvoiceDate(tblSolicitorInvoiceHead.getCreatedate());
                invoiceDetail.setInvoiceNumber(tblSolicitorInvoiceHead.getInvoicecode());
                invoiceDetail.setTotalInstructions(tblSolicitorInvoiceDetail.size());
                invoiceDetail.setIssuedBy(tblUser1 == null ? "Auto Generated" : tblUser1.getUsername());
                invoiceDetail.setPaymentMethod("None");
                invoiceDetail.setTblSolicitorInvoiceDetail(tblSolicitorInvoiceDetail);
                invoiceDetail.setStatus(tblSolicitorInvoiceHead.getStatuscode());
                invoiceDetail.setDuedate(addDaysToSpecificDate(invoiceDetail.getInvoiceDate(),2));
                invoiceDetail.setAmount(tblSolicitorInvoiceHead.getAmount().longValue());
                invoiceDetail.setVatAmount(tblSolicitorInvoiceHead.getVatAmount().longValue());

                List<TblRtastatus> tblRtastatuses = invoicingService.getRtaStatusForInvoices();

                invoiceDetail.setTblRtastatuses(tblRtastatuses);

                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", invoiceDetail);
            } else {
                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingGetApi \n METHOD == getInvoices();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getInvoiceListCsv", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getInvoiceListCsv(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingGetApi \n METHOD == getInvoices(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<ViewInvoice> invoiceListCsvResponses = invoicingService.getInvoiceListCsv();
            if (invoiceListCsvResponses != null && invoiceListCsvResponses.size() > 0) {

                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", invoiceListCsvResponses);
            } else {
                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingGetApi \n METHOD == getInvoices();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getInvoiceLinesCsv", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getInvoiceLinesCsv(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingGetApi \n METHOD == getInvoices(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<ViewInvoiceLine> invoiceLinesCsv = invoicingService.getInvoiceLinesCsv();
            if (invoiceLinesCsv != null && invoiceLinesCsv.size() > 0) {

                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", invoiceLinesCsv);
            } else {
                LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingGetApi \n METHOD == getInvoices();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getInvoices(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllManualInvoices", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllManualInvoices(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingGetApi \n METHOD == getAllManualInvoices(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAllManualInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TblManualinvoicing> allManualInvoices = invoicingService.getAllManualInvoices();
            if (allManualInvoices != null && allManualInvoices.size() > 0) {

                LOG.info("\n EXITING THIS METHOD == getAllManualInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", allManualInvoices);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAllManualInvoices(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingGetApi \n METHOD == getAllManualInvoices();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllManualInvoices(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getManualInvoice/{manualInvId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getManualInvoice(@PathVariable long manualInvId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingGetApi \n METHOD == getManualInvoice(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getManualInvoice(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblManualinvoicing tblManualinvoicing = invoicingService.getManualInvoiceById(manualInvId);
            if (tblManualinvoicing != null) {

                LOG.info("\n EXITING THIS METHOD == getManualInvoice(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblManualinvoicing);
            } else {
                LOG.info("\n EXITING THIS METHOD == getManualInvoice(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingGetApi \n METHOD == getManualInvoice();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getManualInvoice(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getInvoiceCaseNotes/{invoiceHeadId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getInvoiceCaseNotes(@PathVariable String invoiceHeadId,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingGetApi \n METHOD == getInvoiceCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getInvoiceCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

                List<TblInvoicenote> tblInvoicenotes = invoicingService.getInvoiceCaseNotes(invoiceHeadId, tblUser);

                if (tblInvoicenotes != null && tblInvoicenotes.size() > 0) {
                    RtaNotesResponse rtaNotesResponse = new RtaNotesResponse();
                    rtaNotesResponse.setTblInvoicenotes(tblInvoicenotes);

                    LOG.info("\n EXITING THIS METHOD == getInvoiceCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaNotesResponse);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getInvoiceCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }


        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingGetApi \n METHOD == getInvoiceCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getInvoiceCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }
}
