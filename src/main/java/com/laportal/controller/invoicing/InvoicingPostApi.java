package com.laportal.controller.invoicing;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.hdr.HdrService;
import com.laportal.service.hire.HireService;
import com.laportal.service.invoicing.InvoicingService;
import com.laportal.service.rta.RtaService;
import com.laportal.service.user.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/invoicing")
public class InvoicingPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(InvoicingPostApi.class);
    @Autowired
    private InvoicingService invoicingService;
    @Autowired
    private HdrService hdrService;
    @Autowired
    private UsersService usersService;
    @Autowired
    private RtaService rtaService;
    @Autowired
    private HireService hireService;
    @Autowired
    private Environment env;


    @RequestMapping(value = "/removeInvoiceFromDetail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> removeInoviceFromDetail(@RequestBody RemoveInvoiceRequest removeInvoiceRequest,
                                                                           HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingPostApi \n METHOD == removeInoviceFromDetail(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == removeInoviceFromDetail(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (removeInvoiceRequest.getInvType().equals("A")) {
                TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail = invoicingService.getInvoiceDetailByDetailId(Long.valueOf(removeInvoiceRequest.getInvoiceDetailId()));

                if (tblSolicitorInvoiceDetail.getTblCompaign().getCompaigncode().equals("1")) {
                    TblRtaclaim tblRtaclaim = rtaService.getTblRtaclaimById(Long.valueOf(tblSolicitorInvoiceDetail.getCasecode().toString()));
                    tblRtaclaim.setStatuscode("7");

                    tblRtaclaim = rtaService.updateRtaCase(tblRtaclaim, null);
                } else if (tblSolicitorInvoiceDetail.getTblCompaign().getCompaigncode().equals("2")) {

                } else if (tblSolicitorInvoiceDetail.getTblCompaign().getCompaigncode().equals("3")) {
                    TblHdrclaim tblHdrclaim = hdrService.findHdrCaseByIdwithoutuser(Long.valueOf(tblSolicitorInvoiceDetail.getCasecode().toString()));
                    tblHdrclaim.setStatuscode(new BigDecimal(50));

                    tblHdrclaim = hdrService.updateHdrCase(tblHdrclaim);
                } else {

                }


                invoicingService.deleteInvoiceDetail(tblSolicitorInvoiceDetail);
            } else {
                invoicingService.deleteInvoiceDetailManual(removeInvoiceRequest);
            }
            return getResponseFormat(HttpStatus.OK, "Invoice Line Removed SuccessFully", "Invoice Line Removed SuccessFully");

        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingPostApi \n METHOD == removeInoviceFromDetail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == removeInoviceFromDetail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addCaseInInvoice", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addCaseInInvoice(@RequestBody AddCaseInvoiceRequest addCaseInvoiceRequest,
                                                                    HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingPostApi \n METHOD == addCaseInInvoice(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addCaseInInvoice(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail = new TblSolicitorInvoiceDetail();
            TblSolicitorInvoiceHead tblSolicitorInvoiceHead = invoicingService.getInvoiceById(Long.parseLong(addCaseInvoiceRequest.getInvoiceId()));

            /// for RTA INVOICE ADD
            if (addCaseInvoiceRequest.getCompaingcode().equals("1")) {

                TblRtaclaim tblRtaclaim = rtaService.findRtaById(addCaseInvoiceRequest.getCaseId());
                TblRtasolicitor tblRtasolicitor = rtaService.getRtaSolicitorsOfRtaClaim(String.valueOf(tblRtaclaim.getRtacode()));
                TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(String.valueOf(tblRtasolicitor.getCompanycode()));
                List<TblCompanyjob> tblCompanyjobs = usersService.getcompanyJobs(tblCompanyprofile.getCompanycode());
                tblCompanyprofile.setTblCompanyjobs(tblCompanyjobs);
                if (tblSolicitorInvoiceHead != null) {

                    tblSolicitorInvoiceDetail.setTblSolicitorInvoiceHead(tblSolicitorInvoiceHead);

                    tblSolicitorInvoiceDetail.setAddress1(tblRtaclaim.getAddress1());
                    tblSolicitorInvoiceDetail.setAddress2(tblRtaclaim.getAddress2());
                    tblSolicitorInvoiceDetail.setAddress3(tblRtaclaim.getAddress3());
                    tblSolicitorInvoiceDetail.setCasecode(BigDecimal.valueOf(tblRtaclaim.getRtacode()));
                    tblSolicitorInvoiceDetail.setCasenumber(tblRtaclaim.getRtanumber());
                    tblSolicitorInvoiceDetail.setCity(tblRtaclaim.getCity());
                    tblSolicitorInvoiceDetail.setCreatedate(new Date());
                    tblSolicitorInvoiceDetail.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                    tblSolicitorInvoiceDetail.setEmail(tblRtaclaim.getEmail());
                    tblSolicitorInvoiceDetail.setFirstname(tblRtaclaim.getFirstname());
                    tblSolicitorInvoiceDetail.setLastname(tblRtaclaim.getLastname());
                    tblSolicitorInvoiceDetail.setMiddlename(tblRtaclaim.getMiddlename());
                    tblSolicitorInvoiceDetail.setMobile(tblRtaclaim.getMobile());
                    tblSolicitorInvoiceDetail.setNinumber(tblRtaclaim.getNinumber());
                    tblSolicitorInvoiceDetail.setRegion(tblRtaclaim.getRegion());
                    tblSolicitorInvoiceDetail.setPostalcode(tblRtaclaim.getPostalcode());
                    tblSolicitorInvoiceDetail.setTitle(tblRtaclaim.getTitle());
                    tblSolicitorInvoiceDetail.setAdjust(new BigDecimal(0));

                    if (tblCompanyprofile.getTblCompanyjobs() != null && tblCompanyprofile.getTblCompanyjobs().size() > 0) {
                        for (TblCompanyjob tblCompanyjob : tblCompanyjobs) {
                            if (tblCompanyjob.getTblCompaign().getCompaigncode().equals("1") && tblCompanyjob.getStatus().equals("Y")) {

                                // for adult
                                if (tblRtaclaim.getGfirstname() == null || tblRtaclaim.getGfirstname().isEmpty()) {
                                    List<TblRtainjury> tblRtainjuries = rtaService.getInjuriesOfRta(tblRtaclaim.getRtacode());
                                    String injTypeToCharge = "V";
                                    BigDecimal amounTocharge = new BigDecimal(0);
                                    if (tblRtaclaim.getVehicleType().equals("C")) {
                                        for (TblRtainjury tblRtainjury : tblRtainjuries) {
                                            if (tblRtainjury.getTblInjclass().getInjtype().equals("W")) {
                                                injTypeToCharge = "W";
                                                amounTocharge = tblCompanyjob.getWiplash();
                                            } else if (tblRtainjury.getTblInjclass().getInjtype().equals("H")) {
                                                injTypeToCharge = "H";
                                                amounTocharge = tblCompanyjob.getHybrid();
                                            } else if (tblRtainjury.getTblInjclass().getInjtype().equals("S")) {
                                                injTypeToCharge = "S";
                                            }
                                        }
                                    } else if (tblRtaclaim.getVehicleType().equals("B")) {
                                        amounTocharge = tblCompanyjob.getBike();
                                    } else if (tblRtaclaim.getVehicleType().equals("P")) {
                                        amounTocharge = tblCompanyjob.getPedestrian();
                                    }
                                    tblSolicitorInvoiceDetail.setAmount(amounTocharge);
                                    tblSolicitorInvoiceDetail.setCaseinjtype(injTypeToCharge);
                                } else if (tblRtaclaim.getGfirstname() != null || !tblRtaclaim.getGfirstname().isEmpty()) {
                                    // for minor
                                    tblSolicitorInvoiceDetail.setAmount(tblCompanyjob.getMinor());
                                } else {
                                    tblSolicitorInvoiceDetail.setAmount(new BigDecimal(0));
                                }


                            }
                        }

                    }

                    tblSolicitorInvoiceHead.setAmount(tblSolicitorInvoiceHead.getAmount().add(tblSolicitorInvoiceDetail.getAmount()));
                    tblSolicitorInvoiceHead.setUpdatedate(new Date());

                    tblRtaclaim.setStatuscode("13");
                    tblRtaclaim = rtaService.updateRtaCase(tblRtaclaim, null);
                    tblSolicitorInvoiceHead = invoicingService.addSingleInvoice(tblSolicitorInvoiceHead, tblSolicitorInvoiceDetail);

                    if (tblSolicitorInvoiceHead != null) {
                        LOG.info("\n EXITING THIS METHOD == addCaseInInvoice(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Inovice Line Added SuccessFully", null);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == addCaseInInvoice(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Line To Invoice.", null);
                    }

                } else {
                    LOG.info("\n EXITING THIS METHOD == addCaseInInvoice(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Invoice Found ", null);
                }


            }

            /// for HIRE INVOICE ADD
            else if (addCaseInvoiceRequest.getCompaingcode().equals(2)) {

            }

            /// for HDR INVOICE ADD
            else if (addCaseInvoiceRequest.getCompaingcode().equals(3)) {
                TblHdrclaim tblHdrclaim = hdrService.findHdrCaseByIdwithoutuser(Long.parseLong(addCaseInvoiceRequest.getCaseId()));
                TblHdrclaimant tblHdrclaimant = hdrService.findHdrClaimantByClaimId(tblHdrclaim.getHdrclaimcode());
                TblCompanyprofile tblCompanyprofile = hdrService.findHireBuisnessByHdrCaseById(tblHdrclaim.getHdrclaimcode());
                if (tblSolicitorInvoiceHead != null) {

                    tblSolicitorInvoiceDetail.setTblSolicitorInvoiceHead(tblSolicitorInvoiceHead);

                    tblSolicitorInvoiceDetail.setAddress1(tblHdrclaimant.getCaddress1());
                    tblSolicitorInvoiceDetail.setAddress2(tblHdrclaimant.getCaddress2());
                    tblSolicitorInvoiceDetail.setAddress3(tblHdrclaimant.getCaddress3());
                    tblSolicitorInvoiceDetail.setCasecode(BigDecimal.valueOf(tblHdrclaim.getHdrclaimcode()));
                    tblSolicitorInvoiceDetail.setCasenumber(tblHdrclaim.getClaimcode());
                    tblSolicitorInvoiceDetail.setCity(tblHdrclaimant.getCcity());
                    tblSolicitorInvoiceDetail.setCreatedate(new Date());
                    tblSolicitorInvoiceDetail.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                    tblSolicitorInvoiceDetail.setEmail(tblHdrclaimant.getCemail());
                    tblSolicitorInvoiceDetail.setFirstname(tblHdrclaimant.getCfname());
                    tblSolicitorInvoiceDetail.setLastname(tblHdrclaimant.getCsname());
                    tblSolicitorInvoiceDetail.setMiddlename(tblHdrclaimant.getCmname());
                    tblSolicitorInvoiceDetail.setMobile(tblHdrclaimant.getCmobileno());
                    tblSolicitorInvoiceDetail.setNinumber(tblHdrclaimant.getCninumber());
                    tblSolicitorInvoiceDetail.setRegion(tblHdrclaimant.getCregion());
                    tblSolicitorInvoiceDetail.setPostalcode(tblHdrclaimant.getCpostalcode());
                    tblSolicitorInvoiceDetail.setTitle(tblHdrclaimant.getCtitle());

                    if (tblCompanyprofile.getTblCompanyjobs() != null && tblCompanyprofile.getTblCompanyjobs().size() > 0) {
                        List<TblCompanyjob> tblCompanyjobs = tblCompanyprofile.getTblCompanyjobs();
                        for (TblCompanyjob tblCompanyjob : tblCompanyjobs) {
                            if (tblCompanyjob.getTblCompaign().getCompaigncode().equals("3") && tblCompanyjob.getStatus().equals("Y")) {
                                tblSolicitorInvoiceDetail.setAmount(tblCompanyjob.getHousingFee());
                            }
                        }

                    }

                    tblSolicitorInvoiceHead.setAmount(tblSolicitorInvoiceHead.getAmount().add(tblSolicitorInvoiceDetail.getAmount()));
                    tblSolicitorInvoiceHead.setUpdatedate(new Date());

                    tblHdrclaim.setStatuscode(new BigDecimal(51));
                    tblHdrclaim = hdrService.updateHdrCase(tblHdrclaim);
                    tblSolicitorInvoiceHead = invoicingService.addSingleInvoice(tblSolicitorInvoiceHead, tblSolicitorInvoiceDetail);

                    if (tblSolicitorInvoiceHead != null) {
                        LOG.info("\n EXITING THIS METHOD == addCaseInInvoice(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Inovice Line Added SuccessFully", null);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == addCaseInInvoice(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Line To Invoice.", null);
                    }

                } else {
                    LOG.info("\n EXITING THIS METHOD == addCaseInInvoice(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Invoice Found ", null);
                }
            }


            return getResponseFormat(HttpStatus.OK, "Success", "Success");

        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingPostApi \n METHOD == addCaseInInvoice();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addCaseInInvoice(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/sendInvoiceEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> sendInvoiceEmail(@RequestParam("invoiceId") String invoiceId, @RequestParam("invoice") List<MultipartFile> multipartFiles,
                                                                    HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingPostApi \n METHOD == sendInvoiceEmail(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addCaseInInvoice(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblSolicitorInvoiceHead tblSolicitorInvoiceHead = invoicingService.getInvoiceById(Long.valueOf(invoiceId));
            TblCompanyprofile tblCompanyprofile = usersService.getCompaniesProfile(tblSolicitorInvoiceHead.getCompanycode());
            String toEmail = env.getProperty("spring.mail.username");
            TblEmailTemplate tblEmailTemplate = rtaService.findByEmailType("invoice-email");

            if (tblCompanyprofile.getTblUsercategory().equals("1")) {
                toEmail = tblCompanyprofile.getBilltoemail();
            } else if (tblCompanyprofile.getTblUsercategory().equals("2")) {
                toEmail = tblCompanyprofile.getAccountemail();
            }
            String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Invoice_Email\\";
            String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Invoice_Email\\";

            File theDir = new File(UPLOADED_FOLDER + tblSolicitorInvoiceHead.getInvoicecode());
            if (!theDir.exists()) {
                theDir.mkdirs();
            }

            for (MultipartFile multipartFile : multipartFiles) {
                byte[] bytes = multipartFile.getBytes();
                Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename() + ".pdf");
                Files.write(path, bytes);
            }
            Date dueDate = addDaysToSpecificDate(tblSolicitorInvoiceHead.getCreatedate(), 2);



            String body = tblEmailTemplate.getEmailtemplate();
            body.replace("[COMPANY_NAME]",tblCompanyprofile.getName());
            body.replace("[INVOICE_NO]",tblSolicitorInvoiceHead.getInvoicecode());
            body.replace("[DUE_DATE]",formatter.format(dueDate));


            TblEmail tblEmail = new TblEmail();
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setCreatedon(new Date());
            tblEmail.setCcemailaddress("rizwan@legalassistltd.co.uk,safina@legalassistltd.co.uk");
            tblEmail.setEmailbody(body);
            tblEmail.setEmailsubject("Claims Group Invoice Number " + tblSolicitorInvoiceHead.getInvoicecode());
            tblEmail.setEmailaddress(toEmail);
            tblEmail.setEmailattachment(theDir + "\\" + multipartFiles.get(0).getOriginalFilename() + ".pdf");

            tblEmail = usersService.saveEmail(tblEmail);
            TblSolicitorInvoiceHead tblSolicitorInvoiceHead1 = invoicingService.performActionOnInvoicing(String.valueOf(tblSolicitorInvoiceHead.getInvoiceheadid()),
                    "160", tblUser, tblEmail.getEmailcode());

            return getResponseFormat(HttpStatus.OK, "Success", "Success");

        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingPostApi \n METHOD == sendInvoiceEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == sendInvoiceEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnInvoicing", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnInvoicing(
            @RequestBody PerformInvoiceActionRequest performInvoiceActionRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingPostApi \n METHOD == performActionOnInvoicing(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnInvoicing(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                if (performInvoiceActionRequest.getToStatus().equals("161")) {
                    // for PAID status
                    TblSolicitorInvoiceHead tblSolicitorInvoiceHead = invoicingService.performPaidActionOnInvoicing(performInvoiceActionRequest.getCaseId(),
                            performInvoiceActionRequest.getToStatus(), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnInvoicing(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblSolicitorInvoiceHead);


                } else if (performInvoiceActionRequest.getToStatus().equals("163")) {
                    // for voided

                    TblSolicitorInvoiceHead tblSolicitorInvoiceHead = invoicingService.performVoidedActionOnInvoicing(performInvoiceActionRequest.getCaseId(),
                            performInvoiceActionRequest.getToStatus(), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnInvoicing(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblSolicitorInvoiceHead);
                } else if (performInvoiceActionRequest.getToStatus().equals("165")) {
                    // for over due


                    TblSolicitorInvoiceHead tblSolicitorInvoiceHead = invoicingService.performOverDueActionOnInvoicing(performInvoiceActionRequest.getCaseId(),
                            performInvoiceActionRequest.getToStatus(), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnInvoicing(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblSolicitorInvoiceHead);
                }
                LOG.info("\n EXITING THIS METHOD == performActionOnInvoicing(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "New Status Added Kindly Implement Its Logic", null);
            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnInvoicing(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == InvoicingPostApi \n METHOD == performActionOnInvoicing();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnInvoicing(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/manualJob", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> manualJob(@RequestBody ManualJobRequest manualJobRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingPostApi \n METHOD == manualJob(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == manualJob(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                String responsecode = "";
                TblCompaign tblCompaign = usersService.getcompaingById(manualJobRequest.getCampaignCode());
                for (String companyCode : manualJobRequest.getCompanyCodeList()) {
                    if (manualJobRequest.getBillallpendingcases().equalsIgnoreCase("N")) {
                        responsecode = invoicingService.runManualJobForInvoicing(manualJobRequest.getBillByDate(),
                                companyCode, tblCompaign.getCompaignname(), tblUser.getUsercode());
                    }else {
                        responsecode = invoicingService.runManualJobForInvoicing(null,
                                companyCode, tblCompaign.getCompaignname(), tblUser.getUsercode());
                    }
                }

                if (!responsecode.isEmpty()) {
                    LOG.info("\n EXITING THIS METHOD == manualJob(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, responsecode, responsecode);
                } else {
                    LOG.info("\n EXITING THIS METHOD == manualJob(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Job Execution Failed", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == manualJob(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == InvoicingPostApi \n METHOD == manualJob();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == manualJob(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/invoiceSearch", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> invoiceSearch(@RequestBody InvoiceSearch invoiceSearch, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingPostApi \n METHOD == sendInvoiceEmail(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addCaseInInvoice(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");


            invoiceSearch.setDatefrom(outputFormat.format(inputFormat.parse(invoiceSearch.getDatefrom())));
            invoiceSearch.setDateto(outputFormat.format(inputFormat.parse(invoiceSearch.getDateto())));


            List<TblSolicitorInvoiceHead> tblSolicitorInvoiceList = invoicingService.invoiceSearch(invoiceSearch);
            if (tblSolicitorInvoiceList != null && tblSolicitorInvoiceList.size() > 0) {
                return getResponseFormat(HttpStatus.OK, "Record(s) Found", tblSolicitorInvoiceList);
            } else {
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Record Found", null);
            }


        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingPostApi \n METHOD == sendInvoiceEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == sendInvoiceEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/addmanualinvoice", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addmanualinvoice(@RequestBody ManualinvoiceRequest manualinvoiceRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingPostApi \n METHOD == addmanualinvoice(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addmanualinvoice(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }


            TblManualinvoicing tblManualinvoicing = new TblManualinvoicing();
            List<TblManualinvoicingdetail> tblManualinvoicingdetails = new ArrayList<>();

            tblManualinvoicing.setInvoiceno(invoicingService.getmanualInvoiceNumber());
            tblManualinvoicing.setCreateuser(tblUser.getUsercode());
            tblManualinvoicing.setCustomername(manualinvoiceRequest.getCustomername());
            tblManualinvoicing.setCustomeraddress(manualinvoiceRequest.getCustomeraddress());
            tblManualinvoicing.setDuedate(formatter.parse(manualinvoiceRequest.getDuedate()));
            tblManualinvoicing.setGrandtotal(new BigDecimal(manualinvoiceRequest.getGrandtotal()));
            tblManualinvoicing.setTax(new BigDecimal(manualinvoiceRequest.getTax()));
            tblManualinvoicing.setStatus("N");
            tblManualinvoicing.setStatusdescr("NEW");

            for (ManualinvoiceRequestDetail manualinvoiceRequestDetail : manualinvoiceRequest.getManualinvoicedetails()) {
                TblManualinvoicingdetail tblManualinvoicingdetail = new TblManualinvoicingdetail();
                tblManualinvoicingdetail.setDescription(manualinvoiceRequestDetail.getDescription());
                tblManualinvoicingdetail.setAmount(new BigDecimal(manualinvoiceRequestDetail.getAmount()));
                tblManualinvoicingdetail.setCreateuser(tblUser.getUsercode());

                tblManualinvoicingdetails.add(tblManualinvoicingdetail);
            }

            tblManualinvoicing = invoicingService.saveManualInvoice(tblManualinvoicing, tblManualinvoicingdetails);

            if (tblManualinvoicing != null) {

                LOG.info("\n EXITING THIS METHOD == addmanualinvoice(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Invoice Generated", tblManualinvoicing);

            } else {
                LOG.info("\n EXITING THIS METHOD == addmanualinvoice(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Saving Manual Invoice", null);
            }


        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingPostApi \n METHOD == addmanualinvoice();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addmanualinvoice(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/updateInvoiceDetail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateInvoiceDetail(@RequestBody UpdateInvoiceDetail updateInvoiceDetail,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingPostApi \n METHOD == removeInoviceFromDetail(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == removeInoviceFromDetail(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (updateInvoiceDetail.getInvType().equals("A")) {
                TblSolicitorInvoiceDetail tblSolicitorInvoiceDetail = invoicingService.getInvoiceDetailByDetailId(Long.valueOf(updateInvoiceDetail.getInvoiceDetailId()));

                tblSolicitorInvoiceDetail.setAmount(new BigDecimal(updateInvoiceDetail.getCharge()));
                tblSolicitorInvoiceDetail.setAdjust(new BigDecimal(updateInvoiceDetail.getAdjust()));
                invoicingService.updateInvoiceDetail(tblSolicitorInvoiceDetail);
            } else {
                TblInvoiceDetail tblInvoiceDetail = invoicingService.getManualInvoiceDetailById(updateInvoiceDetail.getInvoiceDetailId());
                tblInvoiceDetail.setAdjust(Long.valueOf(updateInvoiceDetail.getAdjust()));
                tblInvoiceDetail.setCharge(Long.valueOf(updateInvoiceDetail.getCharge()));
                invoicingService.updateManualInvoiceDetail(tblInvoiceDetail);
            }


            return getResponseFormat(HttpStatus.OK, "Invoice Line Updated SuccessFully", "Invoice Line Updated SuccessFully");

        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingPostApi \n METHOD == removeInoviceFromDetail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == removeInoviceFromDetail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addManualInvoiceLine", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addManualInvoiceLine(@RequestBody ManualInvoiceLineRequest manualInvoiceLineRequest,
                                                                        HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingPostApi \n METHOD == addManualInvoiceLine(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addManualInvoiceLine(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }


            TblInvoiceDetail tblInvoiceDetail = new TblInvoiceDetail();
            TblSolicitorInvoiceHead tblSolicitorInvoiceHead = new TblSolicitorInvoiceHead();

            tblSolicitorInvoiceHead.setInvoiceheadid(manualInvoiceLineRequest.getInvoiceheadid());

            tblInvoiceDetail.setAdjust(manualInvoiceLineRequest.getAdjust());
            tblInvoiceDetail.setCharge(manualInvoiceLineRequest.getCharge());
            tblInvoiceDetail.setDescription(manualInvoiceLineRequest.getDescription());
            tblInvoiceDetail.setCreateuser(Long.valueOf(tblUser.getUsercode()));
            tblInvoiceDetail.setCreatedate(new Date());
            tblInvoiceDetail.setTblSolicitorInvoiceHead(tblSolicitorInvoiceHead);

            tblInvoiceDetail = invoicingService.addManualInvoiceLine(tblInvoiceDetail);

            return getResponseFormat(HttpStatus.OK, "Invoice Line Added SuccessFully", "Invoice Line Added SuccessFully");

        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingPostApi \n METHOD == addManualInvoiceLine();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addManualInvoiceLine(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/addNoteToInvoice", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNoteToRta(@RequestBody RtaNoteRequest rtaNoteRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == InvoicingPostApi \n METHOD == addNoteToRta(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (!rtaNoteRequest.getInvoiceHeadId().isEmpty()) {

                TblInvoicenote tblInvoicenote = new TblInvoicenote();
                TblSolicitorInvoiceHead tblSolicitorInvoiceHead = new TblSolicitorInvoiceHead();

                tblSolicitorInvoiceHead.setInvoiceheadid(Long.valueOf(rtaNoteRequest.getInvoiceHeadId()));
                tblInvoicenote.setTblSolicitorInvoiceHead(tblSolicitorInvoiceHead);
                tblInvoicenote.setNote(rtaNoteRequest.getNote());
                tblInvoicenote.setUsercategorycode(
                        rtaNoteRequest.getUserCatCode() == null || rtaNoteRequest.getUserCatCode().isEmpty() ? "4"
                                : rtaNoteRequest.getUserCatCode());
                tblInvoicenote.setCreatedon(new Date());
                tblInvoicenote.setUsercode(tblUser.getUsercode());

                tblInvoicenote = invoicingService.addNoteToInvoice(tblInvoicenote);

                if (tblInvoicenote != null && tblInvoicenote.getInvoicenotescode() > 0) {

                    LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblInvoicenote);

                } else {
                    LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == InvoicingPostApi \n METHOD == addNoteToRta();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }
}
