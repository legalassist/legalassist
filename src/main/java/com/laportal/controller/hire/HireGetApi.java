package com.laportal.controller.hire;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.hire.HireService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/hire")
public class HireGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(HireGetApi.class);

    @Autowired
    private HireService hireService;

    @SuppressWarnings("unused")
    @RequestMapping(value = "/getHireCases", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHireCases(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getimmigrationCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getimmigrationCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = hireService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<HireCaseList> immigrationCaseLists = hireService.getAuthHireCasesIntroducers(tblUser.getUsercode());
                if (immigrationCaseLists != null && immigrationCaseLists.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", immigrationCaseLists);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<HireCaseList> viewHdrCaseslist = hireService.getAuthHireCasesSolicitors(tblUser.getUsercode());
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<HireCaseList> viewHdrCaseslist = hireService.getAuthHireCasesLegalAssist();
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getDbCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getDbCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getHireCaseById/{hirecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHireCaseById(@PathVariable long hirecode,
                                                                   HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == hireGetApi \n METHOD == getHireCaseById(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                TblHireclaim tblHireclaim = hireService.getHireCaseById(hirecode, tblUser);

                if (tblHireclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblHireclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == hireGetApi \n METHOD == getHireCaseById();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getHireCaseLogs/{hirecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHireCaseLogs(@PathVariable String hirecode,
                                                                   HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HireGetApi \n METHOD == getHireCaseLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblHirelog> hireCaseLogs = hireService.getHireCaseLogs(hirecode);

                if (hireCaseLogs != null && hireCaseLogs.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHireCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", hireCaseLogs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHireCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getHireCaseLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHireCaseLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getHireCaseNotes/{hirecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCaseNotes(@PathVariable String hirecode,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HireGetApi \n METHOD == getHireCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHireCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblHirenote> tblHireNotes = hireService.getHireCaseNotes(hirecode, tblUser);
                List<TblHirenote> tblHireNotesLegalInternal = hireService.getAuthHdrCaseNotesOfLegalInternal(hirecode, tblUser);

                HireNotesResponse hireNotesResponse = new HireNotesResponse();
                hireNotesResponse.setTblHirenotes(tblHireNotes);
                hireNotesResponse.setTblHirenotesLegalOnly(tblHireNotesLegalInternal);

                if (hireNotesResponse != null ) {
                    LOG.info("\n EXITING THIS METHOD == getHireCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", hireNotesResponse);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHireCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHireCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHireCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getHireCaseMessages/{hirecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCaseMessages(@PathVariable String hirecode,
                                                                          HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HireGetApi \n METHOD == getHireCaseMessages(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHireCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblHiremessage> tblHireMessages = hireService.getHireCaseMessages(hirecode);

                if (tblHireMessages != null && tblHireMessages.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHireCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblHireMessages);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHireCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHireCaseMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHireCaseMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getHireBusinesses/{hirecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHireBusinesses(@PathVariable String hirecode,
                                                                     HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HireGetApi \n METHOD == getHireBusinesses(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHireBusinesses(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblHirebusiness> tblHirebusinesses = hireService.getHireBusinessesAgainstHireCode(Long.valueOf(hirecode));

                if (tblHirebusinesses != null && tblHirebusinesses.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHireBusinesses(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblHirebusinesses);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireBusinesses(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHireBusinesses(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHireBusinesses();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHireBusinesses(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getHireCasedocuments/{hireCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHireCasedocuments(@PathVariable String hireCode,
                                                                        HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getHireCasedocuments(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblHiredocument> hireCasedocuments = hireService.getHireCasedocuments(Long.valueOf(hireCode));
                if (hireCasedocuments != null) {
                    LOG.info("\n EXITING THIS METHOD == getHireCasedocuments(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", hireCasedocuments);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireCasedocuments(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHireCasedocuments(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHireCasedocuments();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHireCasedocuments(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllHireStatusCounts", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllHireStatusCounts(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getAllHireStatusCounts(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = hireService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<ImigrationStatusCountList> hireStatusCountLists = hireService.getAllHireStatusCountsForIntroducers(tblCompanyprofile.getCompanycode());

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", hireStatusCountLists);
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<ImigrationStatusCountList> hireStatusCountLists = hireService.getAllHireStatusCountsForSolicitor(tblCompanyprofile.getCompanycode());

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", hireStatusCountLists);
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<ImigrationStatusCountList> hireStatusCountLists = hireService.getAllHireStatusCounts();

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", hireStatusCountLists);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getAllDbStatusCounts();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllDbStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getHireCasesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHireCasesByStatus(@PathVariable long statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHireCasesByStatus(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = hireService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<HireCaseList> immigrationCaseLists = hireService.getAuthHireCasesIntroducersByStatus(statusId, tblCompanyprofile.getCompanycode());
                if (immigrationCaseLists != null && immigrationCaseLists.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHireCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", immigrationCaseLists);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<HireCaseList> viewHdrCaseslist = hireService.getAuthHireCasesSolicitorsByStatus(statusId, tblUser.getUsercode());
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHireCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<HireCaseList> viewHdrCaseslist = hireService.getAuthHireCasesLegalAssistByStatus(statusId);
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHireCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHireCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHireCasesByStatus();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHireCasesByStatus(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getHireAuditLogs/{hirecasecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getRtaAuditLogs(@PathVariable String hirecasecode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS ==  \n METHOD == getHireAuditLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHireAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<RtaAuditLogResponse> rtaAuditLogResponses = hireService.getHireAuditLogs(hirecasecode);

                if (rtaAuditLogResponses != null && rtaAuditLogResponses.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHireAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaAuditLogResponses);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHireAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrGetApi \n METHOD == getHireAuditLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHireAuditLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getHireDuplicates/{hirecasecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHireDuplicates(@PathVariable String hirecasecode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getHireDuplicates(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHireDuplicates(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<RtaDuplicatesResponse> hireDuplicates = hireService.getHireDuplicates(hirecasecode);

                if (hireDuplicates != null) {
                    LOG.info("\n EXITING THIS METHOD == getHireDuplicates(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", hireDuplicates);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHireDuplicates(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHireDuplicates(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHireDuplicates();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHireDuplicates(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }
}
