package com.laportal.controller.hire;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.dto.xmldto.ClaimsCNFexport;
import com.laportal.model.*;
import com.laportal.service.hire.HireService;
import com.laportal.service.rta.RtaService;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/hire")
public class HirePostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(HirePostApi.class);

    @Autowired
    private HireService hireService;
    @Autowired
    private RtaService rtaService;

    @RequestMapping(value = "/addNewHireCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewRtaCase(@RequestBody SaveHireRequest saveHireRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == addNewHireCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = hireService.isHireCaseAllowed(tblUser.getCompanycode(), "2");
            boolean introducerAllowed = hireService.isHireCaseAllowed(saveHireRequest.getIntroducer(), "2");

            if (allowed && introducerAllowed) {
                TblHireclaim tblHireclaim = new TblHireclaim();

                tblHireclaim.setAccdate(formatter.parse(saveHireRequest.getAccdate()));
                tblHireclaim.setAcctime(saveHireRequest.getAcctime());
                tblHireclaim.setAddress1(saveHireRequest.getAddress1());
                tblHireclaim.setAddress2(saveHireRequest.getAddress2());
                tblHireclaim.setAddress3(saveHireRequest.getAddress3());

                TblCircumstance tblCircumstance = new TblCircumstance();
                if (saveHireRequest.getCircumcode() != null) {
                    tblCircumstance.setCircumcode(Long.valueOf(saveHireRequest.getCircumcode()));
                    tblHireclaim.setCircumcode(tblCircumstance);
                } else {
                    tblHireclaim.setCircumcode(null);
                }

                tblHireclaim.setCity(saveHireRequest.getCity());
                tblHireclaim.setClaimrefno(saveHireRequest.getClaimrefno());
                tblHireclaim.setContactdue(saveHireRequest.getContactdue() == null ? null
                        : formatter.parse(saveHireRequest.getContactdue()));
                tblHireclaim.setCreatedon(new Date());
                tblHireclaim.setDescription(saveHireRequest.getDescription());
                tblHireclaim.setDob(formatter.parse(saveHireRequest.getDob()));
                tblHireclaim.setDriverpassenger(saveHireRequest.getDriverpassenger());
                tblHireclaim.setEmail(saveHireRequest.getEmail());
                tblHireclaim.setFirstname(saveHireRequest.getFirstname());
                tblHireclaim.setGreencardno(saveHireRequest.getGreencardno());


                TblInjclass tblInjclass = new TblInjclass();
                if (saveHireRequest.getInjclasscode() != null) {
                    tblInjclass.setInjclasscode(Long.valueOf(saveHireRequest.getInjclasscode()));
                    tblHireclaim.setTblInjclass(tblInjclass);
                } else {
                    tblHireclaim.setTblInjclass(null);
                }

                tblHireclaim.setInjdescription(saveHireRequest.getInjdescription());
                tblHireclaim.setInjlength(saveHireRequest.getInjlength());
                tblHireclaim.setInstorage(saveHireRequest.getInstorage());
                tblHireclaim.setInsurer(saveHireRequest.getInsurer());
                tblHireclaim.setLandline(saveHireRequest.getLandline());
                tblHireclaim.setLastname(saveHireRequest.getLastname());
                tblHireclaim.setLocation(saveHireRequest.getLocation());
                tblHireclaim.setMakemodel(saveHireRequest.getMakemodel());
                tblHireclaim.setMedicalinfo(saveHireRequest.getMedicalinfo());
                tblHireclaim.setMiddlename(saveHireRequest.getMiddlename());
                tblHireclaim.setMobile(saveHireRequest.getMobile());
                tblHireclaim.setNinumber(saveHireRequest.getNinumber());
//                tblHireclaim.setOngoing(saveHireRequest.getOngoing());
                tblHireclaim.setPartyaddress(saveHireRequest.getPartyaddress());
                tblHireclaim.setPartycontactno(saveHireRequest.getPartycontactno());
                tblHireclaim.setPartyinsurer(saveHireRequest.getPartyinsurer());
                tblHireclaim.setPartymakemodel(saveHireRequest.getPartymakemodel());
                tblHireclaim.setPartyname(saveHireRequest.getPartyname());
                tblHireclaim.setPartypolicyno(saveHireRequest.getPartypolicyno());
                tblHireclaim.setPartyrefno(saveHireRequest.getPartyrefno());
                tblHireclaim.setPartyregno(saveHireRequest.getPartyregno());
                tblHireclaim.setPassengerinfo(saveHireRequest.getPassengerinfo());
                tblHireclaim.setPolicycover(saveHireRequest.getPolicycover());
                tblHireclaim.setPolicyholder(saveHireRequest.getPolicyholder());
                tblHireclaim.setPolicyno(saveHireRequest.getPolicyno());
                tblHireclaim.setPostalcode(saveHireRequest.getPostalcode());
                tblHireclaim.setRdweathercond(saveHireRequest.getRdweathercond());
                tblHireclaim.setRecovered(saveHireRequest.getRecovered());
                tblHireclaim.setRecoveredby(saveHireRequest.getRecoveredby());
                tblHireclaim.setRefno(saveHireRequest.getRefno());
                tblHireclaim.setRegion(saveHireRequest.getRegion());
                tblHireclaim.setRegisterationno(saveHireRequest.getRegisterationno());
                tblHireclaim.setReportedtopolice(saveHireRequest.getReportedtopolice());
                tblHireclaim.setScotland("");
                tblHireclaim.setStorage(saveHireRequest.getStorage());
                tblHireclaim.setTitle(saveHireRequest.getTitle());
                tblHireclaim.setUsercode(tblUser.getUsercode());
                tblHireclaim.setVehiclecondition(saveHireRequest.getVehiclecondition());
                tblHireclaim.setVehicledamage(saveHireRequest.getVehicledamage());
                tblHireclaim.setAdvisor(Long.valueOf(saveHireRequest.getAdvisor()));
                tblHireclaim.setIntroducer(Long.valueOf(saveHireRequest.getIntroducer()));
                tblHireclaim.setEsign("N");

                TblRtastatus tblRtastatus = new TblRtastatus();
                tblRtastatus.setStatuscode(31);

                tblHireclaim.setTblRtastatus(tblRtastatus);
                TblCompanyprofile tblCompanyprofile = hireService.getCompanyProfile(String.valueOf(tblHireclaim.getIntroducer()));
//				int hireDocNumber = hireService.getHireDocNumber();
                tblHireclaim.setHirenumber(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());
                TblHireclaim hireclaim = hireService.addNewHireCase(tblHireclaim, saveHireRequest.getFiles());

                if (hireclaim != null && hireclaim.getHirecode() > 0) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = hireService.saveCompanyProfile(tblCompanyprofile);

                    LOG.info("\n EXITING THIS METHOD == addNewHireCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", hireclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Saving Hire Case", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == addNewRtaCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/updateHireCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateHireCase(@RequestBody UpdateHireRequest updateHireRequest,
                                                                  HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == updateHireCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = hireService.isHireCaseAllowed(tblUser.getCompanycode(), "2");

            if (allowed) {
                TblHireclaim updateHireClaim = hireService.getTblHireClaimById(updateHireRequest.getHirecode());
//				TblHireclaim updateHireClaim = new TblHireclaim();
//				updateHireClaim.setHirecode(tblHireclaim.getHirecode());
                updateHireClaim.setAccdate(formatter.parse(updateHireRequest.getAccdate()));
                updateHireClaim.setAcctime(updateHireRequest.getAcctime());
                updateHireClaim.setAddress1(updateHireRequest.getAddress1());
                updateHireClaim.setAddress2(updateHireRequest.getAddress2());
                updateHireClaim.setAddress3(updateHireRequest.getAddress3());


                TblCircumstance tblCircumstance = new TblCircumstance();
                if (updateHireRequest.getCircumcode() != null) {
                    tblCircumstance.setCircumcode(Long.valueOf(updateHireRequest.getCircumcode()));
                    updateHireClaim.setCircumcode(tblCircumstance);
                } else {
                    updateHireClaim.setCircumcode(null);
                }

                updateHireClaim.setCity(updateHireRequest.getCity());
                updateHireClaim.setClaimrefno(updateHireRequest.getClaimrefno());
                updateHireClaim.setContactdue(updateHireRequest.getContactdue() == null ? null
                        : formatter.parse(updateHireRequest.getContactdue()));
                updateHireClaim.setDescription(updateHireRequest.getDescription());
                updateHireClaim.setDob(formatter.parse(updateHireRequest.getDob()));
                updateHireClaim.setDriverpassenger(updateHireRequest.getDriverpassenger());
                updateHireClaim.setEmail(updateHireRequest.getEmail());
                updateHireClaim.setFirstname(updateHireRequest.getFirstname());
                updateHireClaim.setGreencardno(updateHireRequest.getGreencardno());
                updateHireClaim.setLastupdated(new Date());


                TblInjclass tblInjclass = new TblInjclass();
                if (updateHireRequest.getInjclasscode() != null) {
                    tblInjclass.setInjclasscode(Long.valueOf(updateHireRequest.getInjclasscode()));
                    updateHireClaim.setTblInjclass(tblInjclass);
                } else {
                    updateHireClaim.setTblInjclass(null);
                }

                updateHireClaim.setInjdescription(updateHireRequest.getInjdescription());
                updateHireClaim.setInjlength(updateHireRequest.getInjlength());
                updateHireClaim.setInstorage(updateHireRequest.getInstorage());
                updateHireClaim.setInsurer(updateHireRequest.getInsurer());
                updateHireClaim.setLandline(updateHireRequest.getLandline());
                updateHireClaim.setLastname(updateHireRequest.getLastname());
                updateHireClaim.setLocation(updateHireRequest.getLocation());
                updateHireClaim.setMakemodel(updateHireRequest.getMakemodel());
                updateHireClaim.setMedicalinfo(updateHireRequest.getMedicalinfo());
                updateHireClaim.setMiddlename(updateHireRequest.getMiddlename());
                updateHireClaim.setMobile(updateHireRequest.getMobile());
                updateHireClaim.setNinumber(updateHireRequest.getNinumber());
                updateHireClaim.setOngoing(updateHireRequest.getOngoing());
                updateHireClaim.setPartyaddress(updateHireRequest.getPartyaddress());
                updateHireClaim.setPartycontactno(updateHireRequest.getPartycontactno());
                updateHireClaim.setPartyinsurer(updateHireRequest.getPartyinsurer());
                updateHireClaim.setPartymakemodel(updateHireRequest.getPartymakemodel());
                updateHireClaim.setPartyname(updateHireRequest.getPartyname());
                updateHireClaim.setPartypolicyno(updateHireRequest.getPartypolicyno());
                updateHireClaim.setPartyrefno(updateHireRequest.getPartyrefno());
                updateHireClaim.setPartyregno(updateHireRequest.getPartyregno());
                updateHireClaim.setPassengerinfo(updateHireRequest.getPassengerinfo());
                updateHireClaim.setPolicycover(updateHireRequest.getPolicycover());
                updateHireClaim.setPolicyholder(updateHireRequest.getPolicyholder());
                updateHireClaim.setPolicyno(updateHireRequest.getPolicyno());
                updateHireClaim.setPostalcode(updateHireRequest.getPostalcode());
                updateHireClaim.setRdweathercond(updateHireRequest.getRdweathercond());
                updateHireClaim.setRecovered(updateHireRequest.getRecovered());
                updateHireClaim.setRecoveredby(updateHireRequest.getRecoveredby());
                updateHireClaim.setRefno(updateHireRequest.getRefno());
                updateHireClaim.setRegion(updateHireRequest.getRegion());
                updateHireClaim.setRegisterationno(updateHireRequest.getRegisterationno());
                updateHireClaim.setReportedtopolice(updateHireRequest.getReportedtopolice());
                updateHireClaim.setScotland(updateHireRequest.getScotland());
                updateHireClaim.setStorage(updateHireRequest.getStorage());
                updateHireClaim.setTitle(updateHireRequest.getTitle());
                updateHireClaim.setUsercode(tblUser.getUsercode());
                updateHireClaim.setVehiclecondition(updateHireRequest.getVehiclecondition());
                updateHireClaim.setLastupdateuser(tblUser.getUsercode());

                TblHireclaim hireclaim = hireService.updateHireCase (updateHireClaim);
                List<TblHirebusiness> tblHirebusinesses = hireService.getHireBusinessesAgainstHireCode(hireclaim.getHirecode());
                if(tblHirebusinesses != null && tblHirebusinesses.size() > 0){
                    int update = hireService.updateHireBuisnessDetails(updateHireRequest.getUpdateHireBuisnessDetail());

                }

                if (hireclaim != null && hireclaim.getHirecode() > 0) {
                    LOG.info("\n EXITING THIS METHOD == addNewHireCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Updated SuccessFully", hireclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Saving Updated Case", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == updateRtaCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addHireDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> addHireDocument(@RequestParam("hireClaimCode") String hireClaimCode, @RequestParam("multipartFiles") List<MultipartFile> multipartFiles,
                                                                   HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == addHireDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblHireclaim hireclaim = hireService.getTblHireClaimById(Long.valueOf(hireClaimCode));
            if (hireclaim != null && hireclaim.getHirecode() > 0) {
                String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\hire\\";
                String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\hire\\";
                List<TblHiredocument> tblHiredocuments = new ArrayList<TblHiredocument>();

                File theDir = new File(UPLOADED_FOLDER + hireclaim.getHirecode());
                if (!theDir.exists()) {
                    theDir.mkdirs();
                }

                for (MultipartFile multipartFile : multipartFiles) {
                    byte[] bytes = multipartFile.getBytes();
                    Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                    Files.write(path, bytes);
                    TblHiredocument tblHiredocument = new TblHiredocument();
                    tblHiredocument.setDocurl(UPLOADED_SERVER + hireclaim.getHirecode() + "\\" + multipartFile.getOriginalFilename());
                    tblHiredocument.setDoctype(multipartFile.getContentType());
                    tblHiredocument.setDocname(multipartFile.getOriginalFilename());
                    tblHiredocument.setTblHireclaim(hireclaim);
                    tblHiredocument.setUsercode(tblUser.getUsercode());
                    tblHiredocument.setCreatedon(new Date());

                    tblHiredocuments.add(tblHiredocument);
                }

                tblHiredocuments = hireService.addHireDocument(tblHiredocuments);

                LOG.info("\n EXITING THIS METHOD == addHireDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Document Saved Successfully", null);
            } else {
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Find", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addHireNotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addHireNotes(@RequestBody HireNoteRequest addHireNotesRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == addHireNotes(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addHireNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = hireService.isHireCaseAllowed(tblUser.getCompanycode(), "2");

            if (allowed) {
                if (!addHireNotesRequest.getHireCode().isEmpty()) {
                    TblHirenote tblHirenote = new TblHirenote();
                    TblHireclaim tblHireclaim = new TblHireclaim();

                    tblHireclaim.setHirecode(Long.valueOf(addHireNotesRequest.getHireCode()));
                    tblHirenote.setTblHireclaim(tblHireclaim);
                    tblHirenote.setNote(addHireNotesRequest.getNote());
                    tblHirenote.setUsercategorycode(addHireNotesRequest.getUserCatCode());
                    tblHirenote.setCreatedon(new Date());
                    tblHirenote.setUsercode(tblUser.getUsercode());

                    tblHirenote = hireService.addTblHireNote(tblHirenote);

                    if (tblHirenote != null && tblHirenote.getHirenotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addHireNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblHirenote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addHireNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addHireNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addHireNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == addHireNotes();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addHireNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/assignCaseToBusiness", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> assignCaseToBusiness(@RequestBody AssignCaseToBusiness assignCaseToBusiness,
                                                                        HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == assignCaseToBusiness(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == assignCaseToBusiness(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (!assignCaseToBusiness.getHireclaimcode().isEmpty()) {
                TblHirebusiness tblHirebusiness = new TblHirebusiness();
                TblHireclaim tblHireclaim = new TblHireclaim();
                TblRtastatus tblRtastatus = new TblRtastatus();

                tblHireclaim.setHirecode(Long.valueOf(assignCaseToBusiness.getHireclaimcode()));
                tblRtastatus.setStatuscode(Long.valueOf(assignCaseToBusiness.getStatusCode()));
                tblHireclaim.setTblRtastatus(tblRtastatus);
                tblHirebusiness.setTblHireclaim(tblHireclaim);

                TblCompanyprofile tblCompanyprofile = new TblCompanyprofile();
                tblCompanyprofile.setCompanycode(assignCaseToBusiness.getCompanyprofilecode());
                tblHirebusiness.setTblCompanyprofile(tblCompanyprofile);

                TblUser user = new TblUser();
                user.setUsercode(assignCaseToBusiness.getUserCode());
                tblHirebusiness.setTblUser(user);

                tblHirebusiness.setTblHirebusinessdetails(null);
                tblHirebusiness.setNote(assignCaseToBusiness.getNote());
                tblHirebusiness.setMessage(assignCaseToBusiness.getMessage());
                tblHirebusiness.setStatus("P");
                tblHirebusiness.setStatususer(assignCaseToBusiness.getUserCode());
                tblHirebusiness.setCreatedon(new Date());
                tblHirebusiness.setCreatedby(tblUser.getUsercode());
                tblHirebusiness = hireService.addTblHireBusiness(tblHirebusiness);
                if (tblHirebusiness != null && tblHirebusiness.getHirebusinessescode() > 0) {

                    if (tblHirebusiness.getTblCompanyprofile().getCompanycode().equalsIgnoreCase("634")) {
                        LOG.info("\n\n Sending Email To pinnacle ");
                        SendCaseDataRequest sendCaseDataRequest = new SendCaseDataRequest();
                        sendCaseDataRequest.setCode(tblHireclaim.getHirecode());
                        String status = sendHireCaseOverTheEmail(sendCaseDataRequest, tblUser);
                        if(status.equalsIgnoreCase("1")){

                            TblHirenote tblHirenote = new TblHirenote();

                            tblHirenote.setTblHireclaim(tblHireclaim);
                            tblHirenote.setNote("Email Has Been Sent to Pinnacle.");
                            tblHirenote.setUsercategorycode("4");
                            tblHirenote.setCreatedon(new Date());
                            tblHirenote.setUsercode(tblUser.getUsercode());

                            tblHirenote = hireService.addTblHireNote(tblHirenote);


                            LOG.info("\n\n Email Sent To pinnacle ");
                        }else {
                            LOG.info("\n\n Error Sending Email To pinnacle ");
                        }
                    }


//                    TblHireclaim tblHireclaim1 = hireService.getHireCaseById(tblHirebusiness.getTblHireclaim().getHirecode(), tblUser);

                    if (tblHireclaim != null) {
                        LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Record Found", tblHireclaim);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == assignCaseToBusiness(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Hire Company", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == assignCaseToBusiness(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == assignCaseToBusiness();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == assignCaseToBusiness(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/changeHireBusinessUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> changeHireBusinessUser(@RequestBody ChangeHireBusinessUser changeHireBusinessUser,
                                                                          HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == changeHireBusinessUser(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == changeHireBusinessUser(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = hireService.isHireCaseAllowed(tblUser.getCompanycode(), "2");
            if (allowed) {
                if (!changeHireBusinessUser.getHireclaimcode().isEmpty() && !changeHireBusinessUser.getCompanyCode().isEmpty()) {
                    TblHirebusiness tblHirebusiness = hireService.getHireBusinessAgainstHireAndCompanyCode(Long.valueOf(changeHireBusinessUser.getHireclaimcode()),
                            changeHireBusinessUser.getCompanyCode());

                    TblUser user = new TblUser();
                    user.setUsercode(changeHireBusinessUser.getUserCode());
                    tblHirebusiness.setStatus("P");
                    tblHirebusiness.setTblUser(user);
                    tblHirebusiness.setStatususer(user.getUsercode());
                    tblHirebusiness.setMessage("User Changed SuccessFully");
                    tblHirebusiness = hireService.updateHirebusiness(tblHirebusiness);

                    if (tblHirebusiness != null && tblHirebusiness.getHirebusinessescode() > 0) {


                        TblHireclaim tblHireclaim1 = hireService.getHireCaseById(tblHirebusiness.getTblHireclaim().getHirecode(), tblUser);

                        if (tblHireclaim1 != null) {
                            LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                            return getResponseFormat(HttpStatus.OK, "User Change SuccessFull", tblHireclaim1);
                        } else {
                            LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Changing SuccessFull", null);
                        }
                    } else {
                        LOG.info("\n EXITING THIS METHOD == changeHireBusinessUser(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == changeHireBusinessUser(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == changeHireBusinessUser(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == changeHireBusinessUser();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == changeHireBusinessUser(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/changeHireBusinessStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> changeHireBusinessStatus(@RequestBody ChangeHireBusinessStatus changeHireBusinessStatus,
                                                                            HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == changeHireBusinessStatus(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == changeHireBusinessStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (changeHireBusinessStatus.getHireclaimcode() > 0) {
                TblHirebusiness tblHirebusiness = hireService.getHireBusinessAgainstHireAndCompanyCode(Long.valueOf(changeHireBusinessStatus.getHireclaimcode()),
                        changeHireBusinessStatus.getCompanyCode());
                if (tblHirebusiness != null && tblHirebusiness.getHirebusinessescode() > 0) {
//                    tblHirebusiness.setStatususer(tblUser.getUsercode());
                    tblHirebusiness.setNote(changeHireBusinessStatus.getNote());
                    tblHirebusiness.setStatus(changeHireBusinessStatus.getBussinessStatus());
                    tblHirebusiness = hireService.updateHirebusiness(tblHirebusiness);

                    if (changeHireBusinessStatus.getBussinessStatus().equals("A")) {
                        TblHirebusinessdetail tblHirebusinessdetail = new TblHirebusinessdetail();
                        tblHirebusinessdetail.setBookingdate(changeHireBusinessStatus.getHireBusinessDetailRequest().getBookingdate().isEmpty() ? null : formatter.parse(changeHireBusinessStatus.getHireBusinessDetailRequest().getBookingdate()));
                        tblHirebusinessdetail.setBookingmode(changeHireBusinessStatus.getHireBusinessDetailRequest().getBookingmode());
                        tblHirebusinessdetail.setCreatedby(tblUser.getUsercode());
                        tblHirebusinessdetail.setHireenddate(changeHireBusinessStatus.getHireBusinessDetailRequest().getHireenddate().isEmpty() ? null : formatter.parse(changeHireBusinessStatus.getHireBusinessDetailRequest().getHireenddate()));
                        tblHirebusinessdetail.setHirestartdate(changeHireBusinessStatus.getHireBusinessDetailRequest().getHirestartdate().isEmpty() ? null : formatter.parse(changeHireBusinessStatus.getHireBusinessDetailRequest().getHirestartdate()));
                        tblHirebusinessdetail.setOutsourced(changeHireBusinessStatus.getHireBusinessDetailRequest().getOutsourced());
                        tblHirebusinessdetail.setOutsourcedto(changeHireBusinessStatus.getHireBusinessDetailRequest().getOutsourcedto());
                        tblHirebusinessdetail.setReason(changeHireBusinessStatus.getHireBusinessDetailRequest().getReason());
                        tblHirebusinessdetail.setService(changeHireBusinessStatus.getHireBusinessDetailRequest().getService());
                        tblHirebusinessdetail.setTblHirebusiness(tblHirebusiness);

                        tblHirebusinessdetail = hireService.addTblHireBusinessDetail(tblHirebusinessdetail);
                    }


                    TblHireclaim hireclaim = hireService.getHireCaseById(Long.valueOf(changeHireBusinessStatus.getHireclaimcode()), tblUser);

                    String status = "";
                    if (changeHireBusinessStatus.getHireBusinessDetailRequest().getService().equals("HR") || changeHireBusinessStatus.getHireBusinessDetailRequest().getService().equals("H0")) {
                        status = "25";
                    } else if (changeHireBusinessStatus.getHireBusinessDetailRequest().getService().equals("VD")) {
                        status = "335";
                        // adding TASK to HIRE CASE ///
                        List<TblHiretask> tblHdrtasksList = hireService.getHireTaskAgainstHireCode(changeHireBusinessStatus.getHireclaimcode());
                        if (tblHdrtasksList.size() == 0) {
                            List<TblTask> tblTasks = hireService.getTasksForHire();
                            List<TblHiretask> tblHiretasks = new ArrayList<>();

                            for (TblTask tblTask : tblTasks) {
                                TblHiretask tblHiretask = new TblHiretask();
                                tblHiretask.setTblHireclaim(hireclaim);
                                tblHiretask.setTblTask(tblTask);
                                tblHiretask.setStatus("N");
                                tblHiretask.setCurrenttask("N");

                                tblHiretasks.add(tblHiretask);
                            }

                            tblHiretasks = hireService.saveHireTasks(tblHiretasks);
                        }
                        //////////////////////////////
                    } else {
                        status = "26";
                    }
                    //// for logging
                    long oldStatus = hireclaim.getTblRtastatus().getStatuscode();

                    TblRtastatus tblRtastatus = new TblRtastatus();
                    tblRtastatus.setStatuscode(Long.valueOf(status));

                    hireclaim.setTblRtastatus(tblRtastatus);
                    hireclaim = hireService.updateHireCase(hireclaim);

                    //// for logging
                    TblHirelog tblHirelog = new TblHirelog();
                    TblRtastatus tblStatus = hireService.getTblRtaStatus(Long.valueOf(status));
                    tblHirelog.setCreatedon(new Date());
                    tblHirelog.setDescr(tblStatus.getDescr());
                    tblHirelog.setUsercode(tblUser.getUsercode());
                    tblHirelog.setTblHireclaim(hireclaim);
                    tblHirelog.setNewstatus(Long.valueOf(status));
                    tblHirelog.setOldstatus(oldStatus);
                    tblHirelog = hireService.saveTblHireLogs(tblHirelog);

                    hireService.sendHireEmailToAll(hireclaim, tblHirebusiness);

                    TblHireclaim tblHireclaim1 = hireService.getHireCaseById(hireclaim.getHirecode(), tblUser);

                    if (tblHireclaim1 != null) {
                        LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Buisness Status Updated SuccessFully", tblHireclaim1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Updating Buisness Status", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == changeHireBusinessStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Hire Buisness Attach To The Case", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == changeHireBusinessStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == changeHireBusinessStatus();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == changeHireBusinessStatus(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnHire", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnHire(
            @RequestBody PerformActionOnHireRequest performActionOnHireRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == performActionOnHire(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHire(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                TblHireclaim tblHireclaim = null;

                if (performActionOnHireRequest.getToStatus().equals("38") || performActionOnHireRequest.getToStatus().equals("37")
                        || performActionOnHireRequest.getToStatus().equals("301") || performActionOnHireRequest.getToStatus().equals("302")
                        || performActionOnHireRequest.getToStatus().equals("300")) {
                    // FOR REJECTION AND CANCEL
                    tblHireclaim = hireService.performRejectOrCancelAction(performActionOnHireRequest.getHireclaimcode(),
                            performActionOnHireRequest.getToStatus(), performActionOnHireRequest.getReason(), tblUser);

                }else if (performActionOnHireRequest.getToStatus().equals("332")){
                    // revert status
                    tblHireclaim = hireService.performRevertActionOnHire(performActionOnHireRequest.getHireclaimcode(),
                            performActionOnHireRequest.getToStatus(), performActionOnHireRequest.getReason(), tblUser);
                }
                else {
                    // FOR NORMAL STATUS CHANGE
                    tblHireclaim = hireService.performActionOnHire(performActionOnHireRequest.getHireclaimcode(),
                            performActionOnHireRequest.getToStatus(), tblUser);
                }


                if (tblHireclaim != null) {
                    TblHireclaim tblHireclaim1 = hireService.getHireCaseById(tblHireclaim.getHirecode(), tblUser);

                    if (tblHireclaim1 != null) {
                        LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Action Performed SuccessFully", tblHireclaim1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == getHireCaseById(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Action", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHire(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHire(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == performActionOnHire();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHire(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/copyHireToRta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> copyHireToRta(
            @RequestBody CopyCaseRequest hireCode, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == copyHireToRta(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == copyHireToRta(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                ViewRtaclamin viewRtaclamin = hireService.copyHireToRta(hireCode.getHireCode());
                if (viewRtaclamin != null) {
                    LOG.info("\n EXITING THIS METHOD == copyHireToRta(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case copied successfully", viewRtaclamin);
                } else {
                    LOG.info("\n EXITING THIS METHOD == copyHireToRta(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Unable to copy case. Please try again", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == copyHireToRta(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == copyHireToRta();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == copyHireToRta(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/changeBusinessStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> changeBusinessStatus(@RequestBody ChangeBusinessStatus changeBusinessStatus,
                                                                        HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == changeBusinessStatus(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == changeHireBusinessStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (changeBusinessStatus.getHireclaimcode() > 0) {
                TblHirebusiness tblHirebusiness = hireService.getHireBusinessAgainstHireAndCompanyCode(Long.valueOf(changeBusinessStatus.getHireclaimcode()),
                        changeBusinessStatus.getCompanyCode());

                if (tblHirebusiness != null && tblHirebusiness.getHirebusinessescode() > 0) {
                    tblHirebusiness.setStatususer(tblUser.getUsercode());
                    tblHirebusiness.setStatus(changeBusinessStatus.getStatus());
                    tblHirebusiness = hireService.updateHirebusiness(tblHirebusiness);

                    TblHireclaim hireclaim = hireService.getHireCaseById(Long.valueOf(changeBusinessStatus.getHireclaimcode()), tblUser);

                    LOG.info("\n EXITING THIS METHOD == changeHireBusinessStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Hire Buisness Status Changed Successfully", hireclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == changeHireBusinessStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Changing Hire Buisness Status", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == changeHireBusinessStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == changeHireBusinessStatus();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == changeHireBusinessStatus(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/copyHireToHire", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> copyHireToHire(@RequestBody CopyHireToHireRequest copyHireToHireRequest,
                                                                  HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == addNewHireCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = hireService.isHireCaseAllowed(tblUser.getCompanycode(), "2");

            if (allowed) {
                TblHireclaim saveHireRequest = hireService.getTblHireClaimById(copyHireToHireRequest.getHirecode());
                List<TblHiredocument> saveHiredocuments = hireService.getHireCasedocuments(copyHireToHireRequest.getHirecode());
                TblHireclaim tblHireclaim = new TblHireclaim();

                tblHireclaim.setAccdate(saveHireRequest.getAccdate());
                tblHireclaim.setAcctime(saveHireRequest.getAcctime());
                tblHireclaim.setAddress1(saveHireRequest.getAddress1());
                tblHireclaim.setAddress2(saveHireRequest.getAddress2());
                tblHireclaim.setAddress3(saveHireRequest.getAddress3());
                tblHireclaim.setCircumcode(saveHireRequest.getCircumcode());
                tblHireclaim.setCity(saveHireRequest.getCity());
                tblHireclaim.setClaimrefno(saveHireRequest.getClaimrefno());
                tblHireclaim.setContactdue(saveHireRequest.getContactdue());
                tblHireclaim.setCreatedon(new Date());
                tblHireclaim.setDescription(saveHireRequest.getDescription());
                tblHireclaim.setDob(saveHireRequest.getDob());
                tblHireclaim.setDriverpassenger(saveHireRequest.getDriverpassenger());
                tblHireclaim.setEmail(saveHireRequest.getEmail());
                tblHireclaim.setFirstname(saveHireRequest.getFirstname());
                tblHireclaim.setGreencardno(saveHireRequest.getGreencardno());
                tblHireclaim.setTblInjclass(saveHireRequest.getTblInjclass());
                tblHireclaim.setInjdescription(saveHireRequest.getInjdescription());
                tblHireclaim.setInjlength(saveHireRequest.getInjlength());
                tblHireclaim.setInstorage(saveHireRequest.getInstorage());
                tblHireclaim.setInsurer(saveHireRequest.getInsurer());
                tblHireclaim.setLandline(saveHireRequest.getLandline());
                tblHireclaim.setLastname(saveHireRequest.getLastname());
                tblHireclaim.setLocation(saveHireRequest.getLocation());
                tblHireclaim.setMakemodel(saveHireRequest.getMakemodel());
                tblHireclaim.setMedicalinfo(saveHireRequest.getMedicalinfo());
                tblHireclaim.setMiddlename(saveHireRequest.getMiddlename());
                tblHireclaim.setMobile(saveHireRequest.getMobile());
                tblHireclaim.setNinumber(saveHireRequest.getNinumber());
//                tblHireclaim.setOngoing(saveHireRequest.getOngoing());
                tblHireclaim.setPartyaddress(saveHireRequest.getPartyaddress());
                tblHireclaim.setPartycontactno(saveHireRequest.getPartycontactno());
                tblHireclaim.setPartyinsurer(saveHireRequest.getPartyinsurer());
                tblHireclaim.setPartymakemodel(saveHireRequest.getPartymakemodel());
                tblHireclaim.setPartyname(saveHireRequest.getPartyname());
                tblHireclaim.setPartypolicyno(saveHireRequest.getPartypolicyno());
                tblHireclaim.setPartyrefno(saveHireRequest.getPartyrefno());
                tblHireclaim.setPartyregno(saveHireRequest.getPartyregno());
                tblHireclaim.setPassengerinfo(saveHireRequest.getPassengerinfo());
                tblHireclaim.setPolicycover(saveHireRequest.getPolicycover());
                tblHireclaim.setPolicyholder(saveHireRequest.getPolicyholder());
                tblHireclaim.setPolicyno(saveHireRequest.getPolicyno());
                tblHireclaim.setPostalcode(saveHireRequest.getPostalcode());
                tblHireclaim.setRdweathercond(saveHireRequest.getRdweathercond());
                tblHireclaim.setRecovered(saveHireRequest.getRecovered());
                tblHireclaim.setRecoveredby(saveHireRequest.getRecoveredby());
                tblHireclaim.setRefno(saveHireRequest.getRefno());
                tblHireclaim.setRegion(saveHireRequest.getRegion());
                tblHireclaim.setRegisterationno(saveHireRequest.getRegisterationno());
                tblHireclaim.setReportedtopolice(saveHireRequest.getReportedtopolice());
                tblHireclaim.setScotland("");
                tblHireclaim.setStorage(saveHireRequest.getStorage());
                tblHireclaim.setTitle(saveHireRequest.getTitle());
                tblHireclaim.setUsercode(tblUser.getUsercode());
                tblHireclaim.setVehiclecondition(saveHireRequest.getVehiclecondition());
                tblHireclaim.setVehicledamage(saveHireRequest.getVehicledamage());
                tblHireclaim.setAdvisor(Long.valueOf(saveHireRequest.getAdvisor()));
                tblHireclaim.setIntroducer(Long.valueOf(saveHireRequest.getIntroducer()));

                TblRtastatus tblRtastatus = new TblRtastatus();
                tblRtastatus.setStatuscode(31);

                tblHireclaim.setTblRtastatus(tblRtastatus);

                TblCompanyprofile tblCompanyprofile = hireService.getCompanyProfile(String.valueOf(tblHireclaim.getIntroducer()));
//				int hireDocNumber = hireService.getHireDocNumber();
                tblHireclaim.setHirenumber(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());


                TblHireclaim hireclaim = hireService.addNewHireCase(tblHireclaim, null);


                List<TblHiredocument> tblHiredocuments = new ArrayList<>();
                for (TblHiredocument saveHiredocument : saveHiredocuments) {
                    TblHiredocument tblHiredocument = new TblHiredocument();
                    tblHiredocument.setDocurl(saveHiredocument.getDocurl());
                    tblHiredocument.setDoctype(saveHiredocument.getDoctype());
                    tblHiredocument.setDocname(saveHiredocument.getDocname());
                    tblHiredocument.setTblHireclaim(hireclaim);
                    tblHiredocument.setUsercode(tblUser.getUsercode());
                    tblHiredocument.setCreatedon(new Date());

                    tblHiredocuments.add(tblHiredocument);
                }
                tblHiredocuments = hireService.addHireDocument(tblHiredocuments);

                if (hireclaim != null && hireclaim.getHirecode() > 0) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = hireService.saveCompanyProfile(tblCompanyprofile);

                    LOG.info("\n EXITING THIS METHOD == addNewHireCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", hireclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Saving Hire Case", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == addNewRtaCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/hireCaseReport", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> hireCaseReport(
            @RequestBody HdrCaseReportRequest hireCaseReportRequest, HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == hireCaseReport(); ");
        try {

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == hireCaseReport(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<ViewHirecasereport> rtaCaseReport = hireService.hireCaseReport(hireCaseReportRequest);

                if (rtaCaseReport != null) {
                    LOG.info("\n EXITING THIS METHOD == hireCaseReport(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaCaseReport);
                } else {
                    LOG.info("\n EXITING THIS METHOD == hireCaseReport(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == hireCaseReport(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == hireCaseReport();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == hireCaseReport(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/filterHireCases", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> filterHireCases(@RequestBody FilterRequest filterRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == filterHireCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == filterHireCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = hireService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<HireCaseList> immigrationCaseLists = hireService.getFilterAuthHireCasesIntroducers(filterRequest, tblUser.getUsercode());
                if (immigrationCaseLists != null && immigrationCaseLists.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == filterHireCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", immigrationCaseLists);
                } else {
                    LOG.info("\n EXITING THIS METHOD == filterHireCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<HireCaseList> viewHdrCaseslist = hireService.getFilterAuthHireCasesSolicitors(filterRequest, tblUser.getUsercode());
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == filterHireCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == filterHireCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<HireCaseList> viewHdrCaseslist = hireService.getFilterAuthHireCasesLegalAssist(filterRequest);
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == filterHireCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == filterHireCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == filterHireCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == filterHireCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == filterHireCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("hireClaimCode") long hireClaimCode, @RequestParam("taskCode") long taskCode, @RequestParam(required = false) List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblHireclaim tblHireclaim = hireService.getTblHireClaimById(hireClaimCode);
            List<TblHirebusiness> tblHirebusiness = hireService.getHireBusinessByStatus(hireClaimCode, "A");
            TblCompanyprofile companyprofile = tblHirebusiness.get(0).getTblCompanyprofile();
            if (tblHireclaim != null && tblHireclaim.getHirecode() > 0) {
                TblTask tblTask = hireService.getTaskAgainstCode(taskCode);
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblHiretask tblHiretask = hireService.getHireTaskByHireCodeAndTaskCode(tblHireclaim.getHirecode(), taskCode);
                    String emailUrl = getDataFromProperties("esign.baseurl") + "hire=" + tblHireclaim.getHirecode();
                    if (tblTask.getTaskcode() == 7) {

                        TblCompanydoc tblCompanydoc = hireService.getHireCompanyDocs(companyprofile.getCompanycode(), "2");
                        if (tblCompanydoc == null) {
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "There is No CFA Attach To the Solicitor", null);
                        }


                        if (tblHireclaim.getEmail() != null) {

                            TblEmailTemplate tblEmailTemplate = getEmailTemplateByType("esign");

                            String body = tblEmailTemplate.getEmailtemplate();
                            body = body.replace("[LEGAL_CLIENT_NAME]", tblHireclaim.getFirstname() + " "
                                    + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename()) + " " + tblHireclaim.getLastname());
                            body = body.replace("[SOLICITOR_COMPANY_NAME]", companyprofile.getName());
                            body = body.replace("[ESIGN_URL]", emailUrl);

                            hireService.saveEmail(tblHireclaim.getEmail(), body, "No Win No Fee Agreement", tblHireclaim, null);
                            tblHiretask.setRemarks("Email Sent Successfully to Client");
                        }

                        LOG.info("\n Sending SMS ");
                        if (tblHireclaim.getMobile() != null && !tblHireclaim.getMobile().isEmpty()) {
                            LOG.info("\n SMS SEND");
                            TblEmailTemplate tblEmailTemplateSMS = getEmailTemplateByType("esign-SMS");
                            String message = tblEmailTemplateSMS.getEmailtemplate();
                            message = message.replace("[LEGAL_CLIENT_NAME]", tblHireclaim.getFirstname() + " "
                                    + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename()) + " " + tblHireclaim.getLastname());
                            message = message.replace("[SOLICITOR_COMPANY_NAME]", companyprofile.getName());
                            message = message.replace("[ESIGN_URL]", emailUrl);


                            sendSMS(tblHireclaim.getMobile(), message);
                            tblHiretask.setRemarks("SMS Sent Successfully to Client");
                        }
                        tblHiretask.setStatus("P");

                        saveEmailSmsEsignStatus(String.valueOf(tblHireclaim.getHirecode()), "2", tblUser.getUsercode());

                    } else {
                        String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\hire\\";
                        String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\hire\\";

                        File theDir = new File(UPLOADED_FOLDER + tblHireclaim.getHirecode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }
                        List<TblHiredocument> tblHiredocuments = new ArrayList<TblHiredocument>();
                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);

                            TblHiredocument tblHiredocument = new TblHiredocument();
                            tblHiredocument.setDocurl(UPLOADED_SERVER + tblHireclaim.getHirecode() + "\\" + multipartFile.getOriginalFilename());
                            tblHiredocument.setDoctype(multipartFile.getContentType());
                            tblHiredocument.setDocname(multipartFile.getName());
                            tblHiredocument.setTblHireclaim(tblHireclaim);
                            tblHiredocument.setUsercode(tblUser.getUsercode());
                            tblHiredocument.setCreatedon(new Date());
                            tblHiredocument.setTblTask(tblTask);

                            tblHiredocuments.add(tblHiredocument);
                        }

                        tblHiredocuments = hireService.addHireDocument(tblHiredocuments);
                        tblHiretask.setStatus("C");
                        tblHiretask.setRemarks("Completed");
                    }

                    TblHiretask tblHiretask1 = hireService.updateTblHireTask(tblHiretask);
                    if (tblHiretask1 != null && tblHiretask1.getHiretaskcode() > 0) {

                        List<TblHiretask> tblHiretasks = hireService.getHireTaskAgainstHireCode(tblHireclaim.getHirecode());
                        int count = 0;
                        for (TblHiretask hdrtask : tblHiretasks) {
                            if (hdrtask.getStatus().equals("C")) {
                                count++;
                            }
                        }

                        if (count == tblHiretasks.size()) {
//                      code anything here if all task are completed
                        }

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblHiretask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == addESign(); ");
        try {
            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("code")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }

            TblHireclaim tblHireclaim = hireService.getTblHireClaimById(addESign.getRtaCode());
            if (tblHireclaim != null && tblHireclaim.getHirecode() > 0) {

                if (tblHireclaim.getEsign().equals("Y")) {
                    List<TblHiredocument> tblHiredocuments = hireService.getHireCasedocuments(tblHireclaim.getHirecode());
                    for (TblHiredocument tblRtadocument : tblHiredocuments) {
                        if (tblRtadocument.getDoctype().equalsIgnoreCase("Esign")) {
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("signedDocument", tblRtadocument.getDocbase64());
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.OK, "You Have Already Signed The Document",
                                    map);
                        }
                    }

                }
                // solicitor company
                List<TblHirebusiness> tblHirebusinesses = hireService.getHireBusinessByStatus(tblHireclaim.getHirecode(), "A");
                TblCompanyprofile tblCompanyprofile = tblHirebusinesses.get(0).getTblCompanyprofile();
                if (tblCompanyprofile != null) {

                    TblCompanydoc tblCompanydoc = hireService.getHireCompanyDocs(tblCompanyprofile.getCompanycode(), "2");

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                    byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                    ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                    BufferedImage image1 = ImageIO.read(bis);
                    bis.close();


                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();

                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            while (keys.hasNext()) {
                                String key = keys.next();
                                if (!key.equals("code") && !key.equals("eSign")) {
                                    if (fieldName.equals(key)) {
                                        textField.setValue(jsonObject.getString(key));
                                    }
                                }
                            }

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = hireService.getHireDbColumnValue(fieldName.replace("DB_", ""),
                                        tblHireclaim.getHirecode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            }
                            if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    for (int a = 0; a < document.getNumberOfPages(); a++) {
                        PDPage p = document.getPage(a);
                        PDResources resources = p.getResources();
                        for (COSName xObjectName : resources.getXObjectNames()) {
                            PDXObject xObject = resources.getXObject(xObjectName);
                            if (xObject instanceof PDImageXObject) {
                                PDImageXObject original_img = ((PDImageXObject) xObject);
                                PDImageXObject pdImageXObject = LosslessFactory.createFromImage(document, image1);
                                resources.put(xObjectName, pdImageXObject);
                            }
                        }
                    }

                    LOG.info("\n Esign Document Prepared SuccessFully\n");
                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\hire\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\hire\\";

                    File theDir = new File(UPLOADED_FOLDER + tblHireclaim.getHirecode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblHireclaim.getHirenumber() + "_Signed.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblHireclaim.getHirenumber() + "_Signed.pdf");
                    }

                    tblHireclaim.setEsign("Y");
                    tblHireclaim = hireService.updateHireCase(tblHireclaim);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    LOG.info("\n Esign Saved on folder SuccessFully SuccessFully\n");
                    File file = new File(theDir.getPath() + "\\" + tblHireclaim.getHirenumber() + "_Signed.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    TblTask tblHiretaskdocument = new TblTask();
                    tblHiretaskdocument.setTaskcode(7);

                    TblHiredocument tblHiredocument = new TblHiredocument();
                    tblHiredocument.setTblHireclaim(tblHireclaim);
                    tblHiredocument.setDocbase64(encoded);
                    tblHiredocument.setDocname("Esign");
                    tblHiredocument.setDoctype("Esign");
                    tblHiredocument.setCreatedon(new Date());
                    tblHiredocument.setUsercode("1");
                    tblHiredocument.setDocurl(
                            UPLOADED_SERVER + tblHireclaim.getHirecode() + "\\" + tblHireclaim.getHirenumber() + "_Signed.pdf");
                    tblHiredocument.setTblTask(tblHiretaskdocument);

                    tblHiredocument = hireService.addHireDocumentSingle(tblHiredocument);
                    LOG.info("\n saving doc in database\n");
                    if (tblHiredocument != null && tblHiredocument.getTblHireclaim().getHirecode() > 0) {
//                    Locale.setDefault(backup);


                        TblHiretask tblRtatask = new TblHiretask();
                        tblRtatask.setHiretaskcode(1);
                        tblRtatask.setTblHireclaim(tblHireclaim);
                        tblRtatask.setStatus("C");
                        tblRtatask.setRemarks("Completed");
                        tblRtatask.setCompletedon(new Date());


                        tblRtatask = hireService.updateTblHireTask(tblRtatask);


                        HashMap<String, Object> map = new HashMap<>();
                        map.put("doc", encoded);
                        TblUser legalUser = rtaService.findByUserId("4");

                        // cleint Email
                        TblEmailTemplate tblEmailTemplate = rtaService.findByEmailType("esign-clnt-complete");
                        String clientbody = tblEmailTemplate.getEmailtemplate();

                        clientbody = clientbody.replace("[LEGAL_CLIENT_NAME]",
                                tblHireclaim.getFirstname() + " "
                                        + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ")
                                        + tblHireclaim.getLastname());
                        clientbody = clientbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());

                        hireService.saveEmail(tblHireclaim.getEmail(), clientbody, "No Win No Fee Agreement ", tblHireclaim, legalUser, file.getPath());
                        LOG.info("\n Email Sent To client\n");
                        // Introducer Email
                        tblEmailTemplate = rtaService.findByEmailType("esign-Int-sol-complete");
                        String Introducerbody = tblEmailTemplate.getEmailtemplate();
                        TblUser introducerUser = rtaService.findByUserId(String.valueOf(tblHireclaim.getAdvisor()));

                        Introducerbody = Introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
                        Introducerbody = Introducerbody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
                        Introducerbody = Introducerbody.replace("[CLIENT_NAME]",
                                tblHireclaim.getFirstname() + " "
                                        + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ")
                                        + tblHireclaim.getLastname());
                        Introducerbody = Introducerbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        Introducerbody = Introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());


                        hireService.saveEmail(introducerUser.getUsername(), Introducerbody, "Esign", tblHireclaim, introducerUser);
                        LOG.info("\n Email Sent to intro\n");
                        // PI department Email
                        String LegalHiredeptbody = tblEmailTemplate.getEmailtemplate();

                        LegalHiredeptbody = LegalHiredeptbody.replace("[USER_NAME]", legalUser.getLoginid());
                        LegalHiredeptbody = LegalHiredeptbody.replace("[CASE_NUMBER]", tblHireclaim.getHirenumber());
                        LegalHiredeptbody = LegalHiredeptbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        LegalHiredeptbody = LegalHiredeptbody.replace("[CLIENT_NAME]",
                                tblHireclaim.getFirstname() + " "
                                        + (tblHireclaim.getMiddlename() == null ? "" : tblHireclaim.getMiddlename() + " ")
                                        + tblHireclaim.getLastname());
                        LegalHiredeptbody = LegalHiredeptbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hire") + tblHireclaim.getHirecode());

                        hireService.saveEmail(legalUser.getUsername(), LegalHiredeptbody, "Esign", tblHireclaim, legalUser);

                        saveEsignSubmitStatus(String.valueOf(tblHireclaim.getHirecode()), "2", request);


                        LOG.info("\n Email Sent to Dept\n");
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "You Have SuccessFully Signed The CFA, A Copy Of That CFA Has Been Sent To The Provided Email.", map);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST,
                                "Error While Saving The Document..Esign Compeleted", null);
                    }

                } else {
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No document templaate found against provided Campaign",
                            null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == HirePostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HirePostApi \n METHOD == getESignFields(); ");
        try {

            TblHireclaim tblHireclaim = hireService.getTblHireClaimById(Long.valueOf(eSignFieldsRequest.getCode()));

            if (tblHireclaim != null && tblHireclaim.getHirecode() > 0) {
                List<TblHirebusiness> tblHirebusinesses = hireService.getHireBusinessByStatus(tblHireclaim.getHirecode(), "A");
                TblCompanyprofile tblCompanyprofile = tblHirebusinesses.get(0).getTblCompanyprofile();
                if (!tblHireclaim.getEsign().equals("Y")) {

                    TblCompanydoc tblCompanydoc = hireService.getHireCompanyDocs(tblCompanyprofile.getCompanycode(), "2");
                    if (tblCompanydoc == null) {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "No CFA document found", null);
                    }
                    HashMap<String, String> fields = new HashMap<>();
                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();


                    /////////////////////// FILL the CFA With Client DATA ////////////////////////


                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = hireService.getHireDbColumnValue(fieldName.replace("DB_", ""),
                                        tblHireclaim.getHirecode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            } else if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            } else {
                                fields.put(fieldName, fieldName);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    /////////////////////// END OF FILL the CFA With Client DATA /////////////////

                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\hire\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\hire\\";

                    File theDir = new File(UPLOADED_FOLDER + tblHireclaim.getHirecode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblHireclaim.getHirenumber() + "_view.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblHireclaim.getHirenumber() + "_view.pdf");
                    }

                    File file = new File(theDir.getPath() + "\\" + tblHireclaim.getHirenumber() + "_view.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    fields.put("doc", encoded);
                    fields.put("isEsign", "N");
                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");

                    saveEsignOpenStatus(String.valueOf(tblHireclaim.getHirecode()), "2", request);

                    return getResponseFormat(HttpStatus.OK, "Success", fields);
                } else {
                    List<TblHiredocument> tblHiredocuments = hireService.getHireCasedocuments(tblHireclaim.getHirecode());
                    if (tblHiredocuments != null && tblHiredocuments.size() > 0) {
                        for (TblHiredocument tblHiredocument : tblHiredocuments) {
                            if (tblHiredocument.getTblTask() != null && tblHiredocument.getTblTask().getTaskcode() == 7) {
                                HashMap<String, String> fields = new HashMap<>();
                                fields.put("doc", tblHiredocument.getDocbase64());
                                fields.put("isEsign", "Y");
                                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                                return getResponseFormat(HttpStatus.OK, "You have Already Signed The Document.", fields);
                            }
                        }
                    }
                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Signed Document Found Please Contact Our HelpLine : please call us at 01615374448", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HirePostApi \n METHOD == getESignFields();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    public String sendHireCaseOverTheEmail(SendCaseDataRequest sendCaseDataRequest, TblUser tblUser) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == sendHireCaseOverTheEmail(); ");
            TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(tblUser.getCompanycode());

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            TblHireclaim tblHireclaim = hireService.getHireCaseById(sendCaseDataRequest.getCode(), tblUser);
            List<TblHiredocument> tblHiredocuments = hireService.getHireCasedocuments(tblHireclaim.getHirecode());

            ClaimsCNFexport claimsCNFexport = new ClaimsCNFexport();

            claimsCNFexport.getData().setSourceRef(tblHireclaim.getHirenumber());
            claimsCNFexport.getData().setFileHandler("2");
            claimsCNFexport.getData().setCreditControl("2");
            claimsCNFexport.getData().setReferrer("2");
            claimsCNFexport.getData().getClaimantDetails().setClaimantTitle(tblHireclaim.getTitle());
            claimsCNFexport.getData().getClaimantDetails().setClaimantForename(tblHireclaim.getFirstname());
            claimsCNFexport.getData().getClaimantDetails().setClaimantSurname(tblHireclaim.getLastname());
            claimsCNFexport.getData().getClaimantDetails().setClaimantAddress(tblHireclaim.getAddress1());
            claimsCNFexport.getData().getClaimantDetails().setClaimantAddress2(tblHireclaim.getAddress2());
            claimsCNFexport.getData().getClaimantDetails().setClaimantAddress3(tblHireclaim.getAddress3());
            claimsCNFexport.getData().getClaimantDetails().setClaimantTown(tblHireclaim.getCity());
            claimsCNFexport.getData().getClaimantDetails().setClaimantPostCode(tblHireclaim.getPostalcode());
            claimsCNFexport.getData().getClaimantDetails().setClaimantDOB(formatter.format(tblHireclaim.getDob()));
            claimsCNFexport.getData().getClaimantDetails().setClaimantEmail(tblHireclaim.getEmail());
//        claimsCNFexport.getData().getClaimantDetails().setClaimantWas(viewRtaclamin.getDriverpassenger() == "D" ? "Driver" : "Passenger");
            claimsCNFexport.getData().getClaimantDetails().setClaimantNINumber(tblHireclaim.getNinumber());
            claimsCNFexport.getData().getClaimantDetails().setClaimantHomeTel(tblHireclaim.getLandline() == null ? "" : tblHireclaim.getLandline());
//        claimsCNFexport.getData().getClaimantDetails().setTotalPassenger(String.valueOf(viewRtaclamin.getPassengerRtaClaims().size()));
            claimsCNFexport.getData().getClaimantDetails().setClaimingVD("Yes");
            claimsCNFexport.getData().getClaimantDetails().setClModel(tblHireclaim.getMakemodel());
            claimsCNFexport.getData().getClaimantDetails().setClMake(tblHireclaim.getMakemodel());
            claimsCNFexport.getData().getClaimantDetails().setClaimantRegNo(tblHireclaim.getRegisterationno());
            claimsCNFexport.getData().getClaimantDetails().setInsurerName(tblHireclaim.getInsurer());
            claimsCNFexport.getData().getClaimantDetails().setClPolicyNumber(tblHireclaim.getPolicyno() == null ? "" : tblHireclaim.getPolicyno());

            claimsCNFexport.getData().getTpDetails().setTpForename(tblHireclaim.getPartyname());
            claimsCNFexport.getData().getTpDetails().setTpAddress(tblHireclaim.getPartyaddress());
            claimsCNFexport.getData().getTpDetails().setTpTelMain(tblHireclaim.getPartycontactno());
            claimsCNFexport.getData().getTpDetails().setTpMake(tblHireclaim.getPartymakemodel());
            claimsCNFexport.getData().getTpDetails().setTpModel(tblHireclaim.getPartymakemodel());
            claimsCNFexport.getData().getTpDetails().setTpModel(tblHireclaim.getPartymakemodel());
            claimsCNFexport.getData().getTpDetails().setTpVRN(tblHireclaim.getPartyregno());

            claimsCNFexport.getData().getAccidentDetails().setAccidentDate(formatter.format(tblHireclaim.getAccdate()));
            claimsCNFexport.getData().getAccidentDetails().setAccidentTime(tblHireclaim.getAcctime());
            claimsCNFexport.getData().getAccidentDetails().setAccidentDescription(tblHireclaim.getDescription());
            claimsCNFexport.getData().getAccidentDetails().setLocation(tblHireclaim.getLocation());
            claimsCNFexport.getData().getAccidentDetails().setReportedPolice(tblHireclaim.getReportedtopolice() == "Y" ? "Yes" : "No");

            claimsCNFexport.getData().getInjuryDetails().setNotes(tblHireclaim.getInjdescription());

            String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\hire\\";
            String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\hire\\";


            File theDir = new File(UPLOADED_FOLDER + tblHireclaim.getHirecode());
            if (!theDir.exists()) {
                theDir.mkdirs();
            }
            File file = new File(UPLOADED_FOLDER + tblHireclaim.getHirecode() + "\\" + "thirdpatyxml" + (new Date()).getTime() + ".xml");

            ObjectMapper xmlMapper = new XmlMapper();

            xmlMapper.writeValue(file, claimsCNFexport);

            LOG.info("\n XML For the case " + tblHireclaim.getHirenumber() + " has been created successfully now sending Email\n");


            TblEmail tblEmail = new TblEmail();

            tblEmail.setEmailsubject("HIRE CASE DATA FROM LEGAL ASSIST | CASE REF NO : " + tblHireclaim.getHirenumber());
            tblEmail.setEmailaddress("alex@flynetmedia.co.uk,ashley@pinnaclehire.co.uk");
            tblEmail.setCcemailaddress("ikram@flynetmedia.co.uk,safina@legalassistltd.co.uk,murtza@legalassist");
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setCreatedon(new Date());
            tblEmail.setEmailbody(" This is a test email for integration ");

            String attactments = file.getPath();
            if (tblHiredocuments != null && tblHiredocuments.size() > 0) {
                for (TblHiredocument tblRtadocument : tblHiredocuments) {
                    String filePath = UPLOADED_FOLDER + (tblRtadocument.getDocurl().split("\\\\hire")[1]);
                    attactments = attactments + "," + filePath;
                }
            }
            tblEmail.setEmailattachment(attactments);

            tblEmail = rtaService.saveTblEmail(tblEmail);


            LOG.info("\n EXITING THIS METHOD == sendHireCaseOverTheEmail(); \n\n\n");
            return "1";


        } catch (IOException e) {
            e.printStackTrace();
            return "0";
        }
    }

    @RequestMapping(value = "/copyRtaToRta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> copyRtaToRta(@RequestBody CopyRtaToRtaRequest copyRtaToRtaRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == copyRtaToRta(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == copyRtaToRta(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblRtaclaim tblRtaclaimMain = rtaService
                    .getTblRtaclaimById(Long.parseLong(copyRtaToRtaRequest.getRtacode()));
            List<TblRtadocument> tblRtadocuments = rtaService.getAuthRtaCasedocuments(copyRtaToRtaRequest.getRtacode());

            TblRtaclaim tblRtaclaim = new TblRtaclaim();
            tblRtaclaim.setAccdate(tblRtaclaimMain.getAccdate());
            tblRtaclaim.setAcctime(tblRtaclaimMain.getAcctime());
            tblRtaclaim.setAddress1(tblRtaclaimMain.getAddress1());
            tblRtaclaim.setAddress2(tblRtaclaimMain.getAddress2());
            tblRtaclaim.setAddress3(tblRtaclaimMain.getAddress3());
            tblRtaclaim.setCity(tblRtaclaimMain.getCity());
            tblRtaclaim.setContactdue(tblRtaclaimMain.getContactdue() == null ? null : tblRtaclaimMain.getContactdue());
            tblRtaclaim.setCreatedon(new Date());
            tblRtaclaim.setDescription(tblRtaclaimMain.getDescription());
            tblRtaclaim.setDob(tblRtaclaimMain.getDob());
            tblRtaclaim.setDriverpassenger(tblRtaclaimMain.getDriverpassenger());
            tblRtaclaim.setEmail(tblRtaclaimMain.getEmail());
            tblRtaclaim.setEnglishlevel(tblRtaclaimMain.getEnglishlevel());
            tblRtaclaim.setFirstname(tblRtaclaimMain.getFirstname());
            tblRtaclaim.setGreencardno(tblRtaclaimMain.getGreencardno());
            tblRtaclaim.setInjdescription(tblRtaclaimMain.getInjdescription());
            tblRtaclaim.setInsurer(tblRtaclaimMain.getInsurer());
            tblRtaclaim.setLandline(tblRtaclaimMain.getLandline());
            tblRtaclaim.setLastname(tblRtaclaimMain.getLastname());
            tblRtaclaim.setLocation(tblRtaclaimMain.getLocation());
            tblRtaclaim.setMakemodel(tblRtaclaimMain.getMakemodel());
            tblRtaclaim.setMedicalinfo(tblRtaclaimMain.getMedicalinfo());
            tblRtaclaim.setMiddlename(tblRtaclaimMain.getMiddlename());
            tblRtaclaim.setMobile(tblRtaclaimMain.getMobile());
            tblRtaclaim.setNinumber(tblRtaclaimMain.getNinumber());
            tblRtaclaim.setOngoing(tblRtaclaimMain.getOngoing());
            tblRtaclaim.setPartyaddress(tblRtaclaimMain.getPartyaddress());
            tblRtaclaim.setPartycontactno(tblRtaclaimMain.getPartycontactno());
            tblRtaclaim.setPartyinsurer(tblRtaclaimMain.getPartyinsurer());
            tblRtaclaim.setPartymakemodel(tblRtaclaimMain.getPartymakemodel());
            tblRtaclaim.setPartyname(tblRtaclaimMain.getPartyname());
            tblRtaclaim.setPartypolicyno(tblRtaclaimMain.getPartypolicyno());
            tblRtaclaim.setPartyrefno(tblRtaclaimMain.getPartyrefno());
            tblRtaclaim.setPartyregno(tblRtaclaimMain.getPartyregno());
            tblRtaclaim.setPassengerinfo(tblRtaclaimMain.getPassengerinfo());
            tblRtaclaim.setPolicyno(tblRtaclaimMain.getPolicyno());
            tblRtaclaim.setPostalcode(tblRtaclaimMain.getPostalcode());
            tblRtaclaim.setRdweathercond(tblRtaclaimMain.getRdweathercond());
            tblRtaclaim.setRefno(tblRtaclaimMain.getRefno());
            tblRtaclaim.setRegion(tblRtaclaimMain.getRegion());
            tblRtaclaim.setRegisterationno(tblRtaclaimMain.getRegisterationno());
            tblRtaclaim.setReportedtopolice(tblRtaclaimMain.getReportedtopolice());
            tblRtaclaim.setEsig("N");
            tblRtaclaim.setScotland(tblRtaclaimMain.getScotland());
            tblRtaclaim.setPassword(tblRtaclaimMain.getPassword());
            tblRtaclaim.setTranslatordetails(tblRtaclaimMain.getTranslatordetails());
            tblRtaclaim.setAlternativenumber(tblRtaclaimMain.getAlternativenumber());
            tblRtaclaim.setMedicalevidence(tblRtaclaimMain.getMedicalevidence());
            tblRtaclaim.setReportedon(tblRtaclaimMain.getReportedon() == null ? null : tblRtaclaimMain.getReportedon());
            tblRtaclaim.setReferencenumber(tblRtaclaimMain.getReferencenumber());
            tblRtaclaim.setInjlength(tblRtaclaimMain.getInjlength() == null ? null : tblRtaclaimMain.getInjlength());

            tblRtaclaim.setGaddress1(tblRtaclaimMain.getGaddress1());
            tblRtaclaim.setGaddress2(tblRtaclaimMain.getGaddress2());
            tblRtaclaim.setGaddress3(tblRtaclaimMain.getGaddress3());
            tblRtaclaim.setGcity(tblRtaclaimMain.getGcity());
            tblRtaclaim.setGemail(tblRtaclaimMain.getGemail());
            tblRtaclaim.setGfirstname(tblRtaclaimMain.getGfirstname());
            tblRtaclaim.setGlandline(tblRtaclaimMain.getGlandline());
            tblRtaclaim.setGlastname(tblRtaclaimMain.getGlastname());
            tblRtaclaim.setGmiddlename(tblRtaclaimMain.getGmiddlename());
            tblRtaclaim.setGmobile(tblRtaclaimMain.getGmobile());
            tblRtaclaim.setGpostalcode(tblRtaclaimMain.getGpostalcode());
            tblRtaclaim.setGregion(tblRtaclaimMain.getGregion());
            tblRtaclaim.setGtitle(tblRtaclaimMain.getGtitle());
            tblRtaclaim.setGdob(tblRtaclaimMain.getGdob() == null ? null : tblRtaclaimMain.getGdob());

            tblRtaclaim.setYearofmanufacture(tblRtaclaimMain.getYearofmanufacture());
            tblRtaclaim.setTitle(tblRtaclaimMain.getTitle());
            tblRtaclaim.setVehiclecondition(tblRtaclaimMain.getVehiclecondition());
            tblRtaclaim.setOtherlanguages(tblRtaclaimMain.getOtherlanguages());
            tblRtaclaim.setAirbagopened(tblRtaclaimMain.getAirbagopened());
            tblRtaclaim.setVehicleType(tblRtaclaimMain.getVehicleType());
            tblRtaclaim.setMedicalevidence(tblRtaclaimMain.getMedicalevidence());
            tblRtaclaim.setVdImages(tblRtaclaimMain.getVdImages());
            tblRtaclaim.setInjurySustained(tblRtaclaimMain.getInjurySustained());

            TblCircumstance tblCircumstance = new TblCircumstance();
            List<TblRtainjury> tblRtainjuries = new ArrayList<>();

            tblRtaclaim.setTblCircumstance(tblRtaclaimMain.getTblCircumstance());
            tblRtaclaimMain.setInjclasscodes(rtaService.getInjuriesOfRta(tblRtaclaimMain.getRtacode()));
            if (tblRtaclaimMain.getInjclasscodes() != null) {
                TblRtainjury tblRtainjury;
                for (TblRtainjury injury : tblRtaclaimMain.getInjclasscodes()) {
                    tblRtainjury = new TblRtainjury();

                    tblRtainjury.setTblInjclass(injury.getTblInjclass());
                    tblRtainjuries.add(tblRtainjury);
                }
                tblRtaclaim.setInjclasscodes(tblRtainjuries);
            } else {
                tblRtaclaim.setTblInjclass(null);
            }

            tblRtaclaim.setIntroducer(tblRtaclaimMain.getIntroducer());
            tblRtaclaim.setAdvisor(tblRtaclaimMain.getAdvisor());

            TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(String.valueOf(tblRtaclaim.getIntroducer()));

            tblRtaclaim.setRtanumber(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());
            tblRtaclaim.setStatuscode("1");

            tblRtaclaim.setUsercode(tblUser.getUsercode());


            tblRtaclaim = rtaService.addNewRtaCase(tblRtaclaim, null, null);
            tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
            tblCompanyprofile = rtaService.saveCompanyProfile(tblCompanyprofile);

            List<TblRtadocument> tblRtadocumentsCopy = new ArrayList<>();
            for (TblRtadocument tblRtadocument : tblRtadocuments) {
                TblRtadocument tblRtadocument1 = new TblRtadocument();

                tblRtadocument.setDocname(tblRtadocument.getDocname());
                tblRtadocument.setDoctype("Image");
                tblRtadocument.setDocbase64(tblRtadocument.getDocbase64());
                tblRtadocument.setTblRtaclaim(tblRtaclaim);
                tblRtadocument.setCreatedon(new Date());
                tblRtadocument.setUsercode(tblRtaclaim.getUsercode());
                tblRtadocument.setTblTask(null);

                tblRtadocumentsCopy.add(tblRtadocument1);
            }
            tblRtadocuments = rtaService.addRtaDocument(tblRtadocumentsCopy);

            if (tblRtaclaim != null) {

                copyRtaToRtaRequest.setRtacode(String.valueOf(tblRtaclaim.getRtacode()));
                return getResponseFormat(HttpStatus.OK, "Case Copied To New Case SuccessFully", copyRtaToRtaRequest);
            } else {
                LOG.info("\n EXITING THIS METHOD == copyRtaToRta(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Copying A Case", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaPostApi \n METHOD == copyRtaToRta();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == copyRtaToRta(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
