package com.laportal.controller.pcp;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.PcpCaseList;
import com.laportal.dto.PcpStatusCountList;
import com.laportal.model.*;
import com.laportal.service.pcp.PcpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/Pcp")
public class PcpGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(PcpGetApi.class);

    @Autowired
    private PcpService pcpService;

    @RequestMapping(value = "/getPcpCases", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getPcpCases(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = pcpService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<PcpCaseList> viewHdrCaseslist = pcpService.getAuthPcpCasesIntroducers(tblUser.getUsercode());
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<PcpCaseList> viewHdrCaseslist = pcpService.getAuthPcpCasesSolicitors(tblUser.getUsercode());
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<PcpCaseList> viewHdrCaseslist = pcpService.getAuthPcpCasesLegalAssist();
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getPcpCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getPcpCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getPcpCaseById/{PcpCaseId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrCaseById(@PathVariable long PcpCaseId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCaseById(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblPcpclaim tblPcpclaim = pcpService.findPcpCaseById(PcpCaseId, tblUser);

            if (tblPcpclaim.getTblPcpsolicitors() != null) {
                TblCompanyprofile tblCompanyprofile = pcpService.getCompanyProfile(tblPcpclaim.getTblPcpsolicitors().get(0).getCompanycode());
                tblPcpclaim.getTblPcpsolicitors().get(0).setTblCompanyprofile(tblCompanyprofile);
            }

            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Record Found", tblPcpclaim);
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllPcpStatusCounts", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllPcpStatusCounts(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = pcpService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<PcpStatusCountList> pcpStatusCountsForIntroducers = pcpService.getAllPcpStatusCountsForIntroducers();

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", pcpStatusCountsForIntroducers);
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<PcpStatusCountList> pcpStatusCountsForSolicitor = pcpService.getAllPcpStatusCountsForSolicitor();

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", pcpStatusCountsForSolicitor);
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<PcpStatusCountList> allPcpStatusCounts = pcpService.getAllPcpStatusCounts();

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", allPcpStatusCounts);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getAllPcpStatusCounts();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllPcpStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getPcpCasesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getPcpCasesByStatus(@PathVariable long statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getPcpCasesByStatus(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<PcpCaseList> PcpCasesByStatus = pcpService.getPcpCasesByStatus(statusId);

            if (PcpCasesByStatus != null && PcpCasesByStatus.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getPcpCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", PcpCasesByStatus);
            } else {
                LOG.info("\n EXITING THIS METHOD == getPcpCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getPcpCaseLogs/{Pcpcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getPcpCaseLogs(@PathVariable String Pcpcode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getPcpCaseLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getPcpCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblPcplog> PcpCaseLogs = pcpService.getPcpCaseLogs(Pcpcode);

                if (PcpCaseLogs != null && PcpCaseLogs.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getPcpCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", PcpCaseLogs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getPcpCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getPcpCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getPcpCaseLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getPcpCaseLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getPcpCaseNotes/{Pcpcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getPcpCaseNotes(@PathVariable String Pcpcode,
                                                                   HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getPcpCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getPcpCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblPcpnote> tblPcpnotes = pcpService.getPcpCaseNotes(Pcpcode, tblUser);

                if (tblPcpnotes != null && tblPcpnotes.size() > 0) {
                    for (TblPcpnote tblPcpnote : tblPcpnotes
                    ) {
                        tblPcpnote.setTblPcpclaim(null);
                    }
                    LOG.info("\n EXITING THIS METHOD == getPcpCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblPcpnotes);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getPcpCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getPcpCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getPcpCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getPcpCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getPcpCaseMessages/{Pcpcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getPcpCaseMessages(@PathVariable String Pcpcode,
                                                                      HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getPcpCaseMessages(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblPcpmessage> tblPcpmessages = pcpService.getPcpCaseMessages(Pcpcode);

                if (tblPcpmessages != null && tblPcpmessages.size() > 0) {
                    for (TblPcpmessage tblPcpmessage : tblPcpmessages) {
                        tblPcpmessage.setTblPcpclaim(null);
                    }

                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblPcpmessages);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHdrCaseMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

}
