package com.laportal.controller.pcp;


import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.pcp.PcpService;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.exporting.PdfImageInfo;
import com.spire.pdf.fields.PdfField;
import com.spire.pdf.graphics.PdfImage;
import com.spire.pdf.widget.PdfFormFieldWidgetCollection;
import com.spire.pdf.widget.PdfFormWidget;
import com.spire.pdf.widget.PdfPageCollection;
import com.spire.pdf.widget.PdfTextBoxFieldWidget;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.geom.Dimension2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

@RestController
@RequestMapping("/Pcp")
public class PcpPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(PcpPostApi.class);

    @Autowired
    private PcpService pcpService;

    @RequestMapping(value = "/addNewPcpCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewHdrCase(@RequestBody SavePcpRequest savePcpRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addNewHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = pcpService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                TblPcpclaim tblPcpclaim = new TblPcpclaim();

                tblPcpclaim.setAddressOfClient(savePcpRequest.getAddressOfClient());
                tblPcpclaim.setBrokeraddress(savePcpRequest.getBrokeraddress());
                tblPcpclaim.setBrokername(savePcpRequest.getBrokername());
                tblPcpclaim.setCashDeposit(savePcpRequest.getCashDeposit());
                tblPcpclaim.setChargeForCredit(savePcpRequest.getChargeForCredit());
                tblPcpclaim.setContactNo(savePcpRequest.getContactNo());
                tblPcpclaim.setContractDate(savePcpRequest.getContractDate());
                tblPcpclaim.setContractualApr(savePcpRequest.getContractualApr());
                tblPcpclaim.setContractualMonthlyPayment(savePcpRequest.getContractualMonthlyPayment());
                tblPcpclaim.setDealeraddress(savePcpRequest.getDealeraddress());
                tblPcpclaim.setDealername(savePcpRequest.getDealername());
                tblPcpclaim.setDob(savePcpRequest.getDob());
                tblPcpclaim.setEmailAddress(savePcpRequest.getEmailAddress());
                tblPcpclaim.setEsig(savePcpRequest.getEsig());
                tblPcpclaim.setEsigdate(savePcpRequest.getEsigdate());
                tblPcpclaim.setFcaNumber(savePcpRequest.getFcaNumber());
                tblPcpclaim.setFullName(savePcpRequest.getFullName());
                tblPcpclaim.setLenderaddress(savePcpRequest.getLenderaddress());
                tblPcpclaim.setLendername(savePcpRequest.getLendername());
                tblPcpclaim.setLowestAdvertisedRate(savePcpRequest.getLowestAdvertisedRate());
                tblPcpclaim.setMakemodel(savePcpRequest.getMakemodel());
                tblPcpclaim.setNiNumber(savePcpRequest.getNiNumber());
                tblPcpclaim.setOptionToPurchaseFee(savePcpRequest.getOptionToPurchaseFee());
                tblPcpclaim.setPartExchange(savePcpRequest.getPartExchange());
                tblPcpclaim.setPurchaseDate(savePcpRequest.getPurchaseDate());
                tblPcpclaim.setPurchasePrice(savePcpRequest.getPurchasePrice());
                tblPcpclaim.setRegistration(savePcpRequest.getRegistration());
                tblPcpclaim.setRemarks(savePcpRequest.getRemarks());
                tblPcpclaim.setSetupFees(savePcpRequest.getSetupFees());
                tblPcpclaim.setTotalPayable(savePcpRequest.getTotalPayable());
                tblPcpclaim.setStatus(new BigDecimal(39));
                tblPcpclaim.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                tblPcpclaim.setCreatedate(new Date());
                tblPcpclaim.setContractualTerm(savePcpRequest.getContractualTerm());
                tblPcpclaim.setEndOfAgreement(savePcpRequest.getEndOfAgreement());

                TblCompanyprofile tblCompanyprofile = pcpService.getCompanyProfile(tblUser.getCompanycode());
                tblPcpclaim.setPcpcode(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());

                tblPcpclaim = pcpService.savePcpRequest(tblPcpclaim);

                if (tblPcpclaim != null) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = pcpService.saveCompanyProfile(tblCompanyprofile);

                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblPcpclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/updatePcpCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updatePcpCase(@RequestBody UpdatePcpRequest updatePcpRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == updateHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = pcpService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                TblPcpclaim tblPcpclaim = pcpService.findPcpCaseByIdWithoutUser(updatePcpRequest.getPcpclaimcode());

                tblPcpclaim.setPcpclaimcode(updatePcpRequest.getPcpclaimcode());
                tblPcpclaim.setAddressOfClient(updatePcpRequest.getAddressOfClient());
                tblPcpclaim.setBrokeraddress(updatePcpRequest.getBrokeraddress());
                tblPcpclaim.setBrokername(updatePcpRequest.getBrokername());
                tblPcpclaim.setCashDeposit(updatePcpRequest.getCashDeposit());
                tblPcpclaim.setChargeForCredit(updatePcpRequest.getChargeForCredit());
                tblPcpclaim.setContactNo(updatePcpRequest.getContactNo());
                tblPcpclaim.setContractDate(updatePcpRequest.getContractDate());
                tblPcpclaim.setContractualApr(updatePcpRequest.getContractualApr());
                tblPcpclaim.setContractualMonthlyPayment(updatePcpRequest.getContractualMonthlyPayment());
                tblPcpclaim.setDealeraddress(updatePcpRequest.getDealeraddress());
                tblPcpclaim.setDealername(updatePcpRequest.getDealername());
                tblPcpclaim.setDob(updatePcpRequest.getDob());
                tblPcpclaim.setEmailAddress(updatePcpRequest.getEmailAddress());
                tblPcpclaim.setEsig(updatePcpRequest.getEsig());
                tblPcpclaim.setEsigdate(updatePcpRequest.getEsigdate());
                tblPcpclaim.setFcaNumber(updatePcpRequest.getFcaNumber());
                tblPcpclaim.setFullName(updatePcpRequest.getFullName());
                tblPcpclaim.setLenderaddress(updatePcpRequest.getLenderaddress());
                tblPcpclaim.setLendername(updatePcpRequest.getLendername());
                tblPcpclaim.setLowestAdvertisedRate(updatePcpRequest.getLowestAdvertisedRate());
                tblPcpclaim.setMakemodel(updatePcpRequest.getMakemodel());
                tblPcpclaim.setNiNumber(updatePcpRequest.getNiNumber());
                tblPcpclaim.setOptionToPurchaseFee(updatePcpRequest.getOptionToPurchaseFee());
                tblPcpclaim.setPartExchange(updatePcpRequest.getPartExchange());
                tblPcpclaim.setPurchaseDate(updatePcpRequest.getPurchaseDate());
                tblPcpclaim.setPurchasePrice(updatePcpRequest.getPurchasePrice());
                tblPcpclaim.setRegistration(updatePcpRequest.getRegistration());
                tblPcpclaim.setRemarks(updatePcpRequest.getRemarks());
                tblPcpclaim.setSetupFees(updatePcpRequest.getSetupFees());
                tblPcpclaim.setTotalPayable(updatePcpRequest.getTotalPayable());
                tblPcpclaim.setContractualTerm(updatePcpRequest.getContractualTerm());
                tblPcpclaim.setEndOfAgreement(updatePcpRequest.getEndOfAgreement());
                tblPcpclaim.setPcpcode(updatePcpRequest.getPcpcode());
                tblPcpclaim.setUpdatedate(new Date());
                tblPcpclaim = pcpService.savePcpRequest(tblPcpclaim);

                if (tblPcpclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblPcpclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addPcpNotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addPcpNotes(@RequestBody PcpNoteRequest PcpNoteRequest,
                                                               HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addPcpNotes(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = pcpService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                if (!PcpNoteRequest.getPcpCode().isEmpty()) {
                    TblPcpnote tblPcpnote = new TblPcpnote();
                    TblPcpclaim tblPcpclaim = new TblPcpclaim();

                    tblPcpclaim.setPcpclaimcode(Long.valueOf(PcpNoteRequest.getPcpCode()));
                    tblPcpnote.setTblPcpclaim(tblPcpclaim);
                    tblPcpnote.setNote(PcpNoteRequest.getNote());
                    tblPcpnote.setUsercategorycode(PcpNoteRequest.getUserCatCode());
                    tblPcpnote.setCreatedon(new Date());
                    tblPcpnote.setUsercode(tblUser.getUsercode());

                    tblPcpnote = pcpService.addTblPcpNote(tblPcpnote);

                    if (tblPcpnote != null && tblPcpnote.getPcpnotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblPcpnote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addHdrNotes();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/resendPcpEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> resendPcpEmail(@RequestBody ResendPcpMessageDto resendPcpMessageDto,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == resendRtaEmail(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblPcpmessage tblPcpmessage = pcpService.getTblPcpMessageById(resendPcpMessageDto.getPcpmessagecode());
            if (tblPcpmessage != null && tblPcpmessage.getPcpmessagecode() > 0) {


                TblEmail tblEmail = new TblEmail();
                tblEmail.setSenflag(new BigDecimal(0));
                tblEmail.setEmailaddress(tblPcpmessage.getSentto());
                tblEmail.setEmailbody(tblPcpmessage.getMessage());
                tblEmail.setEmailsubject("Pcp LegalAssist");
                tblEmail.setCreatedon(new Date());


                tblEmail = pcpService.saveTblEmail(tblEmail);
                if (tblEmail != null && tblEmail.getEmailcode() > 0) {

                    LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", tblEmail);
                } else {
                    LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While resending Email", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Email Found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == resendRtaEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/assigncasetosolicitorbyLA", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> assigncasetosolicitorbyLA(
            @RequestBody AssignPcpCasetoSolicitor assignPcpCasetoSolicitor, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblPcpclaim tblPcpclaim = pcpService.assignCaseToSolicitor(assignPcpCasetoSolicitor, tblUser);
                if (tblPcpclaim != null) {
                    tblPcpclaim = pcpService.findPcpCaseById(Long.valueOf(assignPcpCasetoSolicitor.getPcpClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblPcpclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }


            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnPcp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnPcp(
            @RequestBody PerformActionOnPcpRequest performActionOnPcpRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnHdr(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblPcpclaim tblPcpclaim = null;
                if (performActionOnPcpRequest.getToStatus().equals("59")) {
                    // reject status
                    tblPcpclaim = pcpService.performRejectCancelActionOnPcp(performActionOnPcpRequest.getPcpClaimCode(),
                            performActionOnPcpRequest.getToStatus(), performActionOnPcpRequest.getReason(), tblUser);

                } else if (performActionOnPcpRequest.getToStatus().equals("60")) {
                    //cancel status
                    tblPcpclaim = pcpService.performRejectCancelActionOnPcp(performActionOnPcpRequest.getPcpClaimCode(),
                            performActionOnPcpRequest.getToStatus(), performActionOnPcpRequest.getReason(), tblUser);

                } else {
                    tblPcpclaim = pcpService.performActionOnPcp(performActionOnPcpRequest.getPcpClaimCode(),
                            performActionOnPcpRequest.getToStatus(), tblUser);
                }

                if (tblPcpclaim != null) {
                    TblPcpclaim Pcpclaim = pcpService
                            .findPcpCaseById(Long.valueOf(performActionOnPcpRequest.getPcpClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", Pcpclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addESign(); ");
        try {
            Locale backup = Locale.getDefault();
            Locale.setDefault(Locale.ENGLISH);
            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("PcpClaimCode")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }

            TblPcpclaim tblPcpclaim = pcpService.findPcpCaseByIdWithoutUser(addESign.getRtaCode());
            if (tblPcpclaim != null && tblPcpclaim.getPcpclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = pcpService.getCompanyProfileAgainstPcpCode(tblPcpclaim.getPcpclaimcode());
                PdfDocument doc = new PdfDocument();

                String path = "";
                TblCompanydoc tblCompanydoc = new TblCompanydoc();
                tblCompanydoc = pcpService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "E");

                doc.loadFromFile(tblCompanydoc.getPath());

                byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                BufferedImage image1 = ImageIO.read(bis);
                bis.close();

                PdfImage image = PdfImage.fromImage(image1);

                // Get the first worksheet
                PdfPageCollection page = doc.getPages();
                for (int j = 0; j < page.getCount(); j++) {
                    // Get the image information of the page
                    PdfImageInfo[] imageInfo = page.get(j).getImagesInfo();

                    // Loop through the image information
                    for (int i = 0; i < imageInfo.length; i++) {

                        // Get the bounds property of a specific image
                        Rectangle2D rect = imageInfo[i].getBounds();
                        // Get the x and y coordinates
                        System.out.println(String.format("The coordinate of image %d:（%f, %f）", i + 1, rect.getX(),
                                rect.getY()));

                        page.get(j).getCanvas().drawImage(image, rect.getX(), rect.getY(), rect.getWidth(),
                                rect.getHeight());
                    }
                }

                // get the form fields from the document
                PdfFormWidget form = (PdfFormWidget) doc.getForm();

                // get the form widget collection
                PdfFormFieldWidgetCollection formWidgetCollection = form.getFieldsWidget();

                // loop through the widget collection and fill each field with value
                for (int i = 0; i < formWidgetCollection.getCount(); i++) {
                    PdfField field = formWidgetCollection.get(i);
                    if (field instanceof PdfTextBoxFieldWidget) {
                        while (keys.hasNext()) {
                            String key = keys.next();
                            if (!key.equals("rtaCode") && !key.equals("eSign")) {
                                if (field.getName().equals(key)) {
                                    PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;
                                    textBoxField.setText(jsonObject.getString(key));
                                }
                            }
                        }

                        if (field.getName().startsWith("DB_")) {

                            PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;
                            String keyValue = pcpService.getPcpDbColumnValue(field.getName().replace("DB_", ""),
                                    tblPcpclaim.getPcpclaimcode());
                            if (keyValue.isEmpty()) {
                                Dimension2D dimension2D = new Dimension();
                                dimension2D.setSize(0, 0);
                                textBoxField.setSize(dimension2D);
                            } else {
                                textBoxField.setText(keyValue);
                            }


                        }
                        if (field.getName().startsWith("DATE")) {
                            PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;

                            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

                            Date date = new Date();
                            String s = formatter.format(date);
                            textBoxField.setText(s);
                        }
                        field.setFlatten(true);
                        field.setReadOnly(true);
                    }
                }
                String s = "C:\\Esigns\\" + tblPcpclaim.getPcpcode() + ".pdf";
                doc.saveToFile(s);
                File file = new File(s);
//                String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));
                String UPLOADED_FOLDER = "E://temp//";

                TblPcpdocument tblPcpdocument = new TblPcpdocument();
                tblPcpdocument.setTblPcpclaim(tblPcpclaim);
                tblPcpdocument.setDocumentType("E-Sign");
                tblPcpdocument.setDocumentPath(UPLOADED_FOLDER + tblPcpclaim.getPcpcode() + ".pdf");

                byte[] bytes = s.getBytes();
                Path path1 = Paths.get(UPLOADED_FOLDER + tblPcpclaim.getPcpcode() + ".pdf");
                Files.write(path1, bytes);

                tblPcpdocument = pcpService.saveTblPcpDocument(tblPcpdocument);
                if (tblPcpdocument != null && tblPcpdocument.getPcpdocumentscode() > 0) {
                    Locale.setDefault(backup);
                    tblPcpclaim.setEsig("Y");
                    tblPcpclaim = pcpService.savePcpRequest(tblPcpclaim);
                    TblPcptask tblPcptask = pcpService.getPcpTaskByPcpCodeAndTaskCode(tblPcpclaim.getPcpclaimcode(), 1);

                    tblPcptask.setStatus("C");
                    tblPcptask.setRemarks("Completed");

                    TblPcptask tblPcptask1 = pcpService.updateTblPcpTask(tblPcptask);
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", "Esign Completed");
                } else {
                    Locale.setDefault(backup);
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Saving The Document..Esign Compeleted",
                            null);
                }
            } else {
                Locale.setDefault(backup);
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addESign(); ");
        try {

            TblPcpclaim tblPcpclaim = pcpService.findPcpCaseByIdWithoutUser(Long.valueOf(eSignFieldsRequest.getCode()));
            if (tblPcpclaim != null && tblPcpclaim.getPcpclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = pcpService
                        .getCompanyProfileAgainstPcpCode(tblPcpclaim.getPcpclaimcode());

                String path = "";
                TblCompanydoc tblCompanydoc = new TblCompanydoc();
                tblCompanydoc = pcpService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "E");

                HashMap<String, String> fields = new HashMap<>();

                PdfDocument doc = new PdfDocument();
                doc.loadFromFile(tblCompanydoc.getPath());

                PdfFormWidget form = (PdfFormWidget) doc.getForm();
                PdfFormFieldWidgetCollection formWidgetCollection = form.getFieldsWidget();
                for (int i = 0; i < formWidgetCollection.getCount(); i++) {
                    PdfField field = formWidgetCollection.get(i);
                    if (!field.getName().startsWith("DB_") && !field.getName().equals("DATE")) {
                        fields.put(field.getName(), field.getName());
                    }
                }

                String viewDocPath = tblCompanydoc.getPath().replace("Esigns", "ViewEsigns");
                File file = new File(viewDocPath);
                String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                fields.put("doc", encoded);

                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Success", fields);
            } else {
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No HDR document found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("hdrClaimCode") long hdrClaimCode, @RequestParam("taskCode") long taskCode, @RequestParam("multipartFiles") List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblPcpclaim tblPcpclaim = pcpService.findPcpCaseById(hdrClaimCode, tblUser);
            if (tblPcpclaim != null && tblPcpclaim.getPcpclaimcode() > 0) {
                TblTask tblTask = pcpService.getTaskAgainstCode(taskCode);
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblPcptask tblPcptask = pcpService.getPcpTaskByPcpCodeAndTaskCode(tblPcpclaim.getPcpclaimcode(), taskCode);

                    if (tblTask.getTaskcode() == 1) {
                        if (tblPcpclaim.getEmailAddress() == null) {
                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "No Client Email Address Found", null);
                        }
                        String emailUrl = "115.186.147.30:8888/esign?hdr=" + tblPcpclaim.getPcpclaimcode();

                        TblEmail tblEmail = new TblEmail();
                        tblEmail.setSenflag(new BigDecimal(0));
                        tblEmail.setEmailaddress(tblPcpclaim.getEmailAddress());
                        tblEmail.setEmailbody("Email sent to perform esign on mentioned hdr claim number: " + tblPcpclaim.getPcpcode()
                                + "\n Click on the below link to perform action \n" + emailUrl);
                        tblEmail.setEmailsubject("PCP CASE" + tblPcpclaim.getPcpcode());
                        tblEmail.setCreatedon(new Date());

                        tblEmail = pcpService.saveTblEmail(tblEmail);

                        tblPcptask.setStatus("P");
                        tblPcptask.setRemarks("Email Sent Successfully to Client");
                    } else {
                        String UPLOADED_FOLDER = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\";
                        String UPLOADED_SERVER = "http:\\115.186.147.30:8888\\";
                        List<TblPcpdocument> tblPcpdocuments = new ArrayList<>();

                        File theDir = new File(UPLOADED_FOLDER + tblPcpclaim.getPcpcode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }

                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);

                            TblPcpdocument tblPcpdocument = new TblPcpdocument();
                            tblPcpdocument.setDocumentPath(UPLOADED_SERVER + tblPcpclaim.getPcpcode() + "\\" + multipartFile.getOriginalFilename());
                            tblPcpdocument.setDocumentType(multipartFile.getContentType());
                            tblPcpdocument.setTblPcpclaim(tblPcpclaim);

                            tblPcpdocuments.add(tblPcpdocument);
                        }

                        tblPcpdocuments = pcpService.saveTblPcpDocuments(tblPcpdocuments);
                        tblPcptask.setStatus("C");
                        tblPcptask.setRemarks("Completed");
                    }
                    TblPcptask tblHdrtask1 = pcpService.updateTblPcpTask(tblPcptask);
                    if (tblHdrtask1 != null && tblHdrtask1.getPcptaskcode() > 0) {

                        List<TblPcptask> tblPcptasks = pcpService.findTblPcpTaskByPcpClaimCode(tblPcpclaim.getPcpclaimcode());
                        int count = 0;
                        for (TblPcptask Pcptask : tblPcptasks) {
                            if (Pcptask.getStatus().equals("C")) {
                                count++;
                            }
                        }

                        if (count == tblPcptasks.size()) {
                            tblPcpclaim.setStatus(new BigDecimal(61));

                            tblPcpclaim = pcpService.savePcpRequest(tblPcpclaim);
                        }

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblHdrtask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
