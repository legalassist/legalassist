package com.laportal.controller.hdr;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.hdr.HdrService;
import com.laportal.service.rta.RtaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/hdr")
public class HdrGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(HdrGetApi.class);

    @Autowired
    private HdrService hdrService;
    @Autowired
    private RtaService rtaService;

    @RequestMapping(value = "/getHdrCases", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrCases(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = hdrService.getCompanyProfile(tblUser.getCompanycode());
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "3");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }


            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<HdrCasesList> viewHdrCaseslist = hdrService.getAuthHdrCasesIntroducers(tblUser.getUsercode());
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<HdrCasesList> viewHdrCaseslist = hdrService.getAuthHdrCasesSolicitors(tblUser.getUsercode());
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<HdrCasesList> viewHdrCaseslist = hdrService.getAuthHdrCasesLegalAssist();
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getHdrCaseById/{hdrCaseId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrCaseById(@PathVariable long hdrCaseId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCaseById(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "3");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            TblHdrclaim tblHdrclaim = hdrService.findHdrCaseById(hdrCaseId, tblUser);


            List<TblHdrtask> tblHdrtaskList = new ArrayList<>();

            List<TblHdrtask> tblHdrtasks = tblHdrclaim.getHdrCaseTasks();

            if (tblHdrtasks != null && tblHdrtasks.size() > 0) {
                for (TblHdrtask tblHdrtask : tblHdrtasks) {
                    TblTask tblTask = hdrService.findTaskById(Long.valueOf(tblHdrtask.getTaskcode().toString()));

                    tblHdrtask.setTblTask(tblTask);

                    tblHdrtaskList.add(tblHdrtask);
                }
            }
            TblUser user = hdrService.findByUserId(String.valueOf(tblHdrclaim.getAdvisor()));
            tblHdrclaim.setTblUser(user);
            if (tblHdrclaim.getTblHdrsolicitors() != null) {
                TblCompanyprofile tblCompanyprofile = hdrService.getCompanyProfile(tblHdrclaim.getTblHdrsolicitors().get(0).getCompanycode());
                tblHdrclaim.getTblHdrsolicitors().get(0).setTblCompanyprofile(tblCompanyprofile);
                tblHdrclaim.setHdrCaseTasks(tblHdrtaskList);
            } else {
                tblHdrclaim.setHdrCaseTasks(null);
            }



            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Record Found", tblHdrclaim);
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllHdrStatusCounts", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllHdrStatusCounts(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = hdrService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<HdrStatusCountList> hdrStatusCountList = hdrService.getAllHdrStatusCountsForIntroducers(tblCompanyprofile.getCompanycode());
                if(hdrStatusCountList != null) {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", hdrStatusCountList);
                }else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<HdrStatusCountList> hdrStatusCountList = hdrService.getAllHdrStatusCountsForSolicitor(tblCompanyprofile.getCompanycode());

                if(hdrStatusCountList != null) {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", hdrStatusCountList);
                }else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<HdrStatusCountList> hdrStatusCountList = hdrService.getAllHdrStatusCounts();

                if(hdrStatusCountList != null) {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", hdrStatusCountList);
                }else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getHdrCasesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrCasesByStatus(@PathVariable long statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCasesByStatus(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            TblCompanyprofile tblCompanyprofile = hdrService.getCompanyProfile(tblUser.getCompanycode());
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "3");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }


            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<HdrCasesList> viewHdrCaseslist = hdrService.getAuthHdrCasesIntroducersStatusWise(tblCompanyprofile.getCompanycode(),statusId);
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<HdrCasesList> viewHdrCaseslist = hdrService.getAuthHdrCasesSolicitorsStatusWise(tblCompanyprofile.getCompanycode(),statusId);
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<HdrCasesList> viewHdrCaseslist = hdrService.getAuthHdrCasesLegalAssistStatusWise(statusId);
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getHdrCaseLogs/{Hdrcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrCaseLogs(@PathVariable String Hdrcode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCaseLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblHdrlog> HdrCaseLogs = hdrService.getHdrCaseLogs(Hdrcode);

                if (HdrCaseLogs != null && HdrCaseLogs.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHdrCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", HdrCaseLogs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getHdrCaseLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCaseLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getHdrCaseNotes/{Hdrcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCaseNotes(@PathVariable String Hdrcode,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "3");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }



            if (tblUser != null) {
                List<TblHdrnote> tblHdrNotes = hdrService.getHdrCaseNotes(Hdrcode, tblUser);
                List<TblHdrnote> tblHdrNotesLegalInternal = hdrService.getAuthHdrCaseNotesOfLegalInternal(Hdrcode, tblUser);

                if (tblHdrNotes != null && tblHdrNotes.size() > 0) {
                    for (TblHdrnote tblHdrnote : tblHdrNotes) {
                        tblHdrnote.setTblHdrclaim(null);
                    }

                    HdrNotesResponse rtaNotesResponse = new HdrNotesResponse();
                    rtaNotesResponse.setTblHdrnotes(tblHdrNotes);
                    rtaNotesResponse.setTblhdrnotesLegalOnly(tblHdrNotesLegalInternal);

                    LOG.info("\n EXITING THIS METHOD == getHdrCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaNotesResponse);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHdrCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getHdrCaseMessages/{Hdrcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCaseMessages(@PathVariable String Hdrcode,
                                                                          HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCaseMessages(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblHdrmessage> tblHdrMessages = hdrService.getHdrCaseMessages(Hdrcode);

                if (tblHdrMessages != null && tblHdrMessages.size() > 0) {
                    for (TblHdrmessage tblHdrmessage : tblHdrMessages) {
                        tblHdrmessage.setTblHdrclaim(null);
                    }

                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblHdrMessages);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHdrCaseMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getHdrAuditLogs/{hdrcasecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getRtaAuditLogs(@PathVariable String hdrcasecode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS ==  \n METHOD == getHdrAuditLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<RtaAuditLogResponse> rtaAuditLogResponses = hdrService.getHdrAuditLogs(hdrcasecode);

                if (rtaAuditLogResponses != null && rtaAuditLogResponses.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getHdrAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaAuditLogResponses);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrGetApi \n METHOD == getHdrAuditLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrAuditLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getHdrDuplicates/{hdrcasecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrDuplicates(@PathVariable String hdrcasecode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getHdrDuplicates(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrDuplicates(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<RtaDuplicatesResponse> hireDuplicates = hdrService.getHdrDuplicates(hdrcasecode);

                if (hireDuplicates != null) {
                    LOG.info("\n EXITING THIS METHOD == getHdrDuplicates(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", hireDuplicates);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrDuplicates(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrDuplicates(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHdrDuplicates();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrDuplicates(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

}
