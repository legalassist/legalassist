package com.laportal.controller.hdr;


import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.hdr.HdrService;
import com.laportal.service.rta.RtaService;
import com.spire.pdf.PdfDocument;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/hdr")
public class HdrPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(HdrPostApi.class);

    @Autowired
    private HdrService hdrService;

    @Autowired
    private RtaService rtaService;


    @RequestMapping(value = "/addNewHdrCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewHdrCase(@RequestBody SaveHdrRequest saveHdrRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addNewHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "3");
            boolean introducerAllowed = rtaService.isRtaCaseAllowed(saveHdrRequest.getSaveTblHdrclaim().getIntroducer(), "3");

            if (allowed && introducerAllowed) {
                TblHdrclaim tblHdrclaim = new TblHdrclaim();
                TblHdrclaimant tblHdrclaimant = new TblHdrclaimant();
                TblHdrjointtenancy tblHdrjointtenancy = null;
                TblHdrtenancy tblHdrtenancy = new TblHdrtenancy();

                tblHdrclaim.setAprxReporteddate(saveHdrRequest.getSaveTblHdrclaim().getAprxReporteddate());
                tblHdrclaim.setDefectLastreportedon(saveHdrRequest.getSaveTblHdrclaim().getDefectLastreportedon());
                tblHdrclaim.setDefectReported(saveHdrRequest.getSaveTblHdrclaim().getDefectReported());
                tblHdrclaim.setHealthAffected(saveHdrRequest.getSaveTblHdrclaim().getHealthAffected());
                tblHdrclaim.setHealthRelDetails(saveHdrRequest.getSaveTblHdrclaim().getHealthRelDetails());
                tblHdrclaim.setLlContactno(saveHdrRequest.getSaveTblHdrclaim().getLlContactno());
                tblHdrclaim.setLlName(saveHdrRequest.getSaveTblHdrclaim().getLlName());
                tblHdrclaim.setLlRespondDetail(saveHdrRequest.getSaveTblHdrclaim().getLlRespondDetail());
                tblHdrclaim.setLlResponded(saveHdrRequest.getSaveTblHdrclaim().getLlResponded());
                tblHdrclaim.setReportedToLl(saveHdrRequest.getSaveTblHdrclaim().getReportedToLl());
                tblHdrclaim.setReportedToLlBy(saveHdrRequest.getSaveTblHdrclaim().getReportedToLlBy());
                tblHdrclaim.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                tblHdrclaim.setCreatedate(new Date());
                tblHdrclaim.setDefectFirstreportedon(saveHdrRequest.getSaveTblHdrclaim().getDefectFirstreportedon());
                tblHdrclaim.setStatuscode(new BigDecimal(39));
                tblHdrclaim.setAdvisor(new BigDecimal(saveHdrRequest.getSaveTblHdrclaim().getAdvisor()));
                tblHdrclaim.setIntroducer(new BigDecimal(saveHdrRequest.getSaveTblHdrclaim().getIntroducer()));
                tblHdrclaim.setEsig("N");


                tblHdrclaimant.setCaddress1(saveHdrRequest.getSaveTblHdrclaimant().getCaddress1());
                tblHdrclaimant.setCaddress2(saveHdrRequest.getSaveTblHdrclaimant().getCaddress2());
                tblHdrclaimant.setCaddress3(saveHdrRequest.getSaveTblHdrclaimant().getCaddress3());
                tblHdrclaimant.setCcity(saveHdrRequest.getSaveTblHdrclaimant().getCcity());
                tblHdrclaimant.setCcontacttime(saveHdrRequest.getSaveTblHdrclaimant().getCcontacttime());
                tblHdrclaimant.setCdob(saveHdrRequest.getSaveTblHdrclaimant().getCdob());
                tblHdrclaimant.setCemail(saveHdrRequest.getSaveTblHdrclaimant().getCemail());
                tblHdrclaimant.setCfname(saveHdrRequest.getSaveTblHdrclaimant().getCfname());
                tblHdrclaimant.setIsJointTenancy(saveHdrRequest.getSaveTblHdrclaimant().getIsJointTenancy());
                tblHdrclaimant.setClandline(saveHdrRequest.getSaveTblHdrclaimant().getClandline());
                tblHdrclaimant.setCmname(saveHdrRequest.getSaveTblHdrclaimant().getCmname());
                tblHdrclaimant.setCmobileno(saveHdrRequest.getSaveTblHdrclaimant().getCmobileno());
                tblHdrclaimant.setCninumber(saveHdrRequest.getSaveTblHdrclaimant().getCninumber());
                tblHdrclaimant.setCpostalcode(saveHdrRequest.getSaveTblHdrclaimant().getCpostalcode());
                tblHdrclaimant.setCregion(saveHdrRequest.getSaveTblHdrclaimant().getCregion());
                tblHdrclaimant.setCsname(saveHdrRequest.getSaveTblHdrclaimant().getCsname());
                tblHdrclaimant.setCtitle(saveHdrRequest.getSaveTblHdrclaimant().getCtitle());

                if (saveHdrRequest.getSaveTblHdrjointtenancy().getTtitle() != null && !saveHdrRequest.getSaveTblHdrjointtenancy().getTtitle().isEmpty() && !saveHdrRequest.getSaveTblHdrjointtenancy().getTfname().isEmpty()) {
                    tblHdrjointtenancy = new TblHdrjointtenancy();
                    tblHdrjointtenancy.setTaddress1(saveHdrRequest.getSaveTblHdrjointtenancy().getTaddress1());
                    tblHdrjointtenancy.setTaddress2(saveHdrRequest.getSaveTblHdrjointtenancy().getTaddress2());
                    tblHdrjointtenancy.setTaddress3(saveHdrRequest.getSaveTblHdrjointtenancy().getTaddress3());
                    tblHdrjointtenancy.setTcity(saveHdrRequest.getSaveTblHdrjointtenancy().getTcity());
                    tblHdrjointtenancy.settDob(saveHdrRequest.getSaveTblHdrjointtenancy().getTdob());
                    tblHdrjointtenancy.settEmail(saveHdrRequest.getSaveTblHdrjointtenancy().getTemail());
                    tblHdrjointtenancy.settFname(saveHdrRequest.getSaveTblHdrjointtenancy().getTfname());
                    tblHdrjointtenancy.settLandline(saveHdrRequest.getSaveTblHdrjointtenancy().getTlandline());
                    tblHdrjointtenancy.settMname(saveHdrRequest.getSaveTblHdrjointtenancy().getTmname());
                    tblHdrjointtenancy.settMobileno(saveHdrRequest.getSaveTblHdrjointtenancy().getTmobileno());
                    tblHdrjointtenancy.settNicnumber(saveHdrRequest.getSaveTblHdrjointtenancy().getTninumber());
                    tblHdrjointtenancy.setTpostalcode(saveHdrRequest.getSaveTblHdrjointtenancy().getTpostalcode());
                    tblHdrjointtenancy.setTregion(saveHdrRequest.getSaveTblHdrjointtenancy().getTregion());
                    tblHdrjointtenancy.settSname(saveHdrRequest.getSaveTblHdrjointtenancy().getTsname());
                    tblHdrjointtenancy.settTitle(saveHdrRequest.getSaveTblHdrjointtenancy().getTtitle());
                }

                tblHdrtenancy.setArrearsAmount(saveHdrRequest.getSaveTblHdrtenancy().getArrearsAmount());
                tblHdrtenancy.setBenefitsDetail(saveHdrRequest.getSaveTblHdrtenancy().getBenefitsDetail());
                tblHdrtenancy.setContributePm(saveHdrRequest.getSaveTblHdrtenancy().getContributePm());
                tblHdrtenancy.setContributePw(saveHdrRequest.getSaveTblHdrtenancy().getContributePw());
                tblHdrtenancy.setInArrears(saveHdrRequest.getSaveTblHdrtenancy().getInArrears());
                tblHdrtenancy.setInPaymentPlan(saveHdrRequest.getSaveTblHdrtenancy().getInPaymentPlan());
                tblHdrtenancy.setNoOfOccupants(saveHdrRequest.getSaveTblHdrtenancy().getNoOfOccupants());
                tblHdrtenancy.setPaymentPlanPm(saveHdrRequest.getSaveTblHdrtenancy().getPaymentPlanPm());
                tblHdrtenancy.setPaymentPlanPw(saveHdrRequest.getSaveTblHdrtenancy().getPaymentPlanPw());
                tblHdrtenancy.setPropertyType(saveHdrRequest.getSaveTblHdrtenancy().getPropertyType());
                tblHdrtenancy.setReceivingBenefits(saveHdrRequest.getSaveTblHdrtenancy().getReceivingBenefits());
                tblHdrtenancy.setRentContribute(saveHdrRequest.getSaveTblHdrtenancy().getRentContribute());
                tblHdrtenancy.setRentPm(saveHdrRequest.getSaveTblHdrtenancy().getRentPm());
                tblHdrtenancy.setRentPw(saveHdrRequest.getSaveTblHdrtenancy().getRentPw());
                tblHdrtenancy.setStartDate(saveHdrRequest.getSaveTblHdrtenancy().getStartDate());

                List<TblHdraffectedper> tblHdraffectedperList = new ArrayList<TblHdraffectedper>();
                List<SaveTblHdraffectedper> saveTblHdraffectedperList = saveHdrRequest.getSaveTblHdraffectedperList();
                if (saveTblHdraffectedperList != null && saveTblHdraffectedperList.size() > 0) {
                    for (SaveTblHdraffectedper saveTblHdraffectedper : saveTblHdraffectedperList) {
                        TblHdraffectedper tblHdraffectedper = new TblHdraffectedper();

                        tblHdraffectedper.setEvidenceDetail(saveTblHdraffectedper.getEvidenceDetail());
                        tblHdraffectedper.setMedicalEvidence(saveTblHdraffectedper.getMedicalEvidence());
                        tblHdraffectedper.setPersondob(saveTblHdraffectedper.getPersondob());
                        tblHdraffectedper.setPersonname(saveTblHdraffectedper.getPersonname());
                        tblHdraffectedper.setSufferingFrom(saveTblHdraffectedper.getSufferingFrom());

                        tblHdraffectedperList.add(tblHdraffectedper);
                    }
                }

                List<TblHdraffectedroom> tblHdraffectedroomList = new ArrayList<TblHdraffectedroom>();
                List<SaveTblHdraffectedroom> saveTblHdraffectedroomList = saveHdrRequest.getSaveTblHdraffectedroomList();
                if (saveTblHdraffectedroomList != null && saveTblHdraffectedroomList.size() > 0) {
                    for (SaveTblHdraffectedroom saveTblHdraffectedroom : saveTblHdraffectedroomList) {
                        TblHdraffectedroom tblHdraffectedroom = new TblHdraffectedroom();

                        tblHdraffectedroom.setRoomName(saveTblHdraffectedroom.getRoomName());
                        tblHdraffectedroom.setDamageList(saveTblHdraffectedroom.getDamageList());
                        tblHdraffectedroom.setDisrepairDetail(saveTblHdraffectedroom.getDisrepairDetail());
                        tblHdraffectedroom.setLastReported(saveTblHdraffectedroom.getLastReported());
                        tblHdraffectedroom.setPersonalPropertydamage(saveTblHdraffectedroom.getPersonalPropertydamage());
                        tblHdraffectedroom.setReportDetails(saveTblHdraffectedroom.getReportDetails());

                        tblHdraffectedroomList.add(tblHdraffectedroom);
                    }
                }
                TblCompanyprofile tblCompanyprofile = hdrService.getCompanyProfile(tblHdrclaim.getIntroducer().toString());
                tblHdrclaim.setClaimcode(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());

                TblHdrclaim tblHdrclaim1 = hdrService.saveHdrRequest(tblHdrclaim, tblHdrclaimant, tblHdrjointtenancy, tblHdrtenancy, tblHdraffectedperList, tblHdraffectedroomList);

                if (tblHdrclaim1 != null) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = hdrService.saveCompanyProfile(tblCompanyprofile);

                    tblHdrclaim1.setTblHdraffectedpers(null);
                    tblHdrclaim1.setTblHdraffectedrooms(null);
                    tblHdrclaim1.setTblHdrclaimants(null);
                    tblHdrclaim1.setTblHdrjointtenancies(null);
                    tblHdrclaim1.setTblHdrtenancies(null);
                    tblHdrclaim1.setTblHdrdocuments(null);
                    tblHdrclaim1.setTblHdrlogs(null);
                    tblHdrclaim1.setTblHdrmessages(null);
                    tblHdrclaim1.setTblHdrnotes(null);
                    tblHdrclaim1.setHdrCaseTasks(null);
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblHdrclaim1.getHdrclaimcode());
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/uploadHdrDocs", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewHdrCase(@RequestParam("hdrClaimCode") String hdrClaimCode, @RequestParam("multipartFiles") List<MultipartFile> multipartFiles,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addNewHdrCase(); ");

            TblHdrclaim tblHdrclaim = hdrService.findHdrClaimByCode(hdrClaimCode);

//            String UPLOADED_FOLDER = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\hdr\\";
//            String UPLOADED_SERVER = "http:\\115.186.147.30:8080\\hdr\\";
            String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\hdr\\";
            String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\hdr\\";
            List<TblHdrdocument> saveTblHdrDocuments = new ArrayList<>();

            File theDir = new File(UPLOADED_FOLDER + tblHdrclaim.getClaimcode());
            if (!theDir.exists()) {
                theDir.mkdirs();
            }

            for (MultipartFile multipartFile : multipartFiles) {
                byte[] bytes = multipartFile.getBytes();
                Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                Files.write(path, bytes);

                TblHdrdocument tblHdrdocument = new TblHdrdocument();
                tblHdrdocument.setDocumentPath(UPLOADED_SERVER + tblHdrclaim.getClaimcode() + "\\" + multipartFile.getOriginalFilename());
                tblHdrdocument.setDocumentType(multipartFile.getContentType());
                tblHdrdocument.setTblHdrclaim(tblHdrclaim);

                saveTblHdrDocuments.add(tblHdrdocument);
            }

            saveTblHdrDocuments = hdrService.saveTblHdrDocuments(saveTblHdrDocuments);

            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "File Uploaded SuccessFully", null);

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/updateHdrCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateHdrCase(@RequestBody UpdateHdrRequest updateHdrRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == updateHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblHdrclaim tblHdrclaim = hdrService.findHdrCaseByIdwithoutuser(updateHdrRequest.getUpdateTblHdrclaim().getHdrclaimcode());

            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "3");
            boolean introducerAllowed = rtaService.isRtaCaseAllowed(String.valueOf(tblHdrclaim.getIntroducer()), "3");

            if (allowed && introducerAllowed) {

                TblHdrclaimant tblHdrclaimant = new TblHdrclaimant();
                TblHdrjointtenancy tblHdrjointtenancy = new TblHdrjointtenancy();
                TblHdrtenancy tblHdrtenancy = new TblHdrtenancy();

                tblHdrclaim.setHdrclaimcode(updateHdrRequest.getUpdateTblHdrclaim().getHdrclaimcode());
                tblHdrclaim.setAprxReporteddate(updateHdrRequest.getUpdateTblHdrclaim().getAprxReporteddate());
                tblHdrclaim.setDefectLastreportedon(updateHdrRequest.getUpdateTblHdrclaim().getDefectLastreportedon());
                tblHdrclaim.setDefectReported(updateHdrRequest.getUpdateTblHdrclaim().getDefectReported());
                tblHdrclaim.setHealthAffected(updateHdrRequest.getUpdateTblHdrclaim().getHealthAffected());
                tblHdrclaim.setHealthRelDetails(updateHdrRequest.getUpdateTblHdrclaim().getHealthRelDetails());
                tblHdrclaim.setLlContactno(updateHdrRequest.getUpdateTblHdrclaim().getLlContactno());
                tblHdrclaim.setLlName(updateHdrRequest.getUpdateTblHdrclaim().getLlName());
                tblHdrclaim.setLlRespondDetail(updateHdrRequest.getUpdateTblHdrclaim().getLlRespondDetail());
                tblHdrclaim.setLlResponded(updateHdrRequest.getUpdateTblHdrclaim().getLlResponded());
                tblHdrclaim.setReportedToLl(updateHdrRequest.getUpdateTblHdrclaim().getReportedToLl());
                tblHdrclaim.setReportedToLlBy(updateHdrRequest.getUpdateTblHdrclaim().getReportedToLlBy());
                tblHdrclaim.setDefectFirstreportedon(updateHdrRequest.getUpdateTblHdrclaim().getDefectFirstreportedon());
                tblHdrclaim.setUpdatedate(new Date());
                tblHdrclaim.setLastupdateuser(tblUser.getUsercode());

                tblHdrclaimant.setHrdclaimantcode(tblHdrclaim.getTblHdrclaimants().get(0).getHrdclaimantcode());
                tblHdrclaimant.setCaddress1(updateHdrRequest.getUpdateTblHdrclaimant().getCaddress1());
                tblHdrclaimant.setCaddress2(updateHdrRequest.getUpdateTblHdrclaimant().getCaddress2());
                tblHdrclaimant.setCaddress3(updateHdrRequest.getUpdateTblHdrclaimant().getCaddress3());
                tblHdrclaimant.setCcity(updateHdrRequest.getUpdateTblHdrclaimant().getCcity());
                tblHdrclaimant.setCcontacttime(updateHdrRequest.getUpdateTblHdrclaimant().getCcontacttime());
                tblHdrclaimant.setCdob(updateHdrRequest.getUpdateTblHdrclaimant().getCdob());
                tblHdrclaimant.setCemail(updateHdrRequest.getUpdateTblHdrclaimant().getCemail());
                tblHdrclaimant.setCfname(updateHdrRequest.getUpdateTblHdrclaimant().getCfname());
                tblHdrclaimant.setClandline(updateHdrRequest.getUpdateTblHdrclaimant().getClandline());
                tblHdrclaimant.setCmname(updateHdrRequest.getUpdateTblHdrclaimant().getCmname());
                tblHdrclaimant.setCmobileno(updateHdrRequest.getUpdateTblHdrclaimant().getCmobileno());
                tblHdrclaimant.setCninumber(updateHdrRequest.getUpdateTblHdrclaimant().getCninumber());
                tblHdrclaimant.setCpostalcode(updateHdrRequest.getUpdateTblHdrclaimant().getCpostalcode());
                tblHdrclaimant.setCregion(updateHdrRequest.getUpdateTblHdrclaimant().getCregion());
                tblHdrclaimant.setCsname(updateHdrRequest.getUpdateTblHdrclaimant().getCsname());
                tblHdrclaimant.setCtitle(updateHdrRequest.getUpdateTblHdrclaimant().getCtitle());
                tblHdrclaimant.setIsJointTenancy(updateHdrRequest.getUpdateTblHdrclaimant().getIsJointTenancy());
                tblHdrclaimant.setTblHdrclaim(tblHdrclaim);
                tblHdrclaimant.setLastupdateuser(tblUser.getUsercode());
                if (updateHdrRequest.getUpdateTblHdrJointTenancy() != null && updateHdrRequest.getUpdateTblHdrJointTenancy().gettTitle() != null) {
                    tblHdrjointtenancy.setHdrjointtenancycode(tblHdrclaim.getTblHdrjointtenancies().get(0).getHdrjointtenancycode());
                    tblHdrjointtenancy.setTaddress1(updateHdrRequest.getUpdateTblHdrJointTenancy().getTaddress1());
                    tblHdrjointtenancy.setTaddress2(updateHdrRequest.getUpdateTblHdrJointTenancy().getTaddress2());
                    tblHdrjointtenancy.setTaddress3(updateHdrRequest.getUpdateTblHdrJointTenancy().getTaddress3());
                    tblHdrjointtenancy.setTpostalcode(updateHdrRequest.getUpdateTblHdrJointTenancy().getTpostalcode());
                    tblHdrjointtenancy.setTcity(updateHdrRequest.getUpdateTblHdrJointTenancy().getTcity());
                    tblHdrjointtenancy.settDob(updateHdrRequest.getUpdateTblHdrJointTenancy().gettDob());
                    tblHdrjointtenancy.settEmail(updateHdrRequest.getUpdateTblHdrJointTenancy().gettEmail());
                    tblHdrjointtenancy.settFname(updateHdrRequest.getUpdateTblHdrJointTenancy().gettFname());
                    tblHdrjointtenancy.settLandline(updateHdrRequest.getUpdateTblHdrJointTenancy().gettLandline());
                    tblHdrjointtenancy.settMname(updateHdrRequest.getUpdateTblHdrJointTenancy().gettMname());
                    tblHdrjointtenancy.settMobileno(updateHdrRequest.getUpdateTblHdrJointTenancy().gettMobileno());
                    tblHdrjointtenancy.settNicnumber(updateHdrRequest.getUpdateTblHdrJointTenancy().gettNinumber());
                    tblHdrjointtenancy.setTpostalcode(updateHdrRequest.getUpdateTblHdrJointTenancy().getTpostalcode());
                    tblHdrjointtenancy.setTregion(updateHdrRequest.getUpdateTblHdrJointTenancy().getTregion());
                    tblHdrjointtenancy.settSname(updateHdrRequest.getUpdateTblHdrJointTenancy().gettSname());
                    tblHdrjointtenancy.settTitle(updateHdrRequest.getUpdateTblHdrJointTenancy().gettTitle());
                    tblHdrjointtenancy.setTblHdrclaim(tblHdrclaim);
                    tblHdrjointtenancy.setLastupdateuser(tblUser.getUsercode());
                }

                tblHdrtenancy.setHdrtenancycode(updateHdrRequest.getUpdateTblHdrtenancy().getHdrtenancycode());
                tblHdrtenancy.setArrearsAmount(updateHdrRequest.getUpdateTblHdrtenancy().getArrearsAmount());
                tblHdrtenancy.setBenefitsDetail(updateHdrRequest.getUpdateTblHdrtenancy().getBenefitsDetail());
                tblHdrtenancy.setContributePm(updateHdrRequest.getUpdateTblHdrtenancy().getContributePm());
                tblHdrtenancy.setContributePw(updateHdrRequest.getUpdateTblHdrtenancy().getContributePw());
                tblHdrtenancy.setInArrears(updateHdrRequest.getUpdateTblHdrtenancy().getInArrears());
                tblHdrtenancy.setInPaymentPlan(updateHdrRequest.getUpdateTblHdrtenancy().getInPaymentPlan());
                tblHdrtenancy.setNoOfOccupants(updateHdrRequest.getUpdateTblHdrtenancy().getNoOfOccupants());
                tblHdrtenancy.setPaymentPlanPm(updateHdrRequest.getUpdateTblHdrtenancy().getPaymentPlanPm());
                tblHdrtenancy.setPaymentPlanPw(updateHdrRequest.getUpdateTblHdrtenancy().getPaymentPlanPw());
                tblHdrtenancy.setPropertyType(updateHdrRequest.getUpdateTblHdrtenancy().getPropertyType());
                tblHdrtenancy.setReceivingBenefits(updateHdrRequest.getUpdateTblHdrtenancy().getReceivingBenefits());
                tblHdrtenancy.setRentContribute(updateHdrRequest.getUpdateTblHdrtenancy().getRentContribute());
                tblHdrtenancy.setRentPm(updateHdrRequest.getUpdateTblHdrtenancy().getRentPm());
                tblHdrtenancy.setRentPw(updateHdrRequest.getUpdateTblHdrtenancy().getRentPw());
                tblHdrtenancy.setStartDate(updateHdrRequest.getUpdateTblHdrtenancy().getStartDate());
                tblHdrtenancy.setTblHdrclaim(tblHdrclaim);
                tblHdrtenancy.setLastupdateuser(tblUser.getUsercode());

                List<TblHdraffectedper> tblHdraffectedperList = new ArrayList<TblHdraffectedper>();
                List<UpdateTblHdraffectedper> updateTblHdraffectedperList = updateHdrRequest.getUpdateTblHdraffectedperList();
                if (updateTblHdraffectedperList != null && updateTblHdraffectedperList.size() > 0) {
                    for (UpdateTblHdraffectedper updateTblHdraffectedper : updateTblHdraffectedperList) {
                        TblHdraffectedper tblHdraffectedper = new TblHdraffectedper();

                        tblHdraffectedper.setHdraffectedpercode(updateTblHdraffectedper.getHdraffectedpercode());
                        tblHdraffectedper.setEvidenceDetail(updateTblHdraffectedper.getEvidenceDetail());
                        tblHdraffectedper.setMedicalEvidence(updateTblHdraffectedper.getMedicalEvidence());
                        tblHdraffectedper.setPersondob(updateTblHdraffectedper.getPersondob());
                        tblHdraffectedper.setPersonname(updateTblHdraffectedper.getPersonname());
                        tblHdraffectedper.setSufferingFrom(updateTblHdraffectedper.getSufferingFrom());
                        tblHdraffectedper.setTblHdrclaim(tblHdrclaim);
                        tblHdraffectedper.setLastupdateuser(tblUser.getUsercode());

                        tblHdraffectedperList.add(tblHdraffectedper);
                    }
                }

                List<TblHdraffectedroom> tblHdraffectedroomList = new ArrayList<TblHdraffectedroom>();
                List<UpdateTblHdraffectedroom> updateTblHdraffectedroomList = updateHdrRequest.getUpdateTblHdraffectedroomList();
                for (UpdateTblHdraffectedroom updateTblHdraffectedroom : updateTblHdraffectedroomList) {
                    TblHdraffectedroom tblHdraffectedroom = new TblHdraffectedroom();

                    tblHdraffectedroom.setHdraffectedroom(updateTblHdraffectedroom.getHdraffectedroom());
                    tblHdraffectedroom.setRoomName(updateTblHdraffectedroom.getRoomName());
                    tblHdraffectedroom.setDamageList(updateTblHdraffectedroom.getDamageList());
                    tblHdraffectedroom.setDisrepairDetail(updateTblHdraffectedroom.getDisrepairDetail());
                    tblHdraffectedroom.setLastReported(updateTblHdraffectedroom.getLastReported());
                    tblHdraffectedroom.setPersonalPropertydamage(updateTblHdraffectedroom.getPersonalPropertydamage());
                    tblHdraffectedroom.setReportDetails(updateTblHdraffectedroom.getReportDetails());
                    tblHdraffectedroom.setTblHdrclaim(tblHdrclaim);
                    tblHdraffectedroom.setLastupdateuser(tblUser.getUsercode());

                    tblHdraffectedroomList.add(tblHdraffectedroom);
                }
                TblHdrclaim tblHdrclaim1 = hdrService.updateHdrRequest(tblHdrclaim, tblHdrclaimant, tblHdrjointtenancy, tblHdrtenancy, tblHdraffectedperList, tblHdraffectedroomList);

                if (tblHdrclaim1 != null) {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Updated SuccessFully", tblHdrclaim1);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addHdrNotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addHdrNotes(@RequestBody HdrNoteRequest hdrNoteRequest,
                                                               HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addHdrNotes(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = hdrService.isHdrCaseAllowed(tblUser.getCompanycode(), "3");

            if (allowed) {
                if (!hdrNoteRequest.getHdrCode().isEmpty()) {
                    TblHdrnote tblHdrnote = new TblHdrnote();
                    TblHdrclaim tblHdrclaim = new TblHdrclaim();

                    tblHdrclaim.setHdrclaimcode(Long.valueOf(hdrNoteRequest.getHdrCode()));
                    tblHdrnote.setTblHdrclaim(tblHdrclaim);
                    tblHdrnote.setNote(hdrNoteRequest.getNote());
                    tblHdrnote.setUsercategorycode(hdrNoteRequest.getUserCatCode());
                    tblHdrnote.setCreatedon(new Date());
                    tblHdrnote.setUsercode(tblUser.getUsercode());

                    tblHdrnote = hdrService.addTblHdrNote(tblHdrnote);

                    if (tblHdrnote != null && tblHdrnote.getHdrnotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblHdrnote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addHdrNotes();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addESign(); ");
        try {
//            Locale backup = Locale.getDefault();
//            Locale.setDefault(Locale.ENGLISH);
            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("code")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }

            TblHdrclaim tblHdrclaim = hdrService.findHdrCaseByIdwithoutuser(addESign.getRtaCode());
            if (tblHdrclaim.getEsig() != null && tblHdrclaim.getEsig().equals("Y")) {
                List<TblHdrdocument> tblHdrdocuments = hdrService.getHdrDocumetsByHdrCode(tblHdrclaim.getHdrclaimcode());
                for (TblHdrdocument tblHdrdocument : tblHdrdocuments) {
                    if (tblHdrdocument.getDocumentType().equalsIgnoreCase("Esign")) {
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("signedDocument", tblHdrdocument.getDocbase64());
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "You Have Already Signed The Document",
                                map);
                    }
                }
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "You Have Already Signed The CFA", "You Have Already Signed The CFA");
            }


            TblHdrclaimant tblHdrclaimant = hdrService.findHdrClaimantByClaimId(tblHdrclaim.getHdrclaimcode());
            if (tblHdrclaim != null && tblHdrclaim.getHdrclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = hdrService.getCompanyProfileAgainstHdrCode(tblHdrclaim.getHdrclaimcode());
                PdfDocument doc = new PdfDocument();

                TblCompanydoc tblCompanydoc = hdrService.getCompanyDocs(tblCompanyprofile.getCompanycode(), tblHdrclaimant.getIsJointTenancy());

                byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                BufferedImage image1 = ImageIO.read(bis);
                bis.close();


                PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();

                // Iterate through form field names and set values from JSON
                for (PDField field : acroForm.getFieldTree()) {
                    if (field instanceof PDTextField) {
                        PDTextField textField = (PDTextField) field;
                        String fieldName = textField.getFullyQualifiedName();

                        while (keys.hasNext()) {
                            String key = keys.next();
                            if (!key.equals("code") && !key.equals("eSign")) {
                                if (fieldName.equals(key)) {
                                    textField.setValue(jsonObject.getString(key));
                                }
                            }
                        }

                        if (fieldName.startsWith("DB_")) {
                            String keyValue = hdrService.getHdrDbColumnValue(fieldName.replace("DB_", ""),
                                    tblHdrclaim.getHdrclaimcode());
                            if (keyValue.isEmpty()) {
                                textField.setValue("");
                            } else {
                                textField.setValue(keyValue);
                            }

                        }
                        if (fieldName.startsWith("DATE")) {
                            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                            Date date = new Date();
                            String s = formatter.format(date);
                            textField.setValue(s);
                        }

                        field.setReadOnly(true);

                    }
                }

                for (int a = 0; a < document.getNumberOfPages(); a++) {
                    PDPage p = document.getPage(a);
                    PDResources resources = p.getResources();
                    for (COSName xObjectName : resources.getXObjectNames()) {
                        PDXObject xObject = resources.getXObject(xObjectName);
                        if (xObject instanceof PDImageXObject) {
                            PDImageXObject original_img = ((PDImageXObject) xObject);
                            PDImageXObject pdImageXObject = LosslessFactory.createFromImage(document, image1);
                            resources.put(xObjectName, pdImageXObject);
                        }
                    }
                }


                String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\hdr\\";
                String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\hdr\\";

                File theDir = new File(UPLOADED_FOLDER + tblHdrclaim.getHdrclaimcode());
                if (!theDir.exists()) {
                    theDir.mkdirs();
                    String s = theDir.getPath() + "\\" + tblHdrclaim.getClaimcode() + "_Signed.pdf";
                    document.save(theDir.getPath() + "\\" + tblHdrclaim.getClaimcode() + "_Signed.pdf");
                } else {
                    document.save(theDir.getPath() + "\\" + tblHdrclaim.getClaimcode() + "_Signed.pdf");
                }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                File file = new File(theDir.getPath() + "\\" + tblHdrclaim.getClaimcode() + "_Signed.pdf");
                String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                TblTask tblHdrtaskdocument = new TblTask();
                tblHdrtaskdocument.setTaskcode(17);

                TblHdrdocument tblHdrdocument = new TblHdrdocument();
                tblHdrdocument.setTblHdrclaim(tblHdrclaim);
                tblHdrdocument.setDocbase64(encoded);
                tblHdrdocument.setDocname("Esign");
                tblHdrdocument.setDocumentType("Esign");
                tblHdrdocument.setCreatedon(new Date());
                tblHdrdocument.setUsercode("1");
                tblHdrdocument.setDocumentPath(
                        UPLOADED_SERVER + tblHdrclaim.getHdrclaimcode() + "\\" + tblHdrclaim.getClaimcode() + "_Signed.pdf");
                tblHdrdocument.setTblTask(tblHdrtaskdocument);

                tblHdrdocument = hdrService.saveTblHdrDocument(tblHdrdocument);
                if (tblHdrdocument != null && tblHdrdocument.getHdrdocumentscode() > 0) {
//                    Locale.setDefault(backup);
                    tblHdrclaim.setLastupdateuser("3");
                    tblHdrclaim.setEsig("Y");
                    tblHdrclaim.setEsigdate(new Date());
                    tblHdrclaim = hdrService.updateHdrCase(tblHdrclaim);

                    TblHdrtask tblHdrtask = hdrService.getHdrTaskByHdrCodeAndTaskCode(tblHdrclaim.getHdrclaimcode(), 17);

                    tblHdrtask.setStatus("C");
                    tblHdrtask.setRemarks("Completed");

                    TblHdrtask tblRtatask1 = hdrService.updateTblHdrTask(tblHdrtask);

                    HashMap<String, Object> map = new HashMap<>();
                    map.put("doc", encoded);
                    TblUser legalUser = hdrService.findByUserId("3");

                    // cleint Email
                    TblEmailTemplate tblEmailTemplate = rtaService.findByEmailType("esign-clnt-complete");
                    String clientbody = tblEmailTemplate.getEmailtemplate();

                    clientbody = clientbody.replace("[LEGAL_CLIENT_NAME]",
                            tblHdrclaimant.getCfname() + " "
                                    + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                                    + tblHdrclaimant.getCsname());
                    clientbody = clientbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());

                    hdrService.saveEmail(tblHdrclaimant.getCemail(), clientbody, "No Win No Fee Agreement ", tblHdrclaim, null, file.getPath());

                    // Introducer Email
                    tblEmailTemplate = rtaService.findByEmailType("esign-Int-sol-complete");
                    String Introducerbody = tblEmailTemplate.getEmailtemplate();
                    TblUser introducerUser = rtaService.findByUserId(String.valueOf(tblHdrclaim.getAdvisor()));

                    Introducerbody = Introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
                    Introducerbody = Introducerbody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
                    Introducerbody = Introducerbody.replace("[CLIENT_NAME]",
                            tblHdrclaimant.getCfname() + " "
                                    + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                                    + tblHdrclaimant.getCsname());
                    Introducerbody = Introducerbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                    Introducerbody = Introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());


                    hdrService.saveEmail(introducerUser.getUsername(), Introducerbody, tblHdrclaim.getClaimcode() + " | Processing Note", tblHdrclaim, null);

                    // PI department Email
                    String LegalPideptbody = tblEmailTemplate.getEmailtemplate();

                    LegalPideptbody = LegalPideptbody.replace("[USER_NAME]", legalUser.getLoginid());
                    LegalPideptbody = LegalPideptbody.replace("[CASE_NUMBER]", tblHdrclaim.getClaimcode());
                    LegalPideptbody = LegalPideptbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                    LegalPideptbody = LegalPideptbody.replace("[CLIENT_NAME]",
                            tblHdrclaimant.getCfname() + " "
                                    + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname() + " ")
                                    + tblHdrclaimant.getCsname());
                    LegalPideptbody = LegalPideptbody.replace("[CASE_URL]", getDataFromProperties("browser.url.hdr") + tblHdrclaim.getHdrclaimcode());


                    hdrService.saveEmail(legalUser.getUsername(), LegalPideptbody, tblHdrclaim.getClaimcode() + " | Processing Note", tblHdrclaim, null);


                    saveEsignSubmitStatus(String.valueOf(tblHdrclaim.getHdrclaimcode()), "3", request);

                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "You Have SuccessFully Signed The CFA, A Copy Of That CFA Has Been Sent To The Provided Email.", "Esign Completed");
                } else {
//                    Locale.setDefault(backup);
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Saving The Document..Esign Compeleted",
                            null);
                }
            } else {
//                Locale.setDefault(backup);
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No CFA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addESign(); ");
        try {

            TblHdrclaim tblHdrclaim = hdrService.findHdrCaseByIdwithoutuser(Long.valueOf(eSignFieldsRequest.getCode()));
            TblHdrclaimant tblHdrclaimant = hdrService.findHdrClaimantByClaimId(tblHdrclaim.getHdrclaimcode());
            if (!tblHdrclaim.getEsig().equals("Y")) {
                if (tblHdrclaim != null && tblHdrclaim.getHdrclaimcode() > 0) {
                    TblCompanyprofile tblCompanyprofile = hdrService
                            .getCompanyProfileAgainstHdrCode(tblHdrclaim.getHdrclaimcode());

                    TblCompanydoc tblCompanydoc = hdrService.getCompanyDocs(tblCompanyprofile.getCompanycode(), tblHdrclaimant.getIsJointTenancy());
                    if (tblCompanydoc == null) {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "No CFA document found", null);
                    }
                    HashMap<String, String> fields = new HashMap<>();
                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();


                    /////////////////////// FILL the CFA With Client DATA ////////////////////////


                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = hdrService.getHdrDbColumnValue(fieldName.replace("DB_", ""),
                                        tblHdrclaim.getHdrclaimcode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            } else if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            } else {
                                fields.put(fieldName, fieldName);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    /////////////////////// END OF FILL the CFA With Client DATA /////////////////

                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\hdr\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\hdr\\";

                    File theDir = new File(UPLOADED_FOLDER + tblHdrclaim.getHdrclaimcode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblHdrclaim.getClaimcode() + "_view.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblHdrclaim.getClaimcode() + "_view.pdf");
                    }

                    File file = new File(theDir.getPath() + "\\" + tblHdrclaim.getClaimcode() + "_view.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    fields.put("doc", encoded);
                    fields.put("isEsign", "N");

                    saveEsignOpenStatus(String.valueOf(tblHdrclaim.getHdrclaimcode()), "3", request);

                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", fields);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No HDR document found", null);
                }
            } else {
                List<TblHdrdocument> tblHdrdocuments = hdrService.getHdrDocumetsByHdrCode(tblHdrclaim.getHdrclaimcode());
                if (tblHdrdocuments != null && tblHdrdocuments.size() > 0) {
                    for (TblHdrdocument tblHdrdocument : tblHdrdocuments) {
                        if (tblHdrdocument.getTblTask().getTaskcode() == 17) {
                            HashMap<String, String> fields = new HashMap<>();
                            fields.put("doc", tblHdrdocument.getDocbase64());
                            fields.put("isEsign", "Y");
                            LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                            return getResponseFormat(HttpStatus.OK, "You have Already Signed The Document.", fields);
                        }
                    }
                }


                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Client has already signed the document", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/resendHdrEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> resendHdrEmail(@RequestBody ResendHdrMessageDto resendHdrMessageDto,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == resendHdrEmail(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == resendHdrEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblEmail tblEmail = hdrService.resendHdrEmail(resendHdrMessageDto.getHdrmessagecode());
            if (tblEmail != null && tblEmail.getEmailcode() > 0) {

                LOG.info("\n EXITING THIS METHOD == resendHdrEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Success", tblEmail);
            } else {
                LOG.info("\n EXITING THIS METHOD == resendHdrEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While resending Email", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == resendHdrEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == resendHdrEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/assigncasetosolicitorbyLA", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> assigncasetosolicitorbyLA(
            @RequestBody AssignHdrCasetoSolicitorOnHdr assignHdrCasetoSolicitorOnHdr, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblHdrclaim tblHdrclaim = hdrService.assignCaseToSolicitor(assignHdrCasetoSolicitorOnHdr, tblUser);
                if (tblHdrclaim != null) {
                    tblHdrclaim = hdrService.findHdrCaseById(Long.valueOf(assignHdrCasetoSolicitorOnHdr.getHdrClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblHdrclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }


            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnHdr", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnHdr(
            @RequestBody PerformActionOnHdrRequest performActionOnHdrRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == performActionOnHdr(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                if (performActionOnHdrRequest.getToStatus().equals("50")) {
                    List<TblHdrtask> tblHdrtasks = hdrService.getHdrTaskAgainstHdrCodeAndStatus(performActionOnHdrRequest.getHdrClaimCode(), "N");
                    List<TblHdrtask> tblHdrtasks1 = hdrService.getHdrTaskAgainstHdrCodeAndStatus(performActionOnHdrRequest.getHdrClaimCode(), "P");

                    TblHdrclaim tblHdrclaim = hdrService
                            .findHdrCaseById(Long.valueOf(performActionOnHdrRequest.getHdrClaimCode()), tblUser);

                    if (tblHdrtasks != null && tblHdrtasks.size() > 0) {
                        LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Some Task Are Not Performed", tblHdrclaim);
                    } else if (tblHdrtasks1 != null && tblHdrtasks1.size() > 0) {
                        LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Some Task Are In Pending State", tblHdrclaim);
                    } else {

                    }
                }


                TblHdrclaim hdrclaim = null;
                if (performActionOnHdrRequest.getToStatus().equals("59")) {
                    // reject status
                    hdrclaim = hdrService.performRejectCancelActionOnHdr(performActionOnHdrRequest.getHdrClaimCode(),
                            performActionOnHdrRequest.getToStatus(), performActionOnHdrRequest.getReason(), tblUser);

                } else if (performActionOnHdrRequest.getToStatus().equals("60")) {
                    //cancel status
                    hdrclaim = hdrService.performRejectCancelActionOnHdr(performActionOnHdrRequest.getHdrClaimCode(),
                            performActionOnHdrRequest.getToStatus(), performActionOnHdrRequest.getReason(), tblUser);

                } else if (performActionOnHdrRequest.getToStatus().equals("44")) {
                    //Accept status
                    hdrclaim = hdrService.performActionOnHdr(performActionOnHdrRequest.getHdrClaimCode(),
                            performActionOnHdrRequest.getToStatus(), tblUser);

                    List<TblHdrtask> tblHdrtasksList = hdrService.getHdrTaskAgainstHdrCode(performActionOnHdrRequest.getHdrClaimCode());
                    if (tblHdrtasksList.size() == 0) {
                        List<TblTask> tblTasks = hdrService.getTasksForHdr();
                        List<TblHdrtask> tblHdrtasks = new ArrayList<>();

                        for (TblTask tblTask : tblTasks) {
                            TblHdrtask tblHdrtask = new TblHdrtask();
                            tblHdrtask.setTblHdrclaim(hdrclaim);
                            tblHdrtask.setTaskcode(new BigDecimal(tblTask.getTaskcode()));
                            tblHdrtask.setStatus("N");
                            tblHdrtask.setCurrenttask("N");

                            tblHdrtasks.add(tblHdrtask);
                        }

                        tblHdrtasks = hdrService.saveHdrTasks(tblHdrtasks);
                    }
//                    hdrclaim = hdrService.performActionOnHdr(performActionOnHdrRequest.getHdrClaimCode(),
//                            "62", tblUser);
                } else if (performActionOnHdrRequest.getToStatus().equals("62")) {
                    //Accept status
                    hdrclaim = hdrService.performActionOnHdr(performActionOnHdrRequest.getHdrClaimCode(),
                            performActionOnHdrRequest.getToStatus(), tblUser);

                    List<TblHdrtask> tblHdrtasksList = hdrService.getHdrTaskAgainstHdrCode(performActionOnHdrRequest.getHdrClaimCode());
                    if (tblHdrtasksList.size() == 0) {
                        List<TblTask> tblTasks = hdrService.getTasksForHdr();
                        List<TblHdrtask> tblHdrtasks = new ArrayList<>();

                        for (TblTask tblTask : tblTasks) {
                            TblHdrtask tblHdrtask = new TblHdrtask();
                            tblHdrtask.setTblHdrclaim(hdrclaim);
                            tblHdrtask.setTaskcode(new BigDecimal(tblTask.getTaskcode()));
                            tblHdrtask.setStatus("P");
                            tblHdrtask.setCurrenttask("N");

                            tblHdrtasks.add(tblHdrtask);
                        }

                        tblHdrtasks = hdrService.saveHdrTasks(tblHdrtasks);
                    }
//                    hdrclaim = hdrService.performActionOnHdr(performActionOnHdrRequest.getHdrClaimCode(),
//                            "62", tblUser);
                } else if (performActionOnHdrRequest.getToStatus().equals("66")) {
                    //Revert status
//                    List<TblHdrlog> tblHdrlogs = hdrService.getHdrCaseLogs(performActionOnHdrRequest.getHdrClaimCode());
//                    TblHdrlog tblHdrlog = tblHdrlogs.get(0);
                    hdrclaim = hdrService.performRevertOnHdr(performActionOnHdrRequest.getHdrClaimCode(), tblUser);
                } else {
                    hdrclaim = hdrService.performActionOnHdr(performActionOnHdrRequest.getHdrClaimCode(),
                            performActionOnHdrRequest.getToStatus(), tblUser);
                }

                if (hdrclaim != null) {
                    TblHdrclaim tblHdrclaim = hdrService
                            .findHdrCaseById(Long.valueOf(performActionOnHdrRequest.getHdrClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblHdrclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == HdrPostApi \n METHOD == performActionOnHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("hdrClaimCode") long hdrClaimCode, @RequestParam("taskCode") long taskCode, @RequestParam(required = false) List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblHdrclaim tblHdrclaim = hdrService.findHdrCaseById(hdrClaimCode, tblUser);
            TblHdrsolicitor tblHdrsolicitor = hdrService.findHdrSolicitorByHdrCode(tblHdrclaim.getHdrclaimcode());
            TblHdrclaimant tblHdrclaimant = hdrService.findHdrClaimantByClaimId(tblHdrclaim.getHdrclaimcode());
            TblCompanyprofile companyprofile = hdrService.getCompanyProfile(tblHdrsolicitor.getCompanycode());
            if (tblHdrclaim != null && tblHdrclaim.getHdrclaimcode() > 0) {
                TblTask tblTask = hdrService.getTaskAgainstCode(taskCode);
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblHdrtask tblHdrtask = hdrService.getHdrTaskByHdrCodeAndTaskCode(tblHdrclaim.getHdrclaimcode(), taskCode);
                    String emailUrl = getDataFromProperties("esign.baseurl") + "hdr=" + tblHdrclaim.getHdrclaimcode();
                    if (tblTask.getTaskcode() == 17) {
                        TblCompanydoc tblCompanydoc = hdrService.getCompanyDocs(companyprofile.getCompanycode(), tblHdrclaimant.getIsJointTenancy());
                        if (tblCompanydoc == null) {
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "There is No CFA Attach To the Solicitor", null);
                        }


                        if (tblHdrclaim.getTblHdrclaimants().get(0).getCemail() != null) {

                            TblEmailTemplate tblEmailTemplate = getEmailTemplateByType("esign");

                            String body = tblEmailTemplate.getEmailtemplate();
                            body = body.replace("[LEGAL_CLIENT_NAME]", tblHdrclaimant.getCfname() + " "
                                    + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname()) + " " + tblHdrclaimant.getCsname());
                            body = body.replace("[SOLICITOR_COMPANY_NAME]", companyprofile.getName());
                            body = body.replace("[ESIGN_URL]", emailUrl);

                            hdrService.saveEmail(tblHdrclaimant.getCemail(), body, "No Win No Fee Agreement", tblHdrclaim, null);
                            tblHdrtask.setRemarks("Email Sent Successfully to Client");
                        }

                        LOG.info("\n Sending SMS ");
                        if (tblHdrclaimant.getCmobileno() != null && !tblHdrclaimant.getCmobileno().isEmpty()) {
                            LOG.info("\n SMS SEND");
                            TblEmailTemplate tblEmailTemplateSMS = getEmailTemplateByType("esign-SMS");
                            String message = tblEmailTemplateSMS.getEmailtemplate();
                            message = message.replace("[LEGAL_CLIENT_NAME]", tblHdrclaimant.getCtitle() + " " + tblHdrclaimant.getCfname() + " "
                                    + (tblHdrclaimant.getCmname() == null ? "" : tblHdrclaimant.getCmname()) + " " + tblHdrclaimant.getCsname());
                            message = message.replace("[SOLICITOR_COMPANY_NAME]", companyprofile.getName());
                            message = message.replace("[ESIGN_URL]", emailUrl);


                            sendSMS(tblHdrclaimant.getCmobileno(), message);
                            tblHdrtask.setRemarks("SMS Sent Successfully to Client");
                        }
                        tblHdrtask.setStatus("P");

                        saveEmailSmsEsignStatus(String.valueOf(tblHdrclaim.getHdrclaimcode()), "3", tblUser.getUsercode());
                    } else {
                        String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\hdr\\";
                        String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\hdr\\";
                        List<TblHdrdocument> saveTblHdrDocuments = new ArrayList<>();

                        File theDir = new File(UPLOADED_FOLDER + tblHdrclaim.getHdrclaimcode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }

                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);

                            TblHdrdocument tblHdrdocument = new TblHdrdocument();
                            tblHdrdocument.setDocumentPath(UPLOADED_SERVER + tblHdrclaim.getClaimcode() + "\\" + multipartFile.getOriginalFilename());
                            tblHdrdocument.setDocumentType(multipartFile.getContentType());
                            tblHdrdocument.setTblHdrclaim(tblHdrclaim);
                            tblHdrdocument.setTblTask(tblTask);

                            saveTblHdrDocuments.add(tblHdrdocument);
                        }

                        saveTblHdrDocuments = hdrService.saveTblHdrDocuments(saveTblHdrDocuments);
                        tblHdrtask.setStatus("C");
                        tblHdrtask.setRemarks("Completed");
                    }
                    TblHdrtask tblHdrtask1 = hdrService.updateTblHdrTask(tblHdrtask);
                    if (tblHdrtask1 != null && tblHdrtask1.getHdrtaskcode() > 0) {

                        List<TblHdrtask> tblHdrtasks = hdrService.findtblHdrTaskByHdrClaim(tblHdrclaim.getHdrclaimcode());
                        int count = 0;
                        for (TblHdrtask hdrtask : tblHdrtasks) {
                            if (hdrtask.getStatus().equals("C")) {
                                count++;
                            }
                        }

                        if (count == tblHdrtasks.size()) {
                            tblHdrclaim.setStatuscode(new BigDecimal(61));
                            tblHdrclaim.setLastupdateuser(tblUser.getUsercode());
                            tblHdrclaim = hdrService.updateHdrCase(tblHdrclaim);
                        }

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblHdrtask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/deleteHdrDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> deleteHdrDocument(
            @RequestBody DeleteRtaDocumentRequest deleteHdrDocumentRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == deleteHdrDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == deleteHdrDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(tblUser.getCompanycode());
            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {

                hdrService.deleteHdrDocument(deleteHdrDocumentRequest.getDoccode());
                return getResponseFormat(HttpStatus.OK, "Document Deleted", null);
            } else {
                LOG.info("\n EXITING THIS METHOD == deleteHdrDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Action", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrPostApi \n METHOD == deleteHdrDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == deleteHdrDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/setCurrentTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> setCurrentTask(@RequestBody CurrentTaskRequest currentTaskRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == setCurrentTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            int tblHdrtask1 = hdrService.updateCurrentTask(currentTaskRequest.getTaskCode(),
                    currentTaskRequest.getHdrCode(), currentTaskRequest.getCurrent());
            if (tblHdrtask1 > 0) {
                List<TblHdrtask> tblHdrtasksList = hdrService.getHdrTaskAgainstHdrCode(String.valueOf(currentTaskRequest.getHdrCode()));

                if (tblHdrtasksList != null && tblHdrtasksList.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Current Task Updated SuccessFully", tblHdrtasksList);
                } else {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Updating Task", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == setCurrentTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/hdrCaseReport", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> hdrCaseReport(
            @RequestBody HdrCaseReportRequest hdrCaseReportRequest, HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == hdrCaseReport(); ");
        try {

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == hdrCaseReport(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<ViewHdrcasereport> rtaCaseReport = hdrService.hdrCaseReport(hdrCaseReportRequest);

                if (rtaCaseReport != null) {
                    LOG.info("\n EXITING THIS METHOD == hdrCaseReport(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaCaseReport);
                } else {
                    LOG.info("\n EXITING THIS METHOD == hdrCaseReport(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == hdrCaseReport(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == hdrCaseReport();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == hdrCaseReport(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/filterHdrCases", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> filterHdrCases(@RequestBody FilterRequest filterRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == filterHdrCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == filterHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = hdrService.getCompanyProfile(tblUser.getCompanycode());
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "3");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == filterHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }


            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<HdrCasesList> viewHdrCaseslist = hdrService.getFiltreAuthHdrCasesIntroducers(filterRequest, tblUser.getUsercode());
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == filterHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == filterHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<HdrCasesList> viewHdrCaseslist = hdrService.getFiltreAuthHdrCasesSolicitors(filterRequest, tblUser.getUsercode());
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == filterHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == filterHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<HdrCasesList> viewHdrCaseslist = hdrService.getFiltreAuthHdrCasesLegalAssist(filterRequest);
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == filterHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == filterHdrCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == filterHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrPostApi \n METHOD == filterHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == filterHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/copyHdrToHdr", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> copyHdrToHdr(@RequestBody CopyRtaToRtaRequest copyHdrToHdrRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == copyHdrToHdr(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == copyHdrToHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblHdrclaim tblHdrclaimOld = hdrService.findHdrCaseById(Long.parseLong(copyHdrToHdrRequest.getHdrcode()), tblUser);

            TblHdrclaim tblHdrclaim = new TblHdrclaim();
            TblHdrclaimant tblHdrclaimant = new TblHdrclaimant();
            TblHdrjointtenancy tblHdrjointtenancy = null;
            TblHdrtenancy tblHdrtenancy = new TblHdrtenancy();
            List<TblHdraffectedper> tblHdraffectedperList = new ArrayList<TblHdraffectedper>();
            List<TblHdraffectedroom> tblHdraffectedroomList = new ArrayList<TblHdraffectedroom>();


            tblHdrclaim.setAprxReporteddate(tblHdrclaimOld.getAprxReporteddate());
            tblHdrclaim.setDefectLastreportedon(tblHdrclaimOld.getDefectLastreportedon());
            tblHdrclaim.setDefectReported(tblHdrclaimOld.getDefectReported());
            tblHdrclaim.setHealthAffected(tblHdrclaimOld.getHealthAffected());
            tblHdrclaim.setHealthRelDetails(tblHdrclaimOld.getHealthRelDetails());
            tblHdrclaim.setLlContactno(tblHdrclaimOld.getLlContactno());
            tblHdrclaim.setLlName(tblHdrclaimOld.getLlName());
            tblHdrclaim.setLlRespondDetail(tblHdrclaimOld.getLlRespondDetail());
            tblHdrclaim.setLlResponded(tblHdrclaimOld.getLlResponded());
            tblHdrclaim.setReportedToLl(tblHdrclaimOld.getReportedToLl());
            tblHdrclaim.setReportedToLlBy(tblHdrclaimOld.getReportedToLlBy());
            tblHdrclaim.setCreateuser(new BigDecimal(tblUser.getUsercode()));
            tblHdrclaim.setCreatedate(new Date());
            tblHdrclaim.setDefectFirstreportedon(tblHdrclaimOld.getDefectFirstreportedon());
            tblHdrclaim.setStatuscode(new BigDecimal(39));
            tblHdrclaim.setAdvisor(tblHdrclaimOld.getAdvisor());
            tblHdrclaim.setIntroducer(tblHdrclaimOld.getIntroducer());
            tblHdrclaim.setEsig("N");


            tblHdrclaimant.setCaddress1(tblHdrclaimOld.getTblHdrclaimants().get(0).getCaddress1());
            tblHdrclaimant.setCaddress2(tblHdrclaimOld.getTblHdrclaimants().get(0).getCaddress2());
            tblHdrclaimant.setCaddress3(tblHdrclaimOld.getTblHdrclaimants().get(0).getCaddress3());
            tblHdrclaimant.setCcity(tblHdrclaimOld.getTblHdrclaimants().get(0).getCcity());
            tblHdrclaimant.setCcontacttime(tblHdrclaimOld.getTblHdrclaimants().get(0).getCcontacttime());
            tblHdrclaimant.setCdob(tblHdrclaimOld.getTblHdrclaimants().get(0).getCdob());
            tblHdrclaimant.setCemail(tblHdrclaimOld.getTblHdrclaimants().get(0).getCemail());
            tblHdrclaimant.setCfname(tblHdrclaimOld.getTblHdrclaimants().get(0).getCfname());
            tblHdrclaimant.setIsJointTenancy(tblHdrclaimOld.getTblHdrclaimants().get(0).getIsJointTenancy());
            tblHdrclaimant.setClandline(tblHdrclaimOld.getTblHdrclaimants().get(0).getClandline());
            tblHdrclaimant.setCmname(tblHdrclaimOld.getTblHdrclaimants().get(0).getCmname());
            tblHdrclaimant.setCmobileno(tblHdrclaimOld.getTblHdrclaimants().get(0).getCmobileno());
            tblHdrclaimant.setCninumber(tblHdrclaimOld.getTblHdrclaimants().get(0).getCninumber());
            tblHdrclaimant.setCpostalcode(tblHdrclaimOld.getTblHdrclaimants().get(0).getCpostalcode());
            tblHdrclaimant.setCregion(tblHdrclaimOld.getTblHdrclaimants().get(0).getCregion());
            tblHdrclaimant.setCsname(tblHdrclaimOld.getTblHdrclaimants().get(0).getCsname());
            tblHdrclaimant.setCtitle(tblHdrclaimOld.getTblHdrclaimants().get(0).getCtitle());
            if (tblHdrclaimOld.getTblHdrjointtenancies().size() > 0) {
                if (tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettTitle() != null &&
                        !tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettTitle().isEmpty() &&
                        !tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettFname().isEmpty()) {
                    tblHdrjointtenancy = new TblHdrjointtenancy();
                    tblHdrjointtenancy.setTaddress1(tblHdrclaimOld.getTblHdrjointtenancies().get(0).getTaddress1());
                    tblHdrjointtenancy.setTaddress2(tblHdrclaimOld.getTblHdrjointtenancies().get(0).getTaddress2());
                    tblHdrjointtenancy.setTaddress3(tblHdrclaimOld.getTblHdrjointtenancies().get(0).getTaddress3());
                    tblHdrjointtenancy.setTcity(tblHdrclaimOld.getTblHdrjointtenancies().get(0).getTcity());
                    tblHdrjointtenancy.settDob(tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettDob());
                    tblHdrjointtenancy.settEmail(tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettEmail());
                    tblHdrjointtenancy.settFname(tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettFname());
                    tblHdrjointtenancy.settLandline(tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettLandline());
                    tblHdrjointtenancy.settMname(tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettMname());
                    tblHdrjointtenancy.settMobileno(tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettMobileno());
                    tblHdrjointtenancy.settNicnumber(tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettNicnumber());
                    tblHdrjointtenancy.setTpostalcode(tblHdrclaimOld.getTblHdrjointtenancies().get(0).getTpostalcode());
                    tblHdrjointtenancy.setTregion(tblHdrclaimOld.getTblHdrjointtenancies().get(0).getTregion());
                    tblHdrjointtenancy.settSname(tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettSname());
                    tblHdrjointtenancy.settTitle(tblHdrclaimOld.getTblHdrjointtenancies().get(0).gettTitle());
                }
            }
            if (tblHdrclaimOld.getTblHdrtenancies().size() > 0) {
                tblHdrtenancy.setArrearsAmount(tblHdrclaimOld.getTblHdrtenancies().get(0).getArrearsAmount());
                tblHdrtenancy.setBenefitsDetail(tblHdrclaimOld.getTblHdrtenancies().get(0).getBenefitsDetail());
                tblHdrtenancy.setContributePm(tblHdrclaimOld.getTblHdrtenancies().get(0).getContributePm());
                tblHdrtenancy.setContributePw(tblHdrclaimOld.getTblHdrtenancies().get(0).getContributePw());
                tblHdrtenancy.setInArrears(tblHdrclaimOld.getTblHdrtenancies().get(0).getInArrears());
                tblHdrtenancy.setInPaymentPlan(tblHdrclaimOld.getTblHdrtenancies().get(0).getInPaymentPlan());
                tblHdrtenancy.setNoOfOccupants(tblHdrclaimOld.getTblHdrtenancies().get(0).getNoOfOccupants());
                tblHdrtenancy.setPaymentPlanPm(tblHdrclaimOld.getTblHdrtenancies().get(0).getPaymentPlanPm());
                tblHdrtenancy.setPaymentPlanPw(tblHdrclaimOld.getTblHdrtenancies().get(0).getPaymentPlanPw());
                tblHdrtenancy.setPropertyType(tblHdrclaimOld.getTblHdrtenancies().get(0).getPropertyType());
                tblHdrtenancy.setReceivingBenefits(tblHdrclaimOld.getTblHdrtenancies().get(0).getReceivingBenefits());
                tblHdrtenancy.setRentContribute(tblHdrclaimOld.getTblHdrtenancies().get(0).getRentContribute());
                tblHdrtenancy.setRentPm(tblHdrclaimOld.getTblHdrtenancies().get(0).getRentPm());
                tblHdrtenancy.setRentPw(tblHdrclaimOld.getTblHdrtenancies().get(0).getRentPw());
                tblHdrtenancy.setStartDate(tblHdrclaimOld.getTblHdrtenancies().get(0).getStartDate());
            }


            List<TblHdraffectedper> saveTblHdraffectedperList = tblHdrclaimOld.getTblHdraffectedpers();
            if (saveTblHdraffectedperList != null && saveTblHdraffectedperList.size() > 0) {
                for (TblHdraffectedper saveTblHdraffectedper : saveTblHdraffectedperList) {
                    TblHdraffectedper tblHdraffectedper = new TblHdraffectedper();

                    tblHdraffectedper.setEvidenceDetail(saveTblHdraffectedper.getEvidenceDetail());
                    tblHdraffectedper.setMedicalEvidence(saveTblHdraffectedper.getMedicalEvidence());
                    tblHdraffectedper.setPersondob(saveTblHdraffectedper.getPersondob());
                    tblHdraffectedper.setPersonname(saveTblHdraffectedper.getPersonname());
                    tblHdraffectedper.setSufferingFrom(saveTblHdraffectedper.getSufferingFrom());

                    tblHdraffectedperList.add(tblHdraffectedper);
                }
            }


            List<TblHdraffectedroom> saveTblHdraffectedroomList = tblHdrclaimOld.getTblHdraffectedrooms();
            if (saveTblHdraffectedroomList != null && saveTblHdraffectedroomList.size() > 0) {
                for (TblHdraffectedroom saveTblHdraffectedroom : saveTblHdraffectedroomList) {
                    TblHdraffectedroom tblHdraffectedroom = new TblHdraffectedroom();

                    tblHdraffectedroom.setRoomName(saveTblHdraffectedroom.getRoomName());
                    tblHdraffectedroom.setDamageList(saveTblHdraffectedroom.getDamageList());
                    tblHdraffectedroom.setDisrepairDetail(saveTblHdraffectedroom.getDisrepairDetail());
                    tblHdraffectedroom.setLastReported(saveTblHdraffectedroom.getLastReported());
                    tblHdraffectedroom.setPersonalPropertydamage(saveTblHdraffectedroom.getPersonalPropertydamage());
                    tblHdraffectedroom.setReportDetails(saveTblHdraffectedroom.getReportDetails());

                    tblHdraffectedroomList.add(tblHdraffectedroom);
                }
            }


            TblCompanyprofile tblCompanyprofile = hdrService.getCompanyProfile(tblHdrclaim.getIntroducer().toString());
            tblHdrclaim.setClaimcode(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());


            TblHdrclaim tblHdrclaim1 = hdrService.saveHdrRequest(tblHdrclaim, tblHdrclaimant, tblHdrjointtenancy, tblHdrtenancy, tblHdraffectedperList, tblHdraffectedroomList);


            if (tblHdrclaim != null) {
                tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                tblCompanyprofile = hdrService.saveCompanyProfile(tblCompanyprofile);
                List<TblHdrdocument> tblHdrdocumentsOld = hdrService.getHdrDocumetsByHdrCode(tblHdrclaimOld.getHdrclaimcode());
                List<TblHdrdocument> tblHdrdocuments = new ArrayList<>();
                TblHdrdocument tblHdrdocument = null;
                for (TblHdrdocument tblHdrdocumentOld : tblHdrdocumentsOld) {
                    tblHdrdocument = new TblHdrdocument();
                    if (tblHdrdocumentOld.getTblTask() == null) {
                        tblHdrdocument.setDocumentPath(tblHdrdocumentOld.getDocumentPath());
                        tblHdrdocument.setDocumentType(tblHdrdocumentOld.getDocumentType());
                        tblHdrdocument.setTblHdrclaim(tblHdrclaim1);
                        tblHdrdocument.setCreatedon(new Date());
                        tblHdrdocument.setDocbase64(tblHdrdocumentOld.getDocbase64());
                        tblHdrdocument.setDocname(tblHdrdocumentOld.getDocname());
                        tblHdrdocument.setUsercode(tblUser.getUsercode());
                        tblHdrdocuments.add(tblHdrdocument);
                    }
                }

                tblHdrdocuments = hdrService.saveTblHdrDocuments(tblHdrdocuments);

                /// making copy log
                TblHdrclaimcopy copyLog = new TblHdrclaimcopy();
                TblHdrclaimcopy tblHdrclaimcopy = hdrService.checkforChildRecord(tblHdrclaimOld.getHdrclaimcode());
                if(tblHdrclaimcopy != null){
                    copyLog.setTblHdrclaim2(tblHdrclaimcopy.getTblHdrclaim2());
                }else{
                    copyLog.setTblHdrclaim2(tblHdrclaimOld);
                }
                copyLog.setTblHdrclaim1(tblHdrclaim);
                copyLog.setCreatedate(new Date());
                copyLog.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                 copyLog = hdrService.saveCopyLog(copyLog);

                /// end copy log



                copyHdrToHdrRequest.setHdrcode(String.valueOf(tblHdrclaim1.getHdrclaimcode()));
                return getResponseFormat(HttpStatus.OK, "Case Copied To New Case SuccessFully", copyHdrToHdrRequest);
            } else {
                LOG.info("\n EXITING THIS METHOD == copyHdrToHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Copying A Case", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaPostApi \n METHOD == copyHdrToHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == copyHdrToHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
