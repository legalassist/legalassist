package com.laportal.controller.tenancy;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.TenancyCaseList;
import com.laportal.dto.TenancyStatusCountList;
import com.laportal.model.*;
import com.laportal.service.tenancy.TenancyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/tenancy")
public class TenancyGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(TenancyGetApi.class);

    @Autowired
    private TenancyService tenancyService;

    @RequestMapping(value = "/getTenancyCases", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getTenancyCases(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = tenancyService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<TenancyCaseList> tenancyCaseLists = tenancyService.getAuthTenancyCasesIntroducers(tblUser.getUsercode());
                if (tenancyCaseLists != null && tenancyCaseLists.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tenancyCaseLists);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<TenancyCaseList> tenancyCaseLists = tenancyService.getAuthTenancyCasesSolicitors(tblUser.getUsercode());
                if (tenancyCaseLists != null && tenancyCaseLists.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tenancyCaseLists);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<TenancyCaseList> tenancyCaseLists = tenancyService.getAuthTenancyCasesLegalAssist();
                if (tenancyCaseLists != null && tenancyCaseLists.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tenancyCaseLists);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getTenancyCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getTenancyCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getTenancyCaseById/{tenancyCaseId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrCaseById(@PathVariable long tenancyCaseId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCaseById(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblTenancyclaim tblTenancyclaim = tenancyService.findTenancyCaseById(tenancyCaseId, tblUser);

            if (tblTenancyclaim.getTblTenancysolicitors() != null) {
                TblCompanyprofile tblCompanyprofile = tenancyService.getCompanyProfile(tblTenancyclaim.getTblTenancysolicitors().get(0).getCompanycode());
                tblTenancyclaim.getTblTenancysolicitors().get(0).setTblCompanyprofile(tblCompanyprofile);
            }

            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Record Found", tblTenancyclaim);
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllTenancyStatusCounts", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllTenancyStatusCounts(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = tenancyService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<TenancyStatusCountList> tenancyStatusCountsForIntroducers = tenancyService.getAllTenancyStatusCountsForIntroducers();

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tenancyStatusCountsForIntroducers);
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<TenancyStatusCountList> tenancyStatusCountsForSolicitor = tenancyService.getAllTenancyStatusCountsForSolicitor();

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tenancyStatusCountsForSolicitor);
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<TenancyStatusCountList> allTenancyStatusCounts = tenancyService.getAllTenancyStatusCounts();

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", allTenancyStatusCounts);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getAllTenancyStatusCounts();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllTenancyStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getTenancyCasesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getTenancyCasesByStatus(@PathVariable long statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getTenancyCasesByStatus(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<TenancyCaseList> tenancyCasesByStatus = tenancyService.getTenancyCasesByStatus(statusId);

            if (tenancyCasesByStatus != null && tenancyCasesByStatus.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getTenancyCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tenancyCasesByStatus);
            } else {
                LOG.info("\n EXITING THIS METHOD == getTenancyCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getTenancyCaseLogs/{tenancycode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getTenancyCaseLogs(@PathVariable String tenancycode,
                                                                      HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getTenancyCaseLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getTenancyCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblTenancylog> tenancyCaseLogs = tenancyService.getTenancyCaseLogs(tenancycode);

                if (tenancyCaseLogs != null && tenancyCaseLogs.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getTenancyCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tenancyCaseLogs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getTenancyCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getTenancyCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getTenancyCaseLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getTenancyCaseLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getTenancyCaseNotes/{tenancycode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getTenancyCaseNotes(@PathVariable String tenancycode,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getTenancyCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getTenancyCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblTenancynote> tblTenancynotes = tenancyService.getTenancyCaseNotes(tenancycode, tblUser);

                if (tblTenancynotes != null && tblTenancynotes.size() > 0) {
                    for (TblTenancynote tblTenancynote : tblTenancynotes
                    ) {
                        tblTenancynote.setTblTenancyclaim(null);
                    }
                    LOG.info("\n EXITING THIS METHOD == getTenancyCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblTenancynotes);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getTenancyCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getTenancyCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getTenancyCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getTenancyCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getTenancyCaseMessages/{tenancycode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getTenancyCaseMessages(@PathVariable String tenancycode,
                                                                          HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getTenancyCaseMessages(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblTenancymessage> tblTenancymessages = tenancyService.getTenancyCaseMessages(tenancycode);

                if (tblTenancymessages != null && tblTenancymessages.size() > 0) {
                    for (TblTenancymessage tblTenancymessage : tblTenancymessages) {
                        tblTenancymessage.setTblTenancyclaim(null);
                    }

                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblTenancymessages);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHdrCaseMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

}
