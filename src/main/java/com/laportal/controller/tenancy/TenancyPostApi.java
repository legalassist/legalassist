package com.laportal.controller.tenancy;


import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.tenancy.TenancyService;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.exporting.PdfImageInfo;
import com.spire.pdf.fields.PdfField;
import com.spire.pdf.graphics.PdfImage;
import com.spire.pdf.widget.PdfFormFieldWidgetCollection;
import com.spire.pdf.widget.PdfFormWidget;
import com.spire.pdf.widget.PdfPageCollection;
import com.spire.pdf.widget.PdfTextBoxFieldWidget;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.geom.Dimension2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

@RestController
@RequestMapping("/tenancy")
public class TenancyPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(TenancyPostApi.class);

    @Autowired
    private TenancyService tenancyService;

    @RequestMapping(value = "/addNewTenancyCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewHdrCase(@RequestBody SaveTenancyRequest saveTenancyRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addNewHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = tenancyService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                TblTenancyclaim tblTenancyclaim = new TblTenancyclaim();

                tblTenancyclaim.setAmountOfDepositReturned(saveTenancyRequest.getAmountOfDepositReturned());
                tblTenancyclaim.setArrears(saveTenancyRequest.getArrears());
                tblTenancyclaim.setClaimHousingBenefitsLiving(saveTenancyRequest.getClaimHousingBenefitsLiving());
                tblTenancyclaim.setContact(saveTenancyRequest.getContact());
                tblTenancyclaim.setCurrentAddress(saveTenancyRequest.getCurrentAddress());
                tblTenancyclaim.setDateMovedOn(saveTenancyRequest.getDateMovedOn());
                tblTenancyclaim.setDepositPaid(saveTenancyRequest.getDepositPaid());
                tblTenancyclaim.setDepositPaidDate(saveTenancyRequest.getDepositPaidDate());
                tblTenancyclaim.setDepositPaidMethod(saveTenancyRequest.getDepositPaidMethod());
                tblTenancyclaim.setDepositProtected(saveTenancyRequest.getDepositProtected());
                tblTenancyclaim.setDob(saveTenancyRequest.getDob());
                tblTenancyclaim.setEmail(saveTenancyRequest.getEmail());
                tblTenancyclaim.setFullName(saveTenancyRequest.getFullName());
                tblTenancyclaim.setHaveLandlordContact(saveTenancyRequest.getHaveLandlordContact());
                tblTenancyclaim.setLandlordAddress(saveTenancyRequest.getLandlordAddress());
                tblTenancyclaim.setLandlordContact(saveTenancyRequest.getLandlordContact());
                tblTenancyclaim.setLandlordDamageClaim(saveTenancyRequest.getLandlordDamageClaim());
                tblTenancyclaim.setLandlordEmailAddress(saveTenancyRequest.getLandlordEmailAddress());
                tblTenancyclaim.setLandlordLiveInProperty(saveTenancyRequest.getLandlordLiveInProperty());
                tblTenancyclaim.setNameOfAgency(saveTenancyRequest.getNameOfAgency());
                tblTenancyclaim.setNoOfTenancyAgrmtSigned(saveTenancyRequest.getNoOfTenancyAgrmtSigned());
                tblTenancyclaim.setOnlyTenantRegistered(saveTenancyRequest.getOnlyTenantRegistered());
                tblTenancyclaim.setPaidToLandlordOrAgent(saveTenancyRequest.getPaidToLandlordOrAgent());
                tblTenancyclaim.setPaymentPlan(saveTenancyRequest.getPaymentPlan());
                tblTenancyclaim.setReceivingBenefits(saveTenancyRequest.getReceivingBenefits());
                tblTenancyclaim.setRentAmount(saveTenancyRequest.getRentAmount());
                tblTenancyclaim.setRentArrears(saveTenancyRequest.getRentArrears());
                tblTenancyclaim.setRentArrearsDetail(saveTenancyRequest.getRentArrearsDetail());
                tblTenancyclaim.setStillLivingInProperty(saveTenancyRequest.getStillLivingInProperty());
                tblTenancyclaim.setTermDate(saveTenancyRequest.getTermDate());
                tblTenancyclaim.setThinkReason(saveTenancyRequest.getThinkReason());
                tblTenancyclaim.setWithheldReason(saveTenancyRequest.getWithheldReason());
                tblTenancyclaim.setStatus(39);
                tblTenancyclaim.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                tblTenancyclaim.setCreatedate(new Date());

                TblCompanyprofile tblCompanyprofile = tenancyService.getCompanyProfile(tblUser.getCompanycode());
                tblTenancyclaim.setTenancyCode(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());

                tblTenancyclaim = tenancyService.saveTenancyRequest(tblTenancyclaim);

                if (tblTenancyclaim != null) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = tenancyService.saveCompanyProfile(tblCompanyprofile);

                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblTenancyclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/updateTenancyCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateTenancyCase(@RequestBody UpdateTenancyRequest updateTenancyRequest,
                                                                     HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == updateHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = tenancyService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                TblTenancyclaim tblTenancyclaim = tenancyService.findTenancyCaseByIdWithoutUser(updateTenancyRequest.getTenancyclaimcode());

                tblTenancyclaim.setAmountOfDepositReturned(updateTenancyRequest.getAmountOfDepositReturned());
                tblTenancyclaim.setArrears(updateTenancyRequest.getArrears());
                tblTenancyclaim.setClaimHousingBenefitsLiving(updateTenancyRequest.getClaimHousingBenefitsLiving());
                tblTenancyclaim.setContact(updateTenancyRequest.getContact());
                tblTenancyclaim.setCurrentAddress(updateTenancyRequest.getCurrentAddress());
                tblTenancyclaim.setDateMovedOn(updateTenancyRequest.getDateMovedOn());
                tblTenancyclaim.setDepositPaid(updateTenancyRequest.getDepositPaid());
                tblTenancyclaim.setDepositPaidDate(updateTenancyRequest.getDepositPaidDate());
                tblTenancyclaim.setDepositPaidMethod(updateTenancyRequest.getDepositPaidMethod());
                tblTenancyclaim.setDepositProtected(updateTenancyRequest.getDepositProtected());
                tblTenancyclaim.setDob(updateTenancyRequest.getDob());
                tblTenancyclaim.setEmail(updateTenancyRequest.getEmail());
                tblTenancyclaim.setFullName(updateTenancyRequest.getFullName());
                tblTenancyclaim.setHaveLandlordContact(updateTenancyRequest.getHaveLandlordContact());
                tblTenancyclaim.setLandlordAddress(updateTenancyRequest.getLandlordAddress());
                tblTenancyclaim.setLandlordContact(updateTenancyRequest.getLandlordContact());
                tblTenancyclaim.setLandlordDamageClaim(updateTenancyRequest.getLandlordDamageClaim());
                tblTenancyclaim.setLandlordEmailAddress(updateTenancyRequest.getLandlordEmailAddress());
                tblTenancyclaim.setLandlordLiveInProperty(updateTenancyRequest.getLandlordLiveInProperty());
                tblTenancyclaim.setNameOfAgency(updateTenancyRequest.getNameOfAgency());
                tblTenancyclaim.setNoOfTenancyAgrmtSigned(updateTenancyRequest.getNoOfTenancyAgrmtSigned());
                tblTenancyclaim.setOnlyTenantRegistered(updateTenancyRequest.getOnlyTenantRegistered());
                tblTenancyclaim.setPaidToLandlordOrAgent(updateTenancyRequest.getPaidToLandlordOrAgent());
                tblTenancyclaim.setPaymentPlan(updateTenancyRequest.getPaymentPlan());
                tblTenancyclaim.setReceivingBenefits(updateTenancyRequest.getReceivingBenefits());
                tblTenancyclaim.setRentAmount(updateTenancyRequest.getRentAmount());
                tblTenancyclaim.setRentArrears(updateTenancyRequest.getRentArrears());
                tblTenancyclaim.setRentArrearsDetail(updateTenancyRequest.getRentArrearsDetail());
                tblTenancyclaim.setStillLivingInProperty(updateTenancyRequest.getStillLivingInProperty());
                tblTenancyclaim.setTermDate(updateTenancyRequest.getTermDate());
                tblTenancyclaim.setThinkReason(updateTenancyRequest.getThinkReason());
                tblTenancyclaim.setWithheldReason(updateTenancyRequest.getWithheldReason());
                tblTenancyclaim.setTenancyclaimcode(updateTenancyRequest.getTenancyclaimcode());
                tblTenancyclaim.setTenancyCode(updateTenancyRequest.getTenancyCode());
                tblTenancyclaim.setUpdatedate(new Date());

                tblTenancyclaim = tenancyService.saveTenancyRequest(tblTenancyclaim);

                if (tblTenancyclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblTenancyclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addTenancyNotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addTenancyNotes(@RequestBody TenancyNoteRequest tenancyNoteRequest,
                                                                   HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addTenancyNotes(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = tenancyService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                if (!tenancyNoteRequest.getTenancyCode().isEmpty()) {
                    TblTenancynote tblTenancynote = new TblTenancynote();
                    TblTenancyclaim tblTenancyclaim = new TblTenancyclaim();

                    tblTenancyclaim.setTenancyclaimcode(Long.valueOf(tenancyNoteRequest.getTenancyCode()));
                    tblTenancynote.setTblTenancyclaim(tblTenancyclaim);
                    tblTenancynote.setNote(tenancyNoteRequest.getNote());
                    tblTenancynote.setUsercategorycode(tenancyNoteRequest.getUserCatCode());
                    tblTenancynote.setCreatedon(new Date());
                    tblTenancynote.setUsercode(tblUser.getUsercode());

                    tblTenancynote = tenancyService.addTblTenancyNote(tblTenancynote);

                    if (tblTenancynote != null && tblTenancynote.getTenancynotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblTenancynote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addHdrNotes();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/resendTenancyEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> resendTenancyEmail(@RequestBody ResendTenancyMessageDto resendTenancyMessageDto,
                                                                      HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == resendRtaEmail(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblTenancymessage tblTenancymessage = tenancyService.getTblTenancyMessageById(resendTenancyMessageDto.getTenancymessagecode());
            if (tblTenancymessage != null && tblTenancymessage.getTenancymessagecode() > 0) {


                TblEmail tblEmail = new TblEmail();
                tblEmail.setSenflag(new BigDecimal(0));
                tblEmail.setEmailaddress(tblTenancymessage.getSentto());
                tblEmail.setEmailbody(tblTenancymessage.getMessage());
                tblEmail.setEmailsubject("Tenancy LegalAssist");
                tblEmail.setCreatedon(new Date());


                tblEmail = tenancyService.saveTblEmail(tblEmail);
                if (tblEmail != null && tblEmail.getEmailcode() > 0) {

                    LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", tblEmail);
                } else {
                    LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While resending Email", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Email Found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == resendRtaEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/assigncasetosolicitorbyLA", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> assigncasetosolicitorbyLA(
            @RequestBody AssignTenancyCasetoSolicitor assignTenancyCasetoSolicitor, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblTenancyclaim tblTenancyclaim = tenancyService.assignCaseToSolicitor(assignTenancyCasetoSolicitor, tblUser);
                if (tblTenancyclaim != null) {
                    tblTenancyclaim = tenancyService.findTenancyCaseById(Long.valueOf(assignTenancyCasetoSolicitor.getTenancyClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblTenancyclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }


            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnTenancy", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnTenancy(
            @RequestBody PerformActionOnTenancyRequest performActionOnTenancyRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnHdr(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblTenancyclaim tblTenancyclaim = null;
                if (performActionOnTenancyRequest.getToStatus().equals("59")) {
                    // reject status
                    tblTenancyclaim = tenancyService.performRejectCancelActionOnTenancy(performActionOnTenancyRequest.getTenancyClaimCode(),
                            performActionOnTenancyRequest.getToStatus(), performActionOnTenancyRequest.getReason(), tblUser);

                } else if (performActionOnTenancyRequest.getToStatus().equals("60")) {
                    //cancel status
                    tblTenancyclaim = tenancyService.performRejectCancelActionOnTenancy(performActionOnTenancyRequest.getTenancyClaimCode(),
                            performActionOnTenancyRequest.getToStatus(), performActionOnTenancyRequest.getReason(), tblUser);

                } else {
                    tblTenancyclaim = tenancyService.performActionOnTenancy(performActionOnTenancyRequest.getTenancyClaimCode(),
                            performActionOnTenancyRequest.getToStatus(), tblUser);
                }

                if (tblTenancyclaim != null) {
                    TblTenancyclaim tenancyclaim = tenancyService
                            .findTenancyCaseById(Long.valueOf(performActionOnTenancyRequest.getTenancyClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tenancyclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addESign(); ");
        try {
            Locale backup = Locale.getDefault();
            Locale.setDefault(Locale.ENGLISH);
            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("tenancyClaimCode")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }

            TblTenancyclaim tblTenancyclaim = tenancyService.findTenancyCaseByIdWithoutUser(addESign.getRtaCode());
            if (tblTenancyclaim != null && tblTenancyclaim.getTenancyclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = tenancyService.getCompanyProfileAgainstTenancyCode(tblTenancyclaim.getTenancyclaimcode());
                PdfDocument doc = new PdfDocument();

                String path = "";
                TblCompanydoc tblCompanydoc = new TblCompanydoc();
                tblCompanydoc = tenancyService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "E");

                doc.loadFromFile(tblCompanydoc.getPath());

                byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                BufferedImage image1 = ImageIO.read(bis);
                bis.close();

                PdfImage image = PdfImage.fromImage(image1);

                // Get the first worksheet
                PdfPageCollection page = doc.getPages();
                for (int j = 0; j < page.getCount(); j++) {
                    // Get the image information of the page
                    PdfImageInfo[] imageInfo = page.get(j).getImagesInfo();

                    // Loop through the image information
                    for (int i = 0; i < imageInfo.length; i++) {

                        // Get the bounds property of a specific image
                        Rectangle2D rect = imageInfo[i].getBounds();
                        // Get the x and y coordinates
                        System.out.println(String.format("The coordinate of image %d:（%f, %f）", i + 1, rect.getX(),
                                rect.getY()));

                        page.get(j).getCanvas().drawImage(image, rect.getX(), rect.getY(), rect.getWidth(),
                                rect.getHeight());
                    }
                }

                // get the form fields from the document
                PdfFormWidget form = (PdfFormWidget) doc.getForm();

                // get the form widget collection
                PdfFormFieldWidgetCollection formWidgetCollection = form.getFieldsWidget();

                // loop through the widget collection and fill each field with value
                for (int i = 0; i < formWidgetCollection.getCount(); i++) {
                    PdfField field = formWidgetCollection.get(i);
                    if (field instanceof PdfTextBoxFieldWidget) {
                        while (keys.hasNext()) {
                            String key = keys.next();
                            if (!key.equals("rtaCode") && !key.equals("eSign")) {
                                if (field.getName().equals(key)) {
                                    PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;
                                    textBoxField.setText(jsonObject.getString(key));
                                }
                            }
                        }

                        if (field.getName().startsWith("DB_")) {

                            PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;
                            String keyValue = tenancyService.gettenancyDbColumnValue(field.getName().replace("DB_", ""),
                                    tblTenancyclaim.getTenancyclaimcode());
                            if (keyValue.isEmpty()) {
                                Dimension2D dimension2D = new Dimension();
                                dimension2D.setSize(0, 0);
                                textBoxField.setSize(dimension2D);
                            } else {
                                textBoxField.setText(keyValue);
                            }


                        }
                        if (field.getName().startsWith("DATE")) {
                            PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;

                            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

                            Date date = new Date();
                            String s = formatter.format(date);
                            textBoxField.setText(s);
                        }
                        field.setFlatten(true);
                        field.setReadOnly(true);
                    }
                }
                String s = "C:\\Esigns\\" + tblTenancyclaim.getTenancyCode() + ".pdf";
                doc.saveToFile(s);
                File file = new File(s);
//                String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));
                String UPLOADED_FOLDER = "E://temp//";

                TblTenancydocument tblTenancydocument = new TblTenancydocument();
                tblTenancydocument.setTblTenancyclaim(tblTenancyclaim);
                tblTenancydocument.setDocumentType("E-Sign");
                tblTenancydocument.setDocumentPath(UPLOADED_FOLDER + tblTenancyclaim.getTenancyCode() + ".pdf");

                byte[] bytes = s.getBytes();
                Path path1 = Paths.get(UPLOADED_FOLDER + tblTenancyclaim.getTenancyCode() + ".pdf");
                Files.write(path1, bytes);

                tblTenancydocument = tenancyService.saveTblTenancyDocument(tblTenancydocument);
                if (tblTenancydocument != null && tblTenancydocument.getTenancydocumentscode() > 0) {
                    Locale.setDefault(backup);
                    tblTenancyclaim.setEsig("Y");
                    tblTenancyclaim = tenancyService.saveTenancyRequest(tblTenancyclaim);
                    TblTenancytask tblTenancytask = tenancyService.getTenancyTaskByTenancyCodeAndTaskCode(tblTenancyclaim.getTenancyclaimcode(), 1);

                    tblTenancytask.setStatus("C");
                    tblTenancytask.setRemarks("Completed");

                    TblTenancytask tblTenancytask1 = tenancyService.updateTblTenancyTask(tblTenancytask);
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", "Esign Completed");
                } else {
                    Locale.setDefault(backup);
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Saving The Document..Esign Compeleted",
                            null);
                }
            } else {
                Locale.setDefault(backup);
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addESign(); ");
        try {

            TblTenancyclaim tblTenancyclaim = tenancyService.findTenancyCaseByIdWithoutUser(Long.valueOf(eSignFieldsRequest.getCode()));
            if (tblTenancyclaim != null && tblTenancyclaim.getTenancyclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = tenancyService
                        .getCompanyProfileAgainstTenancyCode(tblTenancyclaim.getTenancyclaimcode());

                String path = "";
                TblCompanydoc tblCompanydoc = new TblCompanydoc();
                tblCompanydoc = tenancyService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "E");

                HashMap<String, String> fields = new HashMap<>();

                PdfDocument doc = new PdfDocument();
                doc.loadFromFile(tblCompanydoc.getPath());

                PdfFormWidget form = (PdfFormWidget) doc.getForm();
                PdfFormFieldWidgetCollection formWidgetCollection = form.getFieldsWidget();
                for (int i = 0; i < formWidgetCollection.getCount(); i++) {
                    PdfField field = formWidgetCollection.get(i);
                    if (!field.getName().startsWith("DB_") && !field.getName().equals("DATE")) {
                        fields.put(field.getName(), field.getName());
                    }
                }

                String viewDocPath = tblCompanydoc.getPath().replace("Esigns", "ViewEsigns");
                File file = new File(viewDocPath);
                String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                fields.put("doc", encoded);

                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Success", fields);
            } else {
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No HDR document found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("hdrClaimCode") long hdrClaimCode, @RequestParam("taskCode") long taskCode, @RequestParam("multipartFiles") List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblTenancyclaim tblTenancyclaim = tenancyService.findTenancyCaseById(hdrClaimCode, tblUser);
            if (tblTenancyclaim != null && tblTenancyclaim.getTenancyclaimcode() > 0) {
                TblTask tblTask = tenancyService.getTaskAgainstCode(taskCode);
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblTenancytask tblTenancytask = tenancyService.getTenancyTaskByTenancyCodeAndTaskCode(tblTenancyclaim.getTenancyclaimcode(), taskCode);

                    if (tblTask.getTaskcode() == 1) {
                        if (tblTenancyclaim.getEmail() == null) {
                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "No Client Email Address Found", null);
                        }
                        String emailUrl = "115.186.147.30:8888/esign?hdr=" + tblTenancyclaim.getTenancyclaimcode();

                        TblEmail tblEmail = new TblEmail();
                        tblEmail.setSenflag(new BigDecimal(0));
                        tblEmail.setEmailaddress(tblTenancyclaim.getEmail());
                        tblEmail.setEmailbody("Email sent to perform esign on mentioned hdr claim number: " + tblTenancyclaim.getTenancyCode()
                                + "\n Click on the below link to perform action \n" + emailUrl);
                        tblEmail.setEmailsubject("HDR CASE" + tblTenancyclaim.getTenancyCode());
                        tblEmail.setCreatedon(new Date());

                        tblEmail = tenancyService.saveTblEmail(tblEmail);

                        tblTenancytask.setStatus("P");
                        tblTenancytask.setRemarks("Email Sent Successfully to Client");
                    } else {
                        String UPLOADED_FOLDER = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\";
                        String UPLOADED_SERVER = "http:\\115.186.147.30:8888\\";
                        List<TblTenancydocument> tblTenancydocuments = new ArrayList<>();

                        File theDir = new File(UPLOADED_FOLDER + tblTenancyclaim.getTenancyCode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }

                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);

                            TblTenancydocument tblTenancydocument = new TblTenancydocument();
                            tblTenancydocument.setDocumentPath(UPLOADED_SERVER + tblTenancyclaim.getTenancyCode() + "\\" + multipartFile.getOriginalFilename());
                            tblTenancydocument.setDocumentType(multipartFile.getContentType());
                            tblTenancydocument.setTblTenancyclaim(tblTenancyclaim);

                            tblTenancydocuments.add(tblTenancydocument);
                        }

                        tblTenancydocuments = tenancyService.saveTblTenancyDocuments(tblTenancydocuments);
                        tblTenancytask.setStatus("C");
                        tblTenancytask.setRemarks("Completed");
                    }
                    TblTenancytask tblHdrtask1 = tenancyService.updateTblTenancyTask(tblTenancytask);
                    if (tblHdrtask1 != null && tblHdrtask1.getTenancytaskcode() > 0) {

                        List<TblTenancytask> tblTenancytasks = tenancyService.findTblTenancyTaskByTenancyClaimCode(tblTenancyclaim.getTenancyclaimcode());
                        int count = 0;
                        for (TblTenancytask tenancytask : tblTenancytasks) {
                            if (tenancytask.getStatus().equals("C")) {
                                count++;
                            }
                        }

                        if (count == tblTenancytasks.size()) {
                            tblTenancyclaim.setStatus(61);

                            tblTenancyclaim = tenancyService.saveTenancyRequest(tblTenancyclaim);
                        }

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblHdrtask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
