package com.laportal.controller.abstracts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.laportal.dto.LoginResponse;
import com.laportal.model.*;
import com.laportal.service.abstracts.AbstractService;
import com.laportal.service.user.UsersService;
import com.laportal.util.JWTSecurity;
import com.textmagic.sdk.ApiClient;
import com.textmagic.sdk.ApiException;
import com.textmagic.sdk.Configuration;
import com.textmagic.sdk.api.TextMagicApi;
import com.textmagic.sdk.auth.HttpBasicAuth;
import com.textmagic.sdk.model.PingResponse;
import com.textmagic.sdk.model.SendMessageInputObject;
import com.textmagic.sdk.model.SendMessageResponse;
import eu.bitwalker.useragentutils.UserAgent;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;


@Component
@EnableScheduling
public class AbstractApi {
    private static final String[] IP_HEADER_CANDIDATES = {"X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR", "HTTP_X_FORWARDED", "HTTP_X_CLUSTER_CLIENT_IP", "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR", "HTTP_FORWARDED", "HTTP_VIA", "REMOTE_ADDR"};
    @Autowired
    DataSource dataSource;
    Logger LOG = LoggerFactory.getLogger(AbstractApi.class);
    @SuppressWarnings("unused")
    private MessageSource messageSource;
    @Autowired
    private AbstractService abstractService;
    @Autowired
    private UsersService usersService;
    @SuppressWarnings("unused")
    @Autowired
    private Environment env;

    public static String getClientIpAddress(HttpServletRequest request) {
        for (String header : IP_HEADER_CANDIDATES) {
            String ip = request.getHeader(header);
            if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        return request.getRemoteAddr();
    }

    public static String generatePassword() {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(8);
        for (int i = 0; i < 6; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }

    public ResponseEntity<HashMap<String, Object>> getResponseFormat(HttpStatus status, String message, Object data) {

        int responsestatus;
        if (status.equals(HttpStatus.OK) || status.value() == 200) {
            responsestatus = 1;
        } else {
            responsestatus = 0;
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("responsecode", responsestatus);
        map.put("messages", message);
        map.put("data", data);
        return ResponseEntity.status(status).body(map);
    }

    public ResponseEntity<HashMap<String, Object>> getSingleCustomizeResponseFormat(HttpStatus status, String message,
                                                                                    Object data, String key) {
        int responsestatus;
        if (status.equals(HttpStatus.OK) || status.value() == 200) {
            responsestatus = 1;
        } else {
            responsestatus = 0;
        }

        HashMap<String, Object> datamap = new HashMap<>();
        datamap.put(key, data);

        HashMap<String, Object> map = new HashMap<>();
        map.put("responsecode", responsestatus);
        map.put("messages", message);
        map.put("data", datamap);
        return ResponseEntity.status(status).body(map);
    }

    public ResponseEntity<HashMap<String, Object>> getMultipleCustomizeResponseFormat(HttpStatus status, String message,
                                                                                      HashMap<String, Object> datamap) {
        int responsestatus;
        if (status.equals(HttpStatus.OK) || status.value() == 200) {
            responsestatus = 1;
        } else {
            responsestatus = 0;
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("responsecode", responsestatus);
        map.put("messages", message);
        map.put("data", datamap);
        return ResponseEntity.status(status).body(map);
    }

    public HashMap<String, Object> getResponseFromPostAPI(String authorization, String clientId,
                                                          Map<String, Object> postParam, String url) {
        HashMap<String, Object> responseMap = new HashMap<>();
        try {

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "bearer " + authorization);
            headers.add("X-IBM-Client-Id", clientId);
//			headers.add("accept", "application/json");
//			headers.add("content-type", "application/json");

            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();

//			if (postParam != null && !postParam.isEmpty()) {
//
//				for (Map.Entry<String, Object> paramEntry : postParam.entrySet()) {
//					map.add(paramEntry.getKey(), String.valueOf(paramEntry.getValue()));
//				}
//			}
            ObjectMapper objectMapper = new ObjectMapper();
            HttpEntity<String> request = new HttpEntity<String>(objectMapper.writeValueAsString(postParam), headers);
            ResponseEntity<String> bookmeResponse = new RestTemplate().postForEntity(url, request, String.class);

            @SuppressWarnings("unused")
            String entityResponse = (String) bookmeResponse.getBody();
            HttpStatus entitystatus = (HttpStatus) bookmeResponse.getStatusCode();
            responseMap.put("status", String.valueOf(entitystatus.value()));
            responseMap.put("data", bookmeResponse.getBody());
            return responseMap;

        } catch (RestClientException e) {
            // process exception
            if (e instanceof HttpStatusCodeException) {
                String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
                System.out.println(errorResponse);
                responseMap.put("status", "0");
                responseMap.put("data", errorResponse);
                return responseMap;
            }
        } catch (Exception e) {
            responseMap.put("status", "0");
            responseMap.put("data", e.getMessage());
            return responseMap;
        }

        return null;

    }

    public HashMap<String, Object> getResponseFromGetAPI(String clientId, String clientSecret, String url) {
        HashMap<String, Object> responseMap = new HashMap<>();
        try {

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            headers.add("clientId", clientId);
            headers.add("clientSecret", clientSecret);

            ResponseEntity<String> bookmeResponse = new RestTemplate().getForEntity(url, String.class);

            @SuppressWarnings("unused")
            String entityResponse = (String) bookmeResponse.getBody();
            HttpStatus entitystatus = (HttpStatus) bookmeResponse.getStatusCode();
            responseMap.put("status", String.valueOf(entitystatus.value()));
            responseMap.put("data", bookmeResponse.getBody());
            return responseMap;

        } catch (RestClientException e) {
            // process exception
            if (e instanceof HttpStatusCodeException) {
                String errorResponse = ((HttpStatusCodeException) e).getResponseBodyAsString();
                System.out.println(errorResponse);
                responseMap.put("status", "0");
                responseMap.put("data", errorResponse);
                return responseMap;
            }
        } catch (Exception e) {
            responseMap.put("status", "0");
            responseMap.put("data", e.getMessage());
            return responseMap;
        }

        return null;

    }

    public String getrequestHeadervalue(HttpServletRequest request, String key) {
        return (String) request.getHeader(key);
    }

    public ResponseEntity<HashMap<String, Object>> getResponseFormatWithHTTPStatus(HttpStatus status, String message,
                                                                                   Object data) {

        int responsestatus;
        if (status.equals(HttpStatus.OK) || status.value() == 200) {
            responsestatus = 1;
        } else {
            responsestatus = 0;
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("responsecode", responsestatus);
        map.put("messages", message);
        map.put("data", data);
        return ResponseEntity.status(status).body(map);
    }

    public TblUser getLoggedUserData(String token) {
        if (token != null && token.startsWith("Bearer")) {
            TblLogintoken tblLogintoken = abstractService.verifyToken(token);
            if (tblLogintoken != null) {
                LoginResponse loginResponse = new LoginResponse();
                String jwt = token.substring(7);
                JWTSecurity jwtSecurity = new JWTSecurity();
                HashMap<String, Object> loginResponseData = jwtSecurity.parseJWT(jwt);
                if (loginResponseData != null && !loginResponseData.get("Expired").equals("Y")) {
                    TblUser tblUser = abstractService.getUserById(loginResponseData.get("userCode").toString());
                    if (tblUser != null) {
                        return tblUser;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public void sendEmail(List<String> toEmail, List<String> ccEmail, String subject, String body, List<String> filePaths,
                          List<String> fileNames) {
        LOG.info("\n\n\nINSIDE \n CLASS == Abstract API \n METHOD == sendEmail(); ");
        String smtpHost = env.getProperty("spring.mail.host");
        String fromEmail = env.getProperty("spring.mail.username");
        String emailPassword = env.getProperty("spring.mail.password");
        String smtpPort = env.getProperty("spring.mail.port");

        Properties properties = System.getProperties();
        properties.put("mail.smtp.host", smtpHost);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", smtpPort);
        properties.put("mail.smtp.starttls.required", "true");

        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail, emailPassword);
            }
        });

//        session.setDebug(true);
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromEmail));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));

            InternetAddress[] sendTo = new InternetAddress[toEmail.size()];
            for (int i = 0; i < toEmail.size(); i++) {
                sendTo[i] = new InternetAddress(toEmail.get(i));
            }
            message.setRecipients(javax.mail.internet.MimeMessage.RecipientType.TO, sendTo);


            if (ccEmail != null) {
                InternetAddress[] sendCc = new InternetAddress[ccEmail.size()];
                for (int i = 0; i < ccEmail.size(); i++) {
                    sendCc[i] = new InternetAddress(ccEmail.get(i));
                }

                message.setRecipients(javax.mail.internet.MimeMessage.RecipientType.CC, sendCc);
            }
            message.setSubject(subject);
            message.setContent(body, "text/html");


            if (fileNames != null && fileNames.size() > 0) {
                Multipart multipart = new MimeMultipart();
                // 4) create new MimeBodyPart object and set DataHandler object to this object
                for (int i = 0; i < filePaths.size(); i++) {
                    MimeBodyPart messageBodyPart = new MimeBodyPart();
                    FileDataSource source = new FileDataSource(filePaths.get(i));
                    messageBodyPart.setDataHandler(new DataHandler(source));
                    messageBodyPart.setFileName(fileNames.get(i));
                    multipart.addBodyPart(messageBodyPart);
                }
                message.setContent(multipart);
            }


            LOG.info("\n  Sending Email");
            Transport.send(message);
            LOG.info("\n Email Sent SuccessFully\n");
        } catch (MessagingException mex) {
            mex.printStackTrace();
            LOG.info("\n Exception while Sending email :::\n" + mex.getLocalizedMessage());
            System.out.println(mex.getMessage());
        }
    }

    @Async
    @Scheduled(fixedDelay = 60000) // This Means Delay between previous job. Delay must be in milliseconds
    public void sendMessageToCustomer() {
        try {
            List<TblEmail> pendingEmails = abstractService.getPendingEmails();
            if (pendingEmails != null) {
                for (TblEmail tblEmail : pendingEmails) {
                    List<String> fileNames = null;
                    List<String> filePaths = null;
                    if (tblEmail.getEmailattachment() != null) {
                        fileNames = new ArrayList<>();
                        filePaths = Arrays.asList(tblEmail.getEmailattachment().split(","));
                        for (String name : filePaths) {
                            fileNames.add(name.substring(name.lastIndexOf('\\') + 1));
                        }
                    }

                    String[] toEmails = tblEmail.getEmailaddress().split(",");
                    List<String> toEmailAddresses = new ArrayList<String>();
                    toEmailAddresses.addAll(Arrays.asList(toEmails));

                    List<String> ccEmailAddresses = null;
                    if (tblEmail.getCcemailaddress() != null) {
                        String[] ccEmails = tblEmail.getCcemailaddress().split(",");
                        ccEmailAddresses = new ArrayList<String>();
                        ccEmailAddresses.addAll(Arrays.asList(ccEmails));
                    }

                    sendEmail(toEmailAddresses, ccEmailAddresses, tblEmail.getEmailsubject(), tblEmail.getEmailbody(),
                            filePaths, fileNames);
                    tblEmail.setSenflag(new BigDecimal(1));
                    tblEmail = abstractService.saveTblEmail(tblEmail);
                }
            }
            List<TblBroadcast> pendingBroadCasts = abstractService.getPendingBroadCasts();
            for (TblBroadcast tblBroadcast : pendingBroadCasts) {

                String sechduleDate = tblBroadcast.getSechduleDate();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date dateToCompare = dateFormat.parse(sechduleDate); // convert string to Date object
                Date currentDate = new Date(); // get the current date

                if (dateToCompare.compareTo(currentDate) >= 0) {
                    String[] toEmails = tblBroadcast.getToUsers().split(",");
                    List<String> toEmailAddresses = new ArrayList<String>();
                    toEmailAddresses.addAll(Arrays.asList(toEmails));
                    sendEmail(toEmailAddresses, null, tblBroadcast.getSubject(), tblBroadcast.getMessage(),
                            null, null);
                    tblBroadcast.setStatus("Y");
                    tblBroadcast = usersService.createBroadCastMessage(tblBroadcast);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void callPdfJasperReport(String reportName, String reportOutName,
                                    HashMap<String, Object> parameterMap) {
        try {
            JasperReport jp = JasperCompileManager.compileReport(reportName);
            JasperPrint jasperPrint = null;

            jasperPrint = JasperFillManager.fillReport(jp, parameterMap, dataSource.getConnection());

            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setParameter(JRPdfExporterParameter.JASPER_PRINT, jasperPrint);
            pdfExporter.setParameter(JRPdfExporterParameter.OUTPUT_FILE_NAME, reportOutName);

            pdfExporter.exportReport();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TblEmailTemplate getEmailTemplate(String userCatCode) {
        return abstractService.getEmailTemplate(userCatCode);
    }

    public TblEmailTemplate getEmailTemplateByType(String emailType) {
        return abstractService.getEmailTemplateByType(emailType);
    }

    public String getDataFromProperties(String key) {
        return env.getProperty(key);
    }

    public boolean sendSMS(String toNumber, String message) {
        if (!toNumber.startsWith("+44") && toNumber.startsWith("0")) {
            toNumber = toNumber.replaceFirst("0", "+44");
        }
        LOG.info("\n sending SMS to " + toNumber);
        ApiClient defaultClient = Configuration.getDefaultApiClient();

        // put your Username and API Key from https://my.textmagic.com/online/api/rest-api/keys page.
        HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
        BasicAuth.setUsername(env.getProperty("TextMagic.username"));
        BasicAuth.setPassword(env.getProperty("TextMagic.apikey"));

        TextMagicApi apiInstance = new TextMagicApi();

        // Simple ping request example
        try {
            PingResponse response = apiInstance.ping();
            LOG.info("\n SMS PING === " + response.getPing());
            System.out.println(response.getPing());
        } catch (ApiException e) {
            System.err.println("Exception when calling ping");
            LOG.info("\n Exception" + e.getLocalizedMessage());
            e.printStackTrace();
            return false;
        }

        // Send a new message request example
        SendMessageInputObject sendMessageInputObject = new SendMessageInputObject();
        sendMessageInputObject.setText(message);
        sendMessageInputObject.setPhones(toNumber);

        try {

            SendMessageResponse response = apiInstance.sendMessage(sendMessageInputObject);
            System.out.println(response.getId());
            LOG.info("\n response ID == " + response.getId());
            return true;
        } catch (ApiException e) {
            System.err.println("Exception when calling sendMessage");
            LOG.info("\n Exception" + e.getLocalizedMessage());
            e.printStackTrace();
            return false;
        }

    }

    public Date addDaysToDate(int daysToAdd) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, daysToAdd);
        return calendar.getTime();
    }

    public Date addDaysToSpecificDate(Date specificDate, int daysToAdd) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(specificDate);
        calendar.add(Calendar.DAY_OF_MONTH, daysToAdd);
        return calendar.getTime();
    }

    public TblEsignStatus saveEmailSmsEsignStatus(String claimCode, String compaignCode, String userCode) {
        TblEsignStatus tblEsignStatusCheck = abstractService.getEsignStatus(claimCode, compaignCode);
        TblEsignStatus tblEsignStatus = new TblEsignStatus();
        if (tblEsignStatusCheck != null) {
            tblEsignStatus = tblEsignStatusCheck;
        }


        tblEsignStatus.setEmailSent("Y");
        tblEsignStatus.setEmailDate(new Date());

        tblEsignStatus.setSmsSent("Y");
        tblEsignStatus.setSmsDate(new Date());

        tblEsignStatus.setCreateuser(userCode);
        tblEsignStatus.setClaimcode(new BigDecimal(claimCode));
        TblCompaign tblCompaign = new TblCompaign();
        tblCompaign.setCompaigncode(compaignCode);
        tblEsignStatus.setTblCompaign(tblCompaign);

        return abstractService.saveEsignStatus(tblEsignStatus);
    }

    public TblEsignStatus saveEsignOpenStatus(String claimCode, String compaignCode, HttpServletRequest request) {
        TblEsignStatus tblEsignStatus = abstractService.getEsignStatus(claimCode, compaignCode);
        if (tblEsignStatus != null) {


            String userAgentString = request.getHeader("User-Agent");

            // Parse user agent string
            UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);

            // Get browser and device information
            String browserName = userAgent.getBrowser().getName();
            String deviceType = userAgent.getOperatingSystem().getDeviceType().getName();


            tblEsignStatus.setClientBrowser(browserName);
            tblEsignStatus.setClientDevice(deviceType);
            tblEsignStatus.setEsignOpenDate(new Date());

            tblEsignStatus.setLastupdatedate(new Date());
            tblEsignStatus.setUpdateindex(tblEsignStatus.getUpdateindex() != null ? new BigDecimal(tblEsignStatus.getUpdateindex().longValue() + 1) : new BigDecimal(1));

            return abstractService.saveEsignStatus(tblEsignStatus);
        } else {
            return null;
        }
    }

    public TblEsignStatus saveEsignSubmitStatus(String claimCode, String compaignCode, HttpServletRequest request) {
        TblEsignStatus tblEsignStatus = abstractService.getEsignStatus(claimCode, compaignCode);
        if (tblEsignStatus != null) {


            String userAgentString = request.getHeader("User-Agent");

            // Parse user agent string
            UserAgent userAgent = UserAgent.parseUserAgentString(userAgentString);

            // Get browser and device information
            String browserName = userAgent.getBrowser().getName();
            String deviceType = userAgent.getOperatingSystem().getDeviceType().getName();


            tblEsignStatus.setEsignSubmitDate(new Date());
            tblEsignStatus.setClientDevice(deviceType);
            tblEsignStatus.setClientBrowser(browserName);

            tblEsignStatus.setLastupdatedate(new Date());
            tblEsignStatus.setUpdateindex(tblEsignStatus.getUpdateindex() != null ? new BigDecimal(tblEsignStatus.getUpdateindex().longValue() + 1) : new BigDecimal(1));

            return abstractService.saveEsignStatus(tblEsignStatus);
        } else {
            return null;
        }
    }
}
