package com.laportal.controller.abstracts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

@RestController
@RequestMapping("/report")
public class ReportApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(ReportApi.class);

    @SuppressWarnings("resource")
    @RequestMapping(value = "/rtaClaimReport/{rtaCode}", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<byte[]> rtaClaimReport(@PathVariable String rtaCode) throws IOException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ReportPostApi \n METHOD == rtaClaimReport(); ");

            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put("RTACODE", new Long(rtaCode));

            String reportName = "C:\\reports\\rtaCaseReport.jrxml";
            String reportOutName = "C:\\reports\\rtaCaseReport.pdf";

            callPdfJasperReport(reportName, reportOutName.toString(), hashMap);

            FileInputStream fileInputStream = null;
            byte[] bytesArray = null;

            File file = new File(reportOutName);
            bytesArray = new byte[(int) file.length()];

            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            String filename = "rtaCaseReport.pdf";
            headers.setContentDispositionFormData("inline", filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            ResponseEntity<byte[]> response = new ResponseEntity<>(bytesArray, headers, HttpStatus.OK);
            LOG.info("EXITING THIS METHOD == rtaClaimReport(); \n\n\n");

            return response;
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == ReportPostApi \n METHOD == rtaClaimReport();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("EXITING THIS METHOD == rtaClaimReport(); \n\n\n");
            return null;
        }
    }

    @RequestMapping(value = "/hdrClaimReport/{hdrclaimcode}", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<byte[]> hdrClaimReport(@PathVariable String hdrclaimcode) throws IOException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ReportPostApi \n METHOD == hdrClaimReport(); ");

            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put("hdrclaimcode", hdrclaimcode);

            String reportName = "C:\\reports\\hdrcasereport.jrxml";
            String reportOutName = "C:\\reports\\hdrcasereport.pdf";

            callPdfJasperReport(reportName, reportOutName.toString(), hashMap);

            FileInputStream fileInputStream = null;
            byte[] bytesArray = null;

            File file = new File(reportOutName);
            bytesArray = new byte[(int) file.length()];

            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            String filename = "hdrcasereport.pdf";
            headers.setContentDispositionFormData("inline", filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            ResponseEntity<byte[]> response = new ResponseEntity<>(bytesArray, headers, HttpStatus.OK);
            LOG.info("EXITING THIS METHOD == hdrClaimReport(); \n\n\n");

            return response;
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == ReportPostApi \n METHOD == hdrClaimReport();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("EXITING THIS METHOD == hdrClaimReport(); \n\n\n");
            return null;
        }
    }

}
