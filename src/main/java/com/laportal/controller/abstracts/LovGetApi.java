package com.laportal.controller.abstracts;

import com.laportal.dto.LovResponse;
import com.laportal.model.TblCompanyprofile;
import com.laportal.model.TblUser;
import com.laportal.service.lov.LovService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
public class LovGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(LovGetApi.class);
    @SuppressWarnings("unused")
    @Autowired
    private Environment env;
    @Autowired
    private LovService lovService;

    @RequestMapping(value = "/lovUserCategory", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovUserCategory(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovUserCategory(); ");
            List<LovResponse> lovResponseList = lovService.getlovUserCategory();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovUserCategory(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovUserCategory(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovUserCategory();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovUserCategory(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovCompaign", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovCompaign(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovCompaign(); ");
            List<LovResponse> lovResponseList = lovService.getlovCompaign();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovCompaign(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovCompaign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovCompaign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovCompaign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovRole", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovRole(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovRole(); ");
            List<LovResponse> lovResponseList = lovService.getlovRole();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovRole(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovRole(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovRole();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovRole(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovModule", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovModule(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovModule(); ");
            List<LovResponse> lovResponseList = lovService.getlovModule();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovModule(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovModule(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovModule();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovModule(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovCircumstances", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovCircumstances(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovCircumstances(); ");
            List<LovResponse> lovResponseList = lovService.lovCircumstances();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovCircumstances(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovCircumstances(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovCircumstances();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovCircumstances(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovInjuryClaims/{airBag}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovInjuryClaims(@PathVariable String airBag, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovInjuryClaims(); ");
            List<LovResponse> lovResponseList = lovService.getlovInjuryClaims(airBag.toUpperCase());
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovInjuryClaims(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovInjuryClaims(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovInjuryClaims();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovInjuryClaims(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovInjuryClaims", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovInjuryClaimsNoParam(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovInjuryClaims(); ");
            List<LovResponse> lovResponseList = lovService.getlovInjuryClaims("Y");
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovInjuryClaims(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovInjuryClaims(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovInjuryClaims();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovInjuryClaims(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovSolicitorsForRta/{jurisdiction}/{airbagCase}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovSolicitorsForRta(@PathVariable String jurisdiction, @PathVariable String airbagCase, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovSolicitorsForRta(); ");
            List<LovResponse> lovResponseList = lovService.getSolicitorsForRta(jurisdiction, airbagCase);
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForRta(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForRta(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovSolicitorsForRta();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovSolicitorsForRta(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovCompanyWiseUSer/{companyCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovCompanyWiseUSer(@PathVariable String companyCode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovCompanyWiseUSer(); ");
            List<LovResponse> lovResponseList = lovService.lovCompanyWiseUSer(companyCode);
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovCompanyWiseUSer(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovCompanyWiseUSer(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovCompanyWiseUSer();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovCompanyWiseUSer(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovCompany", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovCompany(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovCompany(); ");
            List<LovResponse> lovResponseList = lovService.getlovCompany();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovCompany(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovCompany(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovCompany();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovCompany(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovStatus", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovStatus(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovStatus(); ");
            List<LovResponse> lovResponseList = lovService.getlovStatus();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovStatus();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovStatus(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovHireBusinessAccept/{hireCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovHireBusiness(@PathVariable String hireCode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovHireBusiness(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            TblCompanyprofile companyprofile = lovService.getCompanyById(tblUser.getCompanycode());
            List<LovResponse> lovResponseList = null;
            if (companyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                lovResponseList = lovService.getlovHireBusiness(Long.valueOf(hireCode));
            } else {
                lovResponseList = lovService.getlovHireBusinessForHireCompanies(Long.valueOf(hireCode), companyprofile.getCompanycode());
            }

            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovHireBusiness(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovHireBusiness(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovHireBusiness();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovHireBusiness(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovHireCompanies", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovHireCompanies(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovHireCompanies(); ");
            List<LovResponse> lovResponseList = lovService.getLovHireCompanies();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovHireCompanies(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovHireCompanies(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovHireCompanies();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovHireCompanies(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovHireBusinessPending/{hireCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> loveHireBusiness(@PathVariable String hireCode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == loveHireBusiness(); ");
            List<LovResponse> lovResponseList = lovService.getLovHireBusiness(Long.valueOf(hireCode));
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == loveHireBusiness(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == loveHireBusiness(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == loveHireBusiness();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == loveHireBusiness(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovTask", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovTask(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovTask(); ");
            List<LovResponse> lovResponseList = lovService.lovTask();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovTask(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovIntroducer", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovIntroducer(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovIntroducer(); ");
            List<LovResponse> lovResponseList = lovService.lovIntroducer();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovIntroducer(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovIntroducer(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovIntroducer();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovIntroducer(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovSolicitorsForHdr", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovSolicitorsForHdr(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovSolicitorsForHdr(); ");
            List<LovResponse> lovResponseList = lovService.lovSolicitorsForHdr();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovSolicitorsForHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovSolicitorsForHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovSolicitorsForTenancy", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovSolicitorsForTenancy(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovSolicitorsForTenancy(); ");
            List<LovResponse> lovResponseList = lovService.lovSolicitorsForTenancy();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForTenancy(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForTenancy(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovSolicitorsForTenancy();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovSolicitorsForTenancy(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovSolicitorsForPcp", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovSolicitorsForPcp(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovSolicitorsForPcp(); ");
            List<LovResponse> lovResponseList = lovService.lovSolicitorsForPcp();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForPcp(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForPcp(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovSolicitorsForPcp();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovSolicitorsForPcp(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovSolicitorsForDb", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovSolicitorsForDb(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovSolicitorsForDb(); ");
            List<LovResponse> lovResponseList = lovService.lovSolicitorsForDb();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovSolicitorsForDb();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovSolicitorsForEl", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovSolicitorsForEl(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovSolicitorsForEl(); ");
            List<LovResponse> lovResponseList = lovService.lovSolicitorsForEl();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovSolicitorsForDb();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovSolicitorsForPl", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovSolicitorsForPl(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovSolicitorsForPl(); ");
            List<LovResponse> lovResponseList = lovService.lovSolicitorsForPl();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovSolicitorsForDb();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovSolicitorsForOl", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovSolicitorsForOl(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovSolicitorsForOl(); ");
            List<LovResponse> lovResponseList = lovService.lovSolicitorsForOl();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovSolicitorsForDb();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovSolicitorsForDb(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovCaseForInvoicing/{solicitorId}/{compaingId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovCaseForInvoicing(@PathVariable String solicitorId, @PathVariable String compaingId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovCaseForInvoicing(); ");
            List<LovResponse> lovResponseList = lovService.lovCaseForInvoicing(solicitorId, compaingId);
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovCaseForInvoicing(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovCaseForInvoicing(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovCaseForInvoicing();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovCaseForInvoicing(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovInvoicingStatus", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovInvoicingStatus(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovCaseForInvoicing(); ");
            List<LovResponse> lovResponseList = lovService.lovInvoicingStatus();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovCaseForInvoicing(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovCaseForInvoicing(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovCaseForInvoicing();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovCaseForInvoicing(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/lovSolicitorsForHire", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovSolicitorsForHire(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovSolicitorsForHdr(); ");
            List<LovResponse> lovResponseList = lovService.lovSolicitorsForHire();

            if (lovResponseList != null && lovResponseList.size() > 0) {

                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorsForHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovSolicitorsForHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovSolicitorsForHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/lovInvoiceStatus", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovInvoiceStatus(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovInvoiceStatus(); ");
            List<LovResponse> lovResponseList = lovService.lovInvoiceStatus();

            if (lovResponseList != null && lovResponseList.size() > 0) {

                LOG.info("\n EXITING THIS METHOD == lovInvoiceStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovInvoiceStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovInvoiceStatus();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovInvoiceStatus(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovSolicitorUsers", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovSolicitorUsers(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovSolicitorUsers(); ");
            List<LovResponse> lovResponseList = lovService.lovSolicitorUsers();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovSolicitorUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovSolicitorUsers();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovSolicitorUsers(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovLegalInternalsUsers", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovLegalInternalsUsers(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovLegalInternalsUsers(); ");
            List<LovResponse> lovResponseList = lovService.lovLegalInternalsUsers();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovLegalInternalsUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovLegalInternalsUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovLegalInternalsUsers();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovLegalInternalsUsers(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/lovIntroducerUsers", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> lovIntroducerUsers(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LovGetApi \n METHOD == lovIntroducerUsers(); ");
            List<LovResponse> lovResponseList = lovService.lovIntroducerUsers();
            if (lovResponseList != null && lovResponseList.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == lovIntroducerUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", lovResponseList);
            } else {
                LOG.info("\n EXITING THIS METHOD == lovIntroducerUsers(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == LovGetApi \n METHOD == lovIntroducerUsers();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == lovIntroducerUsers(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }
}
