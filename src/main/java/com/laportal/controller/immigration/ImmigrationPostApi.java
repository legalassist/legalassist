package com.laportal.controller.immigration;


import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.immigration.ImmigrationService;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.exporting.PdfImageInfo;
import com.spire.pdf.fields.PdfField;
import com.spire.pdf.graphics.PdfImage;
import com.spire.pdf.widget.PdfFormFieldWidgetCollection;
import com.spire.pdf.widget.PdfFormWidget;
import com.spire.pdf.widget.PdfPageCollection;
import com.spire.pdf.widget.PdfTextBoxFieldWidget;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.geom.Dimension2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

@RestController
@RequestMapping("/immigration")
public class ImmigrationPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(ImmigrationPostApi.class);

    @Autowired
    private ImmigrationService immigrationService;

    @RequestMapping(value = "/addNewImmigrationCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewImmigrationCase(@RequestBody SaveImmigrationRequest updateDbRequest,
                                                                         HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addNewHdrCase(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = immigrationService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                TblImmigrationclaim tblImmigrationclaim = new TblImmigrationclaim();

                tblImmigrationclaim.setAdvice(updateDbRequest.getAdvice());
                tblImmigrationclaim.setClientaddress(updateDbRequest.getClientaddress());
                tblImmigrationclaim.setClientcontactno(updateDbRequest.getClientcontactno());
                tblImmigrationclaim.setClientemail(updateDbRequest.getClientemail());
                tblImmigrationclaim.setClientname(updateDbRequest.getClientname());
                tblImmigrationclaim.setClienttitle(updateDbRequest.getClienttitle());
                tblImmigrationclaim.setContacttime(updateDbRequest.getContacttime());
                tblImmigrationclaim.setEsign(updateDbRequest.getEsign());
                tblImmigrationclaim.setEsigndate(updateDbRequest.getEsigndate());
                tblImmigrationclaim.setInquirydatetime(updateDbRequest.getInquirydatetime());
                tblImmigrationclaim.setInquirydescription(updateDbRequest.getInquirydescription());
                tblImmigrationclaim.setInquirynature(updateDbRequest.getInquirynature());
                tblImmigrationclaim.setInquiryreferal(updateDbRequest.getInquiryreferal());
                tblImmigrationclaim.setNationality(updateDbRequest.getNationality());
                tblImmigrationclaim.setNotes(updateDbRequest.getNotes());
                tblImmigrationclaim.setRemarks(updateDbRequest.getRemarks());
                tblImmigrationclaim.setRemarks(updateDbRequest.getRemarks());
                tblImmigrationclaim.setStatus(new BigDecimal(39));
                tblImmigrationclaim.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                tblImmigrationclaim.setCreatedate(new Date());

                TblCompanyprofile tblCompanyprofile = immigrationService.getCompanyProfile(tblUser.getCompanycode());
                tblImmigrationclaim.setImmigrationcode(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());


                tblImmigrationclaim = immigrationService.saveDbRequest(tblImmigrationclaim);

                if (tblImmigrationclaim != null) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = immigrationService.saveCompanyProfile(tblCompanyprofile);

                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblImmigrationclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/updateImmigrationCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateImmigrationCase(@RequestBody UpdateImmigrationRequest updateDbRequest,
                                                                         HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == updateHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = immigrationService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                TblImmigrationclaim tblImmigrationclaim = immigrationService.findDbCaseByIdWithoutUser(updateDbRequest.getImmigrationclaimcode());

                tblImmigrationclaim.setAdvice(updateDbRequest.getAdvice());
                tblImmigrationclaim.setClientaddress(updateDbRequest.getClientaddress());
                tblImmigrationclaim.setClientcontactno(updateDbRequest.getClientcontactno());
                tblImmigrationclaim.setClientemail(updateDbRequest.getClientemail());
                tblImmigrationclaim.setClientname(updateDbRequest.getClientname());
                tblImmigrationclaim.setClienttitle(updateDbRequest.getClienttitle());
                tblImmigrationclaim.setContacttime(updateDbRequest.getContacttime());
                tblImmigrationclaim.setEsign(updateDbRequest.getEsign());
                tblImmigrationclaim.setEsigndate(updateDbRequest.getEsigndate());
                tblImmigrationclaim.setInquirydatetime(updateDbRequest.getInquirydatetime());
                tblImmigrationclaim.setInquirydescription(updateDbRequest.getInquirydescription());
                tblImmigrationclaim.setInquirynature(updateDbRequest.getInquirynature());
                tblImmigrationclaim.setInquiryreferal(updateDbRequest.getInquiryreferal());
                tblImmigrationclaim.setNationality(updateDbRequest.getNationality());
                tblImmigrationclaim.setNotes(updateDbRequest.getNotes());
                tblImmigrationclaim.setRemarks(updateDbRequest.getRemarks());

                tblImmigrationclaim.setRemarks(updateDbRequest.getRemarks());
                tblImmigrationclaim.setUpdatedate(new Date());

                tblImmigrationclaim = immigrationService.saveDbRequest(tblImmigrationclaim);

                if (tblImmigrationclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblImmigrationclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewHdrCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewHdrCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addImmigrationNotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addImmigrationNotes(@RequestBody ImmigrationNoteRequest DbNoteRequest,
                                                                       HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addDbNotes(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = immigrationService.isHdrCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                if (!DbNoteRequest.getDbCode().isEmpty()) {
                    TblImmigrationnote tblDbnote = new TblImmigrationnote();
                    TblImmigrationclaim tblDbclaim = new TblImmigrationclaim();

                    tblDbclaim.setImmigrationclaimcode(Long.valueOf(DbNoteRequest.getDbCode()));
                    tblDbnote.setTblImmigrationclaim(tblDbclaim);
                    tblDbnote.setNote(DbNoteRequest.getNote());
                    tblDbnote.setUsercategorycode(DbNoteRequest.getUserCatCode());
                    tblDbnote.setCreatedon(new Date());
                    tblDbnote.setUsercode(tblUser.getUsercode());

                    tblDbnote = immigrationService.addTblDbNote(tblDbnote);

                    if (tblDbnote != null && tblDbnote.getImmigrationnotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblDbnote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addHdrNotes();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/resendImmigrationEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> resendDbEmail(@RequestBody ResendImmigrationMessageDto resendDbMessageDto,
                                                                 HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == resendRtaEmail(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblImmigrationmessage tblDbmessage = immigrationService.getTblDbMessageById(resendDbMessageDto.getDbmessagecode());
            if (tblDbmessage != null && tblDbmessage.getImmigrationmessagecode() > 0) {


                TblEmail tblEmail = new TblEmail();
                tblEmail.setSenflag(new BigDecimal(0));
                tblEmail.setEmailaddress(tblDbmessage.getSentto());
                tblEmail.setEmailbody(tblDbmessage.getMessage());
                tblEmail.setEmailsubject("Db LegalAssist");
                tblEmail.setCreatedon(new Date());


                tblEmail = immigrationService.saveTblEmail(tblEmail);
                if (tblEmail != null && tblEmail.getEmailcode() > 0) {

                    LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", tblEmail);
                } else {
                    LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While resending Email", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Email Found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == resendRtaEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/assigncasetosolicitorbyLA", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> assigncasetosolicitorbyLA(
            @RequestBody AssignImmigrationCasetoSolicitor assignImmigrationCasetoSolicitor, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblImmigrationclaim tblImmigrationclaim = immigrationService.assignCaseToSolicitor(assignImmigrationCasetoSolicitor, tblUser);
                if (tblImmigrationclaim != null) {
                    tblImmigrationclaim = immigrationService.findImmigrationCaseById(Long.valueOf(assignImmigrationCasetoSolicitor.getImmigrationClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblImmigrationclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }


            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == performActionOnHdrByLegalAssist();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnDb", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnDb(
            @RequestBody PerformActionOnImmigrationRequest performActionOnImmigrationRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnHdr(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblImmigrationclaim tblImmigrationclaim = null;
                if (performActionOnImmigrationRequest.getToStatus().equals("59")) {
                    // reject status
                    tblImmigrationclaim = immigrationService.performRejectCancelActionOnDb(performActionOnImmigrationRequest.getDbClaimCode(),
                            performActionOnImmigrationRequest.getToStatus(), performActionOnImmigrationRequest.getReason(), tblUser);

                } else if (performActionOnImmigrationRequest.getToStatus().equals("60")) {
                    //cancel status
                    tblImmigrationclaim = immigrationService.performRejectCancelActionOnDb(performActionOnImmigrationRequest.getDbClaimCode(),
                            performActionOnImmigrationRequest.getToStatus(), performActionOnImmigrationRequest.getReason(), tblUser);

                } else {
                    tblImmigrationclaim = immigrationService.performActionOnDb(performActionOnImmigrationRequest.getDbClaimCode(),
                            performActionOnImmigrationRequest.getToStatus(), tblUser);
                }

                if (tblImmigrationclaim != null) {
                    TblImmigrationclaim Dbclaim = immigrationService
                            .findImmigrationCaseById(Long.valueOf(performActionOnImmigrationRequest.getDbClaimCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", Dbclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addESign(); ");
        try {
            Locale backup = Locale.getDefault();
            Locale.setDefault(Locale.ENGLISH);
            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("ImmigrationClaimCode")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }

            TblImmigrationclaim tblImmigrationclaim = immigrationService.findDbCaseByIdWithoutUser(addESign.getRtaCode());
            if (tblImmigrationclaim != null && tblImmigrationclaim.getImmigrationclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = immigrationService.getCompanyProfileAgainstDbCode(tblImmigrationclaim.getImmigrationclaimcode());
                PdfDocument doc = new PdfDocument();

                String path = "";
                TblCompanydoc tblCompanydoc = new TblCompanydoc();
                tblCompanydoc = immigrationService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "E");

                doc.loadFromFile(tblCompanydoc.getPath());

                byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                BufferedImage image1 = ImageIO.read(bis);
                bis.close();

                PdfImage image = PdfImage.fromImage(image1);

                // Get the first worksheet
                PdfPageCollection page = doc.getPages();
                for (int j = 0; j < page.getCount(); j++) {
                    // Get the image information of the page
                    PdfImageInfo[] imageInfo = page.get(j).getImagesInfo();

                    // Loop through the image information
                    for (int i = 0; i < imageInfo.length; i++) {

                        // Get the bounds property of a specific image
                        Rectangle2D rect = imageInfo[i].getBounds();
                        // Get the x and y coordinates
                        System.out.println(String.format("The coordinate of image %d:（%f, %f）", i + 1, rect.getX(),
                                rect.getY()));

                        page.get(j).getCanvas().drawImage(image, rect.getX(), rect.getY(), rect.getWidth(),
                                rect.getHeight());
                    }
                }

                // get the form fields from the document
                PdfFormWidget form = (PdfFormWidget) doc.getForm();

                // get the form widget collection
                PdfFormFieldWidgetCollection formWidgetCollection = form.getFieldsWidget();

                // loop through the widget collection and fill each field with value
                for (int i = 0; i < formWidgetCollection.getCount(); i++) {
                    PdfField field = formWidgetCollection.get(i);
                    if (field instanceof PdfTextBoxFieldWidget) {
                        while (keys.hasNext()) {
                            String key = keys.next();
                            if (!key.equals("rtaCode") && !key.equals("eSign")) {
                                if (field.getName().equals(key)) {
                                    PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;
                                    textBoxField.setText(jsonObject.getString(key));
                                }
                            }
                        }

                        if (field.getName().startsWith("DB_")) {

                            PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;
                            String keyValue = immigrationService.getDbDbColumnValue(field.getName().replace("DB_", ""),
                                    tblImmigrationclaim.getImmigrationclaimcode());
                            if (keyValue.isEmpty()) {
                                Dimension2D dimension2D = new Dimension();
                                dimension2D.setSize(0, 0);
                                textBoxField.setSize(dimension2D);
                            } else {
                                textBoxField.setText(keyValue);
                            }


                        }
                        if (field.getName().startsWith("DATE")) {
                            PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;

                            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

                            Date date = new Date();
                            String s = formatter.format(date);
                            textBoxField.setText(s);
                        }
                        field.setFlatten(true);
                        field.setReadOnly(true);
                    }
                }
                String s = "C:\\Esigns\\" + tblImmigrationclaim.getImmigrationcode() + ".pdf";
                doc.saveToFile(s);
                File file = new File(s);
//                String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));
                String UPLOADED_FOLDER = "E://temp//";

                TblImmigrationdocument tblImmigrationdocument = new TblImmigrationdocument();
                tblImmigrationdocument.setTblImmigrationclaim(tblImmigrationclaim);
                tblImmigrationdocument.setDocumentType("E-Sign");
                tblImmigrationdocument.setDocumentPath(UPLOADED_FOLDER + tblImmigrationclaim.getImmigrationcode() + ".pdf");

                byte[] bytes = s.getBytes();
                Path path1 = Paths.get(UPLOADED_FOLDER + tblImmigrationclaim.getImmigrationcode() + ".pdf");
                Files.write(path1, bytes);

                tblImmigrationdocument = immigrationService.saveTblDbDocument(tblImmigrationdocument);
                if (tblImmigrationdocument != null && tblImmigrationdocument.getImmigrationdocumentscode() > 0) {
                    Locale.setDefault(backup);
                    tblImmigrationclaim.setEsign("Y");
                    tblImmigrationclaim = immigrationService.saveDbRequest(tblImmigrationclaim);
                    TblImmigrationtask tblImmigrationtask = immigrationService.getDbTaskByDbCodeAndTaskCode(tblImmigrationclaim.getImmigrationclaimcode(), 1);

                    tblImmigrationtask.setStatus("C");
                    tblImmigrationtask.setRemarks("Completed");

                    TblImmigrationtask tblDbtask1 = immigrationService.updateTblDbTask(tblImmigrationtask);
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", "Esign Completed");
                } else {
                    Locale.setDefault(backup);
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Saving The Document..Esign Compeleted",
                            null);
                }
            } else {
                Locale.setDefault(backup);
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addESign(); ");
        try {

            TblImmigrationclaim tblImmigrationclaim = immigrationService.findDbCaseByIdWithoutUser(Long.valueOf(eSignFieldsRequest.getCode()));
            if (tblImmigrationclaim != null && tblImmigrationclaim.getImmigrationclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = immigrationService
                        .getCompanyProfileAgainstDbCode(tblImmigrationclaim.getImmigrationclaimcode());

                String path = "";
                TblCompanydoc tblCompanydoc = new TblCompanydoc();
                tblCompanydoc = immigrationService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "E");

                HashMap<String, String> fields = new HashMap<>();

                PdfDocument doc = new PdfDocument();
                doc.loadFromFile(tblCompanydoc.getPath());

                PdfFormWidget form = (PdfFormWidget) doc.getForm();
                PdfFormFieldWidgetCollection formWidgetCollection = form.getFieldsWidget();
                for (int i = 0; i < formWidgetCollection.getCount(); i++) {
                    PdfField field = formWidgetCollection.get(i);
                    if (!field.getName().startsWith("DB_") && !field.getName().equals("DATE")) {
                        fields.put(field.getName(), field.getName());
                    }
                }

                String viewDocPath = tblCompanydoc.getPath().replace("Esigns", "ViewEsigns");
                File file = new File(viewDocPath);
                String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                fields.put("doc", encoded);

                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Success", fields);
            } else {
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No HDR document found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("hdrClaimCode") long hdrClaimCode, @RequestParam("taskCode") long taskCode, @RequestParam("multipartFiles") List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblImmigrationclaim tblImmigrationclaim = immigrationService.findImmigrationCaseById(hdrClaimCode, tblUser);
            if (tblImmigrationclaim != null && tblImmigrationclaim.getImmigrationclaimcode() > 0) {
                TblTask tblTask = immigrationService.getTaskAgainstCode(taskCode);
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblImmigrationtask tblImmigrationtask = immigrationService.getDbTaskByDbCodeAndTaskCode(tblImmigrationclaim.getImmigrationclaimcode(), taskCode);

                    if (tblTask.getTaskcode() == 1) {
//                        if (tblImmigrationclaim.getClaimantEmail() == null) {
//                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
//                            return getResponseFormat(HttpStatus.BAD_REQUEST, "No Client Email Address Found", null);
//                        }
//                        String emailUrl = "115.186.147.30:8888/esign?hdr=" + tblImmigrationclaim.getImmigrationclaimcode();
//
//                        TblEmail tblEmail = new TblEmail();
//                        tblEmail.setSenflag(new BigDecimal(0));
//                        tblEmail.setEmailaddress(tblImmigrationclaim.getClaimantEmail());
//                        tblEmail.setEmailbody("Email sent to perform esign on mentioned hdr claim number: " + tblImmigrationclaim.getDbcode()
//                                + "\n Click on the below link to perform action \n" + emailUrl);
//                        tblEmail.setEmailsubject("Db CASE" + tblImmigrationclaim.getDbcode());
//                        tblEmail.setCreatedon(new Date());
//
//                        tblEmail = immigrationService.saveTblEmail(tblEmail);

                        tblImmigrationtask.setStatus("P");
                        tblImmigrationtask.setRemarks("Email Sent Successfully to Client");
                    } else {
                        String UPLOADED_FOLDER = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\";
                        String UPLOADED_SERVER = "http:\\115.186.147.30:8888\\";
                        List<TblImmigrationdocument> tblImmigrationdocuments = new ArrayList<>();

                        File theDir = new File(UPLOADED_FOLDER + tblImmigrationclaim.getImmigrationcode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }

                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);

                            TblImmigrationdocument tblImmigrationdocument = new TblImmigrationdocument();
                            tblImmigrationdocument.setDocumentPath(UPLOADED_SERVER + tblImmigrationclaim.getImmigrationcode() + "\\" + multipartFile.getOriginalFilename());
                            tblImmigrationdocument.setDocumentType(multipartFile.getContentType());
                            tblImmigrationdocument.setTblImmigrationclaim(tblImmigrationclaim);

                            tblImmigrationdocuments.add(tblImmigrationdocument);
                        }

                        tblImmigrationdocuments = immigrationService.saveTblDbDocuments(tblImmigrationdocuments);
                        tblImmigrationtask.setStatus("C");
                        tblImmigrationtask.setRemarks("Completed");
                    }
                    TblImmigrationtask tblHdrtask1 = immigrationService.updateTblDbTask(tblImmigrationtask);
                    if (tblHdrtask1 != null && tblHdrtask1.getImmigrationtaskcode() > 0) {

                        List<TblImmigrationtask> tblImmigrationtasks = immigrationService.findTblDbTaskByDbClaimCode(tblImmigrationclaim.getImmigrationclaimcode());
                        int count = 0;
                        for (TblImmigrationtask Dbtask : tblImmigrationtasks) {
                            if (Dbtask.getStatus().equals("C")) {
                                count++;
                            }
                        }

                        if (count == tblImmigrationtasks.size()) {
                            tblImmigrationclaim.setStatus(new BigDecimal(61));

                            tblImmigrationclaim = immigrationService.saveDbRequest(tblImmigrationclaim);
                        }

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblHdrtask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
