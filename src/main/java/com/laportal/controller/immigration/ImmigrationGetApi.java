package com.laportal.controller.immigration;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.ImigrationStatusCountList;
import com.laportal.dto.ImmigrationCaseList;
import com.laportal.model.*;
import com.laportal.service.immigration.ImmigrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/immigration")
public class ImmigrationGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(ImmigrationGetApi.class);

    @Autowired
    private ImmigrationService immigrationService;

    @RequestMapping(value = "/getImmigrationCases", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getimmigrationCases(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getimmigrationCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getimmigrationCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = immigrationService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<ImmigrationCaseList> immigrationCaseLists = immigrationService.getAuthDbCasesIntroducers(tblUser.getUsercode());
                if (immigrationCaseLists != null && immigrationCaseLists.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", immigrationCaseLists);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<ImmigrationCaseList> viewHdrCaseslist = immigrationService.getAuthDbCasesSolicitors(tblUser.getUsercode());
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<ImmigrationCaseList> viewHdrCaseslist = immigrationService.getAuthDbCasesLegalAssist();
                if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getDbCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getDbCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getImmigrationCaseById/{immigrationCaseId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getImmigrationCaseById(@PathVariable long immigrationCaseId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCaseById(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblImmigrationclaim tblImmigrationclaim = immigrationService.findImmigrationCaseById(immigrationCaseId, tblUser);

            if (tblImmigrationclaim.getTblImmigrationsolicitors() != null) {
                TblCompanyprofile tblCompanyprofile = immigrationService.getCompanyProfile(tblImmigrationclaim.getTblImmigrationsolicitors().get(0).getCompanycode());
                tblImmigrationclaim.getTblImmigrationsolicitors().get(0).setTblCompanyprofile(tblCompanyprofile);
            }

            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Record Found", tblImmigrationclaim);
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllImmigrationStatusCounts", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllImmigrationStatusCounts(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getAllImmigrationStatusCounts(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = immigrationService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<ImigrationStatusCountList> imigrationStatusCountLists = immigrationService.getAllDbStatusCountsForIntroducers();

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", imigrationStatusCountLists);
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<ImigrationStatusCountList> imigrationStatusCountLists = immigrationService.getAllDbStatusCountsForSolicitor();

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", imigrationStatusCountLists);
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<ImigrationStatusCountList> imigrationStatusCountLists = immigrationService.getAllDbStatusCounts();

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", imigrationStatusCountLists);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getAllDbStatusCounts();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllDbStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getImmigrationCasesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getImmigrationCasesByStatus(@PathVariable long statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getDbCasesByStatus(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            List<ImmigrationCaseList> DbCasesByStatus = immigrationService.getDbCasesByStatus(statusId);

            if (DbCasesByStatus != null && DbCasesByStatus.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getDbCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", DbCasesByStatus);
            } else {
                LOG.info("\n EXITING THIS METHOD == getDbCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getImmigrationCaseLogs/{immigrationcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getDbCaseLogs(@PathVariable String immigrationcode,
                                                                 HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getDbCaseLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getDbCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblImmigrationlog> tblImmigrationlogs = immigrationService.getDbCaseLogs(immigrationcode);

                if (tblImmigrationlogs != null && tblImmigrationlogs.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getDbCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblImmigrationlogs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getDbCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getDbCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getDbCaseLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getDbCaseLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getImmigrationCaseNotes/{immigrationcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getImmigrationCaseNotes(@PathVariable String immigrationcode,
                                                                           HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getDbCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getDbCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblImmigrationnote> tblImmigrationnotes = immigrationService.getDbCaseNotes(immigrationcode, tblUser);

                if (tblImmigrationnotes != null && tblImmigrationnotes.size() > 0) {
                    for (TblImmigrationnote tblImmigrationnote : tblImmigrationnotes) {
                        tblImmigrationnote.setTblImmigrationclaim(null);
                    }
                    LOG.info("\n EXITING THIS METHOD == getDbCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblImmigrationnotes);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getDbCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getDbCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getDbCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getDbCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getImmigrationCaseMessages/{immigrationcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getImmigrationCaseMessages(@PathVariable String immigrationcode,
                                                                              HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getDbCaseMessages(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblImmigrationmessage> tblImmigrationmessages = immigrationService.getDbCaseMessages(immigrationcode);

                if (tblImmigrationmessages != null && tblImmigrationmessages.size() > 0) {
                    for (TblImmigrationmessage tblImmigrationmessage : tblImmigrationmessages) {
                        tblImmigrationmessage.setTblImmigrationclaim(null);
                    }

                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblImmigrationmessages);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHdrCaseMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

}
