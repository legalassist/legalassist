package com.laportal.controller.integration;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.ThirdPartyNoteRequest;
import com.laportal.model.TblHireclaim;
import com.laportal.model.TblHirenote;
import com.laportal.model.TblRtaclaim;
import com.laportal.model.TblRtanote;
import com.laportal.service.hire.HireService;
import com.laportal.service.rta.RtaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;

@RestController
@RequestMapping("/integration")
public class LaIntegration extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(LaIntegration.class);

    @Autowired
    private HireService hireService;
    @Autowired
    private RtaService rtaService;

    @RequestMapping(value = "/addThirdPartyNote", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addThirdPartyNote(@Valid @RequestBody ThirdPartyNoteRequest thirdPartyNoteRequest,
                                                                     HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == LaIntegration \n METHOD == addThirdPartyNote(); ");

            if (thirdPartyNoteRequest.getClaimType().equalsIgnoreCase("RTA")) {
                TblRtaclaim tblRtaclaim = rtaService.getTblRtaclaimByRefNo(thirdPartyNoteRequest.getClaimRefNo());
                if (tblRtaclaim != null) {
                    TblRtanote rtanote = new TblRtanote();

                    rtanote.setTblRtaclaim(tblRtaclaim);
                    rtanote.setNote(thirdPartyNoteRequest.getClaimNote());
                    rtanote.setUsercategorycode("7");
                    rtanote.setCreatedon(new Date());
                    rtanote.setUsercode(null);

                    rtanote = rtaService.addNoteToRta(rtanote);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addThirdPartyNote(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Found Of Claim Ref No", null);
                }
            } else if (thirdPartyNoteRequest.getClaimType().equalsIgnoreCase("HIRE")) {
                TblHireclaim tblHireclaim = hireService.getTblHireClaimByRefNo(thirdPartyNoteRequest.getClaimRefNo());
                if (tblHireclaim != null) {
                    TblHirenote tblHirenote = new TblHirenote();

                    tblHirenote.setTblHireclaim(tblHireclaim);
                    tblHirenote.setNote(thirdPartyNoteRequest.getClaimNote());
                    tblHirenote.setUsercategorycode("7");
                    tblHirenote.setCreatedon(new Date());
                    tblHirenote.setUsercode(null);

                    tblHirenote = hireService.addTblHireNote(tblHirenote);


                } else {
                    LOG.info("\n EXITING THIS METHOD == addThirdPartyNote(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Found Of Claim Ref No", null);
                }

            } else if (thirdPartyNoteRequest.getClaimType().equalsIgnoreCase("HDR")) {

            } else {
                LOG.info("\n EXITING THIS METHOD == addThirdPartyNote(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Such Claim Type", null);
            }

            LOG.info("\n EXITING THIS METHOD == addThirdPartyNote(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Note Added To Case SuccessFully", null);
        } catch (Exception e) {
            LOG.error("\n CLASS == LaIntegration \n METHOD == addThirdPartyNote();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addThirdPartyNote(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
