package com.laportal.controller.rta;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.RtaAuditLogResponse;
import com.laportal.dto.RtaDuplicatesResponse;
import com.laportal.dto.RtaNotesResponse;
import com.laportal.dto.RtaStatusCountList;
import com.laportal.model.*;
import com.laportal.service.rta.RtaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/rta")
public class RtaGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(RtaGetApi.class);

    @SuppressWarnings("unused")
    @Autowired
    private Environment env;

    @Autowired
    private RtaService rtaService;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> test(HttpServletRequest request) {
        try {

//            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getAuthRtaCase(); ");
//            List<String> emails = new ArrayList<>();
//            //Create a PdfDocument object
//            PdfDocument doc = new PdfDocument();
//
//            //Load a PDF file
//            doc.loadFromFile("C:\\Users\\Murtaza\\Documents\\New_Blank_Document.pdf");
//            //Load an image
//            PdfImage image = PdfImage.fromFile("C:\\Users\\Murtaza\\Pictures\\wallpapers\\20695.jpg");
//            //Get the first worksheet
//            PdfPageCollection page = doc.getPages();
//            for (int j = 0; j < page.getCount(); j++) {
//                //Get the image information of the page
//                PdfImageInfo[] imageInfo = page.get(j).getImagesInfo();
//
//                //Loop through the image information
//                for (int i = 0; i < imageInfo.length; i++) {
//
//                    //Get the bounds property of a specific image
//                    Rectangle2D rect = imageInfo[i].getBounds();
//                    //Get the x and y coordinates
//                    System.out.println(String.format("The coordinate of image %d:（%f, %f）", i + 1, rect.getX(), rect.getY()));
//
//                    page.get(j).getCanvas().drawImage(image, rect.getX(), rect.getY(), rect.getWidth(),  rect.getHeight());
//                }
//            }
//
//            //get the form fields from the document
//            PdfFormWidget form = (PdfFormWidget) doc.getForm();
//
//            //get the form widget collection
//            PdfFormFieldWidgetCollection formWidgetCollection = form.getFieldsWidget();
//
//            //loop through the widget collection and fill each field with value
//            for (int i = 0; i < formWidgetCollection.getCount(); i++) {
//
//                PdfField field = formWidgetCollection.get(i);
//                String x = field.getName();
//                if (field instanceof PdfTextBoxFieldWidget) {
//                    PdfTextBoxFieldWidget textBoxField = (PdfTextBoxFieldWidget) field;
//
//                    textBoxField.setText("Kaila Smith");
//                }
//            }
//
//                doc.saveToFile("C:\\Users\\Murtaza\\Documents\\test1.pdf");

            //awein commit

            return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getAuthRtaCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }


    @RequestMapping(value = "/getAuthRtaCases", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCases(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getAuthRtaCases(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(tblUser.getCompanycode());

            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "1");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {

                List<ViewRtalistintorducers> viewRtalist = rtaService.getAuthRtaCasesIntroducers(tblUser.getUsercode());
                for (ViewRtalistintorducers viewRtalistintorducers : viewRtalist) {
                    if (viewRtalistintorducers.getUsercategory() != null) {
                        if (viewRtalistintorducers.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode())) {
                            viewRtalistintorducers.setEditflag("Y");
                        } else {
                            viewRtalistintorducers.setEditflag("N");
                        }
                    } else {
                        viewRtalistintorducers.setEditflag("N");
                    }
                }

                if (viewRtalist != null && viewRtalist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewRtalist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }

            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {

                List<ViewRtalistsolicitors> viewRtalist = rtaService.getAuthRtaCasesSolicitors(tblUser.getUsercode());
                for (ViewRtalistsolicitors viewRtalistsolicitors : viewRtalist) {
                    if (viewRtalistsolicitors.getUsercategory() != null) {
                        if (viewRtalistsolicitors.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode())) {
                            viewRtalistsolicitors.setEditflag("Y");
                        } else {
                            viewRtalistsolicitors.setEditflag("N");
                        }
                    } else {
                        viewRtalistsolicitors.setEditflag("N");
                    }
                }
                if (viewRtalist != null && viewRtalist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewRtalist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }

            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {

                List<ViewRtalistintorducers> viewRtalist = rtaService.getAuthRtaCasesLegalAssist();
                for (ViewRtalistintorducers viewRtalistintorducers : viewRtalist) {
                    if (viewRtalistintorducers.getUsercategory() != null) {
                        if (viewRtalistintorducers.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode())) {
                            viewRtalistintorducers.setEditflag("Y");
                        } else {
                            viewRtalistintorducers.setEditflag("N");
                        }
                    } else {
                        viewRtalistintorducers.setEditflag("N");
                    }
                }
                if (viewRtalist != null && viewRtalist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewRtalist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }


        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getAuthRtaCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getAuthRtaCase/{rtacode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCase(@PathVariable String rtacode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getAuthRtaCase(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "1");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            ViewRtaclamin viewRtaclamin = rtaService.getAuthRtaCase(rtacode, tblUser);
            if (viewRtaclamin != null) {
                if (viewRtaclamin.getTaskflag().equals("Y")) {
                    List<TblRtatask> rtaCaseTasks = rtaService.getAuthRtaCaseTasks(rtacode);
                    viewRtaclamin.setRtaCaseTasks(rtaCaseTasks);
                }


                LOG.info("\n EXITING THIS METHOD == getAuthRtaCase(); \n\n\n");
                ResponseEntity<HashMap<String, Object>> responseEntity = getResponseFormat(HttpStatus.OK, "Record Found", viewRtaclamin);
                return responseEntity;
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Some Error In Case File", null);
            }


        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getAuthRtaCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAuthRtaCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getAuthRtaCasePassengers/{rtacode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCasePassengers(@PathVariable String rtacode,
                                                                            HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getAuthRtaCasePassengers(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCasePassengers(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblRtapassenger> tblRtapassengers = rtaService.getAuthRtaCasePassengers(rtacode);

                if (tblRtapassengers != null && tblRtapassengers.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCasePassengers(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblRtapassengers);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCasePassengers(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCasePassengers(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getAuthRtaCasePassengers();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAuthRtaCasePassengers(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getAuthRtaCaseNotes/{rtacode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCaseNotes(@PathVariable String rtacode,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getAuthRtaCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "1");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            if (tblUser != null) {
                List<TblRtanote> tblRtanotes = rtaService.getAuthRtaCaseNotes(rtacode, tblUser);
                List<TblRtanote> tblRtanotesLegalInternal = rtaService.getAuthRtaCaseNotesOfLegalInternal(rtacode, tblUser);

                if (tblRtanotes != null && tblRtanotes.size() > 0) {
                    RtaNotesResponse rtaNotesResponse = new RtaNotesResponse();
                    rtaNotesResponse.setTblRtanotes(tblRtanotes);
                    rtaNotesResponse.setTblRtanotesLegalOnly(tblRtanotesLegalInternal);

                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaNotesResponse);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getAuthRtaCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getAuthRtaCaseMessages/{rtacode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCaseMessages(@PathVariable String rtacode,
                                                                          HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getAuthRtaCaseMessages(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblRtamessage> tblRtamessages = rtaService.getAuthRtaCaseMessages(rtacode);

                if (tblRtamessages != null && tblRtamessages.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblRtamessages);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getAuthRtaCaseMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getAuthRtaCaseLogs/{rtacode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCaseLogs(@PathVariable String rtacode,
                                                                      HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getAuthRtaCaseLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblRtalog> rtaCaseLogs = rtaService.getAuthRtaCaseLogs(rtacode);

                if (rtaCaseLogs != null && rtaCaseLogs.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaCaseLogs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getAuthRtaCaseLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getAuthRtaCaseTasks/{rtacode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCaseTasks(@PathVariable String rtacode,
                                                                       HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getAuthRtaCaseTasks(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseTasks(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblRtatask> rtaCaseTasks = rtaService.getAuthRtaCaseTasks(rtacode);

                if (rtaCaseTasks != null && rtaCaseTasks.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseTasks(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaCaseTasks);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseTasks(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseTasks(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getAuthRtaCaseTasks();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAuthRtaCaseTasks(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getAuthRtaCasedocuments/{rtacode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAuthRtaCasedocuments(@PathVariable String rtacode,
                                                                           HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getAuthRtaCasedocuments(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCasedocuments(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblRtadocument> authRtaCasedocuments = rtaService.getAuthRtaCasedocuments(rtacode);

                if (authRtaCasedocuments != null) {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCasedocuments(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", authRtaCasedocuments);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAuthRtaCasedocuments(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCasedocuments(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getAuthRtaCasedocuments();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAuthRtaCasedocuments(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getWorkFlow", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getWorkFlow(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getWorkFlow(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getWorkFlow(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblRtaflow> tblRtaflows = rtaService.getWorkFlow();

                if (tblRtaflows != null) {
                    LOG.info("\n EXITING THIS METHOD == getWorkFlow(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblRtaflows);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getWorkFlow(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getWorkFlow(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getWorkFlow();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getWorkFlow(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getSubRtaCases/{rtacasecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getSubRtaCases(@PathVariable String rtacasecode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getSubRtaCases(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getSubRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblRtaclaim> tblRtaclaims = rtaService.getSubRtaClaims(rtacasecode);

                if (tblRtaclaims != null) {
                    LOG.info("\n EXITING THIS METHOD == getSubRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblRtaclaims);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getSubRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getSubRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getSubRtaCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getSubRtaCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getAllRtaStatusCounts", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllplStatusCounts(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getAllRtaStatusCounts(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAllRtaStatusCounts(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {
                List<RtaStatusCountList> rtaStatusCountsForIntroducers = rtaService.getAllRtaStatusCountsForIntroducers(tblCompanyprofile.getCompanycode());
                if (rtaStatusCountsForIntroducers != null) {
                    LOG.info("\n EXITING THIS METHOD == getAllRtaStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaStatusCountsForIntroducers);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAllRtaStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Cases Found Against User", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {
                List<RtaStatusCountList> rtaStatusCountsForSolicitor = rtaService.getAllRtaStatusCountsForSolicitor(tblCompanyprofile.getCompanycode());

                if (rtaStatusCountsForSolicitor != null) {
                    LOG.info("\n EXITING THIS METHOD == getAllRtaStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaStatusCountsForSolicitor);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAllRtaStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Cases Found Against User", null);
                }
            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {
                List<RtaStatusCountList> allRtaStatusCounts = rtaService.getAllRtaStatusCounts();

                if (allRtaStatusCounts != null) {
                    LOG.info("\n EXITING THIS METHOD == getAllRtaStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", allRtaStatusCounts);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getAllRtaStatusCounts(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Cases Found Against User", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getAllRtaStatusCounts(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getAllRtaStatusCounts();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllRtaStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getRtaCasesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getRtaCasesByStatus(@PathVariable String statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getRtaCasesByStatus(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getRtaCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(tblUser.getCompanycode());

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {

                List<ViewRtalistintorducers> viewRtalist = rtaService.getAuthRtaCasesIntroducersStatusWise(tblCompanyprofile.getCompanycode(), statusId);
                for (ViewRtalistintorducers viewRtalistintorducers : viewRtalist) {
                    if (viewRtalistintorducers.getUsercategory() != null) {
                        if (viewRtalistintorducers.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode())) {
                            viewRtalistintorducers.setEditflag("Y");
                        } else {
                            viewRtalistintorducers.setEditflag("N");
                        }
                    } else {
                        viewRtalistintorducers.setEditflag("N");
                    }
                }

                if (viewRtalist != null && viewRtalist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getRtaCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewRtalist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getRtaCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }

            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {

                List<ViewRtalistsolicitors> viewRtalist = rtaService.getAuthRtaCasesSolicitorsStatusWise(tblCompanyprofile.getCompanycode(), statusId);
                for (ViewRtalistsolicitors viewRtalistsolicitors : viewRtalist) {
                    if (viewRtalistsolicitors.getUsercategory() != null) {
                        if (viewRtalistsolicitors.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode())) {
                            viewRtalistsolicitors.setEditflag("Y");
                        } else {
                            viewRtalistsolicitors.setEditflag("N");
                        }
                    } else {
                        viewRtalistsolicitors.setEditflag("N");
                    }
                }
                if (viewRtalist != null && viewRtalist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getRtaCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewRtalist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getRtaCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }

            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {

                List<ViewRtalistintorducers> viewRtalist = rtaService.getAuthRtaCasesLegalAssistStatusWise(statusId);
                for (ViewRtalistintorducers viewRtalistintorducers : viewRtalist) {
                    if (viewRtalistintorducers.getUsercategory() != null) {
                        if (viewRtalistintorducers.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode())) {
                            viewRtalistintorducers.setEditflag("Y");
                        } else {
                            viewRtalistintorducers.setEditflag("N");
                        }
                    } else {
                        viewRtalistintorducers.setEditflag("N");
                    }
                }
                if (viewRtalist != null && viewRtalist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getRtaCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewRtalist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getRtaCasesByStatus(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == getRtaCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }


        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getRtaCasesByStatus();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getRtaCasesByStatus(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getRtaDuplicates/{rtacasecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getRtaDuplicates(@PathVariable String rtacasecode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getRtaDuplicates(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getRtaDuplicates(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<RtaDuplicatesResponse> rtaDuplicates = rtaService.getRtaDuplicates(rtacasecode);

                if (rtaDuplicates != null) {
                    LOG.info("\n EXITING THIS METHOD == getRtaDuplicates(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaDuplicates);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getRtaDuplicates(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getRtaDuplicates(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getRtaDuplicates();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getRtaDuplicates(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }


    @RequestMapping(value = "/getRtaAuditLogs/{rtacasecode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getRtaAuditLogs(@PathVariable String rtacasecode, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaGetApi \n METHOD == getRtaAuditLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getRtaAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<RtaAuditLogResponse> rtaAuditLogResponses = rtaService.getRtaAuditLogs(rtacasecode);

                if (rtaAuditLogResponses != null && rtaAuditLogResponses.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getRtaAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaAuditLogResponses);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getRtaAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getRtaAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getRtaAuditLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getRtaAuditLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }
}
