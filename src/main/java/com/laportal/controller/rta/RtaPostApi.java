package com.laportal.controller.rta;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.dto.xmldto.ClaimsCNFexport;
import com.laportal.model.*;
import com.laportal.service.rta.RtaService;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rta")
public class RtaPostApi extends AbstractApi {
    Logger LOG = LoggerFactory.getLogger(RtaGetApi.class);

    @Autowired
    private Environment env;

    @Autowired
    private RtaService rtaService;

    @RequestMapping(value = "/addNewRtaCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewRtaCase(@RequestBody SaveRtaRequest saveRtaRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addNewRtaCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "1");
            boolean introducerAllowed = rtaService.isRtaCaseAllowed(String.valueOf(saveRtaRequest.getIntroducer()),
                    "1");

            if (allowed && introducerAllowed) {
                TblRtaclaim tblRtaclaim = new TblRtaclaim();
                List<TblRtapassenger> tblRtapassengers = new ArrayList<>();
                List<TblRtapassenger> saveRtapassengers;

                tblRtaclaim.setAccdate(formatter.parse(saveRtaRequest.getAccdate()));
                tblRtaclaim.setAcctime(saveRtaRequest.getAcctime());
                tblRtaclaim.setAddress1(saveRtaRequest.getAddress1());
                tblRtaclaim.setAddress2(saveRtaRequest.getAddress2());
                tblRtaclaim.setAddress3(saveRtaRequest.getAddress3());
                tblRtaclaim.setCity(saveRtaRequest.getCity());
                tblRtaclaim.setContactdue(saveRtaRequest.getContactdue() == null ? null
                        : formatter.parse(saveRtaRequest.getContactdue()));
                tblRtaclaim.setCreatedon(new Date());
                tblRtaclaim.setDescription(saveRtaRequest.getDescription());
                tblRtaclaim.setDob(formatter.parse(saveRtaRequest.getDob()));
                tblRtaclaim.setDriverpassenger(saveRtaRequest.getDriverpassenger());
                tblRtaclaim.setEmail(saveRtaRequest.getEmail());
                tblRtaclaim.setEnglishlevel(saveRtaRequest.getEnglishlevel());
                tblRtaclaim.setFirstname(saveRtaRequest.getFirstname());
                tblRtaclaim.setGreencardno(saveRtaRequest.getGreencardno());
                tblRtaclaim.setInjdescription(saveRtaRequest.getInjdescription());
                tblRtaclaim.setInsurer(saveRtaRequest.getInsurer());
                tblRtaclaim.setLandline(saveRtaRequest.getLandline());
                tblRtaclaim.setLastname(saveRtaRequest.getLastname());
                tblRtaclaim.setLocation(saveRtaRequest.getLocation());
                tblRtaclaim.setMakemodel(saveRtaRequest.getMakemodel());
                tblRtaclaim.setMedicalinfo(saveRtaRequest.getMedicalinfo());
                tblRtaclaim.setMiddlename(saveRtaRequest.getMiddlename());
                tblRtaclaim.setMobile(saveRtaRequest.getMobile());
                tblRtaclaim.setNinumber(saveRtaRequest.getNinumber());
                tblRtaclaim.setOngoing(saveRtaRequest.getOngoing());
                tblRtaclaim.setPartyaddress(saveRtaRequest.getPartyaddress());
                tblRtaclaim.setPartycontactno(saveRtaRequest.getPartycontactno());
                tblRtaclaim.setPartyinsurer(saveRtaRequest.getPartyinsurer());
                tblRtaclaim.setPartymakemodel(saveRtaRequest.getPartymakemodel());
                tblRtaclaim.setPartyname(saveRtaRequest.getPartyname());
                tblRtaclaim.setPartypolicyno(saveRtaRequest.getPartypolicyno());
                tblRtaclaim.setPartyrefno(saveRtaRequest.getPartyrefno());
                tblRtaclaim.setPartyregno(saveRtaRequest.getPartyregno());
                tblRtaclaim.setPassengerinfo(saveRtaRequest.getPassengerinfo());
                tblRtaclaim.setPolicyno(saveRtaRequest.getPolicyno());
                tblRtaclaim.setPostalcode(saveRtaRequest.getPostalcode());
                tblRtaclaim.setRdweathercond(saveRtaRequest.getRdweathercond());
                tblRtaclaim.setRefno(saveRtaRequest.getRefno());
                tblRtaclaim.setRegion(saveRtaRequest.getRegion());
                tblRtaclaim.setRegisterationno(saveRtaRequest.getRegisterationno());
                tblRtaclaim.setReportedtopolice(saveRtaRequest.getReportedtopolice());
                tblRtaclaim.setEsig("N");
                tblRtaclaim.setScotland(saveRtaRequest.getScotland());
                tblRtaclaim.setPassword(saveRtaRequest.getPassword());
                tblRtaclaim.setTranslatordetails(saveRtaRequest.getTranslatordetail());
                tblRtaclaim.setAlternativenumber(saveRtaRequest.getAlternativenumber());
                tblRtaclaim.setMedicalevidence(saveRtaRequest.getMedicalevidence());
                tblRtaclaim.setReportedon(saveRtaRequest.getReportedon() == null ? null
                        : formatter.parse(saveRtaRequest.getReportedon()));
                tblRtaclaim.setReferencenumber(saveRtaRequest.getReferencenumber());
                tblRtaclaim.setInjlength(
                        saveRtaRequest.getInjlength() == null ? null : new BigDecimal(saveRtaRequest.getInjlength()));

                tblRtaclaim.setGaddress1(saveRtaRequest.getGaddress1());
                tblRtaclaim.setGaddress2(saveRtaRequest.getGaddress2());
                tblRtaclaim.setGaddress3(saveRtaRequest.getGaddress3());
                tblRtaclaim.setGcity(saveRtaRequest.getGcity());
                tblRtaclaim.setGemail(saveRtaRequest.getGemail());
                tblRtaclaim.setGfirstname(saveRtaRequest.getGfirstname());
                tblRtaclaim.setGlandline(saveRtaRequest.getGlandline());
                tblRtaclaim.setGlastname(saveRtaRequest.getGlastname());
                tblRtaclaim.setGmiddlename(saveRtaRequest.getGmiddlename());
                tblRtaclaim.setGmobile(saveRtaRequest.getGmobile());
                tblRtaclaim.setGpostalcode(saveRtaRequest.getGpostalcode());
                tblRtaclaim.setGregion(saveRtaRequest.getGregion());
                tblRtaclaim.setGtitle(saveRtaRequest.getGtitle());
                tblRtaclaim
                        .setGdob(saveRtaRequest.getGdob() == null ? null : formatter.parse(saveRtaRequest.getGdob()));

                tblRtaclaim.setYearofmanufacture(saveRtaRequest.getYearofmanufacture());
                tblRtaclaim.setTitle(saveRtaRequest.getTitle());
                tblRtaclaim.setVehiclecondition(saveRtaRequest.getVehiclecondition());
                tblRtaclaim.setOtherlanguages(saveRtaRequest.getOtherlanguages());
                tblRtaclaim.setAirbagopened(saveRtaRequest.getAirbagopened());
                tblRtaclaim.setVehicleType(saveRtaRequest.getVehicleType());
                tblRtaclaim.setMedicalevidence(saveRtaRequest.getMedicalevidence());
                tblRtaclaim.setVdImages(saveRtaRequest.getVdImages());
                tblRtaclaim.setInjurySustained(saveRtaRequest.getInjurySustained());

                TblCircumstance tblCircumstance = new TblCircumstance();
                List<TblRtainjury> tblRtainjuries = new ArrayList<>();

                if (saveRtaRequest.getCircumcode() != null) {
                    tblCircumstance.setCircumcode(Long.valueOf(saveRtaRequest.getCircumcode()));
                    tblRtaclaim.setTblCircumstance(tblCircumstance);
                } else {
                    tblRtaclaim.setTblCircumstance(null);
                }
                if (saveRtaRequest.getInjclasscodes() != null) {
                    TblRtainjury tblRtainjury;
                    for (String injury : saveRtaRequest.getInjclasscodes()) {
                        tblRtainjury = new TblRtainjury();
                        TblInjclass tblInjclass = new TblInjclass();
                        tblInjclass.setInjclasscode(Long.valueOf(injury));
                        tblRtainjury.setTblInjclass(tblInjclass);
                        tblRtainjuries.add(tblRtainjury);
                    }
                    tblRtaclaim.setInjclasscodes(tblRtainjuries);
                } else {
                    tblRtaclaim.setTblInjclass(null);
                }

                tblRtaclaim.setStatuscode("1");

                tblRtaclaim.setUsercode(tblUser.getUsercode());
                tblRtaclaim.setIntroducer(saveRtaRequest.getIntroducer());
                tblRtaclaim.setAdvisor(saveRtaRequest.getAdvisor());
                TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(String.valueOf(tblRtaclaim.getIntroducer()));
//                int rtaDocNumber = rtaService.getRtaDocNumber();

                tblRtaclaim.setRtanumber(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());

                TblRtaclaim rtaclaim = rtaService.addNewRtaCase(tblRtaclaim, null, saveRtaRequest.getFiles());

                /// check duplicates and send email to legal internal
                List<RtaDuplicatesResponse> rtaDuplicates = rtaService.getRtaDuplicates(String.valueOf(rtaclaim.getRtacode()));
                if (rtaDuplicates != null && rtaDuplicates.size() > 0) {

                    TblEmailTemplate tblEmailTemplate = rtaService.findByEmailType("Rta-Duplicate");
                    String legalbody = tblEmailTemplate.getEmailtemplate();
                    TblUser legalUser = rtaService.findByUserId("2");

                    legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
                    legalbody = legalbody.replace("[CASE_NUMBER]", tblRtaclaim.getRtanumber());
                    legalbody = legalbody.replace("[CLIENT_NAME]",
                            tblRtaclaim.getFirstname() + " "
                                    + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename() + " ")
                                    + tblRtaclaim.getLastname());

                    String duplicateReferenceNumber = rtaDuplicates.stream()
                            .map(RtaDuplicatesResponse::getCaseNumber)
                            .collect(Collectors.joining(", "));

                    legalbody = legalbody.replace("[DUPLICATE_REFRENCES]", duplicateReferenceNumber);
                    legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblRtaclaim.getRtacode());

                    rtaService.saveEmail(legalUser.getUsername(), legalbody,
                            tblRtaclaim.getRtanumber() + " | Processing Note", tblRtaclaim, legalUser);


                }


                if (rtaclaim != null && rtaclaim.getRtacode() > 0) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = rtaService.saveCompanyProfile(tblCompanyprofile);

                    ///// ADD PASSENGERS IF ANY////////
                    if (saveRtaRequest.getPassengers() != null && saveRtaRequest.getPassengers().size() > 0) {
                        TblRtapassenger tblRtapassenger = null;

                        for (RtaPassengerRequest rtaPassengerRequest : saveRtaRequest.getPassengers()) {
                            tblRtapassenger = new TblRtapassenger();
                            tblRtapassenger.setAddress1(rtaPassengerRequest.getAddress1());
                            tblRtapassenger.setAddress2(rtaPassengerRequest.getAddress2());
                            tblRtapassenger.setAddress3(rtaPassengerRequest.getAddress3());
                            tblRtapassenger.setCity(rtaPassengerRequest.getCity());
                            tblRtapassenger.setDob(rtaPassengerRequest.getDob() == null ? null
                                    : formatter.parse(rtaPassengerRequest.getDob()));
                            tblRtapassenger.setDriverpassenger(rtaPassengerRequest.getDriverpassenger());
                            tblRtapassenger.setEmail(rtaPassengerRequest.getEmail());
                            tblRtapassenger.setEnglishlevel(rtaPassengerRequest.getEnglishlevel());
                            tblRtapassenger.setFirstname(rtaPassengerRequest.getFirstname());
                            tblRtapassenger.setGcity(rtaPassengerRequest.getGcity());
                            tblRtapassenger.setGaddress1(rtaPassengerRequest.getGaddress1());
                            tblRtapassenger.setGaddress2(rtaPassengerRequest.getGaddress2());
                            tblRtapassenger.setGaddress3(rtaPassengerRequest.getGaddress3());
                            tblRtapassenger.setGemail(rtaPassengerRequest.getGemail());
                            tblRtapassenger.setGfirstname(rtaPassengerRequest.getGfirstname());
                            tblRtapassenger.setGlandline(rtaPassengerRequest.getGlandline());
                            tblRtapassenger.setGlastname(rtaPassengerRequest.getGlastname());
                            tblRtapassenger.setGmiddlename(rtaPassengerRequest.getGmiddlename());
                            tblRtapassenger.setGmobile(rtaPassengerRequest.getGmobile());
                            tblRtapassenger.setGpostalcode(rtaPassengerRequest.getGpostalcode());
                            tblRtapassenger.setGregion(rtaPassengerRequest.getGregion());
                            tblRtapassenger.setGtitle(rtaPassengerRequest.getGtitle());
                            tblRtapassenger.setGdob(rtaPassengerRequest.getGdob() == null ? null
                                    : formatter.parse(rtaPassengerRequest.getGdob()));
                            tblRtapassenger.setInjdescr(rtaPassengerRequest.getInjdescription());
                            tblRtapassenger.setInjlength(rtaPassengerRequest.getInjlength() == null ? null
                                    : new BigDecimal(rtaPassengerRequest.getInjlength()));
                            tblRtapassenger.setLandline(rtaPassengerRequest.getLandline());
                            tblRtapassenger.setLastname(rtaPassengerRequest.getLastname());
                            tblRtapassenger.setMedicalinfo(rtaPassengerRequest.getMedicalinfo());
                            tblRtapassenger.setMiddlename(rtaPassengerRequest.getMiddlename());
                            tblRtapassenger.setMobile(rtaPassengerRequest.getMobile());
                            tblRtapassenger.setNinumber(rtaPassengerRequest.getNinumber());
                            tblRtapassenger.setOngoing(rtaPassengerRequest.getOngoing());
                            tblRtapassenger.setPostalcode(rtaPassengerRequest.getPostalcode());
                            tblRtapassenger.setRegion(rtaPassengerRequest.getRegion());
                            tblRtapassenger.setTitle(rtaPassengerRequest.getTitle());
                            tblRtapassenger.setAlternativenumber(rtaPassengerRequest.getAlternativenumber());
                            tblRtapassenger.setEvidencedetails(rtaPassengerRequest.getMedicalevidence());
                            tblRtapassenger.setDetails(rtaPassengerRequest.getDetail());

                            List<TblRtaPassengerInjury> pasengerTblRtainjuries = new ArrayList<>();
                            TblRtaPassengerInjury pasengerTblRtainjury;
                            if (rtaPassengerRequest.getInjclasscodes() != null && rtaPassengerRequest.getInjclasscodes().size() > 0) {
                                for (String injury : rtaPassengerRequest.getInjclasscodes()) {
                                    pasengerTblRtainjury = new TblRtaPassengerInjury();
                                    pasengerTblRtainjury.setInjclasscode(Long.valueOf(injury));
                                    pasengerTblRtainjuries.add(pasengerTblRtainjury);
                                }
                            } else {
                                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                             //   return getResponseFormat(HttpStatus.BAD_REQUEST, "Passenger Couldnot be saved No Injuries Selected", null);
                            }
                            tblRtapassenger.setPasengerTblRtainjuries(pasengerTblRtainjuries);

                            tblRtapassengers.add(tblRtapassenger);

                        }

                        saveRtapassengers = rtaService.addNewRtaPassengers(rtaclaim, tblRtapassengers);

                    } ////// END ADD PASSENGER IF ANY //////

                    LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", rtaclaim);

                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Saving RTA Case", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addNewRtaCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/updateRtaCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateRtaCase(@RequestBody UpdateRtaRequest updateRtaRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == updateRtaCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == updateRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "1");

                if (allowed) {
                    if (updateRtaRequest.getRtacode() > 0) {
                        TblRtaclaim tblRtaclaim = rtaService.getTblRtaclaimById(updateRtaRequest.getRtacode());

                        tblRtaclaim.setAccdate(formatter.parse(updateRtaRequest.getAccdate()));
                        tblRtaclaim.setAcctime(updateRtaRequest.getAcctime());
                        tblRtaclaim.setAddress1(updateRtaRequest.getAddress1());
                        tblRtaclaim.setAddress2(updateRtaRequest.getAddress2());
                        tblRtaclaim.setAddress3(updateRtaRequest.getAddress3());
                        tblRtaclaim.setCity(updateRtaRequest.getCity());
                        tblRtaclaim.setContactdue(updateRtaRequest.getContactdue() == null ? null
                                : formatter.parse(updateRtaRequest.getContactdue()));
                        tblRtaclaim.setDescription(updateRtaRequest.getDescription());
                        tblRtaclaim.setDob(formatter.parse(updateRtaRequest.getDob()));
                        tblRtaclaim.setDriverpassenger(updateRtaRequest.getDriverpassenger());
                        tblRtaclaim.setEmail(updateRtaRequest.getEmail());
                        tblRtaclaim.setEnglishlevel(updateRtaRequest.getEnglishlevel());
                        tblRtaclaim.setFirstname(updateRtaRequest.getFirstname());
                        tblRtaclaim.setGreencardno(updateRtaRequest.getGreencardno());
                        tblRtaclaim.setInjdescription(updateRtaRequest.getInjdescription());
                        tblRtaclaim.setInsurer(updateRtaRequest.getInsurer());
                        tblRtaclaim.setLandline(updateRtaRequest.getLandline());
                        tblRtaclaim.setLastname(updateRtaRequest.getLastname());
                        tblRtaclaim.setLocation(updateRtaRequest.getLocation());
                        tblRtaclaim.setMakemodel(updateRtaRequest.getMakemodel());
                        tblRtaclaim.setMedicalinfo(updateRtaRequest.getMedicalinfo());
                        tblRtaclaim.setMiddlename(updateRtaRequest.getMiddlename());
                        tblRtaclaim.setMobile(updateRtaRequest.getMobile());
                        tblRtaclaim.setNinumber(updateRtaRequest.getNinumber());
                        tblRtaclaim.setOngoing(updateRtaRequest.getOngoing());
                        tblRtaclaim.setPartyaddress(updateRtaRequest.getPartyaddress());
                        tblRtaclaim.setPartycontactno(updateRtaRequest.getPartycontactno());
                        tblRtaclaim.setPartyinsurer(updateRtaRequest.getPartyinsurer());
                        tblRtaclaim.setPartymakemodel(updateRtaRequest.getPartymakemodel());
                        tblRtaclaim.setPartyname(updateRtaRequest.getPartyname());
                        tblRtaclaim.setPartypolicyno(updateRtaRequest.getPartypolicyno());
                        tblRtaclaim.setPartyrefno(updateRtaRequest.getPartyrefno());
                        tblRtaclaim.setPartyregno(updateRtaRequest.getPartyregno());
                        tblRtaclaim.setPassengerinfo(updateRtaRequest.getPassengerinfo());
                        tblRtaclaim.setPolicyno(updateRtaRequest.getPolicyno());
                        tblRtaclaim.setPostalcode(updateRtaRequest.getPostalcode());
                        tblRtaclaim.setRdweathercond(updateRtaRequest.getRdweathercond());
                        tblRtaclaim.setRefno(updateRtaRequest.getRefno());
                        tblRtaclaim.setRegion(updateRtaRequest.getRegion());
                        tblRtaclaim.setRegisterationno(updateRtaRequest.getRegisterationno());
                        tblRtaclaim.setReportedtopolice(updateRtaRequest.getReportedtopolice());
                        tblRtaclaim.setScotland(updateRtaRequest.getScotland());
                        tblRtaclaim.setTitle(updateRtaRequest.getTitle());
                        tblRtaclaim.setVehiclecondition(updateRtaRequest.getVehiclecondition());
                        tblRtaclaim.setPassword(updateRtaRequest.getPassword());
                        tblRtaclaim.setTranslatordetails(updateRtaRequest.getTranslatordetail());
                        tblRtaclaim.setAlternativenumber(updateRtaRequest.getAlternativenumber());
                        tblRtaclaim.setMedicalevidence(updateRtaRequest.getEvidencedatails());
                        tblRtaclaim.setReportedon(updateRtaRequest.getReportedon() == null ? null
                                : formatter.parse(updateRtaRequest.getReportedon()));

                        tblRtaclaim.setGaddress1(updateRtaRequest.getGaddress1());
                        tblRtaclaim.setGaddress2(updateRtaRequest.getGaddress2());
                        tblRtaclaim.setGaddress3(updateRtaRequest.getGaddress3());
                        tblRtaclaim.setGcity(updateRtaRequest.getGcity());
                        tblRtaclaim.setGemail(updateRtaRequest.getGemail());
                        tblRtaclaim.setGfirstname(updateRtaRequest.getGfirstname());
                        tblRtaclaim.setGlandline(updateRtaRequest.getGlandline());
                        tblRtaclaim.setGlastname(updateRtaRequest.getGlastname());
                        tblRtaclaim.setGmiddlename(updateRtaRequest.getGmiddlename());
                        tblRtaclaim.setGmobile(updateRtaRequest.getGmobile());
                        tblRtaclaim.setGpostalcode(updateRtaRequest.getGpostalcode());
                        tblRtaclaim.setGregion(updateRtaRequest.getGregion());
                        tblRtaclaim.setGtitle(updateRtaRequest.getGtitle());
                        tblRtaclaim.setGdob(updateRtaRequest.getGdob() == null ? null
                                : formatter.parse(updateRtaRequest.getGdob()));
                        tblRtaclaim.setYearofmanufacture(updateRtaRequest.getYearofmanufacture());
                        tblRtaclaim.setReferencenumber(updateRtaRequest.getReferencenumber());
                        tblRtaclaim.setInjlength(updateRtaRequest.getInjlength() == null ? null
                                : new BigDecimal(updateRtaRequest.getInjlength()));
                        tblRtaclaim.setAirbagopened(updateRtaRequest.getAirbagopened());
                        tblRtaclaim.setVehicleType(updateRtaRequest.getVehicleType());
                        tblRtaclaim.setMedicalevidence(updateRtaRequest.getMedicalevidence());
                        tblRtaclaim.setVdImages(updateRtaRequest.getVdImages());
                        tblRtaclaim.setInjurySustained(updateRtaRequest.getInjurySustained());
                        tblRtaclaim.setClawbackDate(updateRtaRequest.getClawbackDate());

                        TblCircumstance tblCircumstance = new TblCircumstance();

                        tblCircumstance.setCircumcode(Long.valueOf(updateRtaRequest.getCircumcode()));
                        tblRtaclaim.setTblCircumstance(tblCircumstance);

                        List<TblRtainjury> tblRtainjuries = new ArrayList<>();
                        TblRtainjury tblRtainjury;
                        for (String injury : updateRtaRequest.getInjclasscodes()) {
                            tblRtainjury = new TblRtainjury();
                            TblInjclass tblInjclass = new TblInjclass();
                            tblInjclass.setInjclasscode(Long.valueOf(injury));
                            tblRtainjury.setTblInjclass(tblInjclass);
                            tblRtainjuries.add(tblRtainjury);
                        }
                        tblRtaclaim.setInjclasscodes(tblRtainjuries);

                        tblRtaclaim.setLastupdated(new Date());
                        tblRtaclaim.setLastupdateuser(tblUser.getUsercode());

                        TblRtaclaim rtaclaim = rtaService.updateRtaCase(tblRtaclaim, null);

                        /// Alert Email tp send email to Introducer and Solicitor
                        List<RtaAuditLogResponse> rtaAuditLogResponses = rtaService.getTodayRtaAuditLogs(rtaclaim.getRtacode());
                        if (rtaAuditLogResponses != null && rtaAuditLogResponses.size() > 0) {


                            TblEmailTemplate tblEmailTemplate = rtaService.findByEmailType("Rta-Duplicate");
                            String Introducerbody = tblEmailTemplate.getEmailtemplate();
                            String solicitorbody = tblEmailTemplate.getEmailtemplate();
                            TblUser introducerUser = rtaService.findByUserId(String.valueOf(rtaclaim.getAdvisor()));


                            Introducerbody = Introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
                            Introducerbody = Introducerbody.replace("[CASE_NUMBER]", tblRtaclaim.getRtanumber());
                            Introducerbody = Introducerbody.replace("[CLIENT_NAME]",
                                    tblRtaclaim.getFirstname() + " "
                                            + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename() + " ")
                                            + tblRtaclaim.getLastname());

                            String fieldName = rtaAuditLogResponses.stream()
                                    .map(RtaAuditLogResponse::getFieldName)
                                    .collect(Collectors.joining("\n "));
                            String oldValue = rtaAuditLogResponses.stream()
                                    .map(RtaAuditLogResponse::getOldValue)
                                    .collect(Collectors.joining("\n "));
                            String newValue = rtaAuditLogResponses.stream()
                                    .map(RtaAuditLogResponse::getNewValue)
                                    .collect(Collectors.joining("\n "));

                            Introducerbody = Introducerbody.replace("[FIELD_NAME]", fieldName);
                            Introducerbody = Introducerbody.replace("[OLD_VALUE]", oldValue);
                            Introducerbody = Introducerbody.replace("[NEW_VALUE]", newValue);
                            Introducerbody = Introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblRtaclaim.getRtacode());

                            rtaService.saveEmail(introducerUser.getUsername(), Introducerbody,
                                    tblRtaclaim.getRtanumber() + " | Processing Note", tblRtaclaim, introducerUser);


                            TblRtasolicitor tblRtasolicitor = rtaService.getRtaSolicitorsOfRtaClaim(String.valueOf(rtaclaim.getRtacode()));
                            if (tblRtasolicitor != null) {
                                TblUser solicitorUser = rtaService.findByUserId(String.valueOf(tblRtasolicitor.getUsercode()));
                                // solicitor EMAIL
                                solicitorbody = solicitorbody.replace("[USER_NAME]", solicitorUser.getLoginid());
                                solicitorbody = solicitorbody.replace("[CASE_NUMBER]", tblRtaclaim.getRtanumber());
                                solicitorbody = solicitorbody.replace("[CLIENT_NAME]",
                                        tblRtaclaim.getFirstname() + " "
                                                + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename() + " ")
                                                + tblRtaclaim.getLastname());

                                solicitorbody = solicitorbody.replace("[FIELD_NAME]", fieldName);
                                solicitorbody = solicitorbody.replace("[OLD_VALUE]", oldValue);
                                solicitorbody = solicitorbody.replace("[NEW_VALUE]", newValue);
                                solicitorbody = solicitorbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblRtaclaim.getRtacode());

                                rtaService.saveEmail(solicitorUser.getUsername(), solicitorbody,
                                        tblRtaclaim.getRtanumber() + " | Processing Note", tblRtaclaim, solicitorUser);
                            }


                        }


                        if (rtaclaim != null && rtaclaim.getRtacode() > 0) {

                            LOG.info("\n EXITING THIS METHOD == updateRtaCase(); \n\n\n");
                            return getResponseFormat(HttpStatus.OK, "Case Updated SuccessFully", rtaclaim);

                        } else {
                            LOG.info("\n EXITING THIS METHOD == updateRtaCase(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Updating Case.", null);
                        }
                    } else {
                        LOG.info("\n EXITING THIS METHOD == updateRtaCase(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                    }

                } else {
                    LOG.info("\n EXITING THIS METHOD == updateRtaCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                            null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == updateRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == updateRtaCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == updateRtaCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addNoteToRta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNoteToRta(@RequestBody RtaNoteRequest rtaNoteRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addNoteToRta(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                if (!rtaNoteRequest.getRtaCode().isEmpty()) {
                    TblRtanote tblRtanote = new TblRtanote();
                    TblRtaclaim tblRtaclaim = new TblRtaclaim();

                    tblRtaclaim.setRtacode(Long.valueOf(rtaNoteRequest.getRtaCode()));
                    tblRtanote.setTblRtaclaim(tblRtaclaim);
                    tblRtanote.setNote(rtaNoteRequest.getNote());
                    tblRtanote.setUsercategorycode(
                            rtaNoteRequest.getUserCatCode() == null || rtaNoteRequest.getUserCatCode().isEmpty() ? "4"
                                    : rtaNoteRequest.getUserCatCode());
                    tblRtanote.setCreatedon(new Date());
                    tblRtanote.setUsercode(tblUser.getUsercode());

                    tblRtanote = rtaService.addNoteToRta(tblRtanote);

                    if (tblRtanote != null && tblRtanote.getRtanotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblRtanote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addNoteToRta();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNoteToRta(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnRta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnRta(
            @RequestBody PerformActionOnRtaRequest performActionOnRtaRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnRta(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnRta(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            ViewRtaclamin viewRtaclamin = rtaService
                    .getAuthRtaCase(String.valueOf(performActionOnRtaRequest.getRtacode()), tblUser);
            if (tblUser != null) {
                if (performActionOnRtaRequest.getToStatus().equals("7")) {
                    List<TblRtatask> tblRtatask = rtaService
                            .getRtaTaskAgainstRtaCodeAndStatus(performActionOnRtaRequest.getRtacode(), "N");
                    List<TblRtatask> tblRtatask1 = rtaService
                            .getRtaTaskAgainstRtaCodeAndStatus(performActionOnRtaRequest.getRtacode(), "P");

                    if (tblRtatask != null && tblRtatask.size() > 0) {
                        LOG.info("\n EXITING THIS METHOD == performActionOnRta(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Some Task Are Not Performed", viewRtaclamin);
                    } else if (tblRtatask1 != null && tblRtatask1.size() > 0) {
                        LOG.info("\n EXITING THIS METHOD == performActionOnRta(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Some Task Are In Pending State", viewRtaclamin);
                    } else {

                    }
                }

                TblRtaclaim tblRtaclaim = null;
                if (performActionOnRtaRequest.getToStatus().equals("11")) {
                    // reject status
                    tblRtaclaim = rtaService.performRejectCancelActionOnRta(performActionOnRtaRequest.getRtacode(),
                            performActionOnRtaRequest.getToStatus(), performActionOnRtaRequest.getReason(), tblUser);

                } else if (performActionOnRtaRequest.getToStatus().equals("14")) {
                    // cancel status
                    tblRtaclaim = rtaService.performRejectCancelActionOnRta(performActionOnRtaRequest.getRtacode(),
                            performActionOnRtaRequest.getToStatus(), performActionOnRtaRequest.getReason(), tblUser);

                } else if (performActionOnRtaRequest.getToStatus().equals("169")) {
                    // revert status
                    tblRtaclaim = rtaService.performRevertActionOnRta(performActionOnRtaRequest.getRtacode(),
                            performActionOnRtaRequest.getToStatus(), performActionOnRtaRequest.getReason(), tblUser);

                } else {
                    tblRtaclaim = rtaService.performActionOnRta(performActionOnRtaRequest.getRtacode(),
                            performActionOnRtaRequest.getToStatus(), tblUser);
                }

                if (tblRtaclaim != null) {
                    ViewRtaclamin Rtaclamin = rtaService.getAuthRtaCase(String.valueOf(tblRtaclaim.getRtacode()),
                            tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnRta(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", Rtaclamin);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnRta(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnRta(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnRta();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnRta(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnRtaFromDirectIntro", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnRtaFromDirectIntro(
            @RequestBody PerformHotKeyOnRtaRequest performHotKeyOnRtaRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnRtaFromDirectIntro(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnRtaFromDirectIntro(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblRtaclaim tblRtaclaim = rtaService.performActionOnRtaFromDirectIntro(performHotKeyOnRtaRequest,
                        tblUser);
                if (tblRtaclaim != null) {
                    ViewRtaclamin viewRtaclamin = rtaService.getAuthRtaCase(String.valueOf(tblRtaclaim.getRtacode()),
                            tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnRtaFromDirectIntro(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", viewRtaclamin);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnRtaFromDirectIntro(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnRtaFromDirectIntro(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnRtaFromDirectIntro();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnRtaFromDirectIntro(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addWorkFlow", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addWorkFlow(@RequestBody TblRtaflowRequest tblRtaflowRequest,
                                                               HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addWorkFlow(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addWorkFlow(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {

                TblRtaflow tblRtaflow = new TblRtaflow();
                tblRtaflow.setCompanycode(tblRtaflowRequest.getCompanycode());
                tblRtaflow.setTaskflag(tblRtaflowRequest.getTaskflag());
                tblRtaflow.setEditflag(tblRtaflowRequest.getEditflag());
                tblRtaflow.setUsercategory(tblRtaflowRequest.getUsercategory());
                tblRtaflow.setCreatedon(new Date());
                tblRtaflow.setUsercode(tblUser.getUsercode());

                TblRtastatus tblRtastatus = new TblRtastatus();
                TblCompaign tblCompaign = new TblCompaign();

                tblRtastatus.setStatuscode(Long.valueOf((tblRtaflowRequest.getStatuscode().split("---"))[0].trim()));
                tblCompaign.setCompaigncode(tblRtaflowRequest.getCompaingcode());

                tblRtaflow.setTblRtastatus(tblRtastatus);
                tblRtaflow.setTblCompaign(tblCompaign);

                tblRtaflow = rtaService.addRtaFlow(tblRtaflow);

                if (tblRtaflow != null) {

                    LOG.info("\n EXITING THIS METHOD == addWorkFlow(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblRtaflow);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addWorkFlow(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == addWorkFlow(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addWorkFlow();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addWorkFlow(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addWorkFlowDetail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addWorkFlowDetail(
            @RequestBody TblrtaflowdetailRequest tblrtaflowdetailRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addWorkFlowDetail(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addWorkFlowDetail(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                ;
                TblRtaflowdetail tblRtaflowdetail = new TblRtaflowdetail();
                tblRtaflowdetail.setApiflag(tblrtaflowdetailRequest.getApiflag() == null ? "N" : tblrtaflowdetailRequest.getApiflag());
                tblRtaflowdetail.setButtonname(tblrtaflowdetailRequest.getButtonname());
                tblRtaflowdetail.setButtonvalue((tblrtaflowdetailRequest.getRtastatuscode().split("---"))[0].trim());
                tblRtaflowdetail.setListusercategory(tblrtaflowdetailRequest.getListusercategory() == null ? "N" : tblrtaflowdetailRequest.getListusercategory());
                tblRtaflowdetail.setCaseacceptdialog(tblrtaflowdetailRequest.getCaseacceptdialog() == null ? "N" : tblrtaflowdetailRequest.getCaseacceptdialog());
                tblRtaflowdetail.setCaseassigndialog(tblrtaflowdetailRequest.getCaseassigndialog() == null ? "N" : tblrtaflowdetailRequest.getCaseassigndialog());
                tblRtaflowdetail.setCaserejectdialog(tblrtaflowdetailRequest.getCaserejectdialog() == null ? "N" : tblrtaflowdetailRequest.getCaserejectdialog());

                TblRtaflow tblRtaflow = new TblRtaflow();
                TblRtastatus tblRtastatus = new TblRtastatus();

                tblRtastatus.setStatuscode(Long.valueOf((tblrtaflowdetailRequest.getRtastatuscode().split("---"))[0].trim()));
                tblRtaflow.setRtaflowcode(Long.valueOf(tblrtaflowdetailRequest.getRtaflowcode()));

                tblRtaflowdetail.setTblRtastatus(tblRtastatus);
                tblRtaflowdetail.setTblRtaflow(tblRtaflow);

                tblRtaflowdetail = rtaService.addWorkFlowDetail(tblRtaflowdetail);

                if (tblRtaflowdetail != null) {

                    LOG.info("\n EXITING THIS METHOD == addWorkFlowDetail(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblRtaflowdetail);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addWorkFlowDetail(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == addWorkFlowDetail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaPostApi \n METHOD == addWorkFlowDetail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addWorkFlowDetail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addRtaDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> addRtaDocument(@RequestParam("rtaClaimCode") String rtaClaimCode,
                                                                  @RequestParam("multipartFiles") List<MultipartFile> multipartFiles, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addRtaDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblRtaclaim tblRtaclaim = rtaService.getTblRtaclaimById(Long.valueOf(rtaClaimCode));
            if (tblRtaclaim != null && tblRtaclaim.getRtacode() > 0) {
//                String UPLOADED_FOLDER = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0_Tomcat9-la\\webapps\\rta\\";
//                String UPLOADED_SERVER = "http:\\115.186.147.30:8080\\rta\\";
                String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\rta\\";
                String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\rta\\";
                List<TblRtadocument> tblRtadocuments = new ArrayList<TblRtadocument>();

                File theDir = new File(UPLOADED_FOLDER + tblRtaclaim.getRtacode());
                if (!theDir.exists()) {
                    theDir.mkdirs();
                }

                for (MultipartFile multipartFile : multipartFiles) {
                    byte[] bytes = multipartFile.getBytes();
                    Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                    Files.write(path, bytes);
                    TblRtadocument tblRtadocument = new TblRtadocument();
                    tblRtadocument.setDocurl(
                            UPLOADED_SERVER + tblRtaclaim.getRtacode() + "\\" + multipartFile.getOriginalFilename());
                    tblRtadocument.setDoctype(multipartFile.getContentType());
                    tblRtadocument.setTblRtaclaim(tblRtaclaim);
                    tblRtadocument.setUsercode(tblUser.getUsercode());
                    tblRtadocument.setCreatedon(new Date());

                    tblRtadocuments.add(tblRtadocument);
                }

                tblRtadocuments = rtaService.addRtaDocument(tblRtadocuments);

                // Legal internal Email
                TblEmailTemplate tblEmailTemplate = rtaService.findByEmailType("document-add");
                String legalbody = tblEmailTemplate.getEmailtemplate();
                TblUser legalUser = rtaService.findByUserId("2");

                legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
                legalbody = legalbody.replace("[CASE_NUMBER]", tblRtaclaim.getRtanumber());
                legalbody = legalbody.replace("[CLIENT_NAME]",
                        tblRtaclaim.getFirstname() + " "
                                + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename() + " ")
                                + tblRtaclaim.getLastname());
                legalbody = legalbody.replace("[COUNT]", String.valueOf(tblRtadocuments.size()));
                legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblRtaclaim.getRtacode());

                rtaService.saveEmail(legalUser.getUsername(), legalbody,
                        tblRtaclaim.getRtanumber() + " | Processing Note", tblRtaclaim, legalUser);

                LOG.info("\n EXITING THIS METHOD == addRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Document Saved Successfully", null);
            } else {
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Find", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addRtaDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addRtaDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addESign(); ");
        try {
            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("code")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }

            TblRtaclaim tblRtaclaim = rtaService.getTblRtaclaimById(addESign.getRtaCode());
            if (tblRtaclaim != null && tblRtaclaim.getRtacode() > 0) {

                if (tblRtaclaim.getEsig().equals("Y")) {
                    List<TblRtadocument> tblRtadocuments = rtaService.getAuthRtaCasedocuments(String.valueOf(tblRtaclaim.getRtacode()));
                    for (TblRtadocument tblRtadocument : tblRtadocuments) {
                        if (tblRtadocument.getDoctype().equalsIgnoreCase("Esign")) {
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("signedDocument", tblRtadocument.getDocbase64());
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.OK, "You Have Already Signed The Document",
                                    map);
                        }
                    }

                }
                // solicitor company
                TblCompanyprofile tblCompanyprofile = rtaService
                        .getCompanyProfileAgainstRtaCode(tblRtaclaim.getRtacode());
                if (tblCompanyprofile != null) {

                    TblCompanydoc tblCompanydoc = new TblCompanydoc();
                    if (tblRtaclaim.getScotland().equals("Y")) { // for company scotland
                        if (tblRtaclaim.getGfirstname() == null) { // for adult
                            tblCompanydoc = rtaService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "S");
                        } else { // for minor
                            tblCompanydoc = rtaService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "M", "S");
                        }
                    } else { // for company UK
                        if (tblRtaclaim.getGfirstname() == null) { // for adult
                            tblCompanydoc = rtaService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "E");
                        } else { // for minor
                            tblCompanydoc = rtaService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "M", "E");
                        }
                    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                    byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                    ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                    BufferedImage image1 = ImageIO.read(bis);
                    bis.close();


                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();

                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            while (keys.hasNext()) {
                                String key = keys.next();
                                if (!key.equals("code") && !key.equals("eSign")) {
                                    if (fieldName.equals(key)) {
                                        textField.setValue(jsonObject.getString(key));
                                    }
                                }
                            }

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = rtaService.getRtaDbColumnValue(fieldName.replace("DB_", ""),
                                        tblRtaclaim.getRtacode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            }
                            if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    for (int a = 0; a < document.getNumberOfPages(); a++) {
                        PDPage p = document.getPage(a);
                        PDResources resources = p.getResources();
                        for (COSName xObjectName : resources.getXObjectNames()) {
                            PDXObject xObject = resources.getXObject(xObjectName);
                            if (xObject instanceof PDImageXObject) {
                                PDImageXObject original_img = ((PDImageXObject) xObject);
                                PDImageXObject pdImageXObject = LosslessFactory.createFromImage(document, image1);
                                resources.put(xObjectName, pdImageXObject);
                            }
                        }
                    }

                    LOG.info("\n Esign Document Prepared SuccessFully\n");
                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\rta\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\rta\\";

                    File theDir = new File(UPLOADED_FOLDER + tblRtaclaim.getRtacode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblRtaclaim.getRtanumber() + "_Signed.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblRtaclaim.getRtanumber() + "_Signed.pdf");
                    }

                    tblRtaclaim.setEsig("Y");
                    tblRtaclaim = rtaService.updateRtaCase(tblRtaclaim, null);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    LOG.info("\n Esign Saved on folder SuccessFully SuccessFully\n");
                    File file = new File(theDir.getPath() + "\\" + tblRtaclaim.getRtanumber() + "_Signed.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    TblTask tblRtataskdocument = new TblTask();
                    tblRtataskdocument.setTaskcode(1);

                    TblRtadocument tblRtadocument = new TblRtadocument();
                    tblRtadocument.setTblRtaclaim(tblRtaclaim);
                    tblRtadocument.setDocbase64(encoded);
                    tblRtadocument.setDocname("Esign");
                    tblRtadocument.setDoctype("Esign");
                    tblRtadocument.setCreatedon(new Date());
                    tblRtadocument.setUsercode("1");
                    tblRtadocument.setDocurl(
                            UPLOADED_SERVER + tblRtaclaim.getRtacode() + "\\" + tblRtaclaim.getRtanumber() + "_Signed.pdf");
                    tblRtadocument.setTblTask(tblRtataskdocument);

                    tblRtadocument = rtaService.addRtaDocumentSingle(tblRtadocument);
                    LOG.info("\n saving doc in database\n");
                    if (tblRtadocument != null && tblRtadocument.getRtadoccode() > 0) {
//                    Locale.setDefault(backup);

                        TblRtatask tblRtatask = new TblRtatask();
                        tblRtatask.setRtataskcode(1);
                        tblRtatask.setTblRtaclaim(tblRtaclaim);
                        tblRtatask.setStatus("C");
                        tblRtatask.setRemarks("Completed");
                        tblRtatask.setCompletedon(new Date());

                        int tblRtatask1 = rtaService.updateTblRtaTask(tblRtatask, tblRtaclaim.getRtacode());
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("doc", encoded);
                        TblUser legalUser = rtaService.findByUserId("2");

                        // cleint Email
                        TblEmailTemplate tblEmailTemplate = rtaService.findByEmailType("esign-clnt-complete");
                        String clientbody = tblEmailTemplate.getEmailtemplate();

                        clientbody = clientbody.replace("[LEGAL_CLIENT_NAME]",
                                tblRtaclaim.getFirstname() + " "
                                        + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename() + " ")
                                        + tblRtaclaim.getLastname());
                        clientbody = clientbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());

                        rtaService.saveEmail(tblRtaclaim.getEmail(), clientbody, "No Win No Fee Agreement ", tblRtaclaim, legalUser, file.getPath());
                        LOG.info("\n Email Sent To client\n");
                        // Introducer Email
                        tblEmailTemplate = rtaService.findByEmailType("esign-Int-sol-complete");
                        String Introducerbody = tblEmailTemplate.getEmailtemplate();
                        TblUser introducerUser = rtaService.findByUserId(String.valueOf(tblRtaclaim.getAdvisor()));

                        Introducerbody = Introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
                        Introducerbody = Introducerbody.replace("[CASE_NUMBER]", tblRtaclaim.getRtanumber());
                        Introducerbody = Introducerbody.replace("[CLIENT_NAME]",
                                tblRtaclaim.getFirstname() + " "
                                        + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename() + " ")
                                        + tblRtaclaim.getLastname());
                        Introducerbody = Introducerbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        Introducerbody = Introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblRtaclaim.getRtacode());

                        rtaService.saveEmail(introducerUser.getUsername(), Introducerbody, "Esign", tblRtaclaim, introducerUser);
                        LOG.info("\n Email Sent to intro\n");
                        // PI department Email
                        String LegalPideptbody = tblEmailTemplate.getEmailtemplate();

                        LegalPideptbody = LegalPideptbody.replace("[USER_NAME]", legalUser.getLoginid());
                        LegalPideptbody = LegalPideptbody.replace("[CASE_NUMBER]", tblRtaclaim.getRtanumber());
                        LegalPideptbody = LegalPideptbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        LegalPideptbody = LegalPideptbody.replace("[CLIENT_NAME]",
                                tblRtaclaim.getFirstname() + " "
                                        + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename() + " ")
                                        + tblRtaclaim.getLastname());
                        LegalPideptbody = LegalPideptbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblRtaclaim.getRtacode());

                        rtaService.saveEmail(legalUser.getUsername(), LegalPideptbody, "Esign", tblRtaclaim, legalUser);


                        saveEsignSubmitStatus(String.valueOf(tblRtaclaim.getRtacode()), "1", request);

                        LOG.info("\n Email Sent to Dept\n");
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "You Have SuccessFully Signed The CFA, A Copy Of That CFA Has Been Sent To The Provided Email.", map);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST,
                                "Error While Saving The Document..Esign Compeleted", null);
                    }

                } else {
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No document template found against provided RTA",
                            null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == getESignFields(); ");
        try {

            TblRtaclaim tblRtaclaim = rtaService.getTblRtaclaimById(Long.valueOf(eSignFieldsRequest.getCode()));

            if (tblRtaclaim != null && tblRtaclaim.getRtacode() > 0) {
                TblCompanyprofile tblCompanyprofile = rtaService
                        .getCompanyProfileAgainstRtaCode(tblRtaclaim.getRtacode());
                if (!tblRtaclaim.getEsig().equals("Y")) {

                    String path = "";
                    TblCompanydoc tblCompanydoc = new TblCompanydoc();
                    if (tblRtaclaim.getVehicleType().equals("B")) {
                        tblCompanydoc = rtaService.getCompanyDocsRtaForBike(tblCompanyprofile.getCompanycode());
                    } else {

                        if (tblRtaclaim.getScotland().equals("Y")) { // for company scotland
                            if (tblRtaclaim.getGfirstname() == null) { // for adult
                                tblCompanydoc = rtaService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "S");
                            } else { // for minor
                                tblCompanydoc = rtaService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "M", "S");
                            }
                        } else { // for company UK
                            if (tblRtaclaim.getGfirstname() == null) { // for adult
                                tblCompanydoc = rtaService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "A", "E");
                            } else { // for minor
                                tblCompanydoc = rtaService.getCompanyDocs(tblCompanyprofile.getCompanycode(), "M", "E");
                            }
                        }
                    }

                    if (tblCompanydoc == null) {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "No CFA document found", null);
                    }
                    HashMap<String, String> fields = new HashMap<>();
                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();


                    /////////////////////// FILL the CFA With Client DATA ////////////////////////


                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = rtaService.getRtaDbColumnValue(fieldName.replace("DB_", ""),
                                        tblRtaclaim.getRtacode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            } else if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            } else {
                                fields.put(fieldName, fieldName);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    /////////////////////// END OF FILL the CFA With Client DATA /////////////////

                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\rta\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\rta\\";

                    File theDir = new File(UPLOADED_FOLDER + tblRtaclaim.getRtacode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblRtaclaim.getRtanumber() + "_view.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblRtaclaim.getRtanumber() + "_view.pdf");
                    }

                    File file = new File(theDir.getPath() + "\\" + tblRtaclaim.getRtanumber() + "_view.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    fields.put("doc", encoded);
                    fields.put("isEsign", "N");

                    saveEsignOpenStatus(String.valueOf(tblRtaclaim.getRtacode()), "1", request);

                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", fields);
                } else {

                    List<TblRtadocument> tblRtadocuments = rtaService.getAuthRtaCasedocuments(String.valueOf(tblRtaclaim.getRtacode()));
                    if (tblRtadocuments != null && tblRtadocuments.size() > 0) {
                        for (TblRtadocument tblRtadocument : tblRtadocuments) {
                            if (tblRtadocument.getTblTask() != null && tblRtadocument.getTblTask().getTaskcode() == 1) {
                                HashMap<String, String> fields = new HashMap<>();
                                fields.put("doc", tblRtadocument.getDocbase64());
                                fields.put("isEsign", "Y");
                                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                                return getResponseFormat(HttpStatus.OK, "You have Already Signed The Document.", fields);
                            }
                        }
                    }

                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Signed Document Found Please Contact Our HelpLine : please call us at 01615374448", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == getESignFields();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/resendRtaEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> resendRtaEmail(@RequestBody ResendRtaMessageDto resendRtaMessageDto,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == resendRtaEmail(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblEmail tblEmail = rtaService.resendRtaEmail(resendRtaMessageDto.getRtamessagecode());

            if (tblEmail != null && tblEmail.getEmailcode() > 0) {

                LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Success", tblEmail);
            } else {
                LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While resending Email", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == resendRtaEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("taskCode") String taskCode,
                                                               @RequestParam("rtaClaimCode") String rtaCode,
                                                               @RequestParam(value = "multipartFiles", required = false) List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblRtaclaim tblRtaclaim = rtaService.getTblRtaclaimById(Long.parseLong(rtaCode));

            if (tblRtaclaim != null && tblRtaclaim.getRtacode() > 0) {
                TblTask tblTask = rtaService.getTaskAgainstCode(Long.parseLong(taskCode));
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblRtatask tblRtatask = new TblRtatask();
                    tblRtatask.setRtataskcode(Long.parseLong(taskCode));
                    tblRtatask.setTblRtaclaim(tblRtaclaim);
                    if (tblTask.getTaskcode() == 1) {
                        TblRtasolicitor tblRtasolicitor = rtaService.getRtaSolicitorsOfRtaClaim(rtaCode);
                        TblCompanyprofile solicitorCompany = rtaService.getCompanyProfile(tblRtasolicitor.getCompanycode());
                        if(solicitorCompany == null){
                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "There is no Active solicitor Attach to the case", null);
                        }

                        TblCompanydoc tblCompanydoc = new TblCompanydoc();
                        if (tblRtaclaim.getVehicleType().equals("B")) {
                            tblCompanydoc = rtaService.getCompanyDocsRtaForBike(solicitorCompany.getCompanycode());
                        } else {

                            if (tblRtaclaim.getScotland().equals("Y")) { // for company scotland
                                if (tblRtaclaim.getGfirstname() == null) { // for adult
                                    tblCompanydoc = rtaService.getCompanyDocs(solicitorCompany.getCompanycode(), "A", "S");
                                } else { // for minor
                                    tblCompanydoc = rtaService.getCompanyDocs(solicitorCompany.getCompanycode(), "M", "S");
                                }
                            } else { // for company UK
                                if (tblRtaclaim.getGfirstname() == null) { // for adult
                                    tblCompanydoc = rtaService.getCompanyDocs(solicitorCompany.getCompanycode(), "A", "E");
                                } else { // for minor
                                    tblCompanydoc = rtaService.getCompanyDocs(solicitorCompany.getCompanycode(), "M", "E");
                                }
                            }
                        }

                        if (tblCompanydoc == null) {
                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "There is No CFA Attach To the Solicitor", null);
                        }


                        String emailUrl = getDataFromProperties("esign.baseurl") + "rta=" + tblRtaclaim.getRtacode();
                        TblEmailTemplate tblEmailTemplate = getEmailTemplateByType("esign");

                        String body = tblEmailTemplate.getEmailtemplate();
                        body = body.replace("[LEGAL_CLIENT_NAME]", tblRtaclaim.getFirstname() + " "
                                + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename()) + " " + tblRtaclaim.getLastname());
                        body = body.replace("[SOLICITOR_COMPANY_NAME]", solicitorCompany.getName());
                        body = body.replace("[ESIGN_URL]", emailUrl);
                        if (tblRtaclaim.getEmail() != null) {
//                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
//                            return getResponseFormat(HttpStatus.BAD_REQUEST, "No Client Email Address Found", null);
                            LOG.info("\n Email Sendpos");
                            rtaService.saveEmail(tblRtaclaim.getEmail(), body, "No Win No Fee Agreement", tblRtaclaim, tblUser);
                        }
                        LOG.info("\n Sending SMS ");
                        if (tblRtaclaim.getMobile() != null && !tblRtaclaim.getMobile().isEmpty()) {
                            LOG.info("\n SMS SEND");
                            TblEmailTemplate tblEmailTemplateSMS = getEmailTemplateByType("esign-SMS");
                            String message = tblEmailTemplateSMS.getEmailtemplate();
                            message = message.replace("[LEGAL_CLIENT_NAME]", tblRtaclaim.getFirstname() + " "
                                    + (tblRtaclaim.getMiddlename() == null ? "" : tblRtaclaim.getMiddlename() + " ") + tblRtaclaim.getLastname());
                            message = message.replace("[SOLICITOR_COMPANY_NAME]", solicitorCompany.getName());
                            message = message.replace("[ESIGN_URL]", emailUrl);


                            sendSMS(tblRtaclaim.getMobile(), message);
                            tblRtatask.setRemarks("Esign/SMS Email Sent Successfully to Client");
                        } else {
                            tblRtatask.setRemarks("Esign Email Sent Successfully to Client");
                        }

                        saveEmailSmsEsignStatus(String.valueOf(tblRtaclaim.getRtacode()), "1", tblUser.getUsercode());

                        tblRtatask.setStatus("P");
                    } else {

                        String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\rta\\";
                        String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\rta\\";
                        List<TblRtadocument> tblRtadocuments = new ArrayList<TblRtadocument>();

                        File theDir = new File(UPLOADED_FOLDER + tblRtaclaim.getRtacode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }

                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);
                            TblRtadocument tblRtadocument = new TblRtadocument();
                            tblRtadocument.setDocurl(UPLOADED_SERVER + tblRtaclaim.getRtacode() + "\\"
                                    + multipartFile.getOriginalFilename());
                            tblRtadocument.setDoctype(multipartFile.getContentType());
                            tblRtadocument.setTblRtaclaim(tblRtaclaim);
                            tblRtadocument.setTblTask(tblTask);
                            tblRtadocument.setUsercode(tblUser.getUsercode());
                            tblRtadocument.setCreatedon(new Date());

                            tblRtadocuments.add(tblRtadocument);
                        }
                        rtaService.addRtaDocument(tblRtadocuments);
                        tblRtatask.setStatus("C");
                        tblRtatask.setRemarks("Completed");

                    }
                    tblRtatask.setCompletedon(new Date());
                    tblRtatask.setCompletedby(tblUser.getUsercode());
                    int tblRtatask1 = rtaService.updateTblRtaTask(tblRtatask, Long.parseLong(rtaCode));
                    if (tblRtatask1 > 0) {

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblRtatask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addNewPassengersToRtaCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewPassengersToRtaCase(
            @RequestBody AddNewPassengersRtaCase saveRtaRequest, HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addNewPassengersToRtaCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewPassengersToRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "1");

            if (allowed) {
                List<TblRtapassenger> tblRtapassengers = new ArrayList<>();
                List<TblRtapassenger> saveRtapassengers;

                ///// ADD PASSENGERS IF ANY////////
                if (saveRtaRequest.getPassengers() != null && saveRtaRequest.getPassengers().size() > 0) {
                    TblRtapassenger tblRtapassenger = null;

                    for (RtaPassengerRequest rtaPassengerRequest : saveRtaRequest.getPassengers()) {
                        tblRtapassenger = new TblRtapassenger();
                        tblRtapassenger.setAddress1(rtaPassengerRequest.getAddress1());
                        tblRtapassenger.setAddress2(rtaPassengerRequest.getAddress2());
                        tblRtapassenger.setAddress3(rtaPassengerRequest.getAddress3());
                        tblRtapassenger.setCity(rtaPassengerRequest.getCity());
                        tblRtapassenger.setDob(rtaPassengerRequest.getDob() == null ? null
                                : formatter.parse(rtaPassengerRequest.getDob()));
                        tblRtapassenger.setDriverpassenger(rtaPassengerRequest.getDriverpassenger());
                        tblRtapassenger.setEmail(rtaPassengerRequest.getEmail());
                        tblRtapassenger.setEnglishlevel(rtaPassengerRequest.getEnglishlevel());
                        tblRtapassenger.setFirstname(rtaPassengerRequest.getFirstname());
                        tblRtapassenger.setGcity(rtaPassengerRequest.getGcity());
                        tblRtapassenger.setGaddress1(rtaPassengerRequest.getGaddress1());
                        tblRtapassenger.setGaddress2(rtaPassengerRequest.getGaddress2());
                        tblRtapassenger.setGaddress3(rtaPassengerRequest.getGaddress3());
                        tblRtapassenger.setGemail(rtaPassengerRequest.getGemail());
                        tblRtapassenger.setGfirstname(rtaPassengerRequest.getGfirstname());
                        tblRtapassenger.setGlandline(rtaPassengerRequest.getGlandline());
                        tblRtapassenger.setGlastname(rtaPassengerRequest.getGlastname());
                        tblRtapassenger.setGmiddlename(rtaPassengerRequest.getGmiddlename());
                        tblRtapassenger.setGmobile(rtaPassengerRequest.getGmobile());
                        tblRtapassenger.setGpostalcode(rtaPassengerRequest.getGpostalcode());
                        tblRtapassenger.setGregion(rtaPassengerRequest.getGregion());
                        tblRtapassenger.setGtitle(rtaPassengerRequest.getGtitle());
                        tblRtapassenger.setGdob(rtaPassengerRequest.getGdob() == null ? null
                                : formatter.parse(rtaPassengerRequest.getGdob()));
                        tblRtapassenger.setInjdescr(rtaPassengerRequest.getInjdescription());
                        tblRtapassenger.setInjlength(rtaPassengerRequest.getInjlength() == null ? null
                                : new BigDecimal(rtaPassengerRequest.getInjlength()));
                        tblRtapassenger.setLandline(rtaPassengerRequest.getLandline());
                        tblRtapassenger.setLastname(rtaPassengerRequest.getLastname());
                        tblRtapassenger.setMedicalinfo(rtaPassengerRequest.getMedicalinfo());
                        tblRtapassenger.setMiddlename(rtaPassengerRequest.getMiddlename());
                        tblRtapassenger.setMobile(rtaPassengerRequest.getMobile());
                        tblRtapassenger.setNinumber(rtaPassengerRequest.getNinumber());
                        tblRtapassenger.setOngoing(rtaPassengerRequest.getOngoing());
                        tblRtapassenger.setPostalcode(rtaPassengerRequest.getPostalcode());
                        tblRtapassenger.setRegion(rtaPassengerRequest.getRegion());
                        tblRtapassenger.setTitle(rtaPassengerRequest.getTitle());
                        tblRtapassenger.setAlternativenumber(rtaPassengerRequest.getAlternativenumber());
                        tblRtapassenger.setEvidencedetails(rtaPassengerRequest.getMedicalevidence());
                        tblRtapassenger.setDetails(rtaPassengerRequest.getDetail());

                        List<TblRtaPassengerInjury> pasengerTblRtainjuries = new ArrayList<>();
                        TblRtaPassengerInjury pasengerTblRtainjury;
                        for (String injury : rtaPassengerRequest.getInjclasscodes()) {
                            pasengerTblRtainjury = new TblRtaPassengerInjury();
                            pasengerTblRtainjury.setInjclasscode(Long.valueOf(injury));
                            pasengerTblRtainjuries.add(pasengerTblRtainjury);
                        }
                        tblRtapassenger.setPasengerTblRtainjuries(pasengerTblRtainjuries);

                        tblRtapassengers.add(tblRtapassenger);

                    }
                    TblRtaclaim rtaclaim = rtaService.getTblRtaclaimById(Long.valueOf(saveRtaRequest.getRtacode()));
                    saveRtapassengers = rtaService.addNewRtaPassengers(rtaclaim, tblRtapassengers);

                } ////// END ADD PASSENGER IF ANY //////

                List<TblRtapassenger> rtaPassengers = rtaService.getAuthRtaCasePassengers(saveRtaRequest.getRtacode());
                LOG.info("\n EXITING THIS METHOD == addNewPassengersToRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", rtaPassengers);

            } else {
                LOG.info("\n EXITING THIS METHOD == addNewPassengersToRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addNewPassengersToRtaCase();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewPassengersToRtaCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/copyRtaToHire", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> copyRtaToHire(@RequestBody CopyCaseRequest rtaCode,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == copyRtaToHire(); ");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == copyRtaToHire(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                TblHireclaim tblHireclaim = rtaService.copyRtaToHire(rtaCode.getRtacode());
                if (tblHireclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == copyRtaToHire(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case copied successfully", tblHireclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == copyRtaToHire(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Unable to copy case. Please try again", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == copyRtaToHire(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == copyRtaToHire();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == copyRtaToHire(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/setCurrentTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> setCurrentTask(@RequestBody CurrentTaskRequest currentTaskRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == setCurrentTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            int tblRtatask1 = rtaService.updateCurrentTask(currentTaskRequest.getTaskCode(),
                    currentTaskRequest.getRtaCode(), currentTaskRequest.getCurrent());
            if (tblRtatask1 > 0) {
                List<TblRtatask> rtaCaseTasks = rtaService
                        .getAuthRtaCaseTasks(String.valueOf(currentTaskRequest.getRtaCode()));

                if (rtaCaseTasks != null && rtaCaseTasks.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Current Task Updated SuccessFully", rtaCaseTasks);
                } else {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Updating Task", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == setCurrentTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/rtaCaseReport", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getRtaCaseReport(
            @RequestBody RtaCaseReportRequest rtaCaseReportRequest, HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == rtaCaseReport(); ");
        try {

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == rtaCaseReport(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<ViewRtacasereport> rtaCaseReport = rtaService.getRtaCaseReport(rtaCaseReportRequest);

                if (rtaCaseReport != null && rtaCaseReport.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getRtaCaseReport(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", rtaCaseReport);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getRtaCaseReport(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getRtaCaseReport(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == rtaCaseReport();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == rtaCaseReport(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/deleteRtaDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> deleteRtaDocument(
            @RequestBody DeleteRtaDocumentRequest deleteRtaDocumentRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == deleteRtaDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(tblUser.getCompanycode());
            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {

                rtaService.deleteRtaDocument(deleteRtaDocumentRequest.getDoccode());
                return getResponseFormat(HttpStatus.OK, "Document Deleted", null);
            } else {
                LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Action", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaPostApi \n METHOD == deleteRtaDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/copyRtaToRta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> copyRtaToRta(@RequestBody CopyRtaToRtaRequest copyRtaToRtaRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == copyRtaToRta(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == copyRtaToRta(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblRtaclaim tblRtaclaimMain = rtaService
                    .getTblRtaclaimById(Long.parseLong(copyRtaToRtaRequest.getRtacode()));
            List<TblRtadocument> tblRtadocuments = rtaService.getAuthRtaCasedocuments(copyRtaToRtaRequest.getRtacode());

            TblRtaclaim tblRtaclaim = new TblRtaclaim();
            tblRtaclaim.setAccdate(tblRtaclaimMain.getAccdate());
            tblRtaclaim.setAcctime(tblRtaclaimMain.getAcctime());
            tblRtaclaim.setAddress1(tblRtaclaimMain.getAddress1());
            tblRtaclaim.setAddress2(tblRtaclaimMain.getAddress2());
            tblRtaclaim.setAddress3(tblRtaclaimMain.getAddress3());
            tblRtaclaim.setCity(tblRtaclaimMain.getCity());
            tblRtaclaim.setContactdue(tblRtaclaimMain.getContactdue() == null ? null : tblRtaclaimMain.getContactdue());
            tblRtaclaim.setCreatedon(new Date());
            tblRtaclaim.setDescription(tblRtaclaimMain.getDescription());
            tblRtaclaim.setDob(tblRtaclaimMain.getDob());
            tblRtaclaim.setDriverpassenger(tblRtaclaimMain.getDriverpassenger());
            tblRtaclaim.setEmail(tblRtaclaimMain.getEmail());
            tblRtaclaim.setEnglishlevel(tblRtaclaimMain.getEnglishlevel());
            tblRtaclaim.setFirstname(tblRtaclaimMain.getFirstname());
            tblRtaclaim.setGreencardno(tblRtaclaimMain.getGreencardno());
            tblRtaclaim.setInjdescription(tblRtaclaimMain.getInjdescription());
            tblRtaclaim.setInsurer(tblRtaclaimMain.getInsurer());
            tblRtaclaim.setLandline(tblRtaclaimMain.getLandline());
            tblRtaclaim.setLastname(tblRtaclaimMain.getLastname());
            tblRtaclaim.setLocation(tblRtaclaimMain.getLocation());
            tblRtaclaim.setMakemodel(tblRtaclaimMain.getMakemodel());
            tblRtaclaim.setMedicalinfo(tblRtaclaimMain.getMedicalinfo());
            tblRtaclaim.setMiddlename(tblRtaclaimMain.getMiddlename());
            tblRtaclaim.setMobile(tblRtaclaimMain.getMobile());
            tblRtaclaim.setNinumber(tblRtaclaimMain.getNinumber());
            tblRtaclaim.setOngoing(tblRtaclaimMain.getOngoing());
            tblRtaclaim.setPartyaddress(tblRtaclaimMain.getPartyaddress());
            tblRtaclaim.setPartycontactno(tblRtaclaimMain.getPartycontactno());
            tblRtaclaim.setPartyinsurer(tblRtaclaimMain.getPartyinsurer());
            tblRtaclaim.setPartymakemodel(tblRtaclaimMain.getPartymakemodel());
            tblRtaclaim.setPartyname(tblRtaclaimMain.getPartyname());
            tblRtaclaim.setPartypolicyno(tblRtaclaimMain.getPartypolicyno());
            tblRtaclaim.setPartyrefno(tblRtaclaimMain.getPartyrefno());
            tblRtaclaim.setPartyregno(tblRtaclaimMain.getPartyregno());
            tblRtaclaim.setPassengerinfo(tblRtaclaimMain.getPassengerinfo());
            tblRtaclaim.setPolicyno(tblRtaclaimMain.getPolicyno());
            tblRtaclaim.setPostalcode(tblRtaclaimMain.getPostalcode());
            tblRtaclaim.setRdweathercond(tblRtaclaimMain.getRdweathercond());
            tblRtaclaim.setRefno(tblRtaclaimMain.getRefno());
            tblRtaclaim.setRegion(tblRtaclaimMain.getRegion());
            tblRtaclaim.setRegisterationno(tblRtaclaimMain.getRegisterationno());
            tblRtaclaim.setReportedtopolice(tblRtaclaimMain.getReportedtopolice());
            tblRtaclaim.setEsig("N");
            tblRtaclaim.setScotland(tblRtaclaimMain.getScotland());
            tblRtaclaim.setPassword(tblRtaclaimMain.getPassword());
            tblRtaclaim.setTranslatordetails(tblRtaclaimMain.getTranslatordetails());
            tblRtaclaim.setAlternativenumber(tblRtaclaimMain.getAlternativenumber());
            tblRtaclaim.setMedicalevidence(tblRtaclaimMain.getMedicalevidence());
            tblRtaclaim.setReportedon(tblRtaclaimMain.getReportedon() == null ? null : tblRtaclaimMain.getReportedon());
            tblRtaclaim.setReferencenumber(tblRtaclaimMain.getReferencenumber());
            tblRtaclaim.setInjlength(tblRtaclaimMain.getInjlength() == null ? null : tblRtaclaimMain.getInjlength());

            tblRtaclaim.setGaddress1(tblRtaclaimMain.getGaddress1());
            tblRtaclaim.setGaddress2(tblRtaclaimMain.getGaddress2());
            tblRtaclaim.setGaddress3(tblRtaclaimMain.getGaddress3());
            tblRtaclaim.setGcity(tblRtaclaimMain.getGcity());
            tblRtaclaim.setGemail(tblRtaclaimMain.getGemail());
            tblRtaclaim.setGfirstname(tblRtaclaimMain.getGfirstname());
            tblRtaclaim.setGlandline(tblRtaclaimMain.getGlandline());
            tblRtaclaim.setGlastname(tblRtaclaimMain.getGlastname());
            tblRtaclaim.setGmiddlename(tblRtaclaimMain.getGmiddlename());
            tblRtaclaim.setGmobile(tblRtaclaimMain.getGmobile());
            tblRtaclaim.setGpostalcode(tblRtaclaimMain.getGpostalcode());
            tblRtaclaim.setGregion(tblRtaclaimMain.getGregion());
            tblRtaclaim.setGtitle(tblRtaclaimMain.getGtitle());
            tblRtaclaim.setGdob(tblRtaclaimMain.getGdob() == null ? null : tblRtaclaimMain.getGdob());

            tblRtaclaim.setYearofmanufacture(tblRtaclaimMain.getYearofmanufacture());
            tblRtaclaim.setTitle(tblRtaclaimMain.getTitle());
            tblRtaclaim.setVehiclecondition(tblRtaclaimMain.getVehiclecondition());
            tblRtaclaim.setOtherlanguages(tblRtaclaimMain.getOtherlanguages());
            tblRtaclaim.setAirbagopened(tblRtaclaimMain.getAirbagopened());
            tblRtaclaim.setVehicleType(tblRtaclaimMain.getVehicleType());
            tblRtaclaim.setMedicalevidence(tblRtaclaimMain.getMedicalevidence());
            tblRtaclaim.setVdImages(tblRtaclaimMain.getVdImages());
            tblRtaclaim.setInjurySustained(tblRtaclaimMain.getInjurySustained());

            TblCircumstance tblCircumstance = new TblCircumstance();
            List<TblRtainjury> tblRtainjuries = new ArrayList<>();

            tblRtaclaim.setTblCircumstance(tblRtaclaimMain.getTblCircumstance());
            tblRtaclaimMain.setInjclasscodes(rtaService.getInjuriesOfRta(tblRtaclaimMain.getRtacode()));
            if (tblRtaclaimMain.getInjclasscodes() != null) {
                TblRtainjury tblRtainjury;
                for (TblRtainjury injury : tblRtaclaimMain.getInjclasscodes()) {
                    tblRtainjury = new TblRtainjury();

                    tblRtainjury.setTblInjclass(injury.getTblInjclass());
                    tblRtainjuries.add(tblRtainjury);
                }
                tblRtaclaim.setInjclasscodes(tblRtainjuries);
            } else {
                tblRtaclaim.setTblInjclass(null);
            }

            tblRtaclaim.setIntroducer(tblRtaclaimMain.getIntroducer());
            tblRtaclaim.setAdvisor(tblRtaclaimMain.getAdvisor());

            TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(String.valueOf(tblRtaclaim.getIntroducer()));

            tblRtaclaim.setRtanumber(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());
            tblRtaclaim.setStatuscode("1");

            tblRtaclaim.setUsercode(tblUser.getUsercode());


            tblRtaclaim = rtaService.addNewRtaCase(tblRtaclaim, null, null);
            tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
            tblCompanyprofile = rtaService.saveCompanyProfile(tblCompanyprofile);

            List<TblRtadocument> tblRtadocumentsCopy = new ArrayList<>();
            for (TblRtadocument tblRtadocument : tblRtadocuments) {
                TblRtadocument tblRtadocument1 = new TblRtadocument();

                tblRtadocument.setDocname(tblRtadocument.getDocname());
                tblRtadocument.setDoctype("Image");
                tblRtadocument.setDocbase64(tblRtadocument.getDocbase64());
                tblRtadocument.setTblRtaclaim(tblRtaclaim);
                tblRtadocument.setCreatedon(new Date());
                tblRtadocument.setUsercode(tblRtaclaim.getUsercode());
                tblRtadocument.setTblTask(null);

                tblRtadocumentsCopy.add(tblRtadocument1);
            }
            tblRtadocuments = rtaService.addRtaDocument(tblRtadocumentsCopy);

            if (tblRtaclaim != null) {

                copyRtaToRtaRequest.setRtacode(String.valueOf(tblRtaclaim.getRtacode()));
                return getResponseFormat(HttpStatus.OK, "Case Copied To New Case SuccessFully", copyRtaToRtaRequest);
            } else {
                LOG.info("\n EXITING THIS METHOD == copyRtaToRta(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Copying A Case", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaPostApi \n METHOD == copyRtaToRta();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == copyRtaToRta(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/filterAuthRtaCases", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> filterAuthRtaCases(@RequestBody FilterRequest filterRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == filterAuthRtaCases(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == filterAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(tblUser.getCompanycode());

            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "1");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == filterAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("1")) {

                List<ViewRtalistintorducers> viewRtalist = rtaService.getFilterAuthRtaCasesIntroducers(filterRequest, tblUser.getUsercode());
                for (ViewRtalistintorducers viewRtalistintorducers : viewRtalist) {
                    if (viewRtalistintorducers.getUsercategory() != null) {
                        if (viewRtalistintorducers.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode())) {
                            viewRtalistintorducers.setEditflag("Y");
                        } else {
                            viewRtalistintorducers.setEditflag("N");
                        }
                    } else {
                        viewRtalistintorducers.setEditflag("N");
                    }
                }

                if (viewRtalist != null && viewRtalist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == filterAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewRtalist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == filterAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }

            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("2")) {

                List<ViewRtalistsolicitors> viewRtalist = rtaService.getFilterAuthRtaCasesSolicitors(filterRequest, tblUser.getUsercode());
                for (ViewRtalistsolicitors viewRtalistsolicitors : viewRtalist) {
                    if (viewRtalistsolicitors.getUsercategory() != null) {
                        if (viewRtalistsolicitors.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode())) {
                            viewRtalistsolicitors.setEditflag("Y");
                        } else {
                            viewRtalistsolicitors.setEditflag("N");
                        }
                    } else {
                        viewRtalistsolicitors.setEditflag("N");
                    }
                }
                if (viewRtalist != null && viewRtalist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == filterAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewRtalist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == filterAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }

            } else if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {

                List<ViewRtalistintorducers> viewRtalist = rtaService.getFilterAuthRtaCasesLegalAssist(filterRequest);
                for (ViewRtalistintorducers viewRtalistintorducers : viewRtalist) {
                    if (viewRtalistintorducers.getUsercategory() != null) {
                        if (viewRtalistintorducers.getUsercategory().contains(tblCompanyprofile.getTblUsercategory().getCategorycode())) {
                            viewRtalistintorducers.setEditflag("Y");
                        } else {
                            viewRtalistintorducers.setEditflag("N");
                        }
                    } else {
                        viewRtalistintorducers.setEditflag("N");
                    }
                }
                if (viewRtalist != null && viewRtalist.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == filterAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", viewRtalist);
                } else {
                    LOG.info("\n EXITING THIS METHOD == filterAuthRtaCases(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == filterAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Company Found Against User", null);
            }


        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == filterAuthRtaCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == filterAuthRtaCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }


    @RequestMapping(value = "/sendRtaCaseOverTheEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> sendRtaCaseOverTheEmail(@RequestBody SendCaseDataRequest sendCaseDataRequest, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == sendRtaCaseOverTheEmail(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == sendRtaCaseOverTheEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = rtaService.getCompanyProfile(tblUser.getCompanycode());

            boolean allowed = rtaService.isRtaCaseAllowed(tblUser.getCompanycode(), "1");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == sendRtaCaseOverTheEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            ViewRtaclamin viewRtaclamin = rtaService.getAuthRtaCase(String.valueOf(sendCaseDataRequest.getCode()), tblUser);
            List<TblRtadocument> authRtaCasedocuments = rtaService.getAuthRtaCasedocuments(String.valueOf(sendCaseDataRequest.getCode()));
            ClaimsCNFexport claimsCNFexport = new ClaimsCNFexport();

            claimsCNFexport.getData().setSourceRef(viewRtaclamin.getRtanumber());
            claimsCNFexport.getData().setFileHandler("2");
            claimsCNFexport.getData().setCreditControl("2");
            claimsCNFexport.getData().setReferrer("2");
            claimsCNFexport.getData().getClaimantDetails().setClaimantTitle(viewRtaclamin.getTitle());
            claimsCNFexport.getData().getClaimantDetails().setClaimantForename(viewRtaclamin.getFirstname());
            claimsCNFexport.getData().getClaimantDetails().setClaimantSurname(viewRtaclamin.getLastname());
            claimsCNFexport.getData().getClaimantDetails().setClaimantAddress(viewRtaclamin.getAddress1());
            claimsCNFexport.getData().getClaimantDetails().setClaimantAddress2(viewRtaclamin.getAddress2());
            claimsCNFexport.getData().getClaimantDetails().setClaimantAddress3(viewRtaclamin.getGaddress3());
            claimsCNFexport.getData().getClaimantDetails().setClaimantTown(viewRtaclamin.getCity());
            claimsCNFexport.getData().getClaimantDetails().setClaimantPostCode(viewRtaclamin.getPostalcode());
            claimsCNFexport.getData().getClaimantDetails().setClaimantDOB(formatter.format(viewRtaclamin.getDob()));
            claimsCNFexport.getData().getClaimantDetails().setClaimantEmail(viewRtaclamin.getEmail());
            claimsCNFexport.getData().getClaimantDetails().setClaimantWas(viewRtaclamin.getDriverpassenger() == "D"?"Driver":"Passenger");
            claimsCNFexport.getData().getClaimantDetails().setClaimantNINumber(viewRtaclamin.getNinumber());
            claimsCNFexport.getData().getClaimantDetails().setClaimantHomeTel(viewRtaclamin.getLandline() == null?"":viewRtaclamin.getLandline());
            claimsCNFexport.getData().getClaimantDetails().setTotalPassenger(String.valueOf(viewRtaclamin.getPassengerRtaClaims().size()));
            claimsCNFexport.getData().getClaimantDetails().setClaimingVD(viewRtaclamin.getVdImages() == "Y"?"Yes":"No");
            claimsCNFexport.getData().getClaimantDetails().setClModel(viewRtaclamin.getMakemodel());
            claimsCNFexport.getData().getClaimantDetails().setClMake(viewRtaclamin.getMakemodel());
            claimsCNFexport.getData().getClaimantDetails().setClaimantRegNo(viewRtaclamin.getRegisterationno());
            claimsCNFexport.getData().getClaimantDetails().setInsurerName(viewRtaclamin.getInsurer());
            claimsCNFexport.getData().getClaimantDetails().setClPolicyNumber(viewRtaclamin.getPolicyno() == null?"":viewRtaclamin.getPolicyno());

            claimsCNFexport.getData().getTpDetails().setTpForename(viewRtaclamin.getPartyname());
            claimsCNFexport.getData().getTpDetails().setTpAddress(viewRtaclamin.getPartyaddress());
            claimsCNFexport.getData().getTpDetails().setTpTelMain(viewRtaclamin.getPartycontactno());
            claimsCNFexport.getData().getTpDetails().setTpMake(viewRtaclamin.getPartymakemodel());
            claimsCNFexport.getData().getTpDetails().setTpModel(viewRtaclamin.getPartymakemodel());
            claimsCNFexport.getData().getTpDetails().setTpModel(viewRtaclamin.getPartymakemodel());
            claimsCNFexport.getData().getTpDetails().setTpVRN(viewRtaclamin.getPartyregno());

            claimsCNFexport.getData().getAccidentDetails().setAccidentDate(formatter.format(viewRtaclamin.getAccdate()));
            claimsCNFexport.getData().getAccidentDetails().setAccidentTime(viewRtaclamin.getAcctime());
            claimsCNFexport.getData().getAccidentDetails().setAccidentDescription(viewRtaclamin.getDescription());
            claimsCNFexport.getData().getAccidentDetails().setLocation(viewRtaclamin.getLocation());
            claimsCNFexport.getData().getAccidentDetails().setReportedPolice(viewRtaclamin.getReportedtopolice() == "Y"?"Yes":"No");

            claimsCNFexport.getData().getInjuryDetails().setNotes(viewRtaclamin.getInjdescription());

            String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\rta\\";
            String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\rta\\";

            File file = new File(UPLOADED_FOLDER + viewRtaclamin.getRtacode()+"\\" +"thirdpatyxml"+(new Date()).getTime()+".xml");
//            if (!file.exists()) {
//                file.mkdirs();
//            }
            ObjectMapper xmlMapper = new XmlMapper();
            xmlMapper.writeValue(file, claimsCNFexport);
            LOG.info("\n XML For the case "+viewRtaclamin.getRtanumber()+" has been created successfully now sending Email\n");




            TblEmail tblEmail = new TblEmail();

            tblEmail.setEmailsubject("RTA CASE DATA FROM LEGAL ASSIST | CASE REF NO : "+viewRtaclamin.getRtanumber());
            tblEmail.setEmailaddress("alex@flynetmedia.co.uk,murtzamlk@gmail.com");
            tblEmail.setCcemailaddress("ikram@flynetmedia.co.uk,safina@legalassistltd.co.uk");
            tblEmail.setSenflag(new BigDecimal(0));
            tblEmail.setCreatedon(new Date());
            tblEmail.setEmailbody(" This is a test email for integration ");

            String attactments = file.getPath();
            if(authRtaCasedocuments != null && authRtaCasedocuments.size() > 0) {
                for (TblRtadocument tblRtadocument : authRtaCasedocuments){
                    String filePath = UPLOADED_FOLDER+(tblRtadocument.getDocurl().split("\\\\rta")[1]);
                    attactments = attactments+","+filePath;
                }
            }
            tblEmail.setEmailattachment(attactments);

            tblEmail = rtaService.saveTblEmail(tblEmail);



            LOG.info("\n EXITING THIS METHOD == sendRtaCaseOverTheEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Record Found", null);


        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("\n CLASS == RtaPostApi \n METHOD == sendRtaCaseOverTheEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == sendRtaCaseOverTheEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == sendRtaCaseOverTheEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == sendRtaCaseOverTheEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }
}
