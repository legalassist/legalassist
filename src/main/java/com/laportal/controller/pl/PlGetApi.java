package com.laportal.controller.pl;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.PlCaseList;
import com.laportal.dto.PlNotesResponse;
import com.laportal.dto.PlStatusCountList;
import com.laportal.dto.RtaAuditLogResponse;
import com.laportal.model.*;
import com.laportal.service.pl.PlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/Pl")
public class PlGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(PlGetApi.class);

    @Autowired
    private PlService plService;

    @RequestMapping(value = "/getplCases", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getplCases(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = plService.getCompanyProfile(tblUser.getCompanycode());

            List<PlCaseList> viewHdrCaseslist = plService.getAuthPlCasesUserAndCategoryWise(tblUser.getUsercode(),tblCompanyprofile.getTblUsercategory().getCategorycode());
            if (viewHdrCaseslist != null && viewHdrCaseslist.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", viewHdrCaseslist);
            } else {
                LOG.info("\n EXITING THIS METHOD == getAuthRtaCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getplCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getplCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getplCaseById/{plCaseId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrCaseById(@PathVariable long plCaseId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCaseById(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblPlclaim tblplclaim = plService.findPlCaseById(plCaseId, tblUser);

            if (tblplclaim.getTblPlsolicitors() != null) {
                TblCompanyprofile tblCompanyprofile = plService.getCompanyProfile(tblplclaim.getTblPlsolicitors().get(0).getCompanycode());
                tblplclaim.getTblPlsolicitors().get(0).setTblCompanyprofile(tblCompanyprofile);
            }

            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Record Found", tblplclaim);
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllplStatusCounts", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllplStatusCounts(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getHdrCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = plService.getCompanyProfile(tblUser.getCompanycode());

                List<PlStatusCountList> plStatusCountsForIntroducers = plService.getAllPlStatusCounts(tblCompanyprofile.getCompanycode(),"8");

                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", plStatusCountsForIntroducers);

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getAllplStatusCounts();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllplStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getplCasesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getplCasesByStatus(@PathVariable long statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getplCasesByStatus(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = plService.getCompanyProfile(tblUser.getCompanycode());
            List<PlCaseList> plCasesByStatus = plService.getPlCasesByStatus(tblUser.getUsercode(), String.valueOf(statusId), tblCompanyprofile.getTblUsercategory().getCategorycode());

            if (plCasesByStatus != null && plCasesByStatus.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getplCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", plCasesByStatus);
            } else {
                LOG.info("\n EXITING THIS METHOD == getplCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == HdrGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getplCaseLogs/{plcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getplCaseLogs(@PathVariable String plcode,
                                                                 HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getplCaseLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getplCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblPllog> plCaseLogs = plService.getPlCaseLogs(plcode);

                if (plCaseLogs != null && plCaseLogs.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getplCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", plCaseLogs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getplCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getplCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getplCaseLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getplCaseLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getplCaseNotes/{plcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getplCaseNotes(@PathVariable String plcode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getplCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getplCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = plService.isPlCaseAllowed(tblUser.getCompanycode(), "7");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getplCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            if (tblUser != null) {
                List<TblPlnote> tblElnotes = plService.getPlCaseNotes(plcode, tblUser);
                List<TblPlnote> tblElnotesLegalInternal = plService.getAuthPlCaseNotesOfLegalInternal(plcode, tblUser);

                if (tblElnotes != null && tblElnotes.size() > 0) {
                    PlNotesResponse plNotesResponse = new PlNotesResponse();
                    plNotesResponse.setTblPlnotes(tblElnotes);
                    plNotesResponse.setTblPlnotesLegalOnly(tblElnotesLegalInternal);

                    LOG.info("\n EXITING THIS METHOD == getplCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", plNotesResponse);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getplCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getplCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getplCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getplCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getPlCaseMessages/{plcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getplCaseMessages(@PathVariable String plcode,
                                                                     HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrGetApi \n METHOD == getplCaseMessages(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblPlmessage> tblplmessages = plService.getPlCaseMessages(plcode);

                if (tblplmessages != null && tblplmessages.size() > 0) {
                    for (TblPlmessage tblplmessage : tblplmessages) {
                        tblplmessage.setTblPlclaim(null);
                    }

                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblplmessages);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHdrCaseMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getPlAuditLogs/{plCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getPlAuditLogs(@PathVariable String plCode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getPlAuditLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getPlAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<RtaAuditLogResponse> elAuditLogResponses = plService.getPlAuditLogs(Long.valueOf(plCode));

                if (elAuditLogResponses != null) {
                    LOG.info("\n EXITING THIS METHOD == getPlAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", elAuditLogResponses);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getPlAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getPlAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == ElGetApi \n METHOD == getPlAuditLogs();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getPlAuditLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

}
