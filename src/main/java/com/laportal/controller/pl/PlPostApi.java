package com.laportal.controller.pl;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.pl.PlService;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.fields.PdfField;
import com.spire.pdf.widget.PdfFormFieldWidgetCollection;
import com.spire.pdf.widget.PdfFormWidget;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

@RestController
@RequestMapping("/Pl")
public class PlPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(PlPostApi.class);

    @Autowired
    private PlService plService;

    @RequestMapping(value = "/addNewPlCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewHdrCase(@RequestBody SavePlRequest savePlRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addNewPlCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewPlCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = plService.isPlCaseAllowed(tblUser.getCompanycode(), "8");

            if (allowed) {
                TblPlclaim tblPlclaim = new TblPlclaim();

                tblPlclaim.setTitle(savePlRequest.getTitle());
                tblPlclaim.setFirstname(savePlRequest.getFirstname());
                tblPlclaim.setMiddlename(savePlRequest.getMiddlename());
                tblPlclaim.setLastname(savePlRequest.getLastname());
                tblPlclaim.setPostalcode(savePlRequest.getPostalcode());
                tblPlclaim.setAddress1(savePlRequest.getAddress1());
                tblPlclaim.setAddress2(savePlRequest.getAddress2());
                tblPlclaim.setAddress3(savePlRequest.getAddress3());
                tblPlclaim.setCity(savePlRequest.getCity());
                tblPlclaim.setRegion(savePlRequest.getRegion());

                tblPlclaim.setAccdatetime(savePlRequest.getAccdatetime());
                tblPlclaim.setAccdescription(savePlRequest.getAccdescription());
                tblPlclaim.setAcclocation(savePlRequest.getAcclocation());
                tblPlclaim.setAccreportedto(savePlRequest.getAccreportedto());
                tblPlclaim.setDescribeinjuries(savePlRequest.getDescribeinjuries());
                tblPlclaim.setInjuriesduration(savePlRequest.getInjuriesduration());
                tblPlclaim.setLandline(savePlRequest.getLandline());
                tblPlclaim.setMedicalattention(savePlRequest.getMedicalattention());
                tblPlclaim.setMobile(savePlRequest.getMobile());
                tblPlclaim.setNinumber(savePlRequest.getNinumber());
                tblPlclaim.setOccupation(savePlRequest.getOccupation());
                tblPlclaim.setPassword(savePlRequest.getPassword());
                tblPlclaim.setWitnesses(savePlRequest.getWitnesses());
                tblPlclaim.setDob(savePlRequest.getDob());
                tblPlclaim.setEmail(savePlRequest.getEmail());
                tblPlclaim.setRemarks(savePlRequest.getRemarks());
                tblPlclaim.setStatus(new BigDecimal(724));
                tblPlclaim.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                tblPlclaim.setCreatedate(new Date());
                tblPlclaim.setAcctime(savePlRequest.getAcctime());
                tblPlclaim.setAdvisor(savePlRequest.getAdvisor());
                tblPlclaim.setIntroducer(savePlRequest.getIntroducer());
                tblPlclaim.setEsig("N");

                TblCompanyprofile tblCompanyprofile = plService.getCompanyProfile(tblUser.getCompanycode());
                tblPlclaim.setPlcode(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());

                tblPlclaim = plService.savePlRequest(tblPlclaim);

                if (tblPlclaim != null) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = plService.saveCompanyProfile(tblCompanyprofile);

                    LOG.info("\n EXITING THIS METHOD == addNewPlCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblPlclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewPlCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewPlCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addNewPlCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewPlCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/updatePlCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updatePlCase(@RequestBody UpdatePlRequest updatePlRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == updatePlCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == updatePlCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = plService.isPlCaseAllowed(tblUser.getCompanycode(), "8");

            if (allowed) {
                TblPlclaim tblPlclaim = plService.findPlCaseByIdWithoutUser(updatePlRequest.getPlclaimcode());

                tblPlclaim.setTitle(updatePlRequest.getTitle());
                tblPlclaim.setFirstname(updatePlRequest.getFirstname());
                tblPlclaim.setMiddlename(updatePlRequest.getMiddlename());
                tblPlclaim.setLastname(updatePlRequest.getLastname());
                tblPlclaim.setPostalcode(updatePlRequest.getPostalcode());
                tblPlclaim.setAddress1(updatePlRequest.getAddress1());
                tblPlclaim.setAddress2(updatePlRequest.getAddress2());
                tblPlclaim.setAddress3(updatePlRequest.getAddress3());
                tblPlclaim.setCity(updatePlRequest.getCity());
                tblPlclaim.setRegion(updatePlRequest.getRegion());
                tblPlclaim.setAcctime(updatePlRequest.getAcctime());
                tblPlclaim.setAccdatetime(updatePlRequest.getAccdatetime());
                tblPlclaim.setAccdescription(updatePlRequest.getAccdescription());
                tblPlclaim.setAcclocation(updatePlRequest.getAcclocation());
                tblPlclaim.setAccreportedto(updatePlRequest.getAccreportedto());
                tblPlclaim.setDescribeinjuries(updatePlRequest.getDescribeinjuries());
                tblPlclaim.setInjuriesduration(updatePlRequest.getInjuriesduration());
                tblPlclaim.setLandline(updatePlRequest.getLandline());
                tblPlclaim.setMedicalattention(updatePlRequest.getMedicalattention());
                tblPlclaim.setMobile(updatePlRequest.getMobile());
                tblPlclaim.setNinumber(updatePlRequest.getNinumber());
                tblPlclaim.setOccupation(updatePlRequest.getOccupation());
                tblPlclaim.setPassword(updatePlRequest.getPassword());
                tblPlclaim.setWitnesses(updatePlRequest.getWitnesses());
                tblPlclaim.setDob(updatePlRequest.getDob());
                tblPlclaim.setEmail(updatePlRequest.getEmail());
                tblPlclaim.setRemarks(updatePlRequest.getRemarks());
                tblPlclaim.setUpdatedate(new Date());
                tblPlclaim.setLastupdateuser(tblUser.getUsercode());

                tblPlclaim = plService.updatePlClaim(tblPlclaim);

                if (tblPlclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == updatePlCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Updated SuccessFully", tblPlclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == updatePlCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == updatePlCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == updatePlCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == updatePlCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addPlNotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addPlNotes(@RequestBody PlNoteRequest PlNoteRequest,
                                                              HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addPlNotes(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = plService.isPlCaseAllowed(tblUser.getCompanycode(), "8");

            if (allowed) {
                if (!PlNoteRequest.getPlCode().isEmpty()) {
                    TblPlnote tblPlnote = new TblPlnote();
                    TblPlclaim tblPlclaim = new TblPlclaim();

                    tblPlclaim.setPlclaimcode(Long.valueOf(PlNoteRequest.getPlCode()));
                    tblPlnote.setTblPlclaim(tblPlclaim);
                    tblPlnote.setNote(PlNoteRequest.getNote());
                    tblPlnote.setUsercategorycode(PlNoteRequest.getUserCatCode());
                    tblPlnote.setCreatedon(new Date());
                    tblPlnote.setUsercode(tblUser.getUsercode());

                    tblPlnote = plService.addTblPlNote(tblPlnote);

                    if (tblPlnote != null && tblPlnote.getPlnotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblPlnote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addHdrNotes();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addHdrNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/resendPlEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> resendPlEmail(@RequestBody ResendPlMessageDto resendPlMessageDto,
                                                                 HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == resendRtaEmail(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblEmail tblEmail = plService.resendEmail(resendPlMessageDto.getPlmessagecode());
            if (tblEmail != null && tblEmail.getEmailcode() > 0) {

                LOG.info("\n EXITING THIS METHOD == resendElEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Success", tblEmail);
            } else {
                LOG.info("\n EXITING THIS METHOD == resendElEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While resending Email", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == resendRtaEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnPlFromDirectIntro", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnPlFromDirectIntro(
            @RequestBody PerformHotKeyOnRtaRequest assignPlCasetoSolicitor, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == performActionOnPlFromDirectIntro(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnHdrByLegalAssist(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblPlclaim tblPlclaim = plService.performActionOnPlFromDirectIntro(assignPlCasetoSolicitor, tblUser);
                if (tblPlclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == performActionOnPlFromDirectIntro(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblPlclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnPlFromDirectIntro(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }


            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnPlFromDirectIntro(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == performActionOnPlFromDirectIntro();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnPlFromDirectIntro(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnPl", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnPl(
            @RequestBody PerformActionOnPlRequest performActionOnPlRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performActionOnPl(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnPl(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblPlclaim tblOlclaim = plService.findPlCaseByIdWithoutUser(Long.parseLong(performActionOnPlRequest.getPlCode()));


            if (performActionOnPlRequest.getToStatus().equals("730")) {
                List<TblPltask> tblOltasks = plService
                        .getPlTaskAgainstPLCodeAndStatus(performActionOnPlRequest.getPlCode(), "N");
                List<TblPltask> tblOltasks1 = plService
                        .getPlTaskAgainstPLCodeAndStatus(performActionOnPlRequest.getPlCode(), "P");

                if (tblOltasks != null && tblOltasks.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == performActionOnPl(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Some Task Are Not Performed", tblOlclaim);
                } else if (tblOltasks1 != null && tblOltasks1.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == performActionOnPl(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Some Task Are In Pending State", tblOlclaim);
                } else {

                }
            }


            if (tblUser != null) {
                if ((performActionOnPlRequest.getToStatus().equals("734")) ||
                        (performActionOnPlRequest.getToStatus().equals("774")) ||
                        (performActionOnPlRequest.getToStatus().equals("775")) ||
                        (performActionOnPlRequest.getToStatus().equals("776"))) {
                    // reject status
                    tblOlclaim = plService.performRejectCancelActionOnPl(performActionOnPlRequest.getPlCode(),
                            performActionOnPlRequest.getToStatus(), performActionOnPlRequest.getReason(), tblUser);

                } else if (performActionOnPlRequest.getToStatus().equals("737")) {
                    //cancel status
                    tblOlclaim = plService.performRejectCancelActionOnPl(performActionOnPlRequest.getPlCode(),
                            performActionOnPlRequest.getToStatus(), performActionOnPlRequest.getReason(), tblUser);

                } else if (performActionOnPlRequest.getToStatus().equals("747")) {
                    // revert status
                    tblOlclaim = plService.performRevertActionOnPl(performActionOnPlRequest.getPlCode(),
                            performActionOnPlRequest.getToStatus(), performActionOnPlRequest.getReason(), tblUser);

                } else {
                    tblOlclaim = plService.performActionOnPl(performActionOnPlRequest.getPlCode(),
                            performActionOnPlRequest.getToStatus(), tblUser);
                }

                if (tblOlclaim != null) {
//                    TblOlclaim olclaim = plService
//                            .findOlCaseById(Long.valueOf(performActionOnOlRequest.getOlCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblOlclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performActionOnHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == HdrPostApi \n METHOD == addESign(); ");
        try {
            Locale backup = Locale.getDefault();
            Locale.setDefault(Locale.ENGLISH);
            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("code")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }


            TblPlclaim tblPlclaim = plService.findPlCaseByIdWithoutUser(addESign.getRtaCode());
            if (tblPlclaim != null && tblPlclaim.getPlclaimcode() > 0) {

                if (tblPlclaim.getEsig().equals("Y")) {
                    List<TblPldocument> tblPldocuments = plService.getAuthPlCasedocuments(tblPlclaim.getPlclaimcode());
                    for (TblPldocument tblPldocument : tblPldocuments) {
                        if (tblPldocument.getDoctype().equalsIgnoreCase("Esign")) {
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("signedDocument", tblPldocument.getDocbase64());
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.OK, "You Have Already Signed The Document",
                                    map);
                        }
                    }

                }
                // solicitor company
                TblCompanyprofile tblCompanyprofile = plService
                        .getCompanyProfileAgainstPlCode(tblPlclaim.getPlclaimcode());
                if (tblCompanyprofile != null) {

                    TblCompanydoc tblCompanydoc = plService.getCompanyDocs(tblCompanyprofile.getCompanycode());
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                    byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                    ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                    BufferedImage image1 = ImageIO.read(bis);
                    bis.close();


                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();

                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            while (keys.hasNext()) {
                                String key = keys.next();
                                if (!key.equals("code") && !key.equals("eSign")) {
                                    if (fieldName.equals(key)) {
                                        textField.setValue(jsonObject.getString(key));
                                    }
                                }
                            }

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = plService.getPlDbColumnValue(fieldName.replace("DB_", ""),
                                        tblPlclaim.getPlclaimcode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            }
                            if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    for (int a = 0; a < document.getNumberOfPages(); a++) {
                        PDPage p = document.getPage(a);
                        PDResources resources = p.getResources();
                        for (COSName xObjectName : resources.getXObjectNames()) {
                            PDXObject xObject = resources.getXObject(xObjectName);
                            if (xObject instanceof PDImageXObject) {
                                PDImageXObject original_img = ((PDImageXObject) xObject);
                                PDImageXObject pdImageXObject = LosslessFactory.createFromImage(document, image1);
                                resources.put(xObjectName, pdImageXObject);
                            }
                        }
                    }

                    LOG.info("\n Esign Document Prepared SuccessFully\n");
                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Pl\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Pl\\";

                    File theDir = new File(UPLOADED_FOLDER + tblPlclaim.getPlclaimcode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblPlclaim.getPlcode() + "_Signed.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblPlclaim.getPlcode() + "_Signed.pdf");
                    }

                    tblPlclaim.setEsig("Y");
                    tblPlclaim.setEsigdate(new Date());
                    tblPlclaim = plService.updatePlClaim(tblPlclaim);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    LOG.info("\n Esign Saved on folder SuccessFully SuccessFully\n");
                    File file = new File(theDir.getPath() + "\\" + tblPlclaim.getPlcode() + "_Signed.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    TblTask tblPltaskdocument = new TblTask();
                    tblPltaskdocument.setTaskcode(30);

                    TblPldocument tblPldocument = new TblPldocument();
                    tblPldocument.setTblPlclaim(tblPlclaim);
                    tblPldocument.setDocbase64(encoded);
                    tblPldocument.setDocname("Esign");
                    tblPldocument.setDoctype("Esign");
                    tblPldocument.setCreateddate(new Date());
                    tblPldocument.setCreateuser("1");
                    tblPldocument.setDocurl(
                            UPLOADED_SERVER + tblPlclaim.getPlclaimcode() + "\\" + tblPlclaim.getPlcode() + "_Signed.pdf");
                    tblPldocument.setTblTask(tblPltaskdocument);

                    tblPldocument = plService.addPlSingleDocumentSingle(tblPldocument);
                    LOG.info("\n saving doc in database\n");
                    if (tblPldocument != null && tblPldocument.getPldocumentscode() > 0) {
//                    Locale.setDefault(backup);

                        TblPltask tblPltask = new TblPltask();
                        tblPltask.setPltaskcode(30);
                        tblPltask.setTblPlclaim(tblPlclaim);
                        tblPltask.setStatus("C");
                        tblPltask.setRemarks("Completed");
                        tblPltask.setCompletedon(new Date());

                        int tblRtatask1 = plService.updateTblPlTask(tblPltask);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("doc", encoded);
                        TblUser legalUser = plService.findByUserId("2");

                        // cleint Email
                        TblEmailTemplate tblEmailTemplate = plService.findByEmailType("esign-clnt-complete");
                        String clientbody = tblEmailTemplate.getEmailtemplate();

                        clientbody = clientbody.replace("[LEGAL_CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                                tblElclaim.getFirstname() + " "
////                                        + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
////                                        + tblElclaim.getLastname());
                        clientbody = clientbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());

                        plService.saveEmail(tblPlclaim.getEmail(), clientbody, "No Win No Fee Agreement ", tblPlclaim, legalUser, file.getPath());
                        LOG.info("\n Email Sent To client\n");
                        // Introducer Email
                        tblEmailTemplate = plService.findByEmailType("esign-Int-sol-complete");
                        String Introducerbody = tblEmailTemplate.getEmailtemplate();
                        TblUser introducerUser = plService.findByUserId(String.valueOf(tblPlclaim.getAdvisor()));

                        Introducerbody = Introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
                        Introducerbody = Introducerbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
                        Introducerbody = Introducerbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                                tblElclaim.getFirstname() + " "
//                                        + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                                        + tblElclaim.getLastname());
                        Introducerbody = Introducerbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        Introducerbody = Introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblPlclaim.getPlclaimcode());

                        plService.saveEmail(introducerUser.getUsername(), Introducerbody, "Esign", tblPlclaim, introducerUser);
                        LOG.info("\n Email Sent to intro\n");
                        // PI department Email
                        String LegalPideptbody = tblEmailTemplate.getEmailtemplate();

                        LegalPideptbody = LegalPideptbody.replace("[USER_NAME]", legalUser.getLoginid());
                        LegalPideptbody = LegalPideptbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
                        LegalPideptbody = LegalPideptbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        LegalPideptbody = LegalPideptbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                                tblElclaim.getFirstname() + " "
//                                        + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                                        + tblElclaim.getLastname());
                        LegalPideptbody = LegalPideptbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblPlclaim.getPlclaimcode());

                        plService.saveEmail(legalUser.getUsername(), LegalPideptbody, "Esign", tblPlclaim, legalUser);


                        saveEsignSubmitStatus(String.valueOf(tblPlclaim.getPlclaimcode()), "8", request);

                        LOG.info("\n Email Sent to Dept\n");
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "You Have SuccessFully Signed The CFA, A Copy Of That CFA Has Been Sent To The Provided Email.", map);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST,
                                "Error While Saving The Document..Esign Compeleted", null);
                    }

                } else {
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No document template found against provided RTA",
                            null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == addESign(); ");
        try {
            TblPlclaim tblPlclaim = plService.findPlCaseByIdWithoutUser(Long.valueOf(eSignFieldsRequest.getCode()));

            if (tblPlclaim != null && tblPlclaim.getPlclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = plService
                        .getCompanyProfileAgainstPlCode(tblPlclaim.getPlclaimcode());
                if (!tblPlclaim.getEsig().equals("Y")) {

                    TblCompanydoc tblCompanydoc = plService.getCompanyDocs(tblCompanyprofile.getCompanycode());
                    if (tblCompanydoc == null) {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "No CFA document found", null);
                    }
                    HashMap<String, String> fields = new HashMap<>();
                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();


                    /////////////////////// FILL the CFA With Client DATA ////////////////////////


                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = plService.getPlDbColumnValue(fieldName.replace("DB_", ""),
                                        tblPlclaim.getPlclaimcode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            } else if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            } else {
                                fields.put(fieldName, fieldName);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    /////////////////////// END OF FILL the CFA With Client DATA /////////////////

                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Ol\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Ol\\";

                    File theDir = new File(UPLOADED_FOLDER + tblPlclaim.getPlclaimcode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblPlclaim.getPlcode() + "_view.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblPlclaim.getPlcode() + "_view.pdf");
                    }

                    File file = new File(theDir.getPath() + "\\" + tblPlclaim.getPlcode() + "_view.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    fields.put("doc", encoded);
                    fields.put("isEsign", "N");

                    saveEsignOpenStatus(String.valueOf(tblPlclaim.getPlclaimcode()), "8", request);

                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", fields);
                } else {

                    List<TblPldocument> tblPldocuments = plService.getAuthPlCasedocuments(tblPlclaim.getPlclaimcode());
                    if (tblPldocuments != null && tblPldocuments.size() > 0) {
                        for (TblPldocument tblPldocument : tblPldocuments) {
                            if (tblPldocument.getTblTask() != null && tblPldocument.getTblTask().getTaskcode() == 1) {
                                HashMap<String, String> fields = new HashMap<>();
                                fields.put("doc", tblPldocument.getDocbase64());
                                fields.put("isEsign", "Y");
                                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                                return getResponseFormat(HttpStatus.OK, "You have Already Signed The Document.", fields);
                            }
                        }
                    }

                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Signed Document Found Please Contact Our HelpLine : please call us at 01615374448", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No EL document found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == HdrPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("plClaimCode") long plClaimCode, @RequestParam("taskCode") long taskCode,
                                                               @RequestParam(value = "multipartFiles",required = false) List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }



            TblPlclaim tblPlclaim = plService.findPlCaseByIdWithoutUser(plClaimCode);
            TblPlsolicitor tblPlsolicitor = plService.getPLSolicitorsOfPlclaim(plClaimCode);
            TblCompanyprofile solicitorCompany = plService.getCompanyProfile(tblPlsolicitor.getCompanycode());
            if (tblPlclaim != null && tblPlclaim.getPlclaimcode() > 0) {
                TblTask tblTask = plService.getTaskAgainstCode(taskCode);
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblPltask tblPltask = new TblPltask();
                    tblPltask.setPltaskcode(taskCode);
                    tblPltask.setTblPlclaim(tblPlclaim);
                    if (tblTask.getTaskcode() == 30) {
                        TblCompanydoc tblCompanydoc = plService.getCompanyDocs(solicitorCompany.getCompanycode());
                        if (tblCompanydoc == null) {
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "There is No CFA Attach To the Solicitor", null);
                        }
                        String emailUrl = getDataFromProperties("esign.baseurl") + "Pl=" + tblPlclaim.getPlclaimcode();
                        TblEmailTemplate tblEmailTemplate = getEmailTemplateByType("esign");

                        String body = tblEmailTemplate.getEmailtemplate();
                        body = body.replace("[LEGAL_CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
                        body = body.replace("[SOLICITOR_COMPANY_NAME]", solicitorCompany.getName());
                        body = body.replace("[ESIGN_URL]", emailUrl);
                        if (tblPlclaim.getEmail() != null) {
//                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
//                            return getResponseFormat(HttpStatus.BAD_REQUEST, "No Client Email Address Found", null);
                            LOG.info("\n Email Sendpos");
                            plService.saveEmail(tblPlclaim.getEmail(), body, "No Win No Fee Agreement", tblPlclaim, tblUser);
                        }
                        LOG.info("\n Sending SMS ");
                        if (tblPlclaim.getMobile() != null && !tblPlclaim.getMobile().isEmpty()) {
                            LOG.info("\n SMS SEND");
                            TblEmailTemplate tblEmailTemplateSMS = getEmailTemplateByType("esign-SMS");
                            String message = tblEmailTemplateSMS.getEmailtemplate();
                            message = message.replace("[LEGAL_CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
                            message = message.replace("[SOLICITOR_COMPANY_NAME]", solicitorCompany.getName());
                            message = message.replace("[ESIGN_URL]", emailUrl);


                            sendSMS(tblPlclaim.getMobile(), message);
                            tblPltask.setRemarks("Esign/SMS Email Sent Successfully to Client");
                        } else {
                            tblPltask.setRemarks("Esign Email Sent Successfully to Client");
                        }

                        saveEmailSmsEsignStatus(String.valueOf(tblPlclaim.getPlclaimcode()), "8", tblUser.getUsercode());

                        tblPltask.setStatus("P");
                    } else {

                        String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Pl\\";
                        String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Pl\\";
                        List<TblPldocument> tblPldocuments = new ArrayList<TblPldocument>();

                        File theDir = new File(UPLOADED_FOLDER + tblPlclaim.getPlclaimcode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }

                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);
                            TblPldocument tblPldocument = new TblPldocument();
                            tblPldocument.setDocurl(UPLOADED_SERVER + tblPlclaim.getPlclaimcode() + "\\"
                                    + multipartFile.getOriginalFilename());
                            tblPldocument.setDocumentPath(tblPldocument.getDocurl());
                            tblPldocument.setDoctype(multipartFile.getContentType());
                            tblPldocument.setTblPlclaim(tblPlclaim);
                            tblPldocument.setTblTask(tblTask);
                            tblPldocument.setCreateuser(tblUser.getUsercode());
                            tblPldocument.setCreateddate(new Date());

                            tblPldocuments.add(tblPldocument);
                        }
                        plService.addPlDocument(tblPldocuments);
                        tblPltask.setStatus("C");
                        tblPltask.setRemarks("Completed");

                    }
                    tblPltask.setCompletedon(new Date());
                    tblPltask.setCompletedby(tblUser.getUsercode());
                    int tblRtatask1 = plService.updateTblPlTask(tblPltask);
                    if (tblRtatask1 > 0) {

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblRtatask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }


        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/addPlDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> addPlDocument(@RequestParam("plClaimCode") String plClaimCode,
                                                                 @RequestParam("multipartFiles") List<MultipartFile> multipartFiles, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElPostApi \n METHOD == addPlDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addPlDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblPlclaim tblPlclaim = plService.findPlCaseByIdWithoutUser(Long.valueOf(plClaimCode));
            if (tblPlclaim != null && tblPlclaim.getPlclaimcode() > 0) {
//                String UPLOADED_FOLDER = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0_Tomcat9-la\\webapps\\El\\";
//                String UPLOADED_SERVER = "http:\\115.186.147.30:8080\\El\\";
                String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Pl\\";
                String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Pl\\";
                List<TblPldocument> tblPldocuments = new ArrayList<TblPldocument>();

                File theDir = new File(UPLOADED_FOLDER + tblPlclaim.getPlclaimcode());
                if (!theDir.exists()) {
                    theDir.mkdirs();
                }

                for (MultipartFile multipartFile : multipartFiles) {
                    byte[] bytes = multipartFile.getBytes();
                    Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                    Files.write(path, bytes);
                    TblPldocument tblPldocument = new TblPldocument();
                    tblPldocument.setDocurl(
                            UPLOADED_SERVER + tblPlclaim.getPlclaimcode() + "\\" + multipartFile.getOriginalFilename());
                    tblPldocument.setDocumentPath(tblPldocument.getDocurl());
                    tblPldocument.setDoctype(multipartFile.getContentType());
                    tblPldocument.setTblPlclaim(tblPlclaim);
                    tblPldocument.setCreateuser(tblUser.getUsercode());
                    tblPldocument.setCreateddate(new Date());

                    tblPldocuments.add(tblPldocument);
                }

                tblPldocuments = plService.addPlDocument(tblPldocuments);

                // Legal internal Email
                TblEmailTemplate tblEmailTemplate = plService.findByEmailType("document-add");
                String legalbody = tblEmailTemplate.getEmailtemplate();
                TblUser legalUser = plService.findByUserId("2");

                legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
                legalbody = legalbody.replace("[CASE_NUMBER]", tblPlclaim.getPlcode());
                legalbody = legalbody.replace("[CLIENT_NAME]", tblPlclaim.getFirstname() +(tblPlclaim.getMiddlename() != null?tblPlclaim.getMiddlename():"") +" "+tblPlclaim.getLastname());
//                        tblElclaim.getFirstname() + " "
//                                + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                                + tblElclaim.getLastname());
                legalbody = legalbody.replace("[COUNT]", String.valueOf(tblPldocuments.size()));
                legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.El") + tblPlclaim.getPlclaimcode());

                plService.saveEmail(legalUser.getUsername(), legalbody,
                        tblPlclaim.getPlcode() + " | Processing Note", tblPlclaim, legalUser);

                LOG.info("\n EXITING THIS METHOD == addPlDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Document Saved Successfully", null);
            } else {
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Find", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == ElPostApi \n METHOD == addPlDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addPlDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/setCurrentTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> setCurrentTask(@RequestBody CurrentTaskRequest currentTaskRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == PlPostApi \n METHOD == setCurrentTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            int tblRtatask1 = plService.updateCurrentTask(currentTaskRequest.getTaskCode(),
                    currentTaskRequest.getOlCode(), currentTaskRequest.getCurrent());
            if (tblRtatask1 > 0) {
                List<TblPltask> tblPltaskList = plService.findTblPlTaskByPlClaimCode(currentTaskRequest.getPlCode());

                if (tblPltaskList != null && tblPltaskList.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Current Task Updated SuccessFully", tblPltaskList);
                } else {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Updating Task", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == PlPostApi \n METHOD == setCurrentTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/deleteElDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> deleteRtaDocument(
            @RequestBody DeleteRtaDocumentRequest deleteRtaDocumentRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == deleteRtaDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = plService.getCompanyProfile(tblUser.getCompanycode());
            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {

                plService.deletePlDocument(deleteRtaDocumentRequest.getDoccode());
                return getResponseFormat(HttpStatus.OK, "Document Deleted", null);
            } else {
                LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Action", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaPostApi \n METHOD == deleteRtaDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
