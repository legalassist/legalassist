package com.laportal.controller.ol;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.OlCaseList;
import com.laportal.dto.OlNotesResponse;
import com.laportal.dto.OlStatusCountList;
import com.laportal.dto.RtaAuditLogResponse;
import com.laportal.model.*;
import com.laportal.service.ol.OlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/Ol")
public class OlGetApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(OlGetApi.class);

    @Autowired
    private OlService olService;

    @RequestMapping(value = "/getOlCases", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getOlCases(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlGetApi \n METHOD == getOlCases(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getOlCases(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = olService.getCompanyProfile(tblUser.getCompanycode());

            List<OlCaseList> olCaseLists = olService.getAuthOlCasesUserAndCategoryWise(tblUser.getUsercode(), tblCompanyprofile.getTblUsercategory().getCategorycode());
            if (olCaseLists != null && olCaseLists.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getOlCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", olCaseLists);
            } else {
                LOG.info("\n EXITING THIS METHOD == getOlCases(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, " No Record Found", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == OlGetApi \n METHOD == getOlCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getOlCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getOlCaseById/{OlCaseId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getHdrCaseById(@PathVariable long OlCaseId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getOlCaseById(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getOlCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = olService.isOlCaseAllowed(tblUser.getCompanycode(), "7");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getOlCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            TblOlclaim tblOlclaim = olService.findOlCaseById(OlCaseId, tblUser);
            if (tblOlclaim != null) {

                LOG.info("\n EXITING THIS METHOD == getOlCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", tblOlclaim);
            } else {
                LOG.info("\n EXITING THIS METHOD == getOlCaseById(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Error While Fetching Case Info. Some Info is missing", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == OlGetApi \n METHOD == getOlCaseById();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getOlCaseById(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getAllOlStatusCounts", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getAllOlStatusCounts(HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlGetApi \n METHOD == getAllOlStatusCounts(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getAllOlStatusCounts(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblCompanyprofile tblCompanyprofile = olService.getCompanyProfile(tblUser.getCompanycode());

            List<OlStatusCountList> OlStatusCountsForIntroducers = olService.getAllOlStatusCounts(tblCompanyprofile.getCompanycode(),"7");

            LOG.info("\n EXITING THIS METHOD == getAllOlStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.OK, "Record Found", OlStatusCountsForIntroducers);

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == OlGetApi \n METHOD == getAllOlStatusCounts();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getAllOlStatusCounts(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getOlCasesByStatus/{statusId}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getOlCasesByStatus(@PathVariable long statusId, HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlGetApi \n METHOD == getOlCasesByStatus(); ");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = olService.getCompanyProfile(tblUser.getCompanycode());
            List<OlCaseList> OlCasesByStatus = olService.getOlCasesByStatus(tblUser.getUsercode(), String.valueOf(statusId), tblCompanyprofile.getTblUsercategory().getCategorycode());

            if (OlCasesByStatus != null && OlCasesByStatus.size() > 0) {
                LOG.info("\n EXITING THIS METHOD == getOlCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Record Found", OlCasesByStatus);
            } else {
                LOG.info("\n EXITING THIS METHOD == getOlCasesByStatus(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == OlGetApi \n METHOD == getHdrCases();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getOlCaseLogs/{Olcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getOlCaseLogs(@PathVariable String Olcode,
                                                                 HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlGetApi \n METHOD == getOlCaseLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getOlCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblOllog> OlCaseLogs = olService.getOlCaseLogs(Olcode);

                if (OlCaseLogs != null && OlCaseLogs.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getOlCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", OlCaseLogs);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getOlCaseLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getOlCaseLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaGetApi \n METHOD == getOlCaseLogs();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getOlCaseLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getOlCaseNotes/{Olcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getOlCaseNotes(@PathVariable String Olcode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlGetApi \n METHOD == getOlCaseNotes(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getOlCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = olService.isOlCaseAllowed(tblUser.getCompanycode(), "7");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getHdrCases(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            if (tblUser != null) {
                List<TblOlnote> tblElnotes = olService.getOlCaseNotes(Olcode, tblUser);
                List<TblOlnote> tblElnotesLegalInternal = olService.getAuthOlCaseNotesOfLegalInternal(Olcode, tblUser);

                if (tblElnotes != null && tblElnotes.size() > 0) {
                    OlNotesResponse olNotesResponse = new OlNotesResponse();
                    olNotesResponse.setTblOlnotes(tblElnotes);
                    olNotesResponse.setTblOlnotesLegalOnly(tblElnotesLegalInternal);

                    LOG.info("\n EXITING THIS METHOD == getOlCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", olNotesResponse);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getOlCaseNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getOlCaseNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getOlCaseNotes();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getOlCaseNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getOlCaseMessages/{olcode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getOlCaseMessages(@PathVariable String olcode,
                                                                     HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlGetApi \n METHOD == getOlCaseMessages(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblOlmessage> tblOlmessages = olService.getOlCaseMessages(olcode);

                if (tblOlmessages != null && tblOlmessages.size() > 0) {
                    for (TblOlmessage tblOlmessage : tblOlmessages) {
                        tblOlmessage.setTblOlclaim(null);
                    }

                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblOlmessages);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaGetApi \n METHOD == getHdrCaseMessages();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getHdrCaseMessages(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }


    @RequestMapping(value = "/getOlCaseTasks/{olCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getOlCaseTasks(@PathVariable String olCode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getOlCaseTasks(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getOlCaseTasks(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<TblOltask> tblOltasks = olService.findTblOlTaskByOlClaimCode(Long.parseLong(olCode));

                if (tblOltasks != null && tblOltasks.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == getOlCaseTasks(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblOltasks);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getOlCaseTasks(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getOlCaseTasks(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == ElGetApi \n METHOD == getOlCaseTasks();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getOlCaseTasks(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }

    @RequestMapping(value = "/getOlCasedocuments/{olCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getOlCasedocuments(@PathVariable String olCode,
                                                                      HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getOlCasedocuments(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getOlCasedocuments(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = olService.isOlCaseAllowed(tblUser.getCompanycode(), "7");
            if (!allowed) {
                LOG.info("\n EXITING THIS METHOD == getOlCasedocuments(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction", null);
            }

            if (tblUser != null) {
                List<TblOldocument> tblOldocuments = olService.getAuthOlCasedocuments(Long.parseLong(olCode));

                if (tblOldocuments != null) {
                    LOG.info("\n EXITING THIS METHOD == getOlCasedocuments(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", tblOldocuments);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getOlCasedocuments(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getOlCasedocuments(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == ElGetApi \n METHOD == getOlCasedocuments();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getOlCasedocuments(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }


    @RequestMapping(value = "/getOlAuditLogs/{olCode}", method = RequestMethod.GET)
    public ResponseEntity<HashMap<String, Object>> getOlAuditLogs(@PathVariable String olCode,
                                                                  HttpServletRequest request) {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElGetApi \n METHOD == getOlAuditLogs(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == getOlAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            if (tblUser != null) {
                List<RtaAuditLogResponse> elAuditLogResponses = olService.getOlAuditLogs(Long.valueOf(olCode));

                if (elAuditLogResponses != null) {
                    LOG.info("\n EXITING THIS METHOD == getOlAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Record Found", elAuditLogResponses);
                } else {
                    LOG.info("\n EXITING THIS METHOD == getOlAuditLogs(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getOlAuditLogs(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not LoggedIN", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == ElGetApi \n METHOD == getOlAuditLogs();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == getOlAuditLogs(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }

    }
}
