package com.laportal.controller.ol;

import com.laportal.controller.abstracts.AbstractApi;
import com.laportal.dto.*;
import com.laportal.model.*;
import com.laportal.service.ol.OlService;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

@RestController
@RequestMapping("/Ol")
public class OlPostApi extends AbstractApi {

    Logger LOG = LoggerFactory.getLogger(OlPostApi.class);

    @Autowired
    private OlService olService;

    @RequestMapping(value = "/addNewOlCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addNewOlCase(@RequestBody SaveOlRequest saveOlRequest,
                                                                 HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlPostApi \n METHOD == addNewOlCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewOlCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = olService.isOlCaseAllowed(tblUser.getCompanycode(), "7");

            if (allowed) {
                TblOlclaim tblOlclaim = new TblOlclaim();

                tblOlclaim.setTitle(saveOlRequest.getTitle());
                tblOlclaim.setFirstname(saveOlRequest.getFirstname());
                tblOlclaim.setMiddlename(saveOlRequest.getMiddlename());
                tblOlclaim.setLastname(saveOlRequest.getLastname());
                tblOlclaim.setPostalcode(saveOlRequest.getPostalcode());
                tblOlclaim.setAddress1(saveOlRequest.getAddress1());
                tblOlclaim.setAddress2(saveOlRequest.getAddress2());
                tblOlclaim.setAddress3(saveOlRequest.getAddress3());
                tblOlclaim.setCity(saveOlRequest.getCity());
                tblOlclaim.setRegion(saveOlRequest.getRegion());

                tblOlclaim.setAccdatetime(saveOlRequest.getAccdatetime());
                tblOlclaim.setAccdescription(saveOlRequest.getAccdescription());
                tblOlclaim.setAcclocation(saveOlRequest.getAcclocation());
                tblOlclaim.setAccreportedto(saveOlRequest.getAccreportedto());
                tblOlclaim.setDescribeinjuries(saveOlRequest.getDescribeinjuries());
                tblOlclaim.setInjuriesduration(saveOlRequest.getInjuriesduration());
                tblOlclaim.setLandline(saveOlRequest.getLandline());
                tblOlclaim.setMedicalattention(saveOlRequest.getMedicalattention());
                tblOlclaim.setMobile(saveOlRequest.getMobile());
                tblOlclaim.setNinumber(saveOlRequest.getNinumber());
                tblOlclaim.setOccupation(saveOlRequest.getOccupation());
                tblOlclaim.setPassword(saveOlRequest.getPassword());
                tblOlclaim.setWitnesses(saveOlRequest.getWitnesses());
                tblOlclaim.setDob(saveOlRequest.getDob());
                tblOlclaim.setEmail(saveOlRequest.getEmail());
                tblOlclaim.setRemarks(saveOlRequest.getRemarks());
                tblOlclaim.setStatus(new BigDecimal(669));
                tblOlclaim.setCreateuser(new BigDecimal(tblUser.getUsercode()));
                tblOlclaim.setCreatedate(new Date());
                tblOlclaim.setAcctime(saveOlRequest.getAcctime());
                tblOlclaim.setAdvisor(saveOlRequest.getAdvisor());
                tblOlclaim.setIntroducer(saveOlRequest.getIntroducer());
                tblOlclaim.setAnywitnesses(saveOlRequest.getAnywitnesses());
                tblOlclaim.setEsig("N");

                TblCompanyprofile tblCompanyprofile = olService.getCompanyProfile(tblUser.getCompanycode());
                tblOlclaim.setOlcode(tblCompanyprofile.getTag() + "-" + tblCompanyprofile.getTagnextval());

                tblOlclaim = olService.saveOlRequest(tblOlclaim);

                if (tblOlclaim != null) {
                    tblCompanyprofile.setTagnextval(tblCompanyprofile.getTagnextval() + 1);
                    tblCompanyprofile = olService.saveCompanyProfile(tblCompanyprofile);

                    LOG.info("\n EXITING THIS METHOD == addNewOlCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Save SuccessFully", tblOlclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewOlCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewOlCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == OlPostApi \n METHOD == addNewOlCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewOlCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/updateOlCase", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> updateOlCase(@RequestBody UpdateOlRequest updateOlRequest,
                                                                HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlPostApi \n METHOD == updateHdrCase(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewOlCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = olService.isOlCaseAllowed(tblUser.getCompanycode(), "7");

            if (allowed) {
                TblOlclaim tblOlclaim = olService.findOlCaseByIdWithoutUser(updateOlRequest.getOlclaimcode());

                tblOlclaim.setTitle(updateOlRequest.getTitle());
                tblOlclaim.setFirstname(updateOlRequest.getFirstname());
                tblOlclaim.setMiddlename(updateOlRequest.getMiddlename());
                tblOlclaim.setLastname(updateOlRequest.getLastname());
                tblOlclaim.setPostalcode(updateOlRequest.getPostalcode());
                tblOlclaim.setAddress1(updateOlRequest.getAddress1());
                tblOlclaim.setAddress2(updateOlRequest.getAddress2());
                tblOlclaim.setAddress3(updateOlRequest.getAddress3());
                tblOlclaim.setCity(updateOlRequest.getCity());
                tblOlclaim.setRegion(updateOlRequest.getRegion());
                tblOlclaim.setAcctime(updateOlRequest.getAcctime());
                tblOlclaim.setAccdatetime(updateOlRequest.getAccdatetime());
                tblOlclaim.setAccdescription(updateOlRequest.getAccdescription());
                tblOlclaim.setAcclocation(updateOlRequest.getAcclocation());
                tblOlclaim.setAccreportedto(updateOlRequest.getAccreportedto());
                tblOlclaim.setDescribeinjuries(updateOlRequest.getDescribeinjuries());
                tblOlclaim.setInjuriesduration(updateOlRequest.getInjuriesduration());
                tblOlclaim.setLandline(updateOlRequest.getLandline());
                tblOlclaim.setMedicalattention(updateOlRequest.getMedicalattention());
                tblOlclaim.setMobile(updateOlRequest.getMobile());
                tblOlclaim.setNinumber(updateOlRequest.getNinumber());
                tblOlclaim.setOccupation(updateOlRequest.getOccupation());
                tblOlclaim.setPassword(updateOlRequest.getPassword());
                tblOlclaim.setWitnesses(updateOlRequest.getWitnesses());
                tblOlclaim.setDob(updateOlRequest.getDob());
                tblOlclaim.setEmail(updateOlRequest.getEmail());
                tblOlclaim.setRemarks(updateOlRequest.getRemarks());
                tblOlclaim.setAnywitnesses(updateOlRequest.getAnywitnesses());
                tblOlclaim.setUpdatedate(new Date());
                tblOlclaim.setLastupdateuser(tblUser.getUsercode());
                tblOlclaim = olService.updateOlRequest(tblOlclaim);

                if (tblOlclaim != null) {
                    LOG.info("\n EXITING THIS METHOD == addNewOlCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Case Updated SuccessFully", tblOlclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == addNewOlCase(); \n\n\n");
                    return getResponseFormat(HttpStatus.NOT_FOUND, "Case Save UnSuccessFully", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addNewOlCase(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == OlPostApi \n METHOD == addNewOlCase();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addNewOlCase(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addOlNotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addOlNotes(@RequestBody OlNoteRequest OlNoteRequest,
                                                              HttpServletRequest request) throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlPostApi \n METHOD == addOlNotes(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addOlNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            boolean allowed = olService.isOlCaseAllowed(tblUser.getCompanycode(), "7");

            if (allowed) {
                if (!OlNoteRequest.getOlCode().isEmpty()) {
                    TblOlnote tblOlnote = new TblOlnote();
                    TblOlclaim tblOlclaim = new TblOlclaim();

                    tblOlclaim.setOlclaimcode(Long.valueOf(OlNoteRequest.getOlCode()));
                    tblOlnote.setTblOlclaim(tblOlclaim);
                    tblOlnote.setNote(OlNoteRequest.getNote());
                    tblOlnote.setUsercategorycode(OlNoteRequest.getUserCatCode());
                    tblOlnote.setCreatedon(new Date());
                    tblOlnote.setUsercode(tblUser.getUsercode());

                    tblOlnote = olService.addTblOlNote(tblOlnote);

                    if (tblOlnote != null && tblOlnote.getOlnotecode() > 0) {

                        LOG.info("\n EXITING THIS METHOD == addOlNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Note Added SuccessFully.", tblOlnote);

                    } else {
                        LOG.info("\n EXITING THIS METHOD == addOlNotes(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Adding Note", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == addOlNotes(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Selected..", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addOlNotes(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Transaction",
                        null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == OlPostApi \n METHOD == addOlNotes();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addOlNotes(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/resendOlEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> resendOlEmail(@RequestBody ResendOlMessageDto resendOlMessageDto,
                                                                 HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == OlPostApi \n METHOD == resendRtaEmail(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addNewRtaCase(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblEmail tblEmail = olService.resendEmail(resendOlMessageDto.getOlmessagecode());
            if (tblEmail != null && tblEmail.getEmailcode() > 0) {

                LOG.info("\n EXITING THIS METHOD == resendElEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Success", tblEmail);
            } else {
                LOG.info("\n EXITING THIS METHOD == resendElEmail(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While resending Email", null);
            }
          

        } catch (Exception e) {
            LOG.error("\n CLASS == OlPostApi \n METHOD == resendRtaEmail();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == resendRtaEmail(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }



    @RequestMapping(value = "/performActionOnOlFromDirectIntro", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnOlFromDirectIntro(
            @RequestBody PerformHotKeyOnRtaRequest performHotKeyOnElRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlPostApi \n METHOD == performActionOnOlFromDirectIntro(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnOlFromDirectIntro(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            if (tblUser != null) {
                TblOlclaim tblOlclaim = olService.performActionOnOlFromDirectIntro(performHotKeyOnElRequest,
                        tblUser);
                if (tblOlclaim != null) {

                    LOG.info("\n EXITING THIS METHOD == performActionOnOlFromDirectIntro(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblOlclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnOlFromDirectIntro(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnOlFromDirectIntro(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == OlPostApi \n METHOD == performActionOnOlFromDirectIntro();  ERROR ----- "
                    + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnOlFromDirectIntro(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performActionOnOl", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> performActionOnOl(
            @RequestBody PerformActionOnOlRequest performActionOnOlRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == OlPostApi \n METHOD == performActionOnHdr(); ");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblOlclaim tblOlclaim = olService.findOlCaseByIdWithoutUser(Long.parseLong(performActionOnOlRequest.getOlcode()));


            if (performActionOnOlRequest.getToStatus().equals("675")) {
                List<TblOltask> tblOltasks = olService
                        .getOlTaskAgainstOLCodeAndStatus(performActionOnOlRequest.getOlcode(), "N");
                List<TblOltask> tblOltasks1 = olService
                        .getOlTaskAgainstOLCodeAndStatus(performActionOnOlRequest.getOlcode(), "P");

                if (tblOltasks != null && tblOltasks.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Some Task Are Not Performed", tblOlclaim);
                } else if (tblOltasks1 != null && tblOltasks1.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Some Task Are In Pending State", tblOlclaim);
                } else {

                }
            }


            if (tblUser != null) {
                if ((performActionOnOlRequest.getToStatus().equals("679")) ||
                        (performActionOnOlRequest.getToStatus().equals("719")) ||
                        (performActionOnOlRequest.getToStatus().equals("720")) ||
                        (performActionOnOlRequest.getToStatus().equals("721"))) {
                    // reject status
                    tblOlclaim = olService.performRejectCancelActionOnOl(performActionOnOlRequest.getOlcode(),
                            performActionOnOlRequest.getToStatus(), performActionOnOlRequest.getReason(), tblUser);

                } else if (performActionOnOlRequest.getToStatus().equals("682")) {
                    //cancel status
                    tblOlclaim = olService.performRejectCancelActionOnOl(performActionOnOlRequest.getOlcode(),
                            performActionOnOlRequest.getToStatus(), performActionOnOlRequest.getReason(), tblUser);

                } else if (performActionOnOlRequest.getToStatus().equals("692")) {
                    // revert status
                    tblOlclaim = olService.performRevertActionOnOl(performActionOnOlRequest.getOlcode(),
                            performActionOnOlRequest.getToStatus(), performActionOnOlRequest.getReason(), tblUser);

                } else {
                    tblOlclaim = olService.performActionOnOl(performActionOnOlRequest.getOlcode(),
                            performActionOnOlRequest.getToStatus(), tblUser);
                }

                if (tblOlclaim != null) {
//                    TblOlclaim olclaim = olService
//                            .findOlCaseById(Long.valueOf(performActionOnOlRequest.getOlCode()), tblUser);
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Action Performed", tblOlclaim);
                } else {
                    LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "Error Performing Action", null);
                }

            } else {
                LOG.info("\n EXITING THIS METHOD == performActionOnEl(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Logged In", null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("\n CLASS == OlPostApi \n METHOD == performActionOnHdr();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performActionOnHdr(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/addESign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> addESign(@RequestBody String requestData, HttpServletRequest request)
            throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == OlPostApi \n METHOD == addESign(); ");
        try {


            AddESign addESign = new AddESign();

            JSONObject jsonObject = new JSONObject(requestData);
            @SuppressWarnings("unchecked")
            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (key.equals("code")) {
                    addESign.setRtaCode(Long.valueOf(jsonObject.get(key).toString()));
                } else if (key.equals("eSign")) {
                    addESign.seteSign(jsonObject.get(key).toString());
                }
            }

            TblOlclaim tblOlclaim = olService.findOlCaseByIdWithoutUser(addESign.getRtaCode());
            if (tblOlclaim != null && tblOlclaim.getOlclaimcode() > 0) {

                if (tblOlclaim.getEsig().equals("Y")) {
                    List<TblOldocument> tblOldocuments = olService.getAuthOlCasedocuments(tblOlclaim.getOlclaimcode());
                    for (TblOldocument tblOldocument : tblOldocuments) {
                        if (tblOldocument.getDoctype().equalsIgnoreCase("Esign")) {
                            HashMap<String, Object> map = new HashMap<>();
                            map.put("signedDocument", tblOldocument.getDocbase64());
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.OK, "You Have Already Signed The Document",
                                    map);
                        }
                    }

                }
                // solicitor company
                TblCompanyprofile tblCompanyprofile = olService
                        .getCompanyProfileAgainstOlCode(tblOlclaim.getOlclaimcode());
                if (tblCompanyprofile != null) {

                    TblCompanydoc tblCompanydoc = olService.getCompanyDocs(tblCompanyprofile.getCompanycode());
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                    byte[] decodedBytes = Base64.getDecoder().decode(addESign.geteSign());
                    ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                    BufferedImage image1 = ImageIO.read(bis);
                    bis.close();


                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();

                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            while (keys.hasNext()) {
                                String key = keys.next();
                                if (!key.equals("code") && !key.equals("eSign")) {
                                    if (fieldName.equals(key)) {
                                        textField.setValue(jsonObject.getString(key));
                                    }
                                }
                            }

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = olService.getOlDbColumnValue(fieldName.replace("DB_", ""),
                                        tblOlclaim.getOlclaimcode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            }
                            if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    for (int a = 0; a < document.getNumberOfPages(); a++) {
                        PDPage p = document.getPage(a);
                        PDResources resources = p.getResources();
                        for (COSName xObjectName : resources.getXObjectNames()) {
                            PDXObject xObject = resources.getXObject(xObjectName);
                            if (xObject instanceof PDImageXObject) {
                                PDImageXObject original_img = ((PDImageXObject) xObject);
                                PDImageXObject pdImageXObject = LosslessFactory.createFromImage(document, image1);
                                resources.put(xObjectName, pdImageXObject);
                            }
                        }
                    }

                    LOG.info("\n Esign Document Prepared SuccessFully\n");
                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Ol\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Ol\\";

                    File theDir = new File(UPLOADED_FOLDER + tblOlclaim.getOlclaimcode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblOlclaim.getOlcode() + "_Signed.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblOlclaim.getOlcode() + "_Signed.pdf");
                    }

                    tblOlclaim.setEsig("Y");
                    tblOlclaim.setEsigdate(new Date());
                    tblOlclaim = olService.updateOlCase(tblOlclaim);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    LOG.info("\n Esign Saved on folder SuccessFully SuccessFully\n");
                    File file = new File(theDir.getPath() + "\\" + tblOlclaim.getOlcode() + "_Signed.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    TblTask tblOltaskdocument = new TblTask();
                    tblOltaskdocument.setTaskcode(25);

                    TblOldocument tblOldocument = new TblOldocument();
                    tblOldocument.setTblOlclaim(tblOlclaim);
                    tblOldocument.setDocbase64(encoded);
                    tblOldocument.setDocname("Esign");
                    tblOldocument.setDoctype("Esign");
                    tblOldocument.setCreateddate(new Date());
                    tblOldocument.setCreateuser("1");
                    tblOldocument.setDocurl(
                            UPLOADED_SERVER + tblOlclaim.getOlclaimcode() + "\\" + tblOlclaim.getOlcode() + "_Signed.pdf");
                    tblOldocument.setTblTask(tblOltaskdocument);

                    tblOldocument = olService.addOlSingleDocumentSingle(tblOldocument);
                    LOG.info("\n saving doc in database\n");
                    if (tblOldocument != null && tblOldocument.getOldocumentscode() > 0) {
//                    Locale.setDefault(backup);

                        TblOltask tblOltask = new TblOltask();
                        tblOltask.setOltaskcode(25);
                        tblOltask.setTblOlclaim(tblOlclaim);
                        tblOltask.setStatus("C");
                        tblOltask.setRemarks("Completed");
                        tblOltask.setCompletedon(new Date());

                        int tblRtatask1 = olService.updateTblOlTask(tblOltask);
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("doc", encoded);
                        TblUser legalUser = olService.findByUserId("2");

                        // cleint Email
                        TblEmailTemplate tblEmailTemplate = olService.findByEmailType("esign-clnt-complete");
                        String clientbody = tblEmailTemplate.getEmailtemplate();

                        clientbody = clientbody.replace("[LEGAL_CLIENT_NAME]", tblOlclaim.getFirstname() +(tblOlclaim.getMiddlename() != null?tblOlclaim.getMiddlename():"") +" "+tblOlclaim.getLastname());
//                                tblElclaim.getFirstname() + " "
////                                        + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
////                                        + tblElclaim.getLastname());
                        clientbody = clientbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());

                        olService.saveEmail(tblOlclaim.getEmail(), clientbody, "No Win No Fee Agreement ", tblOlclaim, legalUser, file.getPath());
                        LOG.info("\n Email Sent To client\n");
                        // Introducer Email
                        tblEmailTemplate = olService.findByEmailType("esign-Int-sol-complete");
                        String Introducerbody = tblEmailTemplate.getEmailtemplate();
                        TblUser introducerUser = olService.findByUserId(String.valueOf(tblOlclaim.getAdvisor()));

                        Introducerbody = Introducerbody.replace("[USER_NAME]", introducerUser.getLoginid());
                        Introducerbody = Introducerbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
                        Introducerbody = Introducerbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() +(tblOlclaim.getMiddlename() != null?tblOlclaim.getMiddlename():"") +" "+tblOlclaim.getLastname());
//                                tblElclaim.getFirstname() + " "
//                                        + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                                        + tblElclaim.getLastname());
                        Introducerbody = Introducerbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        Introducerbody = Introducerbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblOlclaim.getOlclaimcode());

                        olService.saveEmail(introducerUser.getUsername(), Introducerbody, "Esign", tblOlclaim, introducerUser);
                        LOG.info("\n Email Sent to intro\n");
                        // PI department Email
                        String LegalPideptbody = tblEmailTemplate.getEmailtemplate();

                        LegalPideptbody = LegalPideptbody.replace("[USER_NAME]", legalUser.getLoginid());
                        LegalPideptbody = LegalPideptbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
                        LegalPideptbody = LegalPideptbody.replace("[SOLICITOR_COMPANY_NAME]", tblCompanyprofile.getName());
                        LegalPideptbody = LegalPideptbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() +(tblOlclaim.getMiddlename() != null?tblOlclaim.getMiddlename():"") +" "+tblOlclaim.getLastname());
//                                tblElclaim.getFirstname() + " "
//                                        + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                                        + tblElclaim.getLastname());
                        LegalPideptbody = LegalPideptbody.replace("[CASE_URL]", getDataFromProperties("browser.url.rta") + tblOlclaim.getOlclaimcode());

                        olService.saveEmail(legalUser.getUsername(), LegalPideptbody, "Esign", tblOlclaim, legalUser);


                        saveEsignSubmitStatus(String.valueOf(tblOlclaim.getOlclaimcode()), "7", request);

                        LOG.info("\n Email Sent to Dept\n");
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "You Have SuccessFully Signed The CFA, A Copy Of That CFA Has Been Sent To The Provided Email.", map);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST,
                                "Error While Saving The Document..Esign Compeleted", null);
                    }

                } else {
                    LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No document template found against provided RTA",
                            null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == OlPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/getESignFields", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> getESignFields(@RequestBody ESignFieldsRequest eSignFieldsRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == OlPostApi \n METHOD == addESign(); ");
        try {
            TblOlclaim tblOlclaim = olService.findOlCaseByIdWithoutUser(Long.valueOf(eSignFieldsRequest.getCode()));

            if (tblOlclaim != null && tblOlclaim.getOlclaimcode() > 0) {
                TblCompanyprofile tblCompanyprofile = olService
                        .getCompanyProfileAgainstOlCode(tblOlclaim.getOlclaimcode());
                if (!tblOlclaim.getEsig().equals("Y")) {

                    TblCompanydoc tblCompanydoc = olService.getCompanyDocs(tblCompanyprofile.getCompanycode());
                    if (tblCompanydoc == null) {
                        LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "No CFA document found", null);
                    }
                    HashMap<String, String> fields = new HashMap<>();
                    PDDocument document = PDDocument.load(new File(tblCompanydoc.getPath()));
                    PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();


                    /////////////////////// FILL the CFA With Client DATA ////////////////////////


                    // Iterate through form field names and set values from JSON
                    for (PDField field : acroForm.getFieldTree()) {
                        if (field instanceof PDTextField) {
                            PDTextField textField = (PDTextField) field;
                            String fieldName = textField.getFullyQualifiedName();

                            if (fieldName.startsWith("DB_")) {
                                String keyValue = olService.getOlDbColumnValue(fieldName.replace("DB_", ""),
                                        tblOlclaim.getOlclaimcode());
                                if (keyValue.isEmpty()) {
                                    textField.setValue("");
                                } else {
                                    textField.setValue(keyValue);
                                }

                            } else if (fieldName.startsWith("DATE")) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                                Date date = new Date();
                                String s = formatter.format(date);
                                textField.setValue(s);
                            } else {
                                fields.put(fieldName, fieldName);
                            }

                            field.setReadOnly(true);

                        }
                    }

                    /////////////////////// END OF FILL the CFA With Client DATA /////////////////

                    String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Ol\\";
                    String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Ol\\";

                    File theDir = new File(UPLOADED_FOLDER + tblOlclaim.getOlclaimcode());
                    if (!theDir.exists()) {
                        theDir.mkdirs();
                        document.save(theDir.getPath() + "\\" + tblOlclaim.getOlcode() + "_view.pdf");
                    } else {
                        document.save(theDir.getPath() + "\\" + tblOlclaim.getOlcode() + "_view.pdf");
                    }

                    File file = new File(theDir.getPath() + "\\" + tblOlclaim.getOlcode() + "_view.pdf");
                    String encoded = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(file));

                    fields.put("doc", encoded);
                    fields.put("isEsign", "N");

                    saveEsignOpenStatus(String.valueOf(tblOlclaim.getOlclaimcode()), "7", request);

                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Success", fields);
                } else {

                    List<TblOldocument> tblOldocuments = olService.getAuthOlCasedocuments(tblOlclaim.getOlclaimcode());
                    if (tblOldocuments != null && tblOldocuments.size() > 0) {
                        for (TblOldocument tblOldocument : tblOldocuments) {
                            if (tblOldocument.getTblTask() != null && tblOldocument.getTblTask().getTaskcode() == 25) {
                                HashMap<String, String> fields = new HashMap<>();
                                fields.put("doc", tblOldocument.getDocbase64());
                                fields.put("isEsign", "Y");
                                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                                return getResponseFormat(HttpStatus.OK, "You have Already Signed The Document.", fields);
                            }
                        }
                    }

                    LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Signed Document Found Please Contact Our HelpLine : please call us at 01615374448", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == getESignFields(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No EL document found", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == OlPostApi \n METHOD == addESign();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/performTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> performTask(@RequestParam("olClaimCode") long olClaimCode, @RequestParam("taskCode") long taskCode,
                                                               @RequestParam(value = "multipartFiles",required = false) List<MultipartFile> multipartFiles,
                                                               HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == OlPostApi \n METHOD == performTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }

            TblOlclaim tblOlclaim = olService.findOlCaseByIdWithoutUser(olClaimCode);
            TblOlsolicitor tblOlsolicitor = olService.getOLSolicitorsOfOlclaim(olClaimCode);
            TblCompanyprofile solicitorCompany = olService.getCompanyProfile(tblOlsolicitor.getCompanycode());
            if (tblOlclaim != null && tblOlclaim.getOlclaimcode() > 0) {
                TblTask tblTask = olService.getTaskAgainstCode(taskCode);
                if (tblTask != null && tblTask.getTaskcode() > 0) {
                    TblOltask tblOltask = new TblOltask();
                    tblOltask.setOltaskcode(taskCode);
                    tblOltask.setTblOlclaim(tblOlclaim);
                    if (tblTask.getTaskcode() == 25) {
                        TblCompanydoc tblCompanydoc = olService.getCompanyDocs(solicitorCompany.getCompanycode());
                        if (tblCompanydoc == null) {
                            LOG.info("\n EXITING THIS METHOD == addESign(); \n\n\n");
                            return getResponseFormat(HttpStatus.BAD_REQUEST, "There is No CFA Attach To the Solicitor", null);
                        }
                        String emailUrl = getDataFromProperties("esign.baseurl") + "Ol=" + tblOlclaim.getOlclaimcode();
                        TblEmailTemplate tblEmailTemplate = getEmailTemplateByType("esign");

                        String body = tblEmailTemplate.getEmailtemplate();
                        body = body.replace("[LEGAL_CLIENT_NAME]", tblOlclaim.getFirstname() +(tblOlclaim.getMiddlename() != null?tblOlclaim.getMiddlename():"") +" "+tblOlclaim.getLastname());
                        body = body.replace("[SOLICITOR_COMPANY_NAME]", solicitorCompany.getName());
                        body = body.replace("[ESIGN_URL]", emailUrl);
                        if (tblOlclaim.getEmail() != null) {
//                            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
//                            return getResponseFormat(HttpStatus.BAD_REQUEST, "No Client Email Address Found", null);
                            LOG.info("\n Email Sendpos");
                            olService.saveEmail(tblOlclaim.getEmail(), body, "No Win No Fee Agreement", tblOlclaim, tblUser);
                        }
                        LOG.info("\n Sending SMS ");
                        if (tblOlclaim.getMobile() != null && !tblOlclaim.getMobile().isEmpty()) {
                            LOG.info("\n SMS SEND");
                            TblEmailTemplate tblEmailTemplateSMS = getEmailTemplateByType("esign-SMS");
                            String message = tblEmailTemplateSMS.getEmailtemplate();
                            message = message.replace("[LEGAL_CLIENT_NAME]", tblOlclaim.getFirstname() +(tblOlclaim.getMiddlename() != null?tblOlclaim.getMiddlename():"") +" "+tblOlclaim.getLastname());
                            message = message.replace("[SOLICITOR_COMPANY_NAME]", solicitorCompany.getName());
                            message = message.replace("[ESIGN_URL]", emailUrl);


                            sendSMS(tblOlclaim.getMobile(), message);
                            tblOltask.setRemarks("Esign/SMS Email Sent Successfully to Client");
                        } else {
                            tblOltask.setRemarks("Esign Email Sent Successfully to Client");
                        }

                        saveEmailSmsEsignStatus(String.valueOf(tblOlclaim.getOlclaimcode()), "7", tblUser.getUsercode());

                        tblOltask.setStatus("P");
                    } else {

                        String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Ol\\";
                        String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Ol\\";
                        List<TblOldocument> tblEldocuments = new ArrayList<TblOldocument>();

                        File theDir = new File(UPLOADED_FOLDER + tblOlclaim.getOlclaimcode());
                        if (!theDir.exists()) {
                            theDir.mkdirs();
                        }

                        for (MultipartFile multipartFile : multipartFiles) {
                            byte[] bytes = multipartFile.getBytes();
                            Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                            Files.write(path, bytes);
                            TblOldocument tblOldocument = new TblOldocument();
                            tblOldocument.setDocurl(UPLOADED_SERVER + tblOlclaim.getOlclaimcode() + "\\"
                                    + multipartFile.getOriginalFilename());
                            tblOldocument.setDocumentPath(tblOldocument.getDocurl());
                            tblOldocument.setDoctype(multipartFile.getContentType());
                            tblOldocument.setTblOlclaim(tblOlclaim);
                            tblOldocument.setTblTask(tblTask);
                            tblOldocument.setCreateuser(tblUser.getUsercode());
                            tblOldocument.setCreateddate(new Date());

                            tblEldocuments.add(tblOldocument);
                        }
                        olService.addOlDocument(tblEldocuments);
                        tblOltask.setStatus("C");
                        tblOltask.setRemarks("Completed");

                    }
                    tblOltask.setCompletedon(new Date());
                    tblOltask.setCompletedby(tblUser.getUsercode());
                    int tblRtatask1 = olService.updateTblOlTask(tblOltask);
                    if (tblRtatask1 > 0) {

                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.OK, "Task Performed Successfully", tblRtatask1);
                    } else {
                        LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                        return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Performing Task", null);
                    }
                } else {
                    LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, "No Task Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No RTA document found", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == OlPostApi \n METHOD == performTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == performTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }


    @RequestMapping(value = "/addOlDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<HashMap<String, Object>> addOlDocument(@RequestParam("olClaimCode") String olClaimCode,
                                                                 @RequestParam("multipartFiles") List<MultipartFile> multipartFiles, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == ElPostApi \n METHOD == addOlDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == addOlDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblOlclaim tblOlclaim = olService.findOlCaseByIdWithoutUser(Long.valueOf(olClaimCode));
            if (tblOlclaim != null && tblOlclaim.getOlclaimcode() > 0) {
//                String UPLOADED_FOLDER = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0_Tomcat9-la\\webapps\\El\\";
//                String UPLOADED_SERVER = "http:\\115.186.147.30:8080\\El\\";
                String UPLOADED_FOLDER = getDataFromProperties("UPLOADED_FOLDER") + "\\Ol\\";
                String UPLOADED_SERVER = getDataFromProperties("UPLOADED_SERVER") + "\\Ol\\";
                List<TblOldocument> tblEldocuments = new ArrayList<TblOldocument>();

                File theDir = new File(UPLOADED_FOLDER + tblOlclaim.getOlclaimcode());
                if (!theDir.exists()) {
                    theDir.mkdirs();
                }

                for (MultipartFile multipartFile : multipartFiles) {
                    byte[] bytes = multipartFile.getBytes();
                    Path path = Paths.get(theDir + "\\" + multipartFile.getOriginalFilename());
                    Files.write(path, bytes);
                    TblOldocument tblOldocument = new TblOldocument();
                    tblOldocument.setDocurl(
                            UPLOADED_SERVER + tblOlclaim.getOlclaimcode() + "\\" + multipartFile.getOriginalFilename());
                    tblOldocument.setDocumentPath(tblOldocument.getDocurl());
                    tblOldocument.setDoctype(multipartFile.getContentType());
                    tblOldocument.setTblOlclaim(tblOlclaim);
                    tblOldocument.setCreateuser(tblUser.getUsercode());
                    tblOldocument.setCreateddate(new Date());

                    tblEldocuments.add(tblOldocument);
                }

                tblEldocuments = olService.addOlDocument(tblEldocuments);

                // Legal internal Email
                TblEmailTemplate tblEmailTemplate = olService.findByEmailType("document-add");
                String legalbody = tblEmailTemplate.getEmailtemplate();
                TblUser legalUser = olService.findByUserId("2");

                legalbody = legalbody.replace("[USER_NAME]", legalUser.getLoginid());
                legalbody = legalbody.replace("[CASE_NUMBER]", tblOlclaim.getOlcode());
                legalbody = legalbody.replace("[CLIENT_NAME]", tblOlclaim.getFirstname() +(tblOlclaim.getMiddlename() != null?tblOlclaim.getMiddlename():"") +" "+tblOlclaim.getLastname());
//                        tblElclaim.getFirstname() + " "
//                                + (tblElclaim.getMiddlename() == null ? "" : tblElclaim.getMiddlename() + " ")
//                                + tblElclaim.getLastname());
                legalbody = legalbody.replace("[COUNT]", String.valueOf(tblEldocuments.size()));
                legalbody = legalbody.replace("[CASE_URL]", getDataFromProperties("browser.url.El") + tblOlclaim.getOlclaimcode());

                olService.saveEmail(legalUser.getUsername(), legalbody,
                        tblOlclaim.getOlcode() + " | Processing Note", tblOlclaim, legalUser);

                LOG.info("\n EXITING THIS METHOD == addElDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.OK, "Document Saved Successfully", null);
            } else {
                return getResponseFormat(HttpStatus.BAD_REQUEST, "No Case Find", null);
            }
        } catch (Exception e) {
            LOG.error("\n CLASS == ElPostApi \n METHOD == addOlDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == addOlDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/setCurrentTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> setCurrentTask(@RequestBody CurrentTaskRequest currentTaskRequest,
                                                                  HttpServletRequest request) throws ParseException {
        LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == setCurrentTask(); ");
        try {
            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            int tblRtatask1 = olService.updateCurrentTask(currentTaskRequest.getTaskCode(),
                    currentTaskRequest.getOlCode(), currentTaskRequest.getCurrent());
            if (tblRtatask1 > 0) {
                List<TblOltask> tblEltaskList = olService.findTblOlTaskByOlClaimCode(currentTaskRequest.getOlCode());

                if (tblEltaskList != null && tblEltaskList.size() > 0) {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.OK, "Current Task Updated SuccessFully", tblEltaskList);
                } else {
                    LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                    return getResponseFormat(HttpStatus.BAD_REQUEST, " No Record Found", null);
                }
            } else {
                LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "Error While Updating Task", null);
            }

        } catch (Exception e) {
            LOG.error("\n CLASS == RtaPostApi \n METHOD == setCurrentTask();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == setCurrentTask(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

    @RequestMapping(value = "/deleteElDocument", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HashMap<String, Object>> deleteRtaDocument(
            @RequestBody DeleteRtaDocumentRequest deleteRtaDocumentRequest, HttpServletRequest request)
            throws ParseException {
        try {
            LOG.info("\n\n\nINSIDE \n CLASS == RtaPostApi \n METHOD == deleteRtaDocument(); ");

            TblUser tblUser = getLoggedUserData(request.getHeader("Authorization"));
            if (tblUser == null) {
                LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.UNAUTHORIZED, "Your Are Not Logged In.", null);
            }
            TblCompanyprofile tblCompanyprofile = olService.getCompanyProfile(tblUser.getCompanycode());
            if (tblCompanyprofile.getTblUsercategory().getCategorycode().equals("4")) {

                olService.deleteOlDocument(deleteRtaDocumentRequest.getDoccode());
                return getResponseFormat(HttpStatus.OK, "Document Deleted", null);
            } else {
                LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
                return getResponseFormat(HttpStatus.BAD_REQUEST, "You Are Not Allowed To Perform This Action", null);
            }

        } catch (Exception e) {
            LOG.error(
                    "\n CLASS == RtaPostApi \n METHOD == deleteRtaDocument();  ERROR ----- " + e.getLocalizedMessage());
            LOG.info("\n EXITING THIS METHOD == deleteRtaDocument(); \n\n\n");
            return getResponseFormat(HttpStatus.NOT_FOUND, "General Processing Error", null);
        }
    }

}
